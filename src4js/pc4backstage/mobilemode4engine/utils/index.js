import { message } from "antd"
import { WeaTools } from 'ecCom';
import { urls, SPRIT, GET, MOBILE_MODE_HISTORY_FOR_ENGINE } from "../constants";

const { success } = message;
const { callApi, ls } = WeaTools;

module.exports = {
    _api: (mod, settings) => { // 封装callApi
        let { data, action, type, dataType, message, url } = settings;

        url = url || `${urls[mod]}${mod}${SPRIT}${action}`; // url
        dataType = dataType || "json";
        type = type || GET;

        return callApi(url, type, data, dataType).then(data => {
            message && success(message);
            return data;
        });
    },
    getHistory: () => {
        return ls.getJSONObj(MOBILE_MODE_HISTORY_FOR_ENGINE) || {};
    },
    setHistory: mobilemodeHistory4Engine => {
        ls.set(MOBILE_MODE_HISTORY_FOR_ENGINE, mobilemodeHistory4Engine);
    }
}