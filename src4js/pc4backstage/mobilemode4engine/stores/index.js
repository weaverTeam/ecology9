// 应用列表
import  AppStore from "./AppStore";

// 创建应用
import AppCreateStore from "./AppCreateStore";

// 母版 
import TemplateStore from "./TemplateStore";

// 皮肤列表
import SkinStore from "./SkinStore";

// 函数列表
import FunctionStore from "./FunctionStore";

const appStore = new AppStore();
const appCreateStore = new AppCreateStore();
const templateStore = new TemplateStore();
const appSkinStore = new SkinStore();
const appFunctionStore = new FunctionStore();

module.exports = {
    appStore,
    appCreateStore,
    templateStore,
    appSkinStore,
    appFunctionStore
}