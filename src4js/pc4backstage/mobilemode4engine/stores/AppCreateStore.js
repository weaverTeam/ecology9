import { observable, computed, action, toJS } from 'mobx';
import { formmodeApplist, modelist, appmodelayout, save } from "../apis/appCreate"
import { STEP_FINISHED } from "../constants/appCreate"
import ObjectAssign from "object-assign";

export default class AppCreateStore {
    @observable applist = [];
    @observable modelist = [];
    @observable appinfo = { //　应用信息　保存应用所需参数
        appbaseinfo: {
            ispublish: true
        }
    };
    @observable modelayout = null;
    @observable modelayoutCount = {}; // 统计使用过的mode 当为0时 代表未选择该模块 {modeid: number,...}
    @observable modelayoutsCache = {}; // 缓存已经选择过的mode
    @observable modeSearchText = "";
    @observable currentPage = 1;
    @observable pageSize = 10;
    @observable currentStep = 0;  // 当前所在步骤
    @observable stepStatus = STEP_FINISHED; // 0 直接完成 1下一步 
    @observable appid = 0; // 选择的应用id
    @observable modeid = 0; // 选择的模块id
    @observable isValidateAppForm = false; // 开启应用表单验证
    @observable isCreating = false; // 是否正在请求创建应用
    @computed get qualifiedModes() { // 符合条件的mode
        let text = this.modeSearchText.toLowerCase();

        return this.modelist.filter(mode => {
            return ~mode.entityname.toLowerCase().indexOf(text);
        });
    }

    // 当前步骤变化
    @action stepChange = currentStep => {
        this.currentStep = currentStep;
    }

    // 验证应用信息是否合法
    @action validateAppForm = isValidateAppForm => {
        this.isValidateAppForm = isValidateAppForm;
    }

    @action stepStatusChange = stepStatus => {
        this.stepStatus = stepStatus;
    }

    // 获取表单建模应用列表
    @action getAppList = () => {
        let store = this;
        return formmodeApplist().then(res => {
            let appid = 0;
            const getAppid = app => {
                return app.children && app.children[0].id || app.id;
            };

            appid = getAppid(res.data[0]);
            store.applist = res.data;
            store.appid = appid;
        });
    }

    // 选择表单建模应用
    @action selectApp = appid => {
        let store = this;
        let { modelayoutsCache } = store;

        appid = appid || store.appid;

        if(!appid) return Promise.resolve();

        return modelist(appid).then(res => {
            store.modelist = res.data;
            store.modeid = res.data[0] && res.data[0].id; // 默认选中第一个模块
            store.appid = appid;
            store.currentPage = 1;
            store.modelayout = null;
        });
    }

    @action selectMode = (appid, modeid) => {
        let store = this;
        let { modelayoutsCache } = store;

        appid = appid || store.appid;
        modeid = modeid || store.modeid;
        
        if(!modeid) {
            return (store.modelayout = null);
        };

        if(modelayoutsCache[modeid]) {
            store.modelayout = modelayoutsCache[modeid];
            store.modeid = modeid;
            return;
        }

        appmodelayout(appid, modeid).then(res => {
            modelayoutsCache[modeid] = res;
            store.modelayout = res;
            store.modeid = modeid;
        });
    }

    // 应用模块搜索
    @action searchMode = text => {
        this.modeSearchText = text;
        this.currentPage = 1;
        this.selectMode(this.appid, this.qualifiedModes.length && this.qualifiedModes[0].id);
    }

    // 模块分页的当前页改变
    @action currentPageChange = currentPage => {
        let defaultModeIndex = (currentPage-1) * this.pageSize; // 当前页第一个mode的下标
        this.currentPage = currentPage;
        this.selectMode(this.appid, this.qualifiedModes[defaultModeIndex].id);
    }

    // 选择布局后 modelayout发生改变
    @action modelayoutChange = (modelayout, exp) => {
        const { moduleId } = modelayout;
        let num = (this.modelayoutCount[moduleId] || 0) + exp;

        this.modelayout = null; // mobx的对象存在预定义key 所以当未声明的属性发生改变后 modelayout并不会有响应
        this.modelayout = modelayout; // 先重置后赋值
        this.modelayoutsCache[moduleId] = modelayout;
        this.modelayoutCount[moduleId] = num;
    }

    // 选择皮肤
    @action selectSkin = skinid => {
        this.appinfo.appbaseinfo.skin = skinid;
    }

    // 完成第一步后 设置应用的基本信息
    @action setAppBaseInfo = appBaseInfo => {
        ObjectAssign(this.appinfo.appbaseinfo, appBaseInfo);
    }

    // 设置应用的模块信息
    @action setAppModuleInfo = appModuleInfo => {
        this.appinfo.appModuleInfo = appModuleInfo;
    }

    // 保存应用
    @action saveApp = () => {
        let store = this;
        const { appinfo, modelayoutCount, modelayoutsCache } = this;
        const moduleInfo = Object.keys(modelayoutCount).map(modeid => {
            if (modelayoutCount[modeid] != 0) {
                return modelayoutsCache[modeid];
            }
        });

        this.isCreating = true;
        
        return save({
            appbaseinfo: toJS(appinfo.appbaseinfo),
            moduleInfo: moduleInfo
        }).then((data)=>{
            store.isCreating = false;
            return data;
        });
    }

    // 保存结束 重置store的值
    @action resetStore = () => {
        this.currentStep = 0; 
        this.modelayoutsCache = {};
        this.modelayoutCount = {};
        this.modeSearchText = "";
        this.appinfo = {
            appbaseinfo: {
                ispublish: true
            }
        }
    }
}