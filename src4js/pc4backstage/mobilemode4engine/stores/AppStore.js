import { observable, action } from 'mobx';
import { applist, skinlist, setSkin, publish, del, waste, appExport, modify, getPreviewImg } from "../apis/app";
import { ipaddr } from "../apis/common"

import { DELETE, sortType, CREATE_DATE, MODIFY_DATE } from "../constants/app";

const { DESC, ASC } = sortType;

export default class AppStore {
    @observable apps = []; // 应用列表
    @observable classifies = []; // 应用类型列表
    @observable statusType = -1; // 发布状态
    @observable classify = -1; // 应用行业
    @observable searchText = ""; // 搜索项
    @observable pageSize = 5; // 页面应用个数
    @observable current = 1; // 当前页
    @observable previewImgs = []; // 当前应用的缩略图
    @observable isCreating = false; // 是否创建 控制创建窗口的显示隐藏
    @observable isImport = false; // 是否导入 控制导入窗口的显示隐藏 
    @observable isQRCodeVisible = false; // 是否显示应用二维码
    @observable isSetSkin = false; // 是否设置皮肤 控制皮肤窗口的显示隐藏
    @observable appid = 0; // 当前所选应用的id
    @observable currentApp = {}; // 当前所选应用
    @observable ipaddr = ""; // 二维码的ipaddr
    
    constructor() { }

    // 获取应用列表
    @action getAppList = params => {
        params = params || {};
        let store = this;
        const appid = params.appid;
        return applist(params).then(data => {
            store.apps = data.items;
            store.classifies = data.classifies;
            if(appid && data.items.length) {
                store.onCurrentAppChange(appid);
            }
            return data.items;
        });
    }

    // 应用分页的当前页改变
    @action onCurrPageChange = current => {
        this.current = current;
    }

    // 当前所选应用更改
    @action onCurrentAppChange = appid => {
        if(!appid) {
            this.appid = 0;
            this.currentApp = null;
            return;
        }
        this.appid = appid;
        this.apps.every(app => {
            if(app.id != appid) return true;
            this.currentApp = app;
            return false;
        });
        this.getPreviewImg(appid);
    }

    //获取
    @action getIPAddr = () => {
        let store = this;

        ipaddr().then(data => {
            store.ipaddr = data.ipaddr;
        });
    }
    

    // 控制创建应用modal的显隐
    @action toggleAppCreate = isshow => {
        this.isCreating = isshow;
    }

    // 控制导入应用modal的显隐
    @action toggleImport = isshow => {
        this.isImport = isshow;
    }

    // 控制应用二维码的显隐
    @action toggleQRCode = isQRCodeVisible => {
        this.isQRCodeVisible = isQRCodeVisible;
    }

    // 显示应用皮肤modal
    @action showAppSkin = appid => {
        this.isSetSkin = true;
        this.appid = appid;
    }

    // 关闭应用皮肤modal
    @action hideAppSkin = () => {
        this.isSetSkin = false;
        this.skinlist = [];
    }

    // 设置应用皮肤
    @action setAppSkin = (appid, skin) => {
        let that = this;
        return setSkin({
            appid: appid,
            skinid: skin.id
        }).then(() => {
            that.currentApp.skin = skin;
        });
    }

    // 修改应用基本信息
    @action updateApp = appinfo => {
        let { apps, getAppList } = this;
        return modify(appinfo).then(data => {
            getAppList(appinfo);
        });
    }

    // 应用发布与下架
    @action publishApp = (appid, ispublish) => {
        const that = this;
        const appinfo = {
            appid: appid,
            optType: ispublish
        };
        
        this.currentApp.ispublish = ispublish;

        publish(appinfo).then(() => {
            const ispublish = appinfo.optType; // 1发布 0未发布

            that.apps = that.apps.map(app => {
                if (app.id == appinfo.appid) {
                    app.ispublish = appinfo.optType;
                }
                return app;
            });
        });
    }

    // 应用导出
    @action exportApp = appid => {
        appExport(appid).then(data => {
            window.open(data.url, "_self");
        });
    }

    // 应用删除与废弃 type: DELETE|WASTE
    @action removeApp = (appid, isDel) => {
        let { apps, onCurrentAppChange } = this;
        let _remove = isDel ? del : waste;

        _remove(appid).then(() => {
            apps.every((app, index) => {
                if (app.id != appid) return true;

                apps.splice(index, 1);
                return false;
            });
            onCurrentAppChange(0);
        });
    }

    // 发布状态变更
    @action statusChange = statusType => {
        this.statusType = statusType;
        this.onCurrPageChange(1);
    }

    // 行业条件变更
    @action classifyChange = classify => {
        this.classify = classify;
        this.onCurrPageChange(1);
    }

    // 搜索关键字变更
    @action onSearchTextChange =  searchText => {
        this.searchText = searchText;
        this.onCurrPageChange(1);
    }

    // 获取应用缩略图
    @action getPreviewImg = appid => {
        if(!this.appid) return;

        let store = this;

        getPreviewImg(this.appid).then(data=> {
            store.previewImgs = data.items;
        });
    }
}