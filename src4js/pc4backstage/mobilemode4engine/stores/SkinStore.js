import { observable, action } from 'mobx';

import { skinlist, del, save } from "../apis/skin"

export default class SkinStore {
    @observable skins = []; // 皮肤列表
    @observable isImport = false; // 是否导入皮肤
    @observable isCreate = false; // 是否新建皮肤
    @observable isRef = false; // 是否显示引用
    @observable isEdit = false; // 是否显示样式编辑
    @observable currSkin = null; // 引用的皮肤对象
    @observable currPage = 1; // 当前页
    @observable searchText = ""; // 模板搜索的关键字
    @observable pageSize = 10; // 每页显示皮肤的数量
    @observable currTemplate = null; // 当前正在编辑的模板

    // 获取皮肤列表
    @action getSkinList = () => {
        const store = this;
        
        return skinlist().then(result => {
            store.skins = result.data;
        })
    }

    // 皮肤删除
    @action onDelete = skinId => {
        const store = this;

        del(skinId).then(() => {
            store.skins = store.skins.filter(skin => {
                if(skin.id != skinId) return skin;
            });
        });
    }

    // 皮肤新建
    @action onSave = skin => {
        return save(skin);
    }

    // 皮肤搜索的关键字变更
    @action onSearchTextChange = searchText => {
        this.searchText = searchText;
    }

    // 显示数量改变
    @action onShowSizeChange = pageSize => {
        this.pageSize = pageSize;
        this.onCurrpageChange(1);
    }

    // 当前页改变
    @action onCurrpageChange = currPage => {
        this.currPage = currPage;
    }

    // 控制导入皮肤窗口的显隐
    @action toggleImport = isImport => {
        this.isImport = isImport;
    }

    // 控制新建皮肤窗口的显隐
    @action toggleCreate = isCreate => {
        this.isCreate = isCreate;
    }

    // 控制样式编辑窗口的显隐
    @action toggleEdit = isEdit => {
        this.isEdit = !!isEdit;
        isEdit && (this.currSkin = isEdit);
    }

    // 显示引用皮肤的应用列表的窗口
    @action toggleRef = isRef => {
        this.isRef = !!isRef;
        isRef && (this.currSkin = isRef);
    }
}