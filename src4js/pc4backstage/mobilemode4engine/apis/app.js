import { _api } from "../utils"
import { modules } from "../constants";

const { APP } = modules;

// 应用相关接口
module.exports  = {
    applist: params => { // 获取应用列表
        return _api(APP, { 
            action: "applist", 
            data: params 
        });
    },
    import: params => { // 应用导入
        return _api(APP, { 
            action: "import", 
            type: "POST", 
            data: params, 
            message: "导入成功" 
        });
    },
    modify: params => { // 应用修改
        return _api(APP, { 
            action: "modify", 
            type: "POST", 
            data: params,
            message: "修改成功" 
        });
    },
    appExport: appid => { // 应用导出
        return _api(APP, { 
            action: "export", 
            data: { appid: appid }
        });
    },
    publish: params => { // 应用发布与下架
        return _api(APP, { 
            action: "publish", 
            data: params
        });
    },
    waste: appid => { // 应用废弃
        return _api(APP, { 
            action: "waste", 
            data: { appid: appid }, 
            message: "废弃成功"
        });
    },
    del: appid => { // 应用删除
        return _api(APP, {
            action: "delete", 
            data:{ appid: appid }, 
            message: "删除成功"
        });
    },
    setSkin: params => { // 设置应用皮肤
        return _api(APP, {
            action: "setSkin", 
            data:params,
            message: "设置成功"
        });
    },
    getPreviewImg: appid => {
        return _api(APP, {
            action: "getPreviewImg",
            data: {
                appid: appid
            }
        });
    }
}