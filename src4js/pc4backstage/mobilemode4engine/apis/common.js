import { _api } from "../utils"
import { modules } from "../constants";

const { COMMON } = modules;

/** 公共接口 */
module.exports = {
    ipaddr: () => {
        return _api(COMMON, {
            action: "ipaddr"
        });
    }
}
