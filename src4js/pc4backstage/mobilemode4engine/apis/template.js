import { _api } from "../utils"
import { modules } from "../constants";

const { TEMPLATE } = modules;

// 模板相关接口
module.exports  = {
    list: () => { // 获取应用列表
        return _api(TEMPLATE, { 
            action: "list"
        });
    },
    del: templateId => {
        return _api(TEMPLATE, {
            action: "delete",
            data: {
                id: templateId
            },
            message: "删除成功"
        });
    },
    modify: template => {
        return _api(TEMPLATE, {
            action: "modify",
            data: template,
            type: "post",
            message: "修改成功"
        });
    }
}