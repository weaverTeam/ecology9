import { _api } from "../utils"
import { modules } from "../constants";

const { APP, FORMMODE } = modules;

// 创建应用相关接口
module.exports  = {
    formmodeApplist: () => {
        return _api(FORMMODE, {
            action: "applist"
        });
    },
    modelist: formmodeappid => {
        return _api(FORMMODE, {
            action: "modelist",
            data: {
                formmodeappid: formmodeappid
            }
        });
    },
    appmodelayout: (appid, modeid) => {
        return _api(APP, {
            action: "appmodelayout",
            data: {
                appid: appid,
                modelid: modeid
            }
        });
    },
    save: params => {
        return _api(APP, {
            action: "save",
            type: "POST",
            message: "应用创建成功",
            data: {"params": JSON.stringify(params)}
        });
    }
}