import { _api } from "../utils"
import { modules } from "../constants";

const { FUNCTION } = modules;

// 函数相关接口
module.exports  = {
    list: () => {
        return _api(FUNCTION, {
            action: "list"
        });
    },
    info: funcId => {
        return _api(FUNCTION, {
            action: "info",
            data: {id: funcId}
        });
    }
}