import { _api } from "../utils"
import { modules } from "../constants";

import UUID from "../lib/UUID";

const { SKIN } = modules;

// 应用相关接口
module.exports = {
    skinlist: params => {
        return _api(SKIN, {
            action: "list",
            data: params
        });
    },
    del: skinId => {
        return _api(SKIN, {
            action: "delete",
            data: {
                id: skinId
            },
            message: "删除成功"
        });
    },
    save: skin => {
        skin.id = new UUID().toString();

        return _api(SKIN, {
            action: "save",
            data: skin,
            type: "post",
            message: "新建成功"
        });
    }
}