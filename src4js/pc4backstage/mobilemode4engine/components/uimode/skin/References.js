import { Button } from "antd";
import { WeaScroll } from "ecCom";
import { Dialog } from "../common";

export default class Create extends React.Component {
    render() {
        const { visible, skin, onCancel } = this.props;
        const { appref, name } = skin;
        const btn = <Button type="primary" onClick={onCancel}>确定</Button>
        
        return (
            <Dialog
                title={`样式引用 - ${name}`}
                visible={visible}
                footer={btn}
                onCancel={onCancel}
                maskClosable={true}
            >
                <div style={{height: appref.length ? "210px" : "20px", overflow: "hidden"}}>
                    <WeaScroll typeClass="scrollbar-macosx" className="scrollbar-appref">
                        { appref.length ?
                            appref.map(app => {
                                return (
                                    <div className="mobilemode-appref">
                                        <img src={app.previewImg || '/mobilemode/images/e9/mobile-app.png'} />
                                        <div>
                                            <p>{app.appname}</p>
                                            <span>{app.descriptions || '无描述信息'}</span>
                                        </div>
                                    </div>
                                )
                            })
                            : <p>无应用引用该皮肤!</p>
                        }
                    </WeaScroll>
                </div>
            </Dialog>
        );
    }
}