import { observable } from "mobx";
import { observer } from "mobx-react";

import { Form } from "antd";
import { WeaInput, WeaTextarea } from "ecCom";
import { Dialog, ImgUpload } from "../common";

const FormItem = Form.Item;

@observer
class Create extends React.Component {
    @observable previewDataUrl = null;

    onSave() {
        const previewDataUrl = this.previewDataUrl;
        const { form, onSave, onCancel, onClosed } = this.props;

        form.validateFields(errors => {
            if(errors) return;

            const skin = form.getFieldsValue();
            
            skin.previewImg = previewDataUrl;
            onSave(skin).then(() => {
                onCancel();
                onClosed();
            });
        });
    }

    onFileChange(base64) {
        this.previewDataUrl = base64;
    }

    render() {
        const { visible, onCancel, form } = this.props;
        const { getFieldProps } = form;
        const formItemLayout = {
          labelCol: { span: 6 },
          wrapperCol: { span: 18 },
        };
        const nameProps = getFieldProps("name", {
            rules: [{
                required: true,
                message: "请输入样式名称"
            }]
        });

        return (
            <Dialog
                visible={visible}
                title="样式新建"
                onOk={this.onSave.bind(this)}
                onCancel={onCancel}>
                <Form form={form} className="template-edit">
                    <FormItem
                        {...formItemLayout}
                        label="名称"
                    >
                        <WeaInput {...nameProps} placeholder="请输入样式名称" />
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="效果图片"
                    >
                        <ImgUpload onChange={this.onFileChange.bind(this)} />
                    </FormItem>
                </Form>
            </Dialog>
        );
    }
}

Create = Form.create()(Create);

export default Create;