import { inject, observer } from 'mobx-react';

import "./index.less";

import { WeaErrorPage, WeaTools } from 'ecCom';
import { Button, Icon } from "antd";
import { Header, Import } from "../common";

import Content from "./Content";
import Create from "./Create";
import References from "./References";
import Edit from "./Edit";

import { modules, API_ADMIN_URL } from "../../../constants";

const { SKIN } = modules;

@inject('appSkinStore')
@observer
class Skins extends React.Component {  
    onIncreased(isCreate) { 
        const { appSkinStore } = this.props;
        const { getSkinList, skins, currSkin, currPage,  pageSize, onCurrpageChange } = appSkinStore;
        const isCurrpageChange = currPage * pageSize == skins.length;
        
        getSkinList().then(() => {
            let sum = appSkinStore.skins.length;

            // 导入或者新建皮肤后, 新增的皮肤在下一页时, 自动跳到下一页
            if(isCurrpageChange && sum - 1 == skins.length) {
                onCurrpageChange(currPage + 1)
            } else if(isCreate) { // 如果是创建 直接跳到最后一页
                onCurrpageChange( Math.floor(sum / pageSize) + (sum % pageSize > 0 ? 1 : 0) );
            }
        });
    }
    
    render() {
        const { appSkinStore } = this.props;
        const { isImport, isCreate, isRef, isEdit, currSkin, appref, toggleImport, toggleCreate, toggleEdit, toggleRef, onSearchTextChange, onSave } = appSkinStore;
        const btn = (
            <div style={{display: "inline-block"}}>
                <Button type="primary" icon="plus" onClick={()=>toggleCreate(true)}>新建</Button>
                <Button type="primary" icon="upload" onClick={()=>toggleImport(true)}>导入</Button>
            </div>
        );

        return (
            <div className="mobilemode-wrapper mobilemode-skin">
                <Header 
                    icon={<i className="icon-mobilemode icon-mobilemode-skin"></i>}
                    title="皮肤管理"
                    placeholder="请输入皮肤名称"
                    btns={btn}
                    onSearchChange={onSearchTextChange}>
                </Header>
                <Content></Content>
                { isImport ?
                    <Import
                        type={SKIN}
                        visible={isImport}
                        hideImport={()=>toggleImport(false)}
                        onClosed={this.onIncreased.bind(this)}>
                    </Import>
                    : null
                }
                { isCreate ?
                    <Create
                        visible={isCreate}
                        onSave={onSave}
                        onCancel={()=>toggleCreate(false)}
                        onClosed={this.onIncreased.bind(this, true)}>
                    </Create>
                    : null
                }
                { isRef ? 
                    <References
                        visible={isRef}
                        skin={currSkin}
                        onCancel={()=>toggleRef(false)}>
                    </References>
                    : null
                }
                { isEdit ?
                    <Edit
                        visible={isEdit}
                        skin={currSkin}
                        onCancel={()=>toggleEdit(false)}>
                    </Edit>
                    : null
                }
            </div>
        );
    }
}

class ErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
Skins = WeaTools.tryCatch(React, ErrorHandler, { error: "" })(Skins);

export default Skins;