import { Dialog } from "../common";

export default class Edit extends React.Component {
    render() {
        const { visible, onCancel, skin } = this.props;
        return (
            <Dialog 
                width={1250}
                className="mobilemode-skin-edit"
                visible={visible}
                title={`样式编辑 - ${skin.name}`}
                footer={null}
                onCancel={ onCancel }>
                <iframe style={{width: "100%", height: "800px", outline: "none", border: "none"}} 
                    src={`/mobilemode/admin/skinDesigner.jsp?skinid=${skin.id}`}>
                </iframe>
            </Dialog>
        );
    }
}