import { inject } from 'mobx-react';
import { Card, Row, Col, Menu, Dropdown, Icon } from "antd";

import { WeaScroll } from "ecCom";

import { API_ADMIN_URL, modules } from "../../../constants";

const MenuItem = Menu.Item;

@inject('appSkinStore')
export default class List extends React.Component {
    constructor() {
        super();
        this.onActions = this.onActions.bind(this);
    }

    onActions(key, skin) {
        const { appSkinStore } = this.props;  
        const { id } = skin;

        switch(key) {
            case "0":
                appSkinStore.toggleEdit(skin);
                break;
            case "1":
                appSkinStore.onDelete(id);
                break;
            case "2":
                window.open(`${API_ADMIN_URL}${modules.SKIN}/export?id=${id}`, "_self");
                break;
            case "3":
                appSkinStore.toggleRef(skin);
                break;
        }
    }

    render() {
        const { onActions, props } = this;
        const { list } = props;
        
        return (
            <div className="skin-list-wrapper">
                <WeaScroll typeClass="scrollbar-macosx">
                {
                    list.map(skin => {
                        const actions = (
                            <Menu className="mobilemode-actions" onClick={ o => onActions(o.key, skin)}>
                                <MenuItem key="0"><Icon type="edit" />编辑</MenuItem>
                                <MenuItem key="1"><Icon type="delete" />删除</MenuItem>
                                <MenuItem key="2"><Icon type="export" />导出</MenuItem>
                                <MenuItem key="3"><Icon type="book" />引用</MenuItem>
                            </Menu>
                        );

                        return (
                            <Card className="mobilemode-card" bordered={false} bodyStyle={{ padding: 0 }}>
                                <div>
                                    <Row gutter={24} style={{lineHeight: "26px"}}>
                                        <Col span={16}>{skin.name}</Col>
                                        <Col span={8} style={{textAlign: "right"}}>
                                            <Dropdown overlay={actions}>
                                                <Icon type="down" className="card-arrow-down" />
                                            </Dropdown>
                                        </Col>
                                    </Row>
                                </div>
                                <div className="card-img-wrapper">
                                    <div className={ skin.previewImg ? "preview-pic" : "preview-pic default"}>
                                        <img src={ skin.previewImg || "/mobilemode/images/e9/default-pic.png" } />
                                    </div>
                                </div>
                            </Card>
                        )
                    })
                }
                </WeaScroll>
            </div>
        );
    }
}