import { inject, observer } from 'mobx-react';

import { Tabs, Badge, Pagination } from 'antd';
import List from "./List";

const TabPane = Tabs.TabPane;


@inject('appSkinStore')
@observer
export default class Content extends React.Component {
    componentWillMount() {
        const { appSkinStore } = this.props;

        appSkinStore.getSkinList();
    }

    render() {
        const { appSkinStore } = this.props;
        const { skins, currPage, searchText, pageSize, onShowSizeChange, onCurrpageChange } = appSkinStore;
        const [ start, end ] = [(currPage - 1) * pageSize, currPage * pageSize - 1];
        const filteredSkins = skins.filter(skin => {
            if(~skin.name.toLowerCase().indexOf(searchText.toLowerCase())) return skin;
        });
        let displayedSkins = filteredSkins.filter((skin, index) => {
            if(index >= start && index <= end) return skin;
        });
        
        if(filteredSkins.length && !displayedSkins.length) { // 当做删除当前页的唯一项后, 自动跳转到上一页
            onCurrpageChange(currPage - 1);
        }

        return (
            <div className="mobilemode-content">
                <List
                    list={displayedSkins}></List>
                <Pagination showSizeChanger 
                    current={currPage}
                    onShowSizeChange={(index,pageSize) => onShowSizeChange(pageSize) } 
                    onChange={onCurrpageChange}
                    defaultCurrent={1} 
                    total={filteredSkins.length} 
                />
            </div>
        );
    }
}