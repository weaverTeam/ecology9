import Applications from "./app";
import Template from "./template";
import Skins from "./skin";
import Functions from "./function"

import "./index.less";

module.exports = {
    Applications,
    Template,
    Skins,
    Functions
}