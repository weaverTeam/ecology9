import { Modal, Icon } from "antd";

import "./css/Dialog.less"

export default class Dialog extends React.Component {
    render() {
        const { title, icon, maskClosable } = this.props;
    
        const iconEl = icon !== null ? <div className="icon-wrapper">{icon}</div> : "";
        const titleEl = (
            <div className="modal-title">
                { iconEl }
                <div>{title}</div>
            </div>
        )

        return (
            <Modal 
                {...this.props}
                className={`mobilemode-modal ${this.props.className || ""}`} 
                maskClosable={maskClosable || false}
                title={titleEl}
            ></Modal>
        );
    }
}