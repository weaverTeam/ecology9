import { observer } from 'mobx-react';
import { observable } from 'mobx';

import { Icon, message } from "antd";
import { WeaUpload } from 'ecCom';
import Dialog from "../common/Dialog";

import { modules, API_ADMIN_URL } from "../../../constants";

const { TEMPLATE, SKIN } = modules;

@observer export default class Import extends React.Component {
    @observable hasSelected = false;
    @observable iserror = false; // 是否
    @observable isImporting = false; // 是否正在导入中
    @observable isClosing = false; // 导入中是否关闭窗口
    @observable isImported = false; // 是否导入完成
    @observable type=TEMPLATE; // 导入模板，应用，皮肤
    @observable config={}; // 各个类型的相关配置

    componentWillMount() {
        const { type } = this.props;

        this.type = type || this.type;
        this.config = {
            actionUrl: `${API_ADMIN_URL}${type}/import`,
        }
        switch(type) {
            case TEMPLATE: 
                this.config = {
                    title: "模板导入",
                    info: "请选择模板开始导入",
                    tip: "仅支持单个模板导入,文件限zip格式",
                    leavMsg: "离开可能造成模板导入失败，确认继续吗?"
                };
                break;
            case SKIN:
                this.config = {
                    title: "样式导入",
                    info: "请选择皮肤开始导入",
                    tip: "仅支持单个皮肤导入,文件限zip格式",
                    leavMsg: "离开可能造成皮肤导入失败，确认继续吗?"
                }
        }
        this.config.actionUrl = `${API_ADMIN_URL}${type}/import`;
    }

    uploadApp() {
        const { hasSelected, props, config } = this;
        const { hideImport } = props;

        if(!hasSelected) return message.info(config.info);

        this.iserror = false;
        this.isImporting = true;
        startUploadAll();
    }

    fileChange(state) {
        const { hasSelected, iserror, props, isClosing } = this;
        const { hideImport, onClosed } = props;
        const $button = $(this.uploadContainer).find("button");

        switch (state) {
            case "false":
                this.hasSelected = !hasSelected;
                $button.prop("disabled", !hasSelected);
                break;
            case "error":
                this.hasSelected = false;
                this.iserror = true;
                this.isImporting = false;
                $button.prop("disabled", false);
                break;
            case "uploaded":
                if(iserror) return;
                
                message.success("导入成功");
                this.isImported = true;
                this.isImporting = false;
                
                if(isClosing) return;

                hideImport();
                onClosed();
        }
    }

    hideImport() {
        const that = this;
        const { hasSelected, props, isImporting, isImported, config } = this;
        const { hideImport, onClosed } = props;
        
        if(!isImporting) return hideImport();
        this.isClosing = true;
        Modal.confirm({
            title: '确认',
            content: config.leavMsg,
            onOk() {
                hideImport();
                onClosed();
            },
            onCancel() {
                that.isClosing = false;
                
                if(!that.isImported) return;

                hideImport();
                onClosed();
            },
        });
    }

    render() {
        const { hasSelected, isImporting, props, config } = this;
        const { visible, hideImport } = props;
        
        return (
            <Dialog
                title={config.title}
                visible={visible}
                okText="开始导入"
                wrapClassName="app-import"
                confirmLoading={isImporting}
                onOk={this.uploadApp.bind(this)}
                maskClosable={false}
                onCancel={this.hideImport.bind(this)}>
                <div style={{ display: "inline-block" }}>
                    <Icon style={{ color: "#2db7f5", fontSize: "15px", verticalAlign: "text-bottom", padding: "0 5px" }} type="info-circle" />
                    <span style={{ color: "#666" }}>{config.tip}</span>
                </div>
                <div ref={el => { this.uploadContainer = el }}>
                    <WeaUpload
                        uploadId="appupload"
                        autoUpload={false}
                        uploadUrl={config.actionUrl}
                        category="0"
                        datas={this.datas}
                        showClearAll={false}
                        onUploading={this.fileChange.bind(this)}
                        onRemove={this.fileChange.bind(this, "false")}>
                    </WeaUpload>
                </div>
                 { this.isImporting ?  
                    <div style={{paddingLeft: "10px" }}>
                        <span style={{ color: "#666" }}>正在导入，请耐心等候...</span>
                    </div> : null
                 } 
            </Dialog>
        );
    }
}