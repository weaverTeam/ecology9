import { WeaSelect, WeaInputSearch } from "ecCom";
import { Icon, Row, Col } from "antd";

import "./css/Header.less";

export default class Header extends React.Component {
    render() {
        const { btns, onSearchChange, title, placeholder, icon } = this.props;

        return (
            <div className="mobilemode-header">
                <Row gutter={24}>
                    <Col span={18} style={{color: "#666"}}>
                        { !icon ?
                            <Icon type="exception" />
                            : icon
                        }
                        <span style={{fontSize: "14px"}}>{title}</span>
                    </Col>
                    <Col span={6} style={{textAlign: "right"}}>
                        <WeaInputSearch
                            placeholder={placeholder}
                            onSearchChange={onSearchChange} />
                        { btns }
                    </Col>
                </Row>
            </div>
        );
    }
}