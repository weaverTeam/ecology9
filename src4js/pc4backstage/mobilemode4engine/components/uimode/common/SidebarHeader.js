import { Row, Col, Dropdown, Icon } from "antd";

import "./css/SidebarHeader.less";

export default class SidebarHeader extends React.Component {
    render() {
        const { title, action, filter, active } = this.props;
        
        return (
            <div className="mobilemode-sidebar-header">
                <Row gutter={24}>
                    <Col span={18}>
                        <h4>{title}</h4>
                    </Col>
                    <Col span={6}>
                        <div style={{ textAlign: "right", color: "#000" }}>
                            { action ?
                                <Dropdown overlay={action} trigger={["click"]}>
                                    <Icon type="plus" style={{paddingRight: "12px"}}/>
                                </Dropdown>
                                : null
                            }
                            { filter ?
                                <Dropdown overlay={filter} trigger={["click"]}>
                                    <Icon type="filter" className="sidebar-filter-icon" style={active? {color: "#108ee9"} : null} />
                                </Dropdown>
                                : null
                            }
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}