import { Menu, Dropdown, Icon, Pagination, Row, Col } from "antd";
import { WeaInputSearch, WeaScroll } from "ecCom";
import SidebarHeader from "../common/SidebarHeader";

import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';

import { getHistory, setHistory } from "../../../utils";
import { APP_DESIGNER_URL } from "../../../constants/app";

@inject('appStore')
@observer
export default class LeftMenu extends React.Component {
    onSelectApp(appid) {
        const { appStore } = this.props;
        const engineHistory = getHistory();

        appStore.onCurrentAppChange(appid);
        engineHistory.appid = appid;
        engineHistory.currPage = appStore.current;
        setHistory(engineHistory);
    }

    render() {
        const { appStore } = this.props;
        const { apps, searchText, statusType, classify, current, onCurrPageChange, appid, pageSize } = appStore;
        const filters = apps.filter((app, index) => {
            return (!searchText || ~app.appname.toLowerCase().indexOf(searchText) || ~app.pinyin.toLowerCase().indexOf(searchText)) &&
                (statusType == -1 || statusType == app.ispublish) &&
                (classify == -1 || classify == app.industry);
        });
        const appItems = filters.filter((app, index) => {
            return index >= pageSize * (current - 1) && index <= pageSize * current - 1;
        });
        
        return (
            <div className="mobilemode-aside">
                <AppSideHeader></AppSideHeader>
                <div style={{padding: "0 20px"}}>
                    <AppList
                        onSelect={this.onSelectApp.bind(this)}
                        selected={appid}
                        appItems={appItems}
                    ></AppList>
                    <div className="mobilemode-pagination">
                        { appItems.length ?
                            <Pagination size="small" onChange={onCurrPageChange} current={current} pageSize={pageSize} defaultCurrent={1} total={filters.length} />
                            : null
                        }
                    </div>
                </div>
            </div>
        )
    }
}

const MenuItem = Menu.Item;
const MenuDivider = Menu.Divider;

@inject('appStore')
@observer
class AppSideHeader extends React.Component {
    onMenuClick(e) {
        const { toggleAppCreate, toggleImport } = this.props.appStore;

        if(e.key == 0) {
            toggleAppCreate(true);
        } else {
            toggleImport(true);
        }
    }

    onFilterMenuClick(e) {
        const key = e.key;
        const { statusChange, classifyChange, classifies } = this.props.appStore;
        
        if(key <= 2) {
            statusChange(key-1);
        } else if( key == 3) {
            classifyChange(-1)
        } else if( key > 3) {
            classifyChange(classifies[key - 4]);
        }
    }

    render() {
        const { onSearchTextChange, classifies, statusType, classify } = this.props.appStore;
        const isActive = statusType != -1 || classify != -1;
        const checked = <Icon type="check" />;
        const menu = (
            <Menu onClick={this.onMenuClick.bind(this)} className="mobilemode-menu">
                <MenuItem key="0"><Icon type="plus"/>创建应用</MenuItem>
                <MenuItem key="1"><Icon type="upload" />导入应用</MenuItem>
            </Menu>
        );
        const filterMenu = (
            <Menu onClick={this.onFilterMenuClick.bind(this)} className="mobilemode-menu"> 
                <MenuItem key="0">{ statusType == -1 ? checked : null }全部</MenuItem>
                <MenuItem key="2">{ statusType == 1 ? checked : null }已发布</MenuItem>
                <MenuItem key="1">{ statusType == 0 ? checked : null }未发布</MenuItem>
                <MenuDivider />
                <MenuItem key="3">{ classify == -1 ? checked : null}所有类型</MenuItem>
                {
                    classifies.map((clf,index) => {
                        return <MenuItem key={index+4}>{clf==classify ? checked : null}{clf}</MenuItem>
                    })
                }
            </Menu>
        );

        return (
            <div className="aside-header">
                <SidebarHeader
                    title="移动应用"
                    action={menu}
                    filter={filterMenu}
                    active={isActive}>
                </SidebarHeader>
                <div style={{padding: "12px 20px"}}>
                    <WeaInputSearch placeholder="搜索应用" onSearchChange={onSearchTextChange}/>
                </div>
            </div>
        )
    }    
}

class AppList extends React.Component {   
    navigateToDesigner(appid) {
        window.open(APP_DESIGNER_URL+appid);
    } 

    render() {
        const { navigateToDesigner, props } = this;
        const { appItems, selected, onSelect } = props;
        const items = appItems.map((app, index) => {
            const appid = app.id;
            const className = selected == appid ? "mobilemode-list-item selected" : "mobilemode-list-item";

            return (
                <div className={className} onClick={()=>onSelect(appid)}>
                    <img src={ app.picpath || "/mobilemode/images/e9/mobile-app.png"} />
                    <div className="item-detail">
                        <h4>{app.appname}</h4>
                        <p>{app.descriptions || "无描述信息"}</p>
                    </div>
                    <div className="actions">
                        <Icon type="edit" className="icon-primary icon-designer" 
                            style={{fontSize: "22px", paddingRight: "8px"}} 
                            onClick={()=>navigateToDesigner(appid)} />
                        <Icon type="cloud-upload" 
                            style={app.ispublish==1?{visibility: "visible"}:{visibility: "hidden"}}/>
                    </div>
                </div>
            );
        });
        return (
            <WeaScroll typeClass="scrollbar-macosx" className="mobilemode-list-scroller">
                <div className="mobile-apps">
                    { items }
                </div>
            </WeaScroll >
        )
    }
}