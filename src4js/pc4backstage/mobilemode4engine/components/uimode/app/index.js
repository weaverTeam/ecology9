import "./index.less"
import { inject, observer } from 'mobx-react';

import { WeaTools, WeaErrorPage  } from 'ecCom';

import LeftMenu from './LeftMenu';
import MainContent from './MainContent';

import { getHistory, setHistory } from "../../../utils";

import AppCreate from "./AppCreate";
import AppImport from "./AppImport";
import AppQRCode from "./AppQRCode";
import AppSkin from "./AppSkin";

@inject('appStore')
@observer
class Applications extends React.Component {
    componentWillMount() {
        const { appStore } = this.props;
        const engineHistory = getHistory();

        // 初始化应用列表数据
        appStore.getAppList()
            .then(function(apps){
                const len = apps.length;

                if(!len) return;

                // 默认选中缓存中的应用或第一个应用
                let appid = engineHistory.appid || apps[0].id;
                appStore.onCurrentAppChange(appid);
                apps.every((app, index) => {
                    if(app.id != appid && index != len - 1 ) {
                        if(index == len - 1) {
                            appid = apps[0].id
                        }
                        return true;
                    } 
                    appStore.onCurrPageChange(Math.ceil((index+1)/appStore.pageSize));
                    return false;
                });

                engineHistory.appid = appid;
                setHistory(engineHistory);
            });
        // 获取ipaddr
        appStore.getIPAddr();
    }

    onClosedAppCreate(appid) {
        const { appStore } = this.props;
        const { getAppList, onCurrentAppChange } = appStore;

        getAppList()
            .then(apps => {
                if(!apps.length) return;

                appid = appid || apps[0].id; // 上传应用无法获取返回id 默认取第一个

                onCurrentAppChange(appid);
            });
    }

    getHostAddr(ipaddr) {
        const { port, protocol, host } = location;
        const hostname = location.hostname.toLowerCase(); 
        let hostaddr = protocol + "//" + host;

        if(hostname == '127.0.0.1' || hostname == 'localhost') {
            hostaddr = ipaddr ? (protocol + "//" + ipaddr + (port ? ":" + port : "") ) : hostaddr;
        }
        return hostaddr;
    }

    render() {
        const { appStore } = this.props;
        const { appid, ipaddr, isCreating, isImport, isQRCodeVisible, isSetSkin, toggleAppCreate, toggleImport, toggleQRCode } = appStore;
        const qrcodeText = this.getHostAddr(ipaddr) + '/mobilemode/appHomepageViewWrap.jsp?appid=' + appid + "&noLogin=1";

        return (
            <div className='mobile-app-wapper'>
                <LeftMenu></LeftMenu>
                <MainContent></MainContent>
                { isCreating?
                    <AppCreate
                        visible={isCreating}
                        closeAppCreate={()=>toggleAppCreate(false)}
                        onClosed={ this.onClosedAppCreate.bind(this) }>
                    </AppCreate> : null
                }
                { isImport ?
                    <AppImport
                        visible={isImport}
                        hideAppImport={()=>toggleImport(false)}
                        onClosed={ this.onClosedAppCreate.bind(this) }>
                    </AppImport> : null
                }
                { isQRCodeVisible ?
                    <AppQRCode
                        visible={isQRCodeVisible}
                        qrcodeText={qrcodeText}
                        hideQRCode={()=>toggleQRCode(false)}>
                    </AppQRCode> : null
                }
                { isSetSkin?
                    <AppSkin
                        appid={appid}
                        visible={isSetSkin}
                        closeAppSkin={appStore.hideAppSkin}
                        setAppSkin={appStore.setAppSkin}>
                    </AppSkin> : null
                }
            </div>
        )
    }
}

class ErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
Applications = WeaTools.tryCatch(React, ErrorHandler, { error: "" })(Applications);

export default Applications;