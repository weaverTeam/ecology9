import { observer } from 'mobx-react';
import { observable } from 'mobx';

import { Col, Card } from "antd";

import { skinlist } from "../../../apis/skin"

@observer export default class AppSkinList extends React.Component {
    @observable skinlist = [];
    @observable selectedId = -1;

    componentWillMount() {
        const that = this;
        
        skinlist({
            appid: that.props.appid
        }).then((res) => {
            let data = res.data;
            let selectedId = -1;

            data.every(skin => {
                if(skin.inuse != 1) return true;

                selectedId = skin.id;
                return false;
            });
            
            that.skinlist = data;
            that.selectedId = selectedId;
        })
    }

    selectSkin(skin) {
        const { selectedId, props } = this;
        const { onSelect } = this.props;

        this.selectedId = selectedId == skin.id ? -1 : skin.id;
        onSelect(skin);
    }

    render() {
        const { skinlist, selectedId, props} = this;
        const { height } = props;
        const using = (
            <a href="javascript:;" style={{ position: "relative", top: "-15px", right: "-24px" }}>
                <i className="icon-mobilemode" style={{ fontSize: "45px" }}>&#xe610;</i>
            </a>
        );

        return (
            <div>
                {
                    skinlist.map((skin, index) => {
                        return (
                            <Col offset={index % 3 == 0 ? 0 : 1} span={7}>
                                <Card
                                    onClick={this.selectSkin.bind(this, skin)}
                                    bodyStyle={{ padding: 0 }}
                                    style={{ marginTop: "16px" }}
                                    extra={selectedId == skin.id ? using : null}>
                                    <img 
                                        style={{width:"100%", height: height||"250px"}} 
                                        src={skin.prevImg || "/mobilemode/images/noImg.jpg"} />
                                    <p style={{padding: "5px 10px",color:"#666"}}>{skin.name}</p>
                                </Card>
                            </Col>
                        )
                    })
                }
            </div>
        );
    }
}