import { observer } from 'mobx-react';
import { observable } from 'mobx';

import { Modal, Icon, message } from "antd";
import { WeaUpload } from 'ecCom';
import plupload from "plupload";

window.plupload = plupload;

@observer export default class AppImport extends React.Component {
    @observable hasSelected = false;
    @observable iserror = false; // 是否
    @observable isImporting = false; // 是否正在导入中
    @observable isClosing = false; // 导入中是否关闭窗口
    @observable isImported = false; // 是否导入完成

    uploadApp() {
        const { hasSelected, props } = this;
        const { hideAppImport } = props;

        if(!hasSelected) return message.info("请选择应用开始导入");

        this.iserror = false;
        this.isImporting = true;
        startUploadAll();
    }

    fileChange(state) {
        const { hasSelected, iserror, props, isClosing } = this;
        const { hideAppImport, onClosed } = props;
        const $button = $(this.uploadContainer).find("button");

        switch (state) {
            case "false":
                this.hasSelected = !hasSelected;
                $button.prop("disabled", !hasSelected);
                break;
            case "error":
                this.hasSelected = false;
                this.iserror = true;
                this.isImporting = false;
                $button.prop("disabled", false);
                break;
            case "uploaded":
                if(iserror) return;
                
                message.success("导入成功");
                this.isImported = true;
                this.isImporting = false;
                
                if(isClosing) return;

                hideAppImport();
                onClosed();
        }
    }

    hideAppImport() {
        const that = this;
        const { hasSelected, props, isImporting, isImported } = this;
        const { hideAppImport, onClosed } = props;
        
        if(!isImporting) return hideAppImport();
        this.isClosing = true;
        Modal.confirm({
            title: '确认',
            content: '离开可能造成应用导入失败，确认继续吗？',
            onOk() {
                hideAppImport();
                onClosed();
            },
            onCancel() {
                that.isClosing = false;
                
                if(!that.isImported) return;

                hideAppImport();
                onClosed();
            },
        });
    }

    render() {
        const { hasSelected, isImporting, props } = this;
        const { visible, hideAppImport } = props;
        
        return (
            <Modal
                title="应用导入"
                visible={visible}
                okText="开始导入"
                wrapClassName="app-import"
                confirmLoading={isImporting}
                onOk={this.uploadApp.bind(this)}
                maskClosable={false}
                onCancel={this.hideAppImport.bind(this)}>
                <div style={{ display: "inline-block" }}>
                    <Icon style={{ color: "#2db7f5", fontSize: "15px", verticalAlign: "text-bottom", padding: "0 5px" }} type="info-circle" />
                    <span style={{ color: "#666" }}>仅支持单个应用导入,文件限zip格式</span>
                </div>
                <div ref={el => { this.uploadContainer = el }}>
                    <WeaUpload
                        uploadId="appupload"
                        autoUpload={false}
                        uploadUrl="/api/mobilemode/admin/app/import"
                        category="0"
                        datas={this.datas}
                        showClearAll={false}
                        onUploading={this.fileChange.bind(this)}
                        onRemove={this.fileChange.bind(this, "false")}>
                    </WeaUpload>
                </div>
                 { this.isImporting ?  
                    <div style={{paddingLeft: "10px" }}>
                        <span style={{ color: "#666" }}>上传完成后，还需解析应用导入到数据库中，请耐心等候...</span>
                    </div> : null
                 } 
            </Modal>
        );
    }
}