import { WeaQrcode} from 'ecCom';
import { Modal } from 'antd'

export default class AppQRCode extends React.Component {
    render() {
        const { visible, qrcodeText, hideQRCode } = this.props;
        console.log(qrcodeText)
        return (
            <Modal
                visible={visible}
                title=""
                footer={null}
                width={200}
                onCancel={hideQRCode}
                wrapClassName="mobile-app-qrcode"
            >
                <WeaQrcode size="168" level={"L"} text={qrcodeText} />
            </Modal>
        )
    }
}