import { Row, Col, Button, Icon, Timeline, Form, Switch, message } from "antd";
import { WeaEchart, WeaTop, WeaInput, WeaUpload } from 'ecCom';
import { inject, observer } from 'mobx-react';

import { getHistory, setHistory } from "../../../utils";
import { APP_DESIGNER_URL } from "../../../constants/app";

const TimelineItem = Timeline.Item;

@inject('appStore')
@observer
export default class MainContent extends React.Component {
    render() {
        const { appStore } = this.props;
        let option = {
            title: {
                text: ""
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
            legend: {
                data:['Activations','Net Cancellations']
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [{
                type : 'category',
                boundaryGap : false,
                splitArea: true,
                data : ['周一','周二','周三','周四','周五','周六','周日']
            }],
            yAxis : [{
                type : 'value',
                splitLine: {
                    show: false
                },
                max: 10,
                boundaryGap: [0, '100%']
            }],
            series : [
                {
                    name:'Activations',
                    type:'line',
                    itemStyle: {
                        normal: {
                            color: "#45b4c6"
                        }
                    },
                    data:[6, 3, 4, 8, 6, 8, 7]
                },
                {
                    name:'Net Cancellations',
                    type:'line',
                    areaStyle: {
                        normal: {
                            color: "#f8f7ee"
                        }
                    },
                    itemStyle: {
                        normal: {
                            color: "#fdb17f"
                        }
                    },
                    line: {
                        smoothMonotone: 'x',
                        clipOverflow: false
                    },
                    data:[5, 1, 2, 3, 5, 4, 5]
                }
            ]
        };
        return (
            <div className="mobile-app-content">
                { appStore.appid ?
                    <div>
                        <AppTitle></AppTitle>
                        <AppContent
                            app={appStore.currentApp}
                            previewImgs={appStore.previewImgs}
                            option={option}></AppContent>
                    </div>
                    : null
                }
            </div>
        )
    }
}

@inject('appStore')
@observer
class AppTitle extends React.Component {
    navigateToDesignerPage() {
        window.open(APP_DESIGNER_URL+this.props.appStore.appid);
    }

    onDropMenuClick(key) {
        const { appStore } = this.props;
        const { appid, exportApp } = appStore;
        let engineHistory = getHistory();

        switch(key) {
            case "1":
                exportApp(appid);
                break;
            case "2":
            case "3":
                appStore.removeApp(appid, key == "3");
                engineHistory.appid = null;
                setHistory(engineHistory);
                break;
            case "4" :
                appStore.toggleQRCode(true);
                break;
        }
    }

    render() {
        
        const dropMenuDatas = [
            {
                key: 1,
                disabled: false,
                icon: <i className='icon-mobilemode icon-mob-small icon-mobilemode-export' />,
                content: '导出'
            },
            {
                key: 2,
                disabled: false,
                icon: <i className='icon-mobilemode icon-mob-small icon-mobilemode-hidden' />,
                content: '废弃'
            },
            {
                key: 3,
                disabled: false,
                icon: <i className='icon-mobilemode icon-mob-small icon-mobilemode-delete' />,
                content: '删除'
            },
            {
                key: 4,
                disabled: false,
                icon: <i className='anticon anticon-qrcode' style={{margin: 0}} />,
                content: '二维码'
            }
        ];
        const btns = [
            <Button type="primary" icon="edit" onClick={this.navigateToDesignerPage.bind(this)}>设计</Button>
        ];

        return (
            <WeaTop
                title="应用主数据"
                icon={null}
                buttons={[btns]}
                showDropIcon={true}
                onDropMenuClick={this.onDropMenuClick.bind(this)}
                dropMenuDatas={dropMenuDatas}
            ></WeaTop>
        )
    }
}

class AppContent extends React.Component {
    render() {
        const { app, previewImgs, option } = this.props;

        return (
            <div style={{padding:"7px 20px 15px;"}}>
                <Row gutter={24}>
                    <Col span={12} className="mobile-app-panel">
                        <h4 className="panel-title">基础信息</h4>
                        <div className="panel-content" style={{padding: "14px 0 7px"}}>
                            <AppBaseInfo></AppBaseInfo>
                        </div>
                    </Col>
                    <Col span={12} className="mobile-app-panel">
                        <h4 className="panel-title">页面预览</h4>
                        <div className="panel-content page-preview-wrapper">
                            <Row gutter={24}>
                                { previewImgs.length ?
                                    previewImgs.map(imgSrc => {
                                        return (
                                            <Col span={8} className="app-thumbnail">
                                                <img src="/mobilemode/images/e9/header.png" />
                                                <p>{app.appname}</p>
                                                <img src={imgSrc}/>
                                            </Col>
                                        )
                                    })
                                    : <p style={{color: "#ccc"}}>无预览图</p>
                                }
                            </Row>
                        </div>
                    </Col>
                </Row>
                <Row gutter={24}>
                    <Col span={12} className="mobile-app-panel">
                        <h4 className="panel-title">日志</h4>
                        <div className="panel-content">
                            <Timeline>
                                <TimelineItem color="green">创建服务现场 2015-09-01</TimelineItem>
                                <TimelineItem color="green">创建服务现场 2015-09-01</TimelineItem>
                                <TimelineItem color="red">
                                <p>初步排除网络异常1</p>
                                <p>初步排除网络异常2</p>
                                <p>初步排除网络异常3 2015-09-01</p>
                                </TimelineItem>
                                <TimelineItem>
                                <p>技术测试异常1</p>
                                <p>技术测试异常2</p>
                                <p>技术测试异常3 2015-09-01</p>
                                </TimelineItem>
                            </Timeline>
                        </div>
                    </Col>
                    <Col span={12} className="mobile-app-panel">
                        <h4 className="panel-title">应用统计</h4>
                        <div className="panel-content">
                            <div style={{height: 300}}>
                                <WeaEchart ref="chart" option={option} useDefault={false} />
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}

const FormItem = Form.Item;

// 应用信息
@inject('appStore')
@observer
class AppBaseInfo extends React.Component {
    constructor() {
        super();
        this.modifyApp = this.modifyApp.bind(this);
        this.onEnterForMoidfyApp = this.onEnterForMoidfyApp.bind(this);
    }

    publishApp(ispublish) {
        const { appStore } = this.props;
        const { currentApp } = appStore;

        appStore.publishApp(currentApp.id, +ispublish);
    }

    showAppSkin() {
        const { appStore } = this.props;
        
        appStore.showAppSkin(appStore.appid);
    }

    onUploading(state) {
        const { appStore } = this.props;

        switch (state) {
            case "uploaded":
                message.success("上传成功");
                appStore.getAppList({
                    appid: appStore.appid
                });
        }
    }

    modifyApp(key, value) {
        const { appStore, form } = this.props;
        const { currentApp, updateApp } = appStore;

        if(currentApp[key] == value) return;
        
        form.validateFields([key], errors => {
            if(errors) return;
            
            let appinfo = {
                appid: currentApp.id
            };
            appinfo[key] = value;
            updateApp(appinfo);
        });
    }

    onEnterForMoidfyApp(e, key) {
        const evt = e.nativeEvent;

        if(evt.keyCode != 13) return;
        this.modifyApp(key, evt.srcElement.value);
    }

    render() {
        const { props, modifyApp, onEnterForMoidfyApp } = this;
        const { form, appStore } = props;
        const { currentApp } = appStore;
        const { getFieldProps } = form;
        const formItemLayout = {
          labelCol: { span: 4 },
          wrapperCol: { span: 20 },
        };
        const nameProps = getFieldProps("appname", {
            rules: [{
                required: true,
                message: "请输入应用名称"
            }],
            initialValue: currentApp.appname
        });
        
        return (
            <Form form={form}>
                <FormItem
                    {...formItemLayout}
                    label="名称"
                >
                    <WeaInput {...nameProps} placeholder="请输入应用名称" 
                        onKeyDown={e=>onEnterForMoidfyApp(e, "appname")}
                        onBlur={value=>modifyApp("appname", value)}/>
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="类型"
                >
                    <WeaInput {...getFieldProps("industry", { initialValue: currentApp.industry }) }
                        onKeyDown={e=>onEnterForMoidfyApp(e, "industry")}
                        onBlur={value=>modifyApp("industry", value)} />
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="发布状态"
                >
                    <Switch checked={currentApp.ispublish == 1} onChange={this.publishApp.bind(this)} />
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="图标"
                    className="app-icon-item"
                >
                    <WeaUpload 
                        uploadId="picpath" 
                        uploadUrl={`/api/mobilemode/admin/app/modify?appid=${currentApp.id}`} 
                        category="1" 
                        onUploading={this.onUploading.bind(this)}
                        autoUpload={true}>
                        <img className="app-icon" src={ currentApp.picpath || "/mobilemode/images/e9/mobile-app.png"} />
                    </WeaUpload>
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="显示顺序"
                >
                    <WeaInput {...getFieldProps("showorder", { rules: [{
                            type: "string",
                            pattern: /^([0-9]*||-[0-9]+)$/,
                            message: "请输入数字"
                        }], initialValue: String(currentApp.showorder) }) } 
                        onKeyDown={e=>onEnterForMoidfyApp(e, "showorder")}
                        onBlur={value=>modifyApp("showorder", value)} />
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="样式风格"
                >
                    <p className="ant-form-text" style={{width: "100%"}} onClick={this.showAppSkin.bind(this)}>{ currentApp.skin.name} </p>
                </FormItem>
                <FormItem
                    {...formItemLayout}
                    label="描述"
                >
                    <WeaInput {...getFieldProps("descriptions", { initialValue: currentApp.descriptions }) } 
                        onKeyDown={e=>onEnterForMoidfyApp(e, "descriptions")}
                        onBlur={value=>modifyApp("descriptions", value)} />
                </FormItem>
            </Form>
        )
    }
}

AppBaseInfo = Form.create()(AppBaseInfo);