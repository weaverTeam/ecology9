import { inject } from 'mobx-react';
import { Card, Row, Col, Menu, Dropdown, Icon } from "antd";

import { WeaScroll } from "ecCom";
import { API_ADMIN_URL, modules } from "../../../constants";

const MenuItem = Menu.Item;

@inject('templateStore')
export default class List extends React.Component {
    constructor() {
        super();
        this.onActions = this.onActions.bind(this);
    }

    onActions(key, template) {
        const { templateStore } = this.props;  
        const { id } = template;

        switch(key) {
            case "0":
                templateStore.toggleEdit(template);
                break;
            case "1":
                templateStore.onDelete(id)
                break;
            case "2":
                this.onExport(id);
                break;
        }
    }

    onExport(tmpId) {
        window.open(`${API_ADMIN_URL}${modules.TEMPLATE}/export?id=${tmpId}`, "_self");
    }

    render() {
        const { onActions, props } = this;
        const { list } = props;

        return (
            <div className="template-list-wrapper">
                <WeaScroll typeClass="scrollbar-macosx">
                {
                    list.map(tmp => {
                        const actions = (
                            <Menu className="mobilemode-actions" onClick={ o => onActions(o.key, tmp)}>
                                <MenuItem key="0" disabled={tmp.issys}><Icon type="edit" />编辑</MenuItem>
                                <MenuItem key="1" disabled={tmp.issys}><Icon type="delete" />删除</MenuItem>
                                <MenuItem key="2"><Icon type="export" />导出</MenuItem>
                            </Menu>
                        );

                        return (
                            <Card className="mobilemode-card" bordered={false} bodyStyle={{ padding: 0 }}>
                                <div>
                                    <Row gutter={24} style={{lineHeight: "26px"}}>
                                        <Col span={16}>{tmp.name}</Col>
                                        <Col span={8} style={{textAlign: "right"}}>
                                            <Dropdown overlay={actions}>
                                                <Icon type="down" className="card-arrow-down" />
                                            </Dropdown>
                                        </Col>
                                    </Row>
                                </div>
                                <div className="card-img-wrapper">
                                    <img src="/mobilemode/images/e9/header.png"/>
                                    <div className={ tmp.previewImg ? "preview-pic" : "preview-pic default"}>
                                        <img src={ tmp.previewImg || "/mobilemode/images/e9/default-pic.png" } />
                                    </div>
                                </div>
                            </Card>
                        )
                    })
                }
                </WeaScroll>
            </div>
        );
    }
}