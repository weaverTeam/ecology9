import { inject, observer } from 'mobx-react';

import { Tabs, Badge, Pagination } from 'antd';
import List from "./List"

const TabPane = Tabs.TabPane;

@inject('templateStore')
@observer
export default class Content extends React.Component {
    componentWillMount() {
        this.props.templateStore.getTmpList();
    }

    render() {
        const { templateStore } = this.props;
        const { templates, currPage, sysTmpNums, customTmpNums, searchText, type, pageSize, onShowSizeChange, onCurrpageChange, onTypeChange } = templateStore;
        const tab1 = <Badge count={sysTmpNums}>系统默认</Badge>
        const tab2 = <Badge count={customTmpNums}>自定义</Badge>
        const [ start, end ] = [(currPage - 1) * pageSize, currPage * pageSize - 1];
        let filteredTmps = templates.filter(tmp => {
            if(~tmp.name.indexOf(searchText)
                && (type == 0 || (type == 1 && tmp.issys) || (type == 2 && !tmp.issys) )) return tmp;
        });
        let displayedTmps = filteredTmps.filter((tmp, index) => {
            if(index >= start && index <= end) return tmp;
        });
        
        if(filteredTmps.length && !displayedTmps.length) { // 当做删除当前页的唯一项后, 自动跳转到上一页
            onCurrpageChange(currPage - 1);
        }

        return (
            <div className="mobilemode-content">
                <Tabs defaultActiveKey="0" onChange={ key => {onTypeChange(key)}}>
                    <TabPane tab="全部" key="0"></TabPane>
                    <TabPane tab={tab1} key="1"></TabPane>
                    <TabPane tab={tab2} key="2"></TabPane>
                </Tabs>
                <List
                    list={displayedTmps}></List>
                <Pagination showSizeChanger 
                    current={currPage}
                    onShowSizeChange={(index,pageSize) => onShowSizeChange(pageSize) } 
                    onChange={onCurrpageChange}
                    defaultCurrent={1} 
                    total={filteredTmps.length} 
                />
            </div>
        );
    }
}