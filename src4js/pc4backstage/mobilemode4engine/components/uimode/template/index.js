import { inject, observer } from 'mobx-react';
import { WeaTools, WeaErrorPage  } from 'ecCom';
import { Button } from "antd";

import { Header, Import } from "../common";
import Content from "./Content";
import Edit from "./Edit";

import "./index.less";

import { modules, API_ADMIN_URL } from "../../../constants";

const { TEMPLATE } = modules;

@inject('templateStore')
@observer
class Template extends React.Component {
    render() {
        const { templateStore } = this.props;
        const { isImport, currTemplate, toggleImport, toggleEdit, getTmpList, onModify, onSearchTextChange } = templateStore;
        const btn = <Button type="primary" icon="upload" onClick={()=>toggleImport(true)}>导入</Button>
        
        return (
            <div className="mobilemode-wrapper mobilemode-template">
                <Header 
                    title="母版管理"
                    placeholder="请输入母版名称"
                    btns={btn}
                    onSearchChange={onSearchTextChange}>
                </Header>
                <Content></Content>
                { isImport ?
                    <Import
                        type={TEMPLATE}
                        visible={isImport}
                        hideImport={()=>toggleImport(false)}
                        onClosed={getTmpList}
                    >
                    </Import>
                    : null
                }
                { currTemplate ?
                    <Edit                    
                        template={currTemplate}
                        visible={!!currTemplate}
                        onEdit={onModify}
                        onCancel={() => toggleEdit(false)}
                        onClosed={getTmpList}
                    >
                    </Edit>
                    : null
                }
            </div>
        )
    }
}

class ErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
Template = WeaTools.tryCatch(React, ErrorHandler, { error: "" })(Template);

export default Template;