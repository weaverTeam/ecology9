import { inject, observer } from 'mobx-react';
import { observable } from "mobx";

import { Row, Col } from "antd";
import { WeaTop } from 'ecCom';

import Codemirror from "react-codemirror";
require('codemirror/lib/codemirror.css');
require('codemirror/mode/javascript/javascript');

@inject('appFunctionStore')
@observer
export default class MainContent extends React.Component {
    render() {
        const { currFunc, currFuncInfo } = this.props.appFunctionStore;

        return (
            <div className="mobile-app-content">
                <WeaTop title={currFunc.sign || ""} buttons={[]} />
                <FunctionContent funcInfo={currFuncInfo}/>
            </div>
        );
    }
}

class FunctionContent extends React.Component {
    componentDidUpdate() {
        const { editor } = this.refs;
        const { example } = this.props.funcInfo;

        example && editor.getCodeMirror().doc.setValue(example)
    }

    render() {
        const { funcInfo } = this.props;
        const { sign, desc, params, returnV, example } = funcInfo;

        return (
            <div style={{padding:"7px 20px 15px;"}}>
                <h4 className="content-title">基本信息</h4>
                <div className="mobilemode-func-detail">
                    <Row gutter={24}>
                        <Col span={3}>签名 :</Col>
                        <Col span={21}>{sign}</Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={3}>描述 :</Col>
                        <Col span={21}>{desc}</Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={3}>参数 :</Col>
                        <Col span={21}>
                            
                            {   params && params.length ?
                                <ul className="func-params">
                                {
                                    params.map(param => {
                                        return (
                                            <li>
                                                <Row gutter={24}>
                                                    <Col span={4}><strong style={{color: "#333"}}>{param.name}</strong></Col>
                                                    <Col span={4}><span>{param.type}</span></Col>
                                                    <Col span={4}><span>{param.required ? "必需":"可选"}</span></Col>
                                                    <Col span={12}><span>{param.explain}</span></Col>
                                                </Row>
                                            </li>
                                        )
                                    })
                                }
                                </ul>
                                : <span>无</span>
                            }
                        </Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={3}>返回 :</Col>
                        <Col span={21}>{returnV || "无"}</Col>
                    </Row>
                    <Row gutter={24}>
                        <Col span={3}>示例 :</Col>
                        <Col span={21}>
                        <Codemirror ref="editor" defaultValue={example} className="func-example"
                            options={{
                                mode: "javascript",
                                readOnly: true,
                                lineNumbers: true
                            }} 
                        />
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}