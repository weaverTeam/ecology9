import { inject, observer } from 'mobx-react';

import "./index.less";

import { WeaTools, WeaErrorPage  } from 'ecCom';

import LeftMenu from "./LeftMenu";
import MainContent from "./MainContent";

@inject('appStore')
@observer
class Functions extends React.Component {
    render() {
        return (
            <div className='mobile-app-wapper'>
                <LeftMenu></LeftMenu>
                <MainContent></MainContent>
            </div>
        )
    }
}

class ErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
Functions = WeaTools.tryCatch(React, ErrorHandler, { error: "" })(Functions);

export default Functions;