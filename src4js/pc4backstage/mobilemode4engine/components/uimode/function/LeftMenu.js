import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';

import { Menu, Dropdown, Icon, Pagination, Row, Col } from "antd";
import { WeaInputSearch, WeaScroll } from "ecCom";
import { SidebarHeader } from "../common"

@inject("appFunctionStore")
@observer
export default class LeftMenu extends React.Component {
    componentWillMount() {
        const { getFunctionList } = this.props.appFunctionStore;

        getFunctionList();
    }

    render() {
        const { appFunctionStore } = this.props;
        const { functions, searchText, currCategory, currFunc, currPage, pageSize, onCurrFuncChange, onCurrPageChange } = appFunctionStore;
        const filterFuncs = functions.filter(func => {
            const id = func.id.toLowerCase();
            const desc = func.desc.toLowerCase();

            if( (~id.indexOf(searchText) || ~desc.indexOf(searchText)) &&
                (currCategory == "all" || currCategory == func.category)) return func;
        });
        const funcItems = filterFuncs.filter( (func, index) => {
            return index >= pageSize * (currPage - 1) && index <= pageSize * currPage - 1;
        });

        return (
            <div className="mobilemode-aside">
                <FunctionAsideHeader></FunctionAsideHeader>
                <div style={{padding: "0 20px"}}>
                    <FunctionList
                        funcItems={funcItems}
                        selected={currFunc.id}
                        onSelect={onCurrFuncChange}>
                    </FunctionList>
                    <div className="mobilemode-pagination">
                        { funcItems.length ?
                            <Pagination size="small" onChange={onCurrPageChange} current={currPage} pageSize={pageSize} defaultCurrent={1} total={filterFuncs.length} />
                            : null
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const MenuItem = Menu.Item;

@inject("appFunctionStore")
@observer
class FunctionAsideHeader extends React.Component {
    render() {
        const { categorys, onSearchTextChange, currCategory, onCurrCategoryChange } = this.props.appFunctionStore;
        const checked = <Icon type="check" />;
        const filterMenu = (
            <Menu onClick={e=>onCurrCategoryChange(e.key)} className="mobilemode-menu"> 
                <MenuItem key="all">{ currCategory == "all" ? checked : null }全部</MenuItem>
                {
                    categorys.map(category => {
                        return <MenuItem key={category.name}>{ currCategory == category.name ? checked : null }{category.text}</MenuItem>
                    })
                }
            </Menu>
        );

        return (
            <div className="aside-header">
                <SidebarHeader
                    title="函数库"
                    filter={filterMenu}
                    active={currCategory != "all"}>
                </SidebarHeader>
                <div style={{padding: "12px 20px"}}>
                    <WeaInputSearch placeholder="搜索应用" onSearchChange={onSearchTextChange}/>
                </div>
            </div>
        )
    }
}

class FunctionList extends React.Component {
    render() {
        const { funcItems, selected, onSelect } = this.props;
        const items = (
            funcItems.map(item => {
                const funcId = item.id;
                const className = selected == funcId ? "mobilemode-list-item selected" : "mobilemode-list-item";

                return (
                    <div className={className} onClick={()=>onSelect(item)}>
                        <div className="item-icon"><Icon type="file-text" /></div>
                        <div className="item-detail">
                            <h4>{item.sign.replace("()", "")}</h4>
                            <p>{item.desc || "无描述信息"}</p>
                        </div>
                    </div>
                )
            })
        )
        return (
            <WeaScroll typeClass="scrollbar-macosx" className="mobilemode-list-scroller">
                <div>
                    { items }
                </div>
            </WeaScroll >
        );
    }
}