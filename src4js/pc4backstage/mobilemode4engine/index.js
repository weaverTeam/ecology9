import { Route } from 'react-router'
import { Applications, Skins, Template, Functions } from './components/uimode'
import store from './stores/';
import "./css/icon.css";

const Routes = (
    <Route path="mobilemode">
        <Route path="admin">
            <Route path="app" component={Applications} />
            <Route path="template" component={Template}/>
            <Route path="skin" component={Skins} />
            <Route path="plugin" />
            <Route path="function" component={Functions} />
            <Route path="interface" />
        </Route>
    </Route>
);

module.exports = {
    Route: Routes,
    store
}