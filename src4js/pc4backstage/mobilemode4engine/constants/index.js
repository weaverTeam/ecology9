// 接口相关
export const API_URL = "/api/mobilemode/"
export const API_ADMIN_URL = "/api/mobilemode/admin/";
export const GET = "GET";
export const modules = {
    APP: 'app',
    SKIN: 'skin',
    FORMMODE: "formmode",
    COMMON: "common",
    TEMPLATE: "template",
    FUNCTION: "function"
};
export const urls = {
    app: API_ADMIN_URL,
    template: API_ADMIN_URL,
    skin: API_ADMIN_URL,
    formmode: API_ADMIN_URL,
    common: API_URL,
    function: API_ADMIN_URL
}


// 公共
export const SPRIT = "/";

// 缓存相关
export const MOBILE_MODE_HISTORY_FOR_ENGINE = "mobilemodeHistory4engine";
