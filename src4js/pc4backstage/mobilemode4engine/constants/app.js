// app sort type
export const sortType = {
    DEFAULT: "default",
    DESC: "desc",
    ASC: "asc"
};

// app operation
export const WASTE = "waste";
export const DELETE = "delete";

// app property 
export const CREATE_DATE = "createdate";
export const MODIFY_DATE = "modifydate";

// app designer url
export const APP_DESIGNER_URL = "/mobilemode/admin/appDesigner.jsp?appid=";