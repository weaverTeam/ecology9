import React from 'react'
import { inject, observer } from 'mobx-react';

@inject('routing')
@observer
class Home extends React.Component {
	constructor(props){
		super(props)
	}
    render() {
        return (
            <div>
	            {this.props.children}
            </div>
        )
    }
}

export default Home