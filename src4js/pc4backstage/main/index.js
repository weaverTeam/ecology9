import React from 'react'
import ReactDOM from 'react-dom';

import { createHistory, useBasename, createHashHistory } from 'history'
import { Router, Route ,useRouterHistory ,Redirect } from 'react-router'

import { Provider } from 'mobx-react';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';

const routingStore = new RouterStore();

const browserHistory  = useRouterHistory(createHashHistory)({
    queryKey: '_key',
	basename: '/'
});

const history = syncHistoryWithStore(browserHistory, routingStore);
window.weaHistory = history;
import Comstore from 'comsMobx' 
const ComStore = Comstore.store;

import PortalEngine from 'weaPortalEngine' 
const PortalEngineRoute = PortalEngine.Route;
const PortalEngineStore = PortalEngine.store;

import PortalEngineTheme from 'weaPortalEngineTheme'
const Theme = PortalEngineTheme.Theme;
const PortalEngineThemeStore = PortalEngineTheme.Store;

import MobilemodeEngine from "weaMobilemodeEngine";
const MobilemodeEngineRoute = MobilemodeEngine.Route;
const MobilemodeEngineStore = MobilemodeEngine.store;

import FormmodeEngine from 'weaFormmodeEngine';
const FormmodeEngineRoute = FormmodeEngine.route;
const FormmodeEngineStore = FormmodeEngine.store;


let stores = {
    // Key can be whatever you want
    routing:routingStore,
    ...ComStore,
    ...PortalEngineStore,
    ...PortalEngineThemeStore,
    ...MobilemodeEngineStore,
    ...FormmodeEngineStore,
}

class Root extends React.Component {
    render() {
        return (
            <Provider {...stores}>
                <Router history={history}>
                    <Route path="/" component={Theme}>
                        {PortalEngineRoute}
                        {MobilemodeEngineRoute}
                        {FormmodeEngineRoute}
                    </Route>
                </Router>
            </Provider>
        )
    }
}

ReactDOM.render(<Root />,document.getElementById('container'));
