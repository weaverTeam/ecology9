/**
 * Created by sunzhangyu on 2017/6/27.
 */
import {observable,action} from 'mobx';
import {WeaTools} from 'ecCom';
import * as THEME_API from '../apis/theme';
import ECLocalStorage from '../util/ecLocalStorage';

export default class MenusStore {
    history = null;
    @observable menuInfo = {
        backEndMenuInfo:ECLocalStorage.getObj("menu","backEndMenuInfo",true)||[],
        subMenuInfo:ECLocalStorage.getObj("menu","subMenuInfo",true)||[],
        columnMenuInfo:ECLocalStorage.getObj("menu","columnMenuInfo",true)||[],
        backEndMenuUrl:ECLocalStorage.getStr("menu","backEndMenuUrl",true)||"",
        infoId:ECLocalStorage.getStr("menu","infoId",true)||"",
        routeUrl:ECLocalStorage.getStr("menu","routeUrl",true)||""
    };
    constructor() {
        //this.history = history;
        this.addBackEndMenus = this.addBackEndMenus.bind(this);
        this.onLoadMain=this.onLoadMain.bind(this);
        this.getBackEndMenuInfo = this.getBackEndMenuInfo.bind(this);
        this.updateBackEndMenus = this.updateBackEndMenus.bind(this);
    }
    getBackEndMenuInfo() {
        return this.menuInfo;
    }

    /**
     * 获取后端菜单信息
     *
     *
     */
    @action
    addBackEndMenus(params={}){
            THEME_API.getBackEndMenus(params).then((result) => {
                // console.log("------------data------------------");
                // console.log(result);
                const backEndMenus=result;
                let initSubMenus=[];
                let initColumnMenus=[];
                let initInfoId="";
                let initRouteUrl="";
                if(backEndMenus.length>0){
                    //初始化二级子菜单
                    initSubMenus=backEndMenus[0].child;
                    initInfoId=initSubMenus[0].infoId;

                    //初始化三级子菜单(纵列菜单)
                    if(initSubMenus.length>0){
                        initColumnMenus=initSubMenus[0].child;
                    }
                }

                //初始化三级菜单地址
                let initBackEndUrl=backEndMenus[0].linkAddress;
                //初始化路由地址
                initRouteUrl=backEndMenus[0].routeurl;
                //将状态值都按模块放入html缓存中
                ECLocalStorage.set("menuInfo","backEndMenuInfo",result,true);
                ECLocalStorage.set("menuInfo","subMenuInfo",initSubMenus,true);
                ECLocalStorage.set("menuInfo","columnMenuInfo",initColumnMenus,true);
                ECLocalStorage.set("menuInfo","backEndMenuUrl",initBackEndUrl,true);
                ECLocalStorage.set("menuInfo","routeUrl",initRouteUrl,true);
                ECLocalStorage.set("menuInfo","infoId",initInfoId,true);
                let menu={
                    routeurl:initRouteUrl,
                    url:initBackEndUrl,
                    target:'mainFrame'
                }
                this.onLoadMain(menu);
                this.menuInfo.backEndMenuInfo=result;
                this.menuInfo.backEndMenuUrl=initBackEndUrl;
                this.menuInfo.subMenuInfo=initSubMenus;
                this.menuInfo.columnMenuInfo=initColumnMenus;
                this.menuInfo.routeUrl=initRouteUrl;
                this.menuInfo.infoId=initInfoId;
            });
    }

    /**
     * 切换后端菜单信息
     *
     *
     */
    @action
    updateBackEndMenus(params){
        ECLocalStorage.set("menuInfo","subMenuInfo",params.subMenuInfo,true);
        ECLocalStorage.set("menuInfo","columnMenuInfo",params.columnMenuInfo,true);
        ECLocalStorage.set("menuInfo","routeUrl",params.routeUrl,true);
        ECLocalStorage.set("menuInfo","infoId",params.infoId,true);
        if("undefined"!=typeof(params.subMenuInfo)){
            this.menuInfo.subMenuInfo=params.subMenuInfo;
        }
        if("undefined"!=typeof(params.columnMenuInfo)){
            this.menuInfo.columnMenuInfo=params.columnMenuInfo;
        }
        if("undefined"!=typeof(params.routeUrl)) {
            this.menuInfo.routeUrl = params.routeUrl;
        }
        if("undefined"!=typeof(params.infoId)) {
            this.menuInfo.infoId=params.infoId;
        }
    }

    /**
     * 加载路由或iframe
     *
     * @param menu      需要加载的菜单
     */
     onLoadMain(menu) {
        let routeUrl = menu.routeurl;
        let url = menu.url;
        console.log("routeUrl : ",routeUrl);
        if (routeUrl) {
            weaHistory.push({pathname: routeUrl});

            document.getElementById('e9frameMain').style.visibility = 'hidden';
            document.getElementById('e9routeMain').style.display = 'block';
            // let mainframe = document.getElementById('mainFrame');
            // mainframe.src = 'about:blank';
            document.getElementById('mainFrame').src = 'about:blank';
        } else if (url && url != 'javascript:void(0);') {
            let target = menu.target || 'mainFrame';
            if ('mainFrame' != target) {
                window.open(url, target);
            } else {
                // let mainframe = document.getElementById('mainFrame');
                // mainframe.src = url;
                document.getElementById('mainFrame').src = url;
                document.getElementById('e9frameMain').style.visibility = 'visible';
                document.getElementById('e9routeMain').style.display = 'none';
            }
        }
    }
}