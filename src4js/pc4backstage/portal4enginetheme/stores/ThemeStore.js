/**
 * Created by sunzhangyu on 2017/6/27.
 */
import {observable,action} from 'mobx';
import {WeaTools} from 'ecCom';
import * as THEME_API from '../apis/theme';
import ECLocalStorage from '../util/ecLocalStorage';
export default class ThemeStore {
    history = null;
    @observable backEndThemeInfo ={
        themeInfo:{
            themeType: 'ecology9',
            themeColorType: ECLocalStorage.getStr('themeTemp', `themeColorType-${ECLocalStorage.getCacheAccount()}`, false) || '0'
        },
        toolbarMoreMenu:[],
        toolbarMoreMenuVisible:false,
        account:{},
        accountVisible:false
    };
    constructor() {
        //this.history = history;
        this.loadToolbarMoreMenu = this.loadToolbarMoreMenu.bind(this);
        this.changeToolbarMoreMenuVisible = this.changeToolbarMoreMenuVisible.bind(this);
        this.loadAccount = this.loadAccount.bind(this);
        this.changeAccountVisible = this.changeAccountVisible.bind(this);
        this.getThemeInfo=this.getThemeInfo.bind(this);
    }
    getThemeInfo(params) {
        return this.backEndThemeInfo;
    }

    /**
     * 加载工具栏更多菜单
     *
     * @returns {function(*)}
     */
    @action
    loadToolbarMoreMenu() {
            THEME_API.getToolbarMoreMenu().then((result) => {
                this.backEndThemeInfo.toolbarMoreMenu=result
            });
    }

    /**
     * 显示隐藏工具栏更多菜单
     *
     * @param visible   是否显示
     * @returns {function(*)}
     */
    @action
    changeToolbarMoreMenuVisible(visible) {
         this.backEndThemeInfo.toolbarMoreMenuVisible=visible;
    }

    /**
     * 加载账号数据
     *
     * @returns {function(*)}
     */
    @action
    loadAccount() {
            THEME_API.getAccount().then((result) => {
                this.backEndThemeInfo.account=result
            });
    }

    /**
     * 显示隐藏账号数据
     *
     * @param visible   是否显示
     *
     * @returns {function(*)}
     */
    @action
    changeAccountVisible(visible) {
        this.backEndThemeInfo.accountVisible=visible;
    }
}