import {WeaTools} from 'ecCom';

// 工具栏菜单
export const getToolbarMenu = () => WeaTools.callApi('/api/portal/toolbar/toolbarmenu', 'POST', {}, 'json');
// 工具栏更多菜单
export const getToolbarMoreMenu = () => WeaTools.callApi('/api/portal/toolbarmore/toolbarmoremenu', 'POST', {menutype: 'back'}, 'json');
// 账号
export const getAccount = () => WeaTools.callApi('/api/portal/account/accountlist', 'POST', {}, 'json');
//后端菜单
export const getBackEndMenus =(params={})=> WeaTools.callApi('/api/portal/backendmenu/backendmenu','POST',params,'json');

