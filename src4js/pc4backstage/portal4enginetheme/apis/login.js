import {WeaTools} from 'ecCom';

// 表单登录
export const login = (params = {}) => WeaTools.callApi('/login/LoginUtil.jsp', 'POST', params, 'json');
// 退出
export const logout = () => WeaTools.callApi('/login/LoginUtil.jsp', 'POST', {type: 'logout'}, 'text');