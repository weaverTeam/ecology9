import React from 'react';
import { inject, observer } from 'mobx-react';
import E9Theme from './ecology9/E9Theme';

@inject('themeStore')
@inject('routing')
@observer
class Theme extends React.Component {

    render() {
        const {themeStore} = this.props;

        const themeInfo4JSON = themeStore.getThemeInfo().themeInfo;
        const themeColorType = themeInfo4JSON.themeColorType;

        return <E9Theme themeColorType={themeColorType} {...this.props} />;
    }
}

export default Theme;


