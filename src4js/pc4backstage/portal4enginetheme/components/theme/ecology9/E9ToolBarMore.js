import React from 'react';
import {Popover} from 'antd';
import { inject, observer } from 'mobx-react';
import {showDialog} from '../../../util/themeUtil';

@inject('menusStore')
@inject('themeStore')
@inject('routing')
@observer
class E9ToolBarMore extends React.Component {
    componentWillMount() {
        const {themeStore} = this.props;
        themeStore.loadToolbarMoreMenu();
    }

    onToolbarMoreMenuClick(item) {
        const {menusStore} = this.props;
        let title = item.name;
        let url = item.url;
        let linkmode = item.linkmode;

        if (linkmode == '1') {
            let width = 700;
            let height = 600;
            let opacity = 0.4;
            if (url == '/systeminfo/version.jsp') {
                width = 630;
                height = 400;
            }
            showDialog({
                title: title,
                url: url,
                width: width,
                height: height,
                opacity: opacity,
                callbackfunc: () => {
                }
            });
        } else {
            menusStore.onLoadMain({url: url, routeurl: '', target: linkmode == '0' ? '_blank' : 'mainFrame'});
        }

        this.onVisibleChange(false);
    }

    onVisibleChange(visible) {
        let e9shadowMain = document.getElementById('e9shadowMain');
        if (visible) {
            e9shadowMain.style.display = 'block';
        } else {
            e9shadowMain.style.display = 'none';
        }

        const {themeStore} = this.props;
        themeStore.changeToolbarMoreMenuVisible(visible);
    }

    render() {
        const {themeStore} = this.props;
        const toolbarMoreMenu4JSON = themeStore.getThemeInfo().toolbarMoreMenu;

        const toolbarMoreMenuContent = <ToolbarMoreMenuContent toolbarMoreMenu={toolbarMoreMenu4JSON} onToolbarMoreMenuClick={this.onToolbarMoreMenuClick.bind(this)} />;

        return (
            <div className="e9header-toolbar-more">
                <Popover visible={themeStore.getThemeInfo().toolbarMoreMenuVisible} onVisibleChange={this.onVisibleChange.bind(this)} placement="bottomLeft" content={toolbarMoreMenuContent} trigger="click" overlayClassName="e9header-toolbar-more-popover">
                    <div className="e9header-toolbar-more-icon" title="更多">
                        <i className="wevicon wevicon-e9header-toolbar-more" />
                    </div>
                </Popover>
            </div>
        )
    }
}

@inject('themeStore')
@inject('routing')
@observer
class ToolbarMoreMenuContent extends React.Component {
    render() {
        const {themeStore} = this.props;
        let items = themeStore.getThemeInfo().toolbarMoreMenu.map((item, index) => {
            return (
                <div key={index} className="e9header-toolbar-more-item" title={item.name} onClick={this.props.onToolbarMoreMenuClick.bind(this, item)}>
                    <i className={`wevicon wevicon-e9header-toolbar-more-${item.id}`} />
                    <span>{item.name}</span>
                </div>
            )
        });
        return (
            <div className="e9header-toolbar-more-content">
                {items}
            </div>
        )
    }
}



// module.exports = {E9ToolBarMore};
export default E9ToolBarMore;