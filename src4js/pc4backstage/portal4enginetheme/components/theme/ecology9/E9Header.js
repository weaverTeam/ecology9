import React from 'react';
import E9TopLogo from './E9TopLogo';
import E9TopMenu from './E9TopMenu';
import E9ToolBarMore from './E9ToolBarMore';
import E9Account from './E9Account';

class E9Header extends React.Component {
    render() {
        return (
            <div className="e9header-container">
                <div className="e9header-left">
                    <E9TopLogo />
                    <E9TopMenu {...this.props} />
                </div>

                <div className="e9header-right">
                    <div className="e9header-sign" id="signPlugin"></div>
                    <E9ToolBarMore />
                    <div className="e9header-toolbar-separate"></div>
                    <E9Account />
                </div>
            </div>
        )
    }
}


export default E9Header;