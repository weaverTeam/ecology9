/**
 * Created by Administrator on 2017/6/12.
 */
import React from 'react';
import { Menu, Icon } from 'antd';
import {WeaScroll} from 'ecCom';
import { inject, observer } from 'mobx-react';
import {WeaPopoverHrm, WeaTools, WeaErrorPage} from 'ecCom';
const SubMenu = Menu.SubMenu;

@inject("themeStore")
@inject("menusStore")
@observer
class E9LeftMenusTree extends React.Component{

    componentWillMount() {
    }

    //递归函数生成左侧菜单树
    formSubmenusChild(obj){
        let cHtml=<div></div>;
        let childArray=obj.child;
        if("undefined"!=typeof(childArray)&&childArray.length>0) {
          cHtml = childArray.map((item, i) => {
                return this.formSubmenusChild(item);
            });
            return <SubMenu key={obj.linkAddress} title={obj.name}>{cHtml}</SubMenu>
        }else{
            return <Menu.Item routeurl={obj.routeurl} key={obj.linkAddress}>{obj.name}</Menu.Item>
        }


    }
    handleClick(e) {

        const {menusStore}=this.props;
  /*      //把菜单Url作为antd Menu组件的key
        let urlParams= {
            iframeUrl: e.key,
            routeUrl:e.item.props.routeurl
        }

        actions.updateMenusUrl(urlParams);*/
        let menuInfo={
            routeurl:e.item.props.routeurl,
            url: e.key,
            target:'mainFrame'
        }
        //根据route路由地址和iframe地址的值判断使用哪个作为菜单页面展示方式
        menusStore.onLoadMain(menuInfo);
    }

    render() {
        const {menusStore} = this.props;
        let columnMenus=[];
        if("undefined"!=typeof(menusStore.getBackEndMenuInfo().columnMenuInfo)||null!=menusStore.getBackEndMenuInfo().columnMenuInfo) {
            columnMenus = menusStore.getBackEndMenuInfo().columnMenuInfo;
        }
        // let columnMenus=columnMenuInfo.toJSON();
        let html=columnMenus.map((obj,i)=> {
            if ("undefined"!=typeof(obj.child)&&obj.child.length>0) {
                return this.formSubmenusChild(obj);
            } else {
                return <Menu.Item routeurl={obj.routeurl} key={obj.linkAddress}>{obj.name}</Menu.Item>
            }
        });

        return (
            <Menu theme="dark"
                onClick={this.handleClick.bind(this)}
                  style={{ width: 201}}
                  defaultOpenKeys={['sub1']}
                  selectedKeys={[menusStore.getBackEndMenuInfo().backEndMenuUrl]}
                  mode="inline"
            >
                {html}
            </Menu>
        )
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== '';
        return (
            <WeaErrorPage msg={hasErrorMsg ? this.props.error : '对不起，该页面异常，请联系管理员！'} />
        );
    }
}
E9LeftMenusTree = WeaTools.tryCatch(React, MyErrorHandler, {error: ''})(E9LeftMenusTree);


export default E9LeftMenusTree;
