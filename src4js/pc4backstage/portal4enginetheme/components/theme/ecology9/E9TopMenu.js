import React from 'react';
import {Popover,Tabs,Icon } from 'antd';
import {WeaScroll} from 'ecCom';
import {WeaPopoverHrm, WeaTools, WeaErrorPage} from 'ecCom';
import { inject, observer } from 'mobx-react';

@inject('menusStore')
@inject('routing')
@observer
class E9TopMenu extends React.Component {
    componentWillMount() {
        const {menusStore} = this.props;
        const params={};
        //获取后端菜单树
        menusStore.addBackEndMenus(params);
    }


        backMenuChangeTab(key){
        const{menusStore}=this.props;
        let backEndMenus=menusStore.getBackEndMenuInfo().backEndMenuInfo;
        // console.log("++++++++++++++++++");
        // console.log(menusStore.getBackEndMenuInfo());
        let currentMenus=backEndMenus[key];
        //二级菜单数组
        let currentSubMenus=currentMenus.child;
        //三级菜单数组
        let currentColumnMenus=[];
        if(currentSubMenus.length>0){
            currentColumnMenus=currentSubMenus[0].child;
        }
        let params={
            subMenuInfo:currentSubMenus,
            columnMenuInfo:currentColumnMenus,
            routeUrl:currentMenus.routeurl,
            infoId:currentSubMenus[0].infoId
        }
        /* //三级菜单首页url
         let backEndMenuUrl=currentColumnMenus[0].linkAddress;*/
            menusStore.updateBackEndMenus(params);
       /* //更新菜单Url状态值
        let urlParams= {
            iframeUrl: currentMenus.linkAddress,
            routeUrl:currentMenus.routeurl
        }
        actions.updateMenusUrl(urlParams);*/
        let menuInfo={
            routeurl:currentMenus.routeurl,
            url:currentMenus.linkAddress,
            target:'mainFrame'
        }
        //根据route路由地址和iframe地址的值判断使用哪个作为菜单页面展示方式
            menusStore.onLoadMain(menuInfo);
    }


    render() {
        const {menusStore} = this.props;
        const TabPane = Tabs.TabPane;
        //后端菜单集合
        const backEndMenus=menusStore.getBackEndMenuInfo().backEndMenuInfo;
        // console.log("后端菜单"+backEndMenus);
        const menuHtml=backEndMenus.map((menuInfo,i)=>{
            return (<TabPane tab={<span className="topMenuSpan"><i className={`menuicon menuicon-admin-portal-${menuInfo.infoId}`}></i>{menuInfo.name}</span>} key={i}></TabPane>);
        });

        return (
            <div className="e9header-top-menu">
               {/* <ul className="tabs" style={{height:'55px',width:'5000px'}}>*/}
                <Tabs defaultActiveKey="0" onTabClick={this.backMenuChangeTab.bind(this)}>
                {menuHtml}
                </Tabs>
             {/*   </ul>*/}
            </div>
        )
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== '';
        return (
            <WeaErrorPage msg={hasErrorMsg ? this.props.error : '对不起，该页面异常，请联系管理员！'} />
        );
    }
}
E9TopMenu = WeaTools.tryCatch(React, MyErrorHandler, {error: ''})(E9TopMenu);



export default E9TopMenu;