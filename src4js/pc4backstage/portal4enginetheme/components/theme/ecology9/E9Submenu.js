/**
 * Created by Administrator on 2017/6/7.
 */
import React from 'react';
import {Popover,Tabs,Icon } from 'antd';
const TabPane = Tabs.TabPane;
import { inject, observer } from 'mobx-react';
import {WeaPopoverHrm, WeaTools, WeaErrorPage} from 'ecCom';

@inject("themeStore")
@inject("menusStore")
@observer
class E9Submenu extends React.Component{

    componentWillMount() {

    }
    componentDidMount() {

    }
    //更新后端菜单状态
    backMenuChangeTab(key){
        const {menusStore}=this.props;
        let subMenus=menusStore.getBackEndMenuInfo().subMenuInfo;
        // console.log("subMenus+++++++++++++",subMenus);
        let currentSubMenus=new Object();
        for(let subMenu of subMenus){
            if(subMenu.infoId==key){
                currentSubMenus=subMenu;
            }
        }

        let currentColumnSubmenus=currentSubMenus.child;
        let params={
            subMenuInfo:subMenus,
            columnMenuInfo:currentColumnSubmenus,
            routeUrl:currentColumnSubmenus[0].routeurl,
            infoId:currentSubMenus.infoId
        }
        menusStore.updateBackEndMenus(params);

        let menuInfo={
            routeurl:currentColumnSubmenus[0].routeurl,
            url:currentColumnSubmenus[0].linkAddress,
            target:'mainFrame'
        }
        //根据route路由地址和iframe地址的值判断使用哪个作为菜单页面展示方式
        menusStore.onLoadMain(menuInfo);
    }

    render(){
        const {menusStore}=this.props;
        const subMenus=menusStore.getBackEndMenuInfo().subMenuInfo;
        // console.log("subMenus+++++++++++++",subMenus);
        let subMenuHtml=subMenus.map((subMenu,i)=>{
            return(
                <TabPane tab={subMenu.name} key={subMenu.infoId}></TabPane>);
        });
        return(<div style={{width:'100%',height:'100%'}}>
            <Tabs   onChange={this.backMenuChangeTab.bind(this)}>
            {subMenuHtml}
            </Tabs>
        </div>);
    }


}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== '';
        return (
            <WeaErrorPage msg={hasErrorMsg ? this.props.error : '对不起，该页面异常，请联系管理员！'} />
        );
    }
}
E9Submenu = WeaTools.tryCatch(React, MyErrorHandler, {error: ''})(E9Submenu);


export default E9Submenu;