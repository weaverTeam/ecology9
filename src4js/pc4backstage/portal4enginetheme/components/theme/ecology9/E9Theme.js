import React from 'react';
import {WeaPopoverHrm, WeaTools, WeaErrorPage} from 'ecCom';
import E9Header from './E9Header';
import E9Submenu from './E9Submenu';
import E9LeftMenusTree from './E9LeftMenusTree';

class E9Theme extends React.Component {
    componentWillMount() {

    }

    componentDidMount() {
    }
    //判断是否重新进行渲染
    shouldComponentUpdate(nextProps) {
            return true;
    }

    render() {
        const {themeColorType} = this.props;
        return (
            <div className={`e9theme-layout-container e9theme-color-${themeColorType}`}>
                <iframe id="hiddenPreLoader" name="hiddenPreLoader" style={{display: "none"}} frameBorder="0" width="0" height="0" src="/spa/workflow/index.html"></iframe>
                <WeaPopoverHrm />
                <div className="e9theme-layout-header">
                    <E9Header {...this.props} />
                </div>
                <div className="e9theme-layout-submenucontent">
                    <E9Submenu />
                </div>
                <div className="e9theme-layout-content">
                    <div className="e9theme-layout-aside" style={{backgroundColor:'#4c5b70'}}>
                        <E9LeftMenusTree {...this.props} />
                    </div>

                    <div className="e9theme-layout-main">
                        <div id="e9shadowMain" className="e9theme-layout-main-shadow"></div>

                        <div id="e9routeMain" className="e9theme-layout-main-route">
                         {this.props.children}
                        </div>

                        <div id="e9frameMain" className="e9theme-layout-main-frame">
                            <iframe id="mainFrame" name="mainFrame" frameBorder="no" width="100%" height="100%" scrolling="auto" src=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== '';
        return (
            <WeaErrorPage msg={hasErrorMsg ? this.props.error : '对不起，该页面异常，请联系管理员！'} />
        );
    }
}

E9Theme = WeaTools.tryCatch(React, MyErrorHandler, {error: ''})(E9Theme);



export default E9Theme;