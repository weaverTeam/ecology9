import React from 'react';

class E9TopLogo extends React.Component {
    componentWillMount() {
    }

    render() {
        // const {topLogoImage} = this.props;
        return (
            <div className="e9header-top-logo">
                {/*{ topLogoImage ? <img src={""} alt="" /> : 'e-cology | 后端引擎应用中心' }*/}
                {'e-cology | 后端引擎应用中心' }
            </div>
        )
    }
}

export default E9TopLogo;