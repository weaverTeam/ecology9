import Theme from './components/theme/Theme';
import './css/index';
import ECLocalStorage from './util/ecLocalStorage';
import MenusStore from './stores/MenusStore';
import ThemeStore from './stores/ThemeStore';
window.ecLocalStorage = ECLocalStorage;

const menusStore=new MenusStore();
const themeStore=new ThemeStore();
module.exports = {
    Theme: Theme,
    Store:{
        menusStore,
        themeStore
    }
};