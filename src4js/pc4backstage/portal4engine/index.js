import Route from 'react-router/lib/Route';

// 门户引擎 - 门户通用 - 样式
import './components/common/css/index.css';
// 门户引擎 - 门户通用 - 无权限
import PortalComNoRight from './components/common/PortalComNoRight';

// 门户维护 - 登陆前门户
import LoginPortal from './components/loginportal/LoginPortal';
// 门户维护 - 登陆后门户
import MainPortal from './components/mainportal/MainPortal';
// 门户维护 - 门户应用设置
import PortalSetting from './components/portalsetting/PortalSetting';
// 门户主题 - 门户主题库
import PortalThemeLib from './components/portalthemelib/PortalThemeLib';
// 门户菜单 - 前端菜单
import LeftMenu from './components/portalmenu/LeftMenu';
// 门户菜单 - 后端菜单
import TopMenu from './components/portalmenu/TopMenu';
// 门户菜单 - 自定义菜单
import CustomMenu from './components/portalmenu/CustomMenu';
// 门户菜单 - 菜单样式库
import MenuStyleLib from './components/menustylelib/MenuStyleLib';
// 门户页面 - 登陆前页面
import LoginPage from './components/loginpage/LoginPage';
// 门户页面 - 登陆后页面
import MainPage from './components/mainpage/MainPage';
// 门户页面 - 页面布局库
import PortalLayout from './components/portallayout/PortalLayout';
// 门户元素 - 元素样式库
import ElementStyleLib from './components/elementstylelib/ElementStyleLib';

import store from './stores/index';

const PortalEngineRoute = (
    <Route path="portalengine">
        <Route path="noright" component={PortalComNoRight}/>
        <Route path="portalmaintenance">
            <Route path="loginportal" component={LoginPortal}/>
            <Route path="mainportal" component={MainPortal}/>
            <Route path="portalsetting" component={PortalSetting}/>
        </Route>
        <Route path="portaltheme">
            <Route path="portalthemelib" component={PortalThemeLib}/>
        </Route>
        <Route path="portalmenu">
            <Route path="leftmenu" component={LeftMenu}/>
            <Route path="topmenu" component={TopMenu}/>
            <Route path="custommenu" component={CustomMenu}/>
            <Route path="menustylelib" component={MenuStyleLib}/>
        </Route>
        <Route path="portalpage">
            <Route path="loginpage" component={LoginPage}/>
            <Route path="mainpage" component={MainPage}/>
            <Route path="portallayout" component={PortalLayout}/>
        </Route>
        <Route path="portalelement">
            <Route path="elementstylelib" component={ElementStyleLib}/>
        </Route>
    </Route>
);

module.exports = {
    Route: PortalEngineRoute,
    store
};
