import {observable, action} from 'mobx';
import {message} from 'antd';
import {WeaTools} from 'ecCom';
import WeaTableMobx from 'comsMobx';
const {comsWeaTableStore} = WeaTableMobx.store;

import {getMenuRules, getMenuHStyle, getMenuVStyle, setMenuStyle} from './utils';

export default class MenuStyleLibStore {
    // 加载状态
    @observable loading = false;

    // 列表
    @observable menustyletype = 'menuh';
    @observable menustylename = '';
    @observable sessionkey = '';

    // 编辑
    @observable editHDialogVisible = false;
    @observable editVDialogVisible = false;
    @observable editData = {};

    // 新建或另存为
    @observable addDialogVisible = false;
    @observable addData = {};

    // 导入
    @observable importDialogVisible = false;

    // 预览
    @observable previewDialogVisible = false;
    @observable previewData = {};

    @action
    getDataList(params = {}) {
        this.loading = true;

        if (this.sessionkey) {
            const state = comsWeaTableStore.state[this.sessionkey.split('_')[0]];
            const {current} = state;
            if (!params.current) {
                params.current = current;
            }
        }

        WeaTools.callApi('/api/portal/menustylelib/list', 'POST', {
            menustyletype: this.menustyletype,
            menustylename: this.menustylename
        }).then(data => {
            this.sessionkey = data.sessionkey;
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);

            this.loading = false;
        });
    }

    @action
    changeMenuStyleType(type) {
        this.menustyletype = type;
        this.menustylename = '';
        this.getDataList();
    }

    @action
    searchMenuStyle(value) {
        this.menustylename = value;
        this.getDataList();
    }

    @action
    editMenuStyle(params = {}) {
        const {menustyletype, styleid} = params;

        if (menustyletype == 'menuh') {
            this.editHDialogVisible = true;
        } else if (menustyletype == 'menuv') {
            this.editVDialogVisible = true;
        }

        WeaTools.callApi('/api/portal/menustylelib/edit', 'POST', {
            menustyletype: menustyletype,
            styleid: styleid
        }).then(data => {
            const pms = document.getElementById('portal-p4e-msl-es');
            pms.innerHTML = data.css;

            if (menustyletype == 'menuh') {
                data.style = getMenuHStyle();
            } else if (menustyletype == 'menuv') {
                data.style = getMenuVStyle();
            }

            this.editData = data;
        });
    }

    @action
    editCancel() {
        this.editHDialogVisible = false;
        this.editVDialogVisible = false;
        this.editData = {};
    }

    @action
    editChange(field, value, selector, attr) {
        if (!attr) {
            this.editData[field] = value;

            if (field == 'cornerTop') {
                let borderRadius = 0;
                if (value == 'Round') {
                    borderRadius = this.editData.cornerTopRadian;
                }
                setMenuStyle(selector, 'border-top-left-radius', borderRadius);
                setMenuStyle(selector, 'border-top-right-radius', borderRadius);
            } else if (field == 'cornerBottom') {
                let borderRadius = 0;
                if (value == 'Round') {
                    borderRadius = this.editData.cornerBottomRadian;
                }
                setMenuStyle(selector, 'border-bottom-left-radius', borderRadius);
                setMenuStyle(selector, 'border-bottom-right-radius', borderRadius);
            } else if (field == 'cornerTopRadian' && this.editData.cornerTop == 'Round') {
                setMenuStyle(selector, 'border-top-left-radius', value);
                setMenuStyle(selector, 'border-top-right-radius', value);
            } else if (field == 'cornerBottomRadian' && this.editData.cornerBottom == 'Round') {
                setMenuStyle(selector, 'border-bottom-left-radius', value);
                setMenuStyle(selector, 'border-bottom-right-radius', value);
            }
        } else {
            this.editData.style[field] = value;

            if (attr == 'background-image') {
                value = 'url(' + value + ')';
            }

            setMenuStyle(selector, attr, value);
        }
    }

    @action
    editSave() {
        const rules = getMenuRules();

        const css = ['\n'];
        for (let i = 0, len = rules.length; i < len; i++) {
            css.push(rules[i].cssText.replace(/{/gm, '{\n').replace(/}/gm, '}\n').replace(/;/gm, ';\n'));
        }
        this.editData.css = css.join('');

        WeaTools.callApi('/api/portal/menustylelib/editsave', 'POST', {
            ...this.editData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    addMenuStyle(obj) {
        if (obj && obj.saveAs) {
            obj.menustylecite = obj.styleid;

            this.addDialogVisible = true;
            this.addData = obj;
        } else {
            WeaTools.callApi('/api/portal/menustylelib/add', 'POST', {}).then(data => {
                // 将请求到的数据结构转换成前端下拉组件的数据结构
                const menuhstyles = [];
                for (let i = 0, len = data.menuhstyles.length; i < len; i++) {
                    const item = data.menuhstyles[i];
                    menuhstyles.push({key: item.styleid, selected: false, showname: item.menustylename})
                }

                const menuvstyles = [];
                for (let i = 0, len = data.menuvstyles.length; i < len; i++) {
                    const item = data.menuvstyles[i];
                    menuvstyles.push({key: item.styleid, selected: false, showname: item.menustylename})
                }

                data.menustyletype = this.menustyletype;
                if (this.menustyletype == 'menuh') {
                    data.menustyles = menuhstyles;
                } else if (this.menustyletype == 'menuv') {
                    data.menustyles = menuvstyles;
                }
                data.menustylecite = data.menustyles.length ? data.menustyles[0].key : '';

                this.addDialogVisible = true;
                this.addData = data;
            });
        }
    }

    @action
    addCancel() {
        this.addDialogVisible = false;
        this.addData = {};
    }

    @action
    addChange(field, value) {
        this.addData[field] = value;
    }

    @action
    addSave(hasEdit) {
        WeaTools.callApi('/api/portal/menustylelib/addsave', 'POST', {
            ...this.addData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();

                if (hasEdit) {
                    this.editMenuStyle(data.menustyle);
                }

                this.addDialogVisible = false;
                this.addData = {};
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    importMenuStyle() {
        this.importDialogVisible = true;
    }

    @action
    importCancel() {
        this.importDialogVisible = false;
    }

    @action
    deleteMenuStyle(obj) {
        const state = comsWeaTableStore.state[this.sessionkey.split('_')[0]];
        const {selectedRowKeys} = state;

        let styleids = '';
        if (obj && obj.styleid) {
            styleids = obj.styleid;
        } else {
            styleids = selectedRowKeys.join(',');
        }

        if (styleids) {
            WeaTools.callApi('/api/portal/menustylelib/delete', 'POST', {
                menustyletype: this.menustyletype,
                styleids: styleids
            }).then(data => {
                if (data.result) {
                    this.getDataList();
                }
            });
        }
    }

    @action
    previewMenuStyle(obj) {
        this.previewDialogVisible = true;
        this.previewData = obj;
    }

    @action
    previewCancel() {
        this.previewDialogVisible = false;
        this.previewData = {};
    }
}
