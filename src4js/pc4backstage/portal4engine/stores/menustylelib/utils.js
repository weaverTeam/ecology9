export const getMenuStyleSheet = () => {
    const es = document.getElementById('portal-p4e-msl-es');
    return es.sheet || es.styleSheet;
};

export const getMenuRules = () => {
    const styleSheet = getMenuStyleSheet();
    return styleSheet.cssRules || styleSheet.rules;
};

export const setMenuStyle = (selector, attr, value) => {
    const rules = getMenuRules();
    const ruleLength = rules.length;

    for (let i = 0; i < ruleLength; i++) {
        const __rule = rules[i];
        const __selector = __rule.selectorText;

        if (__selector.indexOf(selector) != -1) {
            __rule.style.setProperty(attr, value);
            break;
        }
    }
};

export const getStyle = (selector) => {
    let style = {};

    const rules = getMenuRules();
    const ruleLength = rules.length;

    for (let i = 0; i < ruleLength; i++) {
        const __rule = rules[i];
        const __selector = __rule.selectorText;

        if (__selector.indexOf(selector) != -1) {
            style = __rule.style;
            break;
        }
    }

    return style;
};

export const getMenuHStyle = () => {
    const container = getStyle('#menuhContainer');
    const main = getStyle('#menuhContainer .main');
    const mainHover = getStyle('#menuhContainer .main:hover');
    const sub = getStyle('#menuhContainer .sub');
    const subHover = getStyle('#menuhContainer .sub:hover');

    return {
        menuBackgroundColor: rgb2hex(container.backgroundColor || '#000'),
        menuBackgroundImage: getBackgroundImageUrl(container.backgroundImage),

        mainFontColor: rgb2hex(main.color || '#000'),
        mainFontSize: main.fontSize,
        mainFontFamily: main.fontFamily,
        mainFontWeight: main.fontWeight,
        mainFontStyle: main.fontStyle,
        mainBackgroundColor: rgb2hex(main.backgroundColor || '#000'),
        mainBackgroundImage: getBackgroundImageUrl(main.backgroundImage),
        mainBorderRightWidth: main.borderRightWidth,
        mainBorderRightStyle: main.borderRightStyle,
        mainBorderRightColor: rgb2hex(main.borderRightColor || '#000'),
        mainPaddingTop: main.paddingTop,
        mainPaddingBottom: main.paddingBottom,
        mainPaddingLeft: main.paddingLeft,
        mainPaddingRight: main.paddingRight,

        mainHoverFontColor: rgb2hex(mainHover.color || '#000'),
        mainHoverFontSize: mainHover.fontSize,
        mainHoverFontFamily: mainHover.fontFamily,
        mainHoverBackgroundColor: rgb2hex(mainHover.backgroundColor || '#000'),
        mainHoverBackgroundImage: getBackgroundImageUrl(mainHover.backgroundImage),

        subFontColor: rgb2hex(sub.color || '#000'),
        subFontSize: sub.fontSize,
        subFontFamily: sub.fontFamily,
        subFontWeight: sub.fontWeight,
        subFontStyle: sub.fontStyle,
        subBackgroundColor: rgb2hex(sub.backgroundColor || '#000'),
        subBackgroundImage: getBackgroundImageUrl(sub.backgroundImage),
        subBorderBottomWidth: sub.borderBottomWidth,
        subBorderBottomStyle: sub.borderBottomStyle,
        subBorderBottomColor: rgb2hex(sub.borderBottomColor || '#000'),
        subPaddingTop: sub.paddingTop,
        subPaddingBottom: sub.paddingBottom,
        subPaddingLeft: sub.paddingLeft,
        subPaddingRight: sub.paddingRight,

        subHoverFontColor: rgb2hex(subHover.color || '#000'),
        subHoverFontSize: subHover.fontSize,
        subHoverFontFamily: subHover.fontFamily,
        subHoverBackgroundColor: rgb2hex(subHover.backgroundColor || '#000'),
        subHoverBackgroundImage: getBackgroundImageUrl(subHover.backgroundImage)
    };
};

export const getMenuVStyle = () => {
    const mainBg = getStyle('#menuv .mainBg');
    const mainBg_top = getStyle('#menuv .mainBg_top');
    const mainFont = getStyle('#menuv .mainFont');
    const mainBgHover = getStyle('#menuv .mainBg:hover');
    const mainBg_topHover = getStyle('#menuv .mainBg_top:hover');
    const mainFontHover = getStyle('#menuv .mainFont:hover');
    const sub = getStyle('#menuv .sub');
    const subHover = getStyle('#menuv .sub:hover');
    const collapsed = getStyle('#menuv .collapsed');

    return {
        mainFontColor: rgb2hex(mainFont.color || '#000'),
        mainFontSize: mainFont.fontSize,
        mainFontFamily: mainFont.fontFamily,
        mainFontWeight: mainFont.fontWeight,
        mainFontStyle: mainFont.fontStyle,

        mainHoverFontColor: rgb2hex(mainFontHover.color || '#000'),
        mainHoverFontSize: mainFontHover.fontSize,
        mainHoverFontFamily: mainFontHover.fontFamily,

        mainBackgroundColor: rgb2hex(mainBg_top.backgroundColor || '#000'),
        mainBackgroundImage: getBackgroundImageUrl(mainBg_top.backgroundImage),

        mainHoverBackgroundColor: rgb2hex(mainBg_topHover.backgroundColor || '#000'),
        mainHoverBackgroundImage: getBackgroundImageUrl(mainBg_topHover.backgroundImage),

        main2BackgroundColor: rgb2hex(mainBg.backgroundColor || '#000'),
        main2BackgroundImage: getBackgroundImageUrl(mainBg.backgroundImage),

        main2HoverBackgroundColor: rgb2hex(mainBgHover.backgroundColor || '#000'),
        main2HoverBackgroundImage: getBackgroundImageUrl(mainBgHover.backgroundImage),

        mainCollapsedBackgroundImage: getBackgroundImageUrl(collapsed.backgroundImage),
        mainExpandedBackgroundImage: getBackgroundImageUrl(mainFont.backgroundImage),

        subFontColor: rgb2hex(sub.color || '#000'),
        subFontSize: sub.fontSize,
        subFontFamily: sub.fontFamily,
        subFontWeight: sub.fontWeight,
        subFontStyle: sub.fontStyle,

        subHoverFontColor: rgb2hex(subHover.color || '#000'),
        subHoverFontSize: subHover.fontSize,
        subHoverFontFamily: subHover.fontFamily,

        subBackgroundColor: rgb2hex(sub.backgroundColor || '#000'),
        subBackgroundImage: getBackgroundImageUrl(sub.backgroundImage),

        subHoverBackgroundColor: rgb2hex(subHover.backgroundColor || '#000'),
        subHoverBackgroundImage: getBackgroundImageUrl(subHover.backgroundImage)
    };
};

export const getElementStyleSheet = () => {
    const es = document.getElementById('portal-p4e-esl-es');
    return es.sheet || es.styleSheet;
};

export const getElementRules = () => {
    const styleSheet = getElementStyleSheet();
    return styleSheet.cssRules || styleSheet.rules;
};

export const setElementStyle = (selector, attr, value) => {
    // 判断需要设置的选择器是否存在
    let hasSelector = false;

    const rules = getElementRules();
    const ruleLength = rules.length;

    for (let i = 0; i < ruleLength; i++) {
        const __rule = rules[i];
        const __selector = __rule.selectorText;

        if (__selector.indexOf(selector) != -1) {
            hasSelector = true;
            __rule.style.setProperty(attr, value);
            break;
        }
    }

    // 需要设置的选择器不存在, 在样式表最后插入新的规则
    if (!hasSelector) {
        const styleSheet = getElementStyleSheet();
        if (styleSheet.insertRule) {
            styleSheet.insertRule(`#item_esl_es ${selector} {${attr}: ${value}}`, ruleLength);
        } else {
            styleSheet.addRule(`#item_esl_es ${selector}`, `${attr}: ${value}`, ruleLength);
        }
    }
};

export const getElementStyle = () => {
    const getStyle = (selector) => {
        let style = {};

        const rules = getElementRules();
        const ruleLength = rules.length;

        for (let i = 0; i < ruleLength; i++) {
            const __rule = rules[i];
            const __selector = __rule.selectorText;

            if (__selector.indexOf(selector) != -1) {
                style = __rule.style;
                break;
            }
        }

        return style;
    };

    const header = getStyle('.header');
    const toolbar = getStyle('.toolbar');
    const icon = getStyle('.icon');
    const title = getStyle('.title');
    const content = getStyle('.content');
    const font = getStyle('.font');
    const sparator = getStyle('.sparator');
    const tab2 = getStyle('.tab2');
    const tab2unselected = getStyle('.tab2unselected');
    const tab2selected = getStyle('.tab2selected');
    const fontHover = getStyle('.font:hover');
    const headerHover = getStyle('.header:hover');

    return {
        headerHeight: header.height,
        headerIconTop: icon.top,
        headerIconLeft: icon.left,
        headerTitleTop: title.top,
        headerTitleLeft: title.left,
        headerToolbarTop: toolbar.top,
        headerToolbarRight: toolbar.right,

        headerTitleColor: rgb2hex(title.color || '#000'),
        headerHoverBgColor: rgb2hex(headerHover.backgroundColor || '#000'),
        headerHoverBgColorSwitch: headerHover.backgroundColor ? 1 : 0,
        headerTitleBorderBottomColor: rgb2hex(title.borderBottomColor || '#000'),
        headerTitleBorderBottomSwitch: parseInt(title.borderBottomWidth || 0, 10) != 0 ? 1 : 0,
        headerTitleFontSize: title.fontSize,
        headerTitleFontFamily: title.fontFamily,
        headerTitleFontWeight: title.fontWeight,
        headerTitleFontStyle: title.fontStyle,

        headerBackgroundColor: rgb2hex(header.backgroundColor || '#fff'),
        headerBackgroundImage: getBackgroundImageUrl(header.backgroundImage),
        headerBackgroundRepeat: header.backgroundRepeat,
        headerBackgroundPositionX: header.backgroundPositionX,
        headerBackgroundPositionY: header.backgroundPositionY,

        headerBorderTopWidth: header.borderTopWidth == 'medium' ? '3px' : header.borderTopWidth,
        headerBorderTopStyle: header.borderTopStyle,
        headerBorderTopColor: rgb2hex(header.borderTopColor || '#000'),
        headerBorderBottomWidth: header.borderBottomWidth == 'medium' ? '3px' : header.borderBottomWidth,
        headerBorderBottomStyle: header.borderBottomStyle,
        headerBorderBottomColor: rgb2hex(header.borderBottomColor || '#000'),
        headerBorderLeftWidth: header.borderLeftWidth == 'medium' ? '3px' : header.borderLeftWidth,
        headerBorderLeftStyle: header.borderLeftStyle,
        headerBorderLeftColor: rgb2hex(header.borderLeftColor || '#000'),
        headerBorderRightWidth: header.borderRightWidth == 'medium' ? '3px' : header.borderRightWidth,
        headerBorderRightStyle: header.borderRightStyle,
        headerBorderRightColor: rgb2hex(header.borderRightColor || '#000'),

        contentFontColor: rgb2hex(font.color || '#000'),
        contentFontHoverColor: rgb2hex(fontHover.color || '#000'),

        contentFontSize: font.fontSize,
        contentFontFamily: font.fontFamily,
        contentFontWeight: font.fontWeight,
        contentFontStyle: font.fontStyle,

        contentBackgroundColor: rgb2hex(content.backgroundColor || '#fff'),
        contentBackgroundImage: getBackgroundImageUrl(content.backgroundImage),
        contentBackgroundRepeat: content.backgroundRepeat,
        contentBackgroundPositionX: content.backgroundPositionX,
        contentBackgroundPositionY: content.backgroundPositionY,

        contentBorderTopWidth: content.borderTopWidth == 'medium' ? '3px' : content.borderRightWidth,
        contentBorderTopStyle: content.borderTopStyle,
        contentBorderTopColor: rgb2hex(content.borderTopColor || '#000'),
        contentBorderBottomWidth: content.borderBottomWidth == 'medium' ? '3px' : content.borderRightWidth,
        contentBorderBottomStyle: content.borderBottomStyle,
        contentBorderBottomColor: rgb2hex(content.borderBottomColor || '#000'),
        contentBorderLeftWidth: content.borderLeftWidth == 'medium' ? '3px' : content.borderRightWidth,
        contentBorderLeftStyle: content.borderLeftStyle,
        contentBorderLeftColor: rgb2hex(content.borderLeftColor || '#000'),
        contentBorderRightWidth: content.borderRightWidth == 'medium' ? '3px' : content.borderRightWidth,
        contentBorderRightStyle: content.borderRightStyle,
        contentBorderRightColor: rgb2hex(content.borderRightColor || '#000'),

        contentSeparator: getBackgroundImageUrl(sparator.backgroundImage),

        contentPaddingTop: content.paddingTop,
        contentPaddingBottom: content.paddingBottom,
        contentPaddingLeft: content.paddingLeft,
        contentPaddingRight: content.paddingRight,

        tabBackgroundImage: getBackgroundImageUrl(tab2.backgroundImage),
        tabUnSelectedBackgroundImage: getBackgroundImageUrl(tab2unselected.backgroundImage),
        tabSelectedBackgroundImage: getBackgroundImageUrl(tab2selected.backgroundImage),

        tabUnSelectedFontColor: rgb2hex(tab2unselected.color || '#000'),
        tabUnSelectedFontSize: tab2unselected.fontSize,
        tabUnSelectedFontFamily: tab2unselected.fontFamily,
        tabUnSelectedFontWeight: tab2unselected.fontWeight,
        tabUnSelectedFontStyle: tab2unselected.fontStyle,

        tabSelectedFontColor: rgb2hex(tab2selected.color || '#000'),
        tabSelectedFontSize: tab2selected.fontSize,
        tabSelectedFontFamily: tab2selected.fontFamily,
        tabSelectedFontWeight: tab2selected.fontWeight,
        tabSelectedFontStyle: tab2selected.fontStyle
    };
};

const rgb2hex = (color) => {
    if (/rgba|rgb/.test(color)) {
        let rgb = color.replace(/rgba|rgb|\(|\)/g, '').split(', ');

        let hex = '#';
        for (let i = 0; i < rgb.length; i++) {
            let __hex = Number(rgb[i]).toString(16);
            if (__hex.length == 1) {
                __hex = '0' + __hex;
            }
            hex += __hex;
        }

        return hex;
    } else {
        return color;
    }
};

const getBackgroundImageUrl = (backgroundImage) => {
    let url = backgroundImage.replace(/url|\(|\)|"/g, '');
    if (url == '/page/maint/style/none' || url == 'none') {
        url = '';
    }
    return url;
};
