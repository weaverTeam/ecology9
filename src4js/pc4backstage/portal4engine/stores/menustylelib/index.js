import MenuStyleLibStore from'./MenuStyleLibStore';

const portalMenuStyleLibStore = new MenuStyleLibStore();

module.exports = {
    portalMenuStyleLibStore
};
