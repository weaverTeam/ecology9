//登陆后门户
import MainPortalStore from './MainPortalStore';
//E8主题
import E8ThemeStore from './E8ThemeStore';

const mainportal_store = new MainPortalStore();
const e8theme_store = new E8ThemeStore();

module.exports = {
    mainportal_store,
    e8theme_store
};
