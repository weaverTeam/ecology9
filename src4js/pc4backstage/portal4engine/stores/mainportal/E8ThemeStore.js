import {observable, action} from 'mobx';
import {WeaTools} from 'ecCom';

export default class E8ThemeStore {
    @observable e8Theme = null;
    @observable leftMenu = [];
    @observable selectedLeftMenu = null;
    @observable topMenu = [];
    @observable isClickLeftMenu = false;
    @observable isShowTopMoreMenu = false;

    @action
    getE8Theme(id, e8ThemeList) {
        this.e8Theme = null;

        for (let i = 0, len = e8ThemeList.length; i < len; i++) {
            if (e8ThemeList[i].id == id) {
                this.e8Theme = e8ThemeList[i];
                break;
            }
        }
    }

    @action
    getLeftMenu(params = {}) {
        WeaTools.callApi('/api/portal/portalmenu/portalmenu', 'POST', params).then(data => {
            this.leftMenu = this.leftMenuFormat(data.menuList, 'portal');
            this.selectedLeftMenu = this.leftMenu.length > 0 ? this.leftMenu[0] : {};
        });
    }

    leftMenuFormat(data, type) {
        let menu = [];

        for (let i = 0; i < data.length; i++) {
            let obj = {
                // 门户菜单相关
                hpid: data[i].hpid || '',
                subCompanyId: data[i].subcompanyid || '',
                // 格式化后的id由父级菜单id加上菜单本身的id
                id: data[i].infoId ? (data[i].parentId || '0') + '-' + data[i].infoId : (data[i].parentId || '0') + '-' + data[i].id,
                parentId: data[i].parentId || '0',
                // 格式化后的levelid由父级菜单id加上菜单本身levelid
                levelid: (data[i].parentId || '0') + '-' + ('portal' == type ? data[i].levelid.split('_')[0] % 20 + '_portal' : data[i].levelid),
                origName: data[i].name,
                name: data[i].name,
                icon: !data[i].icon || data[i].icon == '/images/homepage/baseelement_wev8.gif' || data[i].icon.indexOf('/images_face/') != -1 || data[i].icon.indexOf('/images/') != -1 || data[i].icon.indexOf('/image_secondary/') != -1 ? '' : data[i].icon,
                url: data[i].url && data[i].url.replace(/&#38;/g, '&'),
                // 门户返回的路由地址最前面有个“#”号，需要去除
                routeurl: 'portal' == type && data[i].routeurl ? '/portal' + data[i].routeurl.substring(1) : data[i].routeurl,
                child: data[i].child ? this.leftMenuFormat(data[i].child, type) : [],
                target: data[i].target || 'mainFrame',
                count: data[i].count != undefined ? data[i].count + '' : '',
                countId: data[i].countId || '',
                titleUrlIcon: data[i].titleUrlIcon || '',
                titleUrl: data[i].titleUrl || '',
                tagColor: data[i].tagColor || '',
                hasTopLine: data[i].hasTopLine || '',
            };

            menu.push(obj);
        }

        return menu;
    }

    @action
    getTopMenu(params = {parentid: '0', needchild: '0', withportal: '1'}) {
        WeaTools.callApi('/api/portal/leftmenu/leftmenu', 'POST', params).then(data => {
            this.topMenu = data;
        });
    }

    @action
    clickLeftMenu(isClick, key, menu, type) {
        this.isClickLeftMenu = isClick;
        if (isClick) {
            this.selectedLeftMenu = menu;
        }
    }

    @action
    topMoreMenuShowToggle() {
        this.isShowTopMoreMenu = !this.isShowTopMoreMenu;
    }

    @action
    hideTopMoreMenu() {
        this.isShowTopMoreMenu = false;
    }
}
