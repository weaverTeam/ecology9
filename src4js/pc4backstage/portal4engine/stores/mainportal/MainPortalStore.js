import {observable, action} from 'mobx';
import {WeaTools} from 'ecCom';
import WeaTableMobx from 'comsMobx';
const {comsWeaTableStore} = WeaTableMobx.store;

export default class MainPortalStore {
    @observable loading = false;
    @observable orgType = '0';
    @observable saveBtnDisabled = true;
    @observable subCompany = {id: '0', name: ''};
    @observable sessionkey = '';
    @observable editDialogVisible = false;
    @observable editContentDialogVisible = false;
    @observable saveAsDialogVisible = false;
    @observable previewDialogVisible = false;
    @observable mainPortal = {};
    @observable e8ThemeList = [];
    @observable e7ThemeList = [];
    @observable eBasicThemeList = [];
    @observable eOfficeThemeList = [];
    @observable eCustomThemeList = [];

    @action
    getSessionKey(params = {}) {
        if (this.sessionkey) {
            const state = comsWeaTableStore.state[this.sessionkey.split('_')[0]];
            const {current} = state;
            if (!params.current) {
                params.current = current;
            }
        }

        WeaTools.callApi('/api/portal/mainportal/list', 'POST', {
            subCompanyId: this.subCompany.id
        }).then(data => {
            this.sessionkey = data.sessionkey;
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);
        });
    }

    @action
    changeSubCompany(subCompany) {
        this.subCompany = subCompany;

        if (subCompany.id == '0') {
            this.orgType = '0';
            this.saveBtnDisabled = true;

        } else {
            this.orgType = '1';
            this.saveBtnDisabled = false;
        }
    }

    @action
    listSave() {
        // 启用
        // 数据格式: id:value;id:value;id:value
        let isOpenArr = [];
        const isOpenDomArr = document.getElementsByName('isOpen');
        for (let i = 0, len = isOpenDomArr.length; i < len; i++) {
            isOpenArr.push(isOpenDomArr[i].value + ':' + (isOpenDomArr[i].checked ? '1' : '0'));
        }
        let isOpenStr = isOpenArr.join(';');

        // 指定
        let appointMpId = '';
        const isAppointDomArr = document.getElementsByName('tId');
        for (let i = 0, len = isAppointDomArr.length; i < len; i++) {
            if (isAppointDomArr[i].checked) {  // 过滤出指定的登录门户, 并获取id
                appointMpId = isAppointDomArr[i].value;
            }
        }

        WeaTools.callApi('/api/portal/mainportal/listsave', 'POST', {
            subCompanyId: this.subCompany.id,
            isOpenStr: isOpenStr,
            appointMpId: appointMpId
        }).then(data => {
            if (data.result) {
                this.getSessionKey();
            }
        });
    }

    @action
    changeOrgType(orgType) {
        this.orgType = orgType;
    }

    @action
    edit(params = {}) {
        if (params.id) {
            WeaTools.callApi('/api/portal/mainportal/edit', 'POST', params).then(data => {
                this.editDialogVisible = true;
                this.mainPortal = data;
            });
        } else {
            this.editDialogVisible = true;
            this.mainPortal = {};
        }
    }

    @action
    cancelEdit() {
        this.editDialogVisible = false;
    }

    @action
    editSave(params = {}) {
        WeaTools.callApi('/api/portal/mainportal/editsave', 'POST', params).then(data => {
            if (data.result) {
                this.editDialogVisible = false;
                this.getSessionKey();
            }
        });
    }

    @action
    editContent(params = {}) {
        WeaTools.callApi('/api/portal/mainportal/editcontent', 'POST', params).then(data => {
            this.editContentDialogVisible = true;
            this.mainPortal = data.mainPortal;
            this.e8ThemeList = data.e8ThemeList;
            this.e7ThemeList = data.e7ThemeList;
            this.eBasicThemeList = data.eBasicThemeList;
            this.eOfficeThemeList = data.eOfficeThemeList;
            this.eCustomThemeList = data.eCustomThemeList;
        });
    }

    @action
    cancelEditContent() {
        this.editContentDialogVisible = false;
    }

    @action
    changeMainPortal(obj) {
        Object.keys(obj).forEach((key) => {
            this.mainPortal[key] = obj[key];
        });
    }

    @action
    editContentSave() {
        WeaTools.callApi('/api/portal/mainportal/editcontentsave', 'POST', {
            mainPortalStr: JSON.stringify(this.mainPortal)
        }).then(data => {
            if (data.result) {
                this.editContentDialogVisible = false;
            }
        });
    }

    @action
    onDelete(obj) {
        const state = comsWeaTableStore.state[this.sessionkey.split('_')[0]];
        const {selectedRowKeys, current} = state;

        let ids = '';
        if (obj && obj.id) {
            ids = obj.id;
        } else {
            ids = selectedRowKeys.join(',');
        }

        if (ids) {
            WeaTools.callApi('/api/portal/mainportal/delete', 'POST', {
                ids: ids
            }).then(data => {
                if (data.result) {
                    comsWeaTableStore.getDatas(this.sessionkey, current);
                }
            });
        }
    }

    @action
    saveAs(obj) {
        this.mainPortal = obj;
        this.saveAsDialogVisible = true;
    }

    @action
    cancelSaveAs() {
        this.saveAsDialogVisible = false;
    }

    @action
    saveAsSave(params) {
        WeaTools.callApi('/api/portal/mainportal/saveassave', 'POST', params).then(data => {
            if (data.result) {
                this.saveAsDialogVisible = false;
                this.getSessionKey();
            }
        });
    }

    @action
    preview(obj) {
        this.mainPortal = obj;
        this.previewDialogVisible = true;
    }

    @action
    cancelPreview() {
        this.previewDialogVisible = false;
    }
}
