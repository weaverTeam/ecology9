// 门户引擎 - 门户通用
import common_store from './common/';

// 门户维护 - 登陆前门户
import loginportal_store from './loginportal/';
// 门户维护 - 登陆后门户
import mainportal_store from './mainportal/';
// 门户维护 - 门户应用设置
import portalsetting_store from './portalsetting/';
// 门户主题 - 门户主题库
import portalthemelib_store from './portalthemelib/';
// 门户菜单 - 前端菜单, 后端菜单, 自定义菜单
import portalmenu_store from './portalmenu/';
// 门户菜单 - 菜单样式库
import menustylelib_store from './menustylelib/';
// 门户页面 - 登陆前页面
import loginpage_store from './loginpage/';
// 门户页面 - 登陆后页面
import mainpage_store from './mainpage/';
// 门户页面 - 页面布局库
import portallayout_store from './portallayout/';
// 门户元素 - 元素样式库
import elementstylelib_store from './elementstylelib/';

module.exports = {
    ...common_store,
    ...loginportal_store,
    ...mainportal_store,
    ...portalsetting_store,
    ...portalthemelib_store,
    ...portalmenu_store,
    ...menustylelib_store,
    ...loginpage_store,
    ...mainpage_store,
    ...portallayout_store,
    ...elementstylelib_store,
};
