import {observable, action} from 'mobx';
import {message} from 'antd';
import {WeaTools} from 'ecCom';

import {getElementRules, getElementStyle, setElementStyle} from '../menustylelib/utils';

export default class ElementStyleLibStore {
    // 加载状态
    @observable loading = false;

    // 列表
    @observable title = '';
    @observable listData = [];

    // 编辑
    @observable editDialogVisible = false;
    @observable editData = {};

    // 新建或另存为
    @observable addDialogVisible = false;
    @observable addData = {};

    // 导入
    @observable importDialogVisible = false;

    @action
    getDataList() {
        this.loading = true;

        WeaTools.callApi('/api/portal/elementstylelib/list', 'POST', {
            title: this.title
        }).then(data => {
            let newData = [];
            let obj = {};
            let index = 0;
            for (let i = 0, len = data.length; i < len; i++) {
                index++;
                obj['col_' + index] = data[i];
                if (index == 3 || i == len - 1) {
                    newData.push(obj);
                    index = 0;
                    obj = {};
                }
            }

            this.listData = newData;
            this.loading = false;
        });
    }

    @action
    searchElementStyle(value) {
        this.title = value;
        this.getDataList();
    }

    @action
    editElementStyle(id) {
        this.editDialogVisible = true;

        WeaTools.callApi('/api/portal/elementstylelib/edit', 'POST', {
            id: id
        }).then(data => {
            const es = document.getElementById('portal-p4e-esl-es');
            es.innerHTML = data.css;

            data.style = getElementStyle();
            this.editData = data;
        });
    }

    @action
    editCancel() {
        this.editDialogVisible = false;
        this.editData = {};
    }

    @action
    editChange(field, value, selector, attr) {
        if (!attr) {
            this.editData[field] = value;

            if (field == 'cornerTop') {
                let borderRadius = 0;
                if (value == 'Round') {
                    borderRadius = this.editData.cornerTopRadian;
                }
                setElementStyle(selector, 'border-top-left-radius', borderRadius);
                setElementStyle(selector, 'border-top-right-radius', borderRadius);
            } else if (field == 'cornerBottom') {
                let borderRadius = 0;
                if (value == 'Round') {
                    borderRadius = this.editData.cornerBottomRadian;
                }
                setElementStyle(selector, 'border-bottom-left-radius', borderRadius);
                setElementStyle(selector, 'border-bottom-right-radius', borderRadius);
            } else if (field == 'cornerTopRadian' && this.editData.cornerTop == 'Round') {
                setElementStyle(selector, 'border-top-left-radius', value);
                setElementStyle(selector, 'border-top-right-radius', value);
            } else if (field == 'cornerBottomRadian' && this.editData.cornerBottom == 'Round') {
                setElementStyle(selector, 'border-bottom-left-radius', value);
                setElementStyle(selector, 'border-bottom-right-radius', value);
            }
        } else {
            this.editData.style[field] = value;

            if (attr == 'background-image') {
                value = 'url(' + value + ')';
            }

            if (field == 'headerHoverBgColorSwitch') {
                if (value == 1) {
                    value = this.editData.style['headerHoverBgColor'];
                } else {
                    value = 'transparent';
                }
            } else if (field == 'headerTitleBorderBottomSwitch') {
                if (value == 1) {
                    value = `2px solid ${this.editData.style['headerTitleBorderBottomColor']}`;
                } else {
                    value = '0px solid #000';
                }
            }

            setElementStyle(selector, attr, value);
        }
    }

    @action
    editSave() {
        const rules = getElementRules();

        const css = ['\n'];
        for (let i = 0, len = rules.length; i < len; i++) {
            css.push(rules[i].cssText.replace(/#item_esl_es/gm, '').replace(/{/gm, '{\n').replace(/}/gm, '}\n').replace(/;/gm, ';\n'));
        }
        this.editData.css = css.join('');

        WeaTools.callApi('/api/portal/elementstylelib/editsave', 'POST', {
            ...this.editData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    addElementStyle(id) {
        WeaTools.callApi('/api/portal/elementstylelib/add', 'POST', {}).then(data => {
            // 将请求到的数据结构转换成前端下拉组件的数据结构
            const elementstyles = [];
            for (let i = 0, len = data.elementstyles.length; i < len; i++) {
                const item = data.elementstyles[i];
                elementstyles.push({key: item.id, selected: false, showname: item.title})
            }

            data.elementstyles = elementstyles;
            data.cite = id || (elementstyles.length ? elementstyles[0].key : '');
            data.saveAs = !!id;

            this.addDialogVisible = true;
            this.addData = data;
        });
    }

    @action
    addCancel() {
        this.addDialogVisible = false;
        this.addData = {};
    }

    @action
    addChange(field, value) {
        this.addData[field] = value;
    }

    @action
    addSave(hasEdit) {
        WeaTools.callApi('/api/portal/elementstylelib/addsave', 'POST', {
            ...this.addData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();

                if (hasEdit) {
                    this.editElementStyle(data.elementstyle.id);
                }

                this.addDialogVisible = false;
                this.addData = {};
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    downloadElementStyle(id) {
        window.open('/page/maint/style/StyleDownload.jsp?type=element&styleid=' + id);
    }

    @action
    deleteElementStyle(ids) {
        if (ids) {
            WeaTools.callApi('/api/portal/elementstylelib/delete', 'POST', {
                ids: ids
            }).then(data => {
                if (data.result) {
                    this.getDataList();
                }
            });
        }
    }

    @action
    importElementStyle() {
        this.importDialogVisible = true;
    }

    @action
    importCancel() {
        this.importDialogVisible = false;
    }
}
