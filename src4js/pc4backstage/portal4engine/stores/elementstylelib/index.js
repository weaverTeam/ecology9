import ElementStyleLibStore from'./ElementStyleLibStore';

const portalElementStyleLibStore = new ElementStyleLibStore();

module.exports = {
    portalElementStyleLibStore
};
