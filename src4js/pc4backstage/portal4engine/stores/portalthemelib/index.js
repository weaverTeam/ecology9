import PortalThemeLibStore from'./PortalThemeLibStore';

const portalThemeLibStore = new PortalThemeLibStore();

module.exports = {
    portalThemeLibStore
};
