import {observable, action} from 'mobx';
import {message} from 'antd';
import {WeaTools} from 'ecCom';
import WeaTableMobx from 'comsMobx';
const {comsWeaTableStore} = WeaTableMobx.store;

export default class PortalThemeLibStore {
    // 加载状态
    @observable loading = false;

    // 列表
    @observable theme = 'ecology8';
    @observable themeName = '';
    @observable sessionkey = '';

    // e7主题编辑
    @observable e7EditDialogVisible = false;
    @observable e7EditData = {};

    // 站点主题预览
    @observable customPreviewDialogVisible = false;
    @observable customPreviewData = {};

    // 站点主题编辑
    @observable customEditDialogVisible = false;
    @observable customEditData = {};

    @action
    getDataList() {
        const theme = this.theme;
        if (theme == 'ecology8') {
            this.getE8DataList();
        } else if (theme == 'ecology7') {
            this.getE7DataList();
        } else if (theme == 'custom') {
            this.getCustomDataList();
        }
    }

    @action
    getE8DataList() {
    }

    @action
    getE7DataList() {
    }

    @action
    getCustomDataList() {
        this.loading = true;

        WeaTools.callApi('/api/portal/portalthemelib/customlist', 'POST', {
            themeName: this.themeName
        }).then(data => {
            this.sessionkey = data.sessionkey;
            comsWeaTableStore.getDatas(data.sessionkey, 1);

            this.loading = false;
        });
    }

    @action
    changeTheme(type) {
        this.theme = type;
        this.themeName = '';
        this.getDataList();
    }

    @action
    searchPortalTheme(value) {
        this.themeName = value;
        this.getDataList();
    }

    @action
    importE7Theme() {
        this.e7EditDialogVisible = true;
        this.e7EditData = {};
    }

    @action
    e7EditCancel() {
        this.e7EditDialogVisible = false;
        this.e7EditData = {};
    }

    @action
    e7EditSave() {
        window.startUploadAll();
    }

    @action
    previewCustomTheme(obj) {
        let dir = document.querySelector(`#template_${obj.id}`).getAttribute('templateDir');
        dir = dir.substring(0, dir.length - 1);
        obj.dir = dir;

        this.customPreviewDialogVisible = true;
        this.customPreviewData = obj;
    }

    @action
    customPreviewCancel() {
        this.customPreviewDialogVisible = false;
        this.customPreviewData = {};
    }

    @action
    downloadCustomTheme(obj) {
        const dir = document.querySelector(`#template_${obj.id}`).getAttribute('templateDir');
        const zip = dir.substring(0, dir.length - 1) + '.zip';

        window.open(`/page/template/zip/${zip}`);
    }

    @action
    editCustomTheme(obj) {
        this.customEditDialogVisible = true;
        this.customEditData = {
            id: obj.id,
            templatename: obj.templatename,
            templatedesc: obj.templatedesc,
            zipName: '',
            filename: '未选择任何文件'
        };
    }

    @action
    customEditCancel() {
        this.customEditDialogVisible = false;
        this.customEditData = {};
    }

    @action
    customEditChange(field, value) {
        this.customEditData[field] = value;
    }

    @action
    customEditSave() {
        WeaTools.callApi('/api/portal/portalthemelib/customsave', 'POST', {
            ...this.customEditData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();
                this.customEditCancel();
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    deleteCustomTheme(obj) {
        const {id} = obj;

        if (id) {
            WeaTools.callApi('/api/portal/portalthemelib/customdelete', 'POST', {
                ids: id
            }).then(data => {
                if (data.result) {
                    this.getDataList();
                }
            });
        }
    }

    @action
    importCustomTheme() {
        this.customEditDialogVisible = true;
        this.customEditData = {
            id: '',
            templatename: '',
            templatedesc: '',
            zipName: '',
            filename: '未选择任何文件'
        };
    }
}
