import { observable, computed, autorun, action } from 'mobx';
class Login4e8SettingStore {
    @observable toolbar = {
        displayMode:''
    };
    @observable settings = {
        bgImage:'',
        logoImage:'',
    };
    defaultSettings = {
        bgImage: '',
        logoImage: '',
    };
    constructor() {
        this.openPictureLib = this.openPictureLib.bind(this);
    }
    @action openPictureLib(type, e){
        if(this.toolbar.displayMode === 'preview') return;
        e.stopPropagation();
        var isSingle = true;
        var tempFile='';
        var pos=tempFile.indexOf("/page/resource/userfile/");
        if(pos!=-1) tempFile=tempFile.substring(pos+24);
        var dlg = new window.top.Dialog();//定义Dialog对象
        dlg.currentWindow = window;   
        dlg.Model=true;
        dlg.Width = top.document.body.clientWidth-100;
        dlg.Height = top.document.body.clientHeight-100;
        dlg.URL="/page/maint/common/CustomResourceMaint.jsp?isDialog=1&?file="+tempFile+"&isSingle="+isSingle;
        dlg.callbackfun = (obj,datas) => {
            if (datas!=null&&datas.id!="false"){
                let src = datas.id;
                if(src.indexOf(",") !== -1){
                    src = src.split(",")[0];
                }
                switch(type){
                    case 'logo':
                        this.settings.logoImage = src;
                        break;
                    case 'bgimage':
                        this.settings.bgImage = src;
                        break;
                }
            }
        }
        dlg.show();
    }
}
module.exports = Login4e8SettingStore;

