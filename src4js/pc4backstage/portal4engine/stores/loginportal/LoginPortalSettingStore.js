import { observable, computed, autorun, action } from 'mobx';
import { message } from 'antd';
import { WeaTools } from 'ecCom';
const { ls } = WeaTools;
class LoginPortalSettingStore {
    @observable settingdata = {
        visible: false,
    };
    login4e9_setting_store = null;
    login4e8_setting_store = null;
    constructor(login4e9_setting_store,login4e8_setting_store) {
        this.login4e9_setting_store = login4e9_setting_store;
        this.login4e8_setting_store = login4e8_setting_store;
        this.doSetting = this.doSetting.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.changeTemplateType = this.changeTemplateType.bind(this);
        this.saveSettingContent = this.saveSettingContent.bind(this);
        this.restoreDefault = this.restoreDefault.bind(this);
    }
    @action doSetting(id){
        WeaTools.callApi('/api/portal/logintemplate/getLoginSetting', 'POST', { id }).then((data) => {
            if (data.status === 'failed') {
                message.error(data.errormsg);
            } else {
                this.settingdata = {
                    title: '登录页设置',
                    visible: true,
                    id,
                    templateType: data.templateType,
                    isCurrent: data.isCurrent,
                }
                if(data.isCurrent){
                    let loginObj = ls.getJSONObj('login');
                    const btImage = loginObj ? loginObj.loginBgImage : '';
                    if(btImage){
                        data.e9settings.bgImage = btImage;
                    } 
                }
                let toolbar = this.login4e9_setting_store.defaultToolbar;
                toolbar.bgImages = data.bgImages;
                toolbar.displayMode = 'edit';
                const bgImgs = data.bgImages;
                for (var i = 0; i < bgImgs.length; i++) {
                   if(bgImgs[i] === data.e9settings.bgImage){
                       toolbar.initialSlide = i;
                       break;
                   }
                }
                this.login4e9_setting_store.toolbar = toolbar;
                this.login4e9_setting_store.settings = data.e9settings;
                this.login4e8_setting_store.settings = data.e8settings;
                this.login4e8_setting_store.toolbar.displayMode = 'edit';

            }
        });
    }
    @action handleCancel(){
        this.settingdata.visible = false;
        this.login4e9_setting_store.settings = null;
    }
    @action changeTemplateType(templateType){
        this.settingdata.templateType = templateType;
    }
    @action saveSettingContent(){
        let settings = {};
        switch(this.settingdata.templateType){
            case 'E9':
                settings = this.login4e9_setting_store.settings;
                if(this.settingdata.isCurrent) {
                    let loginObj = ls.getJSONObj('login');
                    if(!loginObj){
                       loginObj = {
                          loginBgImage: settings.bgImage,
                       } 
                    }
                    ls.set('login', loginObj);
                }
                settings.bgImages = this.login4e9_setting_store.toolbar.bgImages;
                break;
            case 'E8':
                settings = this.login4e8_setting_store.settings;
                break;
        }
        settings.id = this.settingdata.id;
        settings.templateType = this.settingdata.templateType;
        const params = {
            jsonStr: JSON.stringify(settings)
        }
        WeaTools.callApi('/api/portal/logintemplate/saveLoginSetting', 'POST', params).then((data) => {
            if (data.status === 'success') {
                message.success("保存成功");
            } else {
                message.error("保存失败");
            }
        });
    }
    @action restoreDefault(){
        switch(this.settingdata.templateType){
            case 'E9':
                this.login4e9_setting_store.settings = this.login4e9_setting_store.defaultSettings;
                let defaultToolbar = this.login4e9_setting_store.defaultToolbar;
                defaultToolbar.bgImages =  this.login4e9_setting_store.toolbar.bgImages;
                defaultToolbar.displayMode = this.login4e9_setting_store.toolbar.displayMode;
                this.login4e9_setting_store.toolbar = defaultToolbar;
                break;
            default:
                this.login4e8_setting_store.settings = this.login4e8_setting_store.defaultSettings;
                break;
        }
        
    }
}
module.exports = LoginPortalSettingStore;


