import { observable, action } from 'mobx';
import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom'; 
import WeaTableMobx from 'comsMobx';
const { comsWeaTableStore } = WeaTableMobx.store;
class LoginPortalStore {
    @observable sessionKey = '';
    constructor() {
        this.getSessionKey = this.getSessionKey.bind(this);
        this.saveLoginTemplate = this.saveLoginTemplate.bind(this);
        this.doDel = this.doDel.bind(this);
        this.deleteRows = this.deleteRows.bind(this);
    }
    @action getSessionKey(params = {}){
        if (this.sessionKey) {
            const state = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            const { current } = state;
            if (!params.current) {
                params.current = current;
            }
        }
        WeaTools.callApi('/api/portal/logintemplate/list', 'POST', params).then(data => {
            // 触发 table 更新
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);
            this.sessionKey = data.sessionkey;
        });
    }
    saveLoginTemplate(){
        const id = $("#loginPortal input[type='radio'][name='loginTemplateId']:checked").val() || '';
        WeaTools.callApi('/api/portal/logintemplate/operate', 'POST', {
            ids: id,
            optType: 'update'
        }).then((data) => {
            if (data.status === 'success') {
                message.success('保存成功！');
                this.getSessionKey();
            } else {
                message.error('保存失败！');
            }
        });
    }
    @action doDel(ids){
        Modal.confirm({
            title: '信息确认',
            content: '确定要删除吗？',
            onOk: () => {
                WeaTools.callApi('/api/portal/logintemplate/operate', 'POST', {
                    optType: 'delete',
                    ids
                }).then((data) => {
                    if (data.status === 'success') {
                        message.success('删除成功！');
                        this.getSessionKey();
                    } else {
                        message.error('删除失败！');
                    }
                });
            },
        });
    }
    @action deleteRows(){
        let ids = "";
        if(this.sessionKey){
            const { selectedRowKeys } = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            ids = selectedRowKeys.join(",");
        }
        if (ids === '') {
            message.warn('您还没有选择任何数据，无法删除');
        } else {
            this.doDel(ids);
        }
    }
}
module.exports = LoginPortalStore;
