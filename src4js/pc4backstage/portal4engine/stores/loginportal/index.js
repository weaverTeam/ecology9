//登录前门户
import LoginPortalStore from './LoginPortalStore';
//登录前门户另存为
import LoginPortalSaveAsStore from './LoginPortalSaveAsStore';
//登录前门户编辑
import LoginPortalEditStore from './LoginPortalEditStore';
//登录前门户预览
import LoginPortalPreviewStore from './LoginPortalPreviewStore';
//登录前门户设置
import LoginPortalSettingStore from './LoginPortalSettingStore';
//登录前门户登录页e9设置
import Login4e9SettingStore from './Login4e9SettingStore';
//登录前门户登录页e8设置
import Login4e8SettingStore from './Login4e8SettingStore';

const login4e9_setting_store = new Login4e9SettingStore();
const login4e8_setting_store = new Login4e8SettingStore();
const loginportal_store = new LoginPortalStore();
const loginportal_saveas_store = new LoginPortalSaveAsStore(loginportal_store);
const loginportal_edit_store = new LoginPortalEditStore(loginportal_store);
const loginportal_setting_store = new LoginPortalSettingStore(login4e9_setting_store, login4e8_setting_store);
const loginportal_preview_store = new LoginPortalPreviewStore(loginportal_setting_store);
module.exports = {
    login4e9_setting_store,
    login4e8_setting_store,
    loginportal_store,
    loginportal_saveas_store,
    loginportal_edit_store,
    loginportal_preview_store,
    loginportal_setting_store
};
