import { observable, computed, autorun, action } from 'mobx';
import { message } from 'antd';
import { WeaTools } from 'ecCom'; 
import WeaTableMobx from 'comsMobx';
const { comsWeaTableStore } = WeaTableMobx.store;
class LoginPortalEditStore {
    @observable editdata = {
        visible: false,
    };
    loginportal_store = null;
    constructor(loginportal_store) {
         this.loginportal_store = loginportal_store;
         this.doEdit = this.doEdit.bind(this);
         this.handleSubmit = this.handleSubmit.bind(this);
         this.handleCancel = this.handleCancel.bind(this);
         this.setFormItemValue = this.setFormItemValue.bind(this);
    }
    @action doEdit(id = ''){
         WeaTools.callApi('/api/portal/logintemplate/get', 'POST', { id }).then((data) => {
            if (data.status === 'failed') {
                message.error(data.errormsg);
            } else {
                this.editdata = {
                    title: '编辑',
                    visible: true,
                    data: data,
                }
            }
        });
    }
    @action handleSubmit(method = 'save') {
        const { loginTemplateId, loginTemplateName, loginTemplateTitle, isRememberPW, templateType, domainName,
        modeid, menuId, menutypeid, leftmenuid, leftmenustyleid, defaultshow } = this.editdata.data;
        let params = { loginTemplateId, loginTemplateName, loginTemplateTitle, isRememberPW, templateType, domainName,
        modeid, menuId, menutypeid, leftmenuid, leftmenustyleid, defaultshow };
        params.method = method;
        WeaTools.callApi('/api/portal/logintemplate/save', 'POST', params).then((data) => {
            if (data.status === 'success') {
                const sessionKey = this.loginportal_store.sessionKey;
                const { current } = comsWeaTableStore.state[sessionKey.split('_')[0]];
                comsWeaTableStore.getDatas(sessionKey, current || 1);
                message.success('保存成功！');
                this.handleCancel();
    /*            if(params.method === 'save&setting'){
                    doSettingLoginPortal(params.id);
                }*/
            } else {
                message.error('保存失败！');
            }
        });
    }
    @action handleCancel(){
        this.editdata.visible = false;
    }
/*    @action saveAsLoginTemplate = (params = {}) => {
        WeaTools.callApi('/api/portal/logintemplate/saveas', 'POST', params).then(data => {
            if (data.status === 'success') {
                // 触发 table 更新
                message.success('操作成功！');
            } else {
                message.error('操作失败！');
            }
        })
    }*/
    @action setFormItemValue(fieldname, value = ''){
        this.editdata.data[fieldname] = value;
    }
}

module.exports = LoginPortalEditStore;

