import { observable, computed, autorun, action } from 'mobx';
import { message } from 'antd';
import { WeaTools } from 'ecCom';
import WeaTableMobx from 'comsMobx';
const { comsWeaTableStore } = WeaTableMobx.store;
class LoginPortalSaveAsStore {
    @observable saveasdata = {
        visible: false,
    };
    loginportal_store = null;
    constructor(loginportal_store) {
        this.loginportal_store = loginportal_store;
        this.doSaveAs = this.doSaveAs.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.setInfoName = this.setInfoName.bind(this);
    }
    @action doSaveAs(id = ''){
        this.saveasdata = {
             title: id ? '另存为' : '新建',
             visible: true,
             id,
             name: '',
        }
    }
    @action handleCancel(){
        this.saveasdata.visible = false;
    }
    @action handleSubmit(){
        WeaTools.callApi('/api/portal/logintemplate/saveas', 'POST', {id:this.saveasdata.id,name:this.saveasdata.name}).then(data => {
            if (data.status === 'success') {
                // 触发 table 更新
                message.success('操作成功！');
                const sessionKey = this.loginportal_store.sessionKey;
                const { current } = comsWeaTableStore.state[sessionKey.split('_')[0]];
                comsWeaTableStore.getDatas(sessionKey, current || 1);
                this.handleCancel();
            } else {
                message.error('操作失败！');
            }
        })
    }
    @action setInfoName(value){
        this.saveasdata.name = value;
    }
}
module.exports = LoginPortalSaveAsStore;




