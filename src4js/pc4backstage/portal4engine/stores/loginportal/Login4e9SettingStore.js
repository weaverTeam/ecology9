import { observable, computed, autorun, action } from 'mobx';
import { message } from 'antd';
import { WeaTools } from 'ecCom'; 
const { ls } = WeaTools;
class Login4e9SettingStore {
    @observable toolbar = {
        displayMode:'',
        isFormLogin:true,
        bgImages:[],
        selected:'',
        fontColorDisable: true,
        bgColorDisable: true,
        replacePicturesDisable: true,
        bgImagesBoxVisible: true,
        libModalVisible: false,
        initialSlide: -1,
        helpVisible: false,
    };
    defaultToolbar = {
        displayMode:'',
        isFormLogin:true,
        bgImages:[],
        selected:'',
        fontColorDisable: true,
        bgColorDisable: true,
        replacePicturesDisable: true,
        bgImagesBoxVisible: false,
        libModalVisible: false,
        initialSlide: 0,
        helpVisible: false,
    }
    @observable settings = null;
    defaultSettings = {
        isLock: true,
        loginTemplateTitle: "泛微协同商务系统",
        logoPosition: {
            x: 0.5,
            y: 0.5
        },
        logoImage: "/wui/theme/ecology9/image/e9.png",
        areaPosition: {
            x: 0.5,
            y: 0.5
        },
        areaBgColor: "#000000",
        areaOpacity: 0.4,
        fontColor: "#bacde0",
        formOpacity: 1,
        bgImage: "/wui/theme/ecology9/image/bg1.jpg",
        btnColor: "#bacde0",
        btnBgColor: "#0b1a32",
        btnOpacity: 1,
        autoCarousel: false,
        carouselTime: 3000,
        changeBgImage: true,
        showFooter: false,
        qrcodeMiddle: false,
        showQrcode: true,
        logoBoxVisible: true,
        footerColor: "#FFFFFF",
        footerBgColor: "#1b3456",
        footerContent: "中国500强企业OA首选品牌倡导  行业良性发展，一体化高端服务",
        footerOpacity: 1,
    };
    @observable loginType = 'form';
    @observable hasMultiLang = true;
    @observable multiLangVisible = false;
    @observable isRememberAccount = false;
    @observable isRememberPassword = false;
    @observable langId = '7';
    @observable langText = '简体中文';
    @observable isLogging = false;
    @observable loginQRCode = 'ee61a20c-9b3b-4cd1-9fdc-260beed458bc';
    areaWidth = 412;
    boxWidth = top.document.body.clientWidth - 100;
    logoWidth = 184;
    logoHeight = 142;
    @computed get areaHeight() {
        return this.settings.logoBoxVisible ? 550 : 360;
    }
    constructor() {
        this.changeBgImages = this.changeBgImages.bind(this);
        this.changeImage = this.changeImage.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.changeLoginType = this.changeLoginType.bind(this);
        this.changePosition = this.changePosition.bind(this);
        this.changeLoginBgImage = this.changeLoginBgImage.bind(this);
        this.onChangeColor = this.onChangeColor.bind(this);
        this.rememberUserInfo = this.rememberUserInfo.bind(this);
        this.changeValidateCode = this.changeValidateCode.bind(this);
        this.changeMultiLangVisible = this.changeMultiLangVisible.bind(this);
        this.selectLang = this.selectLang.bind(this);
        this.handleOnMouseOver = this.handleOnMouseOver.bind(this);
        this.setToolbarValue = this.setToolbarValue.bind(this);
        this.setFormItemValue = this.setFormItemValue.bind(this);
    }
    @action changeBgImages(bgImages){
        this.toolbar.bgImages = bgImages;
    }
    @action changeImage(src){
        switch(this.toolbar.selected){
            case 'logo':
                this.settings.logoImage = src;
                break;
            case 'bgimage':
                this.settings.bgImage = src;
                this.toolbar.bgImages.push(src);
                break;
        }
    }
    @action onSelect(selected,e){
        if(this.toolbar.displayMode === 'preview') return;
        e.stopPropagation();
        let { logoPosition, areaPosition } = this.settings;
        let fontColorDisable = true;
        let bgColorDisable = true;
        let replacePicturesDisable = true;
        switch(selected){
            case 'area':
                bgColorDisable = false;
                break;
            case 'form':
                fontColorDisable = false;
                break;
            case 'bgimage':
                replacePicturesDisable = false;
                break;
            case 'logo':
                replacePicturesDisable = false;
                break;
            case 'btn':
                fontColorDisable = false;
                bgColorDisable = false;
                break;
            case 'footer':
                fontColorDisable = false;
                bgColorDisable = false;
                break;
        }
        this.toolbar.selected = selected;
        this.toolbar.fontColorDisable = fontColorDisable;
        this.toolbar.bgColorDisable = bgColorDisable;
        this.toolbar.replacePicturesDisable = replacePicturesDisable;
    }
    @action changeLoginType(e){
        e.stopPropagation();
        this.toolbar.isFormLogin = !this.toolbar.isFormLogin;
    }
    @action changePosition(logoRealPosition, areaRealPosition, logoBoxVisible){
        if(this.settings.isLock){
            this.settings.logoPosition = {
                x: (this.areaRealPosition.x + 114)/(this.boxWidth - this.logoWidth),
                y: (this.areaRealPosition.y + 27)/(this.bgImageHeight - this.logoHeight)
            }
        }else{
            this.settings.logoPosition = {
                x: logoRealPosition.x/(this.boxWidth - this.logoWidth),
                y: logoRealPosition.y/(this.bgImageHeight - this.logoHeight)
            }; 
        }
        this.settings.areaPosition = {
            x: areaRealPosition.x/(this.boxWidth - this.areaWidth),
            y: areaRealPosition.y/(this.bgImageHeight - this.areaHeight)
        };
        this.settings.logoBoxVisible = logoBoxVisible;
    } 
    @action changeLoginBgImage(initialSlide, src, isCurrent){
        if(isCurrent){
            let loginObj = ls.getJSONObj('login');
            if(loginObj){
                loginObj.loginBgImage = src;
            }else{
               loginObj = {
                    loginBgImage: src,
               } 
            }
            ls.set('login', loginObj);
        }
        this.settings.bgImage = src;
        this.toolbar.initialSlide = initialSlide;
    }
    @action onChangeColor(type,e){
        const { color, alpha } = e;
        const opacity = alpha/100;
        switch(this.toolbar.selected){
            case 'footer':
                switch(type){
                    case 'fontColor':
                        this.settings.footerColor = color;
                        break;
                    case 'bgColor':
                        this.settings.footerBgColor = color;
                        this.settings.footerOpacity = opacity;
                        break;
                }
                break;
            case 'btn':
                if(type === 'bgColor'){
                    this.settings.btnBgColor = color;
                    this.settings.btnOpacity = opacity;
                }
                break;
            case 'area':
                if(type === 'bgColor'){
                    this.settings.areaBgColor = color;
                    this.settings.areaOpacity = opacity;
                }
                break;
            case 'form':
                if(type === 'fontColor'){
                    this.settings.fontColor = color;
                    this.settings.formOpacity = opacity;
                }
                break;
        }
    }
    @action setToolbarValue(fieldname, value = ''){
        this.toolbar[fieldname] = value;
    }
    @action setFormItemValue(fieldname, value = ''){
        switch(fieldname){
            case 'isLock':
                if(value){
                    this.settings.logoBoxVisible = true;
                }
                this.settings.logoPosition = {
                    x: (this.areaRealPosition.x + 114)/(this.boxWidth - this.logoWidth),
                    y: (this.areaRealPosition.y + 27)/(this.bgImageHeight - this.logoHeight)
                }
                break;
        }
        this.settings[fieldname] = value;
    }
    @action rememberUserInfo(type){
        if(type === 'account'){
            this.isRememberAccount = !this.isRememberAccount;
        }else if(type === 'password'){
            this.isRememberPassword = !this.isRememberPassword;
        }
    }
    @action changeValidateCode(validateCode){
         this.validateCode = validateCode;
    }
    @action changeMultiLangVisible(){
        this.multiLangVisible = !this.multiLangVisible;
    }
    @action selectLang(visible, langId, langText){
        this.multiLangVisible = visible;
        this.langId = langId;
        this.langText = langText;
    }
    @action handleOnMouseOver(domType, e){
        if(this.toolbar.displayMode === 'preview') return;
            e.stopPropagation();
            var oEvent = e || event;
            var divDom = oEvent.currentTarget; 
            divDom.style.border='3px dashed rgba(0,255,0,1)';
            divDom.onmouseout = (ev) => {
            if(this.toolbar.selected !== domType){
                divDom.style.border='3px solid rgba(255,255,255,0)'
            }
        }
    }
    @computed get getBoxHeight() {
        let height = top.document.body.clientHeight - 100;
        switch(this.toolbar.displayMode){
            case 'edit':
                height = height- 50;
                break;
            case 'preview':
                height = height - 30;
                break;
        }
        return height;
    }
    @computed get containerHeight() {
        let height = top.document.body.clientHeight - 100;
        switch(this.toolbar.displayMode){
            case 'edit':
                height = height- 50 - 86;
                break;
            case 'preview':
                height = height - 30;
                break;
        }
        return height;
    }
    @computed get bgImageHeight() {
        let height = top.document.body.clientHeight - 100;
        switch(this.toolbar.displayMode){
            case 'edit':
                height = height- 50 - 86;
                break;
            case 'preview':
                height = height - 30;
                break;
        }
        return height;
    }
    @computed get logoRealPosition() {
        const position = this.settings.logoPosition;
        return {
            x: (this.boxWidth - this.logoWidth) * position.x,
            y: (this.bgImageHeight - this.logoHeight) * position.y
        }
    }
    @computed get areaRealPosition() {
        const position = this.settings.areaPosition;
        return {
            x: (this.boxWidth - this.areaWidth) * position.x,
            y: (this.bgImageHeight - this.areaHeight) * position.y
        }
    }
}
module.exports = Login4e9SettingStore;


