import { observable, computed, autorun, action } from 'mobx';
import { message } from 'antd';
import { WeaTools } from 'ecCom';
const { ls } = WeaTools;
class LoginPortalPreviewStore {
    @observable previewdata = {
        visible: false,
    };
    loginportal_setting_store = null;
    constructor(loginportal_setting_store) {
        this.loginportal_setting_store = loginportal_setting_store;
        this.doPreview = this.doPreview.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }
    @action doPreview(id){
        if(id){
            WeaTools.callApi('/api/portal/logintemplate/getLoginSetting', 'POST', { id }).then((data) => {
                if (data.status === 'failed') {
                    message.error(data.errormsg);
                } else {
                    this.previewdata = {
                        title: '预览',
                        from:'operation',
                        visible: true,
                        id,
                        templateType: data.templateType,
                    }
                    if(data.isCurrent){
                        let loginObj = ls.getJSONObj('login');
                        const btImage = loginObj ? loginObj.loginBgImage : '';
                        if(btImage){
                            data.e9settings.bgImage = btImage;
                        } 
                    }
                    let toolbar = this.loginportal_setting_store.login4e9_setting_store.defaultToolbar;
                    toolbar.bgImages = data.bgImages;
                    toolbar.displayMode = 'preview';
                    const bgImgs = data.bgImages;
                    for (var i = 0; i < bgImgs.length; i++) {
                       if(bgImgs[i] === data.e9settings.bgImage){
                           toolbar.initialSlide = i;
                           break;
                       }
                    }
                    this.loginportal_setting_store.login4e9_setting_store.settings = data.e9settings;
                    this.loginportal_setting_store.login4e9_setting_store.toolbar = toolbar;
                    this.loginportal_setting_store.login4e8_setting_store.settings = data.e8settings;
                    this.loginportal_setting_store.login4e8_setting_store.toolbar.displayMode = 'preview';
                }
            });
        }else{
            this.previewdata = {
                title: '预览',
                from:'setting',
                visible: true,
                id: this.loginportal_setting_store.settingdata.id,
                templateType: this.loginportal_setting_store.settingdata.templateType
            }
            if(this.loginportal_setting_store.settingdata.isCurrent){
                let loginObj = ls.getJSONObj('login');
                const btImage = loginObj ? loginObj.loginBgImage : '';
                if(btImage){
                    this.loginportal_setting_store.login4e9_setting_store.settings.bgImage = btImage;
                }
            }
            this.loginportal_setting_store.login4e9_setting_store.toolbar.selected = '';
            this.loginportal_setting_store.login4e9_setting_store.toolbar.displayMode = 'preview';
            this.loginportal_setting_store.login4e8_setting_store.toolbar.displayMode = 'preview';
        }
    }
    @action handleCancel(){
        this.previewdata.visible = false;
        if(this.previewdata.from === 'setting'){
            this.loginportal_setting_store.login4e9_setting_store.toolbar.displayMode = 'edit';
            this.loginportal_setting_store.login4e8_setting_store.toolbar.displayMode = 'edit';
        }
    }
}
module.exports = LoginPortalPreviewStore;


