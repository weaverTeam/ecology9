import { observable, action } from 'mobx';
import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom'; 
import WeaTableMobx from 'comsMobx';
const { comsWeaTableStore } = WeaTableMobx.store;
class LoginPageStore {
    @observable sessionKey = '';
    @observable portalname = '';
    constructor() {
        this.getSessionKey = this.getSessionKey.bind(this);
        this.deleteRows = this.deleteRows.bind(this);
        this.onSave = this.onSave.bind(this);
        this.doDel = this.doDel.bind(this);
    }
    @action getSessionKey(params = {}){
         if (this.sessionKey) {
            const state = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            const {current} = state;
            if (!params.current) {
                params.current = current;
            }
        }
        if(params.portalname === undefined){
            params.portalname = this.portalname;
        }
        WeaTools.callApi('/api/portal/loginpage/list', 'POST', params).then(data => {
            // 触发 table 更新
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);
            this.sessionKey = data.sessionkey;
            this.portalname = params.portalname;
        });
    }
    @action onSave(){
        let areaResult = "";
        $("#loginpage input[type='checkbox'][name='chkUse']").each(function(i){
            let ischecked = $(this).attr('checked') === 'checked' ? '1' : '0'; 
            if(i!==0) areaResult += "||";
            areaResult += $(this).val() + "_" + ischecked;
        });
        WeaTools.callApi('/api/portal/portalinfo/operate', 'POST', {
            moduleName:'loginpage',
            areaResult,
            method: 'save'
        }).then((data) => {
            if (data.status === 'failed') {
                message.error('保存失败！');
            } else {
                message.success('保存成功！');
                this.getSessionKey();
            }
        });
    }
    @action doDel(ids){
        Modal.confirm({
            title: '信息确认',
            content: '确定要删除吗？',
            onOk: () => {
                WeaTools.callApi('/api/portal/portalinfo/operate', 'POST', {
                    moduleName: 'loginpage',
                    method: 'delhp',
                    hpid: ids,
                }).then((data) => {
                    if (data.status === 'failed') {
                        message.error('删除失败！');
                    } else {
                        message.success('删除成功！');
                        this.getSessionKey();
                    }
                });
            },
        });
    }
    @action deleteRows(){
        let ids = "";
        if(this.sessionKey){
            const { selectedRowKeys } = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            ids = selectedRowKeys.join(",");
        }
        if (ids === '') {
            message.warn('您还没有选择任何数据，无法删除');
        } else {
            this.doDel(ids)
        }
    }
}
module.exports = LoginPageStore;
