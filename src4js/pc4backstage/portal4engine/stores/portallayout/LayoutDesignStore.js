/**打开长url的窗口，add by fmj 2015-03-04 start*/
import { observable, action } from 'mobx';
import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom'; 

class LayoutDesignStore {
    moveflag = false;   //获取选区标识
    resizeflag = false;   //重置大小标识
    resizecontainer = null;
    startcoord = '0,0';
    endcoord = '0,0';
    selectionsx = null;
    selectionsy = null;
    selectionex = null;
    selectioney = null;
    cellmergeinfo = {};
    lettersall = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']; //所有分块
    layouttablewidth = top.document.body.clientWidth * 0.5 - 20;
    dragrange = 5; //可拖拉范围
    portallayout_store = null;
    portalpagefor_layout_store;
    constructor(portallayout_store, portalpagefor_layout_store) {
        this.portallayout_store = portallayout_store;
        this.portalpagefor_layout_store = portalpagefor_layout_store;
        this.initLayoutDesign = this.initLayoutDesign.bind(this);
        this.generatorSelection = this.generatorSelection.bind(this);
        this.caculateSelection = this.caculateSelection.bind(this);
        this.mergeSelection = this.mergeSelection.bind(this);
        this.splitAll = this.splitAll.bind(this);
        this.hsplit = this.hsplit.bind(this);
        this.vsplit = this.vsplit.bind(this);
        this.bindMergeEvent = this.bindMergeEvent.bind(this);
        this.saveLayout = this.saveLayout.bind(this);
        this.reciverLayout = this.reciverLayout.bind(this);
        this.saveCheckLayout = this.saveCheckLayout.bind(this);
    }
    @action initLayoutDesign() {
        const ldHtml = [];
        ldHtml.push("<table class='layouttable' style='cursor: pointer;'>");
        ldHtml.push("<thead>");
        ldHtml.push("<tr>");
        ldHtml.push("<th coord='0,-1' style='width:25%'></th>");
        ldHtml.push("<th coord='1,-1' style='width:25%'></th>");
        ldHtml.push("<th coord='2,-1' style='width:25%'></th>");
        ldHtml.push("<th coord='3,-1' style='width:25%'></th>");
        ldHtml.push("</tr>");
        ldHtml.push("</thead>");
        ldHtml.push("<tbody>");
        ldHtml.push("<tr>");
        ldHtml.push("<td coord='0,0'></td>");
        ldHtml.push("<td coord='1,0'></td>");
        ldHtml.push("<td coord='2,0'></td>");
        ldHtml.push("<td coord='3,0'></td>");
        ldHtml.push("</tr>");
        ldHtml.push("<tr>");
        ldHtml.push("<td coord='0,1'></td>");
        ldHtml.push("<td coord='1,1'></td>");
        ldHtml.push("<td coord='2,1'></td>");
        ldHtml.push("<td coord='3,1'></td>");
        ldHtml.push("</tr>");
        ldHtml.push("<tr>");
        ldHtml.push("<td coord='0,2'></td>");
        ldHtml.push("<td coord='1,2'></td>");
        ldHtml.push("<td coord='2,2'></td>");
        ldHtml.push("<td coord='3,2'></td>");
        ldHtml.push("</tr>");
        ldHtml.push("<tr>");
        ldHtml.push("<td coord='0,3'></td>");
        ldHtml.push("<td coord='1,3'></td>");
        ldHtml.push("<td coord='2,3'></td>");
        ldHtml.push("<td coord='3,3'></td>");
        ldHtml.push("</tr>");
        ldHtml.push("</tbody>");
        ldHtml.push("</table>");
        this.reciverLayout({
            layouttable: ldHtml.join(''),
            cellmergeinfo: {},
        });
    }
    //生成选区
    @action generatorSelection(left, top, width, height) {
        var currentcoord = this.selectionsx+","+this.selectionsy;
        var tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+currentcoord+"']");
        var imageicon=$(".portal-p4e-layout-edit-content .imageicons");
        var menuleft=left+width/2;
        var menutop= top+height-imageicon.height();
        imageicon.css("top",(menutop-2)+"px");
        var rowspan=tdtemp.attr("rowspan")===undefined?1:tdtemp.attr("rowspan");
        var colspan=tdtemp.attr("colspan")===undefined?1:tdtemp.attr("colspan");
        var totalwidth;
        //可拆分
        if((rowspan>1 || colspan>1) && this.endcoord===this.startcoord && this.cellmergeinfo[currentcoord]!==undefined && this.cellmergeinfo[currentcoord].length>0){
            imageicon.find(".mergeicon").hide();
            imageicon.find(".splitall").show();
            totalwidth=33;
            if(colspan>1)  {
                totalwidth+=33;
                imageicon.find(".vsplit").show();
            } else {
              imageicon.find(".vsplit").hide();
            }

            if(rowspan>1){
                totalwidth+=33;
                imageicon.find(".hsplit").show();
            } else {
                imageicon.find(".hsplit").hide();
            }
            imageicon.show();
            //可合并
        } else if(this.startcoord!==this.endcoord){
            totalwidth=33;
            imageicon.find(".mergeicon").show();
            imageicon.find(".splitall").hide();
            imageicon.find(".hsplit").hide();
            imageicon.find(".vsplit").hide();
            imageicon.show();
        }else{
            imageicon.hide();
        }
        imageicon.css("left",(menuleft-totalwidth/2)+"px");
        var wline = $(".portal-p4e-layout-edit-content .wline");
        wline.css("left", left + 'px');
        wline.css("top", top + 'px');
        wline.css("height", height + 'px');
        wline.show();
        var eline = $(".portal-p4e-layout-edit-content .eline");
        eline.css("left", (left + width) + 'px');
        eline.css("top", top + 'px');
        eline.css("height", height + 'px');
        eline.show();
        var nline = $(".portal-p4e-layout-edit-content .nline");
        nline.css("left", left + 'px');
        nline.css("top", top + 'px');
        nline.css("width", width + 'px');
        nline.show();
        var sline = $(".portal-p4e-layout-edit-content .sline");
        sline.css("left", left + 'px');
        sline.css("top", (top + height) + 'px');
        sline.css("width", width + 'px');
        sline.show();
    }
    //计算选区
    @action caculateSelection() {
        var that = this;
        var startxy = this.startcoord.split(",");
        var endxy = this.endcoord.split(",");
        var startx = ~~startxy[0];
        var starty = ~~startxy[1];
        var endx = ~~endxy[0];
        var endy = ~~endxy[1];
        //获取左上角坐标
        var sx = startx <= endx ? startx : endx;
        var sy = starty <= endy ? starty : endy;
        //获取右下角坐标
        var ex = startx <= endx ? endx : startx;
        var ey = starty <= endy ? endy : starty;
        //选区最小坐标和最大坐标
        var minsx = sx, minsy = sy, maxex = ex, maxey = ey;
        var tdtemp;
        var colspan;
        var rowspan;
        for (var i = sx; i <= ex; i++){
            for (var j = sy; j <= ey; j++) {
                tdtemp = $(".portal-p4e-layout-edit-content td[coord='" + i + "," + j + "']");
                if (tdtemp.length > 0 && tdtemp.is(":visible")) {
                    //计算最大横坐标
                    colspan = tdtemp.attr("colspan");
                    if (colspan !== undefined && ~~colspan > 1) {
                        if (maxex < (i + ~~colspan - 1)) {
                            maxex = i + ~~colspan - 1;
                        }
                    }
                    //计算最大纵坐标
                    rowspan = tdtemp.attr("rowspan");
                    if (rowspan !== undefined && ~~rowspan > 1) {
                        if (maxey < (j + ~~rowspan - 1)) {
                            maxey = j + ~~rowspan - 1;
                        }
                    }
                }
            }
        }
        var width = 0, height = 0, left, top;
        //计算宽度
        for (var i = minsx; i <= maxex; i++) {
            tdtemp = $(".portal-p4e-layout-edit-content td[coord='" + i + "," + minsy + "']");
            if (tdtemp.length > 0 && tdtemp.is(":visible")) {
                width += tdtemp.outerWidth();
            }
        }
        //计算高度
        for (var j = minsy; j <= maxey; j++) {
            tdtemp = $(".portal-p4e-layout-edit-content td[coord='" + minsx + "," + j + "']");
            if (tdtemp.length > 0 && tdtemp.is(":visible")) {
                height += tdtemp.outerHeight();
            }
        }

        tdtemp = $(".portal-p4e-layout-edit-content td[coord='" + minsx + "," + minsy + "']");
        left = tdtemp.position().left;
        top = tdtemp.position().top;
        //绘制选区
        this.generatorSelection(left, top, width, height);

        this.selectionsx=minsx;
        this.selectionsy=minsy;
        this.selectionex=maxex;
        this.selectioney=maxey;

    }
    //合并单元格
    @action mergeSelection() {
        var that=this;
        var imageicon=$(".portal-p4e-layout-edit-content .imageicons");
        var tdtemp;
        var mergecellitems=[];
        var colspan=~~this.selectionex-~~this.selectionsx+1;
        var rowspan=~~this.selectioney-~~this.selectionsy+1;
        for(var i=this.selectionsx;i<=this.selectionex;i++){
            for(var j=this.selectionsy;j<=this.selectioney;j++){
                if(i!==this.selectionsx  ||  j!==this.selectionsy){
                    tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+i+","+j+"']");
                    if(tdtemp.length>0){
                        // tdtemp.remove();
                        tdtemp.hide();
                        tdtemp.attr("rowspan","1");
                        tdtemp.attr("colspan","1");
                        //添加合并节点
                        mergecellitems.push(i+","+j);
                    }
                }
            }
        }
        //缓存合并的节点
        this.cellmergeinfo[this.selectionsx+","+this.selectionsy]=mergecellitems;
        tdtemp= $(".portal-p4e-layout-edit-content td[coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdtemp.attr("colspan",colspan);
        tdtemp.attr("rowspan",rowspan);
        //图片默认宽度
        var totalwidth=33;
        imageicon.find(".mergeicon").hide();
        imageicon.find(".splitall").show();
        if(colspan>1){
            totalwidth+=33;
            imageicon.find(".vsplit").show();
        } else
            imageicon.find(".vsplit").hide();
        if(rowspan>1)     {
            totalwidth+=33;
            imageicon.find(".hsplit").show();
        }
        else
        imageicon.find(".hsplit").hide();
      //  imageicon.css("left",(imageicon.position().left-52)+'px');
        imageicon.css("left",(tdtemp.position().left+tdtemp.width()/2-totalwidth/2)+'px');
    }
    //拆分所有
    @action splitAll(){
        var imageicon=$(".portal-p4e-layout-edit-content .imageicons");
        var tdtemp;
        var restoreitems=this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
        for(var i=0;i<restoreitems.length;i++){
            tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+restoreitems[i]+"']");
            tdtemp.show();
        }
        tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdtemp.attr("colspan","1");
        tdtemp.attr("rowspan","1");

        tdtemp.trigger("click");

        imageicon.find(".mergeicon").show();
        imageicon.find(".splitall").hide();
        imageicon.find(".hsplit").hide();
        imageicon.find(".vsplit").hide();
        imageicon.css("left",(imageicon.position().left+52)+'px');
        //移除存储的数据
        delete  this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
    }
    //水平拆分
    @action hsplit(){
        var tdtemp,tdfirst;
        var restoreitems=this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
        tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdfirst=tdtemp;
        tdtemp.attr("rowspan","1");
        var colspan= tdtemp.attr("colspan");
        var restorex;
        var restorey;
        for(var i=0;i<restoreitems.length;i++){
            //获取恢复节点的水平坐标
            restorex=restoreitems[i].split(",")[0];
            restorey=restoreitems[i].split(",")[1];
            if(restorex===this.selectionsx || restorex==this.selectionsx){
                tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+restoreitems[i]+"']");
                tdtemp.attr("colspan",colspan);
                tdtemp.show();
                //并添加合并节点
                if(colspan!=undefined && colspan>1){
                    this.cellmergeinfo[restoreitems[i]]=[];
                    for(var j=0;j<=colspan-1;j++){
                        this.cellmergeinfo[restoreitems[i]].push((~~restorex+j)+","+restorey);
                    }
                }
            }
        }
        tdfirst.trigger("click");
    }
    //垂直拆分
    @action vsplit(){
        var tdtemp,tdfirst;
        var restoreitems=this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
        tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdfirst=tdtemp;
        tdtemp.attr("colspan","1");
        var rowspan= tdtemp.attr("rowspan");
        var restorey;
        var restorex;
        for(var i=0;i<restoreitems.length;i++){
            //获取恢复节点的水平坐标
            restorex=restoreitems[i].split(",")[0];
            restorey=restoreitems[i].split(",")[1];
            if(restorey===this.selectionsy || restorey==this.selectionsy ){
                tdtemp=$(".portal-p4e-layout-edit-content td[coord='"+restoreitems[i]+"']");
                tdtemp.attr("rowspan",rowspan);
                tdtemp.show();
                //添加合并
                if(rowspan!=undefined && rowspan>1){
                    this.cellmergeinfo[restoreitems[i]]=[];
                    for(var j=0;j<=rowspan-1;j++){
                        this.cellmergeinfo[restoreitems[i]].push(restorex+","+(~~restorey+j));
                    }
                }
            }
        }
        tdfirst.trigger("click");
    }
    //获取选区
    @action bindMergeEvent() {
        var that = this;

        $(".portal-p4e-layout-edit-content .layoutcontainer").disableSelection();
       // $(document.body).disableSelection();
        $(".portal-p4e-layout-edit-content .imageicons").disableSelection();

        $(window).resize(function (e) {
            e.stopPropagation();
            that.caculateSelection();
        });

        //合并单元格
        $(".portal-p4e-layout-edit-content .mergeicon").click(function(e){
            that.mergeSelection();
            e.stopPropagation();
        }) ;
        //拆分所有
        $(".portal-p4e-layout-edit-content .splitall").click(function(e){
            that.splitAll();
            e.stopPropagation();
        });

        //横向拆分
        $(".portal-p4e-layout-edit-content .hsplit").click(function(e){
            that.hsplit();
            e.stopPropagation();
        });

        //垂直拆分
        $(".portal-p4e-layout-edit-content .vsplit").click(function(e){
            that.vsplit();
            e.stopPropagation();
        });
        //点击事件
        $(".portal-p4e-layout-edit-content .layouttable").delegate('td', 'click', function (e) {
            var current = $(this);
            var coords=current.attr("coord").split(",");
            that.selectionsx = coords[0];
            that.selectionsy = coords[1];
            var position = current.position();
            var left = position.left;
            var top = position.top;
            var width = current.outerWidth();
            var height = current.outerHeight();
            that.generatorSelection(left, top, width, height);
            e.stopPropagation();
        });

        //计算是否呈现可拖拽
        function caculateMoveFlag(dragrange, e){
            //当前的单元格
            var currenttd=$(e.target);
            //边缘单元格不能拖动
            if(currenttd.attr("coord")===undefined)
                return;
            var coordx=currenttd.attr("coord").split(",")[0];
            var offset=currenttd.offset();
            var left=offset.left+currenttd.width();
            var clientx = e.clientX;
            if(clientx >= left-dragrange  &&  clientx<=left+dragrange && coordx!=='3'){
                $(".portal-p4e-layout-edit-content .layouttable").css("cursor","e-resize");
            }else{
                $(".portal-p4e-layout-edit-content .layouttable").css("cursor","pointer");
            }
        }

        //小数转百分数
        function cacultePci(data){
            data=data+"";
            var datastr=data.substr(2);
            return datastr.substring(0,2)+"."+datastr.substr(2)+"%";
        }

        //计算表头宽度
        function caculateThWidth(e){
            var colspan = that.resizecontainer.attr("colspan");
            var coordx = that.resizecontainer.attr("coord").split(",")[0];
            if(colspan !== undefined){
                coordx=~~coordx+~~colspan-1;
            }
            var th = $(".portal-p4e-layout-edit-content th[coord='"+coordx+",-1']");
            var thnext = $(".portal-p4e-layout-edit-content th[coord='"+(~~coordx+1)+",-1']");
            var distance= e.clientX - (that.resizecontainer.offset().left + that.resizecontainer.width());
            var thwidth = th.width();
            var thnextwidth = thnext.width();
            if(thwidth < 5){
                thwidth = 10;
                that.resizeflag = false;
            }
            if(thnextwidth < 5){
                thnextwidth = 10;
                that.resizeflag = false;
            }
            //计算百分比
            th.css("width",cacultePci((thwidth+distance)/that.layouttablewidth));
            thnext.css("width",cacultePci((thnextwidth-distance)/that.layouttablewidth));
        }

        $(".portal-p4e-layout-edit-content .layouttable").on('mousedown', function (e) {
            var currenttable=$(this);
            var mousestyle=currenttable.css("cursor");
            var current = $(e.target);
            //鼠标样式
            if(mousestyle === 'e-resize'){
                that.resizeflag  =true;
                that.resizecontainer = current;
            }
            //选区开始单元格
            that.startcoord = current.attr("coord");
            that.moveflag = true;
            e.stopPropagation();
        });
        $(".portal-p4e-layout-edit-content .layouttable").on('mousemove', function (e) {
            if(that.resizeflag){
                caculateThWidth(e);
                return;
            }else{
                caculateMoveFlag(that.dragrange, e);
            }
            if (that.moveflag) {
                var target = $(e.target);
                that.endcoord = target.attr("coord");
                that.caculateSelection();
            }
            e.preventDefault();
            e.stopPropagation();
        });
        $(".portal-p4e-layout-edit-content .layouttable").on('mouseup', function (e) {
            var target = $(e.target);
            that.endcoord = target.attr("coord");
            that.moveflag = false;
            if(that.resizeflag!==true)
                that.caculateSelection();
            that.resizeflag=false;
            e.stopPropagation();
        });
        //存储布局文件
    }
    @action saveLayout(params = {}){
        var that = this;
        //主要存储表单和合并单元格的信息
        var layouthtml = "",tdtemp,tds,layout_dialog;
        var allowareas = [];
        var allowarea = "";
        var tableclone=$(".portal-p4e-layout-edit-content .layouttable").clone();
        tds = tableclone.find("td");
        var count=0;
        for(var i=0;i<tds.length;i++){
            tdtemp=$(tds[i]);
            if(tdtemp.css("display")!=='none'){
                allowarea = this.lettersall[count++];//单元格标识连续
                allowareas.push(allowarea);
                tdtemp.html("${elements_"+allowarea+"}");
            }
        }
        params.layouttemplate=$("<div></div>").append(tableclone).html();
        params.layouttable=$("<div></div>").append($(".portal-p4e-layout-edit-content .layouttable").clone()).html();
        params.cellmergeinfo = JSON.stringify(this.cellmergeinfo);
        if(params.layoutid){
            var checkparam = {};
            checkparam.layoutid = params.layoutid;
            checkparam.allowarea = allowareas.join(",");
            WeaTools.callApi('/api/portal/portallayout/savecheck', 'POST', checkparam).then(data => {
                if (data.status === 'failed') {
                    message.error(data.errormsg);
                } else {
                   //未涉及元素的丢失且该布局无相关的门户页面,则直接存储
                    if(data.islostel === '0'  &&  data.reapages === '0'){
                        postLayout();
                    }else{
                        Modal.confirm({
                            title: '信息确认',
                            content: getShowInfo(data),
                            onOk: () => {
                                postLayout();
                            },
                        });
                    } 
                }
            });
        }else{
            //存储布局
            postLayout();
        }
        function bindevent(){
            const layoutid = params.layoutid;
            that.portalpagefor_layout_store.getSessionKey({layoutid});
        }

        //获取提示信息
        function getShowInfo(rs){
            var msg = <div/>;
            var reapages=~~rs.reapages;
            var islostel=rs.islostel;
            if(rs.islostel==='1' &&  reapages>0){
                msg = <div>存储布局会导致元素丢失，且会影响<b><a style={{color:'red', fontSize: '14px'}} href='javascript:void(0);' onClick={bindevent}> {reapages} </a></b>个门户页面!</div>;
            }else if(rs.islostel==='1'){
                msg = <div>存储布局会导致当前门户页面元素丢失!</div>;
            }else {
                msg = <div>存储布局会影响<b><a style={{color:'red', fontSize: '14px'}} href='javascript:void(0);' onClick={bindevent}> {reapages} </a></b>个门户页面!</div>;
            }
            return msg;
        }

        function postLayout(){
            WeaTools.callApi('/api/portal/portallayout/save', 'POST', params).then(data => {
                if (data.status === 'failed') {
                    message.error(data.errormsg);
                } else {
                   that.portallayout_store.getData();
                   that.portallayout_store.onEditCancel();
                }
            });
        }
    }
    //恢复布局
    @action reciverLayout(layout){
        $(".portal-p4e-layout-edit-content .imageicons,.wline,.eline,.nline,.sline").hide();
        const layouttable = layout.layouttable;
        this.cellmergeinfo = layout.cellmergeinfo;
        $(".portal-p4e-layout-edit-content .layouttable").remove();
        $(".portal-p4e-layout-edit-content .layouttablewrapper").append(layouttable);
        this.bindMergeEvent();
    }
    @action saveCheckLayout(params = {}){
        WeaTools.callApi('/api/portal/portallayout/checkname', 'POST', {layoutid: params.layoutid, layoutname: params.layoutname }).then(data => {
            if (data.status === 'failed') {
                message.error(data.errormsg);
            } else {
                this.saveLayout(params);
            }
        });
    }
};
module.exports = LayoutDesignStore;