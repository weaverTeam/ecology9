import { observable, action } from 'mobx';
import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom'; 
import WeaTableMobx from 'comsMobx';
const { comsWeaTableStore } = WeaTableMobx.store;
class PortalPageForLayoutStore {
    @observable sessionKey = '';
    @observable title = '';
    @observable visible = false;
    constructor() {
        this.getSessionKey = this.getSessionKey.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }
    @action getSessionKey(params = {}){
        if (this.sessionKey) {
            const state = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            const { current } = state;
            if (!params.current) {
                params.current = current;
            }
        }
        WeaTools.callApi('/api/portal/portallayout/portallist', 'POST', params).then(data => {
            // 触发 table 更新
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);
            this.sessionKey = data.sessionkey;
            this.title = '页面';
            this.visible = true;
        });
    }
    @action onCancel(params = {}){
        this.title = '';
        this.visible = false;
    }
}
module.exports = PortalPageForLayoutStore;
