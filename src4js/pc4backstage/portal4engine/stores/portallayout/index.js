//页面布局库
import PortalLayoutStore from './PortalLayoutStore';
const portallayout_store = new PortalLayoutStore();

//门户页面列表
import PortalPageForLayoutStore from './PortalPageForLayoutStore';
const portalpagefor_layout_store = new PortalPageForLayoutStore();

//页面布局库
import LayoutDesignStore from './LayoutDesignStore';
const layoutdesign_store = new LayoutDesignStore(portallayout_store, portalpagefor_layout_store);


module.exports = {
    portallayout_store,
    layoutdesign_store,
    portalpagefor_layout_store
};
