import { observable, action } from 'mobx';
import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom'; 
class PortalLayoutStore {
    @observable layoutname = '';
    @observable data = [];
    @observable edit_dialog = {
        visible: false,
        title: '',
        layoutid: '',
        data: {
            layoutname: '',
            layoutdesc: ''
        },
    };
    @observable upload_dialog = {
        visible: false,
        title: '',
    };
    constructor() {
        this.getData = this.getData.bind(this);
        this.showTableLayout = this.showTableLayout.bind(this);
        this.deleteLayout = this.deleteLayout.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onEditCancel = this.onEditCancel.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.saveUpload = this.saveUpload.bind(this);
        this.onUploadCancel = this.onUploadCancel.bind(this);
        this.onDownload = this.onDownload.bind(this);
        this.setUploadZipName = this.setUploadZipName.bind(this);
        this.setEditInputValue = this.setEditInputValue.bind(this);
        this.setUploadInputValue = this.setUploadInputValue.bind(this);
    }
    @action getData(params = {}){
        if(params.layoutname === undefined){
            params.layoutname = this.layoutname;
        }
        WeaTools.callApi('/api/portal/portallayout/list', 'POST', params).then(data => {
            this.data = data;
            this.layoutname = params.layoutname;
        });
    }
    @action showTableLayout(layout){
        const { id, layouttable, layouttype } = layout
        const lettersall = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        if(layouttable !== undefined &&  layouttable!=='' && (layouttype === 'design' || layouttype==='sys')){
            setTimeout(()=>{
                $("#portallayout").find(".layouttable").each(function(i){
                    let count = 0;
                    $(this).find("td").each(function(i){
                       if($(this).css("display") !== 'none'){
                            $(this).html(lettersall[count]);
                            count++;
                       }
                    });
                });
            },10);
        }
    }
    @action deleteLayout(layoutid){
        if(layoutid){
            Modal.confirm({
                title: '信息确认',
                content: '确定要删除吗？',
                onOk: () => {
                    WeaTools.callApi('/api/portal/portallayout/delete', 'POST', {
                        layoutid
                    }).then(data => {
                        if (data.status === 'failed') {
                            message.error(data.errormsg);
                        } else {
                            message.success('删除成功！');
                            this.getData();
                        }
                    });
                },
            });
        }else{
            message.warn('为获取到layoutid的值，无法删除');
        }
    }
    @action onEdit(layoutid){
        if(layoutid){
            WeaTools.callApi('/api/portal/portallayout/edit', 'POST', {
                layoutid
            }).then(data => {
                if (data.status === 'failed') {
                    message.error(data.errormsg);
                } else {
                    this.edit_dialog = {
                        visible: true,
                        title: '编辑布局',
                        data,
                    } 
                }
            });
        }else{
            this.edit_dialog = {
                visible: true,
                title: '新建布局',
                data: { 
                    layoutid: '',
                    layouttable: '',
                    layoutname: '',
                    layoutdesc: '',
                    cellmergeinfo: {},
                },
            } 
        }
    }
    @action onEditCancel(){
        this.edit_dialog = {
            visible: false,
            title: '',
            layoutid: '',
            data: { 
                layoutname: '',
                layoutdesc: ''
            },
        }
    }
    @action onUpload(){
        this.upload_dialog = {
            visible: true,
            title: '上传布局',
            layoutname: '',
            layoutdesc: '',
            zipname: '未选择任何文件',
            layoutzip: '',
            zipTmps: [],
        } 
    }
    @action setEditInputValue(fieldName, value){
        this.edit_dialog.data[fieldName] = value;
    }
    @action setUploadInputValue(fieldName, value){
        this.upload_dialog[fieldName] = value;
    }
    @action setUploadZipName(zipname, zipTmp){
        this.upload_dialog.zipname = zipname;
        this.upload_dialog.layoutzip = zipTmp+"/"+zipname;
        this.upload_dialog.zipTmps.push(zipTmp);
    }
    @action saveUpload(){
        const { layoutname, layoutdesc, zipTmps, layoutzip } = this.upload_dialog;
        let params = {
            layoutname,
            layoutdesc
        }
        if(zipTmps && zipTmps.length > 1){
            zipTmps.pop();
            params.zipTmps = JSON.stringify(zipTmps);
        }
        params.layoutzip = layoutzip;
        WeaTools.callApi('/api/portal/portallayout/saveUpload', 'POST', params).then(data => {
            if (data.status === 'failed') {
                message.error(data.errormsg);
            } else {
                message.success("保存成功");
                this.getData();
                this.upload_dialog = {
                    visible: false,
                    title: '上传布局',
                }
            }
        });
    }
    @action onUploadCancel(){
        const { zipTmps } = this.upload_dialog;
        if(zipTmps && zipTmps.length){
            WeaTools.callApi('/api/portal/portallayout/deleteZip', 'POST', { zipTmps: JSON.stringify(zipTmps) }).then(data => {
                if (data.status === 'failed') {
                    message.error(data.errormsg);
                }
            });
        }
        this.upload_dialog = {
            visible: false,
            title: '上传布局',
        }
    }
    @action onDownload(layoutid,layoutpath){
        layoutpath = "/page/layout/zip/"+layoutpath;
        window.open(layoutpath);
    }
}

module.exports = PortalLayoutStore;
