import { observable, action } from 'mobx';
import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom'; 
import WeaTableMobx from 'comsMobx';
const { comsWeaTableStore } = WeaTableMobx.store;
class MainPageStore {
    @observable infoname = '';
    @observable sessionKey = '';
    @observable subCompany = {id: '', name: ''};
    @observable pageOrderDialog = {
        visible: false,
        title:'页面排序维护',
        dataUrl: '/api/portal/mainpage/portallist',
    };
    @observable shareDialog = {
        module: '',
        visible: false,
        sessionKey: '',
        title:'',
        shareObj: {},
    };
    constructor() {
        this.getSessionKey = this.getSessionKey.bind(this);
        this.changeSubCompany = this.changeSubCompany.bind(this);
        this.onSave = this.onSave.bind(this);
        this.deleteRows = this.deleteRows.bind(this);
        this.doDel = this.doDel.bind(this);
        this.doOpenPageOrderDialog = this.doOpenPageOrderDialog.bind(this);
        this.handlePageOrderCancel = this.handlePageOrderCancel.bind(this);
        this.doSavePageOrder = this.doSavePageOrder.bind(this);
        this.doOpenShareDialog = this.doOpenShareDialog.bind(this);
        this.handleShareCancel = this.handleShareCancel.bind(this);
        this.saveShare = this.saveShare.bind(this);
        this.deleteShare = this.deleteShare.bind(this);
    }
    @action doOpenShareDialog(module, record = {}){
        let params = { module };
        let title = '';
        params.hpid = record.id;
        if(module === 'maint') {
            title = '维护者';
        }else if(module === 'maint') {
            title = '共享范围';  
        }
        WeaTools.callApi('/api/portal/mainpage/share', 'POST', params).then(data => {
            if (data.status === 'failed') {
                message.error(data.errormsg);
            } else {
                this.shareDialog = {
                    module: module,
                    visible: true,
                    sessionKey: data.sessionkey,
                    title,
                    shareObj: record,
                };
            }
        });
    }
    @action handleShareCancel(){
        this.shareDialog.module = '';
        this.shareDialog.visible = false;
    }
    @action saveShare(obj, data, callback) {
        WeaTools.callApi('/api/portal/mainpage/addShare', 'POST', {
            module: this.shareDialog.module,
            hpid: obj.id,
            ...data
        }).then(data => {
            if (data.status === 'failed') {
                message.warning('保存失败！');
            } else {
                message.success('保存成功！');
                callback();
                this.doOpenShareDialog(this.shareDialog.module, obj);
            }
        });
    }
    @action deleteShare(obj, ids){
        if(ids){
            Modal.confirm({
                title: '信息确认',
                content: '确定要删除吗？',
                onOk: () => {
                    WeaTools.callApi('/api/portal/mainpage/delShare', 'POST', {
                        module: this.shareDialog.module,
                        ids,
                    }).then(data => {
                        if (data.status === 'failed') {
                            message.warning('删除失败！');
                        } else {
                            message.success('删除成功！');
                            this.doOpenShareDialog(this.shareDialog.module, obj);
                        }
                    });
                },
            });
        }else{
            message.warn('您还没有选择任何数据，无法删除');
        }
    }
    @action doOpenPageOrderDialog(params = {}){
        this.pageOrderDialog.visible = true;
    }
    @action handlePageOrderCancel(){
        this.pageOrderDialog.visible = false;
    }
    @action doSavePageOrder(params = {}){
        WeaTools.callApi('/api/portal/mainpage/saveportallist', 'POST', params).then(data => {
            if (data.status === 'failed') {
                message.error('保存失败！');
            } else {
                message.success('保存成功！');
                this.getSessionKey();
            }
        });
    }
    @action getSessionKey(params = {}){
        if (this.sessionKey) {
            const state = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            const {current} = state;
            if (!params.current) {
                params.current = current;
            }
        }
        params.subCompanyId = this.subCompany.id;
        if(params.infoname === undefined){
            params.infoname = this.infoname;
        }
        WeaTools.callApi('/api/portal/mainpage/list', 'POST', params).then(data => {
            // 触发 table 更新
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);
            this.sessionKey = data.sessionkey;
            this.infoname = params.infoname;
        });
    }
    @action changeSubCompany(subCompany){
        this.subCompany = subCompany;
        this.getSessionKey();
    }
    @action onSave(){
        let areaResult = "";
        var chkLockedArr = $("#mainpage input[type='checkbox'][name='chkLocked']");
        $("#mainpage input[type='checkbox'][name='chkUse']").each(function(i){
            let ischecked = $(this).attr('checked') === 'checked' ? '1' : '0';
            let isLockchecked = $(chkLockedArr[i]).attr('checked') === 'checked' ? '1' : '0'; 
            if(i!==0) areaResult += "||";
            areaResult += $(this).val() + "_" + ischecked + "_" + isLockchecked;
        });
        WeaTools.callApi('/api/portal/portalinfo/operate', 'POST', {
            moduleName:'mainpage',
            areaResult,
            method: 'save'
        }).then((data) => {
            if (data.status === 'failed') {
                message.error('保存失败！');
            } else {
                message.success('保存成功！');
                this.getSessionKey();
            }
        });
    }
    @action doDel(ids){
        Modal.confirm({
            title: '信息确认',
            content: '确定要删除吗？',
            onOk: () => {
                WeaTools.callApi('/api/portal/portalinfo/operate', 'POST', {
                    moduleName: 'mainpage',
                    method: 'delhp',
                    hpid: ids,
                }).then((data) => {
                    if (data.status === 'failed') {
                        message.error('删除失败！');
                    } else {
                        message.success('删除成功！');
                        this.getSessionKey();
                    }
                });
            },
        });
    }
    @action deleteRows(){
        let ids = "";
        if(this.sessionKey){
            const { selectedRowKeys } = comsWeaTableStore.state[this.sessionKey.split('_')[0]];
            ids = selectedRowKeys.join(",");
        }
        if (ids) {
            this.doDel(ids);
        } else {
            message.warn('您还没有选择任何数据，无法删除');
        }
    }
}
module.exports = MainPageStore;
