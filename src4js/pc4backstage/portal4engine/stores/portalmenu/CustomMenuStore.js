import {observable, action} from 'mobx';
import {message} from 'antd';
import {WeaTools} from 'ecCom';
import WeaTableMobx from 'comsMobx';
const {comsWeaTableStore} = WeaTableMobx.store;

export default class CustomMenuStore {
    // 加载状态
    @observable loading = false;

    // 列表
    @observable menutype = '2';
    @observable menuname = '';
    @observable subcompany = {id: '0', name: ''};
    @observable sessionkey = '';

    // 新建或另存为
    @observable addDialogVisible = false;
    @observable addData = {};

    // 编辑
    @observable editDialogVisible = false;
    @observable editData = {};

    // 树
    _treeData = [];
    _expandedKeys = ['0'];
    @observable treeData = this._treeData;
    @observable expandedKeys = this._expandedKeys;

    // 编辑自定义菜单树节点
    @observable e4iDialogVisible = false;
    @observable e4iDialogTitle = '';
    @observable e4iData = {};

    // 共享
    @observable shareObj = {};
    @observable shareDialogVisible = false;
    @observable shareDialogTitle = '';
    @observable shareSessionKey = '';

    @action
    getDataList(params = {}) {
        this.loading = true;

        if (this.sessionkey) {
            const state = comsWeaTableStore.state[this.sessionkey.split('_')[0]];
            const {current} = state;
            if (!params.current) {
                params.current = current;
            }
        }

        WeaTools.callApi('/api/portal/custommenumaint/list', 'POST', {
            menutype: this.menutype,
            menuname: this.menuname,
            subcompanyid: this.subcompany.id
        }).then(data => {
            this.sessionkey = data.sessionkey;
            comsWeaTableStore.getDatas(data.sessionkey, params.current || 1);

            this.loading = false;
        });
    }

    @action
    selectSubCompany(subcompany) {
        this.subcompany = subcompany;
        this.getDataList();
    }

    @action
    changeMenuType(type) {
        this.menutype = type;
        this.menuname = '';
        this.getDataList();
    }

    @action
    searchCustomMenu(value) {
        this.menuname = value;
        this.getDataList();
    }

    @action
    addCustomMenu(obj = {}) {
        const {
            id = '0',
            menuname = '',
            menudesc = '',
            menutype = this.menutype,
            subcompanyid = this.subcompany.id,
            subcompanyname = this.subcompany.name,
            saveAs = false
        } = obj;

        this.addDialogVisible = true;
        this.addData = {
            menuname: menuname,
            menudesc: menudesc,
            menutype: menutype,
            orgType: '1',
            subcompanyid: subcompanyid,
            subcompanyname: subcompanyname,
            saveAs: saveAs,
            refmenuid: id
        };
    }

    @action
    addCancel() {
        this.addDialogVisible = false;
        this.addData = {};
    }

    @action
    addChange(field, value) {
        this.addData[field] = value;
    }

    @action
    addSave(hasEdit) {
        WeaTools.callApi('/api/portal/custommenumaint/addsave', 'POST', {
            ...this.addData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();

                if (hasEdit) {
                    this.editCustomMenu(data.custommenu);
                }

                this.addDialogVisible = false;
                this.addData = {};
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    editCustomMenu(obj = {}) {
        WeaTools.callApi('/api/portal/custommenumaint/edit', 'POST', {
            id: obj.id,
            menutype: obj.menutype
        }).then(data => {
            this.editData = data.custommenu;
            this.getTreeData(0, 1);
        });

        this.editDialogVisible = true;
    }

    @action
    editCancel() {
        this.editDialogVisible = false;
        this.editData = {};
        this.treeData = this._treeData;
        this.expandedKeys = this._expandedKeys;
    }

    @action
    editChange(field, value) {
        this.editData[field] = value;
    }

    @action
    editSave() {
        WeaTools.callApi('/api/portal/custommenumaint/editsave', 'POST', {
            ...this.editData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getDataList();
                this.editCancel();
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    deleteCustomMenu(obj) {
        const state = comsWeaTableStore.state[this.sessionkey.split('_')[0]];
        const {selectedRowKeys} = state;

        let ids = '';
        if (obj && obj.id) {
            ids = obj.id;
        } else {
            ids = selectedRowKeys.join(',');
        }

        if (ids) {
            WeaTools.callApi('/api/portal/custommenumaint/delete', 'POST', {
                menutype: this.menutype,
                ids: ids
            }).then(data => {
                if (data.result) {
                    this.getDataList();
                }
            });
        }
    }

    @action
    getTreeData(parentId = 0, isroot = 0, parentIds = null) {
        WeaTools.callApi('/api/portal/custommenumaint/treedata', 'POST', {
            mcId: this.editData.id,
            parentId: parentId,
            isroot: isroot
        }).then(data => {
            let treeData = [...this.treeData];

            if (parentId == 0 && isroot == 1) {
                treeData = data;
            } else {
                const loop = (treeData) => {
                    for (let i = 0, len = treeData.length; i < len; i++) {
                        const item = treeData[i];

                        if (item.id == parentId) {
                            if (data && data.length) {
                                item.isLeaf = false;
                                item.children = data;
                            } else {
                                item.isLeaf = true;
                                item.children = [];
                            }
                            break;
                        } else {
                            const children = item.children;
                            if (children && children.length) {
                                loop(children);
                            }
                        }
                    }
                };
                loop(treeData);
            }

            this.treeData = treeData;
            if (parentIds) {
                this.expandedKeys = parentIds;
            }
        });
    }

    @action
    setExpandedKeys(expandedKeys) {
        this.expandedKeys = expandedKeys;
    }

    @action
    addCustomMenuItem(menu) {
        const {id, mcId} = menu;

        this.e4iDialogVisible = true;
        this.e4iDialogTitle = '添加';
        this.e4iData = {
            id: 0,
            parentId: id,
            menutype: mcId,
            menuname: '',
            linkMode: 'normal',
            menuhref: '',
            menutarget: 'mainFrame'
        };
    }

    @action
    editCustomMenuItem(menu) {
        const {id, mcId} = menu;
        WeaTools.callApi('/api/portal/custommenumaint/editcustommenuitem', 'POST', {
            id: id,
            menutype: mcId
        }).then(data => {
            this.e4iDialogVisible = true;
            this.e4iDialogTitle = '编辑';
            this.e4iData = data;
        });
    }

    @action
    editCustomMenuItemCancel() {
        this.e4iDialogVisible = false;
    }

    @action
    editCustomMenuItemChange(field, value) {
        this.e4iData[field] = value;
    }

    @action
    editCustomMenuItemSave() {
        WeaTools.callApi('/api/portal/custommenumaint/custommenuitemsave', 'POST', {
            ...this.e4iData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.editCustomMenuItemCancel();
                this.getTreeData(this.e4iData.parentId, 0, data.parentIds);
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    deleteCustomMenuItem(menu) {
        const {id, mcId, parentId} = menu;

        WeaTools.callApi('/api/portal/custommenumaint/deletecustommenuitem', 'POST', {
            id: id,
            menutype: mcId
        }).then(data => {
            if (data.result) {
                message.success('删除成功！');
                this.getTreeData(parentId, data.parentIds);
            } else {
                message.warning('删除失败！');
            }
        });
    }

    @action
    getShareList(shareObj) {
        const {id, name, mcId} = shareObj;

        this.shareObj = {id: id, name: name, mcId: mcId, infoid: id, customid: mcId};
        this.shareDialogVisible = true;
        this.shareDialogTitle = '使用限制:' + name;

        WeaTools.callApi('/api/portal/portalmenushare/list', 'POST', {
            menuType: 'custom',
            resourceId: '1',
            resourceType: '3',
            infoid: id,
            customid: mcId
        }).then(data => {
            this.shareSessionKey = data.sessionkey;
        });
    }

    @action
    shareCancel() {
        this.shareDialogVisible = false;
    }

    @action
    shareSave(shareObj, shareData, callback) {
        WeaTools.callApi('/api/portal/portalmenushare/save', 'POST', {
            menuType: 'custom',
            resourceId: '1',
            resourceType: '3',
            ...shareObj,
            ...shareData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
                this.getShareList(shareObj);
                callback();
            } else {
                message.warning('保存失败！');
            }
        });
    }

    @action
    shareDelete(shareObj, ids) {
        if (ids) {
            WeaTools.callApi('/api/portal/portalmenushare/delete', 'POST', {
                menuType: this.menuType,
                resourceId: this.resourceId,
                resourceType: this.resourceType,
                ids: ids
            }).then(data => {
                if (data.result) {
                    this.getShareList(shareObj);
                }
            });
        }
    }
}
