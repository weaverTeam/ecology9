// 门户菜单
import PortalMenuStore from'./PortalMenuStore';
// 自定义菜单
import CustomMenuStore from'./CustomMenuStore';

const portalMenuStore = new PortalMenuStore();
const portalCustomMenuStore = new CustomMenuStore();

module.exports = {
    portalMenuStore,
    portalCustomMenuStore
};
