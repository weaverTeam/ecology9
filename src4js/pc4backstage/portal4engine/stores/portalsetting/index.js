import PortalSettingStore from'./PortalSettingStore';

const portalSettingStore = new PortalSettingStore();

module.exports = {
    portalSettingStore
};
