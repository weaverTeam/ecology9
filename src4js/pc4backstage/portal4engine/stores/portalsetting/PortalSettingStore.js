import {observable, action} from 'mobx';
import {message} from 'antd';
import {WeaTools} from 'ecCom';

export default class PortalSettingStore {
    // 加载状态
    @observable loading = false;

    // 设置
    @observable settingData = {};

    @action
    getSettingData() {
        this.loading = true;

        WeaTools.callApi('/api/portal/portalsetting/list', 'POST', {}).then(data => {
            this.settingData = data.settingData;

            this.loading = false;
        });
    }

    @action
    settingChange(field, value) {
        this.settingData[field] = value;
    }

    @action
    editSave() {
        WeaTools.callApi('/api/portal/portalsetting/editsave', 'POST', {
            ...this.settingData
        }).then(data => {
            if (data.result) {
                message.success('保存成功！');
            } else {
                message.warning('保存失败！');
            }
        });
    }
}
