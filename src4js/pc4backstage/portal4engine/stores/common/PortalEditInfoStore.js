import { observable, action } from 'mobx';
import { message } from 'antd';
import { WeaTools } from 'ecCom'; 
class PortalEditInfoStore {
    @observable moduleName = '';
    @observable dialogType = '';
    @observable _store = null;
    @observable dialog = {
        visible: false,
        title:''
    };
    @observable data = null;
    constructor(props) {
        this.handleCancel = this.handleCancel.bind(this);
        this.onPortalInfo = this.onPortalInfo.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.showTableLayout = this.showTableLayout.bind(this);
        this.setFormItemValue = this.setFormItemValue.bind(this);
    }
    @action handleCancel(){
        this.moduleName = '';
        this.dialog.visible = false;
        this.dialog.title = '';
        this.data = null;
        this.dialogType = '';
    }
    @action onPortalInfo(storeInfo = {}, params = {}){
        const { moduleName, _store, method } = storeInfo;
        WeaTools.callApi('/api/portal/portalinfo/get', 'POST', params).then((data) => {
            if (data.status === 'failed') {
                message.error(data.errormsg);
            } else {
                let title = '';
                switch(method){
                    case 'ref':
                        title = '新建';
                        if(moduleName === 'mainpage'){
                             data.subCompanyId = _store.subCompany.id;
                        }
                        break;
                    case 'savebase':
                        title = '编辑';
                        if(moduleName === 'mainpage'){
                             _store.subCompany = {
                                id: data.subCompanyId,
                                name: data.subCompanyName,
                            }
                        }
                        break;
                     case 'saveNew':
                        title = '另存为';
                        if(moduleName === 'mainpage'){
                            _store.subCompany = {
                                id: data.subCompanyId,
                                name: data.subCompanyName,
                            } 
                        }
                        break;
                }
                this.dialog.title = title;
                this.dialog.visible = true;
                this._store = _store;
                this.moduleName = moduleName;
                this.dialogType = method;
                this.data = data;
                this.showTableLayout(data.layoutid);
            }
        });
    }
    @action handleSubmit(){
        const { hpid, infoname, infodesc, subCompanyId, isuse, isRedirectUrl, islocked, menustyleid, layoutid, txtArea_A = '', txtArea_B = '', txtArea_C = '', txtArea_D = '', styleid, bgcolor } = this.data;
        let txtLayoutFlag = "A,B,C,D";
        switch(layoutid){
            case '1':
                txtLayoutFlag = "A";
                break;
            case '2':
                txtLayoutFlag = "A,B";
                break;
            case '3':
                txtLayoutFlag = "A,B,C";
                break;
        } 
        let params = { hpid, infoname, infodesc, subCompanyId, menustyleid, layoutid, txtArea_A, txtArea_B, txtArea_C, txtArea_D, styleid, bgcolor };
        params.moduleName = this.moduleName;
        params.method = this.dialogType;
        params.isuse = isuse ? '1' : '0';
        params.isRedirectUrl = isRedirectUrl ? '1' : '0';
        params.islocked = islocked ? '1' : '0';
        params.txtOnlyOnSave = true;
        params.txtLayoutFlag = txtLayoutFlag;
        params.checknamemethod = 'checkHomepagename';
        params.name = params.infoname;
        WeaTools.callApi('/api/portal/portalinfo/operate', 'POST', params).then((data) => {
            if (data.status === 'failed') {
                message.error(data.errormsg, 3);
            } else {
                message.success("保存成功");
                this.dialog.title = '编辑';
                this.dialogType = 'savebase';
                this.data.hpid = data.hpid;
                this.data.subCompanyId = data.subCompanyId;
                // 触发 table 更新
                this._store.getSessionKey();
            }
        });
    }
    @action showTableLayout(layoutid){
        var layoutPreview = this.data.layoutPreview[layoutid];
        var layouttable = layoutPreview.layouttable;
        var layouttype = layoutPreview.layouttype;
        var lettersall = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        if(layouttable !== undefined &&  layouttable!=='' && (layouttype === 'design' || layouttype==='sys')){
            var count = 0;
            setTimeout(()=>{
                $("#layoutPreviewContainer").find(".layouttable").find("td").each(function(i){
                   if($(this).css("display") !== 'none'){
                        $(this).html(lettersall[count]);
                        count++;
                   }
                });
            },10);
        }
    }
    @action setFormItemValue(fieldname,value = ''){
        switch(fieldname){
            case 'styleid':
                WeaTools.callApi('/api/portal/portalinfo/stylepreview', 'POST', {styleid: value}).then((data) => {
                    if (data.status === 'failed') {
                        message.error(data.errormsg);
                    } else {
                        this.data.styleid = value;
                        this.data.elementStylePreview = data.html;
                    }
                });
                break;
            case 'layoutid':
                this.data.txtArea_A = '';
                this.data.txtArea_B = '';
                this.data.txtArea_C = '';
                this.data.txtArea_D = '';
                this.data.layoutid = value;
                this.showTableLayout(value);
                break;
            case 'isRedirectUrl':
                this.data.isRedirectUrl = value;
                if(!value){
                    this.showTableLayout(this.data.layoutid);
                }
                break;
            default:
                this.data[fieldname] = value;
                break;
        }
    }
}
module.exports = PortalEditInfoStore;
