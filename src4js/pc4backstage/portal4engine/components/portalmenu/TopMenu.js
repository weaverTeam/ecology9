import React from 'react';

import PortalMenu from './PortalMenu';

const TopMenu = () => {
    return (
        <PortalMenu type="top"/>
    );
};

export default TopMenu;
