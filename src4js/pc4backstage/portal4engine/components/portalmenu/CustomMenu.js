import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaTop, WeaLeftRightLayout, WeaOrgTree, WeaRightMenu, WeaTab} from 'ecCom';
import {WeaTable} from 'comsMobx';

import CustomMenuAdd from './CustomMenuAdd';
import CustomMenuEdit from './CustomMenuEdit';

@inject('portalCustomMenuStore')
@observer
class CustomMenu extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSubCompanyClick = this.onSubCompanyClick.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentWillMount() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.getDataList();
    }

    render() {
        const {portalCustomMenuStore} = this.props;
        const {loading, menutype, menuname, sessionkey} = portalCustomMenuStore;

        return (
            <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                <WeaTop
                    title="自定义菜单"
                    icon={<i className='icon-coms-portal'/>}
                    iconBgcolor='#1a57a0'
                    loading={loading}
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenus()}
                    onDropMenuClick={this.onRightMenuClick}
                >
                    <CustomMenuAdd/>
                    <CustomMenuEdit/>
                    <WeaLeftRightLayout
                        leftCom={<WeaOrgTree treeNodeClick={this.onSubCompanyClick}/>}
                        leftWidth="220"
                        defaultShowLeft={true}
                        children={
                            <div>
                                <WeaTab
                                    datas={[{key: '2', title: '登陆后菜单'}, {key: '1', title: '登录前菜单'}]}
                                    keyParam='key'
                                    selectedKey={menutype}
                                    onChange={this.onTabChange}
                                    searchType={['base']}
                                    searchsBaseValue={menuname}
                                    onSearch={this.onSearch}
                                />
                                <WeaTable sessionkey={sessionkey} onOperatesClick={this.onOperatesClick}/>
                            </div>
                        }
                    />
                </WeaTop>
            </WeaRightMenu>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onAdd}>新建</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onDelete}>批量删除</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '新建'});
        rightMenus.push({disabled: false, icon: <Icon type="delete"/>, content: '批量删除'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onAdd();
        } else if (key == '1') {
            this.onDelete();
        }
    }

    onSubCompanyClick(e) {
        const node = e.selectedNodes[0];
        const {id, name} = node.props;

        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.selectSubCompany({id: id, name: name});
    }

    onTabChange(key) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.changeMenuType(key);
    }

    onSearch(value) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.searchCustomMenu(value);
    }

    onOperatesClick(record, index, operate, flag) {
        const {portalCustomMenuStore} = this.props;
        if (operate.index == '0') { // 编辑
            portalCustomMenuStore.editCustomMenu(record, false);

        } else if (operate.index == '1') { // 另存为
            record.saveAs = true;
            portalCustomMenuStore.addCustomMenu(record);

        } else if (operate.index == '2') { // 删除
            portalCustomMenuStore.deleteCustomMenu(record);
        }
    }

    onAdd() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.addCustomMenu();
    }

    onDelete() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.deleteCustomMenu();
    }
}

export default CustomMenu;
