import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Switch} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalMenuStore')
@observer
class PortalMenuEdit4System extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStore} = this.props;
        const {e4sDialogVisible, e4sDialogTitle, e4sData} = portalMenuStore;
        const {customNameLanguageId, topNameLanguageId} = e4sData;

        let customNameInput = '';
        if (customNameLanguageId == '7') {
            customNameInput = <WeaInput value={e4sData.customName} onChange={value => this.onChange('customName', value)}/>;
        } else if (customNameLanguageId == '8') {
            customNameInput = <WeaInput value={e4sData.customName_e} onChange={value => this.onChange('customName_e', value)}/>;
        } else if (customNameLanguageId == '9') {
            customNameInput = <WeaInput value={e4sData.customName_t} onChange={value => this.onChange('customName_t', value)}/>;
        }

        let topNameInput = '';
        if (topNameLanguageId == '7') {
            topNameInput = <WeaInput value={e4sData.topmenuname} onChange={value => this.onChange('topmenuname', value)}/>;
        } else if (topNameLanguageId == '8') {
            topNameInput = <WeaInput value={e4sData.topname_e} onChange={value => this.onChange('topname_e', value)}/>;
        } else if (topNameLanguageId == '9') {
            topNameInput = <WeaInput value={e4sData.topname_t} onChange={value => this.onChange('topname_t', value)}/>;
        }

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={e4sDialogVisible}
                title={e4sDialogTitle}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 680, height: 376}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="菜单系统默认名称">
                            {e4sData.defaultName}
                        </PortalComFormItem>
                        <PortalComFormItem label="打开位置">
                            <WeaSelect
                                options={[
                                    {key: '', selected: false, showname: '默认窗口'},
                                    {key: '_blank', selected: false, showname: '弹出窗口'}
                                ]}
                                value={e4sData.baseTarget}
                                onChange={value => this.onChange('baseTarget', value)}
                            />
                        </PortalComFormItem>
                        <PortalComFormItem label="菜单自定义名称">
                            <Row gutter={16}>
                                <Col span={6}>
                                    <WeaSelect
                                        options={[
                                            {key: '7', selected: false, showname: '中文'},
                                            {key: '8', selected: false, showname: '英文'},
                                            {key: '9', selected: false, showname: '繁体'}
                                        ]}
                                        value={customNameLanguageId}
                                        onChange={value => this.onChange('customNameLanguageId', value)}
                                    />
                                </Col>
                                <Col span={18}>
                                    {customNameInput}
                                </Col>
                            </Row>
                        </PortalComFormItem>
                        <PortalComFormItem label="顶部显示简称">
                            <Row gutter={16}>
                                <Col span={6}>
                                    <WeaSelect
                                        options={[
                                            {key: '7', selected: false, showname: '中文'},
                                            {key: '8', selected: false, showname: '英文'},
                                            {key: '9', selected: false, showname: '繁体'}
                                        ]}
                                        value={topNameLanguageId}
                                        onChange={value => this.onChange('topNameLanguageId', value)}
                                    />
                                </Col>
                                <Col span={18}>
                                    {topNameInput}
                                </Col>
                            </Row>
                        </PortalComFormItem>
                        <PortalComFormItem label="是否使用自定义名称">
                            <Switch
                                style={{marginBottom: '3px'}}
                                checked={e4sData.useCustomName}
                                onChange={checked => this.onChange('useCustomName', checked)}
                            />
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalMenuStore} = this.props;
        portalMenuStore.editSystemMenuChange(field, value);
    }

    onSave() {
        const {portalMenuStore} = this.props;
        portalMenuStore.editSystemMenuSave();
    }

    onCancel() {
        const {portalMenuStore} = this.props;
        portalMenuStore.editSystemMenuCancel();
    }
}

export default PortalMenuEdit4System;
