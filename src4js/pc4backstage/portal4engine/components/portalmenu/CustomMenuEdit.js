import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

import CustomMenuTree from './CustomMenuTree';

@inject('portalCustomMenuStore')
@observer
class CustomMenuEdit extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalCustomMenuStore} = this.props;
        const {editDialogVisible, editData} = portalCustomMenuStore;
        const {menuname, menudesc, menutype, orgType, subcompanyid, subcompanyname} = editData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={editDialogVisible}
                title="编辑菜单"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 680, height: 540}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="菜单名称">
                            <WeaInput value={menuname} onChange={value => this.onChange('menuname', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="菜单描述">
                            <WeaInput value={menudesc} onChange={value => this.onChange('menudesc', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="菜单类型">
                            <WeaSelect
                                disabled={false}
                                options={[
                                    {key: '1', selected: false, showname: '登录前菜单'},
                                    {key: '2', selected: false, showname: '登录后菜单'}
                                ]}
                                value={menutype}
                                onChange={value => this.onChange('menutype', value)}
                            />
                        </PortalComFormItem>
                        {
                            menutype == '2' ?
                                <PortalComFormItem label="所属机构">
                                    <Row gutter={16}>
                                        <Col span={8}>
                                            <WeaSelect
                                                style={{width: '100%'}}
                                                disabled={false}
                                                options={[
                                                    {key: '1', selected: false, showname: '指定机构'},
                                                    {key: '0', selected: false, showname: '所有机构公用'}
                                                ]}
                                                value={orgType}
                                                onChange={value => this.onChange('orgType', value)}
                                            />
                                        </Col>
                                        <Col span={16}>
                                            { orgType == '1' ?
                                                <WeaBrowser
                                                    type={194}
                                                    isSingle={true}
                                                    title="分部"
                                                    tabs={[
                                                        {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                                                        {key: '2', name: '按组织架构', selected: false}
                                                    ]}
                                                    replaceDatas={[{id: subcompanyid, name: subcompanyname}]}
                                                    onChange={value => this.onChange('subcompanyid', value)}
                                                /> : ''
                                            }
                                        </Col>
                                    </Row>
                                </PortalComFormItem> : ''
                        }
                        <PortalComFormItem label="菜单内容">
                            <CustomMenuTree />
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={() => this.onSave()}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editChange(field, value);
    }

    onSave() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editSave();
    }

    onCancel() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editCancel();
    }
}

export default CustomMenuEdit;
