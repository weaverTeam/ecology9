import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaTop, WeaLeftRightLayout, WeaOrgTree, WeaRightMenu, WeaScroll} from 'ecCom';

import './css/index';
import PortalMenuTree from './PortalMenuTree';
import PortalMenuEdit4System from './PortalMenuEdit4System';
import PortalMenuEdit4Custom from './PortalMenuEdit4Custom';
import PortalMenuSync from './PortalMenuSync';

import PortalComShareList from '../common/PortalComShareList';

@inject('portalMenuStore')
@observer
class PortalMenu extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSubCompanyClick = this.onSubCompanyClick.bind(this);
        this.onSync = this.onSync.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onSave4Share = this.onSave4Share.bind(this);
        this.onDelete4Share = this.onDelete4Share.bind(this);
        this.onCancel4Share = this.onCancel4Share.bind(this);
    }

    componentWillMount() {
        const {portalMenuStore, type} = this.props;
        portalMenuStore.init({menuType: type});
    }

    render() {
        const {portalMenuStore} = this.props;
        const {shareObj, shareDialogVisible, shareDialogTitle, shareSessionKey} = portalMenuStore;

        return (
            <WeaRightMenu datas={this.getRightMenus()} width={200} onClick={this.onRightMenuClick}>
                <WeaTop
                    title={`${this.props.type == 'left' ? '前端' : '后端'}菜单`}
                    icon={<i className='icon-coms-portal'/>}
                    iconBgcolor='#1a57a0'
                    loading={false}
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenus()}
                    onDropMenuClick={this.onRightMenuClick}
                >
                    <PortalMenuSync />
                    <PortalMenuEdit4System />
                    <PortalMenuEdit4Custom />
                    <PortalComShareList
                        shareObj={shareObj}
                        visible={shareDialogVisible}
                        title={shareDialogTitle}
                        sessionkey={shareSessionKey}
                        onSave={this.onSave4Share}
                        onDelete={this.onDelete4Share}
                        onCancel={this.onCancel4Share}
                    />
                    <WeaLeftRightLayout
                        leftCom={<WeaOrgTree treeNodeClick={this.onSubCompanyClick}/>}
                        leftWidth="220"
                        defaultShowLeft={true}
                        children={
                            <WeaScroll className="portal-com-scroll" typeClass="scrollbar-macosx">
                                <PortalMenuTree />
                            </WeaScroll>
                        }
                    />
                </WeaTop>
            </WeaRightMenu>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onSync}>将当前设置应用到</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onAdd}>添加</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onSave}>保存</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="share-alt"/>, content: '将当前设置应用到'});
        rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '添加'});
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSync();
        } else if (key == '1') {
            this.onAdd();
        } else if (key == '2') {
            this.onSave();
        }
    }

    onSubCompanyClick(e) {
        const node = e.selectedNodes[0];
        const {id, name} = node.props;

        const {portalMenuStore} = this.props;
        portalMenuStore.selectSubCompany({subCompanyId: id, subCompanyName: name});
    }

    onSync() {
        const {portalMenuStore} = this.props;
        portalMenuStore.syncPortalMenu();
    }

    onAdd() {
        const {portalMenuStore} = this.props;
        portalMenuStore.addCustomMenu({to: 'subCompany'});
    }

    onSave() {
        const {portalMenuStore} = this.props;
        portalMenuStore.treeSave();
    }

    onSave4Share(shareObj, shareData, callback) {
        const {portalMenuStore} = this.props;
        portalMenuStore.shareSave(shareObj, shareData, callback);
    }

    onDelete4Share(shareObj, ids) {
        const {portalMenuStore} = this.props;
        portalMenuStore.shareDelete(shareObj, ids);
    }

    onCancel4Share() {
        const {portalMenuStore} = this.props;
        portalMenuStore.shareCancel();
    }
}

export default PortalMenu;
