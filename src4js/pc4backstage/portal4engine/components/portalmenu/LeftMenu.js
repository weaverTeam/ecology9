import React from 'react';

import PortalMenu from './PortalMenu';

const LeftMenu = () => {
    return (
        <PortalMenu type="left"/>
    );
};

export default LeftMenu;
