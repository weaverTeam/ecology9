import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Checkbox} from 'antd';
import {WeaDialog, WeaRightMenu, WeaSelect, WeaBrowser} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalMenuStore')
@observer
class PortalMenuSync extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStore} = this.props;
        const {syncDialogVisible, syncData} = portalMenuStore;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={syncDialogVisible}
                title="同步菜单设置"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 480, height: 228}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="同步分部">
                            <WeaBrowser
                                type={194}
                                title="分部"
                                tabs={[
                                    {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                                    {key: '2', name: '按组织架构', selected: false}
                                ]}
                                isSingle={false}
                                onChange={value => this.onChange('subCompanyIds', value)}
                            />
                        </PortalComFormItem>
                        <PortalComFormItem label="同步选项">
                            <Row style={{lineHeight: '28px'}}><Col><Checkbox checked={syncData.syncOrder} onChange={e => this.onChange('syncOrder', e.target.checked)}>同步显示顺序</Checkbox></Col></Row>
                            <Row style={{lineHeight: '28px'}}><Col><Checkbox checked={syncData.syncChecked} onChange={e => this.onChange('syncChecked', e.target.checked)}>同步是否隐藏</Checkbox></Col></Row>
                            <Row style={{lineHeight: '28px'}}><Col><Checkbox checked={syncData.syncNewMenu} onChange={e => this.onChange('syncNewMenu', e.target.checked)}>同步新增菜单</Checkbox></Col></Row>
                            <Row gutter={16} style={{lineHeight: '28px'}}>
                                <Col span={12}>
                                    <Checkbox checked={syncData.syncRight} onChange={e => this.onChange('syncRight', e.target.checked)}>同步使用限制权限</Checkbox>
                                </Col>
                                <Col span={12}>
                                    <WeaSelect
                                        options={[
                                            {key: '1', selected: false, showname: '追加'},
                                            {key: '2', selected: false, showname: '替换'}
                                        ]}
                                        value={syncData.syncRightType}
                                        onChange={value => this.onChange('syncRightType', value)}
                                    />
                                </Col>
                            </Row>
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>同步</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '同步'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalMenuStore} = this.props;
        portalMenuStore.syncChange(field, value);
    }

    onSave() {
        const {portalMenuStore} = this.props;
        portalMenuStore.syncSave();
    }

    onCancel() {
        const {portalMenuStore} = this.props;
        portalMenuStore.syncCancel();
    }
}

export default PortalMenuSync;
