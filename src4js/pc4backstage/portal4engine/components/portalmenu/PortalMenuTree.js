import React from 'react';
import {inject, observer} from 'mobx-react';
import {Tree, Icon, Modal, Spin} from 'antd';
const TreeNode = Tree.TreeNode;
const confirm = Modal.confirm;

@inject('portalMenuStore')
@observer
class PortalMenuTree extends React.Component {
    constructor(props) {
        super(props);
        this.onLoadData = this.onLoadData.bind(this);
        this.onExpand = this.onExpand.bind(this);
        this.onCheck = this.onCheck.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onShare = this.onShare.bind(this);
        this.onSync = this.onSync.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    render() {
        const {portalMenuStore} = this.props;
        const {loading, resourceId, resourceType, treeData, checkedKeys, halfCheckedKeys, expandedKeys} = portalMenuStore;

        if (treeData && treeData.length) {
            return (
                <Spin size="large" spinning={loading}>
                    <Tree
                        style={{paddingLeft: '20px'}}
                        className="portal-p4e-pm-tree"
                        checkable={true}
                        checkStrictly={true}
                        draggable={false}
                        expandedKeys={expandedKeys}
                        checkedKeys={{checked: checkedKeys, halfChecked: halfCheckedKeys}}
                        loadData={this.onLoadData}
                        onExpand={this.onExpand}
                        onCheck={this.onCheck}
                        onDrop={this.onDrop}
                    >
                        {this.getTreeNodes(resourceId, resourceType, treeData)}
                    </Tree>
                </Spin>
            );
        } else {
            return (
                <Spin className="portal-com-spin" size="large"/>
            );
        }
    }

    onLoadData(treeNode) {
        const id = treeNode.props.eventKey;

        const {portalMenuStore} = this.props;
        portalMenuStore.getTreeData(id);
    }

    onExpand(expandedKeys) {
        const {portalMenuStore} = this.props;
        portalMenuStore.setExpandedKeys(expandedKeys);
    }

    onCheck(checkedKeys, e) {
        const {portalMenuStore} = this.props;
        portalMenuStore.setCheckedKeys(checkedKeys, e.checked, e.node.props.eventKey);
    }

    onDrop(info) {
        console.log(info);
    }

    onTreeNodeToggle(key, display) {
        document.querySelector(`.portal-p4e-pm-tno-${key}`).style.display = display;
    }

    onAdd(menu) {
        const {portalMenuStore} = this.props;
        portalMenuStore.addCustomMenu({to: 'menu', menu: menu});
    }

    onEdit(menu) {
        const {portalMenuStore} = this.props;
        const {id} = menu;

        if (id < 0) {
            portalMenuStore.editCustomMenu(id);
        } else {
            portalMenuStore.editSystemMenu(id);
        }
    }

    onShare(menu) {
        const {portalMenuStore} = this.props;
        portalMenuStore.getShareList(menu);
    }

    onSync(menu) {
        const {portalMenuStore} = this.props;
        portalMenuStore.syncPortalMenu(menu);
    }

    onDelete(menu) {
        confirm({
            title: '信息确认',
            content: '确定要删除吗？',
            onOk: () => {
                const {id, parentId, canEdit, isParent, isReferedParent, hasReferedToSub} = menu;

                let isall = 0;

                if (canEdit) {
                    isall += 4;
                }

                const hasSubMenu = (!canEdit && isParent) || (canEdit && isReferedParent);
                const subCompanyHasMenu = canEdit && hasReferedToSub;

                if (hasSubMenu || subCompanyHasMenu) {
                    if (hasSubMenu && subCompanyHasMenu) {
                        confirm({
                            title: '信息确认',
                            content: '删除此菜单将会删除此菜单的所有下级菜单，是否继续？',
                            onOk: () => {
                                if (canEdit) {
                                    isall += 1;
                                }

                                confirm({
                                    title: '信息确认',
                                    content: '删除此菜单将会删除所有下级分部的此菜单，是否继续？',
                                    onOk: () => {
                                        isall += 2;

                                        const {portalMenuStore} = this.props;
                                        portalMenuStore.deleteCustomMenu(id, parentId, isall);
                                    }
                                });
                            }
                        });
                    } else if (hasSubMenu) {
                        confirm({
                            title: '信息确认',
                            content: '删除此菜单将会删除此菜单的所有下级菜单，是否继续？',
                            onOk: () => {
                                if (canEdit) {
                                    isall += 1;
                                }

                                const {portalMenuStore} = this.props;
                                portalMenuStore.deleteCustomMenu(id, parentId, isall);
                            }
                        });
                    } else {
                        confirm({
                            title: '信息确认',
                            content: '删除此菜单将会删除所有下级分部的此菜单，是否继续？',
                            onOk: () => {
                                isall += 2;

                                const {portalMenuStore} = this.props;
                                portalMenuStore.deleteCustomMenu(id, parentId, isall);
                            }
                        });
                    }
                } else {
                    const {portalMenuStore} = this.props;
                    portalMenuStore.deleteCustomMenu(id, parentId, isall);
                }
            }
        });
    }

    getTreeNodes(resourceId, resourceType, treeData) {
        // 总部 z* 分部 s* 个人 r*
        let ownerid = '';
        if (resourceType == 1) {
            ownerid = 'z' + resourceId;
        } else if (resourceType == 2) {
            ownerid = 's' + resourceId;
        } else if (resourceType == 3) {
            ownerid = 'r' + resourceId;
        }

        const loop = (data) => {
            return data.map((item) => {
                let addIcon = '', editIcon = '', deleteIcon = '', shareIcon = '', syncIcon = '', linkIcon = '';
                if (item.canEdit) {
                    addIcon = <span className="portal-p4e-pm-tno-i" title="添加" onClick={() => this.onAdd(item)}><Icon type="plus"/></span>;
                    editIcon = <span className="portal-p4e-pm-tno-i" title="编辑" onClick={() => this.onEdit(item)}><Icon type="edit"/></span>;
                    if (item.id < 0) {
                        deleteIcon = <span className="portal-p4e-pm-tno-i" title="删除" onClick={() => this.onDelete(item)}><Icon type="delete"/></span>;
                    }
                    shareIcon = <span className="portal-p4e-pm-tno-i" title="使用限制" onClick={() => this.onShare(item)}><Icon type="lock"/></span>;
                    syncIcon = <span className="portal-p4e-pm-tno-i" title="同步" onClick={() => this.onSync(item)}><Icon type="swap"/></span>;
                } else {
                    addIcon = <span className="portal-p4e-pm-tno-i" title="添加" onClick={() => this.onAdd(item)}><Icon type="plus"/></span>;
                    if (item.ownerid == ownerid) {
                        deleteIcon = <span className="portal-p4e-pm-tno-i" title="删除" onClick={() => this.onDelete(item)}><Icon type="delete"/></span>;
                    }
                    shareIcon = <span className="portal-p4e-pm-tno-i" title="使用限制" onClick={() => this.onShare(item)}><Icon type="lock"/></span>;
                }
                if (item.linkAddress && item.linkAddress != '#' && item.linkAddress.indexOf('javascript') < 0) {
                    linkIcon = <span className="portal-p4e-pm-tno-i" title="链接地址" onClick={() => window.open(item.linkAddress)}><Icon type="link"/></span>;
                }

                const title = (
                    <div
                        onMouseOver={this.onTreeNodeToggle.bind(this, item.key, 'block')}
                        onMouseOut={this.onTreeNodeToggle.bind(this, item.key, 'none')}
                    >
                        <div className="portal-p4e-pm-tnt">
                            <span>{item.name}</span>
                        </div>
                        <div className={`portal-p4e-pm-tno portal-p4e-pm-tno-${item.key}`}>
                            {addIcon}{editIcon}{deleteIcon}{shareIcon}{syncIcon}{linkIcon}
                        </div>
                    </div>
                );

                const children = item.children;
                if (children && children.length) {
                    return <TreeNode key={item.key} title={title} isLeaf={item.isLeaf}>{loop(children)}</TreeNode>;
                }
                return <TreeNode key={item.key} title={title} isLeaf={item.isLeaf}/>;
            });
        };

        return loop(treeData);
    }
}

export default PortalMenuTree;
