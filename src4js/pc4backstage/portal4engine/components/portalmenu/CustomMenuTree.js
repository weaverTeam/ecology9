import React from 'react';
import {inject, observer} from 'mobx-react';
import {Tree, Icon, Modal, Spin} from 'antd';
const TreeNode = Tree.TreeNode;
const confirm = Modal.confirm;
import {WeaScroll} from 'ecCom';

import CustomMenuItemEdit from './CustomMenuItemEdit';

import PortalComShareList from '../common/PortalComShareList';

@inject('portalCustomMenuStore')
@observer
class CustomMenuTree extends React.Component {
    constructor(props) {
        super(props);
        this.onLoadData = this.onLoadData.bind(this);
        this.onExpand = this.onExpand.bind(this);
        this.onTreeNodeToggle = this.onTreeNodeToggle.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onShare = this.onShare.bind(this);
        this.onSave4Share = this.onSave4Share.bind(this);
        this.onDelete4Share = this.onDelete4Share.bind(this);
        this.onCancel4Share = this.onCancel4Share.bind(this);
    }

    render() {
        const {portalCustomMenuStore} = this.props;
        const {menutype, treeData, expandedKeys} = portalCustomMenuStore;
        const {shareObj, shareDialogVisible, shareDialogTitle, shareSessionKey} = portalCustomMenuStore;

        return (
            <div style={{height: '320px'}}>
                <CustomMenuItemEdit />
                <PortalComShareList
                    shareObj={shareObj}
                    visible={shareDialogVisible}
                    title={shareDialogTitle}
                    sessionkey={shareSessionKey}
                    onSave={this.onSave4Share}
                    onDelete={this.onDelete4Share}
                    onCancel={this.onCancel4Share}
                />
                <WeaScroll className="portal-com-scroll" typeClass="scrollbar-macosx">
                    <Tree
                        className="portal-p4e-cm-tree"
                        checkable={false}
                        draggable={false}
                        expandedKeys={expandedKeys}
                        loadData={this.onLoadData}
                        onExpand={this.onExpand}
                        onDrop={this.onDrop}
                    >
                        {this.getTreeNodes(menutype, treeData)}
                    </Tree>
                </WeaScroll>
            </div>
        );
    }

    onLoadData(treeNode) {
        const id = treeNode.props.eventKey;

        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.getTreeData(id, 0);
    }

    onExpand(expandedKeys) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.setExpandedKeys(expandedKeys);
    }

    onTreeNodeToggle(key, display) {
        document.querySelector(`.portal-p4e-cm-tno-${key}`).style.display = display;
    }

    getTreeNodes(menutype, treeData) {
        const loop = (data) => {
            return data.map((item) => {
                let addIcon = <span className="portal-p4e-cm-tno-i" title="添加" onClick={() => this.onAdd(item)}><Icon type="plus"/></span>;

                let editIcon = '', deleteIcon = '', shareIcon = '', linkIcon = '';
                if (item.id != '0') {
                    editIcon = <span className="portal-p4e-cm-tno-i" title="编辑" onClick={() => this.onEdit(item)}><Icon type="edit"/></span>;
                    if (item.id != '1' && !item.isParent) {
                        deleteIcon = <span className="portal-p4e-cm-tno-i" title="删除" onClick={() => this.onDelete(item)}><Icon type="delete"/></span>;
                    }
                    if (menutype == '2') {
                        shareIcon = <span className="portal-p4e-cm-tno-i" title="使用限制" onClick={() => this.onShare(item)}><Icon type="lock"/></span>;
                    }
                    if (item.linkAddress && item.linkAddress != '#' && item.linkAddress.indexOf('javascript') < 0) {
                        linkIcon = <span className="portal-p4e-cm-tno-i" title="链接地址" onClick={() => window.open(item.linkAddress)}><Icon type="link"/></span>;
                    }
                }

                const title = (
                    <div
                        onMouseOver={() => this.onTreeNodeToggle(item.key, 'block')}
                        onMouseOut={() => this.onTreeNodeToggle(item.key, 'none')}
                    >
                        <div className="portal-p4e-cm-tnt">
                            <span>{item.name}</span>
                        </div>
                        <div className={`portal-p4e-cm-tno portal-p4e-cm-tno-${item.key}`}>
                            {addIcon}{editIcon}{deleteIcon}{shareIcon}{linkIcon}
                        </div>
                    </div>
                );

                const children = item.children;
                if (children && children.length) {
                    return <TreeNode key={item.key} title={title} isLeaf={item.isLeaf}>{loop(children)}</TreeNode>;
                }
                return <TreeNode key={item.key} title={title} isLeaf={item.isLeaf}/>;
            });
        };

        return loop(treeData);
    }

    onAdd(menu) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.addCustomMenuItem(menu);
    }

    onEdit(menu) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editCustomMenuItem(menu);
    }

    onShare(menu) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.getShareList(menu);
    }

    onSave4Share(shareObj, shareData, callback) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.shareSave(shareObj, shareData, callback);
    }

    onDelete4Share(shareObj, ids) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.shareDelete(shareObj, ids);
    }

    onCancel4Share() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.shareCancel();
    }

    onDelete(menu) {
        confirm({
            title: '信息确认',
            content: '确定要删除吗？',
            onOk: () => {
                const {portalCustomMenuStore} = this.props;
                portalCustomMenuStore.deleteCustomMenuItem(menu);
            }
        });
    }
}

export default CustomMenuTree;
