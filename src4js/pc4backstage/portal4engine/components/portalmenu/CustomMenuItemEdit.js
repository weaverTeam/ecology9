import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Tooltip} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalCustomMenuStore')
@observer
class CustomMenuItemEdit extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalCustomMenuStore} = this.props;
        const {e4iDialogVisible, e4iDialogTitle, e4iData} = portalCustomMenuStore;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={e4iDialogVisible}
                title={e4iDialogTitle}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 680, height: 376}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="标题">
                            <WeaInput value={e4iData.menuname} onChange={value => this.onChange('menuname', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="链接地址">
                            <Row gutter={16}>
                                <Col span={6}>
                                    <WeaSelect
                                        options={[
                                            {key: 'normal', selected: false, showname: '普通模式'},
                                            {key: 'advance', selected: false, showname: '高级模式'}
                                        ]}
                                        value={e4iData.linkMode || 'normal'}
                                        onChange={value => this.onChange('linkMode', value)}
                                    />
                                </Col>
                                <Col span={17}>
                                    <WeaInput value={e4iData.menuhref} onChange={value => this.onChange('menuhref', value)}/>
                                </Col>
                                <Col span={1}>
                                    <Tooltip placement="bottom" title="如果是外部地址, 请在地址前加上: http://">
                                        <Icon type="question-circle" style={{float: 'right', height: '28px', lineHeight: '28px'}}/>
                                    </Tooltip>
                                </Col>
                            </Row>
                        </PortalComFormItem>
                        <PortalComFormItem label="打开位置">
                            <WeaSelect
                                options={[
                                    {key: 'mainFrame', selected: false, showname: '默认窗口'},
                                    {key: '_blank', selected: false, showname: '弹出窗口'},
                                    {key: '_parent', selected: false, showname: '父窗口'}
                                ]}
                                value={e4iData.menutarget}
                                onChange={value => this.onChange('menutarget', value)}
                            />
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editCustomMenuItemChange(field, value);
    }

    onSave() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editCustomMenuItemSave();
    }

    onCancel() {
        const {portalCustomMenuStore} = this.props;
        portalCustomMenuStore.editCustomMenuItemCancel();
    }
}

export default CustomMenuItemEdit;
