import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Tooltip} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';
import PortalComMaterialLib from '../common/PortalComMaterialLib';

@inject('portalMenuStore')
@observer
class PortalMenuEdit4Custom extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStore} = this.props;
        const {e4cDialogVisible, e4cDialogTitle, e4cData} = portalMenuStore;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={e4cDialogVisible}
                title={e4cDialogTitle}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 680, height: 376}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="菜单名称">
                            <WeaInput value={e4cData.customName} onChange={value => this.onChange('customName', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="顶部显示简称">
                            <WeaInput value={e4cData.topmenuname} onChange={value => this.onChange('topmenuname', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="链接地址">
                            <Row gutter={16}>
                                <Col span={6}>
                                    <WeaSelect
                                        options={[
                                            {key: 'normal', selected: false, showname: '普通模式'},
                                            {key: 'advance', selected: false, showname: '高级模式'}
                                        ]}
                                        value={e4cData.linkMode}
                                        onChange={value => this.onChange('linkMode', value)}
                                    />
                                </Col>
                                <Col span={17}>
                                    <WeaInput value={e4cData.linkAddress} onChange={value => this.onChange('linkAddress', value)}/>
                                </Col>
                                <Col span={1}>
                                    <Tooltip placement="bottom" title="如果是外部地址, 请在地址前加上: http://">
                                        <Icon type="question-circle" style={{float: 'right', height: '28px', lineHeight: '28px'}}/>
                                    </Tooltip>
                                </Col>
                            </Row>
                        </PortalComFormItem>
                        <PortalComFormItem label="打开位置">
                            <WeaSelect
                                options={[
                                    {key: '', selected: false, showname: '默认窗口'},
                                    {key: '_blank', selected: false, showname: '弹出窗口'}
                                ]}
                                value={e4cData.baseTarget}
                                onChange={value => this.onChange('baseTarget', value)}
                            />
                        </PortalComFormItem>
                        <PortalComFormItem label="菜单图标(16*16)">
                            <PortalComMaterialLib value={e4cData.iconUrl} onChange={value => this.onChange('iconUrl', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="顶部显示图标(32*32)">
                            <PortalComMaterialLib value={e4cData.topIconUrl} onChange={value => this.onChange('topIconUrl', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="同步分部">
                            <Row gutter={16}>
                                <Col span={6}>
                                    <WeaSelect
                                        options={[
                                            {key: '0', selected: false, showname: '不同步'},
                                            {key: '1', selected: false, showname: '同步到下级分部'},
                                            {key: '2', selected: false, showname: '同步到指定分部'}
                                        ]}
                                        value={e4cData.syncType}
                                        onChange={value => this.onChange('syncType', value)}
                                    />
                                </Col>
                                <Col span={18}>
                                    {
                                        e4cData.syncType == '2' ?
                                            <WeaBrowser
                                                type={194}
                                                title="分部"
                                                tabs={[
                                                    {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                                                    {key: '2', name: '按组织架构', selected: false}
                                                ]}
                                                isSingle={false}
                                                onChange={value => this.onChange('subCompanyIds', value)}
                                            /> : ''
                                    }
                                </Col>
                            </Row>
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalMenuStore} = this.props;
        portalMenuStore.editCustomMenuChange(field, value);
    }

    onSave() {
        const {portalMenuStore} = this.props;
        portalMenuStore.editCustomMenuSave();
    }

    onCancel() {
        const {portalMenuStore} = this.props;
        portalMenuStore.editCustomMenuCancel();
    }
}

export default PortalMenuEdit4Custom;
