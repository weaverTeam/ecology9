import React from 'react';
import {Button, Icon} from 'antd';
import {inject, observer} from 'mobx-react';
import {WeaTop, WeaLeftRightLayout, WeaOrgTree, WeaRightMenu} from 'ecCom';
import {WeaTable} from 'comsMobx';

import './css/mainportal';
import MainPortalPreview from './MainPortalPreview';
import MainPortalEdit from './MainPortalEdit';
import MainPortalSaveAs from './MainPortalSaveAs';
import MainPortalEditContent from './MainPortalEditContent';

@inject('mainportal_store')
@inject('comsWeaTableStore')
@observer
class MainPortal extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSubCompanyClick = this.onSubCompanyClick.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentWillMount() {
        const {mainportal_store} = this.props;
        mainportal_store.getSessionKey();
    }

    componentDidMount() {
        // 定义全局函数, e8自定义列中定义的函数
        window.detectTemplateStatus = () => {
        };
        window.detectRadioStatus = (() => {
            let appointMpId = '0';
            let isClear = false;

            return () => {
                let element = window.event.srcElement;
                let value = element.value;

                if (appointMpId == value && !isClear) {
                    element.checked = false;
                    isClear = true;
                } else {
                    element.checked = true;
                    appointMpId = value;
                    isClear = false;
                }
            }
        })();
    }

    onOperatesClick(record, index, operate, flag) {
        const {mainportal_store} = this.props;
        if (operate.index == '0') { // 预览
            mainportal_store.preview({id: record.id});

        } else if (operate.index == '1') { // 编辑
            mainportal_store.edit({id: record.id});

        } else if (operate.index == '2') { // 另存为
            mainportal_store.saveAs({id: record.id, templateName: record.templateName});

        } else if (operate.index == '3') { // 门户维护
            mainportal_store.editContent({id: record.id});

        } else if (operate.index == '4') { // 删除
            mainportal_store.onDelete({id: record.id});
        }
    }

    getButtons() {
        const {mainportal_store} = this.props;
        const {saveBtnDisabled} = mainportal_store;
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onAdd}>新建</Button>);
        buttons.push(<Button type="ghost" disabled={saveBtnDisabled} onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onDelete}>批量删除</Button>);
        return buttons;
    }

    getRightMenus() {
        const {mainportal_store} = this.props;
        const {saveBtnDisabled} = mainportal_store;
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '新建'});
        rightMenus.push({disabled: saveBtnDisabled, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="delete"/>, content: '批量删除'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onAdd();
        } else if (key == '1') {
            this.onSave();
        } else if (key == '2') {
            this.onDelete();
        }
    }

    onSubCompanyClick(e) {
        const {mainportal_store} = this.props;

        const node = e.selectedNodes[0];
        const {id, name} = node.props;

        mainportal_store.changeSubCompany({id: id, name: name});
        mainportal_store.getSessionKey({current: 1}, true);
    }

    onAdd() {
        const {mainportal_store} = this.props;
        mainportal_store.edit();
    }

    onSave() {
        const {mainportal_store} = this.props;
        mainportal_store.listSave();
    }

    onDelete() {
        const {mainportal_store} = this.props;
        mainportal_store.onDelete();
    }

    render() {
        const {mainportal_store} = this.props;
        const {loading, sessionkey} = mainportal_store;

        return (
            <WeaTop
                title={'登录后门户'}
                icon={<i className='icon-coms-portal'/>}
                iconBgcolor='#1a57a0'
                loading={loading}
                buttons={this.getButtons()}
                buttonSpace={10}
                showDropIcon={true}
                dropMenuDatas={this.getRightMenus()}
                onDropMenuClick={this.onRightMenuClick}
            >
                <MainPortalPreview />
                <MainPortalEdit />
                <MainPortalSaveAs />
                <MainPortalEditContent />
                <WeaLeftRightLayout
                    leftCom={<WeaOrgTree treeNodeClick={this.onSubCompanyClick}/>}
                    leftWidth="220"
                    defaultShowLeft={true}
                    children={
                        <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                            <WeaTable sessionkey={sessionkey} onOperatesClick={this.onOperatesClick}/>
                        </WeaRightMenu>
                    }
                />
            </WeaTop>
        );
    }
}

export default MainPortal;
