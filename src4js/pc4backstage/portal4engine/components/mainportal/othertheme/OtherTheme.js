import React from 'react';

class OtherTheme extends React.Component {
    render() {
        const {templatetype, subCompanyId, id, skin} = this.props;

        let src = 'about:blank';
        if (templatetype == 'ecology7') {
            src = `/systeminfo/template/PortalTemplateEditor.jsp?subCompanyId=${subCompanyId}&templateid=${id}&skin=${skin}`;

        } else if (templatetype == 'ecologyBasic') {
            src = `/systeminfo/template/PortalTemplateBasicEditor.jsp?subCompanyId=${subCompanyId}&templateid=${id}&skin=${skin}`;

        } else if (templatetype == 'office') {
            src = `/wui/theme/office/page/main.jsp?templateid=${id}`;
        }

        return (
            <div style={{position: 'relative', width: '100%', height: '100%'}}>
                <iframe src={src} style={{float: 'left', width: '100%', height: '100%', border: 'none'}}></iframe>
            </div>
        );
    }
}

export default OtherTheme;
