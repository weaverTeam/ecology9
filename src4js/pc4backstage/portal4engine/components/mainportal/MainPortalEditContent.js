import React from 'react';
import {Button, Icon} from 'antd';
import {inject, observer} from 'mobx-react';
import {WeaDialog, WeaRightMenu} from 'ecCom';

import ThemeList from './ThemeList';
import ThemeContent from './ThemeContent';

@inject('mainportal_store')
@observer
class MainPortalEditContent extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={true}>预览</Button>);
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="primary" disabled={true}>另存为</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: true, icon: <Icon type="desktop"/>, content: '预览'});
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: true, icon: <Icon type="copy"/>, content: '另存为'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
        } else if (key == '1') {
            this.onSave();
        } else if (key == '2') {
        } else if (key == '3') {
            this.onCancel();
        }
    }

    onSave() {
        const {mainportal_store} = this.props;
        mainportal_store.editContentSave();
    }

    onCancel() {
        const {mainportal_store} = this.props;
        mainportal_store.cancelEditContent();
    }

    render() {
        const {mainportal_store} = this.props;
        const {editContentDialogVisible, mainPortal = {}} = mainportal_store;
        const {templateName = ''} = mainPortal;

        return (
            <WeaDialog
                visible={editContentDialogVisible}
                title={templateName}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                buttons={this.getButtons()}
                style={{width: top.document.body.clientWidth - 50, height: top.document.body.clientHeight - 150}}
                onCancel={this.onCancel}
            >
                <div style={{position: 'relative', width: '100%', height: '100%'}}>
                    <div style={{position: 'absolute', left: 0, width: '181px', height: '100%', borderRight: '1px solid #eaeaea'}}>
                        <ThemeList />
                    </div>
                    <div style={{width: '100%', height: '100%', paddingLeft: '181px'}}>
                        <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                            <ThemeContent />
                        </WeaRightMenu>
                    </div>
                </div>
            </WeaDialog>
        );
    }
}

export default MainPortalEditContent;
