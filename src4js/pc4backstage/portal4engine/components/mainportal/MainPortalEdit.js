import React from 'react';
import {Button, Icon, Form} from 'antd';
const FormItem = Form.Item;
import {inject, observer} from 'mobx-react';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

@inject('mainportal_store')
@observer
class MainPortalEdit extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onChangeOrgType = this.onChangeOrgType.bind(this);
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onSave() {
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.form.resetFields();

            const {mainportal_store} = this.props;
            mainportal_store.editSave(values);
        });
    }

    onCancel() {
        this.props.form.resetFields();

        const {mainportal_store} = this.props;
        mainportal_store.cancelEdit();
    }

    onChangeOrgType(value) {
        const {mainportal_store} = this.props;
        mainportal_store.changeOrgType(value);
    }

    render() {
        const {mainportal_store} = this.props;
        const {editDialogVisible, orgType, subCompany, mainPortal} = mainportal_store;

        const formItemLayout = {
            labelCol: {span: 6},
            wrapperCol: {span: 16}
        };
        const {getFieldProps} = this.props.form;

        // 根据对象中是否存在id, 判断新建还是编辑
        const isEdit = !!mainPortal.id;
        const id = isEdit ? mainPortal.id : '';
        const companyId = isEdit ? mainPortal.companyId : subCompany.id;
        const templateName = isEdit ? mainPortal.templateName : '';
        const templateTitle = isEdit ? mainPortal.templateTitle : '';

        getFieldProps('id', {initialValue: id});
        const companyIdProps = getFieldProps('companyId', {initialValue: companyId});
        const templateNameProps = getFieldProps('templateName', {initialValue: templateName});
        const templateTitleProps = getFieldProps('templateTitle', {initialValue: templateTitle});

        let formItem = <div></div>;
        if (!isEdit) {
            const orgTypeOptions = [
                {key: '1', selected: false, showname: '指定机构'},
                {key: '0', selected: false, showname: '所有机构公用'}
            ];

            let browser = <div></div>;
            if (orgType == '1') {
                browser = (
                    <div style={{marginTop: '5px'}}>
                        <WeaBrowser {...companyIdProps} replaceDatas={[subCompany]} type={194} title="分部" tabs={[{key: '1', name: '按列表', selected: false, dataParams: {list: 1}}, {key: '2', name: '按组织架构', selected: false}]} isSingle={true} linkUrl="/hrm/company/HrmSubCompanyDsp.jsp?id="/>
                    </div>
                );
            }

            formItem = (
                <FormItem {...formItemLayout} label="所属机构">
                    <div style={{width: '120px'}}>
                        <WeaSelect options={orgTypeOptions} value={orgType} onChange={this.onChangeOrgType}/>
                    </div>
                    {browser}
                </FormItem>
            );
        }

        return (
            <WeaDialog
                visible={editDialogVisible}
                title={'编辑'}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                buttons={this.getButtons()}
                style={{width: 520, height: 237}}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <Form horizontal style={{padding: '17px'}}>
                        <FormItem {...formItemLayout} label="模板名称">
                            <WeaInput {...templateNameProps}/>
                        </FormItem>
                        <FormItem {...formItemLayout} label="窗口标题">
                            <WeaInput {...templateTitleProps}/>
                        </FormItem>
                        {formItem}
                    </Form>
                </WeaRightMenu>
            </WeaDialog>
        );
    }
}

MainPortalEdit = Form.create()(MainPortalEdit);

export default MainPortalEdit;
