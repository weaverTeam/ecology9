import React from 'react';
import {inject, observer} from 'mobx-react';

import './css/e8theme';
import E8TopLogo from './E8TopLogo';
import E8TopMenu from './E8TopMenu';
import E8FreqUseMenu from './E8FreqUseMenu';
import E8QuickSearch from './E8QuickSearch';
import E8Toolbar from './E8Toolbar';
import E8LeftMenu from './E8LeftMenu';
import E8Main from './E8Main';

@inject('e8theme_store')
@observer
class E8Theme extends React.Component {
    constructor(props) {
        super(props);
        this.onOrderPortalMenu = this.onOrderPortalMenu.bind(this);
    }

    onOrderPortalMenu() {
        const {e8theme_store} = this.props;
        const {isClickLeftMenu} = e8theme_store;

        if (isClickLeftMenu) { // 解决点击菜单的时候也会弹出页面顺序维护窗口
            e8theme_store.clickLeftMenu(false);
        } else {
            let dialog = new window.top.Dialog();
            dialog.Title = '页面顺序维护';
            dialog.Width = 600;
            dialog.Height = 500;
            dialog.URL = '/homepage/maint/HomepageLocation.jsp';
            dialog.opacity = 0.4;
            dialog.callbackfun = () => {
            };
            dialog.callbackfunc4CloseBtn = () => {
                const {e8theme_store} = this.props;
                e8theme_store.getLeftMenu();
            };
            dialog.show();
        }
    }

    render() {
        const {e8theme_store} = this.props;
        const {e8Theme = null} = e8theme_store;

        if (e8Theme != null) {
            return (
                <div className={`e8theme-container e8theme-color-${e8Theme.cssfile || 'left'}`}>
                    <div className="e8theme-header" style={{backgroundColor: e8Theme.topcolor}}>
                        <E8TopLogo />
                        <E8TopMenu />
                        <E8FreqUseMenu />
                        <E8QuickSearch />
                        <E8Toolbar />
                        <div className="e8theme-clear"></div>
                    </div>
                    <div className="e8theme-content">
                        <div className="e8theme-aside" style={{backgroundColor: e8Theme.leftcolor}} onClick={this.onOrderPortalMenu}>
                            <E8LeftMenu />
                        </div>
                        <div className="e8theme-main">
                            <E8Main />
                        </div>
                        <div className="e8theme-clear"></div>
                    </div>
                </div>
            );
        } else {
            return (
                <div className="e8theme-container"></div>
            );
        }
    }
}

export default E8Theme;
