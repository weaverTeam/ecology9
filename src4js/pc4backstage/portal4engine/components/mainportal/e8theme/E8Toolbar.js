const E8Toolbar = () => {
    return (
        <div className="e8theme-toolbar">
            <div className="e8theme-toolbar-fav"></div>
            <div className="e8theme-toolbar-more"></div>
            <div className="e8theme-toolbar-logout"></div>
        </div>
    );
};

export default E8Toolbar;
