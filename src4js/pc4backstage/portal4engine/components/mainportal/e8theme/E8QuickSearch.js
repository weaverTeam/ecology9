const E8QuickSearch = () => {
    return (
        <div className="e8theme-qs">
            <div className="e8theme-qs-content">
                <div className="e8theme-qs-type">
                    <div className="e8theme-qs-type-name">微搜</div>
                    <div className="e8theme-qs-type-down"></div>
                    <div className="e8theme-clear"></div>
                </div>
                <div className="e8theme-qs-split"></div>
                <div className="e8theme-qs-input">
                    <input type="text" placeholder="请输入关键词搜索"/>
                </div>
                <div className="e8theme-qs-button"></div>
                <div className="e8theme-clear"></div>
            </div>
        </div>
    );
};

export default E8QuickSearch;
