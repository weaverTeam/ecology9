import React from 'react';
import {Tooltip} from 'antd';
import {inject, observer} from 'mobx-react';

@inject('e8theme_store')
@inject('mainportal_store')
@observer
class E8TopLogo extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeTopLogo = this.onChangeTopLogo.bind(this);
    }

    onChangeTopLogo() {
        let dialog = new window.top.Dialog();
        dialog.Title = '图片素材库';
        dialog.Width = top.document.body.clientWidth - 100;
        dialog.Height = top.document.body.clientHeight - 100;
        dialog.URL = '/page/maint/common/CustomResourceMaint.jsp?isDialog=1&?file=none&isSingle=';
        dialog.opacity = 0.4;
        dialog.callbackfunParam = {};
        dialog.callbackfun = (callbackfunParam, datas) => {
            const {mainportal_store} = this.props;
            mainportal_store.changeMainPortal({logo: datas.id});
        };
        dialog.callbackfunc4CloseBtn = () => {
        };
        dialog.show();
    }

    render() {
        const {mainportal_store, e8theme_store} = this.props;
        const {mainPortal = {}} = mainportal_store;
        const {e8Theme = {}} = e8theme_store;

        return (
            <Tooltip placement="top" overlay={<span>Logo : 225*50 (推荐)</span>}>
                <div className="e8theme-top-logo" style={{backgroundColor: e8Theme.logocolor}} onClick={this.onChangeTopLogo}>
                    { mainPortal.logo ? <img src={mainPortal.logo} alt=""/> : <span>e-cology | 前端用户中心</span> }
                </div>
            </Tooltip>
        );
    }
}

export default E8TopLogo;
