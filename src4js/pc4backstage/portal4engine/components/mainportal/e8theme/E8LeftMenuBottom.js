const E8LeftMenuBottom = () => {
    return (
        <div className="e8theme-left-menu-btm">
            <div className="e8theme-left-menu-btm-item e8theme-personal"></div>
            <div className="e8theme-left-menu-btm-item e8theme-password"></div>
            <div className="e8theme-left-menu-btm-item e8theme-skin"></div>
            <div className="e8theme-left-menu-btm-item e8theme-remind"></div>
            <div className="e8theme-toggle" title="收缩"></div>
            <div className="e8theme-toggle2" title="展开"></div>
        </div>
    );
};

export default E8LeftMenuBottom;
