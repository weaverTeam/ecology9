import React from 'react';
import {inject, observer} from 'mobx-react';
import {WeaMenu} from 'ecCom';

import E8LeftMenuTop from './E8LeftMenuTop';
import E8LeftMenuBottom from './E8LeftMenuBottom';

@inject('e8theme_store')
@observer
class E8LeftMenu extends React.Component {
    constructor(props) {
        super(props);
        this.onSelect = this.onSelect.bind(this);
    }

    componentWillMount() {
        const {e8theme_store} = this.props;
        e8theme_store.getLeftMenu();
    }

    componentDidMount() {
        const leftHeight = document.querySelector('.e8theme-aside').clientHeight;
        document.querySelector('#menuScrollWrapper').style.height = (leftHeight - 80 - 48) + 'px';
    }

    onSelect(key, menu, type) {
        const {e8theme_store} = this.props;
        e8theme_store.clickLeftMenu(true, key, menu, type);
    }

    render() {
        const {e8theme_store} = this.props;
        const {e8Theme, leftMenu, selectedLeftMenu} = e8theme_store;
        const selectedKey = selectedLeftMenu ? selectedLeftMenu.id : '';

        return (
            <WeaMenu
                mode="inline"
                needSwitch={false}
                inlineWidth="100%"
                datas={leftMenu}
                defaultSelectedKey={selectedKey}
                onSelect={this.onSelect}
                addonBefore={<E8LeftMenuTop e8Theme={e8Theme}/>}
                addonBeforeHeight="80"
                addonAfter={<E8LeftMenuBottom />}
                addonAfterHeight="48"
            />
        );
    }
}

export default E8LeftMenu;
