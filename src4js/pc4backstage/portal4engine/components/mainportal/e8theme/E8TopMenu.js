import React from 'react';
import {inject, observer} from 'mobx-react';

@inject('e8theme_store')
@inject('mainportal_store')
@observer
class E8TopMenu extends React.Component {
    constructor(props) {
        super(props);
        this.onEditTopMenu = this.onEditTopMenu.bind(this);
        this.onHideTopMoreMenu = this.onHideTopMoreMenu.bind(this);
        this.onClickTopMoreMenuBtn = this.onClickTopMoreMenuBtn.bind(this);
    }

    componentWillMount() {
        const {e8theme_store} = this.props;
        e8theme_store.getTopMenu();
    }

    onEditTopMenu() {
        let dialog = new window.top.Dialog();
        dialog.Title = '前端菜单';
        dialog.Width = 600;
        dialog.Height = 500;
        dialog.URL = '/homepage/maint/HomepageTabs.jsp?_fromURL=hpMenu&type=left&isCustom=false&resourceType=1&resourceId=1';
        dialog.opacity = 0.4;
        dialog.callbackfun = () => {
        };
        dialog.callbackfunc4CloseBtn = () => {
            const {e8theme_store} = this.props;
            e8theme_store.getTopMenu();
        };
        dialog.show();
    }

    onHideTopMoreMenu() {
        const {e8theme_store} = this.props;
        e8theme_store.hideTopMoreMenu();
    }

    onClickTopMoreMenuBtn(e) {
        e.stopPropagation();
        const {e8theme_store} = this.props;
        e8theme_store.topMoreMenuShowToggle();
    }

    render() {
        const {e8theme_store} = this.props;
        const {topMenu = [], isShowTopMoreMenu = false} = e8theme_store;

        let topMoreMenuActiveClass = '';
        if (isShowTopMoreMenu) {
            topMoreMenuActiveClass = 'e8theme-top-more-menu-active';
        }

        return (
            <div className={`e8theme-top-menu ${topMoreMenuActiveClass}`} onClick={this.onEditTopMenu} onMouseLeave={this.onHideTopMoreMenu}>
                <div className="e8theme-top-menu-items">
                    {
                        topMenu.map((item, index) => {
                            return (
                                <div key={index} className="e8theme-top-menu-item">
                                    {item.isportal == '1' ? item.name : item.customTopName}
                                </div>
                            );
                        })
                    }
                    <div className="e8theme-clear"></div>
                </div>
                <div className="e8theme-top-more-menu-btn" onClick={this.onClickTopMoreMenuBtn}></div>
                <div className="e8theme-clear"></div>
            </div>
        );
    }
}

export default E8TopMenu;
