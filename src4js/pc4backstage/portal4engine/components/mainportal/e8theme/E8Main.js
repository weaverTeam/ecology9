import React from 'react';
import {inject, observer} from 'mobx-react';

@inject('e8theme_store')
@observer
class E8Main extends React.Component {
    constructor(props) {
        super(props);
        this.onEditHomepage = this.onEditHomepage.bind(this);
        this.onSetHomepageElement = this.onSetHomepageElement.bind(this);
    }

    onEditHomepage() {
        const {e8theme_store} = this.props;
        const {selectedLeftMenu} = e8theme_store;
        const {hpid, subCompanyId} = selectedLeftMenu;

        let dialog = new window.top.Dialog();
        dialog.Title = '编辑';
        dialog.Width = 900;
        dialog.Height = 600;
        dialog.URL = `/homepage/maint/HomepagePageEdit.jsp?opt=edit&method=savebase&hpid=${hpid}&subCompanyId=${subCompanyId}`;
        dialog.opacity = 0.4;
        dialog.callbackfun = () => {
        };
        dialog.callbackfunc4CloseBtn = () => {
        };
        dialog.show();
    }

    onSetHomepageElement() {
        const {e8theme_store} = this.props;
        const {selectedLeftMenu} = e8theme_store;
        const {hpid, subCompanyId} = selectedLeftMenu;

        let dialog = new window.top.Dialog();
        dialog.Title = '设置元素';
        dialog.Width = top.document.body.clientWidth - 100;
        dialog.Height = top.document.body.clientHeight - 100;
        dialog.URL = `/homepage/maint/HomepageTabs.jsp?_fromURL=ElementSetting&isSetting=true&hpid=${hpid}&from=setElement&pagetype=&opt=edit&subCompanyId=${subCompanyId}`;
        dialog.opacity = 0.4;
        dialog.callbackfun = () => {
        };
        dialog.callbackfunc4CloseBtn = () => {
        };
        dialog.show();
    }

    render() {
        return (
            <div className="e8theme-main-portal-setting">
                <div className="e8theme-main-portal-edit" onClick={this.onEditHomepage}>
                    <span>编辑</span>
                </div>
                <div className="e8theme-main-portal-element" onClick={this.onSetHomepageElement}>
                    <span>设置元素</span>
                </div>
            </div>
        );
    }
}

export default E8Main;
