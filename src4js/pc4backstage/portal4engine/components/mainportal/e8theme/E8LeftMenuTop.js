const E8LeftMenuTop = (e8Theme) => {
    return (
        <div className="e8theme-left-menu-top">
            <div className="e8theme-hrm" style={{backgroundColor: e8Theme.hrmcolor}}><span>系统管理员</span></div>
            <div className="e8theme-top-menu-name"><span>门户</span></div>
        </div>
    );
};

export default E8LeftMenuTop;
