import React from 'react';
import {inject, observer} from 'mobx-react';

import E8Theme from './e8theme/E8Theme';
import OtherTheme from './othertheme/OtherTheme';
import CustomTheme from './customtheme/CustomTheme';

@inject('e8theme_store')
@inject('mainportal_store')
@observer
class ThemeContent extends React.Component {
    render() {
        const {mainportal_store, e8theme_store} = this.props;
        const {subCompanyId, e8ThemeList = [], mainPortal = {}} = mainportal_store;
        const {id = '', templatetype = '', skin = ''} = mainPortal;

        let themeContent = <div></div>;
        if (templatetype == 'ecology8') {
            themeContent = <E8Theme />;
            e8theme_store.getE8Theme(skin, e8ThemeList);

        } else if (templatetype == 'ecology7' || templatetype == 'ecologyBasic' || templatetype == 'office') {
            themeContent = <OtherTheme templatetype={templatetype} subCompanyId={subCompanyId} id={id} skin={skin}/>;

        } else if (templatetype == 'custom') {
            themeContent = <CustomTheme />;
        }

        return (
            <div style={{position: 'relative', width: '100%', height: '100%'}}>
                {themeContent}
            </div>
        );
    }
}

export default ThemeContent;
