import React from 'react';
import {Button, Form, Icon} from 'antd';
const FormItem = Form.Item;
import {inject, observer} from 'mobx-react';
import {WeaDialog, WeaRightMenu, WeaInput} from 'ecCom';

@inject('mainportal_store')
@observer
class MainPortalSaveAs extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onSave() {
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.form.resetFields();

            const {mainportal_store} = this.props;
            mainportal_store.saveAsSave(values);
        });
    }

    onCancel() {
        this.props.form.resetFields();

        const {mainportal_store} = this.props;
        mainportal_store.cancelSaveAs();
    }

    render() {
        const {mainportal_store} = this.props;
        const {saveAsDialogVisible, mainPortal} = mainportal_store;

        const formItemLayout = {
            labelCol: {span: 6},
            wrapperCol: {span: 16}
        };
        const {getFieldProps} = this.props.form;

        getFieldProps('id', {initialValue: mainPortal.id || ''});
        const templateNameProps = getFieldProps('templateName', {initialValue: mainPortal.templateName || ''});

        return (
            <WeaDialog
                visible={saveAsDialogVisible}
                title={'另存为'}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                buttons={this.getButtons()}
                style={{width: 520, height: 237}}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <Form horizontal style={{padding: '17px'}}>
                        <FormItem {...formItemLayout} label="模板名称">
                            <WeaInput {...templateNameProps}/>
                        </FormItem>
                    </Form>
                </WeaRightMenu>
            </WeaDialog>
        );
    }
}

MainPortalSaveAs = Form.create()(MainPortalSaveAs);

export default MainPortalSaveAs;
