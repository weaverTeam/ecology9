import React from 'react';
import {inject, observer} from 'mobx-react';
import {WeaScroll} from 'ecCom';

@inject('mainportal_store')
@observer
class ThemeList extends React.Component {
    onSelect(extendtempletid, templatetype, id) {
        const {mainportal_store} = this.props;
        mainportal_store.changeMainPortal({extendtempletid: extendtempletid, templatetype: templatetype, skin: id});
    }

    render() {
        const {mainportal_store} = this.props;
        const {
            e8ThemeList = [],
            e7ThemeList = [],
            eBasicThemeList = [],
            eOfficeThemeList = [],
            eCustomThemeList = [],
            mainPortal = {}
        } = mainportal_store;
        const {templatetype = '', skin = ''} = mainPortal;

        return (
            <WeaScroll className="portal-com-scroll" typeClass="scrollbar-macosx">
                <div className="portal-p4e-mp-tt">
                    <div className="portal-p4e-mp-ttt">ecology8主题</div>
                    <ul>
                        {
                            e8ThemeList.map((e8Theme, index) => {
                                const activeClass = 'ecology8' == templatetype && e8Theme.id == skin ? 'portal-p4e-mp-ta' : '';
                                return (
                                    <li className={`portal-p4e-mp-ti ${activeClass}`} key={index}>
                                        <div className="portal-p4e-mp-til">
                                            <span className="portal-p4e-mp-tin">{index + 1}</span>
                                        </div>
                                        <div className="portal-p4e-mp-tir" onClick={this.onSelect.bind(this, '0', 'ecology8', e8Theme.id)}>
                                            <div className={`portal-p4e-mp-tit e8theme-color-${e8Theme.cssfile || 'left'}`}>
                                                <div className="portal-p4e-mp-tit-tl" style={{backgroundColor: e8Theme.logocolor}}><span>e-cology | 前端用户中心</span></div>
                                                <div className="portal-p4e-mp-tit-hrm" style={{backgroundColor: e8Theme.hrmcolor}}><span>系统管理员</span></div>
                                                <div className="portal-p4e-mp-tit-tmn" style={{backgroundColor: e8Theme.leftcolor}}><span>门户</span></div>
                                            </div>
                                        </div>
                                        <div className="portal-p4e-mp-clear"></div>
                                    </li>
                                );
                            })
                        }
                    </ul>
                </div>
                <div className="portal-p4e-mp-tt">
                    <div className="portal-p4e-mp-ttt">ecology7主题</div>
                    <ul>
                        {
                            e7ThemeList.map((e7Theme, index) => {
                                const activeClass = 'ecology7' == templatetype && e7Theme.id == skin ? 'portal-p4e-mp-ta' : '';
                                return (
                                    <li className={`portal-p4e-mp-ti ${activeClass}`} key={index}>
                                        <div className="portal-p4e-mp-til">
                                            <span className="portal-p4e-mp-tin">{index + 1}</span>
                                        </div>
                                        <div className="portal-p4e-mp-tir" onClick={this.onSelect.bind(this, '0', 'ecology7', e7Theme.id)}>
                                            <div className="portal-p4e-mp-tit">
                                                <img src={e7Theme.preview} alt=""/>
                                            </div>
                                        </div>
                                        <div className="portal-p4e-mp-clear"></div>
                                    </li>
                                );
                            })
                        }
                    </ul>
                </div>
                <div className="portal-p4e-mp-tt">
                    <div className="portal-p4e-mp-ttt">ecologyBasic主题</div>
                    <ul>
                        {
                            eBasicThemeList.map((eBasicTheme, index) => {
                                const activeClass = 'ecologyBasic' == templatetype && eBasicTheme.id == skin ? 'portal-p4e-mp-ta' : '';
                                return (
                                    <li className={`portal-p4e-mp-ti ${activeClass}`} key={index}>
                                        <div className="portal-p4e-mp-til">
                                            <span className="portal-p4e-mp-tin">{index + 1}</span>
                                        </div>
                                        <div className="portal-p4e-mp-tir" onClick={this.onSelect.bind(this, '0', 'ecologyBasic', eBasicTheme.id)}>
                                            <div className="portal-p4e-mp-tit">
                                                <img src={eBasicTheme.preview} alt=""/>
                                            </div>
                                        </div>
                                        <div className="portal-p4e-mp-clear"></div>
                                    </li>
                                );
                            })
                        }
                    </ul>
                </div>
                {
                    eOfficeThemeList.length > 0 ?
                        <div className="portal-p4e-mp-tt">
                            <div className="portal-p4e-mp-ttt">office主题</div>
                            <ul>
                                {
                                    eOfficeThemeList.map((eOfficeTheme, index) => {
                                        const activeClass = 'office' == templatetype && eOfficeTheme.id == skin ? 'portal-p4e-mp-ta' : '';
                                        return (
                                            <li className={`portal-p4e-mp-ti ${activeClass}`} key={index}>
                                                <div className="portal-p4e-mp-til">
                                                    <span className="portal-p4e-mp-tin">{index + 1}</span>
                                                </div>
                                                <div className="portal-p4e-mp-tir" onClick={this.onSelect.bind(this, '0', 'office', eOfficeTheme.id)}>
                                                    <div className="portal-p4e-mp-tit">
                                                        <img src={eOfficeTheme.preview} alt=""/>
                                                    </div>
                                                </div>
                                                <div className="portal-p4e-mp-clear"></div>
                                            </li>
                                        );
                                    })
                                }
                            </ul>
                        </div> : ''
                }
                <div className="portal-p4e-mp-tt">
                    <div className="portal-p4e-mp-ttt">站点主题</div>
                    <ul>
                        {
                            eCustomThemeList.map((eCustomTheme, index) => {
                                const activeClass = 'custom' == templatetype && eCustomTheme.id == skin ? 'portal-p4e-mp-ta' : '';
                                return (
                                    <li className={`portal-p4e-mp-ti ${activeClass}`} key={index}>
                                        <div className="portal-p4e-mp-til">
                                            <span className="portal-p4e-mp-tin">{index + 1}</span>
                                        </div>
                                        <div className="portal-p4e-mp-tir" onClick={this.onSelect.bind(this, '2', 'custom', eCustomTheme.id)}>
                                            <div className="portal-p4e-mp-tit">
                                                <img src={eCustomTheme.preview} alt=""/>
                                            </div>
                                        </div>
                                        <div className="portal-p4e-mp-clear"></div>
                                    </li>
                                );
                            })
                        }
                    </ul>
                </div>
            </WeaScroll>
        );
    }
}

export default ThemeList;
