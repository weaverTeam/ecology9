import React from 'react';
import {inject, observer} from 'mobx-react';
import {WeaDialog} from 'ecCom';

@inject('mainportal_store')
@observer
class MainPortalPreview extends React.Component {
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
    }

    onCancel() {
        const {mainportal_store} = this.props;
        mainportal_store.cancelPreview();
    }

    render() {
        const {mainportal_store} = this.props;
        const {previewDialogVisible, mainPortal} = mainportal_store;
        const previewUrl = `/systeminfo/template/templatePreview.jsp?id=${mainPortal.id}`;

        return (
            <WeaDialog
                className="portal-com-dialog"
                url={previewUrl}
                visible={previewDialogVisible}
                title={'预览'}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: top.document.body.clientWidth - 10, height: top.document.body.clientHeight - 60}}
                onCancel={this.onCancel}
            >
            </WeaDialog>
        );
    }
}

export default MainPortalPreview;
