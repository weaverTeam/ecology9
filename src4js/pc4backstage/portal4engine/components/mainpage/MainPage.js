import React from 'react';
import { inject, observer } from 'mobx-react';
import { WeaTools, WeaErrorPage, WeaTop, WeaRightMenu, WeaLeftRightLayout, WeaOrgTree, WeaDialog, WeaInputSearch } from 'ecCom';
import { WeaTable } from 'comsMobx';
import { Button } from 'antd';
//门户新建、另存为、编辑弹窗
import { PortalEditInfo, WeaPortalTree } from '../common/';
import PortalComShareList from '../common/PortalComShareList';
import { PortalSetting } from 'weaPortal';
import './css/style.css';
@inject('mainpage_store')
@inject('portaleditinfo_store')
@observer
class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.onPortalInfo = this.onPortalInfo.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSubCompanyClick = this.onSubCompanyClick.bind(this);
        //初始化数据
        props.mainpage_store.getSessionKey();
    }
/*  componentDidUpdate(){
        $("#mainpage input[type='checkbox'][name='chkLocked']").click(function(item, i){
        });
        $("#mainpage input[type='checkbox'][name='chkUse']").click(function(item, i){
        });
    }*/
    onPortalInfo(method, params){
        const { portaleditinfo_store, mainpage_store } = this.props;
        let storeInfo = {
            moduleName:'mainpage', 
            _store: mainpage_store, 
            method
        }
        portaleditinfo_store.onPortalInfo(storeInfo, params)
    }
    onOperatesClick(record,index,operate,flag){
        const { id, subcompanyid } = record;
        const { subCompany, onElementSetting, doDel, doOpenShareDialog, handlePortalPreview } = this.props.mainpage_store;
        switch(operate.index){
            case '0': //设置元素
                try{
                    this.refs.portalsetting.handleOnSetting({
                        hpid: id,
                        subCompanyId: subcompanyid,
                        isSetting: true,
                        from: 'setElement',
                        opt: 'edit',
                    });
                } catch(e){
                    window.console ? console.log('门户设置组件PortalSetting不存在： ',e) : alert('门户设置组件PortalSetting不存在： '+e);
                }
                break;
            case '1': //预览
                try{
                    this.refs.portalsetting.handleOnSetting({
                        hpid: id,
                        subCompanyId: subcompanyid,
                        opt: 'priview',
                        module: 'mainpage',
                    });
                } catch(e){
                    window.console ? console.log('门户设置组件PortalSetting不存在： ',e) : alert('门户设置组件PortalSetting不存在： '+e);
                }
                break;
            case '2': //编辑
                this.onPortalInfo('savebase', { hpid: id, subCompanyId: subcompanyid });
                break;
            case '3': //另存为
                this.onPortalInfo('saveNew', { hpid: id, subCompanyId: subcompanyid });
                break;
            case '4': //删除
                doDel(id);
                break;
            case '5': //维护者
                doOpenShareDialog('maint',record);
                break;
            case '6': //共享范围
                doOpenShareDialog('share',record);
                break;
        }
    }
    getButtons(){
        const { mainpage_store } = this.props;
        const { getSessionKey, subCompany, deleteRows, onSave, doOpenPageOrderDialog } = mainpage_store;
        let btns = [];
        btns.push(<Button type="primary" onClick={this.onPortalInfo.bind(this,'ref', { hpid:0, subCompanyId: subCompany.id } )}>新建</Button>);
        btns.push(<Button onClick={onSave}>保存</Button>);
        btns.push(<Button onClick={deleteRows}>批量删除</Button>);
        btns.push(<Button onClick={doOpenPageOrderDialog}>页面顺序维护</Button>);
        btns.push(<WeaInputSearch placeholder="请输入名称" onSearch={value => getSessionKey({infoname:value})} />);
        return btns;
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Add-to'/>,
            content:'新建'
        });
        btns.push({
            icon: <i className='icon-coms-Journal'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-delete'/>,
            content:'批量删除'
        });
        btns.push({
            icon: <i className='icon-coms-integration-o'/>,
            content:'页面顺序维护'
        });
        return btns
    }
    onRightMenuClick(key){
        const { mainpage_store } = this.props;
        const { subCompany, deleteRows, onSave, doOpenPageOrderDialog } = mainpage_store;
        switch(key){
            case '0': //新建
                this.onPortalInfo('ref', { hpid:0, subCompanyId: subCompany.id } )
                break;
            case '1': //保存
                onSave();
                break;
            case '2': //批量删除
                deleteRows();
                break;
            case '3': //页面顺序维护
                doOpenPageOrderDialog();
                break;
        }
    }
    onSubCompanyClick(e) {
        const { mainpage_store } = this.props;
        const node = e.selectedNodes[0];
        const {id, name} = node.props;
        mainpage_store.changeSubCompany({id: id, name: name});
    }
    render(){
        const { mainpage_store } = this.props;
        const { sessionKey, pageOrderDialog, doSavePageOrder, shareDialog, handlePageOrderCancel,
            handleShareCancel, saveShare, deleteShare } = mainpage_store;
        if(!sessionKey) return <div/>
        return <div id="mainpage">
                <WeaTop title={"登录后页面"}
                            loading={false}
                            icon={<i className='icon-coms-portal' />}
                            iconBgcolor='#1a57a0'
                            buttons={this.getButtons()}
                            buttonSpace={10}
                            showDropIcon={true}
                            dropMenuDatas={this.getRightMenu()}
                            onDropMenuClick={this.onRightMenuClick}>
                       {PortalSetting ? <PortalSetting ref="portalsetting"/> : ''}
                       <PortalEditInfo/>
                       <WeaPortalTree
                            visible={pageOrderDialog.visible}
                            title={pageOrderDialog.title}
                            onSave={doSavePageOrder}
                            onCancel={handlePageOrderCancel}
                            dataUrl={pageOrderDialog.dataUrl}
                       />
                       <PortalComShareList
                            visible={shareDialog.visible}
                            title={shareDialog.title}
                            shareObj={shareDialog.shareObj}
                            sessionkey={shareDialog.sessionKey}
                            onSave={saveShare}
                            onDelete={deleteShare}
                            onCancel={handleShareCancel}
                        />
                       <WeaLeftRightLayout
                            leftCom={<WeaOrgTree treeNodeClick={this.onSubCompanyClick}/>}
                            leftWidth="220"
                            defaultShowLeft={true}>
                            <WeaRightMenu width={210} datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                                <WeaTable sessionkey={sessionKey} onOperatesClick={this.onOperatesClick}/>
                            </WeaRightMenu>
                        </WeaLeftRightLayout>
               </WeaTop>
        </div>
    }

}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
MainPage = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(MainPage);

export default MainPage;
