import { inject, observer } from 'mobx-react';
import { WeaTools, WeaErrorPage, WeaRightMenu, WeaDialog } from 'ecCom';
import { WeaTable } from 'comsMobx';
@inject("portalpagefor_layout_store")
@observer
class PortalPage extends React.Component {
    render(){
        const { sessionKey, title, visible, onCancel } = this.props.portalpagefor_layout_store;
        return <WeaDialog title={title} 
                  visible={visible}
                  closable={true}
                  onCancel={onCancel}
                  icon='icon-coms-portal'
                  iconBgcolor='#1a57a0'
                  maskClosable={false}
                  buttons={[]}
                  style={{width:top.document.body.clientWidth * 0.5,height:top.document.body.clientHeight * 0.6 }}>
                  <WeaRightMenu>
                       {visible ? <WeaTable sessionkey={sessionKey}/> : <div/>}
                    </WeaRightMenu>
            </WeaDialog>
    }

}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
PortalPage = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(PortalPage);

export default PortalPage;