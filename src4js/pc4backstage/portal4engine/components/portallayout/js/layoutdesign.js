/**打开长url的窗口，add by fmj 2015-03-04 start*/

function  readCookie(name){  
   var  cookieValue  =  "7";  
   var  search  =  name  +  "=";
   try{
       if(document.cookie.length  >  0) {    
           offset  =  document.cookie.indexOf(search);  
           if  (offset  !=  -1)  
           {    
               offset  +=  search.length;  
               end  =  document.cookie.indexOf(";",  offset);  
               if  (end  ==  -1)  end  =  document.cookie.length;  
               cookieValue  =  unescape(document.cookie.substring(offset,  end))  
           }  
       }  
   }catch(exception){
   }
   return  cookieValue;  
} 
/**
 * User: 三杰lee
 * Date: 14-10-9
 * Time: 上午10:03
 * To change this template use File | Settings | File Templates.
 */



const layoutdesign = {
    moveflag: false,     //获取选区标识
    resizeflag: false,   //重置大小标识
    resizecontainer: undefined,
    startcoord: '0,0',
    endcoord: '0,0',
    selectionsx: undefined,
    selectionsy: undefined,
    selectionex: undefined,
    selectioney: undefined,
    lang: readCookie("languageidweaver"),
    cellmergeinfo: {},
    lettersall: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'], //所有分块
    layouttablewidth: $(".layouttable").width(),
    dragrange: 5, //可拖拉范围
    initLayoutDesign: function () {
        this.bindMergeEvent();
    },
    //生成选区
    generatorSelection: function (left, top, width, height) {
        var currentcoord=this.selectionsx+","+this.selectionsy;
        var tdtemp=$("td[data-coord='"+currentcoord+"']");
        var imageicon=$(".imageicons");
        var menuleft=left+width/2;
        var menutop= top+height-imageicon.height();
        imageicon.css("top",(menutop-2)+"px");
        var rowspan=tdtemp.attr("rowspan")===undefined?1:tdtemp.attr("rowspan");
        var colspan=tdtemp.attr("colspan")===undefined?1:tdtemp.attr("colspan");
        var totalwidth;
        //可拆分
        if((rowspan>1 || colspan>1) && this.endcoord===this.startcoord && this.cellmergeinfo[currentcoord]!==undefined && this.cellmergeinfo[currentcoord].length>0){
            imageicon.find(".mergeicon").hide();
            imageicon.find(".splitall").show();
            totalwidth=33;
            if(colspan>1)  {
                totalwidth+=33;
                imageicon.find(".vsplit").show();
            }
            else
              imageicon.find(".vsplit").hide();

            if(rowspan>1){
                totalwidth+=33;
                imageicon.find(".hsplit").show();
            }
            else
                imageicon.find(".hsplit").hide();
//                    menuleft=left+width/2-(imageicon.width()-33)/2;
            imageicon.show();
            //可合并
        }else if(this.startcoord!==this.endcoord){
            totalwidth=33;
            imageicon.find(".mergeicon").show();
            imageicon.find(".splitall").hide();
            imageicon.find(".hsplit").hide();
            imageicon.find(".vsplit").hide();
//                    menuleft=left+width/2-(imageicon.width()-33*3)/2;
            imageicon.show();
        }else{
            imageicon.hide();
        }
        imageicon.css("left",(menuleft-totalwidth/2)+"px");

        var wline = $(".wline");
        wline.css("left", left + 'px');
        wline.css("top", top + 'px');
        wline.css("height", height + 'px');
        wline.show();
        var eline = $(".eline");
        eline.css("left", (left + width) + 'px');
        eline.css("top", top + 'px');
        eline.css("height", height + 'px');
        eline.show();
        var nline = $(".nline");
        nline.css("left", left + 'px');
        nline.css("top", top + 'px');
        nline.css("width", width + 'px');
        nline.show();
        var sline = $(".sline");
        sline.css("left", left + 'px');
        sline.css("top", (top + height) + 'px');
        sline.css("width", width + 'px');
        sline.show();


    },
    //计算选区
    caculateSelection: function () {
        var that = this;
        var startxy = this.startcoord.split(",");
        var endxy = this.endcoord.split(",");
        var startx = ~~startxy[0];
        var starty = ~~startxy[1];
        var endx = ~~endxy[0];
        var endy = ~~endxy[1];
        //获取左上角坐标
        var sx = startx <= endx ? startx : endx;
        var sy = starty <= endy ? starty : endy;
        //获取右下角坐标
        var ex = startx <= endx ? endx : startx;
        var ey = starty <= endy ? endy : starty;
        //选区最小坐标和最大坐标
        var minsx = sx, minsy = sy, maxex = ex, maxey = ey;
        var tdtemp;
        var colspan;
        var rowspan;
        for (var i = sx; i <= ex; i++)
            for (var j = sy; j <= ey; j++) {
                tdtemp = $("td[data-coord='" + i + "," + j + "']");
                if (tdtemp.length > 0 && tdtemp.is(":visible")) {
                    //计算最大横坐标
                    colspan = tdtemp.attr("colspan");
                    if (colspan !== undefined && ~~colspan > 1) {
                        if (maxex < (i + ~~colspan - 1)) {
                            maxex = i + ~~colspan - 1;
                        }
                    }
                    //计算最大纵坐标
                    rowspan = tdtemp.attr("rowspan");
                    if (rowspan !== undefined && ~~rowspan > 1) {
                        if (maxey < (j + ~~rowspan - 1)) {
                            maxey = j + ~~rowspan - 1;
                        }
                    }
                }
            }
        var width = 0, height = 0, left, top;
        //计算宽度
        for (var i = minsx; i <= maxex; i++) {
            tdtemp = $("td[data-coord='" + i + "," + minsy + "']");
            if (tdtemp.length > 0 && tdtemp.is(":visible")) {
                width += tdtemp.outerWidth();
            }
        }
        //计算高度
        for (var j = minsy; j <= maxey; j++) {
            tdtemp = $("td[data-coord='" + minsx + "," + j + "']");
            if (tdtemp.length > 0 && tdtemp.is(":visible")) {
                height += tdtemp.outerHeight();
            }
        }

        tdtemp = $("td[data-coord='" + minsx + "," + minsy + "']");
        left = tdtemp.position().left;
        top = tdtemp.position().top;
        //绘制选区
        that.generatorSelection(left, top, width, height);

        this.selectionsx=minsx;
        this.selectionsy=minsy;
        this.selectionex=maxex;
        this.selectioney=maxey;

    },
    //合并单元格
    mergeSelection:function(){
        var that=this;
        var imageicon=$(".imageicons");
        var tdtemp;
        var mergecellitems=[];
        var colspan=~~this.selectionex-~~this.selectionsx+1;
        var rowspan=~~this.selectioney-~~this.selectionsy+1;
        for(var i=this.selectionsx;i<=this.selectionex;i++)
            for(var j=this.selectionsy;j<=this.selectioney;j++){
                if(i!==this.selectionsx  ||  j!==this.selectionsy){
                    tdtemp=$("td[data-coord='"+i+","+j+"']");
                    if(tdtemp.length>0){
                        // tdtemp.remove();
                        tdtemp.hide();
                        tdtemp.attr("rowspan","1");
                        tdtemp.attr("colspan","1");
                        //添加合并节点
                        mergecellitems.push(i+","+j);
                    }
                }
            }
        //缓存合并的节点
        this.cellmergeinfo[this.selectionsx+","+this.selectionsy]=mergecellitems;

        tdtemp= $("td[data-coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdtemp.attr("colspan",colspan);
        tdtemp.attr("rowspan",rowspan);
        //图片默认宽度
        var totalwidth=33;
        imageicon.find(".mergeicon").hide();
        imageicon.find(".splitall").show();
        if(colspan>1){
            totalwidth+=33;
            imageicon.find(".vsplit").show();
        } else
            imageicon.find(".vsplit").hide();
        if(rowspan>1)     {
            totalwidth+=33;
            imageicon.find(".hsplit").show();
        }
        else
        imageicon.find(".hsplit").hide();

      //  imageicon.css("left",(imageicon.position().left-52)+'px');
        imageicon.css("left",(tdtemp.position().left+tdtemp.width()/2-totalwidth/2)+'px');
    },
    //拆分所有
    splitAll:function(){
        var imageicon=$(".imageicons");
        var tdtemp;
        var restoreitems=this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
        for(var i=0;i<restoreitems.length;i++){
            tdtemp=$("td[data-coord='"+restoreitems[i]+"']");
            tdtemp.show();
        }
        tdtemp=$("td[data-coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdtemp.attr("colspan","1");
        tdtemp.attr("rowspan","1");

        tdtemp.trigger("click");

        imageicon.find(".mergeicon").show();
        imageicon.find(".splitall").hide();
        imageicon.find(".hsplit").hide();
        imageicon.find(".vsplit").hide();
        imageicon.css("left",(imageicon.position().left+52)+'px');
        //移除存储的数据
        delete  this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
    },
    //水平拆分
    hsplit:function(){
        var tdtemp,tdfirst;
        var restoreitems=this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
        tdtemp=$("td[data-coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdfirst=tdtemp;
        tdtemp.attr("rowspan","1");
        var colspan= tdtemp.attr("colspan");
        var restorex;
        var restorey;
        for(var i=0;i<restoreitems.length;i++){
            //获取恢复节点的水平坐标
            restorex=restoreitems[i].split(",")[0];
            restorey=restoreitems[i].split(",")[1];
            if(restorex===this.selectionsx || restorex==this.selectionsx){
                tdtemp=$("td[data-coord='"+restoreitems[i]+"']");
                tdtemp.attr("colspan",colspan);
                tdtemp.show();
                //并添加合并节点
                if(colspan!=undefined && colspan>1){
                    this.cellmergeinfo[restoreitems[i]]=[];
                    for(var j=0;j<=colspan-1;j++){
                        this.cellmergeinfo[restoreitems[i]].push((~~restorex+j)+","+restorey);
                    }
                }
            }
        }
        tdfirst.trigger("click");
    },
    //垂直拆分
    vsplit:function(){
        var tdtemp,tdfirst;
        var restoreitems=this.cellmergeinfo[this.selectionsx+","+this.selectionsy];
        tdtemp=$("td[data-coord='"+this.selectionsx+","+this.selectionsy+"']");
        tdfirst=tdtemp;
        tdtemp.attr("colspan","1");
        var rowspan= tdtemp.attr("rowspan");
        var restorey;
        var restorex;
        for(var i=0;i<restoreitems.length;i++){
            //获取恢复节点的水平坐标
            restorex=restoreitems[i].split(",")[0];
            restorey=restoreitems[i].split(",")[1];
            if(restorey===this.selectionsy || restorey==this.selectionsy ){
                tdtemp=$("td[data-coord='"+restoreitems[i]+"']");
                tdtemp.attr("rowspan",rowspan);
                tdtemp.show();
                //添加合并
                if(rowspan!=undefined && rowspan>1){
                    this.cellmergeinfo[restoreitems[i]]=[];
                    for(var j=0;j<=rowspan-1;j++){
                        this.cellmergeinfo[restoreitems[i]].push(restorex+","+(~~restorey+j));
                    }
                }
            }
        }
        tdfirst.trigger("click");
    },
    //获取选区
    bindMergeEvent: function () {
        var that = this;

        $(".layoutcontainer").disableSelection();
       // $(document.body).disableSelection();
        $(".imageicons").disableSelection();

        $(window).resize(function (e) {
            e.stopPropagation();
            that.caculateSelection();
        });

        //合并单元格
        $(".mergeicon").click(function(e){
            that.mergeSelection();
            e.stopPropagation();
        }) ;
        //拆分所有
        $(".splitall").click(function(e){
            that.splitAll();
            e.stopPropagation();
        });

        //横向拆分
        $(".hsplit").click(function(e){
            that.hsplit();
            e.stopPropagation();
        });

        //垂直拆分
        $(".vsplit").click(function(e){
            that.vsplit();
            e.stopPropagation();
        });

        //点击事件
        $(".layouttable").delegate('td', 'click', function (e) {
            var current = $(this);
            var coords=current.attr("data-coord").split(",");
            this.selectionsx=coords[0];
            this.selectionsy=coords[1];
            var position = current.position();
            var left = position.left;
            var top = position.top;
            var width = current.outerWidth();
            var height = current.outerHeight();
            that.generatorSelection(left, top, width, height);
            e.stopPropagation();
        });

        //计算是否呈现可拖拽
        function caculateMoveFlag(dragrange, e){
            //当前的单元格
            var currenttd=$(e.target);
            //边缘单元格不能拖动
            if(currenttd.attr("data-coord")===undefined)
                return;
            var coordx=currenttd.attr("data-coord").split(",")[0];
            var offset=currenttd.offset();
            var left=offset.left+currenttd.width();
            var clientx = e.clientX;
            if(clientx >= left-dragrange  &&  clientx<=left+dragrange && coordx!=='3'){
                $(".layouttable").css("cursor","e-resize");
            }else{
                $(".layouttable").css("cursor","pointer");
            }
        }

        //小数转百分数
        function cacultePci(data){
            data=data+"";
            var datastr=data.substr(2);
            return datastr.substring(0,2)+"."+datastr.substr(2)+"%";
        }

        //计算表头宽度
        function caculateThWidth(e){
            var colspan = this.resizecontainer.attr("colspan");
            var coordx = this.resizecontainer.attr("data-coord").split(",")[0];
            if(colspan!==undefined){
                coordx=~~coordx+~~colspan-1;
            }
            var th=$("th[data-coord='"+coordx+",-1']");
            var thnext=$("th[data-coord='"+(~~coordx+1)+",-1']");
            var distance= e.clientX-(this.resizecontainer.offset().left+this.resizecontainer.width());
            var thwidth=th.width();
            var thnextwidth=thnext.width();
            if(thwidth<5){
                thwidth=10;
                this.resizeflag=false;
            }
            if(thnextwidth<5){
                thnextwidth=10;
                this.resizeflag=false;
            }
            //计算百分比
            th.css("width",cacultePci((thwidth+distance)/this.layouttablewidth));
            thnext.css("width",cacultePci((thnextwidth-distance)/this.layouttablewidth));
        }

        $(".layouttable").on('mousedown', function (e) {
            var currenttable=$(this);
            var mousestyle=currenttable.css("cursor");
            var current = $(e.target);
            //鼠标样式
            if(mousestyle==='e-resize'){
                this.resizeflag=true;
                this.resizecontainer=current;
            }

            //选区开始单元格
            this.startcoord = current.attr("data-coord");
            this.moveflag = true;
            e.stopPropagation();
        });
        $(".layouttable").on('mousemove', function (e) {
            if(this.resizeflag){
                caculateThWidth(e);
                return;
            }else{
                caculateMoveFlag(that.dragrange, e);
            }
            if (this.moveflag) {
                var target = $(e.target);
                this.endcoord = target.attr("data-coord");
                that.caculateSelection();
            }
            e.preventDefault();
            e.stopPropagation();
        });
        $(".layouttable").on('mouseup', function (e) {
            var target = $(e.target);
            this.endcoord = target.attr("data-coord");
            this.moveflag = false;
            if(this.resizeflag!==true)
                that.caculateSelection();
            this.resizeflag=false;
            e.stopPropagation();
        });
        //存储布局文件
    },saveLayout:function(){
        //主要存储表单和合并单元格的信息
        var params={},layouthtml="",tdtemp,tds,layout_dialog;
        params.layoutid=$("input[name='layoutid']").val();
        params.layoutname=$("input[name='layoutname']").val();
        params.layoutdesc=$("input[name='layoutdesc']").val();
        var allowareas=[];
        var allowarea="";
        var tableclone=$(".layouttable").clone();
        tds=tableclone.find("td");
        var count=0;
        for(var i=0;i<tds.length;i++){
            tdtemp=$(tds[i]);
            if(tdtemp.css("display")!=='none'){
                allowarea=this.lettersall[count++];//单元格标识连续
                allowareas.push(allowarea);
                tdtemp.html("${elements_"+allowarea+"}");
            }
        }
        params.layouttemplate=$("<div></div>").append(tableclone).html();
        params.layouttable=$("<div></div>").append($(".layouttable").clone()).html();
        params.cellmergeinfo=JSON.stringify(this.cellmergeinfo);

        if(params.layoutid!=='' && params.layoutid !== undefined){
            var checkparam={};
            checkparam.layoutid=params.layoutid;
            checkparam.allowarea=allowareas.join(",");
            $.ajax({
                data: checkparam,
                type: "POST",
                url: "/page/layoutdesign/pages/savecheck.jsp",
                timeout: 20000,
                dataType: 'json',
                success: function(rs){
                    //未涉及元素的丢失且该布局无相关的门户页面,则直接存储
                    if(rs.islostel==='0'  &&  rs.reapages==='0'){
                        postLayout();
                    }else{
                        var remindmsg=getShowInfo(rs);
                        layout_dialog = new window.top.Dialog();
                        layout_dialog.currentWindow = window;   //传入当前window
                        layout_dialog.Width = 300;
                        layout_dialog.Height = 100;
                        layout_dialog.Modal = true;
                        layout_dialog.Title = SystemEnv.getHtmlNoteName(3621,this.lang);
                        layout_dialog.InnerHtml=remindmsg;
                        layout_dialog.OKEvent=function(){
                            //关闭窗口
                            layout_dialog.close();
                            //存储布局
                            postLayout();

                        }
                        layout_dialog.show();
                        layout_dialog.okButton.value=SystemEnv.getHtmlNoteName(3622,this.lang);
                        bindevent(params.layoutid);
                    }
                },fail:function(){
                    window.top.Dialog.alert(SystemEnv.getHtmlNoteName(3623,this.lang));
                }
            });

        }else{
            //存储布局
            postLayout();
        }
        function bindevent(layoutid){
            var item=$(window.top.document).find(".pagedetail");
            if(item.length===0){
                setTimeout(function(){bindevent(layoutid);},500);
            }else{
                item.on("click",function(){
                    var layout_dialog = new window.top.Dialog();
                    layout_dialog.currentWindow = window;   //传入当前window
                    layout_dialog.Width = 800;
                    layout_dialog.Height = 600;
                    layout_dialog.Modal = true;
                    layout_dialog.maxiumnable=true;
                    layout_dialog.Title = SystemEnv.getHtmlNoteName(3624,this.lang);
                    layout_dialog.URL = "/page/maint/layout/LayoutRelatedHomepage.jsp?method=show&layoutid="+layoutid;
                    layout_dialog.show();
                });
            }

        }

        //获取提示信息
        function  getShowInfo(rs){
            var msg="";
            var reapages=~~rs.reapages;
            var islostel=rs.islostel;
            if(rs.islostel==='1' &&  reapages>0){
                msg="<div style='text-align:center;font-size:12px;padding-top: 40px;font-family: '微软雅黑';'>"+SystemEnv.getHtmlNoteName(3625,this.lang)+" <b><a class='pagedetail' style='color:red;font-size: 14px;' href='#'>"+reapages+"</a></b> "+SystemEnv.getHtmlNoteName(3628,this.lang)+"!</div>";
            }else if(rs.islostel==='1'){
                msg="<div style='text-align:center;font-size:12px;padding-top: 40px;font-family: '微软雅黑';'>"+SystemEnv.getHtmlNoteName(3626,this.lang)+"</div>";
            }else {
                msg="<div style='text-align:center;font-size:12px;padding-top: 40px;font-family: '微软雅黑';'>"+SystemEnv.getHtmlNoteName(3627,this.lang)+" <b><a class='pagedetail' style='color:red;font-size: 14px;' href='#'>"+reapages+"</a></b> "+SystemEnv.getHtmlNoteName(3628,this.lang)+"!</div>";
            }
            return msg;
        }

        function  postLayout(){
            $.ajax({
                data: params,
                type: "POST",
                url: "/page/layoutdesign/pages/saveitems.jsp",
                timeout: 20000,
                dataType: 'json',
                success: function(rs){
                    if(rs.success==='0'){
                        window.top.Dialog.alert(SystemEnv.getHtmlNoteName(3623,this.lang));
                    }else{
                        window.top.Dialog.alert(SystemEnv.getHtmlNoteName(3629,this.lang));
                        if($("input[name='layoutid']").val() == ""){//刷新当前页
                            window.location.href="layoutdesign.jsp?layoutid="+rs.success;
                        }
                        //刷新父界面
                        parent.getParentWindow(window).location.reload();

                    }
                },fail:function(){
                    window.top.Dialog.alert(SystemEnv.getHtmlNoteName(3623,this.lang));
                }
            });
        }
    },
    //恢复布局
    reciverLayout:function(layout){
        var layouttable=$(layout.tableinfo);
        this.cellmergeinfo=layout.cellmergeinfo;
        $(".layouttable").remove();
        $(".layouttablewrapper").append(layouttable);
        this.bindMergeEvent();
    }
};

export default layoutdesign;