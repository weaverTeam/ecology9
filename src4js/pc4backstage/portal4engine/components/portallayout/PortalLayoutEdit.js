import { inject, observer } from 'mobx-react';
import { Button, Row, Col } from 'antd';
import { WeaErrorPage, WeaTools, WeaDialog, WeaRightMenu, WeaInput } from 'ecCom';
import PortalPage from './PortalPage';
import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';
@inject('portallayout_store')
@inject('layoutdesign_store')
@observer
class PortalLayoutEdit extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
    }
    handleSubmit() {
        const { portallayout_store, layoutdesign_store } = this.props;
        const { layoutid, layoutname, layoutdesc } = portallayout_store.edit_dialog.data;
        layoutdesign_store.saveCheckLayout({layoutid, layoutname, layoutdesc});
    }
    onCancel(){
        const { portallayout_store } = this.props;
        portallayout_store.onEditCancel();
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Preservation'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-go-back'/>,
            content:'取消'
        });
        return btns
    }
    onRightMenuClick(key){
        switch(key){
            case '0':
                this.handleSubmit();
                break;
            case '1':
                this.onCancel();
                break;
       }
    }
    getButtons(){
        let btns = [];
        const { portallayout_store } = this.props;
        btns.push(<Button type="primary" onClick={this.handleSubmit}>保存</Button>);
        btns.push(<Button onClick={this.onCancel}>取消</Button>);
        return btns;
    }
    render() {
        const { portallayout_store } = this.props;
        const { edit_dialog, onCancel, setEditInputValue } = portallayout_store;
        const { visible, title, data } = edit_dialog;
        const { layoutid, layoutname, layoutdesc, layouttable, cellmergeinfo } = data;
        return <WeaDialog title={title} 
                  visible={visible}
                  closable={true}
                  onCancel={this.onCancel}
                  icon='icon-coms-portal'
                  iconBgcolor='#1a57a0'
                  maskClosable={false}
                  buttons={this.getButtons()}
                  style={{width:top.document.body.clientWidth * 0.5,height:top.document.body.clientHeight * 0.6 }}>
                  <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                    <div className='portal-p4e-layout-edit-content'>
                      {layoutid ? <PortalPage/> : ''}
                      <PortalComForm>
                        <Row>
                            <Col span={12}>
                                <PortalComFormItem label="名称">
                                  <WeaInput fieldName="layoutname" value={layoutname} onChange={setEditInputValue.bind(this,'layoutname')}/>
                                </PortalComFormItem>
                            </Col>
                            <Col span={12}>
                                <PortalComFormItem label="描述">
                                  <WeaInput fieldName="layoutdesc" value={layoutdesc} onChange={setEditInputValue.bind(this,'layoutdesc')}/>
                                </PortalComFormItem>
                            </Col>
                        </Row>      
                      </PortalComForm>
                    {visible ? <LayoutTable {...{layoutid, layouttable, cellmergeinfo}}/> : ''}
                 </div>
               </WeaRightMenu>
        </WeaDialog>
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}

PortalLayoutEdit = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(PortalLayoutEdit);
export default PortalLayoutEdit;

@inject('portallayout_store')
@inject('layoutdesign_store')
@observer
class LayoutTable extends React.Component{
    componentDidMount(){
        const { layoutdesign_store, layoutid, layouttable, cellmergeinfo } = this.props;
        //恢复设置 
        if(layoutid){
            layoutdesign_store.reciverLayout({
                layouttable, 
                cellmergeinfo
            });
        }else{
            // 初始设置
            layoutdesign_store.initLayoutDesign(); 
        } 
    }
    componentDidUpdate(){
        const { layoutdesign_store, layoutid, layouttable, cellmergeinfo } = this.props;
        //恢复设置 
        if(layoutid){
            layoutdesign_store.reciverLayout({
                layouttable, 
                cellmergeinfo
            });
        }else{
            // 初始设置
            layoutdesign_store.initLayoutDesign(); 
        } 
    }
    render(){
        return <div className="layoutcontainer"> 
                 <div style={{position: 'absolute',top: '-20px', height: '20px',overflow: 'hidden', width: '100%',borderTop: '1px solid #F0EAEA'}}> 
                  <div className="inchmain">
                   <div style={{marginLeft: '3px'}}>
                    0
                   </div>
                  </div> 
                  <div className="inch" style={{left: '5%'}}></div> 
                  <div className="inch" style={{left: '10%'}}></div> 
                  <div className="inch" style={{left: '15%'}}></div> 
                  <div className="inch" style={{left: '20%'}}></div> 
                  <div className="inchmain" style={{left: '25%'}}>
                   <div className="inchvalue">
                    25
                   </div>
                  </div> 
                  <div className="inch" style={{left: '30%'}}></div> 
                  <div className="inch" style={{left: '35%'}}></div> 
                  <div className="inch" style={{left: '40%'}}></div> 
                  <div className="inch" style={{left: '45%'}}></div> 
                  <div className="inchmain" style={{left: '50%'}}>
                   <div className="inchvalue">
                    50
                   </div>
                  </div> 
                  <div className="inch" style={{left: '55%'}}></div> 
                  <div className="inch" style={{left: '60%'}}></div> 
                  <div className="inch" style={{left: '65%'}}></div> 
                  <div className="inch" style={{left: '70%'}}></div> 
                  <div className="inchmain" style={{left: '74.9%'}}>
                   <div className="inchvalue">
                    75
                   </div>
                  </div> 
                  <div className="inch" style={{left: '80%'}}></div> 
                  <div className="inch" style={{left: '85%'}}></div> 
                  <div className="inch" style={{left: '90%'}}></div> 
                  <div className="inch" style={{left: '95%'}}></div> 
                  <div className="inchmain" style={{left: '99.9%'}}>
                   <div className="inchvalue" style={{marginLeft: '-25px'}}>
                    100
                   </div>
                  </div> 
                 </div> 
                 <div className="layouttablewrapper"> 
                  <div className="vline wline" style={{left: '0px', top: '0px', height: '134px'}}> 
                  </div> 
                  <div className="vline eline" style={{left: '206px', top: '0px', height: '134px'}}> 
                  </div> 
                  <div className="hline nline" style={{left: '0px', top: '0px', width: '206px'}}> 
                  </div> 
                  <div className="hline sline" style={{left: '0px', top: '134px', width: '206px'}}> 
                  </div> 
                  <div className="imageicons" style={{top: '99px', display: 'none'}}> 
                   <div className="mergeicon" title="合并"></div> 
                   <div className="splitall" title="拆分所有"></div> 
                   <div className="hsplit" title="横向拆分"></div> 
                   <div className="vsplit" title="纵向拆分"></div> 
                  </div> 
                 </div> 
                </div>
           
    }
}