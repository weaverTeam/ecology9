import React from 'react';
import { inject, observer } from 'mobx-react';
import { WeaTools, WeaErrorPage, WeaTop, WeaRightMenu, WeaLeftRightLayout, WeaOrgTree, WeaDialog, WeaInputSearch } from 'ecCom';
import { Button, Table } from 'antd';
import PortalLayoutEdit from './PortalLayoutEdit';
import PortalLayoutUpload from './PortalLayoutUpload';
import './css/style.css';

@inject('portallayout_store')
@observer
class PortalLayout extends React.Component {
    constructor(props) {
        super(props);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSearch = this.onSearch.bind(this);
        //初始化数据
        props.portallayout_store.getData();
    }
    onOperatesClick(record,index,operate,flag){
        const id = record.id;
        switch(operate.index){
            case '0': //新建布局
                break;
            case '1': //上传布局
                break;
        }
    }
    onSearch(value){
        const { portallayout_store } = this.props;
        portallayout_store.getData({layoutname:value});
    }
    getButtons(){
        const { portallayout_store } = this.props;
        let btns = [];
        btns.push(<Button type="primary" onClick={portallayout_store.onEdit.bind(this,'')}>新建布局</Button>);
        btns.push(<Button onClick={portallayout_store.onUpload}>上传布局</Button>);
        btns.push(<WeaInputSearch placeholder="请输入名称" onSearch={this.onSearch} />);
        return btns;
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Add-to'/>,
            content:'新建布局'
        });
        btns.push({
            icon: <i className='icon-coms-Journal'/>,
            content:'上传布局'
        });
        return btns
    }
    onRightMenuClick(key){
        const { portallayout_store } = this.props;
        switch(key){
            case '0': //新建布局
                portallayout_store.onEdit();
                break;
            case '1': //上传布局
                portallayout_store.onUpload();
                break;
        }
    }
    render(){
        const { portallayout_store } = this.props;
        const { data, edit_dialog } = portallayout_store;
        return <div id="portallayout">
                <PortalLayoutEdit/>
                <PortalLayoutUpload/>
                <WeaTop title={"页面布局库"}
                    loading={false}
                    icon={<i className='icon-coms-portal' />}
                    iconBgcolor='#1a57a0'
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenu()}
                    onDropMenuClick={this.onRightMenuClick}>
                    <WeaRightMenu width={210} datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                        {data.length > 0 ? <Table showHeader={false} pagination={{ pageSize: 2 }} dataSource={data} columns={this.getColumns()}/> : ''}
                    </WeaRightMenu>
               </WeaTop>
        </div>
    }
    getColumns(){
        const columns = []
        const arr = [1,2,3,4];
        arr.map(i=>{
            columns.push({
                dataIndex: 'col_'+i,
                key: 'col_'+i,
                width: '25%',
                render: (text) => text.id ? <LayoutBox layout={text}/> : '', 
            });
        });
        return columns;
    }

}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
PortalLayout = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(PortalLayout);

export default PortalLayout;

@inject('portallayout_store')
@observer
class LayoutBox extends React.Component{
    componentDidMount(){
        const { layout, portallayout_store } = this.props;
        portallayout_store.showTableLayout(layout)
    }
    componentDidUpdate(preProps){
        const { layout, portallayout_store } = this.props;
        portallayout_store.showTableLayout(layout)
    }
    render(){
        const { layout, portallayout_store } = this.props;
        const { deleteLayout, onDownload, onEdit } = portallayout_store;
        const { id, isDel, layoutname, layouttable, layouttype, layoutimage, zipname } = layout;
        return <div><div className="portal-p4e-layout-box">
                {layouttable ? <div dangerouslySetInnerHTML={{__html: layouttable}}></div> : <img src={layoutimage} style={{maxWidth:'140px',maxHeight:'145px',verticalAlign:'middle'}}/>}
                <div className="opcontainer">
                    {layouttype === 'design' ? <i onClick={onEdit.bind(this,id)} className="icon-coms-edit opcontainer-icon"/> : '' }
                    {isDel ? <i className="icon-coms-delete opcontainer-icon" onClick={deleteLayout.bind(this,id)}/> : ''}
                    {layouttype === 'cus' ? <i className="icon-coms-download opcontainer-icon" onClick={onDownload.bind(this,id,zipname)}/> : '' }
                </div>
            </div>
            <div className="portal-p4e-layout-title">
              <span>{layoutname} <input type="hidden" id={`layoutthumbnail_${id}`} name={`layoutthumbnail_${id}`} value={id} fname={layoutname} newstype="/images/homepage/layout/layout_01.png"/>
                <span></span>
            </span>
            </div></div>
    }
}
