import { inject, observer } from 'mobx-react';
import { Button } from 'antd';
import { WeaErrorPage, WeaTools, WeaDialog, WeaRightMenu, WeaTextarea, WeaInput, WeaUpload } from 'ecCom';
import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';
@inject('portallayout_store')
@observer
class PortalLayoutUpload extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onFileChange = this.onFileChange.bind(this);
    }
    onFileChange(ids, list){
      	if(list && list.length){
      		  const item = list[list.length - 1];
      		  const { portallayout_store } = this.props;
      		  portallayout_store.setUploadZipName(item.filename, item.filelink);
      	}
	  }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Preservation'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-go-back'/>,
            content:'取消'
        });
        return btns
    }
    onRightMenuClick(key){
        const { portallayout_store } = this.props;
        switch(key){
            case '0':
                portallayout_store.saveUpload();
                break;
            case '1':
                portallayout_store.onUploadCancel();
                break;
       }
    }
    getButtons(){
        let btns = [];
        const { portallayout_store } = this.props;
        btns.push(<Button type="primary" onClick={portallayout_store.saveUpload}>保存</Button>);
        btns.push(<Button onClick={portallayout_store.onUploadCancel}>取消</Button>);
        return btns;
    }
    render() {
        const { portallayout_store } = this.props;
        const { upload_dialog, onUploadCancel, setUploadInputValue } = portallayout_store;
        const { visible, title, layoutname, layoutdesc, zipname, layoutzip } = upload_dialog;
        return <WeaDialog title={title} 
                  visible={visible}
                  closable={true}
                  onCancel={onUploadCancel}
                  icon='icon-coms-portal'
                  iconBgcolor='#1a57a0'
                  maskClosable={false}
                  buttons={this.getButtons()}
                  style={{width:top.document.body.clientWidth * 0.3,height:top.document.body.clientHeight * 0.4 }}>
                  <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                 	<PortalComForm>
                      <PortalComFormItem label="名称">
                        <WeaInput fieldName="layoutname" value={layoutname} onChange={setUploadInputValue.bind(this,'layoutname')}/>
                      </PortalComFormItem>
                      <PortalComFormItem label="描述">
                        <WeaTextarea fieldName="layoutdesc" value={layoutdesc} onChange={setUploadInputValue.bind(this,'layoutdesc')}/>
                      </PortalComFormItem>
                      <PortalComFormItem label="文件(.zip)">
                        <div className="portal-p4e-layout-upload-btn">
                        	<WeaUpload
            					    		value={layoutzip}
            					    		maxUploadSize={4}
            					    		multiSelection={false}
              								uploadId="layoutzip"
              								uploadUrl="/api/portal/portallayout/upload"
              								category="0"
              								viewAttr="2"
              								limitType="zip"
              								onChange={this.onFileChange}
              								showClearAll={true}>
            					    		<Button>选择文件</Button>
            					    	</WeaUpload>
                        </div>
				    	           <span>{zipname}</span>
                      </PortalComFormItem>
                  </PortalComForm>
               </WeaRightMenu>
        </WeaDialog>
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}

PortalLayoutUpload = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(PortalLayoutUpload);
export default PortalLayoutUpload;
