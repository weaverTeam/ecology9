import { inject, observer } from 'mobx-react';
import { Button } from 'antd';
import { WeaErrorPage, WeaTools, WeaTop, WeaLeftRightLayout, WeaDialog, WeaRightMenu } from 'ecCom';

//E9登录页
import Login from './login/';
import TempLateList from './TempLateList';

@inject('loginportal_setting_store')
@inject('loginportal_preview_store')
@observer
class SettingContent extends React.Component {
    render() {
        const { loginportal_setting_store  } = this.props;
        const { settingdata, handleCancel } = loginportal_setting_store
        const { title, visible, id, templateType  } = settingdata;
        return <WeaDialog title={"登录前门户"} 
                      visible={visible}
                      closable={true}
                      onCancel={handleCancel}
                      icon='icon-coms-portal'
                      iconBgcolor='#1a57a0'
                      maskClosable={false}
                      buttons={this.getButtons()}
                      style={{width:top.document.body.clientWidth - 100,height:top.document.body.clientHeight - 150 }}>
                      {visible ? <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                      <WeaLeftRightLayout leftWidth={1} leftCom={<TempLateList/>}>
                          <Login id={id} templateType={templateType}/>
                      </WeaLeftRightLayout>
                   </WeaRightMenu> : ''}
            </WeaDialog>
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Preservation'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-Supervise'/>,
            content:'预览'
        });
        btns.push({
            icon: <i className='icon-coms-Forcible'/>,
            content:'回复默认'
        });
        return btns
    }
    onRightMenuClick(key){
        const { loginportal_setting_store, loginportal_preview_store } = this.props;
        switch(key){
            case '0':
                loginportal_setting_store.saveSettingContent();
                break;
            case '1':
                loginportal_preview_store.doPreview('');
                break;
            case '2':
                loginportal_setting_store.restoreDefault();
                break;
       }
    }
    getButtons(){
        let btns = [];
        const { loginportal_setting_store, loginportal_preview_store  } = this.props;
        const { handleCancel, saveSettingContent, restoreDefault  } = loginportal_setting_store
        btns.push(<Button type="primary" onClick={saveSettingContent}>保存</Button>);
        btns.push(<Button onClick={loginportal_preview_store.doPreview.bind(this,'')}>预览</Button>);
        btns.push(<Button onClick={restoreDefault}>恢复默认</Button>);
        return btns;
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
SettingContent = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(SettingContent);
export default SettingContent;