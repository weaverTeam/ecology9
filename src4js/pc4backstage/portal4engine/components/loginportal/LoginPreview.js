import { inject, observer } from 'mobx-react';
import { WeaErrorPage, WeaTools, WeaDialog } from 'ecCom';
//E9登录页
import Login from './login/';
@inject('loginportal_preview_store')
@observer
class LoginPreview extends React.Component {
    render() {
        const { loginportal_preview_store  } = this.props;
        const { previewdata, handleCancel } = loginportal_preview_store;
        const { title, visible, id, templateType  } = previewdata;
        return <WeaDialog title={title}
                      visible={visible} 
                      closable={true}
                      onCancel={handleCancel}
                      icon='icon-coms-portal'
                      iconBgcolor='#1a57a0'
                      maskClosable={false}
                      buttons={[]}
                      style={{width:top.document.body.clientWidth - 100,height:top.document.body.clientHeight - 120 }}>
                  {visible ? <Login id={id} templateType={templateType}/> : ''}
        </WeaDialog>
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
LoginPreview = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(LoginPreview);

export default LoginPreview;

