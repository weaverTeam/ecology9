import { inject, observer } from 'mobx-react';
import { WeaErrorPage, WeaTools } from 'ecCom';
@inject('loginportal_setting_store')
@observer
class TempLateList extends React.Component{
	getItem(type){
		const { loginportal_setting_store } = this.props;
		const { settingdata, changeTemplateType } = loginportal_setting_store;
		const { templateType } = settingdata;
		return <div style={{margin:'15px',height:top.document.body.clientHeight/5 - 48}}>
				<div className={`login-system-image-setting login-system-image-box${type === templateType?'-checked':''}`}>
	                <div onClick={changeTemplateType.bind(this,type)} className={`login-system-image login-system-image-${type}`}></div>
	            </div>
            </div>
	}
	render(){
		     return <div>
			     {this.getItem('E9')}
			     {this.getItem('E8')}
			     {this.getItem('H')}
			     {this.getItem('H2')}
			     {this.getItem('site')}
		     </div>
	}
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
TempLateList = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(TempLateList);
export default TempLateList;