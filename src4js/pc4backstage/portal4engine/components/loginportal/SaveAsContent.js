import { inject, observer } from 'mobx-react';
import { Button } from 'antd';
import { WeaErrorPage, WeaTools, WeaTop, WeaInput, WeaDialog, WeaRightMenu } from 'ecCom';
import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';
@inject('loginportal_saveas_store')
@observer
class SaveAsContent extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
    }
    getButtons(){
        const { loginportal_saveas_store } = this.props;
        let btns = [];
        btns.push(<Button type="primary" onClick={loginportal_saveas_store.handleSubmit}>确定</Button>);
        btns.push(<Button onClick={loginportal_saveas_store.handleCancel}>取消</Button>)
        return btns
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Preservation'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-go-back'/>,
            content:'取消'
        });
        return btns
    }
    onRightMenuClick(key){
       const { loginportal_saveas_store } = this.props;
       switch(key){
            case '0':
                loginportal_saveas_store.handleSubmit();
                break;
            case '1':
                loginportal_saveas_store.handleCancel()
                break;
       }
    }
    render() {
        const { loginportal_saveas_store } = this.props;
        const{ saveasdata, setInfoName, handleCancel } = loginportal_saveas_store;
        const { title, visible, name } = saveasdata;
        return <WeaDialog title={title}
                  visible={visible} 
                  closable={true}
                  onCancel={handleCancel}
                  icon='icon-coms-portal'
                  iconBgcolor='#1a57a0'
                  maskClosable={false}
                  buttons={this.getButtons()}
                  style={{width: 520, height: 237}}>
                  <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                     <PortalComForm>
                        <PortalComFormItem label="门户名称">
                            <WeaInput fieldName="name" value={name} onBlur={setInfoName}/>
                        </PortalComFormItem>
                      </PortalComForm>
                </WeaRightMenu>
        </WeaDialog>
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
SaveAsContent = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(SaveAsContent);
export default SaveAsContent;