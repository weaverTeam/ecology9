import { inject, observer } from 'mobx-react';
import { WeaErrorPage, WeaTools, WeaTop, WeaScroll, WeaBrowser, WeaInput, WeaSelect, WeaDialog, WeaAlertPage, WeaRightMenu } from 'ecCom';
import { Input, Button, message, Icon, Popover, Checkbox, Collapse, Row, Col, DatePicker, Switch, Select } from 'antd';
const Option = Select.Option;
const Panel = Collapse.Panel;
import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';


import Immutable from 'immutable';
const { fromJS, is } = Immutable;
@inject('loginportal_edit_store')
@observer
class ColCom extends React.Component {
    render(){
        const { templateType, loginportal_edit_store, checkType } = this.props;
        return <Col span={8}>
                <div style={{padding:'15px'}}>
                   <div className={`login-system-image-edit login-system-image-box${templateType === checkType ? '-checked':''}`}>
                      <div onClick={loginportal_edit_store.setFormItemValue.bind(this,'templateType',templateType)} className={`login-system-image login-system-image-${templateType}`}>
                      </div>
                  </div>
                </div>
              </Col>
    }
}


@inject('loginportal_edit_store')
@inject('loginportal_setting_store')
@observer
class EditContent extends React.Component {
    getHeader(title){
        return <div style={{width:'100%',borderBottom:'1px solid #e9e9e9'}}><span style={{fontWeight: 'bold', fontSize: '14px',color: '#746e6e'}}>{title}</span></div>
    }
    getButtons(){
        const { loginportal_edit_store } = this.props;
        let btns = [];
        btns.push(<Button type="primary" onClick={loginportal_edit_store.handleSubmit.bind(this,'save')}>保存</Button>);
        btns.push(<Button onClick={this.onSaveAndSetting.bind(this)}>保存并进入登录页设置</Button>);
        btns.push(<Button onClick={loginportal_edit_store.handleCancel}>取消</Button>);
        return btns
    }
    render() {
        const { loginportal_edit_store } = this.props;
        const { editdata, handleCancel, setFormItemValue } = loginportal_edit_store;
        const { title, visible, data } = editdata;
        let html = <div/>
        if(data) {
          const { loginTemplateId, templateType, loginTemplateName, domainName, loginTemplateTitle, isRememberPW } = data;
          html = <WeaScroll typeClass="scrollbar-macosx" className="portal-loginportal-edit-content">
                       <Collapse defaultActiveKey={['1','2']}>
                        <Panel header={this.getHeader('基本信息')} key="1">
                            <PortalComForm>
                               <Row>
                                <Col span={12}>
                                  <PortalComFormItem labelCol={7} wrapperCol={15} label="门户名称">
                                      <WeaInput fieldName="loginTemplateName" value={loginTemplateName} onChange={setFormItemValue.bind(this,'loginTemplateName')} />
                                  </PortalComFormItem>
                                  <PortalComFormItem labelCol={7} wrapperCol={15} label="系统标题">
                                      <WeaInput fieldName="loginTemplateTitle" value={loginTemplateTitle} onChange={setFormItemValue.bind(this,'loginTemplateTitle')} />
                                  </PortalComFormItem>
                                </Col>
                                <Col span={12}>
                                    <PortalComFormItem labelCol={7} wrapperCol={15} label="绑定访问域名">
                                      <WeaInput fieldName="domainName" value={domainName} onChange={setFormItemValue.bind(this,'domainName')} />
                                      <Popover content={<div style={{width:'180px',lineHeight:'20px'}}>设置后，如果从这个域名进行访问则无论此登录前门户是否启用，都显示该登录前门户</div>} trigger="click">
                                          <i className='icon-coms-help icon-portalengine-help' />
                                      </Popover>
                                    </PortalComFormItem>
                                    <PortalComFormItem labelCol={9} wrapperCol={13} label="允许记住用户名和密码">
                                      <Switch onChange={setFormItemValue.bind(this,'isRememberPW')} defaultChecked={isRememberPW}/>
                                    </PortalComFormItem>
                                  </Col>
                                </Row>
                                {this.getFormItems()}
                           </PortalComForm>
                      </Panel>
                      <Panel header={this.getHeader('样式选择')} key="2">
                            <Row gutter={24}>
                                <ColCom templateType={'E9'} checkType={templateType}/>
                                <ColCom templateType={'E8'} checkType={templateType}/>
                                <ColCom templateType={'H'} checkType={templateType}/>
                            </Row>
                            <Row gutter={16}>
                                <ColCom templateType={'H2'} checkType={templateType}/>
                                <ColCom templateType={'site'} checkType={templateType}/>
                            </Row>
                        </Panel>
                   </Collapse>
               </WeaScroll>
        }
        return <WeaDialog title={title}
                  visible={visible} 
                  closable={true}
                  onCancel={handleCancel} 
                  icon='icon-coms-portal'
                  iconBgcolor='#1a57a0'
                  maskClosable={false}
                  buttons={this.getButtons()}
                  style={{width:'1000px',height:'704px'}}>
                <WeaRightMenu width={190} datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                  {html}  
                </WeaRightMenu>
       </WeaDialog>
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Preservation'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-Flow-setting'/>,
            content:'保存并进入登录页设置'
        });
        btns.push({
            icon: <i className='icon-coms-go-back'/>,
            content:'取消'
        });
        return btns
    }
    onSaveAndSetting(){
        const { loginportal_edit_store } = this.props;
        loginportal_edit_store.handleSubmit();
        setTimeout(()=>{
            const { loginportal_setting_store, loginportal_edit_store } = this.props;
            loginportal_setting_store.doSetting(loginportal_edit_store.editdata.data.loginTemplateId);
        },1000);
    }
    onRightMenuClick(key){
        const { loginportal_edit_store } = this.props;
        const { handleCancel, editdata } = loginportal_edit_store;
        switch(key){
            case '0':
                loginportal_edit_store.handleSubmit();
                break;
            case '1':
                this.onSaveAndSetting();
                break;
            case '2':
                loginportal_edit_store.handleCancel()
                break;
       }
    }
    getName(value,arr){
        for (var i = 0; i < arr.length; i++) {
            var item = arr[i];
            if(value === item.key){
                return item.name;
            }
        }
        return '';
    }
    getFormItems(){
        const { loginportal_edit_store } = this.props;
        const { editdata, handleBModalVisible, setFormItemValue } = loginportal_edit_store;
        const { data } = editdata;
        const { templateType, modeid, menuId, menutypeid, menutypeidname, leftmenuid, leftmenustyleid, leftmenustyleidname, defaultshow, defaultshowname, pagetemplate, topmenu, leftmenu } = data;
        if(templateType === 'site'){
          return <Row>
            <Col span={12}>
                <PortalComFormItem labelCol={7} wrapperCol={15} label="页面模板">
                    <WeaSelect options={pagetemplate} fieldName="modeid" value={modeid} onChange={setFormItemValue.bind(this,'modeid')} />
                </PortalComFormItem>
                <PortalComFormItem labelCol={7} wrapperCol={15} label="顶部菜单">
                    <WeaSelect options={topmenu} fieldName="menuId" value={menuId} onChange={setFormItemValue.bind(this,'menuId')} />
                </PortalComFormItem>
                <PortalComFormItem labelCol={7} wrapperCol={15} label="顶部菜单样式">
                    <WeaBrowser
                        fieldName='menutypeid'
                        title="顶部菜单样式"
                        icon='icon-coms-portal'
                        iconBgcolor='#1a57a0'
                        dataURL='/api/portal/logintemplate/menustylelist'
                        dataParams={{type:'h'}}
                        replaceDatas={defaultshowname}
                        onChange={(ids, names, datas) => setFormItemValue('menutypeid',ids)}
                    />
                </PortalComFormItem>
            </Col>
            <Col span={12}>
                <PortalComFormItem labelCol={7} wrapperCol={15} label="默认显示页">
                    <WeaBrowser
                        fieldName='defaultshow'
                        title="默认显示页"
                        icon='icon-coms-portal'
                        iconBgcolor='#1a57a0'
                        dataURL='/api/portal/logintemplate/defaultdisplaypage'                      
                        replaceDatas={defaultshowname}
                        onChange={(ids, names, datas) => setFormItemValue('defaultshow',ids)}
                    />
                </PortalComFormItem>
                <PortalComFormItem labelCol={7} wrapperCol={15} label="左侧菜单">
                  <WeaSelect options={leftmenu} fieldName="leftmenuid" value={leftmenuid} onChange={setFormItemValue.bind(this,'leftmenuid')} />
                </PortalComFormItem>
                <PortalComFormItem labelCol={7} wrapperCol={15} label="左侧菜单样式">
                    <WeaBrowser
                        fieldName='leftmenustyleid'
                        title="左侧菜单样式"
                        icon='icon-coms-portal'
                        iconBgcolor='#1a57a0'
                        dataURL='/api/portal/logintemplate/menustylelist'
                        dataParams={{type:'v'}}
                        replaceDatas={leftmenustyleidname}
                        onChange={(ids, names, datas) => setFormItemValue('leftmenustyleid',ids)}
                    />
                </PortalComFormItem>
            </Col>
          </Row>
        }
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
EditContent = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(EditContent);

export default EditContent;
