import { Button, Popconfirm, Icon } from 'antd';
import { inject, observer } from 'mobx-react';
import { WeaTop, WeaRightMenu, WeaTools, WeaDialog } from 'ecCom';
import SaveAsContent from './SaveAsContent';
import EditContent from './EditContent';
import SettingContent from './SettingContent';
import LoginPreview from './LoginPreview';
import { WeaTable } from 'comsMobx';
import './css/style.css';
@inject('loginportal_store')
@inject('comsWeaTableStore')
@inject('loginportal_saveas_store')
@inject('loginportal_edit_store')
@inject('loginportal_setting_store')
@inject('loginportal_preview_store')
@observer
class LoginPortal extends React.Component {
    constructor(props) {
        super(props)
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        //初始化数据
        props.loginportal_store.getSessionKey();
    }
    getButtons(){
        const { saveLoginTemplate, deleteRows } = this.props.loginportal_store;
        let btns = [];
        btns.push(<Button type="primary" onClick={saveLoginTemplate}>保存</Button>);
        btns.push(<Button onClick={deleteRows}>批量删除</Button>);
        return btns;
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Preservation'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-delete'/>,
            content:'批量删除'
        });
        return btns
    }
    onRightMenuClick(key){
       const { saveLoginTemplate, deleteRows } = this.props.loginportal_store;
       switch(key){
            case '0':
                saveLoginTemplate()
                break;
            case '1':
                deleteRows()
                break;
       }
    }
    onOperatesClick(record,index,operate,flag){
        const id = record.logintemplateid;
        const { loginportal_store, loginportal_saveas_store, loginportal_edit_store, loginportal_setting_store, loginportal_preview_store } = this.props;
        switch(operate.index){
            case '0':
                loginportal_preview_store.doPreview(id);
                break;
            case '1':
                loginportal_setting_store.doSetting(id);
                break;
            case '2':
                loginportal_edit_store.doEdit(id);
                break;
            case '3':
                loginportal_saveas_store.doSaveAs(id);
                break;
            case '4':
                loginportal_store.doDel(id);
                break;
        }
    }
    render(){
        const { sessionKey } = this.props.loginportal_store;
        if(!sessionKey) return <div/>
        return <div className="portal-loginportal-box" id="loginPortal">
            <LoginPreview/>
            <SettingContent/>
            <EditContent/>
            <SaveAsContent/>
            <WeaTop title={"登录前门户"}
                    loading={false}
                    icon={<i className='icon-coms-portal' />}
                    iconBgcolor='#1a57a0'
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenu()}
                    onDropMenuClick={this.onRightMenuClick}>
                   <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                       <WeaTable sessionkey={sessionKey} onOperatesClick={this.onOperatesClick}/>
                    </WeaRightMenu>
            </WeaTop>
        </div>
    }

}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
LoginPortal = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(LoginPortal);

export default LoginPortal;