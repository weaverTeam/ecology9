import { inject, observer } from 'mobx-react';
import './style.css';
@inject("login4e8_setting_store")
@observer
class HLogin extends React.Component {
    render() {
        const { toolbar, settings, openPictureLib } = this.props.login4e8_setting_store;
        const { bgImage, logoImage } = settings;
        let style = {};
        if(bgImage) style = {
            backgroundImage:'url('+bgImage+')'
        }
        if(toolbar.displayMode === 'edit') {
            style.border = '1px dashed #FF0000';
        }
        return <div className="hlogin-setting-box">
                <div className="hlogin-setting-logo">
                    <img src="/images_face/login/weaverlogo_wev8.gif" width="325" height="50"/>
                </div>
                <div className="hlogin-setting-area" style={style} onClick={openPictureLib.bind(this,'bgimage')}>
                    <div className="hlogin-setting-form">
                        <div style={{height:'22px'}}></div>
                        <input id="loginid" name="loginid" type="text" value="sysadmin"/>
                        <div style={{height:'10px'}}></div>
                        <input name="userpassword" type="password" value="1"/>
                        <div style={{height:'60px'}}></div>
                        <button type="submit"></button>
                    </div>
                </div>
                <div className="hlogin-setting-footer">
                  <div className="hlogin-setting-footer-left">
                    <img src="/images_face/login/copyright_wev8.gif" width="449" height="80"/>
                  </div>
                  <div  className="hlogin-setting-footer-right">
                    <span style={{lineHeight: '20px', fontSize:'9px'}}> <font style={{color:'#990000',fontWeight: 'bold'}}>提示：</font>泛微协同商务系统需要运行在<font style={{color:'#5F7DD0',fontWeight: 'bold'}}>IE6.0</font> 以及使用 <font style={{color:'#5F7DD0',fontWeight: 'bold'}}>Microsoft 的虚拟机(VM)</font>；如果您是首次登录系统请确认您的浏览器是否是IE6.0；Microsoft 的虚拟机(VM)下载请直接点击<a href="/weaverplugin/msjavx86.exe"><font style={{color:'#5F7DD0',fontWeight: 'bold',textDecoration: 'underline'}}>这里</font></a>下载安装，该插件只需在首次下载安装后即可，不需要重复安装。</span>
                  </div>
                </div>
           </div>
    }
}
export default HLogin;