import { inject, observer } from 'mobx-react';
import './style.css';
@inject("login4e8_setting_store")
@observer
class E8Login extends React.Component {
    render() {
        const { toolbar, settings, openPictureLib } = this.props.login4e8_setting_store;
        let { bgImage, logoImage } = settings;
        let boxStyle = {};
        if(bgImage) {
            boxStyle.background = 'url('+bgImage+')';
        }
        let logoStyle = {};
        if(toolbar.displayMode === 'edit') {
            boxStyle.border = '1px dashed #FF0000';
            logoStyle.border = '1px dashed #FF0000';
        }
        if(!logoImage) logoImage = '/wui/theme/ecology8/page/images/login/logo_wev8.png';
        return <div className="e8login-setting-box" style={boxStyle} onClick={openPictureLib.bind(this,'bgimage')}>
                <div className="e8login-setting-area">
                    <div className="e8login-setting-logo">
                        <img src={logoImage} style={logoStyle} onClick={openPictureLib.bind(this,'logo')}/>
                    </div> 
                    <div className="e8login-setting-multiLang">
                    </div>  
                    <div className="e8login-setting-form">
                        <div className="e8login-setting-input">
                           <img src="/wui/theme/ecology8/page/images/login/username_wev8.png"/>  
                           <label for="loginid">帐号</label>
                           <input type="hidden" name="loginid" id="loginid"/>
                        </div>
                        <div className="e8login-setting-line1"/>
                        <div className="e8login-setting-input">
                           <img src="/wui/theme/ecology8/page/images/login/password_wev8.png"/>  
                           <label for="userpassword">密码</label>
                           <input type="hidden" name="userpassword" id="userpassword"/>
                        </div>
                        <div className="e8login-setting-line6"/>
                        <div className="e8login-setting-submit">
                            <input type="submit" value=""/>
                        </div>                        
                    </div> 
              </div>
       </div>
    }
}
export default E8Login;