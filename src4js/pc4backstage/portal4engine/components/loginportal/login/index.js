import E9Login from './e9login/E9Login';
import E8Login from './e8login/E8Login';
import H2Login from './h2login/H2Login';
import HLogin from './hlogin/HLogin';
import SiteLogin from './site/SiteLogin';
class Login extends React.Component {
	shouldComponentUpdate(nextProps){
		const { templateType } = this.props; 
		return templateType !== nextProps.templateType;
	}
	render(){
		const { id, templateType } = this.props;
		if(templateType){
			switch(templateType){
				case 'E9':
					return <E9Login/>
				case 'E8':
					return <E8Login/>
				case 'H2':
					return <H2Login/>
				case 'H':
					return <HLogin/>
				case 'site':
					return <SiteLogin id={id}/>
			}
		}
		return <div style={{height:top.document.body.clientHeight-84}}>{templateType+"登录界面功能未实现"}</div>
	}
}
export default Login;