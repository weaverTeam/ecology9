import { inject, observer } from 'mobx-react';
import { Carousel } from 'antd';
import './style.css';
@inject("login4e8_setting_store")
@observer
class H2Login extends React.Component {
    render() {
        const { toolbar, settings, openPictureLib } = this.props.login4e8_setting_store;
        const { bgImage, logoImage } = settings;
        const imgArr = ['/wui/theme/ecology7/page/images/login/lg_bg1_wev8.jpg','/wui/theme/ecology7/page/images/login/lg_bg6_wev8.jpg','/wui/theme/ecology7/page/images/login/lg_bg3_wev8.jpg'];
        let imgHtml = imgArr.map((img)=>{
            return <div><img src={img} alt=''/></div>
        })
        let style = {};
        if(bgImage) {
            style.background = 'url('+bgImage+') no-repeat';
        }
        if(toolbar.displayMode === 'edit') {
            style.border = '1px dashed #FF0000';
        }
        return <div className="h2login-setting-box">
                <div className="h2login-setting-area" onClick={openPictureLib.bind(this,'bgimage')} style={style}>
                    <div className="h2login-setting-imgs">
                         <Carousel dots={false} autoplay>
                            {imgHtml}
                          </Carousel>
                    </div>
                    <div className="h2login-setting-form">
                        <div style={{height:'30px'}}></div>
                        <input id="loginid" name="loginid" type="text" value="sysadmin"/>
                        <div style={{height:'10px'}}></div>
                        <input name="userpassword" type="password" value="1"/>
                        <div style={{height:'30px'}}></div>
                        <input className="login-submit" type="submit" value="" name="submit"/>
                    </div>
               </div>
           </div>
    }
}
export default H2Login;