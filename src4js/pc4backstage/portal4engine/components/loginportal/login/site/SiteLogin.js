import { inject, observer } from 'mobx-react';
@inject("login4e8_setting_store")
@observer
class SiteLogin extends React.Component {
    render() {
        const { id } = this.props;
        return <div>
            <iframe frameBorder="no" style={{borderCollapse:'collapse',width:top.document.body.clientWidth - 100,height:top.document.body.clientHeight - 300}} src={`/page/maint/login/Page.jsp?templateId=${id}`}></iframe>   
       </div>
    }
}
export default SiteLogin;