import { inject, observer } from 'mobx-react';
import { Checkbox, message, Button, Popover} from 'antd';
import ColorPicker from 'rc-color-picker';
@inject('login4e9_setting_store')
@observer
class E9Form extends React.Component {
    componentWillMount() {
        // 刷新验证码需要的参数，每刷新一次加1
        this.seriesnum_ = 0;
    }
    render() {
        const { login4e9_setting_store } = this.props;
        const { isLogging, isRememberAccount, isRememberPassword, toolbar, settings, onSelect, rememberUserInfo, onChangeColor, handleOnMouseOver } = login4e9_setting_store;
        const { selected } = toolbar;
        const { fontColor, btnBgColor, btnOpacity } = settings;
        let btnStyle = {};
        if(fontColor) btnStyle = {color:fontColor,background:btnBgColor};
        let btnBoxStyle = { border: '3px dashed rgba(0,255,0,0)', opacity:btnOpacity};
        if(selected === 'btn') btnBoxStyle = { border: '3px dashed rgba(0,255,0,1)',opacity:btnOpacity};
        return (
            <div className="e9login-form">
                <div className="e9login-form-account">
                    <i style={{color:fontColor}} className="wevicon wevicon-e9login-account" />
                    <input style={{display: 'none'}} />
                    <input ref="account" style={{color:fontColor}} type="text" className="e9login-form-input"
                           autoComplete="off"
                           defaultValue={'sysadmin'}
                           placeholder="账号"
                    />
                </div>
                <div className="e9login-form-password">
                    <i style={{color:fontColor}} className="wevicon wevicon-e9login-password" />
                    <input style={{display: 'none'}} />
                    <input ref="password" style={{color:fontColor}} type="password" className="e9login-form-input"
                           autoComplete="off"
                           defaultValue={1}
                           placeholder="密码"
                    />
                </div>
                <div className="e9login-form-remember">
                    <div className="e9login-form-remember-content">
                        <div className="e9login-form-remember-account">
                            <Checkbox checked={isRememberAccount} onChange={rememberUserInfo.bind(this, 'account')} style={{color:fontColor}}>记住账号</Checkbox>
                        </div>
                        <div className="e9login-form-remember-password">
                            <Checkbox checked={isRememberPassword} onChange={rememberUserInfo.bind(this, 'password')} style={{color:fontColor}}>记住密码</Checkbox>
                        </div>
                    </div>
                    <div className="e9login-form-clear"></div>
                </div>
                <div className="e9login-form-submit">
                    <div className="e9login-form-submit-box" style={btnBoxStyle} onClick={onSelect.bind(this,'btn')} onMouseOver={handleOnMouseOver.bind(this,'btn')}>
                        <div className="e9login-setting-btns-opts area-opts" style={{display:selected === 'btn'?'':'none'}}>
                            <ColorPicker color={btnBgColor} alpha={btnOpacity*100}
                                onChange={onChangeColor.bind(this,'bgColor')}
                                className="portal-p4e-e9login-colorpicker"
                                children={
                                   <Button>背景颜色</Button>
                            }/>
                        </div>
                        <Button type="primary" style={btnStyle} className="e9login-form-submit-btn"  id="submit" name="submit" loading={isLogging}>
                            登 录
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

module.exports = E9Form;