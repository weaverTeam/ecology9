import { inject, observer } from 'mobx-react';
import { Menu, Dropdown, Icon, Checkbox, Carousel, Button, Popover } from 'antd';
import { WeaUpload } from 'ecCom';
import E9MultiLang from './E9MultiLang';
import E9Form from './E9Form';
import E9QRCode from './E9QRCode';
import E9BgImages from './E9BgImages';
import { WeaDraggable } from 'ecCom';
import LoginSetting from './LoginSetting';
import { collision, colorRgb } from './common';
import Immutable from 'immutable';
import ColorPicker from 'rc-color-picker';
import './css/';
@inject('loginportal_setting_store')
@inject('login4e9_setting_store')
@observer
class E9Login extends React.Component {
    openPictureLib(){
        const { changeImage } = this.props.login4e9_setting_store;
        var isSingle = true;
        var tempFile='';
        var pos=tempFile.indexOf("/page/resource/userfile/");
        if(pos!=-1) tempFile=tempFile.substring(pos+24);
        var dlg = new window.top.Dialog();//定义Dialog对象
        dlg.currentWindow = window;   
        dlg.Model=true;
        dlg.Width = top.document.body.clientWidth-100;
        dlg.Height = top.document.body.clientHeight-100;
        dlg.URL="/page/maint/common/CustomResourceMaint.jsp?isDialog=1&?file="+tempFile+"&isSingle="+isSingle;
        dlg.callbackfun=function(obj,datas){
            if (datas!=null&&datas.id!="false"){
                let src = datas.id;
                if(src.indexOf(",") !== -1){
                    src = src.split(",")[0];
                }
                changeImage(src);
            }
        }
        dlg.show();
    }
    onFileChange = (ids, list) => {
        const { changeImage } = this.props.login4e9_setting_store;
        changeImage(list[list.length-1].filelink);
    }
    handleDrag(type, e){
        let { login4e9_setting_store } = this.props;
        let { toolbar, settings, changePosition, logoRealPosition, areaRealPosition } = login4e9_setting_store;
        if(toolbar.displayMode === 'preview') return;
        let { isLock, logoBoxVisible } = settings;
        var e9lc = this.refs.e9loginContainer;
        let position = {
            x: e.clientX - $(e9lc).offset().left - e.offsetX - 3,
            y: e.clientY - $(e9lc).offset().top - e.offsetY - 3
        }
        if(type === 'logo'){
            logoRealPosition = position;
            if(!isLock){
                 logoBoxVisible = collision(this.refs.e9loginLogo,this.refs.e9loginArea,logoRealPosition,areaRealPosition);
            }
        } else if(type === 'area'){
            areaRealPosition = position;
            if(!isLock){
                 logoBoxVisible = collision(this.refs.e9loginLogo,this.refs.e9loginArea,logoRealPosition,areaRealPosition);
            }
        }
        changePosition(logoRealPosition,areaRealPosition,logoBoxVisible);
    }
    afterChange(current){
        const { setToolbarValue } = this.props.login4e9_setting_store;
        if(current<0) current = 0;
        setToolbarValue('initialSlide', current);
    }
    render() {
        let { login4e9_setting_store, loginportal_setting_store } = this.props;
        let { toolbar, settings, onSelect, setFormItemValue, setToolbarValue, changeLoginType, onChangeColor, handleOnMouseOver } = login4e9_setting_store;
        if(!settings) return <div/>
        let { boxHeight, containerHeight, bgImageHeight, logoRealPosition, areaRealPosition, areaHeight } = login4e9_setting_store;
        const { displayMode, isFormLogin, bgImages, selected, fontColorDisable, bgColorDisable, replacePicturesDisable, bgImagesBoxVisible, initialSlide, helpVisible } = toolbar;
        const { id, showFooter, changeBgImage, templateType, isLock, autoCarousel, carouselTime, fontColor, areaBgColor, areaOpacity, bgImage, logoImage, logoBoxVisible, showQrcode, qrcodeMiddle, footerColor, formOpacity, footerBgColor, footerContent, footerOpacity } = settings;
        let logoStyle = {border: '3px dashed rgba(0,255,0,0)'};
        let areaStyle = {border: '3px dashed rgba(0,255,0,0)'};
        let formStyle = {border: '3px dashed rgba(0,255,0,0)',opacity:formOpacity};
        let footerStyle = {backgroundColor:footerBgColor,color:footerColor,border: '3px dashed rgba(0,255,0,0)',opacity:footerOpacity};
        switch(selected){
            case 'area':
                areaStyle = {border: '3px dashed rgba(0,255,0,1)'};
                break;
            case 'form':
                formStyle = {border: '3px dashed rgba(0,255,0,1)'};
                break;
            case 'logo':
                logoStyle = {border: '3px dashed rgba(0,255,0,1)'};
                break; 
            case 'footer':
                footerStyle.border = '3px dashed rgba(0,255,0,1)';
                break;  
        }
        let spFontColor = selected === 'footer' ? footerColor : fontColor;
        let spBgColor = selected === 'footer' ? footerBgColor : areaBgColor;
        let imageValue = selected === 'logo' ? logoImage : bgImage;
        let bgImageHtml = <img style={{height: bgImageHeight, width:'100%'}} onClick={onSelect.bind(this,'bgimage')} src={bgImages[initialSlide]} alt="图片加载失败" />
        if(autoCarousel){
            bgImageHtml = <Carousel autoplay={true} dots={false} autoplaySpeed={carouselTime} initialSlide={initialSlide} afterChange={this.afterChange.bind(this)}>
                      {bgImages.map(imgSrc => <img style={{height: bgImageHeight}} onClick={onSelect.bind(this,'bgimage')} src={imgSrc} alt="图片加载失败" />)}
                    </Carousel>
        }
        let logohelpStyle = {};
        if(isLock){
            logohelpStyle = {top:areaRealPosition.y + 27,left:areaRealPosition.x + 298}
        }else{
            logohelpStyle = {top:logoRealPosition.y, left:logoRealPosition.x + 184}
        }
        return <div className="e9login-setting-box" style={{height: boxHeight}}>
                    {displayMode === 'preview' ? null : <div className="e9login-setting-toolbar">
                       <LoginSetting id={id} templateType={templateType}/>
                    </div>}
                    {helpVisible ? <div style={{height: (bgImageHeight + 54)}} className="e9login-setting-help-shadow"></div> : ''}  
                    {helpVisible ? <div style={{height: (bgImageHeight + 54)}} className="e9login-setting-help-info" onClick={setToolbarValue.bind(this,'helpVisible',false)}>
                        <div style={{height:bgImageHeight}} className="e9login-setting-container-help">
                            <div style={{height:(areaHeight - 6),top:areaRealPosition.y,left:(areaRealPosition.x - 150)}} className="e9login-setting-container-area-help">
                                <div className="e9login-setting-help-desc">鼠标移入可设置背景颜色，透明度，可拖动更改位置</div>
                            </div>
                            <div style={logohelpStyle} className="e9login-setting-container-logo-help">
                                <div className="e9login-setting-help-desc">取消锁定状态下可拖动更改位置，鼠标移入可设置logo图片，有直接上传和从素材库选择两种方式</div>
                            </div>
                            <div style={{top:areaRealPosition.y + (logoBoxVisible ? 210 : 10),left:areaRealPosition.x + 369}} className="e9login-setting-container-form-help">
                                <div className="e9login-setting-help-desc">鼠标移入可设置字体颜色、透明度、拖动可使位置与背景一起改变</div>
                            </div>
                            <div style={{top:areaRealPosition.y + (logoBoxVisible ? 504 : 314),left:areaRealPosition.x + 49}} className="e9login-setting-container-btn-help">
                                <div className="e9login-setting-help-desc">鼠标移入可设置背景颜色，透明度</div>
                            </div>
                            {showFooter ? <div className="e9login-setting-container-footer-help">
                                <div className="e9login-setting-help-desc">鼠标移入可设置页脚内容、页脚背景颜色、字体颜色、透明度</div>
                            </div> : ''}
                        </div>
                    </div> : '' }  
                    <div ref="e9loginContainer" className="e9login-container e9login-setting-container" style={{height: containerHeight}}>
                        {!isLock ? <WeaDraggable 
                        disabled={displayMode === 'preview'}
                        allowAnyClick={true}
                        handle=".e9login-logo-setting-drag"
                        cancel=".e9login-setting-btns-opts"
                        position={logoRealPosition}
                        onDrag={this.handleDrag.bind(this,'logo')}>
                                <div className="e9login-logo-setting-drag" style={logoStyle} onClick={onSelect.bind(this,'logo')} onMouseOver={handleOnMouseOver.bind(this,'logo')} ref="e9loginLogo">
                                    <div className="e9login-setting-btns-opts logo-opts" style={{display:selected === 'logo'?'':'none'}}>
                                       <div className='opt-div'>
                                        <WeaUpload uploadId="logoupload"
                                            uploadUrl="/api/portal/logintemplate/upload"
                                            category="0"
                                            showClearAll={true}
                                            multiSelection={false}
                                            limitType="png,gif,jpeg,jpg,bmp"
                                            onChange={this.onFileChange}>
                                            <Button>上传logo</Button>
                                          </WeaUpload>
                                        </div>
                                       <div className='opt-div'><Button onClick={this.openPictureLib.bind(this)}>从素材库中选择</Button></div>
                                    </div>
                                    <img className="e9login-logo-img" src={logoImage} alt="logo加载失败" />
                                </div>
                        </WeaDraggable> : ''}
                            <WeaDraggable
                                disabled={displayMode === 'preview'}
                                position={areaRealPosition}
                                allowAnyClick={true}
                                handle=".e9login-area"
                                cancel=".e9login-toggle,.e9login-form-box,.e9login-setting-btns-opts,.e9login-logo .e9login-logo-setting"
                                onDrag={this.handleDrag.bind(this,'area')}>
                                <div className="e9login-area" style={areaStyle} onClick={onSelect.bind(this,'area')} onMouseOver={handleOnMouseOver.bind(this,'area')} ref="e9loginArea">
                                    <div className="e9login-setting-btns-opts area-opts" style={{display:selected === 'area'?'':'none'}}>
                                        <ColorPicker color={areaBgColor} alpha={areaOpacity*100}
                                            onChange={onChangeColor.bind(this,'bgColor')}
                                            className="portal-p4e-e9login-colorpicker"
                                            children={
                                               <Button>背景颜色</Button>
                                        }/>
                                    </div>
                                    <div className="e9login-area-shadow" style={{background:areaBgColor,opacity:areaOpacity}}></div>
                                    {showQrcode && !qrcodeMiddle ? <div className="e9login-toggle" onClick={changeLoginType}>
                                        {isFormLogin ? <i className="wevicon wevicon-e9login-scan" /> : <i className="wevicon wevicon-e9login-back" />}
                                    </div> : <div/>}
                                    <div className="e9login-area-content">
                                        <div className={logoBoxVisible ? 'e9login-logo' : ''}>
                                            {isLock ? <div className="e9login-logo-setting" style={logoStyle} onClick={onSelect.bind(this,'logo')} onMouseOver={handleOnMouseOver.bind(this,'logo')}>
                                                <div className="e9login-setting-btns-opts logo-opts" style={{display:selected === 'logo'?'':'none'}}>
                                                   <div className='opt-div'>
                                                        <WeaUpload
                                                            uploadId="logoupload"
                                                            uploadUrl="/api/portal/logintemplate/upload"
                                                            category="/wui/theme/ecology9/image"
                                                            showClearAll={true}
                                                            multiSelection={false}
                                                            limitType="png,gif,jpeg,jpg,bmp"
                                                            onChange={this.onFileChange}>
                                                        <Button>上传logo</Button>
                                                        </WeaUpload>
                                                   </div>
                                                   <div className='opt-div'><Button onClick={this.openPictureLib.bind(this)}>从素材库中选择</Button></div>
                                                </div>
                                                <img className="e9login-logo-img" src={logoImage} alt="logo加载失败" />
                                            </div> : ''}
                                        </div>
                                        <div className="e9login-form-box" style={formStyle} onClick={onSelect.bind(this,'form')} onMouseOver={handleOnMouseOver.bind(this,'form')}>
                                            <div className="e9login-setting-btns-opts area-opts" style={{display:selected === 'form'?'':'none'}}>
                                                 <ColorPicker color={spFontColor} alpha={formOpacity*100}
                                                    onChange={onChangeColor.bind(this,'fontColor')}
                                                    className="portal-p4e-e9login-colorpicker"
                                                    children={
                                                       <Button>字体颜色</Button>
                                                 }/>
                                            </div>
                                          <E9MultiLang /> {isFormLogin ? <E9Form type={displayMode}/> : <E9QRCode />}
                                       </div>
                                       <div style={{height:'20px',width:'100%'}}></div>
                                    </div>
                                </div>
                            </WeaDraggable>
                        {changeBgImage ? <E9BgImages /> : ''}
                        {bgImageHtml}
                    {showFooter ? <div className="e9login-setting-footer" style={footerStyle} onClick={onSelect.bind(this,'footer')} onMouseOver={handleOnMouseOver.bind(this,'footer')}>
                                    <div className="e9login-setting-btns-footer-opts" style={{display:selected === 'footer'?'':'none'}}>
                                    <ColorPicker color={spFontColor}
                                        onChange={onChangeColor.bind(this,'fontColor')}
                                        className="portal-p4e-e9login-colorpicker"
                                        children={
                                           <Button>字体颜色</Button>
                                    }/>
                                    <div style={{display:'inline-block',width:'10px'}}/>
                                        <ColorPicker color={spBgColor} alpha={footerOpacity*100}
                                            onChange={onChangeColor.bind(this,'bgColor')}
                                            className="portal-p4e-e9login-colorpicker"
                                            children={
                                               <Button>背景颜色</Button>
                                        }/>
                                    </div>
                         <input type="text" value={footerContent} disabled={displayMode === 'preview'} onChange={e => setFormItemValue('footerContent',e.target.value)}></input>
                    </div> : ''}
                    </div>
           </div>
    }
}
export default E9Login;