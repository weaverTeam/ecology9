import { inject, observer } from 'mobx-react';
import { Popover, Button, InputNumber } from 'antd';
import { WeaUpload, WeaInput, WeaCheckbox } from 'ecCom';
import PortalComForm from '../../../common/PortalComForm';
import PortalComFormItem from '../../../common/PortalComFormItem';
@inject('login4e9_setting_store')
@inject('loginportal_setting_store')
@observer
class LoginSetting extends React.Component{
    openPictureLib(){
        var isSingle = true;
        var tempFile='';
        var pos=tempFile.indexOf("/page/resource/userfile/");
        if(pos!=-1) tempFile=tempFile.substring(pos+24);
        var dlg = new window.top.Dialog();//定义Dialog对象
        dlg.currentWindow = window;   
        dlg.Model=true;
        dlg.Width = top.document.body.clientWidth-100;
        dlg.Height = top.document.body.clientHeight-100;
        dlg.URL="/page/maint/common/CustomResourceMaint.jsp?isDialog=1&?file="+tempFile+"&isSingle="+isSingle;
        dlg.callbackfun = (obj,datas) => {
            if (datas!=null&&datas.id!="false"){
                let src = datas.id;
                if(src.indexOf(",") !== -1){
                    src = src.split(",")[0];
                }
                const { login4e9_setting_store, loginportal_setting_store } = this.props;
                const { toolbar, changeBgImages, changeLoginBgImage } = login4e9_setting_store;
    			let bgImages = toolbar.bgImages;
    			if(!this.contains(bgImages, src)) {
    				bgImages.push(src);
    				changeBgImages(bgImages);
		    		changeLoginBgImage(bgImages.length-1, src, loginportal_setting_store.settingdata.isCurrent);
		    	}
            }
        }
        dlg.show();
    }
	changeAutoCarousel(value){
		const { login4e9_setting_store, loginportal_setting_store } = this.props;
		const { toolbar, settings, setFormItemValue, changeLoginBgImage } = login4e9_setting_store;
		if(settings.autoCarousel){
			changeLoginBgImage(toolbar.initialSlide,toolbar.bgImages[toolbar.initialSlide],loginportal_setting_store.settingdata.isCurrent);
		}
		setFormItemValue('autoCarousel',value == '1');
	}
	contains(arr, obj) {
	  var i = arr.length;
	  while (i--) {
	    if (arr[i] === obj) {
	      return true;
	    }
	  }
	  return false;
	}
    onFileChange = (ids, list) => {
    	const { loginportal_setting_store, login4e9_setting_store } = this.props;
    	const { toolbar, changeBgImages, changeLoginBgImage } = login4e9_setting_store;
    	let bgImages = toolbar.bgImages;
    	list.map((item)=>{
    		if(!this.contains(bgImages, item.filelink)) bgImages.push(item.filelink);
    	});
    	changeBgImages(bgImages);
    	changeLoginBgImage(bgImages.length-1,bgImages[bgImages.length-1], loginportal_setting_store.settingdata.isCurrent);
    }
    delBgImages(index){
    	const { toolbar, changeBgImages } = this.props.login4e9_setting_store;
    	let bgImages = toolbar.bgImages;
    	bgImages.map((item)=>{
    		if(this.contains(bgImages, bgImages[index])) {
    			bgImages.remove(bgImages[index]);
    			changeBgImages(bgImages);
    			return;
    		};
    	});
    }
    onDelIconVisible(type, e){
    	var dom = e.currentTarget;
    	const display = type === 'over' ? 'block' : 'none';
    	$(dom).find(".bgimage-del").css({display:display});
    }
	getBgImages(){
		const { login4e9_setting_store, loginportal_setting_store } = this.props;
		const { toolbar, settings, changeLoginBgImage } = login4e9_setting_store;
		const { bgImages } = toolbar;
		const { bgImage } = settings;
		let images = [];
		bgImages.map((imgSrc,i) =>{
			if(i!==0) images.push(<div style={{width:'10px',display:'inline-block'}}/>);
            let class4selected = bgImage == imgSrc ? 'e9login-bg-images-image-selected' : '';
            images.push(<div className="e9login-bg-images-img-div" onMouseOver={this.onDelIconVisible.bind(this,'over')} onMouseOut={this.onDelIconVisible.bind(this,'out')}><img className={`e9login-bg-images-image ${class4selected}`}
                         src={imgSrc}
                         alt=""
                         onClick={changeLoginBgImage.bind(this, i, imgSrc, loginportal_setting_store.settingdata.isCurrent)}
                    />
					<div className="bgimage-del" onClick={this.delBgImages.bind(this,i)}><span>×</span></div>
                    </div>);
        });
        return <div className="e9login-bars-bg-images-container">
        		<div className="e9login-bg-images-title">
			        {'单击设置默认背景（建议尺寸1920*1080）'}
			    </div>
			    <div className="e9login-bg-images-images">
			        {images}
			    </div>
			    <div className="e9login-bg-images-btns">
			    	<span style={{width:'80px',height:'28px',display:'inline-block',marginRight:'20px'}}>
			    	<WeaUpload
			    		value="1"
						uploadId="loginportalupload"
						uploadUrl="/api/portal/logintemplate/upload"
						category="0"
						viewAttr="2"
						limitType="png,gif,jpeg,jpg,bmp"
						onChange={this.onFileChange}
						showClearAll={true}>
			    	<Button type="primary">直接上传</Button>
			    	</WeaUpload></span>
    				<Button onClick={this.openPictureLib.bind(this)}>从素材库中选择</Button>
		       	</div>
			</div>
	}
	render(){
		const { login4e9_setting_store } = this.props;
		const { toolbar, settings, setFormItemValue, setToolbarValue } = login4e9_setting_store;
        const { selected, fontColorDisable, bgColorDisable, replacePicturesDisable, libModalVisible, helpVisible } = toolbar;
		const { isLock, fontColor, logoImage, bgImage, areaBgColor, autoCarousel, changeBgImage, carouselTime, loginTemplateTitle, showFooter, showQrcode, qrcodeMiddle, footerColor, footerBgColor } = settings;
		let spFontColor = selected === 'footer' ? footerColor : fontColor;
		let spBgColor = selected === 'footer' ? footerBgColor : areaBgColor;
		let imageValue = selected === 'logo' ? logoImage : bgImage;
       	return <div style={{height:'100%'}}>
		        <Popover placement="bottomLeft" content={this.getBgImages()} trigger="click">
					<div className='e9login-setting-toolbar-module e9login-toolbar-color'>
						<div className="e9login-setting-toolbar-color-icon"><i className='icon-coms-photo'/></div>
			        	<div><span>背景图片</span></div>
			        </div>
				</Popover>
		        <div className="e9login-setting-toolbar-module e9login-toolbar-carousel">
					<div className="e9login-setting-toolbar-module-div">
						<WeaCheckbox value={autoCarousel} onChange={this.changeAutoCarousel.bind(this)}/>
				       	<div className="e9login-setting-label" onClick={this.changeAutoCarousel.bind(this, autoCarousel ? '0' : '1')}>自动轮播</div>
				        {autoCarousel ? <div className="e9login-setting-label">轮播速度:</div> : ''}
				        {autoCarousel ? <InputNumber defaultValue={carouselTime} onChange={setFormItemValue.bind(this,'carouselTime')} min={1000} style={{width:100}}/> : ''}
			        </div>
			        <div className="e9login-setting-toolbar-module-div">
			        	<WeaCheckbox value={changeBgImage} onChange={value => setFormItemValue('changeBgImage',value == '1')}/>
				       	<div className="e9login-setting-label" onClick={setFormItemValue.bind(this, 'changeBgImage', !changeBgImage)}>允许前端手动切换背景</div>
			        </div>
		        </div>
		        <div className="e9login-setting-toolbar-module e9login-toolbar-line"></div>
		       	<div className={`e9login-setting-toolbar-module e9login-toolbar-color ${isLock ? 'e9login-toolbar-visiable': 'e9login-toolbar-disable'}`}>
					<div className="e9login-setting-toolbar-color-icon" onClick={setFormItemValue.bind(this,'isLock',true)}><i className='icon-coms-locking' /></div>
		        	<div><span>锁定</span></div>
		        </div>
		        <div className={`e9login-setting-toolbar-module e9login-toolbar-color ${isLock ? 'e9login-toolbar-disable': 'e9login-toolbar-visiable'}`}>
					<div className="e9login-setting-toolbar-color-icon" onClick={setFormItemValue.bind(this,'isLock',false)}><i className='icon-coms-unlock' /></div>
		        	<div><span>取消锁定</span></div>
		        </div>
		        <div className="e9login-setting-toolbar-module e9login-toolbar-line"></div>
		        <div className="e9login-setting-toolbar-module e9login-toolbar-title">
					<div className="e9login-setting-toolbar-module-div">
						<div className="e9login-setting-label">登录窗口标题</div>
						<WeaInput value={loginTemplateTitle} onChange={setFormItemValue.bind(this,'loginTemplateTitle')} style={{width:'200px'}}/>
					</div>
		        	<div className="e9login-setting-toolbar-module-div">
		        		<WeaCheckbox value={showFooter} onChange={value => setFormItemValue('showFooter',value == '1')}/>
				       	<div className="e9login-setting-label" onClick={setFormItemValue.bind(this,'showFooter',!showFooter)}>显示页脚</div>
						<WeaCheckbox value={showQrcode} onChange={value => setFormItemValue('showQrcode',value == '1')}/>
   					    <div className="e9login-setting-label" onClick={setFormItemValue.bind(this,'showQrcode',!showQrcode)}>显示二维码</div>
   					    {showQrcode ? <WeaCheckbox value={qrcodeMiddle} onChange={value => setFormItemValue('qrcodeMiddle',value == '1')}/> : '' }
				        {showQrcode ? <div className="e9login-setting-label" onClick={setFormItemValue.bind(this,'qrcodeMiddle',!qrcodeMiddle)}>二维码居中显示</div> : ''}
			     	</div>
		        </div>
		       	<div className="e9login-setting-toolbar-help">
					<div className="e9login-setting-toolbar-color-help" onClick={setToolbarValue.bind(this,'helpVisible',!helpVisible)}><i className='icon-coms-help' /></div>
		        	<div><span>{helpVisible ? '隐藏' : '显示'}说明</span></div>
		        </div>
        </div>
	}
}
module.exports = LoginSetting;
