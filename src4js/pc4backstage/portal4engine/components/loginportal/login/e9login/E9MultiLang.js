import { inject, observer } from 'mobx-react';
@inject('login4e9_setting_store')
@observer
class E9MultiLang extends React.Component {
    onSelectLang(e) {
        const { login4e9_setting_store } = this.props;
        const { multiLangVisible, selectLang } = login4e9_setting_store;
        let langId = e.target.attributes['data-lang-id'].value;
        let langText = e.target.innerText;
        selectLang(!multiLangVisible, langId, langText);
    }

    render() {
        const { login4e9_setting_store } = this.props;
        const {hasMultiLang, multiLangVisible, langId, langText, settings, changeMultiLangVisible, changeLoginType } = login4e9_setting_store;
        const { fontColor, showQrcode, qrcodeMiddle } = settings;
        if (!hasMultiLang) {
            return (
                <div className="e9login-multiLang">
                    <input type="hidden" id="langId" name="langId" value={langId} />
                </div>
            )
        } else {
            let multiLangListHTML = '';
            let class4selected = '';
            if (multiLangVisible) {
                class4selected = 'e9login-multiLangText-selected';
                multiLangListHTML = (
                    <div className="e9login-multiLangList">
                        <ul onClick={this.onSelectLang.bind(this)}>
                            <li data-lang-id="7" className={langId == '7' ? 'e9login-multiLangList-selected' : ''}>简体中文</li>
                            <li data-lang-id="8" className={langId == '8' ? 'e9login-multiLangList-selected' : ''}>English</li>
                            <li data-lang-id="9" className={langId == '9' ? 'e9login-multiLangList-selected' : ''}>繁体中文</li>
                        </ul>
                    </div>
                );
            }

            return (
                <div className="e9login-multiLang">
                    <input type="hidden" id="langId" name="langId" value={langId} />
                    <span className={`e9login-multiLangText ${class4selected}`} onClick={changeMultiLangVisible}>
                        <span style={{color:fontColor}}>{langText}</span>
                        <i style={{color:fontColor}} className="wevicon wevicon-e9login-arrow" />
                        
                    </span>
                    {showQrcode && qrcodeMiddle ? <span className="e9login-multiLangText-sacn-span"><i style={{color:fontColor}} className='icon-coms-Scan e9login-multiLangText-scan' onClick={changeLoginType}/></span> : ''}
                    {multiLangListHTML}
                </div>
            )
        }
    }
}
module.exports = E9MultiLang;