import { inject, observer } from 'mobx-react';
import { InputNumber } from "antd";
import { WeaInput, WeaCheckbox } from 'ecCom';
@inject('loginportal_setting_store')
@inject('login4e9_setting_store')
@observer
class E9BgImages extends React.Component {
     changeAutoCarousel(value){
        const { login4e9_setting_store, loginportal_setting_store } = this.props;
        const { toolbar, settings, setFormItemValue, changeLoginBgImage } = login4e9_setting_store;
        if(settings.autoCarousel){
            changeLoginBgImage(toolbar.initialSlide,toolbar.bgImages[toolbar.initialSlide],loginportal_setting_store.settingdata.isCurrent);
        }
        setFormItemValue('autoCarousel',value == '1');
    }
    render() {
        const { login4e9_setting_store, loginportal_setting_store } = this.props;
        const { toolbar, settings, changeLoginBgImage, setFormItemValue, setToolbarValue } = login4e9_setting_store
        const { bgImages, bgImagesBoxVisible  } = toolbar;
        const { bgImage, autoCarousel, carouselTime } = settings;
        let images = [];
        bgImages.map((imgSrc,i) =>{
            let class4selected = bgImage == imgSrc ? 'e9login-bg-images-image-selected' : '';
            if(i < 24){
                images.push(<img className={`e9login-bg-images-image ${class4selected}`}
                     src={imgSrc}
                     alt=""
                     onClick={changeLoginBgImage.bind(this,i,imgSrc, loginportal_setting_store.settingdata.isCurrent)}
                />); 
            }  
        });
        return (
            <div className="e9login-bg-images">
                <div className="e9login-bg-images-btn" onClick={setToolbarValue.bind(this,'bgImagesBoxVisible', !bgImagesBoxVisible)}>
                    <div className="e9login-bg-images-shadow"></div>
                    <div className="e9login-bg-images-btn-icon">
                        <i className="wevicon wevicon-e9login-palette" />
                    </div>
                </div>
                { bgImagesBoxVisible ? (
                            <div className="e9login-bg-images-container">
                                <div className="e9login-bg-images-toolbar">
                                    <div className="e9login-setting-label">单击切换背景</div>
                                    <WeaCheckbox value={autoCarousel} onChange={this.changeAutoCarousel.bind(this)}/>
                                    <div className="e9login-setting-label" onClick={this.changeAutoCarousel.bind(this, autoCarousel ? '0' : '1')}>自动轮播</div>
                                    {autoCarousel ? <div className="e9login-setting-label">轮播速度:</div> : ''}
                                    {autoCarousel ? <InputNumber defaultValue={carouselTime} onChange={setFormItemValue.bind(this,'carouselTime')} min={1000} style={{width:50,marginTop:'-4px'}}/> : ''}
                                </div>
                                <div className="e9login-bg-images-shadow" style={{background:'#FFFFFF',opacity:1}}></div>
                                <div className="e9login-bg-images-images">
                                    {images}
                                </div>
                            </div>
                        ) : ''
                }
            </div>
        )
    }
}
module.exports = E9BgImages;