const collision = (a,b,ap, bp) => {
      var ax = ap.x;
      var ay = ap.y;
      var aw = a.offsetWidth;
      var ah = a.offsetHeight;
      var bx = bp.x;
      var by = bp.y;
      var bw = b.offsetWidth;
      var bh = b.offsetHeight;
      return (ax + aw > bx && ax < bx + bw && ay + ah > by && ay < by + bh);
}
/*16进制颜色转为RGB格式*/ 
const colorRgb = function(str, a = 1){  
    //十六进制颜色值的正则表达式  
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;  
    var color = { a };
    var sColor = str.toLowerCase();  
    if(sColor && reg.test(sColor)){  
        if(sColor.length === 4){  
            var sColorNew = "#";  
            for(var i=1; i<4; i+=1){  
                sColorNew += sColor.slice(i,i+1).concat(sColor.slice(i,i+1));     
            }  
            sColor = sColorNew;  
        }  

        color.r = parseInt("0x"+sColor.slice(1,3));
        color.g = parseInt("0x"+sColor.slice(3,5));
        color.b = parseInt("0x"+sColor.slice(5,7));
        return color;  
    }else{  
        return color;    
    }  
}

module.exports = {
    collision,
    colorRgb
}
window.colorRgb = colorRgb;