import { inject, observer } from 'mobx-react';
import PropTyps from 'react-router/lib/PropTypes';
import QRCode from 'qrcode.react';
@inject('login4e9_setting_store')
@observer
class E9QRCode extends React.Component {
    static contextTypes = {
        router: PropTyps.routerShape
    };

    componentDidMount() {
        this.getQRCode();
    }

    componentDidUpdate() {
        this.getQRCode();
    }

    componentWillUnmount() {
       window.clearInterval(this.scanInterval);
    }

    getQRCode() {
/*        const { login4e9_setting_store } = this.props;
        const { loginQRCode } = login4e9_setting_store;
        if (loginQRCode != '') {
            jQuery('#QRCodeArea').html('');
            jQuery('#QRCodeArea').qrcode({
                render: 'div',
                text: 'ecologylogin:' + loginQRCode,
                size: 175,
                background: 'none',
                fill: '#424345'
            });
        }*/
    }
    render() {
        const { login4e9_setting_store } = this.props;
        const { loginQRCode } = login4e9_setting_store;
        return (
            <div className="e9login-QRCode">
                <div className="e9login-QRCode-area">
                <QRCode value={loginQRCode} size={175}/>
                </div>
                <div className="e9login-QRCode-describe">请使用e-mobile扫描二维码以登录</div>
            </div>
        )
    }
}

module.exports = E9QRCode;