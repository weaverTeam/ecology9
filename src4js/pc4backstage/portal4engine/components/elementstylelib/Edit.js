import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Tabs, Radio, InputNumber, Checkbox} from 'antd';
const TabPane = Tabs.TabPane;
const RadioGroup = Radio.Group;
import {WeaDialog, WeaRightMenu, WeaScroll, WeaTab, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

import EditGroup from '../menustylelib/EditGroup';
import PortalComMaterialLib from '../common/PortalComMaterialLib';

import Item from './Item';

import 'rc-color-picker/assets/index.css';
import ColorPicker from 'rc-color-picker';

@inject('portalElementStyleLibStore')
@observer
class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalElementStyleLibStore} = this.props;
        const {editDialogVisible, editData} = portalElementStyleLibStore;
        const {
            title, desc, style = {},
            cornerTop, cornerBottom, cornerTopRadian, cornerBottomRadian, imgMode,
            titleState, settingState, refreshIconState, settingIconState, closeIconState, moreLocal,
            iconLogo, iconLock, iconUnLock, iconRefresh, iconSetting, iconClose, iconMore, iconEsymbol
        } = editData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={editDialogVisible}
                title="编辑"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 1000, height: 600}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <div className="portal-p4e-msl-edit">
                        <Row className="portal-p4e-msl-er">
                            <Col span={6}>样式名称</Col>
                            <Col span={18}>
                                <WeaInput value={title} onChange={value => this.onChange('title', value)}/>
                            </Col>
                        </Row>
                        <Row className="portal-p4e-msl-er">
                            <Col span={6}>样式描述</Col>
                            <Col span={18}>
                                <WeaInput value={desc} onChange={value => this.onChange('desc', value)}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Tabs className="portal-p4e-msl-tabs" defaultActiveKey="1">
                                    <TabPane tab="标题" key="1">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="基本">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>高度</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.headerHeight || 0, 10)} onChange={value => this.onChange('headerHeight', value + 'px', '.header', 'height')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图标</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconLogo} onChange={value => this.onChange('iconLogo', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图标位置</Col>
                                                    <Col span={18}>
                                                        <span>距上</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.headerIconTop || 0, 10)} onChange={value => this.onChange('headerIconTop', value + 'px', '.icon', 'top')}/>
                                                        <span>距左</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.headerIconLeft || 0, 10)} onChange={value => this.onChange('headerIconLeft', value + 'px', '.icon', 'left')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>标题位置</Col>
                                                    <Col span={18}>
                                                        <span>距上</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.headerTitleTop || 0, 10)} onChange={value => this.onChange('headerTitleTop', value + 'px', '.title', 'top')}/>
                                                        <span>距左</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.headerTitleLeft || 0, 10)} onChange={value => this.onChange('headerTitleLeft', value + 'px', '.title', 'left')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>工具条位置</Col>
                                                    <Col span={18}>
                                                        <span>距上</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.headerToolbarTop || 0, 10)} onChange={value => this.onChange('headerToolbarTop', value + 'px', '.toolbar', 'top')}/>
                                                        <span>距右</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.headerToolbarRight || 0, 10)} onChange={value => this.onChange('headerToolbarRight', value + 'px', '.toolbar', 'right')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="标题">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.headerTitleColor} onChange={value => this.onChange('headerTitleColor', value.color, '.title', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>选中背景色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.headerHoverBgColor} onChange={value => this.onChange('headerHoverBgColor', value.color, '.header:hover', 'background-color')}/>
                                                        <Checkbox style={{marginLeft: '10px'}} checked={style.headerHoverBgColorSwitch == 1} onChange={e => this.onChange('headerHoverBgColorSwitch', e.target.checked ? 1 : 0, '.header:hover', 'background-color')}>开关</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>下边框颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.headerTitleBorderBottomColor} onChange={value => this.onChange('headerTitleBorderBottomColor', value.color, '.title', 'border-bottom-color')}/>
                                                        <Checkbox style={{marginLeft: '10px'}} checked={style.headerTitleBorderBottomSwitch == 1} onChange={e => this.onChange('headerTitleBorderBottomSwitch', e.target.checked ? 1 : 0, '.title', 'border-bottom')}>开关</Checkbox>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.headerTitleFontSize, 10)} onChange={value => this.onChange('headerTitleFontSize', value + 'px', '.title', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '100px'}} value={style.headerTitleFontFamily} onChange={value => this.onChange('headerTitleFontFamily', value, '.title', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.headerTitleFontWeight == 'bold'} onChange={e => this.onChange('headerTitleFontWeight', e.target.checked ? 'bold' : 'normal', '.title', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.headerTitleFontStyle == 'italic'} onChange={e => this.onChange('headerTitleFontStyle', e.target.checked ? 'italic' : 'normal', '.title', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.headerBackgroundColor == 'transparent'} onChange={e => this.onChange('headerBackgroundColor', e.target.checked ? 'transparent' : '#fff', '.header', 'background-color')}>透明</Checkbox>
                                                        {style.headerBackgroundColor != 'transparent' ? <ColorPicker animation="slide-up" color={style.headerBackgroundColor} onChange={value => this.onChange('headerBackgroundColor', value.color, '.header', 'background-color')}/> : ''}
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.headerBackgroundImage} onChange={value => this.onChange('headerBackgroundImage', value, '.header', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>模式</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBackgroundRepeat} onChange={e => this.onChange('headerBackgroundRepeat', e.target.value, '.header', 'background-repeat')}>
                                                            <Radio value="repeat">横纵平铺</Radio>
                                                            <Radio value="repeat-x">横向平铺</Radio>
                                                            <Radio value="repeat-y">纵向平铺</Radio>
                                                            <Radio value="no-repeat">均不平铺</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>横向</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBackgroundPositionX} onChange={e => this.onChange('headerBackgroundPositionX', e.target.value, '.header', 'background-position-x')}>
                                                            <Radio value="left">左</Radio>
                                                            <Radio value="center">中</Radio>
                                                            <Radio value="right">右</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>纵向</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBackgroundPositionY} onChange={e => this.onChange('headerBackgroundPositionY', e.target.value, '.header', 'background-position-y')}>
                                                            <Radio value="top">上</Radio>
                                                            <Radio value="center">中</Radio>
                                                            <Radio value="bottom">下</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="上边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBorderTopStyle } onChange={e => this.onChange('headerBorderTopStyle', e.target.value, '.header', 'border-top-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.headerBorderTopWidth, 10)} onChange={value => this.onChange('headerBorderTopWidth', value + 'px', '.header', 'border-top-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.headerBorderTopColor} onChange={value => this.onChange('headerBorderTopColor', value.color, '.header', 'border-top-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="下边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBorderBottomStyle } onChange={e => this.onChange('headerBorderBottomStyle', e.target.value, '.header', 'border-bottom-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.headerBorderBottomWidth, 10)} onChange={value => this.onChange('headerBorderBottomWidth', value + 'px', '.header', 'border-bottom-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.headerBorderBottomColor} onChange={value => this.onChange('headerBorderBottomColor', value.color, '.header', 'border-bottom-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="左边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBorderLeftStyle } onChange={e => this.onChange('headerBorderLeftStyle', e.target.value, '.header', 'border-left-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.headerBorderLeftWidth, 10)} onChange={value => this.onChange('headerBorderLeftWidth', value + 'px', '.header', 'border-left-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.headerBorderLeftColor} onChange={value => this.onChange('headerBorderLeftColor', value.color, '.header', 'border-left-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="右边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.headerBorderRightStyle } onChange={e => this.onChange('headerBorderRightStyle', e.target.value, '.header', 'border-right-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.headerBorderRightWidth, 10)} onChange={value => this.onChange('headerBorderRightWidth', value + 'px', '.header', 'border-right-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.headerBorderRightColor} onChange={value => this.onChange('headerBorderRightColor', value.color, '.header', 'border-right-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                    <TabPane tab="内容" key="2">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="标题">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.contentFontColor} onChange={value => this.onChange('contentFontColor', value.color, '.font', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>选中颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.contentFontHoverColor} onChange={value => this.onChange('contentFontHoverColor', value.color, '.font:hover', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.contentFontSize, 10)} onChange={value => this.onChange('contentFontSize', value + 'px', '.font', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '100px'}} value={style.contentFontFamily} onChange={value => this.onChange('contentFontFamily', value, '.font', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.contentFontWeight == 'bold'} onChange={e => this.onChange('contentFontWeight', e.target.checked ? 'bold' : 'normal', '.font', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.contentFontStyle == 'italic'} onChange={e => this.onChange('contentFontStyle', e.target.checked ? 'italic' : 'normal', '.font', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <Col span={18}>
                                                            <Checkbox checked={style.contentBackgroundColor == 'transparent'} onChange={e => this.onChange('contentBackgroundColor', e.target.checked ? 'transparent' : '#fff', '.content', 'background-color')}>透明</Checkbox>
                                                            {style.contentBackgroundColor != 'transparent' ? <ColorPicker animation="slide-up" color={style.contentBackgroundColor} onChange={value => this.onChange('contentBackgroundColor', value.color, '.content', 'background-color')}/> : ''}
                                                        </Col>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.contentBackgroundImage} onChange={value => this.onChange('contentBackgroundImage', value, '.content', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>模式</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBackgroundRepeat} onChange={e => this.onChange('contentBackgroundRepeat', e.target.value, '.content', 'background-repeat')}>
                                                            <Radio value="repeat">横纵平铺</Radio>
                                                            <Radio value="repeat-x">横向平铺</Radio>
                                                            <Radio value="repeat-y">纵向平铺</Radio>
                                                            <Radio value="no-repeat">均不平铺</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>横向</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBackgroundPositionX} onChange={e => this.onChange('contentBackgroundPositionX', e.target.value, '.content', 'background-position-x')}>
                                                            <Radio value="left">左</Radio>
                                                            <Radio value="center">中</Radio>
                                                            <Radio value="right">右</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>纵向</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBackgroundPositionY} onChange={e => this.onChange('contentBackgroundPositionY', e.target.value, '.content', 'background-position-y')}>
                                                            <Radio value="top">上</Radio>
                                                            <Radio value="center">中</Radio>
                                                            <Radio value="bottom">下</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="上边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBorderTopStyle } onChange={e => this.onChange('contentBorderTopStyle', e.target.value, '.content', 'border-top-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.contentBorderTopWidth, 10)} onChange={value => this.onChange('contentBorderTopWidth', value + 'px', '.content', 'border-top-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.contentBorderTopColor} onChange={value => this.onChange('contentBorderTopColor', value.color, '.content', 'border-top-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="下边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBorderBottomStyle } onChange={e => this.onChange('contentBorderBottomStyle', e.target.value, '.content', 'border-bottom-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.contentBorderBottomWidth, 10)} onChange={value => this.onChange('contentBorderBottomWidth', value + 'px', '.content', 'border-bottom-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.contentBorderBottomColor} onChange={value => this.onChange('contentBorderBottomColor', value.color, '.content', 'border-bottom-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="左边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBorderLeftStyle } onChange={e => this.onChange('contentBorderLeftStyle', e.target.value, '.content', 'border-left-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.contentBorderLeftWidth, 10)} onChange={value => this.onChange('contentBorderLeftWidth', value + 'px', '.content', 'border-left-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.contentBorderLeftColor} onChange={value => this.onChange('contentBorderLeftColor', value.color, '.content', 'border-left-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="右边框">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.contentBorderRightStyle } onChange={e => this.onChange('contentBorderRightStyle', e.target.value, '.content', 'border-right-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.contentBorderRightWidth)} onChange={value => this.onChange('contentBorderRightWidth', value + 'px', '.content', 'border-right-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.contentBorderRightColor} onChange={value => this.onChange('contentBorderRightColor', value.color, '.content', 'border-right-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="其它">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>行小图标</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconEsymbol} onChange={value => this.onChange('iconEsymbol', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>分隔线</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.contentSeparator} onChange={value => this.onChange('contentSeparator', value, '.sparator', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>内容边距</Col>
                                                    <Col span={18}>
                                                        <span>距上</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.contentPaddingTop, 10)} onChange={value => this.onChange('contentPaddingTop', value + 'px', '.content', 'padding-top')}/>
                                                        <span>距下</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.contentPaddingBottom, 10)} onChange={value => this.onChange('contentPaddingBottom', value + 'px', '.content', 'padding-bottom')}/>
                                                        <br/>
                                                        <span>距左</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.contentPaddingLeft, 10)} onChange={value => this.onChange('contentPaddingLeft', value + 'px', '.content', 'padding-left')}/>
                                                        <span>距右</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.contentPaddingRight, 10)} onChange={value => this.onChange('contentPaddingRight', value + 'px', '.content', 'padding-right')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                    <TabPane tab="其它" key="3">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="基本">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>上边角</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={cornerTop} onChange={e => this.onChange('cornerTop', e.target.value, '.header')}>
                                                            <Radio value="Right">直角</Radio>
                                                            <Radio value="Round">圆角</Radio>
                                                        </RadioGroup>
                                                        <WeaInput style={{width: '50px'}} value={cornerTopRadian} onChange={value => this.onChange('cornerTopRadian', value, '.header')}/> </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>下边角</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={cornerBottom} onChange={e => this.onChange('cornerBottom', e.target.value, '.content')}>
                                                            <Radio value="Right">直角</Radio>
                                                            <Radio value="Round">圆角</Radio>
                                                        </RadioGroup>
                                                        <WeaInput style={{width: '50px'}} value={cornerBottomRadian} onChange={value => this.onChange('cornerBottomRadian', value, '.content')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>标题栏</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={titleState} onChange={e => this.onChange('titleState', e.target.value)}>
                                                            <Radio value="show">显示</Radio>
                                                            <Radio value="hidden">隐藏</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>设置栏</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={settingState} onChange={e => this.onChange('settingState', e.target.value)}>
                                                            <Radio value="show">显示</Radio>
                                                            <Radio value="hidden">隐藏</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>刷新</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={refreshIconState} onChange={e => this.onChange('refreshIconState', e.target.value)}>
                                                            <Radio value="show">显示</Radio>
                                                            <Radio value="hidden">隐藏</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>设置</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={settingIconState} onChange={e => this.onChange('settingIconState', e.target.value)}>
                                                            <Radio value="show">显示</Radio>
                                                            <Radio value="hidden">隐藏</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>关闭</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={closeIconState} onChange={e => this.onChange('closeIconState', e.target.value)}>
                                                            <Radio value="show">显示</Radio>
                                                            <Radio value="hidden">隐藏</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>更多</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={moreLocal} onChange={e => this.onChange('moreLocal', e.target.value)}>
                                                            <Radio value="title">标题</Radio>
                                                            <Radio value="footer">底部</Radio>
                                                            <Radio value="hidden">隐藏</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图显</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={imgMode} onChange={e => this.onChange('imgMode', e.target.value)}>
                                                            <Radio value="page">页面</Radio>
                                                            <Radio value="flash">Flash</Radio>
                                                        </RadioGroup>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="图标">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>锁定</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconLock} onChange={value => this.onChange('iconLock', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>非锁定</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconUnLock} onChange={value => this.onChange('iconUnLock', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>刷新</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconRefresh} onChange={value => this.onChange('iconRefresh', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>设置</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconSetting} onChange={value => this.onChange('iconSetting', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>关闭</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconClose} onChange={value => this.onChange('iconClose', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>更多</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconMore} onChange={value => this.onChange('iconMore', value)}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="TAB页">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>背景条</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.tabBackgroundImage} onChange={value => this.onChange('tabBackgroundImage', value, '.tab2', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>选中前</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.tabUnSelectedBackgroundImage} onChange={value => this.onChange('tabUnSelectedBackgroundImage', value, '.tab2unselected', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>选中后</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.tabSelectedBackgroundImage} onChange={value => this.onChange('tabSelectedBackgroundImage', value, '.tab2selected', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="TAB页字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.tabUnSelectedFontColor} onChange={value => this.onChange('tabUnSelectedFontColor', value.color, '.tab2unselected', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.tabUnSelectedFontSize, 10)} onChange={value => this.onChange('tabUnSelectedFontSize', value + 'px', '.tab2unselected', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '100px'}} value={style.tabUnSelectedFontFamily} onChange={value => this.onChange('tabUnSelectedFontFamily', value, '.tab2unselected', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.tabUnSelectedFontWeight == 'bold'} onChange={e => this.onChange('tabUnSelectedFontWeight', e.target.checked ? 'bold' : 'normal', '.tab2unselected', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.tabUnSelectedFontStyle == 'italic'} onChange={e => this.onChange('tabUnSelectedFontStyle', e.target.checked ? 'italic' : 'normal', '.tab2unselected', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="TAB页选中后字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.tabSelectedFontColor} onChange={value => this.onChange('tabSelectedFontColor', value.color, '.tab2selected', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.tabSelectedFontSize, 10)} onChange={value => this.onChange('tabSelectedFontSize', value + 'px', '.tab2selected', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '100px'}} value={style.tabSelectedFontFamily} onChange={value => this.onChange('tabSelectedFontFamily', value, '.tab2selected', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.tabSelectedFontWeight == 'bold'} onChange={e => this.onChange('tabSelectedFontWeight', e.target.checked ? 'bold' : 'normal', '.tab2selected', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.tabSelectedFontStyle == 'italic'} onChange={e => this.onChange('tabSelectedFontStyle', e.target.checked ? 'italic' : 'normal', '.tab2selected', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                </Tabs>
                            </Col>
                        </Row>
                    </div>
                    <div className="portal-p4e-msl-preview">
                        <div className="portal-p4e-esl-item">
                            <style type="text/css" id="portal-p4e-esl-es"/>
                            <Item {...editData}/>
                        </div>
                    </div>
                    <div className="portal-p4e-msl-clear"></div>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value, ruleIndex, ruleId) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.editChange(field, value, ruleIndex, ruleId);
    }

    onSave() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.editSave();
    }

    onCancel() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.editCancel();
    }
}

export default Edit;
