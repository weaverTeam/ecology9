import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalElementStyleLibStore')
@observer
class Add extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalElementStyleLibStore} = this.props;
        const {addDialogVisible, addData} = portalElementStyleLibStore;
        const {title, desc, elementstyles, cite, saveAs} = addData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={addDialogVisible}
                title="新建样式"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 530, height: 240}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="样式名称">
                            <WeaInput value={title} onChange={value => this.onChange('title', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="样式描述">
                            <WeaInput value={desc} onChange={value => this.onChange('desc', value)}/>
                        </PortalComFormItem>
                        {
                            !saveAs ?
                                <PortalComFormItem label="选择已有样式	">
                                    <WeaSelect
                                        options={elementstyles}
                                        value={cite}
                                        onChange={value => this.onChange('cite', value)}
                                    />
                                </PortalComFormItem> : ''
                        }
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={() => this.onSave(true)}>保存并进入详细设置</Button>);
        buttons.push(<Button type="primary" onClick={() => this.onSave(false)}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存并进入详细设置'});
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave(true);
        } else if (key == '1') {
            this.onSave(false);
        } else if (key == '2') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.addChange(field, value);
    }

    onSave(hasEdit) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.addSave(hasEdit);
    }

    onCancel() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.addCancel();
    }
}

export default Add;
