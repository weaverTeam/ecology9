import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaDialog, WeaRightMenu, WeaUpload} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalElementStyleLibStore')
@observer
class ImportXML extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalElementStyleLibStore} = this.props;
        const {importDialogVisible} = portalElementStyleLibStore;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={importDialogVisible}
                title="导入样式"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 530, height: 240}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="文件(.xml)">
                            <form ref="StyleImport" method="post" encType="multipart/form-data" action="/weaver/weaver.page.style.StyleImpServlet">
                                <input type="hidden" name="type" value="element"/>
                                <input type="file" name="stylefile" id="stylefile" style={{width: '100%', height: '28px', lineHeight: '20px', border: '1px solid #e9e9e2'}}/>
                            </form>
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onSave() {
        this.refs['StyleImport'].submit();
    }

    onCancel() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.importCancel();
    }
}

export default ImportXML;
