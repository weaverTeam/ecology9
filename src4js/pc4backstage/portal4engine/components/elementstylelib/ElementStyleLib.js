import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaTop, WeaRightMenu, WeaTab} from 'ecCom';

import './css/index';
import Edit from './Edit';
import ListItem from './ListItem';
import Add from './Add';
import ImportXML from './ImportXML';

@inject('portalElementStyleLibStore')
@observer
class ElementStyleLib extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onImport = this.onImport.bind(this);
    }

    componentWillMount() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.getDataList();
    }

    render() {
        const {portalElementStyleLibStore} = this.props;
        const {loading, title, listData} = portalElementStyleLibStore;

        return (
            <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                <WeaTop
                    title="元素样式库"
                    icon={<i className='icon-coms-portal'/>}
                    iconBgcolor='#1a57a0'
                    loading={loading}
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenus()}
                    onDropMenuClick={this.onRightMenuClick}
                >
                    <WeaTab
                        onlyShowRight={true}
                        searchType={['base']}
                        searchsBaseValue={title}
                        onSearch={this.onSearch}
                    />
                    <div className="portal-p4e-esl">
                        <Edit />
                        <Add />
                        <ImportXML />
                        <table style={{width: '100%'}}>
                            <tbody>
                            {
                                listData.map((item) => {
                                    return (
                                        <tr>
                                            <td><ListItem {...item['col_1']}/></td>
                                            <td><ListItem {...item['col_2']}/></td>
                                            <td><ListItem {...item['col_3']}/></td>
                                        </tr>
                                    );
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </WeaTop>
            </WeaRightMenu>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onAdd}>新建</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onImport}>导入</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '新建'});
        rightMenus.push({disabled: false, icon: <Icon type="export"/>, content: '导入'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onAdd();
        } else if (key == '1') {
            this.onImport();
        }
    }

    onSearch(value) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.searchElementStyle(value);
    }

    onAdd() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.addElementStyle();
    }

    onImport() {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.importElementStyle();
    }
}

export default ElementStyleLib;
