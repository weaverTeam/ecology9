import React from 'react';

class Item extends React.Component {
    render() {
        const {
            prefix, title,
            titleState, settingState, refreshIconState, settingIconState, closeIconState, moreLocal,
            iconLogo, iconLock, iconUnLock, iconRefresh, iconSetting, iconClose, iconMore, iconEsymbol
        } = this.props;

        if (!prefix) {
            return <div></div>
        }

        return (
            <div className="item" id={`item_${prefix}`}>
                {
                    titleState == 'show' ?
                        <div className="header" id={`header_${prefix}`}>
                            <span className="icon" id={`icon_${prefix}`}>
                                <img src={iconLogo} alt=""/>
                            </span>
                            <span className="title" id={`title_${prefix}`}>{title}</span>

                            {
                                settingState == 'show' ?
                                    <span className="toolbar" id={`toolbar_${prefix}`}>
                                        <ul>
                                            <li><a href="javascript:;"><img src={iconLock} alt=""/></a></li>
                                            <li><a href="javascript:;"><img src={iconUnLock} alt=""/></a></li>
                                            {refreshIconState == 'show' ? <li><a href="javascript:;"><img src={iconRefresh} alt=""/></a></li> : ''}
                                            {settingIconState == 'show' ? <li><a href="javascript:;"><img src={iconSetting} alt=""/></a></li> : ''}
                                            {closeIconState == 'show' ? <li><a href="javascript:;"><img src={iconClose} alt=""/></a></li> : ''}
                                            {moreLocal == 'title' ? <li><a href="javascript:;"><img src={iconMore} alt=""/></a></li> : ''}
                                        </ul>
                                    </span> : ''
                            }
                        </div> : ''
                }
                <div className="content" id={`content_${prefix}`}>
                    <div className="content_view" id={`content_view_id_${prefix}`}>
                        <div className="tab2">
                            <table height="32" cellSpacing="0" cellPadding="0">
                                <tr>
                                    <td className="tab2selected">聚焦新闻</td>
                                    <td className="tab2unselected">行业聚焦</td>
                                    <td className="tab2unselected">媒体视点</td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%">
                            <tr height="18">
                                <td width="8"><img src={iconEsymbol} alt=""/></td>
                                <td width="*"><a href="javascript:;"><font className='font'>Gmail Labs推出搜索自动完成功能</font></a></td>
                                <td width="40"><font className="font">曾东平</font></td>
                            </tr>
                            <tr className='sparator' height='1px'>
                                <td colSpan="3"/>
                            </tr>
                            <tr height="18">
                                <td width="8"><img src={iconEsymbol} alt=""/></td>
                                <td width="*"><a href="javascript:;"><font className='font'>Gmail Labs推出搜索自动完成功能</font></a></td>
                                <td width="40"><font className="font">曾东平</font></td>
                            </tr>
                            <tr className='sparator' height='1px'>
                                <td colSpan="3"/>
                            </tr>
                            <tr height="18">
                                <td width="8"><img src={iconEsymbol} alt=""/></td>
                                <td width="*"><a href="javascript:;"><font className='font'>Gmail Labs推出搜索自动完成功能</font></a></td>
                                <td width="40"><font className="font">曾东平</font></td>
                            </tr>
                            <tr className='sparator' height='1px'>
                                <td colSpan="3"/>
                            </tr>
                            <tr height="18">
                                <td width="8"><img src={iconEsymbol} alt=""/></td>
                                <td width="*"><a href="javascript:;"><font className='font'>Gmail Labs推出搜索自动完成功能</font></a></td>
                                <td width="40"><font className="font">曾东平</font></td>
                            </tr>
                            <tr className='sparator' height='1px'>
                                <td colSpan="3"/>
                            </tr>
                            <tr height="18">
                                <td width="8"><img src={iconEsymbol} alt=""/></td>
                                <td width="*"><a href="javascript:;"><font className='font'>Gmail Labs推出搜索自动完成功能</font></a></td>
                                <td width="40"><font className="font">曾东平</font></td>
                            </tr>
                        </table>
                    </div>
                    <div className="footer" id={`footer_${prefix}`} style={{textAlign: 'right'}}>
                        {moreLocal == 'footer' ? <a href="javascript:;"><img src={iconMore} alt=""/></a> : ''}
                    </div>
                </div>
            </div>
        );
    }
}

export default Item;
