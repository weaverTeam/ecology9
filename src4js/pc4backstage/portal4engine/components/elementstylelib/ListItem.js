import React from 'react';
import {inject, observer} from 'mobx-react';
import {Modal} from 'antd';
const confirm = Modal.confirm;

import Item from './Item';

@inject('portalElementStyleLibStore')
@observer
class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.onMouseEnterItem = this.onMouseEnterItem.bind(this);
        this.onMouseLeaveItem = this.onMouseLeaveItem.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.onSaveAs = this.onSaveAs.bind(this);
        this.onDownload = this.onDownload.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    render() {
        const {id, css} = this.props;
        if (!id) {
            return <div></div>;
        }

        return (
            <div className="portal-p4e-esl-li" onMouseEnter={this.onMouseEnterItem} onMouseLeave={this.onMouseLeaveItem}>
                <div className="portal-p4e-esl-item">
                    <style type="text/css" dangerouslySetInnerHTML={{__html: css}}/>
                    <Item {...this.props}/>
                </div>
                <div className="portal-p4e-esl-operation" ref="operation">
                    <ul>
                        <li title="编辑" className="portal-p4e-esl-edit" onClick={() => this.onEdit(id)}/>
                        <li title="另存为" className="portal-p4e-esl-saveAs" onClick={() => this.onSaveAs(id)}/>
                        <li title="下载" className="portal-p4e-esl-download" onClick={() => this.onDownload(id)}/>
                        <li title="删除" className="portal-p4e-esl-delete" onClick={() => this.onDelete(id)}/>
                    </ul>
                </div>
            </div>
        );
    }

    onMouseEnterItem() {
        this.refs['operation'].style.height = '45px';
    }

    onMouseLeaveItem() {
        this.refs['operation'].style.height = '0';
    }

    onEdit(id) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.editElementStyle(id);
    }

    onSaveAs(id) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.addElementStyle(id);
    }

    onDownload(id) {
        const {portalElementStyleLibStore} = this.props;
        portalElementStyleLibStore.downloadElementStyle(id);
    }

    onDelete(id) {
        confirm({
            title: '信息确认',
            content: '确定要删除吗？',
            onOk: () => {
                const {portalElementStyleLibStore} = this.props;
                portalElementStyleLibStore.deleteElementStyle(id);
            }
        });
    }
}

export default ListItem;
