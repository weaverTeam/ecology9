import React from 'react';
import {inject, observer} from 'mobx-react';
import {WeaDialog} from 'ecCom';

@inject('portalMenuStyleLibStore')
@observer
class Preview extends React.Component {
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStyleLibStore} = this.props;
        const {previewDialogVisible, previewData} = portalMenuStyleLibStore;
        const {styleid, menustyletype} = previewData;
        const previewUrl = `/page/maint/style/${menustyletype == 'menuh' ? 'MenuStylePriviewH' : 'MenuStylePriviewV'}.jsp?styleid=${styleid}&type=${menustyletype}`;

        return (
            <WeaDialog
                className="portal-com-dialog"
                url={previewUrl}
                visible={previewDialogVisible}
                title={'预览'}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 530, height: menustyletype == 'menuh' ? 330 : 530}}
                onCancel={this.onCancel}
            >
            </WeaDialog>
        );
    }

    onCancel() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.previewCancel();
    }
}

export default Preview;
