const EditGroup = (props) => {
    return (
        <div className="portal-p4e-msl-eg">
            <div className="portal-p4e-msl-egt">
                <div className="portal-p4e-msl-egtl"></div>
                <div className="portal-p4e-msl-egtc">
                    <span>{props.title}</span>
                </div>
                <div className="portal-p4e-msl-egtr"></div>
                <div className="portal-p4e-msl-egtr2"></div>
            </div>
            <div className="portal-p4e-msl-egc">
                {props.children}
            </div>
        </div>
    );
};

export default EditGroup;
