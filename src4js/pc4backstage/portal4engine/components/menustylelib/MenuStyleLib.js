import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaTop, WeaRightMenu, WeaTab} from 'ecCom';
import {WeaTable} from 'comsMobx';

import './css/index';
import EditH from './EditH';
import EditV from './EditV';
import Add from './Add';
import ImportXML from './ImportXML';
import Preview from './Preview';

@inject('portalMenuStyleLibStore')
@inject('comsWeaTableStore')
@observer
class MenuStyleLib extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onImport = this.onImport.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }

    componentWillMount() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.getDataList();
    }

    render() {
        const {portalMenuStyleLibStore} = this.props;
        const {loading, menustyletype, menustylename, sessionkey} = portalMenuStyleLibStore;

        return (
            <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                <WeaTop
                    title="菜单样式库"
                    icon={<i className='icon-coms-portal'/>}
                    iconBgcolor='#1a57a0'
                    loading={loading}
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenus()}
                    onDropMenuClick={this.onRightMenuClick}
                >
                    <WeaTab
                        datas={[{key: 'menuh', title: '横向菜单样式'}, {key: 'menuv', title: '纵向菜单样式'}]}
                        keyParam='key'
                        selectedKey={menustyletype}
                        onChange={this.onTabChange}
                        searchType={['base']}
                        searchsBaseValue={menustylename}
                        onSearch={this.onSearch}
                    />
                    <WeaTable sessionkey={sessionkey} onOperatesClick={this.onOperatesClick}/>
                    <style type="text/css" id="portal-p4e-msl-es">

                    </style>
                    <EditH />
                    <EditV />
                    <Add />
                    <ImportXML />
                    <Preview />
                </WeaTop>
            </WeaRightMenu>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onAdd}>新建样式</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onImport}>导入</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onDelete}>批量删除</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '新建样式'});
        rightMenus.push({disabled: false, icon: <Icon type="export"/>, content: '导入'});
        rightMenus.push({disabled: false, icon: <Icon type="delete"/>, content: '批量删除'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onAdd();
        } else if (key == '1') {
            this.onImport();
        } else if (key == '2') {
            this.onDelete();
        }
    }

    onTabChange(key) {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.changeMenuStyleType(key);
    }

    onSearch(value) {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.searchMenuStyle(value);
    }

    onOperatesClick(record, index, operate, flag) {
        const {portalMenuStyleLibStore} = this.props;
        if (operate.index == '0') { // 预览
            portalMenuStyleLibStore.previewMenuStyle(record);

        } else if (operate.index == '1') { // 编辑
            portalMenuStyleLibStore.editMenuStyle(record);

        } else if (operate.index == '2') { // 另存为
            record.saveAs = true;
            portalMenuStyleLibStore.addMenuStyle(record);

        } else if (operate.index == '3') { // 删除
            portalMenuStyleLibStore.deleteMenuStyle(record);
        }
    }

    onAdd() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.addMenuStyle();
    }

    onImport() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.importMenuStyle();
    }

    onDelete() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.deleteMenuStyle();
    }
}

export default MenuStyleLib;
