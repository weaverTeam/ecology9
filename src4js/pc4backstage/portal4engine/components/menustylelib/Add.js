import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaSelect} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalMenuStyleLibStore')
@observer
class Add extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStyleLibStore} = this.props;
        const {addDialogVisible, addData} = portalMenuStyleLibStore;
        const {menustylename, menustyledesc, menustyletype, menustyles, menustylecite, saveAs} = addData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={addDialogVisible}
                title="新建样式"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 530, height: 240}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="样式名称">
                            <WeaInput value={menustylename} onChange={value => this.onChange('menustylename', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="样式描述">
                            <WeaInput value={menustyledesc} onChange={value => this.onChange('menustyledesc', value)}/>
                        </PortalComFormItem>
                        {
                            !saveAs ?
                                <PortalComFormItem label="菜单类型">
                                    <WeaSelect
                                        disabled={true}
                                        options={[
                                            {key: 'menuh', selected: false, showname: '横向菜单'},
                                            {key: 'menuv', selected: false, showname: '纵向菜单'}
                                        ]}
                                        value={menustyletype}
                                        onChange={value => this.onChange('menustyledtype', value)}
                                    />
                                </PortalComFormItem> : ''
                        }
                        {
                            !saveAs ?
                                <PortalComFormItem label="选择已有样式	">
                                    <WeaSelect
                                        options={menustyles}
                                        value={menustylecite}
                                        onChange={value => this.onChange('menustylecite', value)}
                                    />
                                </PortalComFormItem> : ''
                        }
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={() => this.onSave(true)}>保存并进入详细设置</Button>);
        buttons.push(<Button type="primary" onClick={() => this.onSave(false)}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存并进入详细设置'});
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave(true);
        } else if (key == '1') {
            this.onSave(false);
        } else if (key == '2') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.addChange(field, value);
    }

    onSave(hasEdit) {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.addSave(hasEdit);
    }

    onCancel() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.addCancel();
    }
}

export default Add;
