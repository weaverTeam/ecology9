import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Tabs, InputNumber, Checkbox} from 'antd';
const TabPane = Tabs.TabPane;
import {WeaDialog, WeaRightMenu, WeaScroll, WeaTab, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

import EditGroup from './EditGroup';
import PortalComMaterialLib from '../common/PortalComMaterialLib';

import 'rc-color-picker/assets/index.css';
import ColorPicker from 'rc-color-picker';

@inject('portalMenuStyleLibStore')
@observer
class EditV extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStyleLibStore} = this.props;
        const {editVDialogVisible, editData} = portalMenuStyleLibStore;
        const {title, desc, style = {}} = editData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={editVDialogVisible}
                title="菜单样式模板"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 1000, height: 600}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <div className="portal-p4e-msl-edit">
                        <Row className="portal-p4e-msl-er">
                            <Col span={6}>样式名称</Col>
                            <Col span={18}>
                                <WeaInput value={title} onChange={value => this.onChange('title', value)}/>
                            </Col>
                        </Row>
                        <Row className="portal-p4e-msl-er">
                            <Col span={6}>样式描述</Col>
                            <Col span={18}>
                                <WeaInput value={desc} onChange={value => this.onChange('desc', value)}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Tabs className="portal-p4e-msl-tabs" defaultActiveKey="1">
                                    <TabPane tab="主菜单" key="1">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainFontColor} onChange={value => this.onChange('mainFontColor', value.color, '#menuv .mainFont', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.mainFontSize, 10)} onChange={value => this.onChange('mainFontSize', value + 'px', '#menuv .mainFont', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '100px'}} value={style.mainFontFamily} onChange={value => this.onChange('mainFontFamily', value, '#menuv .mainFont', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.mainFontWeight == 'bold'} onChange={e => this.onChange('mainFontWeight', e.target.checked ? 'bold' : 'normal', '#menuv .mainFont', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.mainFontStyle == 'italic'} onChange={e => this.onChange('mainFontStyle', e.target.checked ? 'italic' : 'normal', '#menuv .mainFont', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="选中后字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainHoverFontColor} onChange={value => this.onChange('mainHoverFontColor', value.color, '#menuv .mainFont:hover', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.mainHoverFontSize, 10)} onChange={value => this.onChange('mainHoverFontSize', value + 'px', '#menuv .mainFont:hover', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '108px'}} value={style.mainHoverFontFamily} onChange={value => this.onChange('mainHoverFontFamily', value, '#menuv .mainFont:hover', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="首菜单背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainBackgroundColor} onChange={value => this.onChange('mainBackgroundColor', value.color, '#menuv .mainBg_top', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.mainBackgroundImage} onChange={value => this.onChange('mainBackgroundImage', value, '#menuv .mainBg_top', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="首菜单选中后背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainHoverBackgroundColor} onChange={value => this.onChange('mainHoverBackgroundColor', value.color, '#menuv .mainBg_top:hover', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.mainHoverBackgroundImage} onChange={value => this.onChange('mainHoverBackgroundImage', value, '#menuv .mainBg_top:hover', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="其他菜单背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.main2BackgroundColor} onChange={value => this.onChange('main2BackgroundColor', value.color, '#menuv .mainBg', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.main2BackgroundImage} onChange={value => this.onChange('main2BackgroundImage', value, '#menuv .mainBg', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="其他菜单选中后背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.main2HoverBackgroundColor} onChange={value => this.onChange('main2HoverBackgroundColor', value.color, '#menuv .mainBg:hover', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.main2HoverBackgroundImage} onChange={value => this.onChange('main2HoverBackgroundImage', value, '#menuv .mainBg:hover', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="其它">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>收缩图标</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.mainCollapsedBackgroundImage} onChange={value => this.onChange('mainCollapsedBackgroundImage', value, '#menuv .collapsed', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>展开图标</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.mainExpandedBackgroundImage} onChange={value => this.onChange('mainExpandedBackgroundImage', value, '#menuv .mainFont', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                    <TabPane tab="子菜单" key="2">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subFontColor} onChange={value => this.onChange('subFontColor', value.color, '#menuv .sub', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.subFontSize, 10)} onChange={value => this.onChange('subFontSize', value + 'px', '#menuv .sub', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '108px'}} value={style.subFontFamily} onChange={value => this.onChange('subFontFamily', value, '#menuv .sub', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.subFontWeight == 'bold'} onChange={e => this.onChange('subFontWeight', e.target.checked ? 'bold' : 'normal', '#menuv .sub', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.subFontStyle == 'italic'} onChange={e => this.onChange('subFontStyle', e.target.checked ? 'italic' : 'normal', '#menuv .sub', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="选中后字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subHoverFontColor} onChange={value => this.onChange('subHoverFontColor', value.color, '#menuv .sub:hover', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.subHoverFontSize, 10)} onChange={value => this.onChange('subHoverFontSize', value + 'px', '#menuv .sub:hover', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '108px'}} value={style.subHoverFontFamily} onChange={value => this.onChange('subHoverFontFamily', value, '#menuv .sub:hover', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subBackgroundColor} onChange={value => this.onChange('subBackgroundColor', value.color, '#menuv .sub', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.subBackgroundImage} onChange={value => this.onChange('subBackgroundImage', value, '#menuv .sub', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="选中后背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subHoverBackgroundColor} onChange={value => this.onChange('subHoverBackgroundColor', value.color, '#menuv .sub:hover', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.subHoverBackgroundImage} onChange={value => this.onChange('subHoverBackgroundImage', value, '#menuv .sub:hover', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                </Tabs>
                            </Col>
                        </Row>
                    </div>
                    <div className="portal-p4e-msl-preview">
                        <div id="menuvContainer" className="menuvContainer">
                            <div id="menuv" className="menuv">
                                <div className="mainBg_top">
                                    <a href="javascript:;" className="mainFont">Menu1</a>
                                    <a href="javascript:;" className="sub">Menu1-1</a>
                                    <a href="javascript:;" className="sub">Menu1-2</a>
                                    <a href="javascript:;" className="sub">Menu1-3</a>
                                </div>
                                <div className="mainBg">
                                    <a href="javascript:;" className="mainFont">Menu2</a>
                                    <a href="javascript:;" className="sub">Menu2-1</a>
                                    <a href="javascript:;" className="sub">Menu2-2</a>
                                    <a href="javascript:;" className="sub">Menu2-3</a>
                                </div>
                                <div className="mainBg">
                                    <a href="javascript:;" className="mainFont">Menu3</a>
                                    <a href="javascript:;" className="sub">Menu3-1</a>
                                    <a href="javascript:;" className="sub">Menu3-2</a>
                                    <a href="javascript:;" className="sub">Menu3-3</a>
                                </div>
                                <div className="mainBg">
                                    <a href="javascript:;" className="collapsed mainFont">Menu4</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="portal-p4e-msl-clear"></div>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value, ruleIndex, ruleId) {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.editChange(field, value, ruleIndex, ruleId);
    }

    onSave() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.editSave();
    }

    onCancel() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.editCancel();
    }
}

export default EditV;
