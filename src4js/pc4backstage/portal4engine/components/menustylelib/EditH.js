import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Row, Col, Tabs, Radio, InputNumber, Checkbox} from 'antd';
const TabPane = Tabs.TabPane;
const RadioGroup = Radio.Group;
import {WeaDialog, WeaRightMenu, WeaScroll, WeaTab, WeaInput, WeaSelect, WeaBrowser} from 'ecCom';

import EditGroup from './EditGroup';
import PortalComMaterialLib from '../common/PortalComMaterialLib';

import 'rc-color-picker/assets/index.css';
import ColorPicker from 'rc-color-picker';

@inject('portalMenuStyleLibStore')
@observer
class EditH extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalMenuStyleLibStore} = this.props;
        const {editHDialogVisible, editData} = portalMenuStyleLibStore;
        const {title, desc, style = {}, cornerTop, cornerBottom, cornerTopRadian, cornerBottomRadian, iconMainDown, iconSubDown} = editData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={editHDialogVisible}
                title="菜单样式模板"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 1000, height: 600}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <div className="portal-p4e-msl-edit">
                        <Row className="portal-p4e-msl-er">
                            <Col span={6}>样式名称</Col>
                            <Col span={18}>
                                <WeaInput value={title} onChange={value => this.onChange('title', value)}/>
                            </Col>
                        </Row>
                        <Row className="portal-p4e-msl-er">
                            <Col span={6}>样式描述</Col>
                            <Col span={18}>
                                <WeaInput value={desc} onChange={value => this.onChange('desc', value)}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Tabs className="portal-p4e-msl-tabs" defaultActiveKey="1">
                                    <TabPane tab="主菜单" key="1">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="菜单条背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.menuBackgroundColor} onChange={value => this.onChange('menuBackgroundColor', value.color, '#menuhContainer', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.menuBackgroundImage} onChange={value => this.onChange('menuBackgroundImage', value, '#menuhContainer', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>上边角</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={cornerTop} onChange={e => this.onChange('cornerTop', e.target.value, '#menuhContainer')}>
                                                            <Radio value="Right">直角</Radio>
                                                            <Radio value="Round">圆角</Radio>
                                                        </RadioGroup>
                                                        <WeaInput style={{width: '50px'}} value={cornerTopRadian} onChange={value => this.onChange('cornerTopRadian', value, '#menuhContainer')}/> </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>下边角</Col>
                                                    <Col span={18}>
                                                        <RadioGroup value={cornerBottom} onChange={e => this.onChange('cornerBottom', e.target.value, '#menuhContainer')}>
                                                            <Radio value="Right">直角</Radio>
                                                            <Radio value="Round">圆角</Radio>
                                                        </RadioGroup>
                                                        <WeaInput style={{width: '50px'}} value={cornerBottomRadian} onChange={value => this.onChange('cornerBottomRadian', value, '#menuhContainer')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainFontColor} onChange={value => this.onChange('mainFontColor', value.color, '#menuhContainer .main', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.mainFontSize, 10)} onChange={value => this.onChange('mainFontSize', value + 'px', '#menuhContainer .main', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '100px'}} value={style.mainFontFamily} onChange={value => this.onChange('mainFontFamily', value, '#menuhContainer .main', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.mainFontWeight == 'bold'} onChange={e => this.onChange('mainFontWeight', e.target.checked ? 'bold' : 'normal', '#menuhContainer .main', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.mainFontStyle == 'italic'} onChange={e => this.onChange('mainFontStyle', e.target.checked ? 'italic' : 'normal', '#menuhContainer .main', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="选中后字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainHoverFontColor} onChange={value => this.onChange('mainHoverFontColor', value.color, '#menuhContainer .main:hover', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.mainHoverFontSize, 10)} onChange={value => this.onChange('mainHoverFontSize', value + 'px', '#menuhContainer .main:hover', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '108px'}} value={style.mainHoverFontFamily} onChange={value => this.onChange('mainHoverFontFamily', value, '#menuhContainer .main:hover', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="菜单背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainBackgroundColor} onChange={value => this.onChange('mainBackgroundColor', value.color, '#menuhContainer .main', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.mainBackgroundImage} onChange={value => this.onChange('mainBackgroundImage', value, '#menuhContainer .main', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="菜单选中后背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.mainHoverBackgroundColor} onChange={value => this.onChange('mainHoverBackgroundColor', value.color, '#menuhContainer .main:hover', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.mainHoverBackgroundImage} onChange={value => this.onChange('mainHoverBackgroundImage', value, '#menuhContainer .main:hover', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="分隔线">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.mainBorderRightStyle } onChange={e => this.onChange('mainBorderRightStyle', e.target.value, '#menuhContainer .main', 'border-right-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.mainBorderRightWidth, 10)} onChange={value => this.onChange('mainBorderRightWidth', value + 'px', '#menuhContainer .main', 'border-right-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.mainBorderRightColor} onChange={value => this.onChange('mainBorderRightColor', value.color, '#menuhContainer .main', 'border-right-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="其它">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>子菜单图标</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconMainDown} onChange={value => this.onChange('iconMainDown', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>内容边距</Col>
                                                    <Col span={18}>
                                                        <span>距上</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.mainPaddingTop, 10)} onChange={value => this.onChange('mainPaddingTop', value + 'px', '#menuhContainer .main', 'padding-top')}/>
                                                        <span>距下</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.mainPaddingBottom, 10)} onChange={value => this.onChange('mainPaddingBottom', value + 'px', '#menuhContainer .main', 'padding-bottom')}/>
                                                        <br/>
                                                        <span>距左</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.mainPaddingLeft, 10)} onChange={value => this.onChange('mainPaddingLeft', value + 'px', '#menuhContainer .main', 'padding-left')}/>
                                                        <span>距右</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.mainPaddingRight, 10)} onChange={value => this.onChange('mainPaddingRight', value + 'px', '#menuhContainer .main', 'padding-right')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                    <TabPane tab="子菜单" key="2">
                                        <WeaScroll className="portal-p4e-msl-es" typeClass="scrollbar-macosx">
                                            <EditGroup title="字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subFontColor} onChange={value => this.onChange('subFontColor', value.color, '#menuhContainer .sub', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.subFontSize, 10)} onChange={value => this.onChange('subFontSize', value + 'px', '#menuhContainer .sub', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '108px'}} value={style.subFontFamily} onChange={value => this.onChange('subFontFamily', value, '#menuhContainer .sub', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>样式</Col>
                                                    <Col span={18}>
                                                        <Checkbox checked={style.subFontWeight == 'bold'} onChange={e => this.onChange('subFontWeight', e.target.checked ? 'bold' : 'normal', '#menuhContainer .sub', 'font-weight')}>粗体</Checkbox>
                                                        <Checkbox checked={style.subFontStyle == 'italic'} onChange={e => this.onChange('subFontStyle', e.target.checked ? 'italic' : 'normal', '#menuhContainer .sub', 'font-style')}>斜体</Checkbox>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="选中后字体">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subHoverFontColor} onChange={value => this.onChange('subHoverFontColor', value.color, '#menuhContainer .sub:hover', 'color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字号</Col>
                                                    <Col span={18}>
                                                        <InputNumber value={parseInt(style.subHoverFontSize, 10)} onChange={value => this.onChange('subHoverFontSize', value + 'px', '#menuhContainer .sub:hover', 'font-size')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>字体</Col>
                                                    <Col span={18}>
                                                        <WeaInput style={{width: '108px'}} value={style.subHoverFontFamily} onChange={value => this.onChange('subHoverFontFamily', value, '#menuhContainer .sub:hover', 'font-family')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="菜单背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subBackgroundColor} onChange={value => this.onChange('subBackgroundColor', value.color, '#menuhContainer .sub', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.subBackgroundImage} onChange={value => this.onChange('subBackgroundImage', value, '#menuhContainer .sub', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="菜单选中后背景">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>颜色</Col>
                                                    <Col span={18}>
                                                        <ColorPicker animation="slide-up" color={style.subHoverBackgroundColor} onChange={value => this.onChange('subHoverBackgroundColor', value.color, '#menuhContainer .sub:hover', 'background-color')}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>图片</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={style.subHoverBackgroundImage} onChange={value => this.onChange('subHoverBackgroundImage', value, '#menuhContainer .sub:hover', 'background-image')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="分隔线">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>类型</Col>
                                                    <Col span={18}>
                                                        <RadioGroup style={{marginBottom: '5px'}} value={style.subBorderBottomStyle } onChange={e => this.onChange('subBorderBottomStyle', e.target.value, '#menuhContainer .sub', 'border-bottom-style')}>
                                                            <Radio value="none">无</Radio>
                                                            <Radio value="solid">实线</Radio>
                                                            <Radio value="dotted">点线</Radio>
                                                            <Radio value="dashed">虚线</Radio>
                                                            <Radio value="double">双线</Radio>
                                                        </RadioGroup>
                                                        <br/>
                                                        <InputNumber style={{float: 'left', width: '55px', marginRight: '10px'}} value={parseInt(style.subBorderBottomWidth, 10)} onChange={value => this.onChange('subBorderBottomWidth', value, '#menuhContainer .sub', 'border-bottom-width')}/>
                                                        <ColorPicker animation="slide-up" color={style.subBorderBottomColor} onChange={value => this.onChange('subBorderBottomColor', value.color, '#menuhContainer .sub', 'border-bottom-color')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                            <EditGroup title="其它">
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>子菜单图标</Col>
                                                    <Col span={18}>
                                                        <PortalComMaterialLib value={iconSubDown} onChange={value => this.onChange('iconSubDown', value)}/>
                                                    </Col>
                                                </Row>
                                                <Row className="portal-p4e-msl-er">
                                                    <Col span={6}>内容边距</Col>
                                                    <Col span={18}>
                                                        <span>距上</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.subPaddingTop, 10)} onChange={value => this.onChange('subPaddingTop', value + 'px', '#menuhContainer .sub', 'padding-top')}/>
                                                        <span>距下</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.subPaddingBottom, 10)} onChange={value => this.onChange('subPaddingBottom', value + 'px', '#menuhContainer .sub', 'padding-bottom')}/>
                                                        <br/>
                                                        <span>距左</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px', marginRight: '10px'}} value={parseInt(style.subPaddingLeft, 10)} onChange={value => this.onChange('subPaddingLeft', value + 'px', '#menuhContainer .sub', 'padding-left')}/>
                                                        <span>距右</span>
                                                        <InputNumber style={{width: '55px', marginLeft: '5px'}} value={parseInt(style.subPaddingRight, 10)} onChange={value => this.onChange('subPaddingRight', value + 'px', '#menuhContainer .sub', 'padding-right')}/>
                                                    </Col>
                                                </Row>
                                            </EditGroup>
                                        </WeaScroll>
                                    </TabPane>
                                </Tabs>
                            </Col>
                        </Row>

                    </div>
                    <div className="portal-p4e-msl-preview">
                        <div id="menuhContainer" className="menuhContainer">
                            <div id="menuh" className="menuh">
                                <ul>
                                    <li><a href="javascript:;" className="main">Menu1</a></li>
                                    <li>
                                        <a href="javascript:;" className="main">
                                            <span>Menu2</span>
                                            <img src={iconMainDown} alt="" className="down"/>
                                        </a>
                                        <ul>
                                            <li><a href="javascript:;" className="sub">Menu2-1</a></li>
                                            <li>
                                                <a href="javascript:;" className="sub">
                                                    <span>Menu2-2</span>
                                                    <img src={iconSubDown} alt="" className="right"/>
                                                </a>
                                                <ul>
                                                    <li><a href="javascript:;" className="sub">Menu2-2-1</a></li>
                                                    <li><a href="javascript:;" className="sub">Menu2-2-2</a></li>
                                                    <li><a href="javascript:;" className="sub">Menu2-2-3</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="javascript:;" className="main">Menu3</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="portal-p4e-msl-clear"></div>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value, ruleIndex, ruleId) {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.editChange(field, value, ruleIndex, ruleId);
    }

    onSave() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.editSave();
    }

    onCancel() {
        const {portalMenuStyleLibStore} = this.props;
        portalMenuStyleLibStore.editCancel();
    }
}

export default EditH;
