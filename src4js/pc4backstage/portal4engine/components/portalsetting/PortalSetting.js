import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Switch, InputNumber} from 'antd';
import {WeaTop, WeaRightMenu, WeaSelect} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalSettingStore')
@observer
class PortalSetting extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    componentWillMount() {
        const {portalSettingStore} = this.props;
        portalSettingStore.getSettingData();
    }

    render() {
        const {portalSettingStore} = this.props;
        const {loading, settingData} = portalSettingStore;
        const {needRefresh, refreshMins, rsstype, showlasthp} = settingData;

        return (
            <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                <WeaTop
                    title="门户应用设置"
                    icon={<i className='icon-coms-portal'/>}
                    iconBgcolor='#1a57a0'
                    loading={loading}
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenus()}
                    onDropMenuClick={this.onRightMenuClick}
                >
                    <PortalComForm>
                        <PortalComFormItem label="首页自动刷屏">
                            <Switch
                                style={{marginBottom: '3px'}}
                                checked={needRefresh == '1'}
                                onChange={checked => this.onChange('needRefresh', checked ? '1' : '0')}
                            />
                        </PortalComFormItem>
                        {
                            needRefresh == '1' ?
                                <PortalComFormItem label="自动刷屏间隔时间">
                                    <InputNumber value={refreshMins} onChange={value => this.onChange('refreshMins', value)}/>
                                    <span style={{marginLeft: '5px'}}>分钟</span>
                                </PortalComFormItem> : ''
                        }
                        <PortalComFormItem label="RSS读取类型">
                            <WeaSelect
                                options={[
                                    {key: '1', selected: false, showname: '客户端'},
                                    {key: '2', selected: false, showname: '服务器'}
                                ]}
                                value={rsstype}
                                onChange={value => this.onChange('rsstype', value)}
                            />
                        </PortalComFormItem>
                        <PortalComFormItem label="记住上次访问首页">
                            <Switch
                                style={{marginBottom: '3px'}}
                                checked={showlasthp == '1'}
                                onChange={checked => this.onChange('showlasthp', checked ? '1' : '0')}
                            />
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaTop>
            </WeaRightMenu>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onSave}>保存</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        }
    }

    onChange(field, value) {
        const {portalSettingStore} = this.props;
        portalSettingStore.settingChange(field, value);
    }

    onSave() {
        const {portalSettingStore} = this.props;
        portalSettingStore.editSave();
    }
}

export default PortalSetting;
