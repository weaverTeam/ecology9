import { Button } from 'antd';
import { inject, observer } from 'mobx-react';
import { WeaTop, WeaRightMenu, WeaTools, WeaInputSearch, WeaBrowser } from 'ecCom';
import { WeaTable } from 'comsMobx';
import { PortalEditInfo } from '../common/';
import { PortalSetting } from 'weaPortal';
@inject('loginpage_store')
@inject('portaleditinfo_store')
@inject('comsWeaTableStore')
@observer
class LoginPage extends React.Component {
     constructor(props) {
        super(props);
        this.onPortalInfo = this.onPortalInfo.bind(this);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
    }
    componentWillMount(){
       this.props.loginpage_store.getSessionKey();
    }
    render(){
        const { loginpage_store, comsWeaTableStore } = this.props;
        const {  sessionKey } = loginpage_store;
        if(!sessionKey) return <div/>
        return <div className="portal-loginpage-box" id="loginpage">
                <WeaTop title={"登录前页面"}
                        loading={false}
                        icon={<i className='icon-coms-portal' />}
                        iconBgcolor='#1a57a0'
                        buttons={this.getButtons()}
                        buttonSpace={10}
                        showDropIcon={true}
                        dropMenuDatas={this.getRightMenu()}
                        onDropMenuClick={this.onRightMenuClick}>
                       {PortalSetting ? <PortalSetting ref="portalsetting"/> : ''}
                       <PortalEditInfo/>
                       <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                           <WeaTable sessionkey={sessionKey} onOperatesClick={this.onOperatesClick}/>
                        </WeaRightMenu>
                </WeaTop>
        </div>
    }
    onPortalInfo(method, params){
        const { portaleditinfo_store, loginpage_store } = this.props;
        let storeInfo = {
            moduleName:'loginpage', 
            _store: loginpage_store, 
            method
        }
        portaleditinfo_store.onPortalInfo(storeInfo, params)
    }
    getButtons(){
        const { loginpage_store } = this.props;
        const { getSessionKey, deleteRows, onSave } = loginpage_store;
        let btns = [];
        btns.push(<Button type="primary" onClick={this.onPortalInfo.bind(this, 'ref', {})}>新建</Button>);
        btns.push(<Button onClick={onSave}>保存</Button>);
        btns.push(<Button onClick={deleteRows}>批量删除</Button>);
        btns.push(<WeaInputSearch placeholder="请输入名称" onSearch={value => getSessionKey({portalname:value})} />);
        return btns;
    }
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Add-to'/>,
            content:'新建'
        });
        btns.push({
            icon: <i className='icon-coms-Journal'/>,
            content:'保存'
        });
        btns.push({
            icon: <i className='icon-coms-delete'/>,
            content:'批量删除'
        });
        return btns
    }
    onRightMenuClick(key){
        const { loginpage_store } = this.props;
        switch(key){
            case '0': //新建
                this.onPortalInfo('ref',{});
                break;
            case '1': //保存
                loginpage_store.onSave();
                break;
            case '2': //批量删除
                loginpage_store.deleteRows();
                break;

        }
    }
    onOperatesClick(record, index, operate, flag){
        const { loginpage_store } = this.props;
        const id = record.id;
        switch(operate.index){
            case '0':
                try{
                    this.refs.portalsetting.handleOnSetting({
                        hpid: id,
                        subCompanyId: -1,
                        pagetype: 'loginview',
                        isSetting: true,
                        from: 'setElement',
                        opt: 'edit',
                    });
                } catch(e){
                    window.console ? console.log('门户设置组件PortalSetting不存在： ',e) : alert('门户设置组件PortalSetting不存在： '+e);
                }
                break;
            case '1':
                try{
                    this.refs.portalsetting.handleOnSetting({
                        hpid: id,
                        subCompanyId: -1,
                        opt: 'priview',
                    });
                } catch(e){
                    window.console ? console.log('门户设置组件PortalSetting不存在： ',e) : alert('门户设置组件PortalSetting不存在： '+e);
                }
                break;
            case '2':
                this.onPortalInfo('savebase', { hpid: id });
                break;
            case '3':
                this.onPortalInfo('saveNew', { hpid: id });
                break;
            case '4':
                loginpage_store.doDel(id);
                break;
        }
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
LoginPage = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(LoginPage);

export default LoginPage;