import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon, Modal} from 'antd';
const confirm = Modal.confirm;
import {WeaTop, WeaRightMenu, WeaTab} from 'ecCom';
import {WeaTable} from 'comsMobx';

import E7Edit from './E7Edit';
import CustomPreview from './CustomPreview';
import CustomEdit from './CustomEdit';

@inject('portalThemeLibStore')
@observer
class PortalThemeLib extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
        this.onOperatesClick = this.onOperatesClick.bind(this);
        this.onImportE7Theme = this.onImportE7Theme.bind(this);
        this.onImportCustomTheme = this.onImportCustomTheme.bind(this);
    }

    componentWillMount() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.getDataList();
    }

    render() {
        const {portalThemeLibStore} = this.props;
        const {loading, theme, themeName, sessionkey} = portalThemeLibStore;

        return (
            <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                <WeaTop
                    title="门户主题库"
                    icon={<i className='icon-coms-portal'/>}
                    iconBgcolor='#1a57a0'
                    loading={loading}
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenus()}
                    onDropMenuClick={this.onRightMenuClick}
                >
                    <WeaTab
                        datas={[{key: 'ecology8', title: 'ECOLOGY8主题'}, {key: 'ecology7', title: 'ECOLOGY7主题'}, {key: 'custom', title: '站点主题'}]}
                        keyParam='key'
                        selectedKey={theme}
                        onChange={this.onTabChange}
                        searchType={['base']}
                        searchsBaseValue={themeName}
                        onSearch={this.onSearch}
                    />
                    {theme == 'custom' ? <WeaTable sessionkey={sessionkey} onOperatesClick={this.onOperatesClick}/> : ''}
                    <E7Edit />
                    <CustomPreview />
                    <CustomEdit />
                </WeaTop>
            </WeaRightMenu>
        );
    }

    getButtons() {
        const {portalThemeLibStore} = this.props;
        const {theme} = portalThemeLibStore;

        let buttons = [];
        if (theme == 'ecology7') {
            buttons.push(<Button type="primary" disabled={false} onClick={this.onImportE7Theme}>导入主题</Button>);
        } else if (theme == 'custom') {
            buttons.push(<Button type="primary" disabled={false} onClick={this.onImportCustomTheme}>导入主题</Button>);
        }

        return buttons;
    }

    getRightMenus() {
        const {portalThemeLibStore} = this.props;
        const {theme} = portalThemeLibStore;

        let rightMenus = [];
        if (theme == 'ecology7' || theme == 'custom') {
            rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '导入主题'});
        }
        return rightMenus;
    }

    onRightMenuClick(key) {
        const {portalThemeLibStore} = this.props;
        const {theme} = portalThemeLibStore;

        if (key == '0') {
            if (theme == 'ecology7') {
                this.onImportE7Theme();
            } else if (theme == 'custom') {
                this.onImportCustomTheme();
            }
        }
    }

    onTabChange(key) {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.changeTheme(key);
    }

    onSearch(value) {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.searchPortalTheme(value);
    }

    onImportE7Theme() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.importE7Theme();
    }

    onOperatesClick(record, index, operate, flag) {
        const {portalThemeLibStore} = this.props;
        if (operate.index == '0') { // 预览
            portalThemeLibStore.previewCustomTheme(record);
        } else if (operate.index == '1') { // 下载
            portalThemeLibStore.downloadCustomTheme(record);
        } else if (operate.index == '2') { // 编辑
            portalThemeLibStore.editCustomTheme(record);
        } else if (operate.index == '3') { // 删除
            confirm({
                title: '信息确认',
                content: '确定要删除吗？',
                onOk: () => {
                    portalThemeLibStore.deleteCustomTheme(record);
                }
            });
        }
    }

    onImportCustomTheme() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.importCustomTheme();
    }
}

export default PortalThemeLib;
