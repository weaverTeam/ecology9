import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaDialog, WeaRightMenu, WeaInput, WeaUpload} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalThemeLibStore')
@observer
class CustomEdit extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onFileChange = this.onFileChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalThemeLibStore} = this.props;
        const {customEditDialogVisible, customEditData} = portalThemeLibStore;
        const {templatename, templatedesc, zipName, filename} = customEditData;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={customEditDialogVisible}
                title="编辑模板"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 530, height: 240}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="名称">
                            <WeaInput value={templatename} onChange={value => this.onChange('templatename', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="描述">
                            <WeaInput value={templatedesc} onChange={value => this.onChange('templatedesc', value)}/>
                        </PortalComFormItem>
                        <PortalComFormItem label="文件(.zip)">
                            <div style={{display: 'inline-block', marginRight: '10px'}}>
                                <WeaUpload
                                    value={zipName}
                                    uploadId="zipName"
                                    uploadUrl="/api/portal/portalthemelib/customupload"
                                    multiSelection={false}
                                    category="0"
                                    limitType="zip"
                                    onChange={this.onFileChange}
                                    showClearAll={true}
                                >
                                    <Button>选择文件</Button>
                                </WeaUpload>
                            </div>
                            <span>{filename}</span>
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.customEditChange(field, value);
    }

    onFileChange(ids, list) {
        if (list && list.length) {
            const item = list[list.length - 1];
            this.onChange('zipName', item.filelink);
            this.onChange('filename', item.filename);
        }
    }

    onSave() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.customEditSave();
    }

    onCancel() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.customEditCancel();
    }
}

export default CustomEdit;
