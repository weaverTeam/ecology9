import React from 'react';
import {inject, observer} from 'mobx-react';
import {WeaDialog} from 'ecCom';

@inject('portalThemeLibStore')
@observer
class CustomPreview extends React.Component {
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalThemeLibStore} = this.props;
        const {customPreviewDialogVisible, customPreviewData} = portalThemeLibStore;
        const {dir} = customPreviewData;
        const previewUrl = `/page/template/${dir}/index.htm`;

        return (
            <WeaDialog
                className="portal-com-dialog"
                url={previewUrl}
                visible={customPreviewDialogVisible}
                title={'预览'}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 800, height: 500}}
                onCancel={this.onCancel}
            >
            </WeaDialog>
        );
    }

    onCancel() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.customPreviewCancel();
    }
}

export default CustomPreview;
