import React from 'react';
import {inject, observer} from 'mobx-react';
import {Button, Icon} from 'antd';
import {WeaDialog, WeaRightMenu, WeaUpload} from 'ecCom';

import PortalComForm from '../common/PortalComForm';
import PortalComFormItem from '../common/PortalComFormItem';

@inject('portalThemeLibStore')
@observer
class E7Edit extends React.Component {
    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    render() {
        const {portalThemeLibStore} = this.props;
        const {e7EditDialogVisible} = portalThemeLibStore;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={e7EditDialogVisible}
                title="导入主题"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 530, height: 240}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="文件(.zip)">
                            <WeaUpload
                                uploadId="e7theme"
                                uploadUrl="/api/portal/portalthemelib/e7upload"
                                category="/page/e7upload/"
                                autoUpload={false}
                                multiSelection={false}
                                limitType="zip"
                                showClearAll={false}
                            />
                        </PortalComFormItem>
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onSave() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.e7EditSave();
    }

    onCancel() {
        const {portalThemeLibStore} = this.props;
        portalThemeLibStore.e7EditCancel();
    }
}

export default E7Edit;
