import React from 'react';
import {Button, Icon, Row, Col, InputNumber} from 'antd';
import {WeaDialog, WeaRightMenu, WeaSelect, WeaBrowser} from 'ecCom';

import PortalComForm from './PortalComForm';
import PortalComFormItem from './PortalComFormItem';

class PortalRightShareEdit extends React.Component {
    state = {
        data: {
            sharetype: '1',
            sharevalue: '',
            seclevelmin: 0,
            seclevelmax: 100,
            seclevel: '0-100',
            rolelevel: '0',
            jobtitlelevel: '1',
            jobtitlesharevalue: ''
        }
    };

    constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    getButtons() {
        let buttons = [];
        buttons.push(<Button type="primary" disabled={false} onClick={this.onSave}>保存</Button>);
        buttons.push(<Button type="ghost" disabled={false} onClick={this.onCancel}>取消</Button>);
        return buttons;
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="save"/>, content: '保存'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onSave();
        } else if (key == '1') {
            this.onCancel();
        }
    }

    onChange(field, value) {
        const {data} = this.state;
        data[field] = value;

        if (field === 'seclevelmin' || field === 'seclevelmax') {
            data['seclevel'] = data.seclevelmin + '-' + data.seclevelmax;
        }

        this.setState({
            data: data
        });
    }

    onSave() {
        this.props.onSave(this.state.data);
    }

    onCancel() {
        this.props.onCancel();
    }

    render() {
        const {
            sharetype,
            sharevalue,
            seclevelmin,
            seclevelmax,
            rolelevel,
            jobtitlelevel,
            jobtitlesharevalue
        } = this.state.data;

        let objItemVisible = true;
        let seclevelItemVisible = true;
        let rolelevelItemVisible = false;
        let jobtitlelevelItemVisible = false;

        let objItem = '';
        let rolelevelItem = '';
        let jobtitlelevelItem = '';

        if (sharetype === '1') { // 部门
            objItem = (
                <WeaBrowser
                    type={57}
                    isSingle={false}
                    title="部门"
                    tabs={[
                        {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                        {key: '2', name: '按组织架构', selected: false}
                    ]}
                    onChange={value => this.onChange('sharevalue', value)}
                />
            );
        } else if (sharetype === '6') { // 分部
            objItem = (
                <WeaBrowser
                    type={194}
                    isSingle={false}
                    title="分部"
                    tabs={[
                        {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                        {key: '2', name: '按组织架构', selected: false}
                    ]}
                    onChange={value => this.onChange('sharevalue', value)}
                />
            );
        } else if (sharetype === '3') { // 安全级别
            objItemVisible = false;
        } else if (sharetype === '5') { // 人力资源
            seclevelItemVisible = false;

            objItem = (
                <WeaBrowser
                    type={17}
                    isSingle={false}
                    title="人力资源"
                    onChange={value => this.onChange('sharevalue', value)}
                />
            );
        } else if (sharetype === '2') { // 角色
            rolelevelItemVisible = true;

            objItem = (
                <WeaBrowser
                    type={267}
                    isSingle={true}
                    title="角色"
                    onChange={value => this.onChange('sharevalue', value)}
                />
            );

            rolelevelItem = (
                <WeaSelect
                    options={[
                        {key: '0', selected: false, showname: '部门'},
                        {key: '1', selected: false, showname: '分部'},
                        {key: '2', selected: false, showname: '总部'}
                    ]}
                    value={rolelevel}
                    onChange={value => this.onChange('rolelevel', value)}
                />
            );
        } else if (sharetype === '7') { // 岗位
            seclevelItemVisible = false;
            jobtitlelevelItemVisible = true;

            objItem = (
                <WeaBrowser
                    type={278}
                    isSingle={false}
                    title="岗位"
                    onChange={value => this.onChange('sharevalue', value)}
                />
            );

            let jobtitlelevelObj = '';
            if (jobtitlelevel == '2') {
                jobtitlelevelObj = (
                    <WeaBrowser
                        type={57}
                        isSingle={false}
                        title="部门"
                        tabs={[
                            {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                            {key: '2', name: '按组织架构', selected: false}
                        ]}
                        onChange={value => this.onChange('jobtitlesharevalue', value)}
                    />
                );
            } else if (jobtitlelevel == '3') {
                jobtitlelevelObj = (
                    <WeaBrowser
                        type={194}
                        isSingle={false}
                        title="分部"
                        tabs={[
                            {key: '1', name: '按列表', selected: false, dataParams: {list: 1}},
                            {key: '2', name: '按组织架构', selected: false}
                        ]}
                        onChange={value => this.onChange('jobtitlesharevalue', value)}
                    />
                );
            }

            jobtitlelevelItem = (
                <Row gutter={16}>
                    <Col span={8}>
                        <WeaSelect
                            options={[
                                {key: '1', selected: false, showname: '总部'},
                                {key: '2', selected: false, showname: '指定部门'},
                                {key: '3', selected: false, showname: '指定分部'}
                            ]}
                            value={jobtitlelevel}
                            onChange={value => this.onChange('jobtitlelevel', value)}
                        />
                    </Col>
                    <Col span={16}>
                        {jobtitlelevelObj}
                    </Col>
                </Row>
            );
        }

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={this.props.visible}
                title="添加共享"
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 480, height: 228}}
                buttons={this.getButtons()}
                onCancel={this.onCancel}
            >
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <PortalComForm>
                        <PortalComFormItem label="对象类型">
                            <WeaSelect
                                options={[
                                    {key: '1', selected: false, showname: '部门'},
                                    {key: '6', selected: false, showname: '分部'},
                                    {key: '3', selected: false, showname: '安全级别'},
                                    {key: '5', selected: false, showname: '人力资源'},
                                    {key: '2', selected: false, showname: '角色'},
                                    {key: '7', selected: false, showname: '岗位'}
                                ]}
                                value={sharetype}
                                onChange={value => this.onChange('sharetype', value)}
                            />
                        </PortalComFormItem>
                        {
                            objItemVisible ?
                                <PortalComFormItem label="对象">
                                    {objItem}
                                </PortalComFormItem> : ''
                        }
                        {
                            rolelevelItemVisible ?
                                <PortalComFormItem label="角色级别">
                                    {rolelevelItem}
                                </PortalComFormItem> : ''
                        }
                        {
                            jobtitlelevelItemVisible ?
                                <PortalComFormItem label="岗位级别">
                                    {jobtitlelevelItem}
                                </PortalComFormItem> : ''
                        }
                        {
                            seclevelItemVisible ?
                                <PortalComFormItem label="安全级别">
                                    <InputNumber min={0} max={100} defaultValue={seclevelmin} onChange={value => this.onChange('seclevelmin', value)}/>
                                    <span style={{margin: '0 5px'}}>-</span>
                                    <InputNumber min={0} max={100} defaultValue={seclevelmax} onChange={value => this.onChange('seclevelmax', value)}/>
                                </PortalComFormItem> : ''
                        }
                    </PortalComForm>
                </WeaRightMenu>
            </WeaDialog>
        );
    }
}

export default PortalRightShareEdit;
