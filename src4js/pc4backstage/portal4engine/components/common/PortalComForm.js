import React from 'react';

// 门户表单组件
class PortalComForm extends React.Component {
    render() {
        return (
            <div className={`portal-com-form ${this.props.className || ''}`}>
                {this.props.children || ''}
            </div>
        );
    }
}

export default PortalComForm;
