import React from 'react';
import {Row, Col} from 'antd';

// 门户表单项组件
class PortalComFormItem extends React.Component {
    render() {
        const {className = '', labelCol = 6, wrapperCol = 16, label = '', children = ''} = this.props;

        // style={{display: 'table'}}
        // style={{float: 'none', display: 'table-cell', verticalAlign: 'middle'}}
        // style={{float: 'none', display: 'table-cell', verticalAlign: 'middle'}}

        return (
            <Row gutter={16} className={`portal-com-form-item ${className}`}>
                <Col span={labelCol}>
                    <div className="portal-com-form-item-label">
                        {label}
                    </div>
                </Col>
                <Col span={wrapperCol}>
                    <div className="portal-com-form-item-control">
                        {children}
                    </div>
                </Col>
            </Row>
        );
    }
}

export default PortalComFormItem;
