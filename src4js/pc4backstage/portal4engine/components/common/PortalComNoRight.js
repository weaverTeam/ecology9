import React from 'react';

const PortalComNoRight = () => {
    return (
        <div style={{position: 'relative', width: '100%', height: '100%', backgroundColor: '#fff'}}>
            <div style={{position: 'absolute', top: '50%', left: '50%', width: '210px', height: '100px', marginTop: '-50px', marginLeft: '-105px', textAlign: 'center'}}>
                <i className="icon-coms-locking" style={{color: '#cbcbcb', fontSize: '64px'}}/>
                <div style={{height: '20px', marginTop: '15px', color: '#9a9a9a', fontSize: '12px'}}>对不起，您暂时没有权限</div>
            </div>
        </div>
    );
};

export default PortalComNoRight;
