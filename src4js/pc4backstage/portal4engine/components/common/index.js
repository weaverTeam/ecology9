//登录前页面及登录后页面新建、编辑、另存为弹窗组件
import PortalEditInfo from './PortalEditInfo';
//排序组件
import WeaPortalTree from './WeaPortalTree';
module.exports = {
	PortalEditInfo,
	WeaPortalTree,
}