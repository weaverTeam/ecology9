import React from "react";
import {WeaInputSearch} from 'ecCom';

// 素材库组件
class PortalComMaterialLib extends React.Component {
    state = {
        value: this.props.value
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: nextProps.value
        });
    }

    onShow() {
        let dialog = new window.top.Dialog();
        dialog.Title = '素材库';
        dialog.Width = top.document.body.clientWidth - 100;
        dialog.Height = top.document.body.clientHeight - 100;
        dialog.URL = '/page/maint/common/CustomResourceMaint.jsp?isDialog=1&?file=none&isSingle=';
        dialog.opacity = 0.4;
        dialog.callbackfunParam = {};
        dialog.callbackfun = (callbackfunParam, datas) => {
            this.onChange(datas.id);
        };
        dialog.callbackfunc4CloseBtn = () => {
        };
        dialog.show();
    }

    onChange(value) {
        this.setState({
            value: value
        });

        this.props.onChange(value);
    }

    render() {
        return <WeaInputSearch style={{width: '100%'}} value={this.state.value} onSearch={this.onShow.bind(this)}/>
    }
}

export default PortalComMaterialLib;
