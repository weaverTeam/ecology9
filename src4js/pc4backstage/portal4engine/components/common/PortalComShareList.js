import React from 'react';
import {Button, Icon, Row, Col} from 'antd';
import {WeaDialog, WeaRightMenu} from 'ecCom';
import {WeaTable, store} from 'comsMobx';
const {comsWeaTableStore} = store;

import PortalComShareEdit from './PortalComShareEdit';

class PortalComShareList extends React.Component {
    state = {
        editVisible: false
    };

    constructor(props) {
        super(props);
        this.getRightMenus = this.getRightMenus.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onEditSave = this.onEditSave.bind(this);
        this.onEditCancel = this.onEditCancel.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        // 刷新列表
        if (this.props.sessionkey != nextProps.sessionkey) {
            comsWeaTableStore.getDatas(nextProps.sessionkey, 1);
        }
    }

    getRightMenus() {
        let rightMenus = [];
        rightMenus.push({disabled: false, icon: <Icon type="plus"/>, content: '添加'});
        rightMenus.push({disabled: false, icon: <Icon type="delete"/>, content: '批量删除'});
        rightMenus.push({disabled: false, icon: <Icon type="cross"/>, content: '取消'});
        return rightMenus;
    }

    onRightMenuClick(key) {
        if (key == '0') {
            this.onAdd();
        } else if (key == '1') {
            this.onDelete();
        } else if (key == '2') {
            this.onCancel();
        }
    }

    onAdd() {
        this.setState({
            editVisible: true
        });
    }

    onDelete() {
        const {shareObj, sessionkey} = this.props;
        const state = comsWeaTableStore.state[sessionkey.split('_')[0]];
        const {selectedRowKeys} = state;

        this.props.onDelete(shareObj, selectedRowKeys.join(','));
    }

    onCancel() {
        this.props.onCancel();
    }

    onEditSave(data) {
        const {shareObj} = this.props;
        this.props.onSave(shareObj, data, () => {
            this.onEditCancel();
        });
    }

    onEditCancel() {
        this.setState({
            editVisible: false
        });
    }

    render() {
        const {visible, title, sessionkey} = this.props;

        return (
            <WeaDialog
                className="portal-com-dialog"
                visible={visible}
                title={title}
                icon="icon-coms-portal"
                iconBgcolor='#1a57a0'
                style={{width: 780, height: 578}}
                buttons={[<Button type="ghost" onClick={this.onCancel}>取消</Button>]}
                onCancel={this.onCancel}
            >
                <PortalComShareEdit visible={this.state.editVisible} onSave={this.onEditSave} onCancel={this.onEditCancel}/>
                <WeaRightMenu datas={this.getRightMenus()} onClick={this.onRightMenuClick}>
                    <Row style={{height: '40px', lineHeight: '35px', borderBottom: '1px solid #e9e9e9'}}>
                        <Col style={{paddingRight: '20px', textAlign: 'right'}}>
                            <Button type="primary" onClick={this.onAdd} style={{borderRadius: '2px'}}>添加</Button>
                            <Button type="ghost" onClick={this.onDelete} style={{borderRadius: '2px', marginLeft: '10px'}}>批量删除</Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <WeaTable sessionkey={sessionkey} needScroll={true}/>
                        </Col>
                    </Row>
                </WeaRightMenu>
            </WeaDialog>
        );
    }
}

export default PortalComShareList;
