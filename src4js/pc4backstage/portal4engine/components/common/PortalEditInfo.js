import React from 'react';
import { Button, Collapse, Switch, Popover, InputNumber } from 'antd';
const Panel = Collapse.Panel
import {inject, observer} from 'mobx-react';
import {WeaTools, WeaErrorPage, WeaDialog, WeaRightMenu, WeaInput, WeaSelect, WeaBrowser, WeaNewScroll  } from 'ecCom';
import ColorPicker from 'rc-color-picker';
import PortalComForm from './PortalComForm';
import PortalComFormItem from './PortalComFormItem';
import './css/style.css';
@inject('portaleditinfo_store')
@observer
class PortalEditInfo extends React.Component{
	constructor(props) {
        super(props);
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.handleOnChangeLayout = this.handleOnChangeLayout.bind(this);
    }
	getButtons(){
		const { portaleditinfo_store } = this.props;
		let btns = [];
        btns.push(<Button type="primary" onClick={portaleditinfo_store.handleSubmit}>保存</Button>);
        btns.push(<Button onClick={portaleditinfo_store.handleCancel}>取消</Button>);
        return btns;
	}
    getRightMenu(){
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-Journal'/>,
            content:'保存'
        });
       	btns.push({
            icon: <i className='icon-coms-go-back'/>,
            content:'取消'
        });
        return btns
    }
    onRightMenuClick(key){
    	const { portaleditinfo_store } = this.props;
        switch(key){
            case '0': //保存
                portaleditinfo_store.handleSubmit();
                break;
            case '1': //取消
            	portaleditinfo_store.handleCancel();
                break;
        }
    }
    handleOnChangeLayout(value){
		const { onChangeWeaSelect, showTableLayout } = this.props.portaleditinfo_store;
    	onChangeWeaSelect('layoutid',value);
    	showTableLayout(value);
    }
	render(){
		const { portaleditinfo_store } = this.props;
		const { moduleName, dialog, data, handleCancel, setFormItemValue, _store } = portaleditinfo_store;
        let html = <div/>;
        if(data){
        	const { hpid, infoname, infodesc, subCompanyId, isuse, isRedirectUrl, redirectUrl, islocked, menustyleid, menuStyleInfo, layoutid, layoutInfo, layoutPreview, txtArea_A, txtArea_B, txtArea_C, txtArea_D, styleid, elementStyleInfo, elementStylePreview, bgcolor } = data;
        	html = <div className="mainportal-add-content" >
	             		<PortalComForm>
		                  	<Collapse defaultActiveKey={['1','2','3']}>
						    <Panel header="基本信息" key="1">
							  	<PortalComFormItem label="名称">
			                      	<WeaInput fieldName="infoname" value={infoname} onChange={setFormItemValue.bind(this,'infoname')} />
			                    </PortalComFormItem>
			                    <PortalComFormItem label="描述">
			                    	<WeaInput fieldName="infodesc" value={infodesc} onChange={setFormItemValue.bind(this,'infodesc')} />
			                    </PortalComFormItem>
			                    {moduleName === 'mainpage' ?  <PortalComFormItem label="所属机构">
			                        <WeaBrowser fieldName="subCompanyId" onChange={value => setFormItemValue('subCompanyId', value)} replaceDatas={[_store.subCompany]} type={194} title="分部" tabs={[{key: '1', name: '按列表', WeaSelected: false, dataParams: {list: 1}}, {key: '2', name: '按组织架构', WeaSelected: false}]} isSingle={true} linkUrl="/hrm/company/HrmSubCompanyDsp.jsp?id="/>
			                    </PortalComFormItem> : '' }
			                    <PortalComFormItem label="启用">
			                    	<Switch onChange={setFormItemValue.bind(this,'isuse')} defaultChecked={isuse}/>
			                    </PortalComFormItem>
			                    <PortalComFormItem label="直接显示指定页面内容">
			                    	<Switch onChange={setFormItemValue.bind(this,'isRedirectUrl')} defaultChecked={isRedirectUrl}/>
			                    </PortalComFormItem>
			                    {isRedirectUrl ? <PortalComFormItem label="页面URL">
			                    	<WeaInput fieldName="redirectUrl" value={redirectUrl} onChange={setFormItemValue.bind(this,'redirectUrl')} />
			                    </PortalComFormItem> : ''}
			                   {!isRedirectUrl && moduleName === 'mainpage' ?  <PortalComFormItem label="锁定">
			                      	<Switch onChange={setFormItemValue.bind(this,'islocked')} defaultChecked={islocked}/>
			                    </PortalComFormItem> :''}
			                    {!isRedirectUrl ?  <PortalComFormItem label="菜单样式">
		                            <WeaSelect widthMatchOptions={true} options={menuStyleInfo} fieldName="menustyleid" value={menustyleid} onChange={setFormItemValue.bind(this,'menustyleid')} />
			                    </PortalComFormItem> :''}
						    </Panel>
						    {!isRedirectUrl ? <Panel header="首页样式" key="2">
						    	<PortalComFormItem label="布局">
			                        <WeaSelect widthMatchOptions={true} options={layoutInfo} fieldName="layoutid" value={layoutid} onChange={setFormItemValue.bind(this,'layoutid')}/>
			                    </PortalComFormItem>
			                    <PortalComFormItem label="布局预览" className="portal-p4e-mainportal-layoutpreview">
			                        <div className="mainportal-layout-preview">
	         							<div id="layoutPreviewContainer" style={{float:'left'}} dangerouslySetInnerHTML={{__html:layoutPreview[layoutid].layouttable}}></div>
		         						<div style={{float:'left',marginLeft:'30px', display: layoutPreview[layoutid].layouttype === 'sys' ? '' : 'none'}}>
		         							<div><span>A区域宽度:</span> <InputNumber defaultValue={txtArea_A} onChange={setFormItemValue.bind(this,'txtArea_A')} min={0} max={100} />  %</div>
		         							{parseInt(layoutid) > 1 ? <div><span>B区域宽度:</span> <InputNumber defaultValue={txtArea_B} onChange={setFormItemValue.bind(this,'txtArea_B')} min={0} max={100} />  %</div> : <div/>}
		         							{parseInt(layoutid) > 2 ? <div><span>C区域宽度:</span> <InputNumber defaultValue={txtArea_C} onChange={setFormItemValue.bind(this,'txtArea_C')} min={0} max={100} />  %</div> : <div/>}
		         							{parseInt(layoutid) > 3 ? <div><span>D区域宽度:</span> <InputNumber defaultValue={txtArea_D} onChange={setFormItemValue.bind(this,'txtArea_D')} min={0} max={100} />  %</div> : <div/>}
		         						</div>
		         					</div>
			                    </PortalComFormItem>
			                    <PortalComFormItem label="元素样式">
		                            <WeaSelect widthMatchOptions={true} options={elementStyleInfo} fieldName="styleid" value={styleid} onChange={setFormItemValue.bind(this,'styleid')} />
			                    </PortalComFormItem>
			                   	<PortalComFormItem label="样式预览" className="portal-p4e-mainportal-stylepreview">
			                        <div dangerouslySetInnerHTML={{__html:elementStylePreview}}></div>
			                    </PortalComFormItem>
			                    {moduleName === 'mainpage' ? <PortalComFormItem label="背景颜色">
			                    	<WeaInput fieldName="bgcolor" value={bgcolor} onBlur={setFormItemValue.bind(this,'bgcolor')} style={{width: '100px', background: bgcolor}}/>
                                	<ColorPicker color={bgcolor} 
                                		onChange={e => setFormItemValue('bgcolor',e.color)}
                                	 	children={
                                			<img class="color" src="/js/jquery/plugins/farbtastic/color_wev8.png" className="portal-p4e-mainportal-bgcolor"/>
                                	}/>
			                    </PortalComFormItem> : '' }
						    </Panel> : ''}
						  </Collapse>
	                  </PortalComForm>
	             	</div>;
      	}
		return <WeaDialog title={dialog.title}
					visible={dialog.visible} 
                  	closable={true}
                 	onCancel={handleCancel}
                  	icon='icon-coms-portal'
                  	iconBgcolor='#1a57a0'
                  	maskClosable={false}
                  	buttons={this.getButtons()}
                  	style={{width:top.document.body.clientWidth*0.5,height:top.document.body.clientHeight*0.8}}>
                  	<WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
	                  	<WeaNewScroll scrollId="mainpageAdd" height={top.document.body.clientHeight*0.8}>
		             		{html}
		             	</WeaNewScroll>
	             	</WeaRightMenu>
        		</WeaDialog>
	}
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
PortalEditInfo = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(PortalEditInfo);
export default PortalEditInfo;