import WeaTable from './components/Table'
import store from './stores/index'

module.exports = {
	store,
	WeaTable
}