import { inject, observer } from 'mobx-react';
import { WeaTable } from 'ecCom';

@inject('comsWeaTableStore')

export default @observer class WeaTableMobx extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const { comsWeaTableStore, sessionkey, hasOrder, needScroll, onOperatesClick = '' } = this.props;
		const tablekey = sessionkey ? sessionkey.split('_')[0] : 'init';
		const tableNow = comsWeaTableStore.state[tablekey] || comsWeaTableStore.state['init'];
		const {
			//table
			datakey,
			datas,
			columns,
			operates,
			sortParams,
			selectedRowKeys,
			loading,
			showCheck,
			pageAutoWrap,
			//自定义列
			colSetVisible,
			colSetdatas,
			colSetKeys,
			//pagination
			count,
			current,
			pageSize,
		} = tableNow;
		return (
			<WeaTable
	            hasOrder={hasOrder}
	            needScroll={needScroll}
	            loading={loading}
	            tableCheck={showCheck}
	            pageAutoWrap={pageAutoWrap}
	            
	            count={count}
	        	current={current}
	            pageSize={pageSize}
	            
	            datas={datas.slice()}
	            columns={this.getColumns()}
	            rowSel={this.getRowSel()}
	            operates={operates.slice()}
	            sortParams={sortParams.slice()}
	            onChange={(p,f,s) => comsWeaTableStore.getDatas(sessionkey,p.current,p.pageSize,s)}
	            
	            colSetVisible={colSetVisible}
	            colSetdatas={colSetdatas.slice()}
	            colSetKeys={colSetKeys.slice()}
	            showColumnsSet={bool => comsWeaTableStore.setColSetVisible(sessionkey,bool)}
	            onTransferChange={keys => comsWeaTableStore.setTableColSetkeys(sessionkey,keys)}
	            saveColumnsSet={() => comsWeaTableStore.tableColSet(sessionkey)}
	            
	            onOperatesClick={onOperatesClick}
	            />
		)
	}
	getRowSel() {
		const { comsWeaTableStore, sessionkey } = this.props;
		const tablekey = sessionkey ? sessionkey.split('_')[0] : 'init';
		const tableNow = comsWeaTableStore.state[tablekey] || comsWeaTableStore.state['init'];
		const { selectedRowKeys } = tableNow;
		return {
			selectedRowKeys: selectedRowKeys.slice(),
			onChange(sRowKeys, selectedRows) {
				comsWeaTableStore.setSelectedRowKeys(sessionkey,sRowKeys);
			},
			onSelect(record, selected, selectedRows) {},
			onSelectAll(selected, selectedRows, changeRows) {}
		};
	}
	getColumns() {
		const { comsWeaTableStore, sessionkey, getColumns } = this.props;
		const tablekey = sessionkey ? sessionkey.split('_')[0] : 'init';
		const tableNow = comsWeaTableStore.state[tablekey] || comsWeaTableStore.state['init'];
		const { columns } = tableNow;
		let newColumns = columns.slice();
		if(typeof getColumns === 'function'){
			newColumns = getColumns(newColumns);
		}else{
			newColumns = newColumns.map(column => {
				let newColumn = column;
				newColumn.render = (text, record, index) => { //前端元素转义
					let valueSpan = record[newColumn.dataIndex + "span"];
					return(
						<div className="wea-url" dangerouslySetInnerHTML={{__html: valueSpan}} />
					)
				}
				return newColumn;
			});
		}
		return newColumns
	}
}