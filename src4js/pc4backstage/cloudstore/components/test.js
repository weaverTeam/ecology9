import React from 'react'
import { inject, observer } from 'mobx-react';

import {Button} from 'antd'

@inject('cloudstore_store1')
@inject('cloudstore_store2')
@inject('routing')
@observer
class App extends React.Component {
    render() {
        console.log("云商店！");
        const {routing,cloudstore_store1} = this.props;
        return (
            <div>
            <Button type="ghost" onClick={()=>{
                cloudstore_store1.addUser();
            }}>添加</Button>
            {
                cloudstore_store1.getUserList().map((obj)=>{
                    return <div>{obj.name}</div>
                })
            }
            </div>
        )
    }
}

export default App