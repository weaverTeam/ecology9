import {observable,action} from 'mobx';

//import {message} from 'antd'

export default class TaskStore {
    history = null;
    @observable taskList = [];
    constructor(history) {
        this.history = history;
        this.addTask = this.addTask.bind(this);
        this.getTaskList = this.getTaskList.bind(this);
    }
    @action
    addTask(params) {
        this.taskList.push({name:"task1"});
    }
    getTaskList(params) {
        return this.taskList.slice();
    }
}