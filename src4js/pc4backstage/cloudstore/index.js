import Route from 'react-router/lib/Route'

import Test from './components/test'
import Store1 from './stores/Store1'
import Store2 from './stores/Store2'

const CloudstoreRoute = (
	<Route path="cloudstore" breadcrumbName="云商店" component={Test}>
        
    </Route>
)

const cloudstore_store1 = new Store1();
const cloudstore_store2 = new Store2();

//console.log("cloudstore_store1:",cloudstore_store1);

module.exports = {
    Route:CloudstoreRoute,
    store:{
        cloudstore_store1,
        cloudstore_store2
    }
}