import { createStore } from 'redux'

export default function configureStore(rootReducer,initialState) {
  const store = createStore(rootReducer, initialState)

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../index.js', () => {
      const nextReducer = rootReducer
      store.replaceReducer(nextReducer)
    })
  }

  return store
}