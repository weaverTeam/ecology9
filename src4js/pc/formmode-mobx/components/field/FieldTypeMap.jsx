const Map = {
    /**
     * 人力资源
     */
    Resource: 1,
    /**
     * 日期
     */
    Date: 2,
    /**
     * 资产（16）
     */
    Capital_16: 3,
    /**
     * 部门
     */
    Department: 4,
    /**
     * 仓库
     */
    Storage: 5,
    /**
     * 客户
     */
    CRM: 7,
    /**
     * 项目
     */
    Project: 8,
    /**
     * 文档
     */
    Document: 9,
    /**
     * 入库方式
     */
    Storage_In_Mode: 10,
    /**
     * 出口方式
     */
    Storage_Out_Mode: 11,
    /**
     * 币种
     */
    Currency:12,
    /**
     * 产品类型
     */
    Product_Type:13,
    /**
     * 科目-所有
     */
    Subject_All:14,
    /**
     * 科目-明细
     */
    Subject_Item:15,
    /**
     * 流程
     */
    Request:16,
    /**
     * 多人力资源
     */
    Mutil_Resource:17,
    /**
     * 多客户
     */
    Mutil_CRM:18,
    /**
     * 时间
     */
    Time:19,
    /**
     * 计划类型
     */
    Plan_Category:20,
    /**
     * 计划种类
     */
    Plan_Type:21,
    /**
     * 报销费用类型
     */
    Cost_Category:22,
    /**
     * 资产
     */
    Capital:23,
    /**
     * 岗位
     */
    Job:24,
    /**
     * 资产组
     */
    Capital_Group:25,
    /**
     * 资产(3)
     */
    Capital_3:26,
    /**
     * 应聘人
     */
    Applicants:27,
    /**
     * 会议
     */
    Meetting:28,
    /**
     * 奖惩类型
     */
    Award_Ctegory:29,
    /**
     * 学历
     */
    Education:30,
    /**
     * 用工性质
     */
    Employment_Nature:31,
    /**
     * 培训安排
     */
    Training_Schedule:32,
    /**
     * 加班类型
     */
    Overtime_Type:33,
    /**
     * 请假类型
     */
    Leave_Type:34,
    /**
     * 业务合同
     */
    Business_Contract:35,
    /**
     * 合同性质
     */
    Contract_Nature:36,
    /**
     * 多文档
     */
    Mutil_Document:37,
    /**
     * 相关产品
     */
    Related_Products:38,
    /**
     * 公文种类
     */
    Document_Type:52,
    /**
     * 紧急程度
     */
    Urgent_Level:53,
    /**
     * 保密级别
     */
    Secret_Level:54,
    /**
     * 发文字号
     */
    Issue_Number:55,
    /**
     * 系统地址
     */
    System_Address:56,
    /**
     * 多部门
     */
    Mutil_Department:57,
    /**
     * 城市
     */
    City:58,
    /**
     * 称谓
     */
    Title:59,
    /**
     * 客户类型
     */
    CRM_Type:60,
    /**
     * 客户描述
     */
    CRM_Description:61,
    /**
     * 客户规模
     */
    CRM_Scope:62,
    /**
     * 行业
     */
    Industry:63,
    /**
     * 邮件列表
     */
    EMail_List:64,
    /**
     * 多角色
     */
    Mutil_Role:65,
    /**
     * 计量单位
     */
    Unit:69,
    /**
     * 会议室
     */
    Meetting_Rome:87,
    /**
     * 会议类型
     */
    Meetting_Type:89,
    /**
     * 空会议室使用情况
     */
    Space_Meetting_Rome_Info:118,
    /**
     * 专业
     */
    Profession:119,
    /**
     * 目标
     */
    Goal:125,
    /**
     * 报告
     */
    Report:126,
    /**
     * 项目模板
     */
    Project_Template:129,
    /**
     * 预算(预算审批流转单专用)
     */
    Budget_For_approval:134,
    /**
     * 多项目
     */
    Mutil_Project:135,
    /**
     * 车辆
     */
    Car:137,
    /**
     * 人力资源条件
     */
    Resource_Condition:141,
    /**
     * 收发文单位
     */
    Document_Department:142,
    /**
     * 多流程
     */
    Mutil_Request:152,
    /**
     * 角色人员
     */
    Role_Member:160,
    /**
     * 自定义浏览框
     */
    Custom:161,
    /**
     * 自定义多选浏览框
     */
    Mutil_Custom:162,
    /**
     * 分部
     */
    SubCompany:164,
    /**
     * 分权人力资源
     */
    Resource_Right:165,
    /**
     * 分权多人力资源
     */
    Mutil_Resource_Right:166,
    /**
     * 分权部门
     */
    Department_Right:167,
    /**
     * 分权多部门
     */
    Mutil_Department_Right:168,
    /**
     * 分权分部
     */
    SubCompany_Right:169,
    /**
     * 分权多分部
     */
    Mutil_SubCompany_Right:170,
    /**
     * 归档流程
     */
    Archiving_Request:171,
    /**
     * 年份
     */
    Year:178,
    /**
     * 资产资料
     */
    Capital_Source:179,
    /**
     * 网上调查
     */
    Survey:182,
    /**
     * 多会议
     */
    Mutil_Meetting:184,
    /**
     * 多分部
     */
    Mutil_SubCompany:194,
    /**
     * Sap浏览框
     */
    SAP_Custom:224,
    /**
     * 系统集成浏览框
     */
    System_Custom:226,
    /**
     * 资产类型
     */
    Capital_Type:242,
    /**
     * 资产状态
     */
    Capital_Status:243,
    /**
     * 项目类型
     */
    Project_Type:244,
    /**
     * 工作类型
     */
    Work_Type:245,
    /**
     * 项目状态
     */
    Project_Status:246,
    /**
     * 成本中心
     */
    Cost_Center:251,
    /**
     * 自定义树形浏览框
     */
    Tree_Custom:256,
    /**
     * 自定义树形多选浏览框
     */
    Mutil_Tree_Custom:257,
    /**
     * 国家
     */
    Country:258,
    /**
     * 语言
     */
    Lanuage:259,
    /**
     * 职称
     */
    JobTitle:260,
    /**
     * 多维度组织
     */
    Organization:261,
    /**
     * 工作地点
     */
    Work_Place:262,
    /**
     * 区县
     */
    District:263,
    /**
     * 客户状态
     */
    CRM_Status:264,
    /**
     * 获取方式
     */
    Get_Way:265,
    /**
     * 角色
     */
    Role:267,
    /**
     * 星期多选
     */
    Mutil_Week:268,
    /**
     * 多提醒方式
     */
    Mutil_Remide_Way:269,
    /**
     * 服务项目
     */
    Service:270,
    /**
     * 商机来源
     */
    Business_Source:274,
    /**
     * 多岗位
     */
    Mutil_Job:278,
    /**
     * 合同
     */
    Contract:279,
    /**
     * 班次
     */
    Shifts:280, 
}

export const Mutil = [
    Map.Mutil_CRM,
    Map.Mutil_Custom,
    Map.Mutil_Department,
    Map.Mutil_Department_Right,
    Map.Mutil_Document,
    Map.Mutil_Job,
    Map.Mutil_Meetting,
    Map.Mutil_Project,
    Map.Mutil_Remide_Way,
    Map.Mutil_Request,
    Map.Mutil_Resource,
    Map.Mutil_Resource_Right,
    Map.Mutil_Role,
    Map.Mutil_SubCompany,
    Map.Mutil_SubCompany_Right,
    Map.Mutil_Tree_Custom,
    Map.Mutil_Week
]

export default Map;