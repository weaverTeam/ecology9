import React from 'react';
import { WeaInput } from 'ecCom';
import _prefixCls from '../_util/prefixCls';
import { Tooltip } from 'mode-coms';

export default class TextField extends React.Component {
    static defaultProps = {
        prefixCls: `${_prefixCls}-field-text`,
        overlayClassName: `${_prefixCls}-field-text-overlay`,
    }
    onChange = (value, name, obj) => {
        const { fieldId, setFieldsValue } = this.props;
        setFieldsValue({ [`field${fieldId}`]: value });
    }
    render() {
        const { prefixCls, overlayClassName } = this.props;
        const _props = {};
        if ('fieldId' in this.props) {
            _props.fieldname = `field${this.props.fieldId}`;
        }
        if ('viewAttr' in this.props) {
            _props.viewAttr = this.props.viewAttr;
        }
        if ('value' in this.props) {
            _props.value = this.props.value;
        }
        if ('onChange' in this.props) {
            _props.onChange = this.props.onChange;
        }
        if ('onBlur' in this.props) {
            _props.onBlur = this.props.onBlur;
        }
        if ('onFocus' in this.props) {
            _props.onFocus = this.props.onFocus;
        }
        _props.placeholder = "请输入";
        const renderTitle = () => {
            return <span>
                {this.props.value || _props.placeholder}
            </span>;
        }
        return (
            <WeaInput {..._props} className={`${prefixCls}`} onChange={this.onChange} />
        )
    }
}