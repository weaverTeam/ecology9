import React from 'react';
import PropTypes from 'prop-types';
import _prefixCls from '../_util/prefixCls';
import { Form, Col } from 'mode-coms';
import FieldMap from './FieldMap';
import './style/index.less';

export default class Field extends React.Component {
    static defaultProps = {
        form: {},
        field: {},
        prefixCls: `${_prefixCls}-field`,
        span: 12
    }
    static propTypes = {
        prefixCls: PropTypes.string,
        form: PropTypes.any.isRequired,
        field: PropTypes.object.isRequired,
    }
    render() {
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        const { span, form, field, prefixCls } = this.props;
        const { getFieldDecorator } = form;
        const { fieldHtmlType, type, fieldDbType } = field;
        let FieldCom = null;
        if (fieldHtmlType in FieldMap) {
            if (type in FieldMap[fieldHtmlType]) {
                FieldCom = FieldMap[fieldHtmlType][type];
            } else {
                FieldCom = FieldMap[fieldHtmlType].default;
            }
        } else {
            FieldCom = FieldMap.default;
        }
        return (
                <Form.Item
                    className={prefixCls}
                    {...formItemLayout}
                    label={field.showTitle}
                >
                    {getFieldDecorator(`field${field.fieldId}`, {
                    })(<FieldCom  {...field} />)}
                </Form.Item>
        )
    }
}