import { WeaBrowser, WeaInput, WeaTextarea, WeaCheckbox, WeaSelect } from 'ecCom';


import TextField from './TextField';
import NumberField from './NumberField';
import BrowserField from './BrowserField';
import SelectField from './SelectField';
export default {
    '1': {//文本框
        '1': TextField,//文本字段
        '2': NumberField,//整数字段
        '3': NumberField,//浮点数字段
        '4': NumberField,//金额转换字段
        '5': NumberField,//千分位字段
        default: TextField
    },
    '2': {//多行文本框
        '1': TextField, //多行文本
        '2': TextField, //富文本框
        default: TextField
    },
    '3': {//多行文本框
        default: BrowserField
    },
    '4': {//check框
        default: WeaCheckbox
    },
    '5': {//下拉框
        default: SelectField
    },
    /*'6': {
        default: WeaInput
    },*/
    default: TextField
}