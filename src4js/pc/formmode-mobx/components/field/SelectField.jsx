import React from 'react';
import _prefixCls from '../_util/prefixCls';
import { Tooltip, Select } from 'mode-coms';

export default class TextField extends React.Component {
    static defaultProps = {
        prefixCls: `${_prefixCls}-field-text`,
        overlayClassName: `${_prefixCls}-field-text-overlay`,
    }
    onChange = (value, name, obj) => {
        console.log('value', value)
        const { fieldId, setFieldsValue } = this.props;
        setFieldsValue({ [`field${fieldId}`]: value });
    }
    shouldComponentUpdate(nextProps) {
        console.log(nextProps, nextProps.value, this.props.value, nextProps.value != this.props.value)
        return nextProps.value != this.props.value;
    }
    filterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
    render() {
        const { prefixCls, overlayClassName, placeholder = "请选择", options = [] } = this.props;

        const defaultValue = options.filter(opt => opt.selected).map(opt => opt.key);
        return (
            <Select
                defaultValue={defaultValue}
                showSearch
                allowClear
                placeholder={placeholder}
                optionFilterProp="children"
                onChange={this.onChange}
                filterOption={this.filterOption}
                size="large"
            >
                {
                    options.map(opt => (<Select.Option value={opt.selectValue}>{opt.selectName}</Select.Option>))
                }
            </Select>
        )
    }
}