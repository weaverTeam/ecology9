import React from 'react';
import { WeaBrowser } from 'ecCom';
import _prefixCls from '../_util/prefixCls';
import FieldTypeMap, { Mutil } from './FieldTypeMap';

class BrowserField extends React.Component {
    onChange = (value, name, obj) => {
        const { fieldId, setFieldsValue } = this.props;
        setFieldsValue({ [`field${fieldId}`]: value })
    }
    render() {
        const { fieldId, fieldName, fieldHtmlType, fieldDbType, billId, type, fieldAttr, viewType } = this.props;
        const field = {
            fieldName: `field${fieldId}`,
            type: parseInt(type),
            isSingle: Mutil.indexOf(parseInt(type)) < 0,
            fieldAttr,
            isDetail: viewType,
            placeholder: '请选择',
            dataParams: { type: fieldDbType, formid: billId },
            conditionDataParams: { type: fieldDbType, formid: billId },
        }
        const modal = {
            hasAdvanceSerach: true,
        }
        return (
            <WeaBrowser {...field} {...modal} onChange={this.onChange} />
        )
    }
}

export default BrowserField;