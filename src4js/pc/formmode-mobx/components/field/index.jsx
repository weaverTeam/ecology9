import Field from './Field';
import QueryField from './QueryField';

Field.QueryField = QueryField;

export default Field;