import React from 'react';
import { inject, observer } from 'mobx-react';
import { Table } from 'mode-coms';
import _prefixCls from '../_util/prefixCls';
@inject('searchTableStore')
@observer
class ComTable extends React.Component {
    constructor(props) {
        super(props);
        this.store = props.searchTableStore;
        this.scrollWidth = 0;

    }
    static defaultProps = {
        prefixCls: `${_prefixCls}-search`,
    }
    componentDidMount() {
        jQuery(window).resize(this.handleResize);
    }
    handleResize = () => {
        clearTimeout(this.resizeTimer);
        this.resizeTimer = setTimeout(() => {
            this.forceUpdate();
        }, 200);
    }
    containerRef = (ref) => {
        this._container = ref;
    }
    renderRowClass = (record, index) => {
        const { prefixCls, } = this.props;
        return `${prefixCls}-tr ${prefixCls}-tr-${index}`
    }
    parseShowColumns = (showColumns) => {
        this.scrollWidth = 0;
        const { prefixCls } = this.props;
        const clientWidth = document.documentElement.clientWidth;
        return showColumns.map(column => {
            const width = Math.max(parseInt(column.colWidth * 15), 150);
            this.scrollWidth += width;
            return {
                dataIndex: column.dataIndex,
                key: column.dataIndex,
                title: column.showTitle,
                className: `${prefixCls}-td`,
                width,
                render: (text, record, index) => {
                    return <div className={`${prefixCls}-cell`} dangerouslySetInnerHTML={{ __html: text }}></div>
                }
            }
        })
    }
    changeTable = (pageIndex, pageSize) => {
        const { guid, prefixCls } = this.props;
        this.store.datas(guid, {pageIndex, pageSize});
    }
    wrapRef = (ref) => {
        this._wrap = ref;
    }
    render() {
        console.log('-----------------------render search/table')
        const { guid, prefixCls } = this.props
        const { loading, datas, showColumns, pageIndex, pageSize, total} = this.store.getState(guid);
        const columns = this.parseShowColumns(showColumns);
        const scroll = { x: this.scrollWidth, y: jQuery(this._wrap).height() - 110 }
        return (
            <div
                className={`${prefixCls}-scroll-wrap`}
                ref={this.wrapRef}>
                <Table
                    ref={this.containerRef}
                    loading={loading}
                    scroll={scroll}
                    dataSource={datas}
                    columns={columns}
                    rowClassName={this.renderRowClass}
                    rowSelection={{}}
                    pagination={{
                        current: pageIndex,
                        pageSize: pageSize,
                        total: total,
                        showSizeChanger: true,
                        pageSizeOptions: ['10', '20', '50', '100'],
                        showQuickJumper: true,
                        showTotal: (total, range) => `第${range[0]}-${range[1]}行，共${total}行`,
                        onShowSizeChange: this.changeTable,
                        onChange: this.changeTable
                    }}
                >
                </Table>
            </div>
        )
    }
}

export default ComTable;