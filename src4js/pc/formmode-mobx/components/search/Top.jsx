import React from 'react';

class Top extends React.Component {
    render() {
        return (
            <div className={`${prefixCls}-top`}>
                <div className={`${prefixCls}-top-icon`}>
                    <img src={icon} />
                </div>
                <div className={`${prefixCls}-top-wrap`}>

                    <div className={`${prefixCls}-top-title`}>
                        <div className={`${prefixCls}-top-name`}>{name}</div>
                        <div className={`${prefixCls}-top-desc`}>{desc}</div>
                    </div>
                    <div className={`${prefixCls}-top-buttons`}>
                        <div className={`${prefixCls}-top-buttons-wrap`}>
                            <ButtonGroup>
                                {
                                    quickButtons.map((button, index) => {
                                        return (
                                            <Button key={button.expandId} type={index == 0 ? "primary" : ""}>{button.listBatchName}</Button>
                                        )
                                    })
                                }
                                {disQuickSearch ? null : <Input.Search
                                    className={`${prefixCls}-top-search`}
                                    placeholder="请输入"
                                />}
                                <Popover
                                    content={this.renderCustomSearch()}
                                    placement="bottomRight"
                                    trigger="click"
                                    visible={advancedQueryShow}
                                >
                                    <Button onClick={this.showAdvancedQuery} >高级查询</Button>
                                </Popover>
                            </ButtonGroup>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}