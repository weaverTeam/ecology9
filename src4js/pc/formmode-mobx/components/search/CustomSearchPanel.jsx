import React from 'react';
import ReactDOM from 'react-dom';
import { observer, inject } from 'mobx-react';
import { Collapse, Row, Col, Button, Form } from 'mode-coms';
import _prefixCls from '../_util/prefixCls';
import { WeaSearchGroup, WeaScroll } from 'ecCom';
import Field from '../field';
const QueryField = Field.QueryField;
class CustomSearchPanel extends React.Component {
    static defaultProps = {
        prefixCls: `${_prefixCls}-search-condition-panel`
    }
    componentDidMount(){
        console.log(ReactDOM.findDOMNode(this.refs.scroll))
        ReactDOM.findDOMNode(this.refs.scroll).style['overflow-x'] = 'hidden!important';
        jQuery(ReactDOM.findDOMNode(this.refs.scroll)).css({'overflow-x':'hidden!important'})

    }
    handleSubmit = (e) => {
        e.preventDefault();
        const { guid } = this.props;
        this.props.store.toggleAdvancedQueryPanel(guid, false)
        this.props.store.search(guid);
    }
    validateFields = (e) => {
        const { validateFieldsAndScroll } = this.props.form;
        validateFieldsAndScroll();
    }
    resetFields = () => {
        const { resetFields } = this.props.form;
        resetFields();
    }
    render() {
        const { prefixCls, form, guid, height } = this.props;
        const { queryColumns, advancedQueryShow } = this.props.store.getState(guid);
        const { getFieldDecorator, validateFieldsAndScroll, resetFields } = form;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        return (
            <div className={prefixCls}
            >
                <Form
                    onSubmit={this.handleSubmit}
                >
                    <div className={`${prefixCls}-scroll-wrap`}
                        style={{ height }}
                    >
                        <WeaScroll
                            ref="scroll"
                            className={`${prefixCls}-scroll`}
                            typeClass="scrollbar-macosx"
                            style={{overflowX:'hidden !important'}}
                        >
                            <div className={`${prefixCls}-title`}>查询条件</div>
                            <Row style={{ marginLeft: 20 }}>
                                {
                                    queryColumns.map(column => (
                                        <Col span={12}>
                                            <QueryField form={form} field={column} />
                                        </Col>
                                    ))
                                }
                            </Row>
                        </WeaScroll>
                    </div>
                    <Row style={{ padding: 15, borderTop: '1px solid #e5e5e5' }}>
                        <Col span={24} style={{ textAlign: 'center' }}>
                            <Button type="primary" htmlType="submit" onClick={this.validateFields}>搜索</Button>
                            <Button style={{ marginLeft: 8 }} onClick={this.resetFields}>清空条件</Button>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}



export default Form.create({
    onFieldsChange: (props, fields) => {
        const { guid, store } = props;
        console.log(fields)
        store.saveToConditionForm(guid, fields);
    },
    mapPropsToFields: (props) => {
        const { guid, store } = props;
        const { conditionForm } = store.getState(guid)
        console.log('conditionForm',conditionForm)
        return { ...conditionForm }
    }

})(CustomSearchPanel);