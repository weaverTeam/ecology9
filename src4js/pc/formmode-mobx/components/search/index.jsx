import React from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import PropTypes from 'prop-types';
import TweenOne from 'rc-tween-one';
import { Icon, Button, Input, Popover, Tabs } from 'mode-coms';
import _prefixCls from '../_util/prefixCls';
import Coms from '../_util/comids';
import './style/index.less';
import CustomSearchPanel from './CustomSearchPanel';
import { WeaRightMenu } from 'ecCom';
import ComTable from './Table';
import { SearchAction } from 'weaFormmodeActions';
import ThreeSideLayoutStore from '../../stores/three-side-layout';
const ButtonGroup = Button.Group;


@inject('searchStore')
@observer
class Search extends React.Component {
    constructor(props) {
        super(props);
        const { searchStore, buttons, guid, layoutGuid } = props;
        this.store = searchStore;
        this.Actions = new SearchAction({ store: searchStore, guid: guid, layoutStore: ThreeSideLayoutStore, layoutGuid });
        this.quickButtons = buttons.filter(button => button.shortCutButtonBool || (button.batchSetId == "0" && button.isSystemFlag == '101'));
        this.rightMenus = buttons.map(button => ({ key: button.isSystemFlag, content: button.listBatchName }));
    }
    static defaultProps = {
        prefixCls: `${_prefixCls}-search`,
        conditionPrefixCls: `${_prefixCls}-search-condition`,
    }
    componentDidMount() {
        const { guid, pageNumber } = this.props;
        this.store.search(guid, { pageNumber });
    }
    calQueryHeight = (queryColumns = []) => {
        const length = queryColumns.length;
        const rows = length / 2 + length % 2 + 1;
        return Math.min(400, rows * 42);
    }
    renderCustomSearch = (height) => {
        const { guid } = this.props;
        return <CustomSearchPanel
            height={height}
            guid={guid}
            store={this.store}
        />
    }
    toggleAdvancedQuery = (e, isAdvancedQueryShow) => {
        const { guid } = this.props;
        if (typeof isAdvancedQueryShow === 'boolean') {
            this.store.toggleAdvancedQueryPanel(guid, isAdvancedQueryShow);
        } else {
            this.store.toggleAdvancedQueryPanel(guid);
        }
    }

    containerRef = (ref) => {
        this._container = ref;
    }
    renderPaginationShowTitle = (total, range) => `第${range[0]}-${range[1]}行，共${total}行`;
    handleTabChange = (key) => {
        this.store.search(this.props.guid, { groupFieldKey: key });
    }
    handleQuickButtonClick = (isSystemFlag, e) => {
        if (e) e.stopPropagation();
        console.log(this.Actions.getAction(parseInt(isSystemFlag)))
        this.Actions.runAction(parseInt(isSystemFlag));
    }
    handleMenuClick = (key, e) => {
        //this.event.stopPropagation();
        this.Actions.runAction(parseInt(key));
    }
    render() {
        console.log("----------------render search")
        const { prefixCls, conditionPrefixCls, guid, icon, name, desc, disQuickSearch, isShowQueryCondition, buttons } = this.props;
        const { loading, queryColumns, primaryKey, advancedQueryShow, dataKey, groups, groupsMap } = this.store.getState(guid);
        const maskClassName = advancedQueryShow ? `${prefixCls}-mask ${prefixCls}-mask-show` : `${prefixCls}-mask`;
        const advancedClassName = advancedQueryShow ? `${prefixCls}-advanced ${prefixCls}-advanced-show` : `${prefixCls}-advanced`;
        const queryHeight = this.calQueryHeight(queryColumns);

        return (
            <div
                className={prefixCls}
                ref={this.containerRef}
            >
                <WeaRightMenu datas={this.rightMenus} onClick={this.handleMenuClick}>
                    <div
                        className={maskClassName}
                        onClick={this.toggleAdvancedQuery}
                    >
                    </div>
                    <div
                        className={advancedClassName}
                        style={{ height: queryHeight }}
                    >
                        {this.renderCustomSearch(queryHeight)}
                    </div>
                    <div className={`${prefixCls}-top`}>
                        <div className={`${prefixCls}-top-icon`}>
                            <img src={icon} />
                        </div>
                        <div className={`${prefixCls}-top-wrap`}>
                            <div className={`${prefixCls}-top-title`}>
                                <div className={`${prefixCls}-top-name`}>{name}</div>
                                <div className={`${prefixCls}-top-group`}>
                                    {
                                        groups.length > 0 ? <Tabs size="small" onChange={this.handleTabChange}>
                                            {
                                                groups.map(group => (
                                                    <Tabs.TabPane tab={`${group.name}(${groupsMap[group.key]||0})`} key={group.key}></Tabs.TabPane>
                                                ))
                                            }
                                        </Tabs> : null
                                    }

                                </div>
                            </div>
                            <div className={`${prefixCls}-top-buttons`}>
                                <div className={`${prefixCls}-top-buttons-wrap`}>
                                    <ButtonGroup>
                                        {
                                            this.quickButtons.map((button, index) => {
                                                return (
                                                    <Button
                                                        key={button.expandId}
                                                        type={index == 0 ? "primary" : ""}
                                                        onClick={this.handleQuickButtonClick.bind(this, button.isSystemFlag)}
                                                    >
                                                        {button.listBatchName}
                                                    </Button>
                                                )
                                            })
                                        }
                                        {disQuickSearch ? null : <Input.Search
                                            className={`${prefixCls}-top-search`}
                                            placeholder="请输入"
                                        />}
                                        <Button onClick={this.toggleAdvancedQuery} >高级查询</Button>
                                    </ButtonGroup>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ComTable
                        guid={guid}
                    />
                </WeaRightMenu>
            </div>
        )
    }
}



export default Search;