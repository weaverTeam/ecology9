
import { SearchAction } from 'weaFormmodeActions';
export default class Actions {
    constructor(props) {
        this.guid = props.guid;
        this.store = props.store;
        this.search = this.search.bind(this);
        this.searchAction = new SearchAction(props);
    }
    search = () => {
        this.store.toggleAdvancedQueryPanel(this.guid, false);
        this.store.search(this.guid);
    }
    add = () => {
        this.otherProps.toggleRight();
    }
    defaultAction = () => { }

    getAction = (isSystemFlag) => {
        switch (isSystemFlag) {
            case 100:
                return this.search;
            case 101:
                return this.add;
            default:
                return this.defaultAction;
        }
    }
    runAction = (expandId) => {
        console.log(expandId);
        console.log(this.getAction(expandId))
        this.getAction(expandId)();
    }

}