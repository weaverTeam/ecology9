import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import TweenOne from 'rc-tween-one';
import { Icon, Button, Spin } from 'mode-coms';
import prefixCls from '../_util/prefixCls';
import Coms from '../_util/comids';
import './style/index.less';
import Docker from '../docker';

@inject('threeSideLayoutStore')
@observer
class ThreeSideLayout extends React.Component {
    constructor(props) {
        super(props);
        this.store = props.threeSideLayoutStore;
        this.toggleRightTimer = null;
    }
    static defaultProps = {
        prefixCls: `${prefixCls}-three-side-layout`,
        middleAnimation: { left: 0, duration: 500, },
        leftAnimation: { width: 0, opacity: 0, duration: 500, },
        rightAnimation: { left: 700, opacity: 1, duration: 500 },
        leftWidth: 400,
    }
    static propTypes = {
        prefixCls: PropTypes.string,
        middleAnimation: PropTypes.object,
        leftAnimation: PropTypes.object,
        rightAnimation: PropTypes.object,
        leftWidth: PropTypes.number,
        leftShow: PropTypes.bool,
        sessionKey: PropTypes.string,
        tweenone: PropTypes.object,
        rightTweenone: PropTypes.object,
        child: PropTypes.object,
    }
    render() {
        const { prefixCls, guid, leftAnimation, middleAnimation, rightAnimation, leftWidth, leftProps, middleProps, rightProps } = this.props;
        const { tweenone, rightTweenone, leftShow } = this.store.getState(guid);
        if (!leftShow) {
            rightAnimation.left = 300;
        }
        return (
            <div
                className={prefixCls}
            >
                <TweenOne
                    className={`${prefixCls}-left`}
                    animation={leftAnimation}
                    {...tweenone}
                    style={{ width: leftWidth }}
                >
                    {leftProps && <Docker {...leftProps} layoutGuid={guid} />}
                </TweenOne>
                <TweenOne
                    className={`${prefixCls}-middle`}
                    animation={middleAnimation}
                    {...tweenone}
                    style={{ left: leftWidth }}
                    onClick={this.hideRight}
                >
                    <div className={`${prefixCls}-toggle`}>
                        <Button
                            type="primary"
                            shape="circle"
                            className={`${prefixCls}-toggle-button`}
                            onClick={this.toggleLeft}
                        >
                            <Icon type={`${leftShow ? 'left' : 'right'}`} style={{ paddingLeft: 14 }} />
                        </Button>
                    </div>
                    {middleProps && <Docker {...middleProps} layoutGuid={guid} />}
                </TweenOne>
                <TweenOne
                    className={`${prefixCls}-right`}
                    animation={rightAnimation}
                    {...rightTweenone}
                >
                    {rightProps && <Docker {...rightProps} layoutGuid={guid} />}
                </TweenOne>
            </div>
        )
    }

    toggleLeft = (e) => {
        const { guid } = this.props;
        this.store.toggleLeft(guid);
        const { propoverHandles = [] } = window.ComsMode || {};
        for (const fun of propoverHandles) {
            fun(e);
        }
    }
    hideRight = (e) => {
        const { guid } = this.props;
        this.store.toggleRight(guid, false);
    }
    toggleRight = (e) => {
        const { guid } = this.props;
        this.store.toggleRight(guid);

    }
}

export default ThreeSideLayout;