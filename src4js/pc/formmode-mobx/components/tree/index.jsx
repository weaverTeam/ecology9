import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import { Icon, Button, Tree, Row, Col, Input } from 'mode-coms';
import _prefixCls from '../_util/prefixCls';
import Coms from '../_util/comids';
import './style/index.less';
import TreeNode from './TreeNode';
import { WeaScroll } from 'ecCom';
const Search = Input.Search;

@inject('treeStore')
@observer class Index extends React.Component {
    constructor(props) {
        super(props);
        this.store = props.treeStore;
    }
    static propTypes = {
        guid: PropTypes.string,
        sessionKey: PropTypes.string.isRequired,
        actions: PropTypes.any,
        prefixCls: PropTypes.string,
        datas: PropTypes.array,
        suffixs: PropTypes.array,
        treename: PropTypes.string,
        rootname: PropTypes.string,
        rooticon: PropTypes.any,
    }
    static defaultProps = {
        prefixCls: `${_prefixCls}-tree`
    }
    datas = (treeNode) => {
        const { guid } = this.props;
        const parentkey = treeNode.props.eventKey;
        return new Promise((resolve) => {
            this.store.datas({ guid, parentkey: parentkey == 'root' ? '' : parentkey, resolve });
        });
    }
    render() {
        console.log('---------------render tree')
        const { prefixCls, guid, treename, rootname, rooticon } = this.props;
        const { datas, suffixs } = this.store.getState(guid);
        const realDatas = [{ key: "root", name: rootname, icon: rooticon, children: datas }]
        return (
            <div className={prefixCls}>
                <div className={`${prefixCls}-top`}>
                    <span className={`${prefixCls}-top-title`}>{treename}</span>
                    <Search
                        className={`${prefixCls}-top-search`}
                        placeholder="input search text"
                        onSearch={value => console.log(value)}
                    />
                </div>
                <div className={`${prefixCls}-scroll-wrap`} >
                    <WeaScroll
                        className={`${prefixCls}-scroll`}
                        typeClass="scrollbar-macosx"
                    >
                        <Tree
                            showLine={false}
                            onSelect={() => {
                            }}
                            loadData={this.datas}
                        >
                            {this.renderTreeNodes(realDatas, suffixs)}
                        </Tree>
                    </WeaScroll>
                </div>
            </div>
        )
    }

    renderTreeNodes = (datas, suffixs = [], deep = 0) => {
        const { prefixCls } = this.props;
        if (datas && datas.length > 0) {
            return datas.map((node) => {
                return (
                    <TreeNode
                        className={`${prefixCls}-node`}
                        wrapClassName={(selected) => (selected ? `${prefixCls}-node-wrap ${prefixCls}-node-wrap-selected` : `${prefixCls}-node-wrap`)}
                        key={node.key}
                        wrapStyle={{ paddingLeft: deep * 18 }}
                        title={this.renderTitle(node, suffixs)}
                        isLeaf={node.isLeaf}
                    >
                        {node.children && node.children.length > 0 ? this.renderTreeNodes(node.children, suffixs, deep + 1) : null}
                    </TreeNode>
                )
            })
        } else {
            return null;
        }
    }
    renderTitle = (node, suffixs) => {
        const { prefixCls } = this.props;
        return (
            <Row
                className={`${prefixCls}-node-title`}
                onMouseEnter={this.titleHoverIn}
                onMouseLeave={this.titleHoverOut}
            >
                <Col span="16" className={`${prefixCls}-node-title-text`} title={node.name}>
                    {node.icon && node.icon != "" && <img src={node.icon} width={16} height={16} />}
                    <span style={{ height: 26, verticalAlign: 'middle' }}>{node.name}</span>
                </Col>
                <Col span="8" className={`${prefixCls}-node-title-suffix`}>
                    <div>
                    </div>
                </Col>
            </Row>
        )
    }
    titleHoverIn = (e) => {


    }
    titleHoverOut = (e) => {

    }
}

export default Index;