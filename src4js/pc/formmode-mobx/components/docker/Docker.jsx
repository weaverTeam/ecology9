import React from 'react';
import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import prefixCls from '../_util/prefixCls';
import Coms from '../_util/comids';
import generateUUID from '../_util/generateUUID';
class Docker extends React.Component {
    constructor(props) {
        super(props);
    }
    static defaultProps = {
        prefixCls: `${prefixCls}-docker`
    }
    static propTypes = {
        prefixCls: PropTypes.string,
        guid: PropTypes.string,
        child: PropTypes.object
    }
    dockerRef = (ref) => { this._docker = ref; }
    componentWillReact() {
        console.log("Docker will re-render, since the todo has changed!");
    }
    render() {
        const { prefixCls, guid, child, layoutGuid } = this.props;
        let ChildCom = child && Coms[child.component];
        return (
            <div
                ref={this.dockerRef}
                className={prefixCls}
            >
                {ChildCom && <ChildCom {...child.props} guid={guid} layoutGuid={layoutGuid}/>}
            </div>
        )
    }
}

export default Docker;