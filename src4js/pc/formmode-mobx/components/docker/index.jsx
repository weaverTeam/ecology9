import React from 'react';
import Docker from './Docker';
import RouterDocker from './RouterDocker';
import './style/index.less';
Docker.RouterDocker = RouterDocker;
export default Docker;