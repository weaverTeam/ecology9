import React from 'react';
import { observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import {WeaPopoverHrm} from 'ecCom';
import PropTypes from 'prop-types';
import prefixCls from '../_util/prefixCls';
import Coms from '../_util/comids';
import generateUUID from '../_util/generateUUID';
@inject('dockerStore')
@observer
class RouterDocker extends React.Component {
    constructor(props) {
        super(props);
        this.store = props.dockerStore;
        this.isSingle = window.location.href.indexOf("") >=0
    }
    static defaultProps = {
        prefixCls: `${prefixCls}-docker`
    }

    componentWillReact() {
        console.log("routerDocker will re-render, since the todo has changed!");
    }
    componentDidMount = () => {
        const { location: { query: { guid, id } = {} } = {} } = this.props;
        if (id || guid) {
            this.store.initPage({ guid, id });
        }
    }
    dockerRef = (ref) => { this._docker = ref; }
    render() {
        const { prefixCls } = this.props;
        const { child, guid } = this.store.state;
        let ChildCom = child.component ? Coms[child.component] : null
        return (
            <div
                ref={this.dockerRef}
                className={prefixCls}
            >
                {this.isSingle&&<WeaPopoverHrm />}
                {ChildCom && <ChildCom {...child.props} guid={guid} />}
            </div>
        )
    }
}

export default RouterDocker;