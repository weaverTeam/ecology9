import Docker from '../docker';
import ThreeSideLayout from '../three-side-layout';
import Tree from '../tree';
import Search from '../search';
export default {
    Docker,
    ThreeSideLayout,
    Tree,
    Search
}