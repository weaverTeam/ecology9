import dockerStore from './docker';
import threeSideLayoutStore from './three-side-layout';
import treeStore from './tree';
import searchStore from './search';

export default {
    dockerStore,
    threeSideLayoutStore,
    treeStore,
    ...searchStore,
}