import { observable, action, autorun } from 'mobx';
import searchTableStore from './SearchTableStore';
import API from '../../apis';

class SearchStore {
    @observable
    state = {
        init: {
            advancedQueryShow: false,
            buttons: [],
            queryColumns: [],
            conditionForm: {},
            groups: []
        }
    }
    @action
    update = (modeId, payload) => {
        this.state = {
            ...this.state,
            [modeId]: {
                ...(this.state[modeId] || this.state.init),
                ...payload
            }
        }
    }
    search = (guid, params = { pageNumber: 10 }) => {
        const _state = this.getState(guid);
        const { dataKey, conditionForm, groupFieldKey } = _state;

        const fieldsMap = {};
        for (let key in conditionForm) {
            const field = conditionForm[key] || {};
            if ('name' in field && typeof field.value != 'undefined') {
                fieldsMap[field.name] = field.value;
            }
        }
        const { pageSize = params.pageNumber, pageIndex } = searchTableStore.getState(guid);
        const paramsTmp = { guid, oldDataKey: dataKey, ...fieldsMap, pageSize, pageIndex };
        if('groupFieldKey' in _state) {
            paramsTmp.groupFieldKey = _state.groupFieldKey;
        }
        if ('groupFieldKey' in params) {
            paramsTmp.groupFieldKey = params.groupFieldKey;
            this.update(guid, { groupFieldKey: params.groupFieldKey });
        }
        API.Search.search(paramsTmp).then((data) => {
            const { api_status, api_errormsg } = data;
            if (api_status) {
                this.update(guid, { ...data });
                searchTableStore.dataKey(guid, data.dataKey);
            } else {
                console.log(api_errormsg);
                throw api_errormsg;
            }
        })
    }

    saveToConditionForm = (guid, fields) => {
        const _state = this.getState(guid);
        console.log('fields', fields)
        this.state = {
            ...this.state,
            [guid]: {
                ..._state,
                conditionForm: {
                    ..._state.conditionForm,
                    ...fields
                }
            }
        }
    }
    toggleAdvancedQueryPanel = (guid, isShow) => {
        if (typeof isShow == 'boolean') {
            this.update(guid, { advancedQueryShow: isShow })
        } else {
            const _state = this.getState(guid);
            this.update(guid, { advancedQueryShow: !_state.advancedQueryShow });
        }
    }
    getState = (guid) => {
        if (guid in this.state) {
            return this.state[guid];
        } else {
            return this.state.init;
        }
    }
    getConditionForm = (guid) => {
        if (guid in this.state) {
            return this.state[guid].conditionForm;
        } else {
            return this.state.init.conditionForm;
        }
    }
}

export default {
    searchStore: new SearchStore(),
    searchTableStore
}