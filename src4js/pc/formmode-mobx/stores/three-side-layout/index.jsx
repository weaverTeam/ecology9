import { observable, action, autorun } from 'mobx';
import API from '../../apis';

class ThreeSideLayoutStore {
    @observable
    state = {
        init: {
            tweenone: { paused: true, reverse: false, moment: null },
            rightTweenone: { paused: true, reverse: false, moment: null },
            leftShow: true,
            rightShow: false,
        }
    }
    @action
    update = (modeId, info) => {
        return {
            ...this.state,
            [modeId]: {
                ...(this.state[modeId] || {}),
                ...info
            }
        }
    }
    @action
    toggleRight = (modeId, isShow) => {
        const _state = { ...(this.state[modeId] || this.state.init) };
        const { rightShow = false } = _state;
        if (rightShow == isShow) return;
        let rightTweenone;
        if (!rightShow) {
            rightTweenone = {
                reverse: false,
                paused: false,
                moment: null,
            }
        } else {
            rightTweenone = {
                reverse: true,
                paused: false,
                moment: null,
            }
        }
        this.state = {
            ...this.state,
            [modeId]: { ..._state, rightShow: !_state.rightShow, rightTweenone },
        }
    }
    @action
    toggleLeft = (modeId) => {
        const _state = { ...(this.state[modeId] || this.state.init) };
        const { leftShow = true } = _state;
        let tweenone;
        if (leftShow) {
            tweenone = {
                reverse: false,
                paused: false,
                moment: null,
            }
        } else {
            tweenone = {
                reverse: true,
                paused: false,
                moment: null,
            }
        }
        this.state = {
            ...this.state,
            [modeId]: { ..._state, leftShow: !_state.leftShow, tweenone },
        }
    }
    getState = (guid) => {
        if (guid in this.state) {
            return this.state[guid];
        } else {
            return this.state.init;
        }
    }
}

export default new ThreeSideLayoutStore();