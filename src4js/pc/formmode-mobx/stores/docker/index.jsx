import { observable, action, autorun } from 'mobx';
import API from '../../apis';

class DockerStore {
    @observable
    state = {
        child: {}
    }
    constructor() {
    }
    @action
    update = (info) => {
        this.state = {
            ...this.state,
            ...info
        }
    }
    initPage = (info) => {
        const { guid, id } = info;
        API.Docker.init(info).then(({ child, guid, api_errormsg, api_status }) => {
            if (api_status) {
                this.update({ child, guid });
            } else {
                console.log(api_errormsg);
                throw api_errormsg;
            }
        })
    }
}
const dockerStore = new DockerStore()
export default dockerStore;