import { WeaTools } from 'ecCom';
export const url = '/api/formmode/page/'

const init = (params) =>{
    return WeaTools.callApi(`${url}init`, "get", params);
}

export default {
    init
}