import Docker from './docker';
import Tree from './tree';
import Search from './search';
export default {
    Docker,
    Tree,
    Search
}