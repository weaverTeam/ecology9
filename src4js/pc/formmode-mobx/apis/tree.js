
import {WeaTools} from 'ecCom';
export const url = '/api/formmode/page/'

const datas = (params) =>{
    return WeaTools.callApi(`${url}datas`, "get", params);
}

export default {
    datas
}