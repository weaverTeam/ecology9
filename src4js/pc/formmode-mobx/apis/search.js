import { WeaTools } from 'ecCom';
export const url = '/api/formmode/page/'

const datas = (params) => {
    return WeaTools.callApi(`${url}datas`, "get", params);
}
const search = (params) => {
    return WeaTools.callApi(`${url}search`, "get", params);
}

export default {
    datas,
    search
}