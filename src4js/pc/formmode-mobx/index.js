import 'babel-polyfill';
import React from 'react'
import ReactDOM from 'react-dom';

import { createHistory, useBasename, createHashHistory } from 'history'
import { Router, Route, useRouterHistory, Redirect } from 'react-router'

import { Provider } from 'mobx-react';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';

import FormmodeStore from './stores';
import Docker from './components/docker';
import './index.css';

const routingStore = new RouterStore();

const browserHistory = useRouterHistory(createHashHistory)({
    queryKey: '_key',
    basename: '/'
});

const history = syncHistoryWithStore(browserHistory, routingStore);
window.weaHistory = history;


let stores = {
    // Key can be whatever you want
    routing: routingStore,
    ...FormmodeStore
}
window.stores = stores;
class Root extends React.Component {
    render() {
        return (
            <Provider {...stores}>
                <Router history={history}>
                    <Route path="/">
                        <Route path="formmode" >
                            <Route path="docker" component={Docker.RouterDocker} />
                        </Route>
                    </Route>
                </Router>
            </Provider>
        )
    }
}
ReactDOM.render(<Root />, document.getElementById('root'));
