import { WeaTools } from 'ecCom'

export const getDatas = params => {
	return WeaTools.callApi('/', 'GET', params);
}


export const getUserDefInfoP = params => {
	return WeaTools.callApi('/api/meeting/base/getUserDefInfo', 'POST', params);
}

export const absentP = params => {
	return WeaTools.callApi('/api/meeting/statistics/absent', 'POST', params);
}

export const resolutP = params => {
	return WeaTools.callApi('/api/meeting/statistics/resolut', 'POST', params);
}
export const processdP = params => {
	return WeaTools.callApi('/api/meeting/statistics/process', 'POST', params);
}