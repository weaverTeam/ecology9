import { WeaTools } from 'ecCom'

export const getDatas = params => {
	return WeaTools.callApi('/api/meeting/decision/getlist ', 'post', params);//与后台通讯获取会议任务数据
}

export const getCondition = params => {
	return WeaTools.callApi('/api/meeting/decision/getCondition ', 'post', params);//与后台通讯获取查询条件
}

export const overCalendarItem = params => {
	return WeaTools.callApi('/api/meeting/decision/overCalendarItem ', 'post', params);//与后台通讯获取查询条件
}
