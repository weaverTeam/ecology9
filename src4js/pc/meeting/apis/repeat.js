import { WeaTools } from 'ecCom'

export const getRepeatDatas = params => {
    return WeaTools.callApi('/api/meeting/repeat/getData', 'post', params);//与后台通讯获取数据
}

export const getCondition = params => {
    return WeaTools.callApi('/api/meeting/repeat/getCondition', 'post', params);
}

export const StopIntvl = params => {
    return WeaTools.callApi('/api/meeting/repeat/stop', 'post', params);
}

export const ChgIntvl = params => {
    return WeaTools.callApi('/api/meeting/repeat/chg', 'post', params);
}

export const CancelIntvl = params => {
    return WeaTools.callApi('/api/meeting/repeat/cancel', 'post', params);
}

export const DeleteIntvl = params => {
    return WeaTools.callApi('/api/meeting/repeat/delete', 'post', params);
}