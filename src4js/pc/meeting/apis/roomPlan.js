import {WeaTools} from 'ecCom'

const roomPlan = {
    getRoomReportBaseData: (params) => WeaTools.callApi('/api/meeting/room/getRoomReportBaseData', 'POST', params),
    getRoomReportData: (params) => WeaTools.callApi('/api/meeting/room/getRoomReportData', 'POST', params),
    getRoomReportList: (params) => WeaTools.callApi('/api/meeting/room/getRoomReportList', 'POST', params),
    cancelMeeting: (params) => WeaTools.callApi('/api/meeting/base/cancelMeeting', 'POST', params),
    overMeeting: (params) => WeaTools.callApi('/api/meeting/base/overMeeting', 'POST', params)
};

export default roomPlan;