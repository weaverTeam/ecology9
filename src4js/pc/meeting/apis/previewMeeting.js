import { WeaTools } from 'ecCom'

export const getDatas = params => {
	return WeaTools.callApi('/', 'GET', params);
}

export const signMeetingByHandP = params => {
	return WeaTools.callApi('/api/meeting/sign/signMeetingByHand', 'POST', params);
}

export const signMeetingByHandOneP = params => {
	return WeaTools.callApi('/api/meeting/sign/signMeetingByHandOne', 'POST', params);
}

export const getViewMeetingFieldP = params => {
	return WeaTools.callApi('/api/meeting/field/getViewMeetingField', 'POST', params);
}

/*议程资料删除*/
export const deltopicdoc = (params) => WeaTools.callApi('/api/meeting/detail/deltopicdoc', 'POST', params);
/*议程资料保存*/
export const addtopicdoc = (params) => WeaTools.callApi('/api/meeting/detail/addtopicdoc', 'POST', params);
/*保存回执备注*/
export const saveRemark = (params) => WeaTools.callApi('/api/meeting/member/saveRemark', 'POST', params);
/*参会人员回执*/
export const meetingReHrm = (params) => WeaTools.callApi('/api/meeting/member/meetingReHrm', 'POST', params);
/*参会客户回执*/
export const meetingReCrm = (params) => WeaTools.callApi('/api/meeting/member/meetingReCrm', 'POST', params);
/*显示回执详情*/
export const showDetailData = (params) => WeaTools.callApi('/api/meeting/member/showDetailData', 'POST', params);
/*获得时间安排数据*/
export const getTopicDate = (params) => WeaTools.callApi('/api/meeting/detail/gettopicdate','POST', params);
/*保存时间安排数据*/
export const saveTopicDate = (params) => WeaTools.callApi('/api/meeting/detail/savetopicdate','POST', params);



export const doDeleteP = params => {
	return WeaTools.callApi('/api/meeting/base/deleteMeeting', 'POST', params);
}

export const reSubmit2P = params => {
	return WeaTools.callApi('/api/meeting/base/submitMeeting', 'POST', params);
}


export const cancelMeetingP = params => {
	return WeaTools.callApi('/api/meeting/base/cancelMeeting', 'POST', params);
}


export const overMeetingP = params => {
	return WeaTools.callApi('/api/meeting/base/overMeeting', 'POST', params);
}


export const copyP = params => {
	return WeaTools.callApi('/api/meeting/base/copyMeeting', 'POST', params);
}

export const onStopIntvlP = params => {
	return WeaTools.callApi('/api/meeting/repeat/stop', 'POST', params);
}

export const cancelIntvlP = params => {
	return WeaTools.callApi('/api/meeting/repeat/cancel', 'POST', params);
}

export const onChgIntvlP = params => {
	return WeaTools.callApi('/api/meeting/repeat/stop', 'POST', params);
}

export const getSignListP = params => {
	return WeaTools.callApi('/api/meeting/sign/getSignList', 'POST', params);
}



