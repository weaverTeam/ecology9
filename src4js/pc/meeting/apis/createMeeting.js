import { WeaTools } from 'ecCom'

export const getDatas = params => {
	return WeaTools.callApi('/', 'GET', params);
}

export const getCalendarListP = params => {
	return WeaTools.callApi('/api/meeting/calendar/getCalendarList', 'POST', params);
}

export const getNextMeetingP = params => {
	return WeaTools.callApi('/api/meeting/calendar/getNextMeeting', 'POST', params);
}

export const getDataP = params => {
	return WeaTools.callApi('/api/meeting/calendar/getData', 'POST', params);
}
export const getMeetingBaseFieldP = params => {
	return WeaTools.callApi('/api/meeting/field/getMeetingBaseField', 'POST', params);
}

export const getMeetingTopicFieldP = params => {
	return WeaTools.callApi('/api/meeting/field/getMeetingTopicField', 'POST', params);
}
export const getMeetingServiceFieldP = params => {
	return WeaTools.callApi('/api/meeting/field/getMeetingServiceField', 'POST', params);
}

export const changeBroserMeetingTypeP = params => {
	return WeaTools.callApi('/api/meeting/field/changeMeetingType', 'POST', params);
}

export const newMeetingP = params => {
	return WeaTools.callApi('/api/meeting/base/newMeeting', 'POST', params);
}

export const editMeetingP = params => {
	return WeaTools.callApi('/api/meeting/base/editMeeting', 'POST', params);
}

export const chkMeetingP = params => {
	return WeaTools.callApi('/api/meeting/base/chkMeeting', 'POST', params);
}
