import * as types from '../constants/ActionTypes'
import * as DecisionAPI from '../apis/decision'
import {WeaTable} from 'comsRedux'
const WeaTableAction = WeaTable.action;

export const doLoading = params => {
    return (dispatch, getState) => {
        dispatch({
            type: types.DECISION_LOADING,
            params
        });
        //调用API
        DecisionAPI.getDatas(params).then((data) => {
            dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
            dispatch({
                type: types.DECISION_LOADING,
                searchParamsAd: {...params },
                dataKey: data.sessionkey
            });
        });
    }
}

//初始化会议查询页查询条件
export const initDatas = params => {
    return(dispatch, getState) => {
        DecisionAPI.getCondition(params).then((data) => {
            const { groupinfo, pagetitle, conditioninfo } = data;
            dispatch({
                type:types.DECISION_CONDITION,
                conditioninfo: conditioninfo,
                topTab:data.groupinfo
            });
	});
    }
}

//高级搜索受控
export const setShowSearchAd = bool =>{
    return (dispatch, getState) => {
        dispatch({type: types.SET_SHOW_SEARCHAD_DECISION,value:bool})
    }
}

//高级搜索表单内容
export const saveFields = (value = {}) => {
    return (dispatch, getState) => {
        dispatch({type: types.SAVE_FIELDS,value:value})
    }
}

//任务完成
export const Done = (params,searchParamsAd) => {
    return (dispatch, getState) => {
        //调用API
        DecisionAPI.overCalendarItem(params).then((result)=>{
                dispatch(doLoading(searchParamsAd));
            });
    }
}


export const savePreviewMeetingId = (value={}) => {
    return (dispatch, getState) => {
        dispatch({type: types.DECISION_SAVEPREVIEWMEETINGID, data:value.meetingid});
    }
}
