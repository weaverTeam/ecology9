import * as types from '../constants/ActionTypes'
import Apis from '../apis/roomPlan';

export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.ROOMPLAN_LOADING,
			loading
		});
	}
}

export const setOrg = (org)=> {
    return {
        type: types.ROOMPLAN_SETORG,
        org: org
    };
};