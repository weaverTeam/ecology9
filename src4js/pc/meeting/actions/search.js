import * as types from '../constants/ActionTypes'
import * as SearchAPI from '../apis/search'
import {WeaTable} from 'comsRedux'
const WeaTableAction = WeaTable.action;

export const doLoading = params => {
    return (dispatch, getState) => {
        dispatch({
            type: types.SEARCH_LOADING,
            params
        });
        //调用API
        SearchAPI.getDatas(params).then((data) => {
            dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
            dispatch({
                type: types.SEARCH_LOADING,
                searchParamsAd: {...params },
                dataKey: data.sessionkey
            });
        });
    }
}

//初始化会议查询页查询条件
export const initDatas = params => {
    return(dispatch, getState) => {
        SearchAPI.getCondition(params).then((data) => {
            const { groupinfo, pagetitle, conditioninfo } = data;
            dispatch({
                type:types.SEARCH_CONDITION,
                conditioninfo: conditioninfo,
                topTab:data.groupinfo
            });
        });
    }
}

//高级搜索受控
export const setShowSearchAd = bool =>{
    return (dispatch, getState) => {
        dispatch({type: types.SET_SHOW_SEARCHAD_SEARCH,value:bool})
    }
}

//高级搜索表单内容
export const saveFields = (value = {}) => {
    return (dispatch, getState) => {
        dispatch({type: types.SAVE_FIELDS_SEARCH,value:value})
    }
}


export const savePreviewMeetingId = (value={}) => {
    return (dispatch, getState) => {
        dispatch({type: types.SEARCH_SAVEPREVIEWMEETINGID, data:value.meetingid});
    }
}