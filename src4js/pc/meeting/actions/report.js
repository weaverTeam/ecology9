import * as types from '../constants/ActionTypes'
import * as API_LIST from '../apis/report'
import isEmpty from 'lodash/isEmpty';

export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.REPORT_LOADING,
			loading
		});
	}
}

//搜索内容
export const setSearchInfo = (value = {},type) => {
	return(dispatch, getState) => {
		if(type == 'time'){
			dispatch({type:types.REPORT_SETSEARCH_TIME, data: value })
		}
		if(type == 'type'){
			dispatch({type:types.REPORT_SETSEARCH_TYPE, data: value })
		}
	}
}

//获取人员的所在部门和分部
export const getUserDefInfo = (value = {}) => {
	return (dispatch, getState) => {
		isEmpty(value) &&
		API_LIST.getUserDefInfoP(value).then(data =>{
			dispatch({type:types.REPORT_GETUSER_INFO, data: data});
		});
		!isEmpty(value) && 
			dispatch({type:types.REPORT_GETUSER_INFO, data: value});
	}
}

//e9会议报表-会议缺席统计
export const absent = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.absentP(value).then(data =>{
			dispatch({type:types.REPORT_ABSENT, data: data});
		});
	}
}


//e9会议报表-会议缺席统计
export const resolut = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.resolutP(value).then(data =>{
			dispatch({type:types.REPORT_RESOLUT, data: data});
		});
	}
}


//e9会议报表-会议任务统计
export const processd = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.processdP(value).then(data =>{
			dispatch({type:types.REPORT_PROCESSD, data: data});
		});
	}
}


//会议报表里的tab
export const changeReportTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.REPORT_CHANGEREPORTTAB,data:value
		});
	}
}