import * as types from '../constants/ActionTypes'
import * as RepeatAPI from '../apis/repeat'
import {WeaTable} from 'comsRedux'
const WeaTableAction = WeaTable.action;

export const doLoading = params => {
    return (dispatch, getState) => {
        dispatch({
            type: types.SEARCH_LOADING,
            params
        });
        //调用API
        RepeatAPI.getRepeatDatas(params).then((data) => {
            dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
            dispatch({
                type: types.SEARCH_LOADING,
                searchParamsAd: {...params },
                dataKey: data.sessionkey
            });
        });
    }
}

//初始化会议查询页查询条件
export const initDatas = params => {
    return(dispatch, getState) => {
        RepeatAPI.getCondition(params).then((data) => {
            const { groupinfo, pagetitle, conditioninfo } = data;
            dispatch({
                type:types.SEARCH_CONDITION,
                conditioninfo: conditioninfo,
                topTab:data.groupinfo
            });
        });
    }
}

//高级搜索受控
export const setShowSearchAd = bool =>{
    return (dispatch, getState) => {
        dispatch({type: types.SET_SHOW_SEARCHAD_REPEAT,value:bool})
    }
}

//高级搜索表单内容
export const saveFields = (value = {}) => {
    return (dispatch, getState) => {
        dispatch({type: types.SAVE_FIELDS_REPEAT,value:value})
    }
}

//立即终止周期会议
export const StopIntvl = (meetingid,searchParamsAd) => {
    return (dispatch, getState) => {
        //调用API
        RepeatAPI.StopIntvl(meetingid).then((result)=>{
                dispatch(doLoading(searchParamsAd));
            });
    }
}
//提前结束周期会议
export const ChgIntvl = (params,searchParamsAd) => {
    return (dispatch, getState) => {
        //调用API
        RepeatAPI.ChgIntvl(params).then((result)=>{
                dispatch(doLoading(searchParamsAd));
            });
    }
}
//取消周期会议
export const CancelIntvl = (meetingid,searchParamsAd) => {
    return (dispatch, getState) => {
        //调用API
        RepeatAPI.CancelIntvl(meetingid).then((result)=>{
                dispatch(doLoading(searchParamsAd));
            });
    }
}
//删除周期会议
export const DeleteIntvl = params => {
    return (dispatch, getState) => {
        //调用API
        RepeatAPI.DeleteIntvl(params);
    }
}



//提前结束modal受控
export const advanceModal = (value = {},params = {}) => {
    return (dispatch, getState) => {
        dispatch({
            type: types.REPEAT_ADVANCEMODAL,data:value
        });
        params && 
        dispatch({
            type: types.REPEAT_ADVANCEMODALPARAMS,data:params
        });
    }
}

//新建会议显隐
export const controlModal = (value,date='') => {
    return (dispatch, getState) => {
        dispatch({type: types.REPEAT_CREATEDATE, data:date});
        dispatch({type: types.REPEAT_CONTROLMODAL, data:value});
        !value && RepeatAPI.getRepeatDatas(value).then(data =>{
            dispatch(WeaTableAction.getDatas(data.sessionkey, 1));
            dispatch({type:types.REPEAT_GETREPEATDATAS, data: data.sessionkey});
        });
    }
}

export const savePreviewMeetingId = (value={}) => {
    return (dispatch, getState) => {
        dispatch({type: types.REPEAT_SAVEPREVIEWMEETINGID, data:value.meetingid});
    }
}
