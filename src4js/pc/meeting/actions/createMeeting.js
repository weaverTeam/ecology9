import * as types from '../constants/ActionTypes'
import * as API_LIST from '../apis/createmeeting'
import { WeaTable } from 'comsRedux'
import objectAssign from 'object-assign'
// const WeaTableAction = WeaTable.action;
import * as PreviewMeetingAction from './previewMeeting'

export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CREATEMEETING_LOADING,
			loading
		});
	}
}



//切换新建会议里的tab
export const changeNewMeetingTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CREATE_CHANGENEWMEETINGTAB,data:{
				newMeetingTab:value,
				errorMessage:'',
			}
		});
	}
}




//新建会议基本信息

export const getMeetingBaseField = value => {
	return (dispatch, getState) => {
		API_LIST.getMeetingBaseFieldP(value).then(data =>{
			dispatch({type:types.CREATE_GETMEETINGBASEFIELD, data: data});//请求数据，e9会议-新建会议页面显示内容
		});
	}
}
export const controlDatas = (value="") => {
	return (dispatch, getState) => {
			dispatch({type:types.CREATE_GETMEETINGBASEFIELD, data: value});//清空
	}
}





//查看会议
export const previewModal = value => {
	return (dispatch, getState) => {
		value &&
		API_LIST.getMeetingBaseFieldP(value).then(data =>{
			dispatch({type:types.CALVIEW_GETMEETINGBASEFIELD, data: data});//请求数据，e9会议-新建会议页面显示内容
			dispatch({type: types.CALVIEW_PREVIEWMODAL, data:true});
		});
		!value &&
		dispatch({type: types.CALVIEW_PREVIEWMODAL, data:value});
	}
}



//表单内容存储到store
export const saveFields = (value = {}) => {
	return(dispatch, getState) => {
		dispatch({type:types.CREATE_SAVE_FIELDS, data: value })
	}
}

//新建会议-议程
export const getMeetingTopicField = value => {
	return (dispatch, getState) => {
		(!value||!!value.meetingid) &&
		API_LIST.getMeetingTopicFieldP(value).then(data =>{
			let count = 100/data.columns.length;
            data.columns = data.columns.map((x) => {
               x['width']=`${count}%`
               return x;
            });
			// dispatch({type:types.CREATE_GETMEETINGTOPICFIELDC, data: data.columns||-1});//请求数据，列
			// dispatch({type:types.CREATE_GETMEETINGTOPICFIELDD, data: data.datas||-1});//请求数据，数据
			dispatch({type:types.CREATE_GETMEETINGTOPICFIELDC,
			 		  data:{topicColumns: data.columns||-1,
			 		  	    topicDatas: data.datas||-1
			 		  }
			 });//请求数据，列
		});
		if(typeof(value)==='object') {
			dispatch({type:types.CREATE_GETMEETINGTOPICFIELDD, data: value});//未请求数据，拿edittable的数据
		}
	}
}

//新建会议-服务
export const getMeetingServiceField = value => {
	return (dispatch, getState) => {
		(!value||!!value.meetingid) &&
		API_LIST.getMeetingServiceFieldP(value).then(data =>{
            let count = 100/data.columns.length;
            data.columns = data.columns.map((x) => {
               x['width']=`${count}%`
               return x;
            });
			dispatch({type:types.CREATE_GETMEETINGSERVICEFIELDC,
			 		  data:{serviceColumns: data.columns||-1,
			 		  	    serviceDatas: data.datas||-1,
			 		  	    serviceKeycol:data.keycol ||-1,
			 		  }
			 });//请求数据，列
			// dispatch({type:types.CREATE_GETMEETINGSERVICEFIELDD, data: data.datas||-1});//请求数据，数据
		});
		if(typeof(value)==='object') {
			dispatch({type:types.CREATE_GETMEETINGSERVICEFIELDD, data: value});//未请求数据，拿edittable的数据
		}
	}
}

//e9会议-会议类型切换获取默认召集人
export const changeBroserMeetingType = value => {
	return (dispatch, getState) => {
		API_LIST.changeBroserMeetingTypeP(value).then(data =>{
			let arr =[];
			arr.push(data);
			dispatch(saveFields({...value.oldFields,caller:{rep:arr,value:data.id,valueSpan:data.name}}));
			dispatch({type:types.CREATE_CHANGEBROSERMEETINGTYPE, data: data});
		});
	}
}

//保存新建的会议
export const newMeeting = (value,type) => {
	return (dispatch, getState) => {
		API_LIST.newMeetingP(value).then(data =>{
			data.meetingid != ''?
			dispatch({type:types.CREATE_NEWMEETING, data:{
				meetingid:data,
				type:type,
				newmeetingTab:1,
				errorMessage:data.error || ''
			}}):'';//保存成功返回值
			// data.error &&  dispatch({type:types.CREATE_SAVEERRORMESSAGE, data: data.error})
		});
	}
}


//保存编辑的会议
export const editMeeting = (value,type) => {
	return (dispatch, getState) => {
		API_LIST.editMeetingP(value).then(data =>{
			data.meetingid != ''?
			dispatch(PreviewMeetingAction.getViewMeetingField({meetingid:data.meetingid}))//编辑提交完刷新页面
			// dispatch({type:types.CREATE_NEWMEETING, data:{
			// 	meetingid:data,
			// 	type:type,
			:'';//保存成功返回值
		});
	}
}

