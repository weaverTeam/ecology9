import * as types from '../constants/ActionTypes'
import * as API_LIST from '../apis/calView'
import { WeaTable } from 'comsRedux'
import objectAssign from 'object-assign'
const WeaTableAction = WeaTable.action;


export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CALVIEW_LOADING,
			loading
		});
	}
}

//获取table
export const getCalendarList = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({ type:types.CALVIEW_LOADING, loading: true });
		API_LIST.getCalendarListP(value).then(data =>{
			dispatch(WeaTableAction.getDatas(data.sessionkey, 1));
			dispatch({type:types.CALVIEW_GETCALENDARLIST, data: data.sessionkey});
			dispatch({ type: types.CALVIEW_LOADING, loading: false });
		});
	}
}

//切换模式
export const changeType = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CALVIEW_CHANGETYPE,data:value
		});
	}
}
//切换新建会议里的tab
export const changeNewMeetingTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CALVIEW_CHANGENEWMEETINGTAB,data:value
		});
	}
}


//切换会议状态
export const changeMeetingType = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CALVIEW_CHANGEMEETINGTYPE,data:value
		});
	}
}

//e9会议日历-获取下一个要参加的会议
export const getNextMeeting = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getNextMeetingP(value).then(data =>{
			dispatch({type:types.CALVIEW_GETNEXTMEETING, data: data});
		});
	}
}
//e9会议日历-会议日历数据获取
export const getData = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getDataP(value).then(data =>{
			dispatch({type:types.CALVIEW_GETDATA, data: data});
		});
	}
}


//切换月份
export const changeMonth = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CALVIEW_CHANGEMONTH,data:value
		});
	}
}

//保存日期
export const saveDate = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.CALVIEW_SAVEDATE,data:value
		});
	}
}

//新建会议显隐
export const controlModal = (value,previewMeetingId = '',date = new Date()) => {
	return (dispatch, getState) => {
		// value &&
		// API_LIST.getMeetingBaseFieldP().then(data =>{
			// dispatch({type:types.CALVIEW_GETMEETINGBASEFIELD, data: data});//请求数据，e9会议-新建会议页面显示内容
			// dispatch({type: types.CALVIEW_CONTROLMODAL, data:value});
		// });
		// !value &&
		dispatch({type: types.CALVIEW_CREATEDATE, data:date});
		dispatch({type: types.CALVIEW_CONTROLMODAL, data:{
			type:previewMeetingId?'edit':'create',
			previewMeetingId:previewMeetingId,
			control:value,
		}});
	}
}


//查看会议(废弃)
// export const previewModal = value => {
// 	return (dispatch, getState) => {
// 		value &&
// 		API_LIST.getMeetingBaseFieldP(value).then(data =>{
// 			dispatch({type:types.CALVIEW_GETMEETINGBASEFIELD, data: data});//请求数据，e9会议-新建会议页面显示内容
// 			dispatch({type: types.CALVIEW_PREVIEWMODAL, data:true});
// 		});
// 		!value &&
// 		dispatch({type: types.CALVIEW_PREVIEWMODAL, data:value});
// 	}
// }

// //查看会议(合并接口) 转到preview
// export const getViewMeetingField = value => {
// 	return (dispatch, getState) => {
// 		value &&
// 		API_LIST.getViewMeetingFieldP(value).then(data =>{
// 			dispatch({type:types.CALVIEW_VIEWMEETINGFIELD, data:{
// 				previewModal:true,
// 				previewMeetingId:value.meetingid,
// 				tabbase:data.base,
// 				tabtopic:data.topic,
// 				tabservice:data.service,
// 				tabmember:data.member,
// 				tabsign:data.sign,
// 				tabdecision:data.decision,
// 				tabdiscuss:data.discuss,
// 			}});//查看会议页面，所有tab页内容
// 			// dispatch({type: types.CALVIEW_PREVIEWMODAL, data:true});
// 		});
// 		!value &&
// 		dispatch({type: types.CALVIEW_PREVIEWMODAL, data:value});
// 	}
// }


//表单内容存储到store
export const saveFields = (value = {}) => {
	return(dispatch, getState) => {
		dispatch({type:types.CALVIEW_SAVE_FIELDS, data: value })
	}
}

//查看会议-议程(废弃)
// export const getMeetingTopicField = value => {
// 	return (dispatch, getState) => {
// 		(!value||!!value.meetingid) &&
// 		API_LIST.getMeetingTopicFieldP(value).then(data =>{
// 			let count = 100/data.columns.length;
//             data.columns = data.columns.map((x) => {
//                x['width']=`${count}%`
//                return x;
//             });
// 			dispatch({type:types.CALVIEW_GETMEETINGTOPICFIELDC, data: data.columns||-1});//请求数据，列
// 			dispatch({type:types.CALVIEW_GETMEETINGTOPICFIELDD, data: data.datas||-1});//请求数据，数据
// 		});
// 		if(typeof(value)==='object') {
// 			dispatch({type:types.CALVIEW_GETMEETINGTOPICFIELDD, data: value});//未请求数据，拿edittable的数据
// 		}
// 	}
// }

//查看会议-服务（废弃）
// export const getMeetingServiceField = value => {
// 	return (dispatch, getState) => {
// 		(!value||!!value.meetingid) &&
// 		API_LIST.getMeetingServiceFieldP(value).then(data =>{
//             let count = 100/data.columns.length;
//             data.columns = data.columns.map((x) => {
//                x['width']=`${count}%`
//                return x;
//             });
// 			dispatch({type:types.CALVIEW_GETMEETINGSERVICEFIELDC, data: data.columns||-1});//请求数据，列
// 			dispatch({type:types.CALVIEW_GETMEETINGSERVICEFIELDD, data: data.datas||-1});//请求数据，数据
// 		});
// 		if(typeof(value)==='object') {
// 			dispatch({type:types.CALVIEW_GETMEETINGSERVICEFIELDD, data: value});//未请求数据，拿edittable的数据
// 		}
// 	}
// }

//e9会议-会议类型切换获取默认召集人
export const changeBroserMeetingType = value => {
	return (dispatch, getState) => {
		API_LIST.changeBroserMeetingTypeP(value).then(data =>{

			dispatch(saveFields({...value.oldFields,caller:{name:data.name,value:data.id}}));
			dispatch({type:types.CALVIEW_CHANGEBROSERMEETINGTYPE, data: data});
		});
	}
}

//保存新建的会议
export const newMeeting = value => {
	return (dispatch, getState) => {
		API_LIST.newMeetingP(value).then(data =>{
			data.meetingid != ''?
			dispatch(getData({
				selectUser:'',
				selectdate:value.begindate,
				meetingType:0,
			})):''
			dispatch({type:types.CALVIEW_NEWMEETING, data: data});//保存成功返回值
		});
	}
}

// //查看会议-签到-table
// export const getTabSignTable = (value = {}) => {
// 	return (dispatch, getState) => {
// 		dispatch({ type:types.CALVIEW_LOADING, loading: true });
// 		dispatch(WeaTableAction.getDatas(value.sessionkey, 1));
// 		dispatch({type:types.CALVIEW_GETTABSIGNTABLE, data: value.sessionkey});
// 		dispatch({ type: types.CALVIEW_LOADING, loading: false });
// 	}
// }


// //切换查看会议里的tab
// export const changeTabSignTab = value => {
// 	return (dispatch, getState) => {
// 		dispatch({
// 			type: types.CALVIEW_CHANGETABSIGNTAB,data:value
// 		});
// 	}
// }

export const savePreviewMeetingId = (value={}) => {
	return (dispatch, getState) => {
		dispatch({type: types.CALVIEW_SAVEPREVIEWMEETINGID, data:value.meetingid});
	}
}
