import * as types from '../constants/ActionTypes'
import * as API_LIST from '../apis/previewMeeting'
import { WeaTable } from 'comsRedux'
import objectAssign from 'object-assign'
const WeaTableAction = WeaTable.action;
import * as CalViewAction from './calView'
import * as tool from '../components/calview/genericmethod/index'



export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.PREVIEW_LOADING,
			loading
		});
	}
}

//查看会议(合并接口)
export const getViewMeetingField = value => {
	return (dispatch, getState) => {
		value &&
		API_LIST.getViewMeetingFieldP(value).then(data =>{
			dispatch({type:types.PREVIEW_VIEWMEETINGFIELD, data:{
				previewModal:true,
				previewMeetingId:value.meetingid,
				tabbase:data.base,
				tabtopic:data.topic,
				tabservice:data.service,
				tabmember:data.member,
				tabsign:data.sign,
				tabdecision:data.decision,
				tabdiscuss:data.discuss,
				tabs:data.tabs,
				previewmeetingTab:1,
			}});//查看会议页面，所有tab页内容
			// dispatch({type: types.CALVIEW_PREVIEWMODAL, data:true});
		});
		!value &&
		dispatch({type:types.PREVIEW_VIEWMEETINGFIELD, data:{
				previewModal:value,
				previewMeetingId:'',
				tabbase:{},
				tabtopic:'',
				tabservice:'',
				tabmember:'',
				tabsign:'',
				tabdecision:'',
				tabdiscuss:'',
				previewmeetingTab:1,
			}});//查看会议页面，所有tab页内容
	}
}
//查看会议modal显隐受控
export const previewModal = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.PREVIEW_PREVIEWMODAL,data:value
		});
	}
}

//查看会议-签到-table
export const getTabSignTable = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({ type:types.PREVIEW_LOADING, loading: true });
		dispatch(WeaTableAction.getDatas(value.sessionkey, 1));
		dispatch({type:types.PREVIEW_GETTABSIGNTABLE, data: value.sessionkey});
		dispatch({ type: types.PREVIEW_LOADING, loading: false });
	}
}


//切换查看会议里的tab
export const changeTabSignTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.PREVIEW_CHANGETABSIGNTAB,data:value
		});
	}
}



//查看会议-会议签到-添加会议签到记录
export const signMeetingByHand = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.signMeetingByHandP(value).then(data =>{
			if(data == 1){
				dispatch({type:types.PREVIEW_SIGNMEETINGBYHAND, data: false});
				// signMeetingByHand
				API_LIST.getSignListP({meetingid:value.meetingid}).then((e)=>{
					dispatch(WeaTableAction.getDatas(e.sessionkey, 1));
					dispatch({type:types.PREVIEW_GETTABSIGNTABLE, data: e.sessionkey});
				})
			}
		});
	}
}


export const signMeetingByHandOne = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.signMeetingByHandOneP(value).then(data =>{
			//判断下data
			API_LIST.getSignListP({meetingid:value.meetingid}).then((e)=>{
					dispatch(WeaTableAction.getDatas(e.sessionkey, 1));
					dispatch({type:types.PREVIEW_GETTABSIGNTABLE, data: e.sessionkey});
				})
		});
	}
}

//查看会议-签到-table
export const getTabDecisionTable = (value = "") => {
	return (dispatch, getState) => {
		dispatch(WeaTableAction.getDatas(value, 1));
		dispatch({type:types.PREVIEW_GETTABDECISIONTABLE, data: value});
	}
}

export const delTopicDoc = (params, dataIndex) => {
	return (dispatch, state) => {
		API_LIST.deltopicdoc(params).then((result) => {
			dispatch({
			    type: types.PREVIEW_MEETTOPICDELDOC,
                params: params,
                dataIndex: dataIndex
            })
		});
	}
}

export const addTopicDoc = (params, dataRecord) => {
    return (dispatch, state) => {
        API_LIST.addtopicdoc(params).then((result) => {
            dispatch({
                type: types.PREVIEW_MEETTOPICADDDOC,
                data: result,
                dataRecord: dataRecord
            })
        });
    }
}

export const setNewRemark = (remark) => {
	return {
	    type: types.PREVIEW_MEMBER_SETNEWREMARK,
        remark: remark
    }
}

/**
 * 设置新回执
 */
export const setNewReply = (showValue, formValue, type) => {
	return {
        type: types.PREVIEW_MEMBER_SETNEWREPLY,
        showValue: showValue,
        values: formValue,
        tableType: type
    }
};

export const setTopicNewTime = (meetingId, topicId, reshowArray) => {
	return {
        type: types.PREVIEW_MEMBER_SETTOPICNEWTIME,
        meetingId,topicId,reshowArray
	};
}
//切换新建会议里的tab
export const changePreviewMeetingTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.PREVIEW_CHANGEPREVIEWMEETINGTAB,data:value
		});
	}
}

//删除
export const doDelete = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.doDeleteP(value).then(data =>{
			dispatch({//关闭页面
				type: types.PREVIEW_PREVIEWMODAL,data:false
			});
			if(value.type == 1){
                dispatch(CalViewAction.getData({
                    selectUser:'',
                    selectdate:value.date || tool.changeDateFormat(new Date()),
                    meetingType:value.meetingType,
                }));
            }else{
                let params = objectAssign({},{
                    selectUser:'',
                    selectdate:value.date,
                    meetingType:value.meetingType,//会议类型
                })
                dispatch(CalViewAction.getCalendarList(params));
            }
		});
	}
}

//提交2
export const reSubmit2 = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.reSubmit2P(value).then(data =>{
			dispatch({//关闭页面
				type: types.PREVIEW_PREVIEWMODAL,data:false
			});
			if(value.type == 1){
                dispatch(CalViewAction.getData({
                    selectUser:'',
                    selectdate:value.date || tool.changeDateFormat(new Date()),
                    meetingType:value.meetingType,
                }));
            }else{
                let params = objectAssign({},{
                    selectUser:'',
                    selectdate:value.date,
                    meetingType:value.meetingType,//会议类型
                })
                dispatch(CalViewAction.getCalendarList(params));
            }
		});
	}
}
//取消会议
export const cancelMeeting = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.cancelMeetingP(value).then(data =>{
			if(data.status == true){
				dispatch({//关闭页面
					type: types.PREVIEW_PREVIEWMODAL,data:false
				});
				if(value.type == 1){
	                dispatch(CalViewAction.getData({
	                    selectUser:'',
	                    selectdate:value.date || tool.changeDateFormat(new Date()),
	                    meetingType:value.meetingType,
	                }));
	            }else{
	                let params = objectAssign({},{
	                    selectUser:'',
	                    selectdate:value.date,
	                    meetingType:value.meetingType,//会议类型
	                })
	                dispatch(CalViewAction.getCalendarList(params));
	            }
			}

		});
	}
}

//结束会议
export const overMeeting = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.overMeetingP(value).then(data =>{
		});
	}
}

//复制会议
export const copy = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.copyP(value).then(data =>{
			if(data.status == true){
				// dispatch({//关闭页面
				// 	type: types.PREVIEW_PREVIEWMODAL,data:data.meetingid
				// });
				if(value.type == 1){
	                dispatch(CalViewAction.getData({
	                    selectUser:'',
	                    selectdate:value.date || tool.changeDateFormat(new Date()),
	                    meetingType:value.meetingType,
	                }));
	            }else{
	                let params = objectAssign({},{
	                    selectUser:'',
	                    selectdate:value.date,
	                    meetingType:value.meetingType,//会议类型
	                })
	                dispatch(CalViewAction.getCalendarList(params));
	            }
				API_LIST.getViewMeetingFieldP({meetingid:data.meetingid}).then(datas =>{

					dispatch({type:types.PREVIEW_VIEWMEETINGFIELD, data:{
						previewModal:true,
						previewMeetingId:value.meetingid,
						tabbase:datas.base,
						tabtopic:datas.topic,
						tabservice:datas.service,
						tabmember:datas.member,
						tabsign:datas.sign,
						tabdecision:datas.decision,
						tabdiscuss:datas.discuss,
						tabs:datas.tabs,
						previewmeetingTab:1,
					}});//查看会议页面，所有tab页内容
					// dispatch({type: types.CALVIEW_PREVIEWMODAL, data:true});
				});
				
			}

		});
	}
}


//停止重复会议
export const onStopIntvl = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.onStopIntvlP(value).then(data =>{
			// dispatch({type:types.PREVIEW_SIGNMEETINGBYHAND, data: data});
		});
	}
}

//取消周期会议
export const cancelIntvl = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.cancelIntvlP(value).then(data =>{
			// dispatch({type:types.PREVIEW_SIGNMEETINGBYHAND, data: data});
		});
	}
}


//提前结束重复会议
export const getSignList = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getSignListP({meetingid:value.meetingid}).then((e)=>{
			dispatch(WeaTableAction.getDatas(e.sessionkey, 1));
			dispatch({type:types.PREVIEW_GETTABSIGNTABLE, data: e.sessionkey});
		})
	}
}
