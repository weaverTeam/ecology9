import {Row,Col,Icon,DatePicker} from 'antd';
import { WeaBrowser,WeaDatePicker} from 'ecCom';
import isEmpty from 'lodash/isEmpty';
const RangePicker = DatePicker.RangePicker;

class Main extends React.Component {
	constructor(props) { 
		super(props);
		this.state = {
			hasBrowse:'',
			hasDatePicker:false,
		}
	}
	componentDidMount() {
		//一些初始化请求
		const {hasBrowse = '',replaceDatas = ''} = this.props;
		if(replaceDatas != '' && !isEmpty(hasBrowse) ){
			let replace = hasBrowse.type == "164" ? replaceDatas.defsub : replaceDatas.defdep;//164为分部默认值
			hasBrowse.replaceDatas = [];
			hasBrowse.replaceDatas.push(replace);
			this.setState({
				hasBrowse:hasBrowse
			})
		}
	}
	componentWillReceiveProps(nextProps) {
		let {hasBrowse = {},replaceDatas = ''} = nextProps;
		if(isEmpty(hasBrowse)){
			this.setState({
				hasBrowse:''
			})
		}
		if(!isEmpty(replaceDatas) && !isEmpty(hasBrowse) ){
			let replace = hasBrowse.type == "164" ? replaceDatas.defsub : replaceDatas.defdep;//164为分部默认值
			hasBrowse.replaceDatas = [];
			hasBrowse.replaceDatas.push(replace);
			this.setState({
				hasBrowse:hasBrowse
			})
		}
		if(this.props.hasDatePicker != nextProps.hasDatePicker){
			this.setState({
				hasDatePicker:nextProps.hasDatePicker
			})
		}
	}
	render() {
		let that = this;
		let {hasBrowse,hasDatePicker } = this.state;
		return (
			<div style={{borderBottom:'1px solid #e9e9e9',background:"#fff",height:"40px"}} >
				<div style={{paddingLeft:'16px',display:'inline-block',width:'120px',fontWeight: "bold",color: "#333333",lineHeight:'40px',height:'39px',backgroundColor:'#F4F4F4'}}>
					{this.props.label}
				</div>
				<div style={{borderBottom:'1px solid #e9e9e9',display:'inline-block',lineHeight: "40px", background: "#fff", position: 'relative',height:'40px'}} className={"SearchButton"}>	
					{
						this.props.datas.map(
							function(data) {
								const on = that.props.selItem == data.id;
								return <a className={on ? "on" : ""} onClick={that.props.setData.bind(that,data)}>
											{ data.name }
									   </a>
							}
						)
			   		}
			   		{
			   			hasBrowse &&
			   			<div style={{display:'inline-block',width:'300px'}}>
			   				<WeaBrowser {...hasBrowse} resize inputStyle={{width:'200px;'}} onChange = {that.props.browseChange.bind(that,hasBrowse.type)} />
			   			</div>
			   		}
			   		{
			   			hasDatePicker &&
			   			<RangePicker style={{ width: 184 }} onChange = {that.props.dateChange.bind(that)} />
			   		}
				</div>	
		   </div>
		)
	}
};

export default Main;