export const changeDateFormat = data => {

	//日期格式转为yyyy-MM-dd
	let now = data || new Date();
    let year = now.getFullYear();
    let month =(now.getMonth() + 1).toString();
    let day = (now.getDate()).toString();
    if (month.length == 1) {
        month = "0" + month;
    }
    if (day.length == 1) {
        day = "0" + day;
    }
    let dateString =  year + "-" + month + "-" + day;
    return dateString;
}