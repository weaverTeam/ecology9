import {Row,Col,Switch,Tag,DatePicker,Tabs} from 'antd';
const MonthPicker = DatePicker.MonthPicker;
const TabPane = Tabs.TabPane;
import {WeaTab,WeaBrowser} from 'ecCom';
import { WeaOrgTree } from 'ecCom';


export default class RightContainer extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            color:'',
            // checked: props.checked ? props.checked : false,

        }
    }
    componentWillReceiveProps(nextProps) {
        // if (this.props.checked !== nextProps.checked) {
        //     this.setState({checked: nextProps.checked});
        // }
    }

	render(){
        const {style,all} = this.props;
		const {actions,meetingType,nextMeeting} = all;
        let caller = [];
        nextMeeting && nextMeeting.caller && caller.push({
            "id": nextMeeting.caller.id,
            "name": nextMeeting.caller.name
        })
		const {color} = this.state;
		return(
                <div className="rightC" style={{width:'221px',position:'absolute',height:'100%',borderLeft:'1px solid #eaeaea'}}>
                     <Tabs defaultActiveKey="0" onChange={this.onChange}>
                        <TabPane tab="会议状态" key="0">

                        
                            <Row style={{backgroundColor:meetingType==0?'#d9f4ff':(color==0?'#edfaff':''),height:'34px',lineHeight:'34px'}} onMouseEnter={()=>{this.setState({color:0})}} onMouseOut={()=>{this.setState({color:5})}} onClick={()=>{actions.changeMeetingType(0)}}>
                                <i className="icon-Meeting-all" style ={{color:'#00bafb',marginLeft:'15px',fontSize:'14px'}} />
                                <span style={{marginLeft:'10px'}}>所有</span>
                                {meetingType==0?
                                <i className="icon-Meeting-Selected" style={{color:'#2db7f5',fontSize:'16px',marginLeft:'112px'}}  />
                                :''}
                            </Row>
                            <Row style={{backgroundColor:meetingType==1?'#d9f4ff':(color==1?'#edfaff':''),height:'34px',lineHeight:'34px'}} onMouseEnter={()=>{this.setState({color:1})}} onMouseOut={()=>{this.setState({color:5})}} onClick={()=>{actions.changeMeetingType(1)}}>
                                <i className="icon-Meeting-Is-over" style ={{color:'#bdbdbd',marginLeft:'15px',fontSize:'14px'}} />

                                <span style={{marginLeft:'10px'}}>已结束</span>
                                {meetingType==1?
                                <i className="icon-Meeting-Selected" style={{color:'#2db7f5',fontSize:'16px',marginLeft:'100px'}}  />
                                :''}
                            </Row>
                            <Row style={{backgroundColor:meetingType==2?'#d9f4ff':(color==2?'#edfaff':''),height:'34px',lineHeight:'34px'}} onMouseEnter={()=>{this.setState({color:2})}} onMouseOut={()=>{this.setState({color:5})}} onClick={()=>{actions.changeMeetingType(2)}}>
                                <i className="icon-Meeting-In" style ={{color:'#ff9c00',marginLeft:'15px',fontSize:'14px'}} />

                                <span style={{marginLeft:'10px'}}>进行中</span>
                                {meetingType==2?
                                <i className="icon-Meeting-Selected" style={{color:'#2db7f5',fontSize:'16px',marginLeft:'100px'}}  />
                                :''}
                            </Row>
                            <Row style={{backgroundColor:meetingType==3?'#d9f4ff':(color==3?'#edfaff':''),height:'34px',lineHeight:'34px',marginBottom:'18px'}} onMouseEnter={()=>{this.setState({color:3})}} onMouseOut={()=>{this.setState({color:5})}} onClick={()=>{actions.changeMeetingType(3)}}>
                                <i className="icon-Meeting-Not-begin" style ={{color:'#f34555',marginLeft:'15px',fontSize:'14px'}} />

                                <span style={{marginLeft:'10px'}}>未开始</span>
                                {meetingType==3?
                                <i className="icon-Meeting-Selected" style={{color:'#2db7f5',fontSize:'16px',marginLeft:'100px'}}  />
                                :''}
                            </Row>
                            {nextMeeting && nextMeeting.nodata == "没有需要参加的会议" ?
                            <Row style={{borderTop:'1px solid #eaeaea',marginTop:'30px',marginLeft:'15px',marginRight:'15px'}}>
                                <Row style={{marginTop:'150px'}}>
                                <span style={{backgroundImage:'url(/cloudstore/images/e9/noNextMeeting.png)',backgroundRepeat:'no-repeat',display:'inline-block',height:'100px',width:'200px'}}></span>
                                <span style={{textAlign:'center',display:'inline-block',width:'100%'}}>没有需要参加的会议</span>
                                </Row>
                            </Row>                            
                            :
                            <Row style={{borderTop:'1px solid #2db7f5',marginTop:'30px',marginLeft:'15px',marginRight:'15px'}}>
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                    <Col span={3}>
                                        <i className="icon-Meeting-Remind" style={{color:'#ff9c00',fontSize:'19px'}} />
                                    </Col>
                                    <Col span={1}>
                                    </Col>
                                    <Col span={19}>
                                        <div style={{fontSize:'14px',color:'#333333'}}>{`下次会议在${nextMeeting && nextMeeting.toptitle}召开`}</div>
                                    </Col>
                                </Row>
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                    
                                    <Col span={19} offset = {4}>
                                        <div style={{fontSize:'12px',color:'#868686'}}>会议名称</div>
                                        <div style={{fontSize:'12px',wordWrap:'break-word',color:'#333333'}}>{(nextMeeting && nextMeeting.meetingName)||""}</div>
                                    </Col>
                                </Row>
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                   
                                    <Col span={19} offset = {4}>
                                        <div style={{fontSize:'12px',color:'#868686'}}>会议地点</div>
                                        <div style={{fontSize:'12px',wordWrap:'break-word',color:'#333333'}}>{(nextMeeting && nextMeeting.meetingRoom)||""}</div>
                                    </Col>
                                </Row>
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                    
                                    <Col span={19} offset = {4}>
                                        <div style={{fontSize:'12px',color:'#868686'}}>召集人</div>
                                        <WeaBrowser 
                                        type={17}
                                        isSingle={false}
                                        replaceDatas={caller}
                                        viewAttr={1}
                                        inputStyle = {{width:'100%',fontSize:'10px',color:'#333333'}} />
                                    </Col>
                                </Row>
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                    
                                    <Col span={19} offset = {4}>
                                        <div style={{fontSize:'12px',color:'#868686'}}>时间</div>
                                        <div style={{fontSize:'12px',color:'#333333'}}>{nextMeeting.meetingDate && nextMeeting.meetingDate.split(" ")[0]}</div>
                                        <div style={{fontSize:'12px',color:'#333333'}}>{nextMeeting.meetingDate && `${nextMeeting.meetingDate.split("~ ")[0].split(" ")[1]} ~ ${nextMeeting.meetingDate.split("~ ")[1].split(" ")[1]}`}</div>
                                    </Col>
                                </Row>
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                   
                                    <Col span={19} offset = {4}>
                                        <div style={{fontSize:'12px',color:'#868686'}}>参会人员</div>
                                        <WeaBrowser 
                                        type={17}
                                        isSingle={false}
                                        replaceDatas={nextMeeting && nextMeeting.members}
                                        viewAttr={1}
                                        inputStyle = {{width:'100%',fontSize:'10px',color:'#333333'}} />
                                    </Col>
                                </Row>
                                {nextMeeting && nextMeeting.crms && nextMeeting.crms.length>0?
                                <Row type="flex" justify="center" align="top" style={{marginTop:'20px'}}>
                                    <Col span={3}>
                                        <i className="icon-Meeting-Participants" style={{color:'#00bafb',fontSize:'19px'}} />
                                    </Col>
                                    <Col span={1}>
                                    </Col>
                                    <Col span={19}>
                                        <div style={{fontSize:'12px',color:'#868686'}}>参会客户</div>
                                        <span style={{color:'#333333'}} onClick={()=>{window.open("/CRM/data/ViewCustomer.jsp?CustomerID=")}}>测试人
                                        </span>
                                    </Col>
                                </Row>
                                :''}
                            </Row>
                        
                        }
                        </TabPane>

                        <TabPane tab="人员组织" key="1">
                        {/*
                            <WeaOrgTree  
                            topPrefix='meeting' 
                            isLoadUser needDropMenu={true}  
                            needSearch  
                            inputRightDom={"<span  style='display:inline-block;width:28px;height:28px;border:1px solid #d9d9d9;backgroundColor=#f5f5f5'><i class='icon-Meeting-Refresh' style='display:inline-block;margin:0 6px;color:#bababa;font-size:16px'/></span>"}  
                            treeNodeClick={(e)=>{console.log(e.node.props)}}/>
                        */}
                        </TabPane>
                     </Tabs>
                </div>

				
		)
	}
    
    onChange(e){
        
    }

}



