import React, {Component} from 'react'
import PropTypes from 'react-router/lib/PropTypes'
import objectAssign from 'object-assign'
import * as tool from '../GenericMethod/index'
import WeaMonthPicker from '../wea-month-picker/'
import {Row,Col} from 'antd';
import './index.less'

class WeaDateSwitch extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: props.defaultValue || new Date(),
            color:false,
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.valueDate !== nextProps.valueDate) {
            this.setState({value: new Date(nextProps.valueDate)});
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }

    render() {
        let {title, style,all} = this.props,
            {value,color} = this.state;
        let {type,date,meetingType} = all;
        let dates = new Date();
        let m = dates.getMonth()+1;
        let values = objectAssign({},{ 
                    selectUser:'',
                    selectdate:date,
                    meetingType:meetingType,//会议类型
                });
        return (
            <Row style={style}>
                <span style={{color:color?'#2db7f5':'#333333',cursor:color?'pointer':'auto'}} onMouseEnter={()=>{this.setState({color:true})}} onMouseOut={()=>{this.setState({color:false})}} onClick={()=>{this.setState({value: new Date()});this.props.actions.changeMonth((new Date()).getTime());this.props.actions.saveDate(`${dates.getFullYear()}-${m<10?'0'+m:m}-${dates.getDate()}`)}}>本月</span>
                <div className="wea-date-switch">
                    <span className="wea-ds-pre icon-coms-Browse-box-delete" style={{color:'#868686'}} onClick={this.preDateClick}/>
                    <WeaMonthPicker value={value} onChange={this.onChange}/>
                    <span className="wea-ds-next icon-coms-Browse-box-Add-to" style={{color:'#868686'}} onClick={this.nextDateClick}/>
                </div>
                <span className="icon-Meeting-Refresh"
                    onClick={()=>{
                        type==1 ?
                        this.props.actions.getData({
                            selectUser:'',
                            selectdate:all.date || tool.changeDateFormat(dates),//拼接日期格式为yyyy-MM-dd
                            meetingType:all.meetingType,
                        })
                        :
                        this.props.actions.getCalendarList(values)}}>
                    
                </span>
            </Row>
        )
    }

    preDateClick = () => {
        let {value} = this.state
        value.setMonth(value.getMonth() - 1)
        this.setState({value: value})
        if (this.props.onChange) this.props.onChange(value)
    }

    nextDateClick = () => {
        let {value} = this.state
        value.setMonth(value.getMonth() + 1)
        this.setState({value: value})
        if (this.props.onChange) this.props.onChange(value)
    }

    onChange = (value) => {
        this.setState({value: value})
        if (this.props.onChange) this.props.onChange(value)
    }
}

export default WeaDateSwitch