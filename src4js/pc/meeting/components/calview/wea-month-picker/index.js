import React, {Component} from 'react'
import PropTypes from 'react-router/lib/PropTypes'

import {Dropdown} from 'antd';

import './index.less'

class WeaMonthPicker extends Component {
    constructor(props) {
        super(props)

        let {value} = this.props
        this.state = {
            visible: false,

            value: value,
            year: value.getFullYear(),
            month: value.getMonth() + 1
        }
    }

    componentWillReceiveProps(props) {
        let {year, month} = this.state,
            _year = props.value.getFullYear(),
            _month = props.value.getMonth() + 1
        this.setState({
            value: props.value,
            year: _year,
            month: _month
        })
    }

    render() {
        let {style} = this.props,
            {year, month, visible} = this.state,
            dateStr = year + "-" + (month < 10 ? '0' + month.toString() : month)
        return (
            <div className="wea-month-picker" style={style}>
                <Dropdown
                    overlay={
                        <div className="wea-month-picker-modal">
                            <div className="wea-mpm-head">
                                <span className="wea-mpmh-pre icon-blog-left" onClick={() => this.preYearClick(year)}/>
                                <span className="wea-mpmh-text">{dateStr}</span>
                                <span className="wea-mpmh-next icon-blog-right"
                                      onClick={() => this.nextYearClick(year)}/>
                            </div>
                            <table className="wea-mpm-body" onClick={this.setMonth}>
                                <tr>
                                    <td value="1"><span
                                        className={"wea-mpmb-item " + (month == 1 ? "active" : "")}>一月</span></td>
                                    <td value="2"><span
                                        className={"wea-mpmb-item " + (month == 2 ? "active" : "")}>二月</span></td>
                                    <td value="3"><span
                                        className={"wea-mpmb-item " + (month == 3 ? "active" : "")}>三月</span></td>
                                </tr>
                                <tr>
                                    <td value="4"><span
                                        className={"wea-mpmb-item " + (month == 4 ? "active" : "")}>四月</span></td>
                                    <td value="5"><span
                                        className={"wea-mpmb-item " + (month == 5 ? "active" : "")}>五月</span></td>
                                    <td value="6"><span
                                        className={"wea-mpmb-item " + (month == 6 ? "active" : "")}>六月</span></td>
                                </tr>
                                <tr>
                                    <td value="7"><span
                                        className={"wea-mpmb-item " + (month == 7 ? "active" : "")}>七月</span></td>
                                    <td value="8"><span
                                        className={"wea-mpmb-item " + (month == 8 ? "active" : "")}>八月</span></td>
                                    <td value="9"><span
                                        className={"wea-mpmb-item " + (month == 9 ? "active" : "")}>九月</span></td>
                                </tr>
                                <tr>
                                    <td value="10"><span
                                        className={"wea-mpmb-item " + (month == 10 ? "active" : "")}>十月</span></td>
                                    <td value="11"><span className={"wea-mpmb-item " + (month == 11 ? "active" : "")}>十一月</span>
                                    </td>
                                    <td value="12"><span className={"wea-mpmb-item " + (month == 12 ? "active" : "")}>十二月</span>
                                    </td>
                                </tr>
                            </table>
                            <div className="wea-mpm-foot" onClick={this.setToday}>
                                本月
                            </div>
                        </div>
                    }
                    visible={visible}
                    onVisibleChange={this.changeVisible}
                >
                    <span className="wea-mp-text">
                        {dateStr}
                    </span>
                </Dropdown>
            </div>
        )
    }

    changeVisible = (visible) => {
        this.setState({visible: visible})

        if (visible) {
            this.date = this.state.value
        } else {
            let {year, month} = this.state,
                _year = this.date.getFullYear(),
                _month = this.date.getMonth() + 1,
                date = new Date()

            if (_year != year || _month != month) {
                date.setFullYear(year)
                date.setMonth(month - 1)
                this.setState({value: date})
                if (this.props.onChange) this.props.onChange(date)
            }
        }
    }

    preYearClick = (year) => {
        this.setState({year: year - 1})
    }

    nextYearClick = (year) => {
        this.setState({year: year + 1})
    }

    setToday = () => {
        let value = new Date()
        this.setState({
            value: value,
            year: value.getFullYear(),
            month: value.getMonth() + 1
        })
    }

    setMonth = (e) => {
        let value = e.target.parentNode.getAttribute("value")
        if (value) {
            this.setState({month: value})
        }
    }
}

export default WeaMonthPicker