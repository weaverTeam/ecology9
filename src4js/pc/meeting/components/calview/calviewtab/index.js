import {Row,Col,Switch,Tag,DatePicker,Popover,Tooltip} from 'antd';
const MonthPicker = DatePicker.MonthPicker;
import objectAssign from 'object-assign'
import WeaDateSwitch from '../wea-date-switch/index'
import * as tool from '../genericMethod/index'

export default class CalViewTab extends React.Component {
	constructor(props) {
        super(props);
        this.changeMonth=this.changeMonth.bind(this)
        this.state = {
            date: '',

        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.month !== nextProps.month) {
            this.setState({date: nextProps.month});
        }
    }
    changeMonth(month){
        const date = tool.changeDateFormat(month);
        const {all} = this.props;
        const {actions} = all;
        let m = month.getMonth()+1;
        actions.changeMonth(month.getTime());
        actions.saveDate(date);
    }

	render(){
        const {all} = this.props;
		let {actions,type,meetingType,sessionkey,date} = all;
        
        let dates = new Date();
        let m = dates.getMonth()+1;
        let value = objectAssign({},{
            selectUser:'',
            selectdate:date,
            meetingType:meetingType,//会议类型
        })
		return(
			<Row className="calviewtab" style={{borderBottom:'1px solid #e9e9e9',borderLeft:'1px solid #e9e9e9',display:'block',height:'45px'}}>
                    <Col span="10" style={{textAlign:"left",height:'45px'}}>
                        <WeaDateSwitch
                        style={{width:'220px',height:'46px',lineHeight:'46px',fontSize:'12px',marginLeft:'20px',color:'#333333',display:'inline-block'}}
                        defaultValue={new Date()}
                        onChange={this.changeMonth}
                        valueDate={this.state.date||new Date()}
                        actions={actions}
                        all={all}   
                        />
                    </Col>
                    <Col span="14" style={{textAlign:"right",height:'45px'}}>
                        <div style={{backgroundColor:type==1?'#00bafb':'#ffffff',display:'inline-block',height:'26px',width:'26px',marginTop:'8px',border:'1px solid #00bafb',borderTopLeftRadius:'2px',borderBottomLeftRadius:'2px'}}>
                            <Tooltip arrowPointAtCenter={true} placement="bottomRight" title={"时间视图"} >
                                <i className="icon-Meeting-Tile-view"
                                style={{color:type==1?'#ffffff':'#bababa',fontSize:'14px',marginTop:'5px',marginRight:'5px',display:'inline-block'}} 
                                onClick={()=>{actions.changeType(1);
                                    actions.getData({
                                        selectUser:'',
                                        selectdate:all.date || tool.changeDateFormat(dates),//拼接日期格式为yyyy-MM-dd
                                        meetingType:all.meetingType})
                                }}>
                                </i>
                            </Tooltip>
                        </div>
                        <div style={{backgroundColor:type==2?'#00bafb':'#ffffff',marginRight:'15px',display:'inline-block',height:'26px',width:'26px',marginTop:'8px',border:'1px solid #00bafb',borderTopRightRadius:'2px',borderBottomRightRadius:'2px'}}>
                            <Tooltip arrowPointAtCenter={true} placement="bottomRight" title={"列表视图"} >
                                <i className="icon-Meeting-List-view" style={{color:type==2?'#ffffff':'#bababa',fontSize:'14px',marginTop:'5px',marginRight:'5px',display:'inline-block'}} onClick={()=>{actions.changeType(2);actions.getCalendarList(value)}}>
                                </i>
                            </Tooltip>
                        </div>
	                </Col>
            </Row>
				
		)
	}

}


