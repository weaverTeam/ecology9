import {Row,Col,Switch,Tag,DatePicker,Calendar, Button,Popover,Tooltip} from 'antd';
import {WeaTable} from 'comsRedux'
import {WeaDialog} from 'ecCom';
import * as tool from '../genericMethod/index'



const MonthPicker = DatePicker.MonthPicker;

export default class LeftContainer extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            num:0,
        };
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.month !== nextProps.month) {
            this.setState({date: nextProps.month});
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    onPanelChange(date, mode){
    }
    getListData(value) {
        let listData = [];
        const {all} = this.props;
        const {data} = all;
        if(data.events && data.events.length==0){
            return listData;
        }else{
            data.events && data.events.length!==0 && data.events.map((item,index)=>{
                if((value.getMonth()+1) == item.begindate.split("-")[1]){
                    if(value.getDayOfMonth() == item.begindate.split("-")[2]){
                        listData.push({
                        id:item.id,
                        type:'normal',
                        status:item.status,
                        content:`${item.begintime} ${item.name}`});
                    }else if(value.getDayOfMonth() <= item.enddate.split("-")[2] && item.begindate.split("-")[2] < value.getDayOfMonth()){
                        listData.push({
                        id:item.id,
                        type:'normal',
                        status:item.status,
                        content:`${item.begintime} ${item.name}`});
                    }
                }
            })
        }
        return listData;
    }

    dateCellRender(value) {
        const listData = this.getListData(value);
        const {actions} = this.props.all;
        var a = 0 ;
        return (
            <div title="">
                <div style={{overflow:'hidden'}}>
                    <ul className="events" style= {{height:'66px'}}>
                        {
                           listData.length!=0 && listData.map((item, index) =>{
                                 a = index;
                              return <li key={index} onClick={(e)=>{e.stopPropagation();actions.savePreviewMeetingId({meetingid:item.id})}} style={{margin:'2px 0 2px 0',height:'20px'}} className={`metting-calview-calendar-${item.status}`}>
                                <Tooltip placement="rightTop" title={item.content}>
                                    <a style={{width:'100%',display:'inline-block',overflow:'hidden',color:'#333333'}}>
                                        <span className={`event-${item.status}`} style={{marginLeft:'5px'}}>●</span>
                                        <span style={{whiteSpace:'nowrap',marginLeft:'5px'}}>{item.content}</span>
                                    </a>
                                </Tooltip>
                              </li>
                            }
                            )
                        }
                    </ul>
                </div>

                <div>
                {
                    a>2?
                    <Popover
                    className="metting-calview-popover"
                    getTooltipContainer={() => document.getElementById('metting-calview-id')}
                    placement="rightTop" title="" 
                    content={<div>
                                <ul className="events">
                                    {
                                        listData.map((item, index) =>{
                                          return <li key={index} onClick={(e)=>{e.stopPropagation();actions.savePreviewMeetingId({meetingid:item.id})}} className={`metting-calview-calendar-${item.status}`}>
                                            <span className={`event-${item.status}`} style={{marginLeft:'5px'}}>●</span>
                                            <span style={{marginLeft:'5px'}}>
                                                {item.content}
                                            </span>
                                          </li>
                                        }
                                        )
                                    }
                                </ul>
                            </div>}
                    trigger="click"
                    onClick={(e)=>{e.stopPropagation()}}
                    >
                        <span style={{fontSize:'12px',color:'#25a7e6',textDecoration:'underline',marginLeft:'18px'}}>显示更多会议</span>
                    </Popover>
                       
                      :''
                }
                </div>
            </div>

        );
    }
    getLocale() {
        return {
            firstDayOfWeek: 0,
            lang:{format:{
                eras: ['公元前', '公元'],
                months: ['一月', '二月', '三月', '四月', '五月', '六月',
                    '七月', '八月', '九月', '十月', '十一月', '十二月'],
                shortMonths: ['一月', '二月', '三月', '四月', '五月', '六月',
                    '七月', '八月', '九月', '十月', '十一月', '十二月'],
                weekdays: ['星期天', '星期一', '星期二', '星期三', '星期四',
                    '星期五', '星期六'],
                shortWeekdays: ['周日', '周一', '周二', '周三', '周四', '周五',
                    '周六'],
                veryShortWeekdays: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                ampms: ['上午', '下午'],
                datePatterns: ["yyyy'年'M'月'd'日' EEEE", "yyyy'年'M'月'd'日'", "yyyy-M-d", "yy-M-d"],
                timePatterns: ["ahh'时'mm'分'ss'秒' 'GMT'Z", "ahh'时'mm'分'ss'秒'", "H:mm:ss", "ah:mm"],
                dateTimePattern: '{date} {time}'
            }},
        }
    }
    onSelect(geoDate) {
        const {all} = this.props;
        const {actions} = all;
        this.setState({date:new Date(geoDate.time)});
        actions.changeMonth(geoDate.time);
        let date = new Date(geoDate.time);
        date = tool.changeDateFormat(date);
        actions.saveFields({
            begindate:{name:'begindate',value:date},
            enddate:{name:'enddate',value:date},
            begintime:{name:'begintime',valueSpan:'00:00',value:'00:00'},
            endtime:{name:'endtime',valueSpan:'23:59',value:'23:59'},
        });
        // actions.getMeetingTopicField();
        // actions.getMeetingServiceField();
        actions.controlModal(true,'',date);
    }    
    
	render(){
		const {sessionkey,loading,all,data,month} = this.props;

		return(
			    <div className="leftC" style={{paddingLeft:'220px',width:'100%'}}>
                    <div>
                    {
                        this.props.children
                    }
                    </div>
                    <div >
                    {all.type==1?
                        <Calendar
                            defaultValue={new Date()}
                            dateCellRender={this.dateCellRender.bind(this)}
                            onPanelChange={this.onPanelChange}
                            locale={this.getLocale()}
                            onSelect={this.onSelect.bind(this)}
                            value={this.state.date}
                        />
                        :
                        <WeaTable
                            hasOrder={true}
                            needScroll={false}
                            loading={loading}
                            sessionkey={sessionkey}
                            getColumns = {this.getColumns}
                        />
                    }
                    </div>
                </div>
				
		)
	}
    getColumns = (e)=>{
        const {actions} = this.props.all;
        let newColumns = '';
        newColumns = e.map(column => {
                let newColumn = column;
                newColumn.render = (text, record, index) => { //前端元素转义
                    let valueSpan = record[newColumn.dataIndex + "span"] !== undefined ? record[newColumn.dataIndex + "span"] : record[newColumn.dataIndex];
                    return(
                        newColumn.dataIndex == 'name'  ?
                         <div className="wea-url-name" style ={{cursor:'pointer'}} onClick={()=>{actions.savePreviewMeetingId({meetingid:record.id})}} dangerouslySetInnerHTML={{__html: valueSpan}} />
                        :
                         <div className="wea-url" dangerouslySetInnerHTML={{__html: valueSpan}} />

                    )
                }
                return newColumn;
            });
        return newColumns;
    }

}


