import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as ReportAction from '../actions/Report'
import SearchButton from './report/SearchButton';
import { WeaErrorPage, WeaTools,WeaEchart,WeaTop } from 'ecCom'
import isEmpty from 'lodash/isEmpty';
import objectAssign from 'object-assign'

import {Tabs} from 'antd';
const TabPane = Tabs.TabPane;
import isEqual from 'lodash/isEqual';
//import { } from 'comsRedux'

class Report extends React.Component {
	static contextTypes = {
		router: PropTypes.routerShape
	}
	constructor(props) {
		super(props);
		this.changeTab = this.changeTab.bind(this);
		        
	}
	componentWillMount() {
		//一些初始化请求
		const { actions } = this.props;
		actions.getUserDefInfo();
	}
	componentDidMount() {
		//一些初始化请求
		const { actions } = this.props;
	}
	componentWillReceiveProps(nextProps) {
		const { actions } = this.props;
		let {setsearchtime,setsearchtype,userinfo,absent,reporttab} = nextProps;
		let params = {
				timeSag:setsearchtime,
				selectType:setsearchtype,

			};
		if(this.props.reporttab != reporttab||
			(this.props.setsearchtime != nextProps.setsearchtime && nextProps.setsearchtime!=6)||
			this.props.setsearchtype != nextProps.setsearchtype||
			(this.props.userinfo != nextProps.userinfo && !isEmpty(userinfo))){
			
			if(setsearchtype == 1){//分部
				params = objectAssign(params,{
					subIds:userinfo.defsub.id
				})
			}
			if(setsearchtype == 2){//部门
				params = objectAssign(params,{
					depIds:userinfo.defdep.id
				})
			}
			if(reporttab == 1){
				actions.absent(params)
			}
			if(reporttab == 2){
				actions.resolut(params)
			}
			if(reporttab == 3){
				actions.processd(params)
			}
		}

	}
	shouldComponentUpdate(nextProps, nextState) {
		//组件渲染控制
		//return this.props.title !== nextProps.title
		return true;
	}
	componentDidUpdate(){
		let {absent,resolut,processd} = this.props
		if(!isEmpty(absent)){
			if(this.refs.chart1){
				this.refs.chart1.clear();
				this.refs.chart1.paint();
			}
		}
		if(!isEmpty(resolut)){
			if(this.refs.chart2){
				this.refs.chart2.clear();
				this.refs.chart2.paint();
			}
		}
		if(!isEmpty(processd)){
			if(this.refs.chart3){
				this.refs.chart3.clear();
				this.refs.chart3.paint();
			}
		}
	}
	componentWillUnmount() {
		//组件卸载时一般清理一些状态
	}
	render() {
		let {setsearchtime, setsearchtype,loading,userinfo,absent,resolut,processd,reporttab} = this.props;
  		let browseparams = setsearchtype==1 ?{
                            "dataParams":{

                            },
                            "hasAddBtn":false,
                            "hasAdvanceSerach":true,
                            "isAutoComplete":1,
                            "isDetail":0,
                            "isMultCheckbox":false,
                            "isSingle":true,
                            "linkUrl":"/hrm/company/HrmSubCompanyDsp.jsp?id=",
                            "tabs":[
                                {
                                    "dataParams":{
                                        "list":"1"
                                    },
                                    "key":"1",
                                    "name":"按列表",
                                    "selected":false
                                },
                                {
                                    "key":"2",
                                    "name":"按组织结构",
                                    "selected":false
                                }
                            ],
                            "title":"分部",
                            "type":"164",
                            "viewAttr":2,
                        }
                    :setsearchtype==2 ?{
                            "dataParams":{

                            },
                            "hasAddBtn":false,
                            "hasAdvanceSerach":true,
                            "isAutoComplete":1,
                            "isDetail":0,
                            "isMultCheckbox":false,
                            "isSingle":true,
                            "linkUrl":"/hrm/company/HrmDepartmentDsp.jsp?id=",
                            "tabs":[
                                {
                                    "dataParams":{
                                        "list":"1"
                                    },
                                    "key":"1",
                                    "name":"按列表",
                                    "selected":false
                                },
                                {
                                    "key":"2",
                                    "name":"按组织结构",
                                    "selected":false
                                }
                            ],
                            "title":"部门",
                            "type":"4",
                            "viewAttr":2
                        }:'';

        let timedatas = [
	        {id:'2',name:'本周'},
	        {id:'3',name:'本月'},
	        {id:'7',name:'上个月'},
	        {id:'4',name:'本季'},
	        {id:'9',name:'上一季'},
	        {id:'5',name:'本年'},
	        {id:'8',name:'上一年'},
	        {id:'6',name:'指定日期范围'},
	        ];
        let typedatas = [
	        {id:'3',name:'总部'},
	        {id:'1',name:'分部'},
	        {id:'2',name:'部门'},
	        ];
		
		let option1 = '',option2 = '' , option3 = '';
		
		if(!isEmpty(absent)){
			option1 = {
			    title: {
			        x: 'center',
			        text: '会议缺席次数统计排名',
			    },
			    tooltip: {
			        trigger: 'item'
			    },
			    toolbox: {
			        show: true,
			        feature: {
			            dataView: {show: true, readOnly: false},
			            restore: {show: true},
			            saveAsImage: {show: true}
			        }
			    },
			    grid: {
			        borderWidth: 10,
			        y: 80,
			        y2: 60
			    },
			    xAxis: [
			        {
			            type: 'category',
			            show: false,
			            data: ['Line', 'Bar', 'Scatter', 'K', 'Pie', 'Radar', 'Chord', 'Force', 'Map', 'Gauge', 'Funnel']
			        }
			    ],
			    yAxis: [
			        {
			            type: 'value',
			            show: false
			        }
			    ],
			    series: [
			        {
			            name: '会议缺席次数统计排名',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#C1232B','#B5C334','#FCCE10','#E87C25','#27727B',
			                           '#FE8463','#9BCA63','#FAD860','#F3A43B','#60C0DD',
			                           '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			            
			        }
			    ]
			};
			option1.series[0].data = absent.value||[];
			option1.xAxis[0].data = absent.name||[];
		}
		if(!isEmpty(resolut)){
			option2 = {
			    title: {
			        x: 'center',
			        text: '会议决议提交情况',
			    },
			    tooltip: {
			        trigger: 'item'
			    },
			    toolbox: {
			        show: true,
			        feature: {
			            dataView: {show: true, readOnly: false},
			            restore: {show: true},
			            saveAsImage: {show: true}
			        }
			    },
			    grid: {
			        borderWidth: 10,
			        y: 80,
			        y2: 60
			    },
			    xAxis: [
			        {
			            type: 'category',
			            show: false,
			            data: ['Line', 'Bar', 'Scatter', 'K', 'Pie', 'Radar', 'Chord', 'Force', 'Map', 'Gauge', 'Funnel']
			        }
			    ],
			    yAxis: [
			        {
			            type: 'value',
			            show: false
			        }
			    ],
			    series: [
			    	{
			            name: '已提交',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#4DB6C3'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			        },
			        {
			            name: '未提交',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#E64C47'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			        }
			        
			    ]
			}
			option2.series[0].data = resolut.value[0]||[];//已提交
			option2.series[1].data = resolut.value[1]||[];//未提交
			option2.xAxis[0].data = resolut.name;
		}
		if(!isEmpty(processd)){
			option3 = {
			    title: {
			        x: 'center',
			        text: '会议任务完成情况',
			    },
			    tooltip: {
			        trigger: 'item'
			    },
			    toolbox: {
			        show: true,
			        feature: {
			            dataView: {show: true, readOnly: false},
			            restore: {show: true},
			            saveAsImage: {show: true}
			        }
			    },
			    grid: {
			        borderWidth: 10,
			        y: 80,
			        y2: 60
			    },
			    xAxis: [
			        {
			            type: 'category',
			            show: false,
			            data: ['Line', 'Bar', 'Scatter', 'K', 'Pie', 'Radar', 'Chord', 'Force', 'Map', 'Gauge', 'Funnel']
			        }
			    ],
			    yAxis: [
			        {
			            type: 'value',
			            show: false
			        }
			    ],
			    series: [
			    	{
			            name: '未开始',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#FFCC00'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			        },
			        {
			            name: '已延期',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#D50020'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			        },
			        {
			            name: '进行中',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#8BCC52'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			        },
			        {
			            name: '已完成',
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: function(params) {
			                        var colorList = [
			                          '#1FC3E1'
			                        ];
			                        return colorList[params.dataIndex]
			                    },
			                    label: {
			                        show: true,
			                        position: 'top',
			                        formatter: '{b}\n{c}'
			                    }
			                }
			            },
			            data: [12,21,10,4,12,5,6,5,25,23,7],
			        }

			    ]
			}
			option3.series[0].data = processd.value[0]||[];//未开始
			option3.series[1].data = processd.value[1]||[];//已延期
			option3.series[2].data = processd.value[2]||[];//进行中
			option3.series[3].data = processd.value[3]||[];//已完成
			option3.xAxis[0].data = processd.name;
		}

		
		let hasDatePicker = setsearchtime == 6 ? true : false;

		return (
			<div className={"metting-report"} style={{height: 800}}>
				<WeaTop 
	            	loading={loading} 
	            	icon={<i className='icon-coms-meeting' />} 
	            	iconBgcolor='#f14a2d' 
	            	title='会议报表'  
	            	buttons={[]}
		            showDropIcon={false}
	                dropMenuDatas={this.getRightMenu()}
	                onDropMenuClick={this.onRightMenuClick.bind(this)}
	            >
	            <div style={{margin:'16px',borderTop:'1px solid #e9e9e9',borderLeft:'1px solid #e9e9e9',borderRight:'1px solid #e9e9e9',borderRadius:'4px'}}>
					<SearchButton
						label="时间范围：" 
			   			datas={timedatas} 
			   			setData={this.setModuleData.bind(this,'time')} 
			   			selItem={setsearchtime}
			   			hasDatePicker={hasDatePicker}
			   			dateChange={this.dateChange}
			   		/>
			   		<SearchButton
						label="数据统计范围：" 
			   			datas={typedatas} 
			   			setData={this.setModuleData.bind(this,'type')} 
			   			selItem={setsearchtype}
			   			hasBrowse={browseparams}
			   			replaceDatas={userinfo}
			   			browseChange={this.browseChange}
			   		/>
	            </div>    
	                <Tabs type="card" activeKey={`${reporttab}`} onChange={this.changeTab}>
                        <TabPane tab="会议缺席统计" key="1">
                            <div style={{height: 300,backgroundColor:'#ffffff',paddingTop:'20px'}}>
			                    <WeaEchart ref="chart1" useDefault={false} option={option1} />
			                </div>
                        </TabPane>
                        <TabPane tab="会议决议统计" key="2">
                            <div style={{height: 300,backgroundColor:'#ffffff',paddingTop:'20px'}}>
			                    <WeaEchart ref="chart2" useDefault={false} option={option2} />
			                </div>
                        </TabPane>
                        <TabPane tab="会议任务统计" key="3">
                            <div style={{height: 300,backgroundColor:'#ffffff',paddingTop:'20px'}}>
			                    <WeaEchart ref="chart3" useDefault={false} option={option3} />
			                </div>
                        </TabPane>
	                </Tabs>
                </WeaTop>
            </div>
		)
	}
	changeTab(e){
        const {actions} = this.props;
        actions.changeReportTab(e);
    }
	setModuleData = (a,b) => {
		this.props.actions.setSearchInfo(b.id,a);
	}
	dateChange = (value, dateString) => {
		let {actions,setsearchtime,setsearchtype,userinfo,reporttab} = this.props;
		let params = {
				timeSag:setsearchtime,
				selectType:setsearchtype,
			};
		if(setsearchtype == 1){//分部
			params = objectAssign(params,{
				subIds:userinfo.defsub.id
			})
		}
		if(setsearchtype == 2){//部门
			params = objectAssign(params,{
				depIds:userinfo.defdep.id
			})
		}
		params = objectAssign(params,{
				begindate:dateString[0],
				enddate:dateString[1],
			})

		if(reporttab == 1){
			actions.absent(params)
		}
		if(reporttab == 2){
			actions.resolut(params)
		}
		if(reporttab == 3){
			actions.processd(params)
		}

	}
	browseChange = (browsetype,ids) => {
		let {actions,setsearchtime,setsearchtype,userinfo,reporttab} = this.props;
		let params = {
				timeSag:setsearchtime,
				selectType:setsearchtype,
			};
		if(browsetype == 164){//分部
			params = objectAssign(params,{
				subIds:ids
			})
		}
		if(browsetype == 4){//部门
			params = objectAssign(params,{
				depIds:ids
			})
		}
		if(reporttab == 1){
			actions.absent(params)
		}
		if(reporttab == 2){
			actions.resolut(params)
		}
		if(reporttab == 3){
			actions.processd(params)
		}
	}
	getRightMenu(){
		
	}
	onRightMenuClick(){
		const {actions} = this.props;
	}


}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

Report = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(Report);

//form 表单与 redux 双向绑定
//Report = createForm({
//	onFieldsChange(props, fields) {
//		props.actions.saveFields({ ...props.fields, ...fields });
//	},
//	mapPropsToFields(props) {
//		return props.fields;
//	}
//})(Report);


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	const { loading, title } = state.meetingReport;
	return { ...state.meetingReport }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(ReportAction, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Report);




