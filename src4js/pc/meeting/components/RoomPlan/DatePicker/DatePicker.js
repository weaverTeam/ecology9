import {DatePicker} from 'antd';
import moment from 'moment';
import './DatePicker.less';

const MonthPicker = DatePicker.MonthPicker;

class MyDatePicker extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            togglePicker: false,
            date: props.date,
            dateLabel: this.parseDate(props.date)
        };
        this.onChange = this.onChange.bind(this);
        this.togglePicker = this.togglePicker.bind(this);
        this.parseDate = this.parseDate.bind(this);
        this.changeValue = this.changeValue.bind(this);
	}
	
	componentWillReceiveProps(nextProps) {
        this.setState({
            date: nextProps.date,
            dateLabel: this.parseDate(nextProps.date, nextProps.type)
        });
    }
	
    parseDate(value, type = this.props.type) {
        if (!value) return "请选择日期";
        let curMoment = value._isAMomentObject ? value : moment(value);
        switch(type) {
            case "month":
                return curMoment.format("YYYY年M月");
            case "week": {
                if (curMoment.day() > 0) {
                    return curMoment.day(1).format("YYYY年M月D日") + "~" + curMoment.day(7).format("YYYY年M月D日");
                } else {
                    return curMoment.day(-6).format("YYYY年M月D日") + "~" + curMoment.day(7).format("YYYY年M月D日");
                }
            }
            case "day":
            default:
                return curMoment.format("YYYY年M月D日");
        }
    }
    
    onChange(date,dateStr) {
        this.setState({
            date: date,
            dateLabel: this.parseDate(date)
        });
        this.togglePicker();
        this.callback(date)
    }
    togglePicker() {
        this.setState({
            togglePicker: !this.state.togglePicker
        });
    }
    changeValue(diff) {
        let curMoment = moment(this.state.date);
        switch(this.props.type) {
            case "month": {
                let month = curMoment.month();
                curMoment.month(month + diff);
                break;
            }
            case "week": {
                let dayOfYear = curMoment.dayOfYear();
                curMoment.dayOfYear(dayOfYear + diff * 7);
                break;
            }
            case "day": {
                let dayOfYear = curMoment.dayOfYear();
                curMoment.dayOfYear(dayOfYear + diff);
                break;
            }
        }
        this.setState({
            date: curMoment,
            dateLabel: this.parseDate(curMoment)
        });
        this.callback(curMoment);
    }
    
    callback(value) {
        if (!this.props.onChange) return;
        let dateRange = this.getDateRange(value);
        this.props.onChange(value, dateRange);
    }
    
    getDateRange(value) {
        let curMoment = moment(value);
        curMoment.fmt = () => curMoment.format("YYYY-MM-DD");
        let date;
        switch (this.props.type) {
            case "month":
                date = [];
                date.push(curMoment.date(1).fmt());
                date.push(curMoment.date(31).fmt());
                break;
            case "week": {
                date = [];
                if (curMoment.day() > 0) {
                    date.push(curMoment.day(1).fmt());
                } else {
                    date.push(curMoment.day(-6).fmt());
                }
                date.push(curMoment.day(7).fmt());
                break;
            }
            case "day":
            default:
                date = curMoment.fmt();
        }
        return date;
    }
    
	render() {
	    let date = this.state.date, defaultValue = moment.isMoment(date) ? date.toDate() : date;
	    let pickerProps = {
	        onChange: this.onChange,
            open: this.state.togglePicker,
            defaultValue: defaultValue,
            toggleOpen: this.togglePicker
        };
		return (
			<div className={`myDatePicker ${this.props.className}`}>
                <span className="icon-coms-Browse-box-delete" onClick={this.changeValue.bind(this, -1)}></span>
                <span
                    style={this.state.togglePicker ? {display: 'none'} : {}}
                    onClick={this.togglePicker}
                    className="label"
                    >
                    {this.state.dateLabel}
                </span>
                <span style={this.state.togglePicker ? {} : {display: 'none'}}>
                    {this.props.type == 'month'?
                        <MonthPicker {...pickerProps}/> :
                        <DatePicker {...pickerProps}/>
                    }
                </span>
                <span className="icon-coms-Browse-box-Add-to" onClick={this.changeValue.bind(this, 1)}></span>
            </div>
		);
	}
}
MyDatePicker.defaultProps = {
    type: 'date'
};
MyDatePicker.propTypes = {
    type: React.PropTypes.string,//month为月选择器，week为周选择器，其它为日选择器
    onChange: React.PropTypes.func,//时间发生变化的回调,会返回选择的日期与类型对应的日期范围
    date: React.PropTypes.any,//当前日期，支持jsDate,字符串
    className: React.PropTypes.string//附加的样式名
};
export default MyDatePicker;