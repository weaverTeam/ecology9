import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { WeaErrorPage, WeaTools } from 'ecCom'
import * as RoomPlanAction from '../../actions/roomPlan'
import {
    WeaTop,
    WeaTab,
    WeaLeftRightLayout,
    WeaRightMenu
} from 'ecCom'
import LeftTab from './LeftTab';
import RoomPlanContainer from './RoomPlanContainer';
import {Button} from 'antd';
//import { } from 'comsRedux'

import '../../css/RoomPlan.less';

const rightBtns = [
    {key: "new",content:'新建会议',icon: <span className='icon-coms-New-Flow'/>},
];
class RoomPlan extends React.Component {
	static contextTypes = {
		router: PropTypes.routerShape
	}
	constructor(props) {
		super(props);
	}
	render() {
		const { loading, title, org } = this.props;
		return (
			<div>
                <WeaRightMenu datas={rightBtns} onClick={this.clickButton}>
                    <WeaTop
                        title={title}
                        loading={loading}
                        icon={<span className='icon-coms-meeting'/>}
                        iconBgcolor='#0079de'
                        buttons={[
                            <Button type="primary" size="large" onClick={this.createMeeting}>新建会议</Button>,
                        ]}
                        showDropIcon={true}
                        dropMenuDatas={rightBtns}
                        onDropMenuClick={this.clickButton}
                    >
                        <WeaLeftRightLayout defaultShowLeft={false} leftCom={<LeftTab/>}>
                            <div className="wea-right-container">
                                <RoomPlanContainer ref="roomCon" subId={org.id} subName={org.name}/>
                            </div>
                        </WeaLeftRightLayout>
                    </WeaTop>
                </WeaRightMenu>
            </div>
            
		)
	}
	
	createMeeting = () => {
		this.refs.roomCon && this.refs.roomCon.getWrappedInstance().openCreateMeetingDialog();
	}
    
	clickButton= (key) => {
	    this.createMeeting();
	}
}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

RoomPlan = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(RoomPlan);

//form 表单与 redux 双向绑定
//RoomPlan = createForm({
//	onFieldsChange(props, fields) {
//		props.actions.saveFields({ ...props.fields, ...fields });
//	},
//	mapPropsToFields(props) {
//		return props.fields;
//	}
//})(RoomPlan);


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	const { loading, title, org } = state.meetingRoomPlan;
	return { loading, title, org }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(RoomPlanAction, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(RoomPlan);