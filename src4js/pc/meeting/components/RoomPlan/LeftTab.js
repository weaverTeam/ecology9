import {WeaOrgTree } from 'ecCom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as RoomPlanAction from '../../actions/roomPlan'

class LeftTab extends React.Component {
	constructor(props) {
		super(props);
        this.clickTree = ::this.clickTree;
	}
	clickTree(t) {
        if (t && t.node && t.node.props ) {
            this.props.actions.setOrg(t.node.props)
        }
    }
	render() {
		return (
			<div>
                <WeaOrgTree  treeNodeClick={this.clickTree}
                             needSearch={true}
                />
            </div>
		);
	}
}

const mapStateToProps = state => {
    return {}
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(RoomPlanAction, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LeftTab);