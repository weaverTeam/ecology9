import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import objectAssign from 'object-assign'
import * as CalViewAction from '../actions/calview'
import * as PreviewMeetingAction from '../actions/previewMeeting'

import * as tool from './calview/genericmethod/index'
import pick from 'lodash/pick';
import isEmpty from 'lodash/isEmpty';
import { WeaErrorPage, WeaTools,WeaSearchGroup,WeaTop,WeaTab,WeaRightMenu,WeaLeftRightLayout } from 'ecCom';
import {Row,Col,Input,Tabs,Button,Alert,Spin,Icon,Form,Modal} from 'antd';
const createForm = Form.create;
import {WeaTable} from 'comsRedux'
const WeaTableAction = WeaTable.action;
import CalViewTab from './calview/calviewtab/index'
import LeftContainer from './calview/leftcontainer/index'
import RightContainer from './calview/rightcontainer/index'
import PreviewMeeting from './previewmeeting/index'
import CreateMeeting from './createmeeting/index'


// import "../css/CalView.css"
import "../css/calView.less"
import "../css/icon.css"


let _this = null;

//import { } from 'comsRedux'

class CalView extends React.Component {
	static contextTypes = {
		router: PropTypes.routerShape
	}
	constructor(props) {
		super(props);
		_this = this;
	}
	componentWillMount() {
		//一些初始化请求
		const { actions } = this.props;
		let dates = new Date();
		let m = dates.getMonth()+1;

		actions.getNextMeeting({});
		actions.getData({
			selectUser:'',
			selectdate:tool.changeDateFormat(dates),//拼接日期格式为yyyy-MM-dd
			meetingType:0,
		});

	}
	componentWillReceiveProps(nextProps) {
		const keyOld = this.props.location.key;
		const keyNew = nextProps.location.key;
		const {type,date,meetingType} = nextProps;
		let dates = new Date();
		// let m = dates.getMonth()+1;
		// let value = objectAssign({},{
  //           selectUser:'',
  //           selectdate:date,
  //           meetingType:meetingType,//会议类型
  //       })
		//点击菜单路由刷新组件
		if(keyOld !== keyNew) {

		}
		if ((this.props.meetingType !== nextProps.meetingType) || (this.props.date !== nextProps.date)) {
			if(type == 1){
	            this.props.actions.getData({
					selectUser:'',
					selectdate:nextProps.date || tool.changeDateFormat(dates),
					meetingType:nextProps.meetingType,
				});
			}else{
				let value = objectAssign({},{
		            selectUser:'',
		            selectdate:date,
		            meetingType:meetingType,//会议类型
		        })
				this.props.actions.getCalendarList(value);
			}
        }
        if (this.props.date !== nextProps.date) {
        	if(type == 1){
	            this.props.actions.getData({
				selectUser:'',
				selectdate:nextProps.date || tool.changeDateFormat(dates),
				meetingType:nextProps.meetingType,
				});
	        }else{
	        	let value = objectAssign({},{
		            selectUser:'',
		            selectdate:date,
		            meetingType:meetingType,//会议类型
		        })
	        	this.props.actions.getCalendarList(value);
	        }
        }
		//设置页标题
		//		if(window.location.pathname.indexOf('/') >= 0 && nextProps.title && document.title !== nextProps.title)
		//			document.title = nextProps.title;
	}
	shouldComponentUpdate(nextProps, nextState) {
		//组件渲染控制
		//return this.props.title !== nextProps.title
		return true;
	}
	componentWillUnmount() {
		//组件卸载时一般清理一些状态

	}
	render() {
		const { loading, title,sessionkey,month,actions,orderFields,controlModal,createDate,meetingType,date,previewMeetingId,createMeetType,tabBase} = this.props;
		const btns = [
					 	(<Button type="primary" disabled={false} onClick={()=>{this.create()}}>新建会议</Button>),
					 	(<Button type="primary" disabled={true} onClick={()=>{}}>会议室使用情况</Button>)
				     ];
		return (
			<div className="metting-calview" id="metting-calview-id">
				<WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)} >
	            	<WeaTop 
	            	loading={loading} 
	            	icon={<i className='icon-coms-meeting' />} 
	            	iconBgcolor='#f14a2d' 
	            	title='会议日历'  
	            	buttons={btns}
		            showDropIcon={false}
	                dropMenuDatas={this.getRightMenu()}
	                onDropMenuClick={this.onRightMenuClick.bind(this)}
	            	>
	            	<div style={{overflow:'hidden',height:'100%'}}>
		            	
		            	<Row className="metting-calview-left-right" style={{minWidth:'1066px',height:'100%'}}>
			            	<RightContainer onChange={()=>{this.changeTab()}} all={this.props} />
			            	<LeftContainer month={month} loading={loading} sessionkey={sessionkey} all={this.props}>
			            		<CalViewTab all={this.props} month={month} />
			            	</LeftContainer>
			            	
		            	</Row>

	            	</div>
	            	</WeaTop>
	            </WeaRightMenu>
	            {/*<NewMeeting all={this.props} />*/}
	            <PreviewMeeting 
	            	meetingId = {previewMeetingId}
	            	onClose={(meetingid='')=>{actions.savePreviewMeetingId({meetingid:meetingid})}}
	            />
	            <CreateMeeting
	            	editDatas={tabBase}//编辑的默认值
	            	previewMeetingId = {previewMeetingId}//会议ID，编辑提交时用到
	            	createMeetType={createMeetType}//新建//编辑
	            	createDate={createDate}//默认当天，当天可不传，不要传空
	            	visible={controlModal}//显示true/关闭false
	            	onClose={(meetingid='',type="")=>{actions.controlModal(false);actions.getData({selectUser:'',selectdate:tool.changeDateFormat(date),meetingType:meetingType});}}//自定义的关闭组件方式，当点击X、草稿、提交按钮后抛出false,提交后会抛出meetingid
	             />
	            
            </div>
		)
	}
	create(){
		const {actions} = this.props;
		actions.controlModal(true,'',new Date());
	
	}
	changeTab(e){
		const {actions} = this.props;
	}
	getRightMenu(){
		
	}
	onRightMenuClick(){
		const {actions} = this.props;
	}

}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

CalView = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(CalView);

//form 表单与 redux 双向绑定
CalView = createForm({
	onFieldsChange(props, fields) {
		const oldFields = {...props.orderFields};
		props.actions.saveFields({ ...oldFields, ...fields });
	},
	mapPropsToFields(props) {
		return props.orderFields;
	}
})(CalView);


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	const { comsWeaTable } = state;
	let {tabBase,tabTopic,tabService} = state.previewMeeting;
	tabBase.Topic = tabTopic;
	tabBase.Service = tabService;
	return { ...state.meetingCalView,comsWeaTable ,tabBase}
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators({...CalViewAction,...PreviewMeetingAction,...WeaTableAction}, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CalView);

