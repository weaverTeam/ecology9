import {Row,Col,Switch,Tag,DatePicker,Tabs,Icon,Form,Input,TimePicker} from 'antd';
import { WeaBrowser,WeaTextarea,WeaCheckbox,WeaDatePicker,WeaTimePicker,WeaInput,WeaUpload,WeaSelect} from 'ecCom';
const FormItem = Form.Item;
import "./index.less"
import isEmpty from 'lodash/isEmpty';
import objectAssign from 'object-assign'
import classNames from 'classnames'
let _this = null;

import * as tool from '../../../calview/genericmethod/index'

class CreateMeetingGroup extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            showGroup: props.showGroup ? props.showGroup : false,
            uploadValue:[],
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    componentWillReceiveProps(nextProps) {
        const {newOrderFields,actions,form} = this.props;
        if (this.props.showGroup !== nextProps.showGroup) {
            this.setState({showGroup: nextProps.showGroup});
        }
    }
    render() {
        const {title,items,needTigger,newOrderFields,actions,caller,form,createMeetType} = this.props;
        const {showGroup} = this.state;
        var remindTypeNewValue = '';
        if(newOrderFields.remindTypeNew){
            remindTypeNewValue = newOrderFields.remindTypeNew.value;
            
        }
        const {getFieldProps} = form;
        const formItemLayout = {
          labelCol: { span: 7, offset:1 },
          wrapperCol: { span: 12, offset:1 },
        };

        return (
            <div className="wea-search-group">
                <Row className="wea-title">
                    <Col span="20">
                        <div>{title}</div>
                    </Col>
                    {needTigger &&
                        <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                            <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                        </Col>
                    }
                </Row>
                <Row className="wea-content" style={showGroup ? {} : {height:0}}>
                    <Form horizontal>
                    {
                        items && items.fieldlist.map((element,index)=>{

                            var otherparams={};
                            let style={
                                width:'250px',
                            };
                            let propsRequired = element.ismand;
                            let viewAttr = element.ismand==true?3:2;
                            if(createMeetType == 'edit' && (element.fieldname == 'address'||element.fieldname == 'customizeAddress')){//编辑的时候刚进入页面不会触发onchange，所以（address和customizeAddress联动）在这里判断是否必填
                                items.fieldlist.forEach((record)=>{
                                    if(record.fieldname == "address" && (record.fieldValue.length !=0)){
                                        propsRequired = false;
                                        viewAttr = 2;
                                    }
                                    if(record.fieldname == 'customizeAddress' && (record.fieldValue !='')){
                                        propsRequired = false;
                                        viewAttr = 2;
                                    }
                                })
                            }
                            var displayItem = true;
                            if(element.fieldname=="remindImmediately"||element.fieldname=="remindBeforeStart"||element.fieldname=="remindBeforeEnd"){
                                displayItem=remindTypeNewValue?true:false;//未选择提醒方式，以上三行不显示
                            }
                            if(element.fieldname=="repeatdays"||element.fieldname=="repeatweeks"||element.fieldname=="repeatmonths"||element.fieldname=="rptWeekDays"){
                                displayItem=remindTypeNewValue?true:false;
                            }
                            if(element.fieldname=="repeatdays" && newOrderFields['repeatType']){
                               displayItem= newOrderFields['repeatType'].value == 1 ?true :false;
                               propsRequired = newOrderFields['repeatType'].value == 1 ? true :false;
                            }
                            if((element.fieldname=="repeatweeks"||element.fieldname=="rptWeekDays")&& newOrderFields['repeatType']){
                               displayItem= newOrderFields['repeatType'].value == 2 ?true :false;
                               propsRequired = newOrderFields['repeatType'].value == 2 ? true :false;

                            }
                            if((element.fieldname=="repeatmonths"||element.fieldname=="repeatmonthdays")&& newOrderFields['repeatType']){
                               displayItem= newOrderFields['repeatType'].value == 3 ?true :false;
                               propsRequired = newOrderFields['repeatType'].value == 3 ? true :false;

                            }
                            if(element.fieldname=="remindHoursBeforeStart"||element.fieldname=="remindTimesBeforeStart"||element.fieldname=="remindHoursBeforeEnd"||element.fieldname=="remindTimesBeforeEnd"){
                                displayItem = false;
                            }
                            
                            let hasAddBtn = element.cfg && element.cfg.addbtn;
                            let addOnClick = element.fieldname=="meetingtype"?
                            "/meeting/Maint/MeetingTypeAdd.jsp?dialog=1":
                            element.fieldname=="address"?
                            "/meeting/Maint/MeetingRoomAdd.jsp?dialog=1&fromN=1"
                            :"";
                            let time = '';
                            if(element.fieldname=="begintime"&&newOrderFields.begintime){
                                time = newOrderFields.begintime.valueSpan;
                            }
                            if(element.fieldname=="endtime"&&newOrderFields.endtime){
                                time = newOrderFields.endtime.valueSpan;
                            }
                            
                            let nameProps =  getFieldProps(`${element.fieldname}`, {
                                rules: [{ required: propsRequired,message: '请输入' }]
                            });


                            if(element.fieldhtmltype==3 && element.fieldtype!=2 && element.fieldtype!=19){//浏览按钮
                               nameProps =  getFieldProps(`${element.fieldname}`, {
                                    rules: [{ required: propsRequired,message: '请输入' }],
                                    onChange:(ids, names, datas)=>{
                                        if(ids&&datas){
                                            let _datas = datas.map((a)=>{
                                                for(let i in a){
                                                    if(i=='id'||i=='name'){
                                                    }else{
                                                        delete a[i];
                                                    }
                                                }
                                                return a;
                                            })
                                            // let newOrderFieldsCopy = {...newOrderFields};

                                            newOrderFields[element.fieldname] = {
                                                    id:ids,
                                                    rep:_datas,
                                                    value:ids,
                                                    isbroser:true,
                                                }
                                            if(element.fieldname != 'meetingtype'){
                                                this.props.actions.saveFields(newOrderFields)
                                                
                                            }
                                        }
                                    }
                                }) 
                            }
                            let date = '';
                            if(element.fieldname=="begindate"&&newOrderFields.begindate){
                                date = newOrderFields.begindate.value;
                            }
                            if(element.fieldname=="enddate"&&newOrderFields.enddate){
                                date = newOrderFields.enddate.value;
                            }
                            if(element.fieldname=="repeatbegindate"&&newOrderFields.repeatbegindate){
                                date = newOrderFields.repeatbegindate.value;
                            }
                            if(element.fieldname=="repeatenddate"&&newOrderFields.repeatenddate){
                                date = newOrderFields.repeatenddate.value;
                            }
                            if(element.fieldname=="begindate" ||element.fieldname=="enddate"||element.fieldname=="repeatbegindate" ||element.fieldname=="repeatenddate")
                                nameProps =  getFieldProps(`${element.fieldname}`, {
                                    rules: [{ required: propsRequired,message: '请输入' }],
                                    getValueFromEvent:(date, dateString) => dateString
                            })
                            // let am = Math.floor((Math.random()*10));
                            if(element.fieldname=="begintime" ||element.fieldname=="endtime") {
                                nameProps =  getFieldProps(`${element.fieldname}`, {
                                    rules: [{ required: propsRequired,message: '请输入' }],
                                    getValueFromEvent:(date, dateString) => dateString
                                });
                            }

                            let browerparams = element.fieldhtmltype == 3?element.cfg.browcfg:'';//浏览按钮
 
                            const req = classNames({
                                'required': element.ismand && newOrderFields[element.fieldname]&& !newOrderFields[element.fieldname].value,
                            });
                            let options = [];
                            if(element.fieldname=="repeatType"){
                               options = [{key: "1",showname: "按天重复"},{key: "2", showname: "按周重复"},{key: "3", showname: "按月重复"}]
                                
                            }
                            if(element.fieldname=="repeatStrategy"){
                               options = [{key: "0",showname: "非工作日时：日期不变"},{key: "1", showname: "非工作日时：推迟到下一工作日"},{key: "2", showname: "非工作日时：取消会议"}]
                                
                            }
                           
                            if(element.fieldhtmltype==3 && element.fieldtype!=2 && element.fieldtype!=19){//浏览按钮
                                    replaceDatas=[];
                                if(newOrderFields[element.fieldname]){
                                    replaceDatas = newOrderFields[element.fieldname].rep//浏览按钮处理方式
                                }
                            }
                            let obj = {}
                            if(element.fieldhtmltype==5){
                                options = element.cfg.items;
                            }
                            let checked =false;
                            if (element.fieldname == 'remindImmediately'&& newOrderFields[element.fieldname]){
                                checked = newOrderFields[element.fieldname].value;
                            }
                            if(element.fieldname == 'remindImmediately'){
                            }
                            let   defaultUploadValue=[];
                            if(element.fieldhtmltype==6){
                                if(newOrderFields[element.fieldname] && newOrderFields[element.fieldname].rep != undefined){
                                    defaultUploadValue = newOrderFields[element.fieldname].rep//附件上传
                                }
                            }
                            if(element.fieldhtmltype==6){
                            }

                            return (
                                <FormItem
                                  {...formItemLayout}
                                  label={`${element.fieldlabel}`}
                                  style={{display:displayItem?'block':'none',marginBottom:'15px'}}
                                >
                                    {element.fieldhtmltype==1?
                                        <WeaInput {...nameProps} viewAttr={viewAttr} hasBorder style={style}/>
                                    :element.fieldhtmltype==2?
                                        <WeaTextarea {...nameProps} style={style}/>
                                    :element.fieldhtmltype==3 && element.fieldtype!=2 && element.fieldtype!=19?
                                        <WeaBrowser { ...browerparams } hasBorder {...nameProps}  replaceDatas={replaceDatas} hasAddBtn={hasAddBtn} addOnClick ={()=>{window.open(addOnClick)}} viewAttr={viewAttr} inputStyle={style}/>
                                    :element.fieldhtmltype==3 && element.fieldtype==2?
                                        <DatePicker className={`${req}`} style={style} {...nameProps} value={date} />
                                    :element.fieldhtmltype==3 && element.fieldtype==19?
                                        <TimePicker className={`${req}`} style={style} {...nameProps} value={time} format="HH:mm"/>
                                    :element.fieldhtmltype==4 && element.fieldname =="remindImmediately"?
                                        <Switch {...nameProps}  checked = {checked} />
                                    :element.fieldhtmltype==4 && element.fieldname !=="remindImmediately"?  
                                        <WeaCheckbox {...nameProps} />
                                    :element.fieldhtmltype==5?
                                        <WeaSelect {...nameProps} viewAttr={viewAttr} options={options} style={style}/>
                                    :element.fieldhtmltype==6 && !isEmpty(element.cfg)?
                                        <WeaUpload

                                            
                                            maxUploadSize={element.cfg.maxSize}//最大上传大小限制，单位为MB
                                            btnSize="default"//按钮大小
                                            uploadUrl={`${element.cfg.uploadUrl}?category=${element.cfg.category}`}//文件上传服务器接口地址
                                            category={`${element.cfg.category}`}//文件上传目录
                                            onUploading={()=>{}}//文件上传状态接收函数
                                            datas = {defaultUploadValue}
                                            onChange={(ids,list)=>{
                                                // console.log('idslist',ids,list,newOrderFields[element.fieldname])
                                                let newOrderFieldsCopy = {...newOrderFields};
                                                newOrderFieldsCopy[element.fieldname] = {
                                                    name:element.fieldname,
                                                    value:ids,
                                                    rep:list,
                                                }

                                                // this.setState({uploadValue:list})
                                            this.props.actions.saveFields(newOrderFieldsCopy)

                                            }}
                                            ref={'newmeetingupload'}
                                            uploadId={"_newmeetingupload"}//上传组件标识，同时生成隐藏域 id
                                            style={{display: "inline-block", padding: "0"}}
                                            autoUpload={true}//自动上传
                                        />
                                    :
                                        <div style={{display:'inline-block'}}>
                                            <span style={{color:'#FF0000'}}>附件存放目录未设置！</span>
                                            <span style={{marginLeft:'20px'}}><a href="/meeting/Maint/MeetingSet.jsp" target="_blank">点此设置</a></span>
                                        </div>
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart"?
                                        <span>开始前</span>:
                                        element.fieldname=="remindBeforeEnd"?
                                        <span>结束前</span>:''
                                    }

                                    {
                                        element.fieldname=="remindBeforeStart"?
                                        <WeaInput {...getFieldProps('remindHoursBeforeStart')} viewAttr={viewAttr} style={{width:'50px'}} />:
                                        element.fieldname=="remindBeforeEnd"?
                                        <WeaInput {...getFieldProps('remindHoursBeforeEnd')} viewAttr={viewAttr} style={{width:'50px'}} />:''
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart" || element.fieldname=="remindBeforeEnd"?
                                        <span>小时</span>:''

                                    }
                                    {
                                        element.fieldname=="remindBeforeStart"?
                                        <WeaInput {...getFieldProps('remindTimesBeforeStart')} viewAttr={viewAttr} style={{width:'50px'}} />:
                                        element.fieldname=="remindBeforeEnd"?
                                        <WeaInput {...getFieldProps('remindTimesBeforeEnd')} viewAttr={viewAttr} style={{width:'50px'}} />:''
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart" || element.fieldname=="remindBeforeEnd"?
                                        <span>分钟</span>:''
                                    }
                                    
                                </FormItem>
                               
                            )
                        })
                    }
                    </Form>

                </Row>
            </div>
        )
    }

}

export default CreateMeetingGroup




