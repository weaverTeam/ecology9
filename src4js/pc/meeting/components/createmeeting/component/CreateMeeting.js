import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as CreatemeetingAction from '../../../actions/createMeeting'
import  * as CreateMeetingApi from '../../../apis/createMeeting'

import {Row,Col,Switch,Tag,DatePicker,Popover,Modal,Button,Tabs,Form,message} from 'antd';
const confirm = Modal.confirm;
const errorModal = Modal.error;
const createForm = Form.create;
const MonthPicker = DatePicker.MonthPicker;
const TabPane = Tabs.TabPane;
import { WeaDialog,WeaButton,WeaTableEdit,WeaCheckbox,WeaTools,WeaErrorPage,WeaNewScroll} from 'ecCom';
import objectAssign from 'object-assign'
import "./index.less"
import CreateMeetingGroup from "./createmeetinggroup/index.js"
let _this = null;
import mapKeys from 'lodash/mapKeys';
import mapValues from 'lodash/mapValues';
import isEmpty from 'lodash/isEmpty';
import * as tool from '../../calview/genericmethod/index'
//import { } from 'comsRedux'

class CreateMeeting extends React.Component {
	// static contextTypes = {
	// 	router: PropTypes.routerShape
	// }
	constructor(props) {
		super(props);
        _this= this;
        this.state = {
            showDrop: false,

        }
        this.changeTab=this.changeTab.bind(this);
	}
	componentDidMount() {
		//一些初始化请求

	}
	componentWillReceiveProps(nextProps) {
		// const keyOld = this.props.location.key;
		// const keyNew = nextProps.location.key;
		//点击菜单路由刷新组件
		// if(keyOld !== keyNew) {

		// }
		//设置页标题
		//		if(window.location.pathname.indexOf('/') >= 0 && nextProps.title && document.title !== nextProps.title)
		//			document.title = nextProps.title;
        let {editDatas='',createMeetType='create'} = nextProps;
		let { actions } = this.props;
        let fields = {};
        if(this.props.visible!=nextProps.visible){
            if(nextProps.visible && createMeetType=='create'){
				let date = new Date();
				let {createDate=date,createType='',begintime='00:00',endtime='23:59',address={}} = nextProps;
				actions.getMeetingBaseField({isInterval:createType?1:''});
				actions.getMeetingTopicField();
				actions.getMeetingServiceField();
				if(typeof(createDate)!='string'){
		        	createDate = tool.changeDateFormat(createDate);
				}
				fields = createType=='isInterval'?
				{
		            
		            repeatType:{name:'repeatType',value:'1'},
		            repeatStrategy:{name:'repeatStrategy',value:'0'},
		            repeatbegindate:{name:"repeatbegindate",value:createDate},
		            repeatenddate:{name:"repeatenddate",value:createDate},
		            begintime:{name:'begintime',valueSpan:begintime,value:begintime},
		            endtime:{name:'endtime',valueSpan:endtime,value:endtime},
			    }
				:
				{	
		            begindate:{name:'begindate',value:createDate},
		            enddate:{name:'enddate',value:createDate},
		            begintime:{name:'begintime',valueSpan:begintime,value:begintime},
		            endtime:{name:'endtime',valueSpan:endtime,value:endtime},
			    }
			    if(!isEmpty(address)){
			    	fields = objectAssign(fields,{
						address:{name:'address',value:address.id,valueSpan:address.name}
			    	})
			    }
				actions.saveFields(fields);
			}

            if(nextProps.visible && createMeetType=='edit'){
                actions.controlDatas(editDatas);//受控datas
                editDatas.datas.forEach((element)=>{
                    element.fieldlist.forEach((e)=>{
                        if(typeof(e.fieldValue) ==="object"){
                            if(e.fieldhtmltype==3 && e.fieldtype!=2 && e.fieldtype!=19){
                                if(e.fieldValue.length > 0){
                                    let arrId = [];
                                    let arrName = [];
                                    let strVaule = '';
                                    let strVauleSpan = '';
                                    e.fieldValue.forEach((element)=>{
                                        arrId.push(element['id']);
                                        arrName.push(element['name']);
                                    })
                                    strVaule = arrId.join(",");//3,4,5
                                    strVauleSpan = arrName.join(",");//3,4,5
                                    fields = objectAssign(fields,{
                                        [`${e.fieldname}`]:{name:e.fieldname,rep:e.fieldValue,value:strVaule,valueSpan:strVauleSpan}
                                    })
                                }else{
                                    fields = objectAssign(fields,{
                                        [`${e.fieldname}`]:{name:e.fieldname,rep:e.fieldValue,value:undefined,valueSpan:undefined}
                                    })
                                }
                            }else if(e.fieldhtmltype==3 && (e.fieldtype==2 || e.fieldtype==19)){
                                fields = objectAssign(fields,{
                                    [`${e.fieldname}`]:{name:e.fieldname,value:e.fieldValue[0].id,valueSpan:e.fieldValue[0].name}
                                })
                            }else if(e.fieldhtmltype==6){//附件
                                if(e.fieldValue.length > 0){
                                let arrId = [];
                                let arrName = [];
                                let strVaule = '';
                                let strVauleSpan = '';
                                e.fieldValue.forEach((element)=>{
                                        arrId.push(element['fileid']);
                                        arrName.push(element['filename']);
                                    })
                                strVaule = arrId.join(",");
                                strVauleSpan = arrName.join(",");
                                fields = objectAssign(fields,{
                                    [`${e.fieldname}`]:{name:e.fieldname,rep:e.fieldValue,value:strVaule,valueSpan:strVauleSpan}
                                })
                                }else{
                                    fields = objectAssign(fields,{
                                        [`${e.fieldname}`]:{name:e.fieldname,rep:e.fieldValue,value:undefined,valueSpan:undefined}
                                    })
                                }
                            }
                        }else{
                            let fieldsValue = e.fieldValue;
                            if(e.fieldname == "remindImmediately"){
                                fieldsValue = e.fieldValue == 1 ? true :false;
                            }
                            fields = objectAssign(fields,{
                                [`${e.fieldname}`]:{name:e.fieldname,value:fieldsValue,valueSpan:fieldsValue}
                            })
                        }
                    })
                });

                actions.saveFields(fields);
            }
		}
		if(nextProps.meetingid && this.props.meetingid !=nextProps.meetingid ){
			nextProps.onClose(nextProps.meetingid,nextProps.savetype);
		}
        if(nextProps.errorMessage && this.props.errorMessage != nextProps.errorMessage){
            message.error(`${nextProps.errorMessage}`,5);
        }
	}
	shouldComponentUpdate(nextProps, nextState) {
		//组件渲染控制
		//return this.props.title !== nextProps.title
		return true
	}
	componentWillUnmount() {
		//组件卸载时一般清理一些状态
		const { actions } = this.props;
		actions.controlDatas();//清空datas
	}
	render() {
		const {showDrop,showErrorMessage} = this.state;
        const {savetype,editDatas,visible,actions,datas,topicColumns,topicDatas,serviceColumns,serviceDatas,form,newmeetingTab,onClose,meetingid,newOrderFields,caller,createMeetType ='create'} = this.props;
        const {resetFields} = form;
        let buttons = [
            <Button key="draft" type="primary" size="large"  onClick={this.handleSubmit.bind(this,'draft')} >草 稿</Button>,
            <Button key="sub" type="primary" size="large"  onClick={this.handleSubmit.bind(this,'submit')} >提 交</Button>
            ];
        if(createMeetType == 'edit'){
           buttons = [
            <Button key="save" type="primary" size="large"  onClick={this.handleSubmit.bind(this,'save')} >保 存</Button>
           ] 
        }


        let columns_topicField = topicColumns==-1?'':topicColumns;
        let datas_topicField = topicDatas==-1?'':topicDatas;
        let columns_serviceField = serviceColumns==-1?'':serviceColumns;
        let datas_serviceField = serviceDatas==-1?'':serviceDatas;
        let dialogTitle = createMeetType=="create"?"新建会议":"编辑会议";
        if(createMeetType == 'edit'&& editDatas){
            let countService = 100/editDatas.Service.columns.length;
            columns_serviceField = editDatas.Service.columns.map((x)=>{
                x['width']=`${countService}%`
                return x;
            });
            datas_serviceField = editDatas.Service.datas;
            let countTop = 100/editDatas.Topic.columns.length;
            columns_topicField = editDatas.Topic.columns.map((x)=>{
                x['width']=`${countTop}%`
                return x;
            });
            datas_topicField = editDatas.Topic.datas;

        }
		return (
			<WeaDialog
                    zIndex={801}
                    visible={visible}
                    title={dialogTitle}
                    className="createMeeting"
                    icon="icon-coms-meeting"
                    iconBgcolor="#f14a2d"
                    buttons={buttons}
                    style={{width:'790px',height:'500px'}}
                    onCancel={()=>{onClose(false);resetFields();actions.changeNewMeetingTab(1)}}
                >
                    <div className="newmeeting-body">
                        <WeaNewScroll height='500px'>
                            <Tabs activeKey={`${newmeetingTab}`} onChange={this.changeTab} >
                                <TabPane tab="会议信息" key="1">
                                    {
                                        datas.datas && (datas.datas.map((element,index,array)=>{
                                        	return <CreateMeetingGroup createMeetType={createMeetType} needTigger={true} title={element.grouplabel} form={this.props.form} showGroup={true} items={element} newOrderFields={newOrderFields} actions={actions} caller={caller}/>
                                        }))
                                    }
                                    
                                </TabPane>
                                
                                <TabPane tab="会议议程" key="2" disabled={columns_topicField==-1?true:false} >
                                    <WeaTableEdit
                                        title="议程列表"
                                        datas={datas_topicField}
                                        columns={columns_topicField}
                                        onChange={this.onChange.bind(this,'topic')}
                                        viewAttr={2}
                                    />
                                </TabPane>
                                
                                
                                <TabPane tab="会议服务" key="3" disabled={serviceColumns==-1?true:false} >
                                    <WeaTableEdit
                                        title="服务列表"
                                        datas={datas_serviceField}
                                        columns={columns_serviceField}
                                        onChange={this.onChange.bind(this,'service')}
                                        viewAttr={2}
                                    />
                                </TabPane>
                                
                            </Tabs>
                        </WeaNewScroll>
                    </div>
                    
                </WeaDialog>
		)
	}


	onChange(type,datas,column){
        let servicerows = '';
        let topicrows = '';
        const {actions,newOrderFields} = _this.props;
        let each = {};
        let eachvalue = {};
        let alldatas={};
        datas.forEach((element,index)=>{
            each = mapKeys(element,(value, key)=>{
                return key + '_' + (index+1);
            });
            eachvalue = mapValues(each, (e,i)=>{
              return {
                value:e,
                name:i,
              };
            });
            alldatas = objectAssign(alldatas,eachvalue);
        });
        if(type=='topic'){
            topicrows = datas.length;
            alldatas = objectAssign(alldatas,{
                topicrows:{
                    value:topicrows||0,
                    name:'topicrows'
                }
            });
            actions.getMeetingTopicField(datas);

        }
        if(type=='service'){
            servicerows = datas.length;
            alldatas = objectAssign(alldatas,{
                servicerows:{
                    value:servicerows||0,
                    name:'servicerows'
                }
            });
            // datas.forEach((e,index,array)=>{
            //     for(let i in e){
            //         if(e[i]=''){
            //             delete e;
            //         }
            //    }
            // })
            actions.getMeetingServiceField(datas);
        }
        alldatas = objectAssign(newOrderFields,alldatas);
        actions.saveFields(alldatas);
    }
    changeTab(e){
        const {actions} = _this.props;
        actions.changeNewMeetingTab(e);
    }
    handleSubmit(type) {
        const {newOrderFields,form,actions,onClose,createType = '',meetingid,createMeetType = 'create',previewMeetingId,serviceKeycol} = this.props
        const {resetFields,getFieldsValue} = form;
        let neworderFieldsParam = null;
        // startUploadAll();//附件批量上传
        form.validateFields((errors, values) => {
            console && console.error(errors);
        for(let key in errors){
            if(key !== 'accessorys' && key !== "remindImmediately"){
                return;
            }
        } 


        // ReportApi.getReportTable(params).then(result => {
        //     this.setState({
        //         loading: false,
        //         data: result.tableInfo
        //     });
        // });






           neworderFieldsParam =  mapValues(newOrderFields, 'value');

            if(type=='draft'){
                neworderFieldsParam = objectAssign(neworderFieldsParam,{
                    method:'save',
                })
            }
            if(type=='submit'){
                neworderFieldsParam = objectAssign(neworderFieldsParam,{
                     method:'',
                })
            }
            if(createType=='isInterval'){
                neworderFieldsParam = objectAssign(neworderFieldsParam,{
                     isInterval:1,
                })
            }
            if(createType==''){
                neworderFieldsParam = objectAssign(neworderFieldsParam,{
                     isInterval:'',
                })
            }
            for(let key in neworderFieldsParam){
                if(neworderFieldsParam[key] == undefined ){
                    delete neworderFieldsParam[key];
                }
            }
            // let keycol = tabService.keycol;
            // neworderFieldsParam.serviceitems = {};
            // tabService.datas.forEach((element,index,array)=>{
            //     for(let key in element){
            //         if(key == keycol){
            //             params.serviceitems[`${keycol}_${index}`] = element[keycol]
            //         }
            //     }
            // })
            let params = objectAssign({},neworderFieldsParam);
            // params.serviceitems = {};
            let servicearr = [];
            // let servicestr = '';

            for(let key in neworderFieldsParam){
                if(key.indexOf(`${serviceKeycol}_`)== 0){
                    servicearr.push(neworderFieldsParam[key]);
                }
            }
            let servicearrcopy = servicearr.filter((e,index,self)=>{
                return self.indexOf(e) == index;
            })
           
            params.serviceitems = servicearrcopy.join(',');
            if(createMeetType == 'create' && createType != 'isInterval' && type != 'draft'){
                type=='submit' && CreateMeetingApi.chkMeetingP(params).then((result)=>{
                    let conflictType =  '会议室';//冲突类型
                    let conMsg = '';
                    let conflictMsg = '';//提示内容
                    if(!result.address.chkstatus){
                        conMsg = result.address.msg.map((e)=>{
                                        return <div>{`${e}冲突`}</div>
                                    });
                        conflictMsg = (
                                <div>
                                    {conMsg}
                                    {
                                        <span>是否继续申请？</span>
                                    }
                                </div>
                            )
                        if(result.address.cansub == true){//冲突了是否可提交，true是可提交
                            confirm({
                                title:"会议室冲突",
                                content: conflictMsg,
                                onOk() {
                                    if(!result.member.chkstatus){
                                        let cont = result.member.msg.hrms.map((e)=>{
                                            return <div>{e}</div>
                                        })
                                        let contcrms = result.member.msg.crms.map((e)=>{
                                            return <div>{e}</div>
                                        })
                                        conflictMsg = (
                                            <div>
                                            {
                                                result.member.msg.hrms.length>0?'人员：':''
                                            }
                                            {cont}
                                            {
                                                result.member.msg.crms.length>0?'客户：':''
                                            }
                                            {contcrms}
                                            {
                                                <span>是否继续申请？</span>
                                            }
                                            </div>
                                            )


                                        if(result.member.cansub == true){
                                            confirm({
                                                title:"参会人员冲突",
                                                content: conflictMsg,
                                                onOk() {
                                                    if(!result.service.chkstatus){
                                                        conMsg = result.service.msg.map((e)=>{
                                                            return <div>{`${e}冲突`}</div>
                                                        })
                                                        conflictMsg = (
                                                            <div>
                                                                {conMsg}
                                                                {
                                                                    <span>是否继续申请？</span>
                                                                }
                                                            </div>
                                                        )

                                                        if(result.service.cansub == true){
                                                            confirm({
                                                                title:"会议服务冲突",
                                                                content: conflictMsg,
                                                                onOk() {
                                                                    actions.newMeeting(neworderFieldsParam,type);
                                                                    actions.saveFields({});
                                                                },
                                                                onCancel() {},
                                                            });
                                                        }else{
                                                            errorModal({
                                                                title: "会议服务冲突不能提交",
                                                                content: conMsg,
                                                            });
                                                        }

                                                    }else{
                                                        actions.newMeeting(neworderFieldsParam,type);
                                                        actions.saveFields({});
                                                    }
                                                },
                                                onCancel() {},
                                            });    
                                        }else{
                                            errorModal({
                                                title: "参会人员冲突不能提交",
                                                content: conflictMsg,
                                            });
                                        }
                                    }else if(!result.service.chkstatus){
                                                conMsg = result.service.msg.map((e)=>{
                                                            return <div>{`${e}冲突`}</div>
                                                        })
                                                conflictMsg = (
                                                            <div>
                                                                {conMsg}
                                                                {
                                                                    <span>是否继续申请？</span>
                                                                }
                                                            </div>
                                                        )
                                            confirm({
                                                title:"会议服务冲突",
                                                content: conflictMsg,
                                                onOk() {

                                                    actions.newMeeting(neworderFieldsParam,type);
                                                    actions.saveFields({});
                                                },
                                                onCancel() {},
                                            });
                                    }else{
                                        actions.newMeeting(neworderFieldsParam,type);
                                        actions.saveFields({});
                                    }
                                },
                                onCancel() {},
                            });
                        }else{
                            errorModal({
                                title: "会议室冲突不能提交",
                                content: conMsg,
                            });
                        }

                    }else if(!result.member.chkstatus){
                        //tanchuconfirm
                        let cont = result.member.msg.hrms.map((e)=>{
                            return <div>{e}</div>
                        })
                        let contcrms = result.member.msg.crms.map((e)=>{
                            return <div>{e}</div>
                        })
                        conflictMsg = (
                            <div>
                            {
                                result.member.msg.hrms.length>0?'人员：':''
                            }
                            {cont}
                            {
                                result.member.msg.crms.length>0?'客户：':''
                            }
                            {contcrms}
                            {
                                <span>是否继续申请？</span>
                            }
                            </div>
                            )

                        if(result.member.cansub == true){
                            confirm({
                                title:"参会人员冲突",
                                content: conflictMsg,
                                onOk() {
                                    if(!result.service.chkstatus){
                                        conMsg = result.service.msg.map((e)=>{
                                                            return <div>{`${e}冲突`}</div>
                                                        })
                                        conflictMsg = (
                                                            <div>
                                                                {conMsg}
                                                                {
                                                                    <span>是否继续申请？</span>
                                                                }
                                                            </div>
                                                        )
                                        if(result.service.cansub == true){
                                            confirm({
                                                title:"会议服务冲突",
                                                content: conflictMsg,
                                                onOk() {
                                                    actions.newMeeting(neworderFieldsParam,type);
                                                    actions.saveFields({});
                                                },
                                                onCancel() {},
                                            });
                                        }else{
                                            errorModal({
                                                title: "会议服务冲突不能提交",
                                                content: conMsg,
                                            });
                                        }

                                    }else{
                                        actions.newMeeting(neworderFieldsParam,type);
                                        actions.saveFields({});
                                    }
                                },
                                onCancel() {},
                            });    
                        }else{
                            errorModal({
                                title: "参会人员冲突不能提交",
                                content: conflictMsg,
                            });
                        }

                    }else if(!result.service.chkstatus){
                        //tanchuconfirm
                        conMsg = result.service.msg.map((e)=>{
                                    return <div>{`${e}冲突`}</div>
                                })
                        conflictMsg = (
                                        <div>
                                            {conMsg}
                                            {
                                                <span>是否继续申请？</span>
                                            }
                                        </div>
                                    )
                        if(result.service.cansub == true){
                            confirm({
                                title:"会议服务冲突",
                                content: conflictMsg,
                                onOk() {
                                    actions.newMeeting(neworderFieldsParam,type);
                                    actions.saveFields({});
                                },
                                onCancel() {},
                            });
                        }else{
                            errorModal({
                                title: "会议服务冲突不能提交",
                                content: conMsg,
                            });
                        }
                    }else{
                        //chkstatus both true
                        actions.newMeeting(neworderFieldsParam,type);
                        actions.saveFields({});
                    }



                    // if(result.chkstatus == true){//是否冲突，true是不冲突
                    //     actions.newMeeting(neworderFieldsParam,type);
                    //     // actions.changeNewMeetingTab(1);
                    //     actions.saveFields({});
                    // }else{
                    //     var conflictType = result.type == 'address' ? '会议室': result.type == 'member' ? '参会人员' : '会议服务';//冲突类型
                    //     var conflictMsg = '';//冲突提示
                    //     if(result.type == 'address'){
                    //         conflictMsg = `${result.msg[0]}使用冲突，是否继续申请？`
                    //     }
                    //     if(result.type == 'service'){
                    //         conflictMsg = `${result.msg[0]}使用冲突，是否继续申请？`
                    //     }
                    //     if(result.type == 'member'){
                    //             if(result.msg.hrms.length>0||result.msg.crms.length>0){
                    //                 let cont = result.msg.hrms.map((e)=>{
                    //                     return <div>{e}</div>
                    //                 })
                    //                 let contcrms = result.msg.crms.map((e)=>{
                    //                     return <div>{e}</div>
                    //                 })
                    //                 conflictMsg = (
                    //                     <div>
                    //                     {
                    //                         result.msg.hrms.length>0?'人员：':''
                    //                     }
                    //                     {cont}
                    //                     {
                    //                         result.msg.crms.length>0?'客户：':''
                    //                     }
                    //                     {contcrms}
                    //                     {
                    //                         <span>是否继续申请？</span>
                    //                     }
                    //                     </div>
                    //                     )
                    //             }
                    //     }
                    //     if(result.cansub == true){//冲突了是否可提交，true是可提交
                    //         confirm({
                    //             title:`${conflictType}冲突`,
                    //             content: conflictMsg,
                    //             onOk() {
                    //                 actions.newMeeting(neworderFieldsParam,type);
                    //                 // actions.changeNewMeetingTab(1);
                    //                 actions.saveFields({});
                    //             },
                    //             onCancel() {},
                    //         });
                    //     }else{
                    //         errorModal({
                    //             title: `${conflictType}冲突不能提交`,
                    //             content: `${conflictMsg}`,
                    //         });
                    //     }
                    // }
                })

            }else if(createMeetType == 'create' && createType == 'isInterval'|| type=='draft'){
                actions.newMeeting(neworderFieldsParam,type);
                // actions.changeNewMeetingTab(1);
                actions.saveFields({});
            }else if(createMeetType == 'edit'){

                neworderFieldsParam = objectAssign(neworderFieldsParam,{
                     meetingid:previewMeetingId,
                })
                actions.editMeeting(neworderFieldsParam,type);
                actions.changeNewMeetingTab(1);
                onClose(false);
            }

            // actions.changeNewMeetingTab(1);
            // actions.saveFields({});
            // onClose(false,meetingid)

            // resetFields();//情空表单
        });
    }





}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

CreateMeeting = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(CreateMeeting);

CreateMeeting = createForm({
	onFieldsChange(props, fields) {
		const oldFields = {...props.newOrderFields};
		const {actions,createMeetType = 'create'} = _this.props;
    		if (fields.customizeAddress && fields.customizeAddress.value) {
    			if (!isEmpty(oldFields) && oldFields.address) {
    				delete oldFields.address;
                    props.datas.datas[0].fieldlist.forEach((element)=>{
                        if(element.fieldname == "address"){
                            element.ismand = false;
                        }
                    })
    			}
    		}
    		if (fields.address && fields.address.value ) {
    			if (!isEmpty(oldFields) && oldFields.customizeAddress) {
    				delete oldFields.customizeAddress;
                    props.datas.datas[0].fieldlist.forEach((element)=>{
                        if(element.fieldname == "customizeAddress"){
                            element.ismand = false;
                        }
                    })
                    props.actions.saveFields({ ...oldFields, ...fields });//address和customaddress联动
    			}
    		}


    		if (fields.customizeAddress && fields.customizeAddress.value) {
    			// props.datas.datas[0].fieldlist[4].ismand = false;

                props.datas.datas[0].fieldlist.forEach((element)=>{
                    if(element.fieldname == "address"){
                        element.ismand = false;
                    }
                })

    		}
    		if (fields.address && fields.address.value ) {
    			// props.datas.datas[0].fieldlist[5].ismand = false;
                props.datas.datas[0].fieldlist.forEach((element)=>{
                    if(element.fieldname == "customizeAddress"){
                        element.ismand = false;
                    }
                })
    		}
    		if (fields.customizeAddress && fields.customizeAddress.value =='') {
    			// props.datas.datas[0].fieldlist[4].ismand = true;
                props.datas.datas[0].fieldlist.forEach((element)=>{
                    if(element.fieldname == "address"){
                        element.ismand = true;
                    }
                })
    		}
    		if (fields.address && fields.address.value =='') {
    			// props.datas.datas[0].fieldlist[5].ismand = true;
                props.datas.datas[0].fieldlist.forEach((element)=>{
                    if(element.fieldname == "customizeAddress"){
                        element.ismand = true;
                    }
                })
    		}
            if(!isEmpty(fields.meetingtype)&&fields.meetingtype.value){
                if(!oldFields.meetingtype || (oldFields.meetingtype&&(oldFields.meetingtype.vaule!=fields.meetingtype.value))){
                    let coldFields = {...oldFields};
                    actions.changeBroserMeetingType({
                            oldFields:coldFields,
                            caller:(oldFields.caller&&oldFields.caller.value)||'',
                            meetingtype:fields.meetingtype.value,
                        })
                }
            }
            if(fields.remindImmediately){
                fields.remindImmediately.value = fields.remindImmediately.value == true ? 1: 0;
            }
    		// if(((isEmpty(oldFields.meetingtype)||((oldFields.meetingtype)&&(!oldFields.meetingtype.value))))||
    		// ((!isEmpty(oldFields.meetingtype))&&(!isEmpty(fields.meetingtype)))){
    		// 	//会议类型切换获取默认召集人
    		// 	let coldFields = {...oldFields, ...fields};
    		// 	actions.changeBroserMeetingType({
    		// 	 		oldFields:coldFields,
      //                   caller:(oldFields.caller&&oldFields.caller.value)||'',
      //                   meetingtype:fields.meetingtype.value,
      //               })
    		// }
            // fields.isnotbroser
            let paramType = '',oldparamType = '';
            for(let key in fields){
                paramType = fields[key].isbroser;
                oldparamType = oldFields[key]?oldFields[key].isbroser:false;
            }
    		if(paramType||oldparamType){//浏览按钮不在这保存表单除了address
            }else{
    			props.actions.saveFields({ ...oldFields, ...fields });

            }


	},
	mapPropsToFields(props) {
		return props.newOrderFields;
	}
})(CreateMeeting);



// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	const { loading, title } = state.createMeeting;
	return { ...state.createMeeting }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(CreatemeetingAction, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateMeeting);



