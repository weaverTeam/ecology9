import {Row,Col,Switch,Tag,DatePicker,Tabs,Icon,Form,Input,TimePicker} from 'antd';
import { WeaBrowser,WeaTextarea,WeaCheckbox,WeaDatePicker,WeaTimePicker,WeaInput,WeaUpload,WeaSelect} from 'ecCom';
const FormItem = Form.Item;
import "./index.less"
import isEmpty from 'lodash/isEmpty';
let _this = null;

import * as tool from '../../calview/genericmethod/index'

class PreviewMeetingGroup extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            showGroup: props.showGroup ? props.showGroup : false,
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.showGroup !== nextProps.showGroup) {
            this.setState({showGroup: nextProps.showGroup});
        }
    }
    


    render() {
        const {title,items,needTigger} = this.props;
        const {showGroup} = this.state;
        // var remindTypeNewValue = '';
        // if(orderFields.remindTypeNew){
        //     remindTypeNewValue = orderFields.remindTypeNew.value;
            
        // }
       
        const formItemLayout = {
          labelCol: { span: 7, offset:1 },
          wrapperCol: { span: 12, offset:1 },
        };
        let testa = ''
        return (
            <div className="wea-search-group">
                <Row className="wea-title">
                    <Col span="20">
                        <div>{title}</div>
                    </Col>
                    {needTigger &&
                        <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                            <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                        </Col>
                    }
                </Row>
                <Row className="wea-content" style={showGroup ? {} : {height:0}}>
                    <Form horizontal>
                    {
                        items && items.fieldlist.map((element,index)=>{

                            var otherparams={};
                            let defaultValue ='';
                            let style={
                                width:'250px',
                            };
                            var displayItem = true;
                            // if(element.fieldname=="remindImmediately"||element.fieldname=="remindBeforeStart"||element.fieldname=="remindBeforeEnd"){
                                // displayItem=remindTypeNewValue?true:false;

                            // }
                            let replaceDatas = [];
                            //查看的默认值
                            if(!isEmpty(element.fieldValue)){
                                if(element.fieldhtmltype == 3){
                                    if(element.fieldtype!=2&&element.fieldtype!=19){
                                        replaceDatas = element.fieldValue;
                                    }
                                    if(element.fieldtype==2||element.fieldtype==19){
                                        defaultValue =  element.fieldValue[0].name;
                                    }
                                }
                                if(element.fieldhtmltype == 1||element.fieldhtmltype == 2||element.fieldhtmltype == 5){//新增=5下拉框
                                    defaultValue =  element.fieldValue;
                                }
                            }
                            if(element.fieldhtmltype == 5){
                                options = element.cfg.items;
                            }
                            if(element.fieldhtmltype==4){
                                defaultValue =  element.fieldValue == 1 ? true :false;
                            }
                            let defaultStartValue = '';
                            let defaultEndValue = '';   
                            if(element.fieldname=="remindHoursBeforeStart"||element.fieldname=="remindTimesBeforeStart"||element.fieldname=="remindHoursBeforeEnd"||element.fieldname=="remindTimesBeforeEnd"){
                                displayItem = false;
                            }
                            if(element.fieldname=="remindBeforeStart"){
                                items.fieldlist.map((e)=>{
                                    if(e.fieldname == 'remindHoursBeforeStart'){
                                        defaultHoursValue = e.fieldValue;
                                    }
                                    if(e.fieldname == 'remindTimesBeforeStart'){
                                        defaultTimeValue = e.fieldValue;
                                    }
                                })
                            }
                            if(element.fieldname=="remindBeforeEnd"){
                                items.fieldlist.map((e)=>{
                                    if(e.fieldname == 'remindHoursBeforeEnd'){
                                        defaultHoursValue = e.fieldValue;
                                    }
                                    if(e.fieldname == 'remindTimesBeforeEnd'){
                                        defaultTimeValue = e.fieldValue;
                                    }
                                })
                            }
                            let defaultUploadValue = [];
                            if(element.fieldhtmltype==6){//附件
                                defaultUploadValue = element.fieldValue;
                            }
                            let browerparams = element.fieldhtmltype == 3?element.cfg.browcfg:'';//浏览按钮
                            if(element.fieldname=="remindImmediately"||element.fieldname=="remindBeforeStart"||element.fieldname=="remindBeforeEnd"){
                                // displayItem=remindTypeNewValue?true:false;//未选择提醒方式，以上三行不显示
                                items.fieldlist.forEach((e)=>{
                                    if(e.fieldname == "remindTypeNew"){
                                        displayItem = e.fieldValue.length>0 ?true :false;
                                    }
                                })
                            }

                            return (
                                <FormItem
                                  {...formItemLayout}
                                  label={`${element.fieldlabel}`}
                                  style={{display:displayItem?'block':'none'}}
                                >
                                    {element.fieldhtmltype==1 || element.fieldhtmltype==2?
                                        <WeaInput  viewAttr={1} hasBorder value={defaultValue} style={style}/>
                                    :element.fieldhtmltype==3 && element.fieldtype!=2 && element.fieldtype!=19?
                                        <WeaBrowser  hasBorder {...browerparams} replaceDatas={replaceDatas}  viewAttr={1} inputStyle={style}/>
                                    :element.fieldhtmltype==3 && element.fieldtype==2?
                                        <DatePicker style={style} disabled value={defaultValue} />
                                    :element.fieldhtmltype==3 && element.fieldtype==19?
                                        <TimePicker style={style} disabled value={defaultValue} format="HH:mm" />
                                    :element.fieldhtmltype==4 && element.fieldname =="remindImmediately"?
                                        <Switch disabled={true} checked = {defaultValue}/>
                                    :element.fieldhtmltype==4 && element.fieldname !=="remindImmediately"?  
                                        <WeaCheckbox viewAttr={1} value = {defaultValue}/>
                                    :element.fieldhtmltype==5?
                                        <WeaSelect viewAttr={1} hasBorder options={options} value={defaultValue} style={style}/>
                                    :element.fieldhtmltype==6 && !isEmpty(element.cfg)?
                                        <WeaUpload
                                            viewAttr={1}
                                            maxUploadSize={element.cfg.maxSize}//最大上传大小限制，单位为MB
                                            btnSize="default"//按钮大小
                                            uploadUrl={`${element.cfg.uploadUrl}?category=${element.cfg.category}`}//文件上传服务器接口地址
                                            category={`${element.cfg.category}`}//文件上传目录
                                            ref={'newmeetingupload'}
                                            uploadId={"_newmeetingupload"}//上传组件标识，同时生成隐藏域 id
                                            style={{display: "inline-block", padding: "0"}}
                                            datas = {defaultUploadValue}
                                        />
                                    :
                                        <div style={{display:'inline-block'}}>
                                            <span style={{color:'#FF0000'}}>附件存放目录未设置！</span>
                                            <span style={{marginLeft:'20px'}}><a href="/meeting/Maint/MeetingSet.jsp" target="_blank">点此设置</a></span>
                                        </div>
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart"?
                                        <span style={{paddingLeft:'5px'}}>开始前</span>:
                                        element.fieldname=="remindBeforeEnd"?
                                        <span style={{paddingLeft:'5px'}}>结束前</span>:''
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart"?
                                        <WeaInput viewAttr={1} hasBorder style={{width:'50px'}} value={defaultHoursValue}/>:
                                        element.fieldname=="remindBeforeEnd"?
                                        <WeaInput viewAttr={1} hasBorder style={{width:'50px'}} value={defaultHoursValue}/>:''
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart" || element.fieldname=="remindBeforeEnd"?
                                        <span>小时</span>:''

                                    }
                                    {
                                        element.fieldname=="remindBeforeStart"?
                                        <WeaInput viewAttr={1} hasBorder style={{width:'50px'}} value={defaultTimeValue}/>:
                                        element.fieldname=="remindBeforeEnd"?
                                        <WeaInput viewAttr={1} hasBorder style={{width:'50px'}} value={defaultTimeValue}/>:''
                                    }
                                    {
                                        element.fieldname=="remindBeforeStart" || element.fieldname=="remindBeforeEnd"?
                                        <span>分钟</span>:''
                                    }
                                    
                                    
                                </FormItem>
                               
                            )
                        })
                    }
                    </Form>

                </Row>
            </div>
        )
    }

}

export default PreviewMeetingGroup


