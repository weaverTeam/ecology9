import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as PreviewMeetingAction from '../../actions/previewMeeting'
import * as CalViewAction from '../../actions/calView'
import mapKeys from 'lodash/mapKeys';
import mapValues from 'lodash/mapValues';
import isEmpty from 'lodash/isEmpty';
import {Row,Col,Switch,Tag,DatePicker,Popover,Modal,Button,Tabs,Form} from 'antd';
const confirm = Modal.confirm;
const errorModal = Modal.error;
const MonthPicker = DatePicker.MonthPicker;
const TabPane = Tabs.TabPane;
import { WeaDialog,WeaButton,WeaTableEdit,WeaCheckbox,WeaNewScroll} from 'ecCom';
import objectAssign from 'object-assign'
import PreviewMeetingGroup from "./previewmeetinggroup/index.js"
import Collapse from "./collapse/index.js"
import MeetTopic from './meetTopic/';
import MeetService from './meetService/';
import MeetMember from './meetMember/';
import TabSign from "./tabsign/index.js"
import Decision from "./decision/index.js"
import TabDiscuss from "./tabDiscuss/index.js"
import  * as CreateMeetingApi from '../../apis/createMeeting'

let _this = null;


class PreviewMeeting extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            showDrop: false,

        }
        this.changeTab=this.changeTab.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        const {actions} = nextProps;
        if((this.props.meetingId != nextProps.meetingId) && nextProps.meetingId){
            actions.getViewMeetingField({meetingid:nextProps.meetingId})

        }
    }

	render(){
        const {showDrop} = this.state;
        const {onClose,tabs,previewmeetingTab,previewModal,datas,viewMeetingField,previewMeetingId,actions,tabDecisionTable,tabSignTable,tabSignTab,tabBase,tabTopic,tabService,tabMember,tabSign,tabDecision,tabDiscuss} = this.props;
        let buttonsList = tabBase.btns;
        let buttons = []; 
        buttons = buttonsList ? [
            <Button key="doEdit" style={{display:buttonsList.some((element)=>{return element=='doEdit'})?'inline-block':'none'}} type="primary" size="large" onClick={this.edit} >编 辑</Button>,
            <Button key="onChgIntvl" style={{display:buttonsList.some((element)=>{return element=='onChgIntvl'})?'inline-block':'none'}} type="primary" size="large" onClick={this.onChgIntvl} >提前结束重复会议</Button>,
           
            <Button key="cancelIntvl" style={{display:buttonsList.some((element)=>{return element=='cancelIntvl'})?'inline-block':'none'}} type="primary" size="large" onClick={this.cancelIntvl} >取消周期会议</Button>,
            <Button key="onStopIntvl" style={{display:buttonsList.some((element)=>{return element=='onStopIntvl'})?'inline-block':'none'}} type="primary" size="large" onClick={this.onStopIntvl} >停止重复会议</Button>,
            <Button key="changeMeeting" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >变更会议</Button>,
            <Button key="overMeeting" style={{display:buttonsList.some((element)=>{return element=='overMeeting'})?'inline-block':'none'}} type="primary" size="large" onClick={this.overMeeting} >结束会议</Button>,
            
            <Button key="onShowDecision" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >会议决议</Button>,
            <Button key="copyNewMeeting" style={{display:buttonsList.some((element)=>{return element=='copyNewMeeting'})?'inline-block':'none'}} type="primary" size="large" onClick={this.copy} >复 制</Button>,
            <Button key="cancelMeeting" style={{display:buttonsList.some((element)=>{return element=='cancelMeeting'})?'inline-block':'none'}} type="primary" size="large" onClick={this.cancelMeeting} >取消会议</Button>,
            <Button key="exportExcel" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >导出Excel</Button>,

            <Button key="doSubmit" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >批 准</Button>,
            <Button key="doSubmit2" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >提交审批</Button>,
            <Button key="doReject" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >退 回</Button>,
            <Button key="reSubmit" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >提交审批</Button>,

            <Button key="reSubmit2" style={{display:buttonsList.some((element)=>{return element=='reSubmit2'})?'inline-block':'none'}} type="primary" size="large" onClick={this.reSubmit2} >提 交</Button>,
            <Button key="doDelete" style={{display:buttonsList.some((element)=>{return element=='doDelete'})?'inline-block':'none'}} type="primary" size="large" onClick={this.doDelete} >删 除</Button>,
            <Button key="btn_cancle" style={{display:buttonsList.some((element)=>{return element=='btn_cancle'})?'inline-block':'none'}} type="primary" size="large" onClick={()=>{actions.previewModal(false);onClose()}} >关 闭</Button>,
            <Button key="doViewLog" style={{display:'none'}} type="primary" size="large" onClick={()=>{}} >显示日志</Button>,

            ]:[];



        // let columns_topicField = topicColumns==-1?'':topicColumns;
        // let datas_topicField = topicDatas==-1?'':topicDatas;
        // let columns_serviceField = serviceColumns==-1?'':serviceColumns;
        // let datas_serviceField = serviceDatas==-1?'':serviceDatas;

        // let tabsList = [];tan页显示受控
        // let d = tabsList.some((element)=>{return element=='35'});tan页显示受控
		return(
			
            <WeaDialog
                zIndex={800}
                visible={previewModal}
                title="查看会议"
                icon="icon-coms-meeting"
                iconBgcolor="#f14a2d"
                className="previewMeeting"
                buttons={buttons}
                style={{width:'790px',height:'500px'}}
                onCancel={()=>{actions.getViewMeetingField(false);onClose()}}
            >
                <div className="newmeeting-body">
                    <WeaNewScroll height='500px'>
                        <Tabs defaultActiveKey="1" activeKey = {`${previewmeetingTab}`} onChange={this.changeTab} >
                            <TabPane tab="会议信息" key="1" disabled = {tabs?(!tabs.base):true}>
                                {
                                    tabBase.datas && tabBase.datas.map((element,index,array)=>{
                                     return <PreviewMeetingGroup needTigger={true} title={element.grouplabel} showGroup={true} items={element} />
                                    })
                                }
                            </TabPane>
                            <TabPane tab="会议议程" key="2" disabled={tabs?(!tabs.topic):true} >
                                <MeetTopic data={tabTopic} actions = {actions} meetingId={previewMeetingId}/>
                            </TabPane>

                            <TabPane tab="会议服务" key="3" disabled={tabs?(!tabs.service):true} >
                                <MeetService data={tabService} />
                            </TabPane>
                            <TabPane tab="参会回执" key="4" disabled={tabs?(!tabs.member):true} >
                                <MeetMember data={tabMember} actions = {actions} meetingId={previewMeetingId}/>
                            </TabPane>
                            <TabPane tab="会议决议" key="5" disabled={tabs?(!tabs.decision):true} >
                                <Decision
                                    tabDecision= {tabDecision}
                                    actions = {actions}
                                    tabDecisionTable = {tabDecisionTable}

                                />
                            </TabPane>
                            <TabPane tab="会议签到" key="6" disabled={tabs?(!tabs.sign):true} >
                                <TabSign 
                                    tabSignTable = {tabSignTable}
                                    actions = {actions}
                                    tabSignTab = {tabSignTab}
                                    tabSign= {tabSign}
                                    meetingid = {previewMeetingId}
                                />
                            </TabPane>

                            <TabPane tab="相关交流" key="7" disabled={true} >
                                <TabDiscuss



                                />
                            </TabPane>
                            
                            
                        </Tabs>
                    </WeaNewScroll>
                </div>

            </WeaDialog>
				
		)
	}
    edit = ()=>{
        const {actions,previewMeetingId} = this.props;
        actions.controlModal(true,previewMeetingId);

    }
    doDelete = ()=>{
        const {actions,previewMeetingId,meetingType,date,type} = this.props;
        actions.doDelete({
            meetingid:previewMeetingId,
            meetingType:meetingType,
            date:date,
            type:type,
        });

    }
    reSubmit2 = ()=>{
        const {actions,previewMeetingId,meetingType,date,type,tabBase,tabService} = this.props;
        //添加冲突校验
        let params = {};
        
        tabBase.datas.forEach((element)=>{
            element.fieldlist.forEach((e)=>{
                params[e.fieldname] = e.fieldValue;
            })
        })

        for(let key in params){

            if(typeof(params[key]) === "object"){
                if((params[key].length==0)||(params[key].length==1 && isEmpty(params[key][0]))){
                    delete params[key];
                }else if(params[key].length >= 1){
                    let arr = [];
                    let str = '';
                    params[key].forEach((element)=>{
                        arr.push(element['id']);
                    })
                    str = arr.join(",");//3,4,5+
                    params[key] = str;
                }
            }else if(params[key] == undefined){
                delete params[key];
            }
        }
        let keycol = tabService.keycol;
        // params.serviceitems = {};
        let servicearr = []; 
        tabService.datas.forEach((element,index,array)=>{
            for(let key in element){
                if(key == keycol){
                    servicearr.push(element[keycol]);
                }
            }
        })
        
            let servicearrcopy = servicearr.filter((e,index,self)=>{
                return self.indexOf(e) == index;
            });
            params.serviceitems = servicearrcopy.join(',');

        CreateMeetingApi.chkMeetingP(params).then((result)=>{
            // if(result.chkstatus == true){//是否冲突，true是不冲突
            //     actions.reSubmit2({
            //         meetingid:previewMeetingId,
            //         meetingType:meetingType,
            //         date:date,
            //         type:type,
            //     });
            // }else{
            //     var conflictType = result.type == 'address' ? '会议室': result.type == 'member' ? '参会人员' : '会议服务';//冲突类型
            //     var conflictMsg = '';//冲突提示
            //     if(result.type == 'address'){
            //         conflictMsg = `${result.msg[0]}使用冲突，是否继续申请？`
            //     }
            //     if(result.type == 'service'){
            //         conflictMsg = `${result.msg[0]}使用冲突，是否继续申请？`
            //     }
            //     if(result.type == 'member'){
            //             if(result.msg.hrms.length>0||result.msg.crms.length>0){
            //                 let cont = result.msg.hrms.map((e)=>{
            //                     return <div>{e}</div>
            //                 })
            //                 let contcrms = result.msg.crms.map((e)=>{
            //                     return <div>{e}</div>
            //                 })
            //                 conflictMsg = (
            //                     <div>
            //                     {
            //                         result.msg.hrms.length>0?'人员：':''
            //                     }
            //                     {cont}
            //                     {
            //                         result.msg.crms.length>0?'客户：':''
            //                     }
            //                     {contcrms}
            //                     </div>
            //                     )
            //             }
            //     }
            //     if(result.cansub == true){//冲突了是否可提交，true是可提交
            //         confirm({
            //             title:`${conflictType}冲突`,
            //             content: conflictMsg,
            //             onOk() {
            //                 actions.reSubmit2({
            //                 meetingid:previewMeetingId,
            //                 meetingType:meetingType,
            //                 date:date,
            //                 type:type,
            //             });
            //             },
            //             onCancel() {},
            //         });
            //     }else{
            //         errorModal({
            //             title: `${conflictType}冲突不能提交`,
            //             content: `${conflictMsg}`,
            //         });
            //     }
            // }


                    let conflictType =  '会议室';//冲突类型
                    let conMsg = '';
                    let conflictMsg = '';//提示内容
                    if(!result.address.chkstatus){
                        conMsg = result.address.msg.map((e)=>{
                                        return <div>{`${e}冲突`}</div>
                                    });
                        conflictMsg = (
                                <div>
                                    {conMsg}
                                    {
                                        <span>是否继续申请？</span>
                                    }
                                </div>
                            )
                        if(result.address.cansub == true){//冲突了是否可提交，true是可提交
                            confirm({
                                title:"会议室冲突",
                                content: conflictMsg,
                                onOk() {
                                    if(!result.member.chkstatus){
                                        let cont = result.member.msg.hrms.map((e)=>{
                                            return <div>{e}</div>
                                        })
                                        let contcrms = result.member.msg.crms.map((e)=>{
                                            return <div>{e}</div>
                                        })
                                        conflictMsg = (
                                            <div>
                                            {
                                                result.member.msg.hrms.length>0?'人员：':''
                                            }
                                            {cont}
                                            {
                                                result.member.msg.crms.length>0?'客户：':''
                                            }
                                            {contcrms}
                                            {
                                                <span>是否继续申请？</span>
                                            }
                                            </div>
                                            )


                                        if(result.member.cansub == true){
                                            confirm({
                                                title:"参会人员冲突",
                                                content: conflictMsg,
                                                onOk() {
                                                    if(!result.service.chkstatus){
                                                        conMsg = result.service.msg.map((e)=>{
                                                            return <div>{`${e}冲突`}</div>
                                                        })
                                                        conflictMsg = (
                                                            <div>
                                                                {conMsg}
                                                                {
                                                                    <span>是否继续申请？</span>
                                                                }
                                                            </div>
                                                        )

                                                        if(result.service.cansub == true){
                                                            confirm({
                                                                title:"会议服务冲突",
                                                                content: conflictMsg,
                                                                onOk() {
                                                                    actions.reSubmit2({
                                                                        meetingid:previewMeetingId,
                                                                        meetingType:meetingType,
                                                                        date:date,
                                                                        type:type,
                                                                    });
                                                                },
                                                                onCancel() {},
                                                            });
                                                        }else{
                                                            errorModal({
                                                                title: "会议服务冲突不能提交",
                                                                content: conMsg,
                                                            });
                                                        }

                                                    }else{
                                                        actions.newMeeting(neworderFieldsParam,type);
                                                        actions.saveFields({});
                                                    }
                                                },
                                                onCancel() {},
                                            });    
                                        }else{
                                            errorModal({
                                                title: "参会人员冲突不能提交",
                                                content: conflictMsg,
                                            });
                                        }
                                    }else if(!result.service.chkstatus){
                                                conMsg = result.service.msg.map((e)=>{
                                                            return <div>{`${e}冲突`}</div>
                                                        })
                                                conflictMsg = (
                                                            <div>
                                                                {conMsg}
                                                                {
                                                                    <span>是否继续申请？</span>
                                                                }
                                                            </div>
                                                        )
                                            confirm({
                                                title:"会议服务冲突",
                                                content: conflictMsg,
                                                onOk() {
                                                    actions.reSubmit2({
                                                        meetingid:previewMeetingId,
                                                        meetingType:meetingType,
                                                        date:date,
                                                        type:type,
                                                    });
                                                },
                                                onCancel() {},
                                            });
                                    }else{
                                        actions.reSubmit2({
                                            meetingid:previewMeetingId,
                                            meetingType:meetingType,
                                            date:date,
                                            type:type,
                                        });
                                    }
                                },
                                onCancel() {},
                            });
                        }else{
                            errorModal({
                                title: "会议室冲突不能提交",
                                content: conMsg,
                            });
                        }

                    }else if(!result.member.chkstatus){
                        //tanchuconfirm
                        let cont = result.member.msg.hrms.map((e)=>{
                            return <div>{e}</div>
                        })
                        let contcrms = result.member.msg.crms.map((e)=>{
                            return <div>{e}</div>
                        })
                        conflictMsg = (
                            <div>
                            {
                                result.member.msg.hrms.length>0?'人员：':''
                            }
                            {cont}
                            {
                                result.member.msg.crms.length>0?'客户：':''
                            }
                            {contcrms}
                            {
                                <span>是否继续申请？</span>
                            }
                            </div>
                            )

                        if(result.member.cansub == true){
                            confirm({
                                title:"参会人员冲突",
                                content: conflictMsg,
                                onOk() {
                                    if(!result.service.chkstatus){
                                        conMsg = result.service.msg.map((e)=>{
                                                            return <div>{`${e}冲突`}</div>
                                                        })
                                        conflictMsg = (
                                                            <div>
                                                                {conMsg}
                                                                {
                                                                    <span>是否继续申请？</span>
                                                                }
                                                            </div>
                                                        )
                                        if(result.service.cansub == true){
                                            confirm({
                                                title:"会议服务冲突",
                                                content: conflictMsg,
                                                onOk() {
                                                    actions.reSubmit2({
                                                        meetingid:previewMeetingId,
                                                        meetingType:meetingType,
                                                        date:date,
                                                        type:type,
                                                    });
                                                },
                                                onCancel() {},
                                            });
                                        }else{
                                            errorModal({
                                                title: "会议服务冲突不能提交",
                                                content: conMsg,
                                            });
                                        }

                                    }else{
                                        actions.reSubmit2({
                                            meetingid:previewMeetingId,
                                            meetingType:meetingType,
                                            date:date,
                                            type:type,
                                        });
                                    }
                                },
                                onCancel() {},
                            });    
                        }else{
                            errorModal({
                                title: "参会人员冲突不能提交",
                                content: conflictMsg,
                            });
                        }

                    }else if(!result.service.chkstatus){
                        //tanchuconfirm
                        conMsg = result.service.msg.map((e)=>{
                                    return <div>{`${e}冲突`}</div>
                                })
                        conflictMsg = (
                                        <div>
                                            {conMsg}
                                            {
                                                <span>是否继续申请？</span>
                                            }
                                        </div>
                                    )
                        if(result.service.cansub == true){
                            confirm({
                                title:"会议服务冲突",
                                content: conflictMsg,
                                onOk() {
                                    actions.reSubmit2({
                                        meetingid:previewMeetingId,
                                        meetingType:meetingType,
                                        date:date,
                                        type:type,
                                    });
                                },
                                onCancel() {},
                            });
                        }else{
                            errorModal({
                                title: "会议服务冲突不能提交",
                                content: conMsg,
                            });
                        }
                    }else{
                        //chkstatus both true
                        actions.reSubmit2({
                                meetingid:previewMeetingId,
                                meetingType:meetingType,
                                date:date,
                                type:type,
                            });
                    }

















        })
        // actions.reSubmit2({
        //     meetingid:previewMeetingId,
        //     meetingType:meetingType,
        //     date:date,
        //     type:type,
        // });
       

    }
    cancelMeeting = ()=>{
        const {actions,previewMeetingId,meetingType,date,type} = this.props;
        actions.cancelMeeting({
            meetingid:previewMeetingId,
            meetingType:meetingType,
            date:date,
            type:type,
        });

    }
    overMeeting = ()=>{
        const {actions,previewMeetingId} = this.props;
        actions.overMeeting({meetingid:previewMeetingId});

    }
    copy = ()=>{
        const {actions,previewMeetingId,meetingType,date,type} = this.props;
        actions.copy({
            meetingid:previewMeetingId,
            meetingType:meetingType,
            date:date,
            type:type,
        });

    }
    onStopIntvl = ()=>{
        const {actions,previewMeetingId} = this.props;
        actions.onStopIntvl({meetingid:previewMeetingId});

    }
    cancelIntvl = ()=>{
        const {actions,previewMeetingId} = this.props;
        actions.cancelIntvl({meetingid:previewMeetingId});

    }
    
    onChgIntvl = ()=>{
        const {actions,previewMeetingId} = this.props;
        actions.onChgIntvl({meetingid:previewMeetingId});

    }
    
    
    
    
    changeTab(e){
        const {tabTopic,tabSign,tabDecision,actions} = this.props;
        actions.changePreviewMeetingTab(e);
        if(e=='6'){
            actions.getTabSignTable(tabSign);
        }
        if(e =="5"){
            actions.getTabDecisionTable(tabDecision.list.sessionkey);

        }
    }
   

}


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
    const { date,meetingType,type} = state.meetingCalView;
    return { ...state.previewMeeting,meetingType,date,type}
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators({...PreviewMeetingAction,...CalViewAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PreviewMeeting);

