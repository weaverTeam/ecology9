import {Row,Col,Switch,Tag,DatePicker,Tabs,Icon,Form,Input,TimePicker,Button} from 'antd';
import { WeaQrcode ,WeaDialog,WeaBrowser,WeaNewScroll,WeaTextarea } from 'ecCom';
import isEmpty from 'lodash/isEmpty';
import Collapse from "../collapse/index.js"
import {WeaTable} from 'comsRedux'
let _this = null;

class TabSign extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            title: props.title ? props.title : '请输入标题',
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    componentWillReceiveProps(nextProps) {
        // if (this.props.showGroup !== nextProps.showGroup) {
        //     this.setState({showGroup: nextProps.showGroup});
        // }
    }
    


    render() {
        const {title,allSign} = this.state;
        const  {actions,tabDecision,tabDecisionTable} = this.props;
        const colStyle = {
            textAlign:'center',
            marginBottom:'20px',
        }
        const colRightStyle = {
            marginBottom:'20px',
        }
        const colSpanStyle = {
            wordBreak:'break-all',
        }
        const colDivStyle = {
            borderBottom:'1px solid #EAEAEA',
            minHeight:'18px'
        }
        let testarr = ["1111111111111","22222222222222222222","33333333333333333","4adasda"];
        return (
            <div className="preview-tabsign">
                <Collapse
                    title = "基本信息"
                    showGroup = {true}
                >
                    <Row>
                        <Col span={6} offset={1} style={colStyle}>
                            <span style = {{}}>决议概述：</span>

                        </Col>
                        <Col span={12} offset={1} style={colRightStyle}>
                            
                            {/*<WeaTextarea viewAttr ={1} value ={`${e}`}/>*/}
                            <div style = {colDivStyle}>
                                <span style = {colSpanStyle}>{`${tabDecision.decision}`}</span>
                            </div>
                        </Col>
                        <Col span={6} offset={1} style={colStyle}>
                            <span style = {{}}>相关文档：</span>
                            
                        </Col>
                        <Col span={12} offset={1} style={colRightStyle}>
                            
                            {/*<WeaTextarea viewAttr ={1} value ={`${e}`}/>*/}
                            <div style = {colDivStyle}>
                                <span style = {colSpanStyle}>{`${tabDecision.decisionatchids}`}</span>
                            </div>
                        </Col>
                        <Col span={6} offset={1} style={colStyle}>
                            <span style = {{}}>相关流程：</span>
                            
                        </Col>
                        <Col span={12} offset={1} style={colRightStyle}>
                            
                            {/*<WeaTextarea viewAttr ={1} value ={`${e}`}/>*/}
                            <div style = {colDivStyle}>
                                <span style = {colSpanStyle}>{`${tabDecision.decisionwfids}`}</span>
                            </div>
                        </Col>
                        <Col span={6} offset={1} style={colStyle}>
                            <span style = {{}}>相关客户：</span>
                            
                        </Col>
                        <Col span={12} offset={1} style={colRightStyle}>
                            
                            {/*<WeaTextarea viewAttr ={1} value ={`${e}`}/>*/}
                            <div style = {colDivStyle}>
                                <span style = {colSpanStyle}>{`${tabDecision.decisioncrmids}`}</span>
                            </div>
                        </Col>
                        <Col span={6} offset={1} style={colStyle}>
                            <span style = {{}}>相关项目：</span>
                            
                        </Col>
                        <Col span={12} offset={1} style={colRightStyle}>
                            
                            {/*<WeaTextarea viewAttr ={1} value ={`${e}`}/>*/}
                            <div style = {colDivStyle}>
                                <span style = {colSpanStyle}>{`${tabDecision.decisionprjids}`}</span>
                            </div>
                        </Col>
                        <Col span={6} offset={1} style={colStyle}>
                            <span style = {{}}>相关任务：</span>
                            
                        </Col>
                        <Col span={12} offset={1} style={colRightStyle}>
                            
                            {/*<WeaTextarea viewAttr ={1} value ={`${e}`}/>*/}
                            <div style = {colDivStyle}>
                                <span style = {colSpanStyle}>{`${tabDecision.decisiontskids}`}</span>
                            </div>
                        </Col>
                        
                    </Row>
                </Collapse>
                <Collapse 
                    title = "决议任务"
                    showGroup = {true}
                    
                >
                    <WeaTable
                        hasOrder={true}
                        needScroll={true}
                        sessionkey={tabDecisionTable}
                    />

                </Collapse>
                
            </div>
        )
    }

    testonChange = (ids, names, datas)=>{
        this.setState({
            allSign: datas,
        });
    }

}

export default TabSign


