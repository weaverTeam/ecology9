import {WeaAlertPage} from 'ecCom';
import {Table} from 'antd';
import Util from '../../../util/util';
class MeetService extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
	    let {columns, datas} = this.props.data || {};
        columns.forEach((col) => {
            col.render = Util.tableCol.render(col);
        });
		return (
			<div className="wea-meeting-service-view">
                <div className="panel-title">服务列表</div>
                {columns.length == 0 || datas.length == 0 ?
                    <WeaAlertPage icon="icon-blog-blank">
                        暂无数据
                    </WeaAlertPage>:
                    <div className="wea-new-table">
                        <Table columns={columns} dataSource={datas} pagination={false}/>
                    </div>
                }
            </div>
		);
	}
}

export default MeetService;