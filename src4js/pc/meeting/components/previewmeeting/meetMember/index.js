import {WeaAlertPage} from 'ecCom';
import {Table, Button} from 'antd';
import MeetMemberReceiptDialog from './MeetMemberReceiptDialog';
import MeetMemberRemarkDialog from './MeetMemberRemarkDialog';

class MeetMember extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            hrmCollapse: false,
            crmCollapse: false,
            otherCollapse: false,
            markCollapse: false
        };
	}
	render() {
	    const data = this.props.data || {};
        let {hrmcolumns, hrmdatas,hrmnum} = data;
        hrmcolumns = hrmcolumns.map((col) => {
            return this.getHrmCol(col);
        });
        hrmdatas = hrmdatas.map((data, index) => {
            data.key = index;
            return data;
        });
        let {crmcolumns, crmdatas,crmnum} = data;
        crmcolumns = crmcolumns.map((col) => {
            col.dataIndex=col.key;
            return col;
        });
        crmdatas = crmdatas.map((data, index) => {
            data.key = index;
            return data;
        });
        let {otherbtn, othersremark,othertitle} = data;
        let {hrmCollapse, crmCollapse,otherCollapse, markCollapse} = this.state;
        hrmcolumns.push({
            key: 'receipt',
            title: '回执',
            render: (text, record, index) => {
                if (record.showbtn) {
                    return <a onClick={this.openReceipt(record, 'hrm')} className="color-blue">回执</a>
                }
                return <a onClick={this.openReceipt(record, 'hrm')} className="color-blue">回执</a>
            }
        });
        crmcolumns.push({
            key: 'receipt',
            title: '回执',
            render: (text, record, index) => {
                if (record.showbtn) {
                    return <a onClick={this.openReceipt(record, 'crm')} className="color-blue">回执</a>
                }
                return <a onClick={this.openReceipt(record, 'crm')} className="color-blue">回执</a>
            }
        });
        return (
            <div className="wea-meeting-member-view">
                <div>
                    <div className="panel-title cursor-pointer" style={{paddingTop: 0}} onClick={() => this.setState({hrmCollapse: !hrmCollapse})}>
                        参会人员列表（应到人数{hrmnum}人）
                        <span className={`fr ${hrmCollapse ? 'icon-coms-down' : 'icon-coms-up'}`}></span>
                    </div>
                    {hrmCollapse || (hrmcolumns.length == 0 || hrmdatas.length == 0 ?
                        <WeaAlertPage icon="icon-blog-blank">
                            暂无参会人员
                        </WeaAlertPage>:
                        <div className="wea-new-table">
                            <Table columns={hrmcolumns} dataSource={hrmdatas} pagination={false}
                                   expandIconAsCell={true}
                                   expandRowByClick={true}
                                   expandedRowRender={this.expandHrmRow}
                            />
                        </div>
                    )}
                </div>
                
                {crmcolumns.length != 0 && crmdatas.length != 0 &&
                     <div>
                         
                        <div className="panel-title cursor-pointer" onClick={() => this.setState({crmCollapse: !crmCollapse})}>
                            参会客户（应到客户数{crmnum}）
                            <span className={`fr ${crmCollapse ? 'icon-coms-down' : 'icon-coms-up'}`}></span>
                        </div>
                         {crmCollapse ||
                            <div className="wea-new-table">
                                <Table columns={crmcolumns} dataSource={crmdatas} pagination={false}/>
                            </div>
                         }
                     </div>
                }
                
                <div>
                    <div className="panel-title cursor-pointer" onClick={() => this.setState({otherCollapse: !otherCollapse})}>
                        其他人员
                        <span className={`fr ${otherCollapse ? 'icon-coms-down' : 'icon-coms-up'}`}></span>
                    </div>
                    {otherCollapse || (othertitle ? <div className="panel-content">{othertitle}</div>:<div className="panel-content no-data">暂无</div>)}
                </div>
                
                <div>
                    <div className="panel-title cursor-pointer" onClick={() => this.setState({markCollapse: !markCollapse})}>
                        备注
                        <span className={`fr ${markCollapse ? 'icon-coms-down' : 'icon-coms-up'}`}></span>
                        {otherbtn && <span className="fr icon-coms-BatchEditing-Hot cursor-pointer" onClick={this.openRemark}></span>}
                    </div>
                    {markCollapse || (othersremark ? <div className="panel-content" dangerouslySetInnerHTML={{__html:othersremark}} />:<div className="panel-content no-data">暂无备注</div>)}
                </div>
                
                <div className="panel-title summary border-none">
                    已确定参会人员总计<span className="color-red">{hrmnum+crmnum}</span>人，
                    其中公司员工<span className="color-red">{hrmnum}</span>人，
                    外部人员<span className="color-red">{crmnum}</span>人
                </div>
                <MeetMemberReceiptDialog ref="receipt" hrmcolumns={hrmcolumns} crmcolumns={crmcolumns} meetingId={this.props.meetingId} actions={this.props.actions}/>
                <MeetMemberRemarkDialog ref="remark" meetingId={this.props.meetingId}
                                        setNewRemark={this.props.actions.setNewRemark}/>
            </div>
        );
	}
	openReceipt(record, type) {
	    return (e) => {
            e.stopPropagation();
            this.refs.receipt.open(record, type);
	    }
    }
    
    openRemark = (e) => {
        e.stopPropagation();
        this.refs.remark.open();
    }
    getHrmCol(col) {
        let newCol = {...col};
        newCol.dataIndex=col.key;
        switch(col.key) {
            case "membername": {
                newCol.render = (text, record, index) => {
                    return <a href={`javaScript:openhrm(${record.memberid});`} onClick={window.pointerXY} title={record.membername} >{record.membername}</a>
                }
                break;
            }
            case "isattend":
            case "begindate":
            case "bookroom":
            case "roomstander":
            case "bookticket":
            case "backdate":
            case "ticketstander":
            default: break;
        }
        return newCol;
    }
    
    expandHrmRow = (record, index) => {
        let otherInfo = Array.isArray(record.othermember) ? record.othermember.reduce((sum, val) => sum ? sum + "," + val.othername : val.othername, "") : "";
    	return (
    	    <div className="expand">
                其它参会人员：{otherInfo || "无"}
            </div>
        )
    }
}

export default MeetMember;