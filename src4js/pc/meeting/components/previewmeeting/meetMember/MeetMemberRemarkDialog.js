import {WeaDialog,WeaTextarea} from 'ecCom';
import {Button, Input} from 'antd';
import * as Apis from '../../../apis/previewMeeting';
class MeetTopicRemarkDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            remark: props.remark
        };
    }
    componentWillReceiveProps(nextProps) {
    	if (nextProps.remark != this.props.remark) {
    		this.setState({
    		    remark: nextProps.remark
            });
    	}
    }
    render() {
        let {visible, remark} = this.state;
        return (
            <WeaDialog visible={visible} title="备注"
                       icon="icon-coms-meeting" iconBgcolor="#f14a2d"
                       onCancel={this.back}
                       style={{height: 200}}
                       buttons={[
                           <Button type="primary" onClick={this.save}>保存</Button>,
                           <Button onClick={this.back}>返回</Button>,
                       ]}>
                <div className="wea-meet-member-remark">
                    <div className="ant-col-6">备注：</div>
                    <div className="ant-col-18">
                        <Input type="textarea" placeholder="在此输入内容" autosize={{ minRows: 8}}
                               onChange={(e) => this.setState({remark: e.target.value})} value={remark} />
                    </div>
                </div>
            </WeaDialog>
        );
    }
    open = () => {
        this.setState({
            visible: true
        });
    }
    
    back = () => {
        this.setState({
            visible: false,
        });
    }
    
    save = () => {
        let remark = this.state.remark;
        Apis.saveRemark({meetingid: this.props.meetingId, othersremark: remark}).then((result) => {
        	this.back();
            this.props.setNewRemark && this.props.setNewRemark(remark);
        });
    }
}

export default MeetTopicRemarkDialog;