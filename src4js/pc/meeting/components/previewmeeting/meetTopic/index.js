import {WeaTable, WeaBrowser, WeaAlertPage} from 'ecCom';
import MeetTopicTimeDialog from "./MeetTopicTimeDialog";
import {Modal} from 'antd';
import Util from '../../../util/util';
const confirm = Modal.confirm;

class MeetTopic extends React.Component {
    constructor(props) {
        super(props);
        this.browserProps = {
            "completeParams": {},
            "conditionDataParams": {},
            "dataParams": {},
            "destDataParams": {},
            "hasAddBtn": true,
            "hasAdvanceSerach": true,
            "idSeparator": ",",
            "isAutoComplete": 1,
            "isDetail": 0,
            "isMultCheckbox": false,
            "isSingle": true,
            "linkUrl": "/docs/docs/DocDsp.jsp?isrequest=1&id=",
            "pageSize": 10,
            "quickSearchName": "",
            "title": "文档",
            "type": "9",
            "viewAttr": 2,
            customized: true,
            onChange: this.chooseDoc
        };
        this.operates = [
            {
                flag: "true",
                key: "time",
                text: "安排时间",
                index: 'time'
            },
            {
                flag: "true",
                key: "doc",
                text: (
                    <WeaBrowser {...this.browserProps} >
                        提供资料
                    </WeaBrowser>
                ),
                index: 'doc'
            }
        ];
        this.state = {
            record: {}
        };
    }
    
    render() {
        let {meetingId, actions} = this.props;
        let {columns = [], datas = [], subject} = this.props.data || {};
        columns.forEach((col) => {
            col.display = 'true';
            if (col.key == "topicdate") {
                col.dataIndex = col.key;
                col.render = (text, record, index) => {
                    let textTip = "";
                    text.forEach((t) => {
                    	textTip += t;
                        textTip += "\n";
                    });
                    return (
                        <span title={textTip}>
                            {text.map((t) => <span style={{display: 'block'}}>{t}</span>)}
                        </span>
                    );
                }
            } else {
                col.render = Util.tableCol.render(col);
            }
        });
        datas.forEach((data, index) => {
            if (!data) return;
            data.randomFieldOp = {
                time: data.btns && data.btns[0],
                doc: data.btns && data.btns[1]
            };
            data.randomFieldId = index;
            data.key = index;
        });
        return (
            <div className="wea-meeting-topic-view">
                <div className="edit-title">
                    <span className="title">议程列表</span>
                    <span className="icon-coms-BatchEditing-Hot cursor-pointer" style={{display:"none"}}></span>
                </div>
                {columns.length == 0 || datas.length == 0 ?
                    <WeaAlertPage icon="icon-blog-blank">
                        暂无数据
                    </WeaAlertPage> :
                    <WeaTable columns={columns} datas={datas} loading={false} pagination={false}
                              className="topic-view-table"
                              operates={this.operates}
                              onOperatesClick={this.operate}
                              expandIconAsCell={true}
                              expandRowByClick={true}
                              expandedRowRender={this.expandRow}
                    />
                }
                <MeetTopicTimeDialog ref="timeDialog" titleKey={subject} meetingId={meetingId} actions={actions}/>
            </div>
        );
    }
    
    operate = (record, index, operate, flag) => {
        this.setState({
            record: record
        });
        if (operate.key == "time") {
            this.refs.timeDialog.open(record);
        }
    }
    
    expandRow = (record, index) => {
        if (!record.doclist || record.doclist.length == 0) return undefined;
        let {meetingId} = this.props;
        return (
            <div className="expand">
                <table>
                    <tbody>
                    {record.doclist.map((doc) => {
                        return (
                            <tr>
                                <td><a href={Util.href.doc(doc.docid, meetingId)} target="_blank">{doc.docname}</a></td>
                                <td style={{width: "15%"}}><a href={`javaScript:openhrm(${doc.hrmid});`} onClick={window.pointerXY} title={doc.hrmname} >{doc.hrmname}</a></td>
                                <td style={{width: "20%"}}><span onClick={this.deleteDoc.bind(this, doc, index)}
                                          className="ant-modal-close-x cursor-pointer"></span></td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
    deleteDoc = (doc, dataIndex) => {
        confirm({
            title: `确认要删除 ${doc.docname} 这篇文档吗？`,
            onOk: () => {
                let {actions} = this.props;
                return actions.delTopicDoc({id: doc.id}, dataIndex);
            }
        });
    }
    chooseDoc = (ids, names, datas) => {
        let {actions, meetingId} = this.props;
        let {record} = this.state;
        actions.addTopicDoc({meetingid: meetingId, topicid: record.id, docid: ids}, record);
    }
}

export default MeetTopic;