import {WeaDialog, WeaTableEdit } from 'ecCom';
import {Button, DatePicker, TimePicker,Form} from 'antd';
import moment from 'moment';
import * as Apis from '../../../apis/previewMeeting';
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
class MeetTopicTimeForm extends React.Component {
	constructor(props) {
		super(props);
        this.clear = ::this.clear;
        this.onChange = ::this.onChange;
        this.state = {datas:[]};
	}
	componentDidMount() {
        this.props.initFunc && this.props.initFunc(this);
	}
	render() {
	    let {form}  = this.props;
        let {datas=[]} = this.state;
		return (
            <Form inline>
                <WeaTableEdit columns={getTimeTableColumns(form)} datas={datas} title="时间安排" ref="tableEdit" onChange={this.onChange}/>
            </Form>
		);
	}
	
    validateFieldsAndScroll = (callback) => {
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }
            callback(values);
        });
    }
    
    onChange(datas) {
        this.setState({
            datas: datas
        });
    }
    
    clear() {
        this.setState({
            datas: []
        });
    }
};
MeetTopicTimeForm = Form.create()(MeetTopicTimeForm);

const getTimeTableColumns = (form) => {
    const {getFieldProps} = form;
    return [{
        title: <span style={{marginRight: "70px"}}>日期范围</span>,
        dataIndex: "range",
        key: "range",
        render: (text, record, index) => {
            let data = record || {};
            let begin = data.begindate ? new Date(data.begindate + " " + (data.begintime || "00:00")) : undefined;
            let end = data.enddate ? new Date(data.enddate + " " + (data.endtime || "00:00")) : undefined;
            let props = getFieldProps("_" + index,  {
                rules: [
                    { required: true, message: "请选择日期范围"},
                    {validator: (rule, value, callback) => {
                        if (!Array.isArray(value) || value.some((s) => !s)) {
                            callback("请选择日期范围");
                        } else
                            callback();
                        
                    }}
                ],
                initialValue: [
                    begin,
                    end
                ]
            });
            return (
                <FormItem>
                    <RangePicker {...props} style={{ width: 510 }} showTime format="yyyy-MM-dd HH:mm" />
                </FormItem>
            );
        }
    }];
    /*let columnDefs = [
     {key: 'begindate', label: '开始日期', type: "date", nullMsg: "请选择开始日期"},
     {key: 'begintime', label: '开始时间', type: "time", nullMsg: "请选择开始时间"},
     {key: 'enddate', label: '结束日期', type: "date", nullMsg: "请选择结束日期"},
     {key: 'endtime', label: '结束时间', type: "time", nullMsg: "请选择结束时间"}
     ];
     let cols = columnDefs.map((colDef) => {
     let col = {
     title: colDef.label, //列名
     dataIndex: colDef.key, //列的id 对应数据
     key: colDef.key, //前端渲染key值
     render: (text, record, index) => {
     let props = getFieldProps(colDef.key + "_" + index,  {
     rules: [
     { required: true, message: colDef.nullMsg },
     {validator: validTime,}
     ],
     });
     if (colDef.type == "date") {
     return (
     <FormItem>
     <DatePicker {...props}/>
     </FormItem>
     );
     } else {
     return (
     <FormItem>
     <TimePicker {...props} format="HH:mm"/>
     </FormItem>
     );
     }
     }
     };
     return col;
     });
     return cols;*/
}
class MeetTopicTimeDialog extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            visible: false,
            record: {},
        };
	}
	render() {
	    let {visible, record} = this.state;
        let {titleKey} = this.props;
		return (
			<WeaDialog visible={visible} title={`设置会议议程：${record[titleKey] || ''}`}
                       style={{width: 600, height: 600}}
                       icon="icon-coms-meeting" iconBgcolor="#f14a2d"
                       onCancel={this.back}
                       buttons={[<Button type="primary" onClick={this.submit}>提交</Button>, <Button onClick={this.back}>返回</Button>]}>
                <div className="wea-meeting-topic-time">
                    <MeetTopicTimeForm initFunc={(form) => this.form = form}/>
                </div>
            </WeaDialog>
		);
	}
	open = (record) => {
		this.setState({
		    visible: true,
            record: record,
            datas: []
        }, () => {
            let {meetingId} = this.props;
        	Apis.getTopicDate({meetingid: meetingId, topicid: record.id}).then((result) => {
        		this.form.onChange(result.data);
        	});
        });
	}
	
	back = () => {
		this.setState({
		    visible: false,
            record: {}
        }, this.form.clear);
	}
	
	
	submit = () => {
        let {meetingId,actions} = this.props;
        const {record} = this.state;
        this.form.validateFieldsAndScroll((values={}) => {
            let saveObj = {};
        	saveObj.meetingid = meetingId;
            saveObj.topicid = record.id;
            Object.entries(values).forEach((entry) => {
            	let idx = entry[0];
                let beginMoment = moment(entry[1][0]);
                let endMoment = moment(entry[1][1]);
                saveObj["begindate" + idx] = beginMoment.format("YYYY-MM-DD");
                saveObj["begintime" + idx] = beginMoment.format("HH:mm");
                saveObj["enddate" + idx] = endMoment.format("YYYY-MM-DD");
                saveObj["endtime" + idx] = endMoment.format("HH:mm");
            });
            saveObj.rows = Object.entries(values).length;
            Apis.saveTopicDate(saveObj).then((result) => {
            	this.back();
            	actions.setTopicNewTime(meetingId, record.id, result.data);
            });
        });
	}
	
}

export default MeetTopicTimeDialog;