import {Row,Col,Switch,Tag,DatePicker,Tabs,Icon,Form,Input,TimePicker,Button} from 'antd';
import { WeaQrcode ,WeaDialog,WeaBrowser,WeaNewScroll} from 'ecCom';
import isEmpty from 'lodash/isEmpty';
import Collapse from "../collapse/index.js"
import {WeaTable} from 'comsRedux'
let _this = null;

class TabSign extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            title: props.title ? props.title : '请输入标题',
            allSign:null,//签到人
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    componentWillReceiveProps(nextProps) {
        // if (this.props.showGroup !== nextProps.showGroup) {
        //     this.setState({showGroup: nextProps.showGroup});
        // }
    }
    


    render() {
        const {title,allSign} = this.state;
        const  {tabSignTable,actions,tabSignTab,tabSign,meetingid} = this.props;
        const buttons = [
            <Button key="save" type="primary" size="large" 
                onClick={()=>{
                    let arr = [];
                    let str = '';
                    this.state.allSign.forEach((element)=>{
                        arr.push(element['id']);
                    })
                    str = arr.join(",");//3,4,5
                    let objparam = {};
                    objparam.userid = str;
                    objparam.qrticket = tabSign.qrcode;
                    objparam.meetingid = meetingid;
                    actions.signMeetingByHand(objparam);

                }} 
            >
            保 存
            </Button>,
            ];
        const iconStyle = {
            display:'inline-block',
            color:'#58A0F8',
            fontSize:'16px',
            paddingRight:'8px'
        }
        return (
            <div className="preview-tabsign">
                <Collapse
                    title = "会议签到二维码"
                    showGroup = {true}
                    childrenStyle={{textAlign:'center'}}
                 >
                    <WeaQrcode size={118} level={"L"} text={`/weaver/weaver.meeting.qrcode.CreateQRCodeServlet?isUrl=1&content=${tabSign.qrcode}`}/>
                    <span style={{display:'block'}}>使用Emobile【扫一扫】功能扫描二维码即可签到</span>
                </Collapse>
                <Collapse 
                    title = "签到记录"
                    showGroup = {true}
                    customIcon = {
                        <span style={{display:'inline-block'}}>
                            <span className="icon-coms-download" style={iconStyle} onClick={()=>{}} />
                            <span className="icon-coms-Add-to-hot" style={iconStyle} onClick={()=>{actions.changeTabSignTab(true)}} />
                        </span>

                    }
                >
                    <WeaTable
                        hasOrder={true}
                        needScroll={false}
                        sessionkey={tabSignTable}
                        onOperatesClick={this.onOperatesClick2}
                    />

                </Collapse>
                <WeaDialog
                    visible={tabSignTab}
                    title="添加会议签到记录"
                    className="weadialog-tabsign"
                    icon="icon-coms-meeting"
                    iconBgcolor="#f14a2d"
                    buttons={buttons}
                    style={{width:'600px',height:'200px'}}
                    onCancel={()=>{actions.changeTabSignTab(false)}}
                >
                    <Row style={{paddingTop:'80px'}}>
                        <Col span={8} style={{textAlign:"center",height:'30px'}}>
                            <span style={{display:'inline-block',lineHeight:'30px'}}>签到人:</span>
                        </Col>
                        <Col span={16}>
                            <WeaBrowser
                                hasAddBtn={false}
                                hasAdvanceSerach={true}
                                isAutoComplete={1}
                                isDetail={0}
                                isMultCheckbox={false}
                                isSingle={false}
                                linkUrl="/hrm/resource/HrmResource.jsp?id="
                                quickSearchName=""
                                title="多人力资源"
                                type="17"
                                viewAttr={2}
                                onChange={this.testonChange}
                                inputStyle={{width:'250px'}}
                            />
                        </Col>
                    </Row>
                </WeaDialog>
            </div>
        )
    }

    testonChange = (ids, names, datas)=>{
        this.setState({
            allSign: datas,
        });
    }
    onOperatesClick2 = (record,index,operate,flag)=>{
        const {actions,meetingid} = this.props;
        if(index == 0){//清空
            actions.delSign({
                id:record.id,//等接口
                meetingid:meetingid,
            })
        }
        if(index == 1){//补录
            actions.signMeetingByHandOne({
                id:record.id,//等接口
                meetingid:meetingid,
            })
            
        }
        
    }

}

export default TabSign




