import {Row,Col,Switch,Tag,DatePicker,Tabs,Icon,Form,Input,TimePicker,Button} from 'antd';
import { WeaQrcode ,WeaDialog,WeaBrowser,WeaNewScroll} from 'ecCom';
import isEmpty from 'lodash/isEmpty';
import Collapse from "../collapse/index.js"
import {WeaTable} from 'comsRedux'
let _this = null;

class TabDiscuss extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            title: props.title ? props.title : '请输入标题',
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    componentWillReceiveProps(nextProps) {
        // if (this.props.showGroup !== nextProps.showGroup) {
        //     this.setState({showGroup: nextProps.showGroup});
        // }
    }
    


    render() {
        const {title} = this.state;
        const  {actions} = this.props;
        let tetsArr = ["11111","22222","33333"]
        
        return (
            <div className="preview-tabDiscuss"
                style = {{backgroundColor:'#eaeaea',padding:'10px 20px'}}
            >
                <div style = {{backgroundColor:'#ffffff',height:'200px'}}>富文本
                </div>
                <div style = {{backgroundColor:'#ffffff',marginTop:'10px'}}>
                    {
                        tetsArr.map(()=>{
                            return  <div style = {{borderBottom:'1px solid #E2E2E2',padding:"10px 10px 0 10px"}}>
                                        <Row>
                                            <Col span = {4}>图片
                                            </Col>
                                            <Col span = {20}>
                                                <div>第一行
                                                </div>
                                                <div>第二行
                                                </div>
                                                <div>第三行
                                                </div>
                                                <div>第四行
                                                </div>
                                                <div>第五行
                                                </div>
                                                <div>
                                                    <Row>
                                                        <Col span = {4}>
                                                            时间
                                                        </Col>
                                                        <Col span = {8} offset = {12}>
                                                            编辑，删除按钮
                                                        </Col>
                                                    </Row>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>  
                        })
                    }
                     
                </div>
               
               
            </div>
        )
    }


}

export default TabDiscuss


