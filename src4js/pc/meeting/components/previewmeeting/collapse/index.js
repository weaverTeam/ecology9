import {Row,Col,Switch,Tag,DatePicker,Tabs,Icon,Form,Input,TimePicker} from 'antd';
import isEmpty from 'lodash/isEmpty';
let _this = null;

class Collapse extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            showGroup: props.showGroup ? props.showGroup : false,
            title: props.title ? props.title : '请输入标题',
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        //组件渲染控制
        //return this.props.title !== nextProps.title
        return true;
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.showGroup !== nextProps.showGroup) {
            this.setState({showGroup: nextProps.showGroup});
        }
    }
    


    render() {
        const {showGroup,title} = this.state;
        const {customIcon = '',childrenStyle = {}} = this.props;
        return (
            <div className="wea-search-group">
                <Row className="wea-title">
                    <Col span="14">
                        <div style = {{fontSize:'12px'}}>{title}</div>
                    </Col>
                    <Col span="10" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                        {customIcon &&
                            customIcon
                        }
                        <span className = {showGroup?"icon-coms-up":"icon-coms-down"} onClick={()=>this.setState({showGroup:!showGroup})} style={{display:'inline-block'}}/>
                    </Col>
                </Row>
                <Row className="wea-content" style={showGroup ? childrenStyle : {height:0}}>
                    {this.props.children}
                </Row>
            </div>
        )
    }

}

export default Collapse
