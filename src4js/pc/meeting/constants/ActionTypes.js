/**
 * 会议日历
 */
export const CALVIEW_LOADING = 'meeting_calview_loading'
export const CALVIEW_GETCALENDARLIST = 'meeting_calview_getcalendarlist'
export const CALVIEW_CHANGETYPE = 'meeting_calview_changetype'
export const CALVIEW_CHANGENEWMEETINGTAB = 'meeting_calview_changenewmeetingtab'
export const CALVIEW_CHANGEMEETINGTYPE = 'meeting_calview_changemeetingtype'
export const CALVIEW_GETNEXTMEETING = 'meeting_calview_getnextmeeting'
export const CALVIEW_GETDATA = 'meeting_calview_getdata'
export const CALVIEW_CHANGEMONTH = 'meeting_calview_changemonth'
export const CALVIEW_SAVEDATE = 'meeting_calview_savedate'
export const CALVIEW_CONTROLMODAL = 'meeting_calview_controlmodal'
export const CALVIEW_PREVIEWMODAL = 'meeting_calview_previewmodal'
export const CALVIEW_GETMEETINGBASEFIELD = 'meeting_calview_getmeetingbase'
export const CALVIEW_SAVE_FIELDS = 'meeting_calview_savefields'
export const CALVIEW_VIEWMEETINGFIELD = 'meeting_calview_viewmeetingfield'
export const CALVIEW_NEWMEETING = 'meeting_calview_newmeeting'
export const CALVIEW_CHANGEBROSERMEETINGTYPE = 'meeting_calview_changebrosermeetingtype'
export const CALVIEW_CREATEDATE = 'meeting_calview_createdate'
export const CALVIEW_GETTABSIGNTABLE = 'meeting_calview_gettabsigntable'
export const CALVIEW_CHANGETABSIGNTAB = 'meeting_calview_changetabsigntab'
export const CALVIEW_SAVEPREVIEWMEETINGID = 'meeting_calview_savepreviewmeetingid'


export const CREATE_CHANGENEWMEETINGTAB = 'meeting_create_changenewmeetingtab'
export const CREATE_GETMEETINGBASEFIELD = 'meeting_create_getmeetingbase'
export const CREATE_NEWMEETING = 'meeting_create_newmeeting'
export const CREATE_SAVE_FIELDS = 'meeting_create_savefields'
export const CREATE_GETMEETINGTOPICFIELDC = 'meeting_create_getmeetingtopicfieldc'
export const CREATE_GETMEETINGTOPICFIELDD = 'meeting_create_getmeetingtopicfieldd'
export const CREATE_GETMEETINGSERVICEFIELDC = 'meeting_create_getmeetingservicefieldc'
export const CREATE_GETMEETINGSERVICEFIELDD = 'meeting_create_getmeetingservicefieldd'
export const CREATE_CHANGEBROSERMEETINGTYPE = 'meeting_create_changebrosermeetingtype'
export const CREATE_SAVEERRORMESSAGE = 'meeting_create_saveerrormessage'


export const PREVIEW_LOADING = 'meeting_preview_loading'
export const PREVIEW_VIEWMEETINGFIELD = 'meeting_preview_viewmeetingfield'
export const PREVIEW_PREVIEWMODAL = 'meeting_preview_previewmodal'
export const PREVIEW_GETTABSIGNTABLE = 'meeting_preview_gettabsigntable'
export const PREVIEW_CHANGETABSIGNTAB = 'meeting_preview_changetabsigntab'
export const PREVIEW_SIGNMEETINGBYHAND = 'meeting_preview_signmeetingbyhand'
export const PREVIEW_GETTABDECISIONTABLE = 'meeting_preview_gettabdecisiontable'
export const PREVIEW_CHANGEPREVIEWMEETINGTAB = 'meeting_preview_changepreviewmeetingtab'

/*会议议程查看*/
export const PREVIEW_MEETTOPICDELDOC = 'meeting_PREVIEW_MEETTOPICDELDOC';
export const PREVIEW_MEETTOPICADDDOC = 'meeting_PREVIEW_MEETTOPICADDDOC';
export const PREVIEW_MEMBER_SETNEWREMARK = 'meeting_PREVIEW_MEMBER_SETNEWREMARK';
export const PREVIEW_MEMBER_SETNEWREPLY = 'meeting_PREVIEW_MEMBER_SETNEWREPLY';
export const PREVIEW_MEMBER_SETTOPICNEWTIME = 'meeting_PREVIEW_MEMBER_SETTOPICNEWTIME';



/**
 * 会议室使用情况
 */
export const ROOMPLAN_LOADING = 'meeting_roomplan_loading';
export const ROOMPLAN_SETORG = 'ROOMPLAN_SETORG';

/**
 * 查询会议
 */
export const SEARCH_LOADING = 'meeting_search_loading'
export const SET_SHOW_SEARCHAD_SEARCH ='meeting_set_show_searchad_search'//高级搜索框显示
export const SAVE_FIELDS_SEARCH = 'meeting_save_fields_search'
export const SEARCH_SAVEPREVIEWMEETINGID = 'meeting_search_savepreviewmeetingid'


/**
 * 会议任务
 */
export const DECISION_LOADING = 'meeting_decision_loading'
export const DECISION_CONDITION = 'meeting_decision_condition'
export const SET_SHOW_SEARCHAD_DECISION = 'meeting_set_show_searchad_decision'
export const DECISION_SAVEPREVIEWMEETINGID = 'meeting_decision_savepreviewmeetingid'

/**
 * 会议报表
 */
export const REPORT_LOADING = 'meeting_report_loading'
export const REPORT_SETSEARCH_TIME = 'meeting_report_setsearch_time'
export const REPORT_SETSEARCH_TYPE = 'meeting_report_setsearch_type'
export const REPORT_GETUSER_INFO = 'meeting_report_getuser_info'
export const REPORT_ABSENT = 'meeting_report_absent'
export const REPORT_RESOLUT = 'meeting_report_resolut'
export const REPORT_PROCESSD = 'meeting_report_processd'
export const REPORT_CHANGEREPORTTAB = 'meeting_report_changereporttab'




/**
 * 周期会议
 */
export const REPEAT_LOADING = 'meeting_repeat_loading'
export const SET_SHOW_SEARCHAD_REPEAT ='meeting_set_show_searchad_repeat'//高级搜索框显示
export const SAVE_FIELDS_REPEAT = 'meeting_save_fields_repeat'
export const REPEAT_ADVANCEMODAL = 'meeting_repeat_advancemodal'
export const REPEAT_ADVANCEMODALPARAMS = 'meeting_repeat_advancemodal_params'
export const REPEAT_CONTROLMODAL = 'meeting_repeat_controlmodal'
export const REPEAT_CREATEDATE = 'meeting_repeat_createdate'
export const REPEAT_GETREPEATDATAS = 'meeting_repeat_getrepeatdatas'
export const REPEAT_SAVEPREVIEWMEETINGID = 'meeting_repeat_savepreviewmeetingid'


/**
 * 会议查询条件
 */
export const SEARCH_CONDITION ='meeting_search_condition'
/**
 * 高级搜索框显示
 */
// export const SET_SHOW_SEARCHAD ='meeting_set_show_searchad'
/**
 * 高级搜所表单
 */
export const SAVE_FIELDS = 'meeting_save_fields'
/**
 * 终止周期会议
 */
// export const STOP_INTVL = 'meeting_stop_intvl'//未用到
/**
 * 提前结束周期会议
 */
// export const CHG_INTVL = 'meeting_chg_intvl'//未用到
/**
 * 取消周期会议
 */
// export const CANCEL_INTVL = 'meeting_cancel_intvl'//未用到
