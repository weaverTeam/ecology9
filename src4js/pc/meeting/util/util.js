import {WeaInput, WeaSelect, WeaBrowser, WeaCheckbox} from 'ecCom';
const util = {
    href: {
        doc: (docId, meetingId) => `/docs/docs/DocDsp.jsp?id=${docId}&meetingid=${meetingId}`
    },
    getBrowserDatas: (record, key)=> {
        let replaceDatas = [];
        if (record[key + 'span'] !== undefined) {
            let keys = record[key].split(',');
            let values = record[key + 'span'].split(',');
            if (keys.length === values.length) {
                keys.map((k, i) => {
                    replaceDatas.push({id: k, name: values[i]});
                });
            } else {
                console && console.error('浏览按钮数据有误！！显示名中不能包含有英文逗号" , "');
            }
        }
        return replaceDatas;
    },
    tableCol: {
        render: (col) => {
            const {com = []} = col;
            return (text, record, index) => {
                let _com = [];
                com.map(c => {
                    if (typeof c.props === 'object') {
                        _com.push(c);
                    } else {
                        const {key, label = '', type = 'INPUT', options = [], browserConditionParam = {}, width = 120} = c;
                        const viewAttr = 1;
                        const _type = type.toUpperCase();
                        _com.push(
                            <span >
                            {label && <span style={{marginLeft: 5}}>{label}</span>}
                                { _type === 'INPUT' &&
                                <WeaInput
                                    defaultValue={record[key]}
                                    value={record[key]}
                                    style={{width, display: 'inline-block'}}
                                    viewAttr={ viewAttr }
                                />
                                }
                                { _type === 'SELECT' &&
                                <WeaSelect
                                    defaultValue={record[key]}
                                    value={record[key]}
                                    options={options}
                                    style={{width, display: 'inline-block'}}
                                    viewAttr={ viewAttr }
                                />
                                }
                                { _type === 'BROWSER' &&
                                <WeaBrowser
                                    replaceDatas={util.getBrowserDatas(record, key)}
                                    {...browserConditionParam}
                                    inputStyle={{width, display: 'inline-block'}}
                                    viewAttr={ viewAttr }
                                />
                                }
                                { _type === 'CHECKBOX' &&
                                <WeaCheckbox
                                    style={{width, display: 'inline-block'}}
                                    value={record[key]}
                                    viewAttr={ viewAttr }
                                />
                                }
                        </span>
                        )
                    }
                });
                return (
                    <div>
                        {_com}
                    </div>
                )
            }
        }
    }
};
export default util;