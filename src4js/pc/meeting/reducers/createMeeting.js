import * as types from '../constants/ActionTypes'

let initialState = {
	title: "新建会议",
	loading: false,
	datas:'',//e9会议-新建会议页面显示内容
	newOrderFields:'',
	topicColumns:'',//新建议程的列
	topicDatas:'',//新建议程的数据
	serviceColumns:'',//新建服务的列
	serviceDatas:'',//新建服务的数据
	serviceKeycol:'',
	caller:'',
	previewModal:false,
	newmeetingTab:'1',
	meetingid:'',
	savetype:'',
	errorMessage:'',
};

export default function createMeeting(state = initialState, action) {
	switch(action.type) {
		case types.CREATEMEETING_LOADING:
			return {...state, loading: action.loading};

		case types.CREATE_CHANGENEWMEETINGTAB:
			return {...state, 
				newmeetingTab: action.data.newMeetingTab,
				errorMessage:action.data.errorMessage,
			}
		case types.CALVIEW_PREVIEWMODAL:
			return {...state, previewModal: action.data}
		case types.CREATE_GETMEETINGBASEFIELD:
			return {...state, datas: action.data}
		case types.CREATE_SAVE_FIELDS:
			return {...state, newOrderFields: action.data}
		case types.CREATE_GETMEETINGTOPICFIELDC:
			return {...state, 
				topicColumns: action.data.topicColumns,
				topicDatas: action.data.topicDatas,
			}
		case types.CREATE_GETMEETINGTOPICFIELDD:
			return {...state, topicDatas: action.data}
		case types.CREATE_GETMEETINGSERVICEFIELDC:
			return {...state,
				serviceColumns: action.data.serviceColumns,
				serviceDatas: action.data.serviceDatas,
				serviceKeycol:action.data.serviceKeycol,
			}
		case types.CREATE_GETMEETINGSERVICEFIELDD:
			return {...state, serviceDatas: action.data}
		case types.CREATE_NEWMEETING:
			return {...state,
				meetingid: action.data.meetingid,
				savetype:action.data.type,
				newmeetingTab:action.data.newmeetingTab,
				errorMessage:action.data.errorMessage,
			}
			
		case types.CREATE_CHANGEBROSERMEETINGTYPE:
			return {...state, caller: action.data}
		case types.CREATE_SAVEERRORMESSAGE:
			return {...state, errorMessage: action.data}
			
		default:
			return state
	}
}