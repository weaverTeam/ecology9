import * as types from '../constants/ActionTypes'

let initialState = {
	title: "周期会议",
	loading: false,
	conditioninfo:[],
	searchParamsAd:{},
    showSearchAd:false,
	fields:{},
	dataKey:"",
	meetingid:"",
	enddate:"",
	topTab:[],
	advanceModal:false,
	advanceModalparams:'',
	controlModal:false,
	createDate:new Date(),
	previewMeetingId:'',

};

export default function repeat(state = initialState, action) {
	switch(action.type) {
		case types.SEARCH_LOADING:
			return {...state, loading: action.loading,dataKey:action.dataKey};
		case types.SEARCH_CONDITION:
			return {...state,conditioninfo:action.conditioninfo,topTab:action.topTab};
		case types.SET_SHOW_SEARCHAD_REPEAT:
			return {...state,showSearchAd:action.value};
		case types.SAVE_FIELDS_REPEAT:
			return {...state,fields:action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()};
		case types.STOP_INTVL:
			return {meetingid:action.meetingid}
		case types.REPEAT_ADVANCEMODAL:
			return {...state, advanceModal: action.data}
		case types.REPEAT_ADVANCEMODALPARAMS:
			return {...state, advanceModalparams: action.data}
		case types.REPEAT_CREATEDATE:
			return {...state, createDate: action.data}
		case types.REPEAT_CONTROLMODAL:
			return {...state, controlModal: action.data}
		case types.REPEAT_GETREPEATDATAS:
			return {...state, dataKey: action.data}
		case types.REPEAT_SAVEPREVIEWMEETINGID:
			return {...state, previewMeetingId: action.data}	
		default:
			return state
	}
}
