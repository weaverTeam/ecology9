import * as types from '../constants/ActionTypes'

let initialState = {
	title: "会议任务",
	loading: false,
	setsearchtime:'3',
	setsearchtype:'1',
	userinfo:'',
	absent:{},
	resolut:{},
	processd:{},
	reporttab:'1',
};

export default function decision(state = initialState, action) {
	switch(action.type) {
		case types.REPORT_LOADING:
			return {...state, loading: action.loading};
		case types.REPORT_SETSEARCH_TIME:
			return {...state, setsearchtime: action.data};
		case types.REPORT_SETSEARCH_TYPE:
			return {...state, setsearchtype: action.data};
		case types.REPORT_GETUSER_INFO:
			return {...state, userinfo: action.data};
		case types.REPORT_ABSENT:
			return {...state, absent: action.data};
		case types.REPORT_RESOLUT:
			return {...state, resolut: action.data};
		case types.REPORT_PROCESSD:
			return {...state, processd: action.data};
		case types.REPORT_CHANGEREPORTTAB:
			return {...state, reporttab: action.data};
			
			
		default:
			return state
	}
}