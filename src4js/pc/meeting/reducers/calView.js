import * as types from '../constants/ActionTypes'

let initialState = {
	title: "会议日历",
	loading: false,
	type:1,//1:表格，2:日历
	meetingType:0,//会议进行状态 0 所有 1已结束 2进行中 3未开始
	nextMeeting:'',
	data:'',
	sessionkey:'',
	month:'',
	date:'',//日期
	controlModal:false,
	datas:'',//e9会议-新建会议页面显示内容
	orderFields:'',
	viewMeetingField:'',//查看会议全部内容
	caller:'',
	previewModal:false,
	newmeetingTab:'1',
	createDate:new Date(),
	previewMeetingId:'',//查看会议-会议ID
	createMeetType:'create',

};

export default function calView(state = initialState, action) {
	switch(action.type) {
		case types.CALVIEW_LOADING:
			return {...state, loading: action.loading};
		case types.CALVIEW_CHANGETYPE:
			return {...state, type: action.data}
		case types.CALVIEW_CHANGEMEETINGTYPE:
			return {...state, meetingType: action.data}
		case types.CALVIEW_CHANGENEWMEETINGTAB:
			return {...state, newmeetingTab: action.data}
		case types.CALVIEW_GETCALENDARLIST:
			return {...state, sessionkey: action.data}
		case types.CALVIEW_GETNEXTMEETING:
			return {...state, nextMeeting: action.data}
		case types.CALVIEW_GETDATA:
			return {...state, data: action.data}
		case types.CALVIEW_CHANGEMONTH:
			return {...state, month: action.data}
		case types.CALVIEW_SAVEDATE:
			return {...state, date: action.data}
		case types.CALVIEW_CREATEDATE:
			return {...state, createDate: action.data}
		case types.CALVIEW_CONTROLMODAL:
			return {...state, 
				controlModal: action.data.control,
				previewMeetingId:action.data.previewMeetingId,
				createMeetType:action.data.type,
			}
		case types.CALVIEW_GETMEETINGBASEFIELD:
			return {...state, datas: action.data}
		case types.CALVIEW_SAVE_FIELDS:
			return {...state, orderFields: action.data}
		
		case types.CALVIEW_NEWMEETING:
			return {...state, meetingid: action.data}
		case types.CALVIEW_CHANGEBROSERMEETINGTYPE:
			return {...state, caller: action.data}
		case types.CALVIEW_SAVEPREVIEWMEETINGID:
			return {...state, previewMeetingId: action.data}
			
			
		default:
			return state
	}
}