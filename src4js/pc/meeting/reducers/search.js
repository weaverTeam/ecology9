import * as types from '../constants/ActionTypes'

let initialState = {
	title: "查询会议",
	loading: false,
	conditioninfo:[],
	searchParamsAd:{},
    showSearchAd:false,
	fields:{},
	dataKey:"",
	topTab:[],
	previewMeetingId:'',
};

export default function search(state = initialState, action) {
	switch(action.type) {
		case types.SEARCH_LOADING:
			return {...state, loading: action.loading,dataKey:action.dataKey};
		case types.SEARCH_CONDITION:
			return {...state,conditioninfo:action.conditioninfo,topTab:action.topTab};
		case types.SET_SHOW_SEARCHAD_SEARCH:
			return {...state,showSearchAd:action.value};
		case types.SAVE_FIELDS_SEARCH:
			return {...state,fields:action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()};
        case types.SEARCH_SAVEPREVIEWMEETINGID:
			return {...state, previewMeetingId: action.data}
		default:
			return state
	}
}
