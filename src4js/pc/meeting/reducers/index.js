import meetingCalView from "./calView"
import meetingRoomPlan from "./roomPlan"
import meetingSearch from "./search"
import meetingDecision from "./decision"
import meetingRepeat from "./repeat"
import createMeeting from "./createMeeting"
import meetingReport from "./report"
import previewMeeting from "./previewMeeting"


export default {
	meetingCalView,
	meetingRoomPlan,
	meetingSearch,
	meetingDecision,
	meetingRepeat,
	createMeeting,
	meetingReport,
	previewMeeting
}