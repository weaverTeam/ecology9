import * as types from '../constants/ActionTypes'

let initialState = {
	title: "会议任务",
	loading: false,
	conditioninfo:[],
	searchParamsAd:{},
    showSearchAd:false,
	fields:{},
	dataKey:"",
	meetingid:"",
	enddate:"",
	topTab:[],
	showNew:false,
	previewMeetingId:'',
};

export default function decision(state = initialState, action) {
		switch(action.type) {
		case types.DECISION_LOADING:
			return {...state, loading: action.loading,dataKey:action.dataKey};
		case types.DECISION_CONDITION:
			return {...state,conditioninfo:action.conditioninfo,topTab:action.topTab};
		case types.SET_SHOW_SEARCHAD_DECISION:
			return {...state,showSearchAd:action.value};
		case types.SAVE_FIELDS:
			return {...state,fields:action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()};
		case types.STOP_INTVL:
			return {meetingid:action.meetingid}
		case types.SET_SHOW_NEW:
			return {...state,showNew:action.value};
		case types.DECISION_SAVEPREVIEWMEETINGID:
			return {...state, previewMeetingId: action.data}
		default:
			return state
	}
}