import * as types from '../constants/ActionTypes'

let initialState = {
	title: "查看会议",
	loading: false,
	previewModal:false,
	tabBase:{}, //基本信息
	tabTopic:'', //议程
	tabService:'', //服务
	tabMember:'', //参会情况
	tabSign:'', //签到
	tabDecision:'', //决议
	tabDiscuss:'', //相关交流
	tabSignTable:'',
	tabSignTab:false,
	tabDecisionTable:'',
	previewMeetingId:'',
	previewmeetingTab:1,
	tabs:'',

};

export default function previewMeeting(state = initialState, action) {
	switch(action.type) {
		case types.PREVIEW_LOADING:
			return {...state, loading: action.loading};
		case types.PREVIEW_PREVIEWMODAL:
			return {...state, previewModal: action.data}
		case types.PREVIEW_VIEWMEETINGFIELD:
			return {...state,
				previewModal:action.data.previewModal,
				previewMeetingId:action.data.previewMeetingId,
				tabBase: action.data.tabbase,
				tabTopic:action.data.tabtopic,
				tabService: action.data.tabservice,
				tabMember:action.data.tabmember,
				tabSign: action.data.tabsign,
				tabDecision:action.data.tabdecision,
				tabDiscuss: action.data.tabdiscuss,
				tabs:action.data.tabs,
				previewmeetingTab:action.data.previewmeetingTab,
			}
		case types.PREVIEW_GETTABSIGNTABLE:
			return {...state, tabSignTable: action.data}
		case types.PREVIEW_GETTABDECISIONTABLE:
			return {...state, tabDecisionTable: action.data}
		case types.PREVIEW_CHANGETABSIGNTAB:
			return {...state, tabSignTab: action.data}
		case types.PREVIEW_SIGNMEETINGBYHAND:
			return {...state, tabSignTab: action.data}
        case types.PREVIEW_MEETTOPICDELDOC: {
            let tabTopic = {...state.tabTopic};
            let data = {...tabTopic.datas[action.dataIndex]};
            let doclist = [];
            data.doclist.forEach((d) => {
            	if (d.id != action.params.id) {
            	    doclist.push({...d});
                }
            });
            data.doclist = doclist;
            tabTopic.datas[action.dataIndex] = data;
            return {...state, tabTopic: tabTopic};
        }
        case types.PREVIEW_MEETTOPICADDDOC: {
            let tabTopic = {...state.tabTopic};
            let dataIndex;
            tabTopic.datas.forEach((record, index) => {
            	if (action.dataRecord.id == record.id) {
                    dataIndex = index;
                }
            });
            if (!dataIndex) return state;
            tabTopic.datas[dataIndex].doclist.push(action.data);
            return {...state, tabTopic: tabTopic};
        }
        
        case types.PREVIEW_MEMBER_SETNEWREMARK: {
            let tabMember = {...state.tabMember};
            tabMember.othersremark = action.remark;
            return {...state, tabMember: tabMember};
        }
        
        case types.PREVIEW_MEMBER_SETNEWREPLY: {
            let tabMember = {...state.tabMember};
            let datas = action.tableType == 'hrm' ? tabMember.hrmdatas : tabMember.crmdatas;
            let idx = datas.findIndex((ele) => ele.recorderid == action.values.recorderid);
            let data = {...datas[idx], ...action.showValue, values: action.values};
            data.memberid = action.values.memberid;
            data.recorderid = action.values.recorderid;
            datas[idx] = data;
            return {...state, tabMember: tabMember};
        }
        case types.PREVIEW_MEMBER_SETTOPICNEWTIME: {
            let tabTopic = {...state.tabTopic};
            let dataIndex;
            tabTopic.datas.forEach((record, index) => {
                if (action.topicId == record.id) {
                    dataIndex = index;
                }
            });
            if (!dataIndex) return state;
            tabTopic.datas[dataIndex].topicdate = action.reshowArray;
            return {...state, tabTopic: tabTopic};
        }
        case types.PREVIEW_CHANGEPREVIEWMEETINGTAB:
			return {...state, previewmeetingTab: action.data}
			
		default:
			return state
	}
}