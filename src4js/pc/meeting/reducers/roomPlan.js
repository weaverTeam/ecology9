import * as types from '../constants/ActionTypes'

let initialState = {
    title: '会议室使用情况',
    loading: false,
    org: {}//左侧选择的区域
}

export default function roomPlan(state = initialState, action) {
    switch (action.type) {
        case types.ROOMPLAN_LOADING:
            return {...state, loading: action.loading};
        case types.ROOMPLAN_SETORG:
            return {...state, org: action.org};
        default:
            return state
    }
}