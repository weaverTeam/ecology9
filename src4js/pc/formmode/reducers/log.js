import Constants from '../constants/ActionTypes'

import objectAssign from 'object-assign'

const initialState = {
}

export default (state = initialState, action) => {
    switch (action.type) {
        case Constants.LOG_SHOWSEARCH:
            return objectAssign({}, state, {
                logSearchData: action.logSearchData,
                showSearchAd: action.showSearchAd
            });
        case Constants.LOG_INIT:
            return objectAssign({}, state, {
                logData: action.logData,
                total: action.total,
                current: action.current,
                pagesize: action.pagesize,
                showSearchAd: action.showSearchAd,
            });
        default:
            return state
    }
}