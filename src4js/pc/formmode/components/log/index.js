import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { WeaTable, WeaTools, WeaTop, WeaTab, WeaSearchGroup, WeaDialog } from 'ecCom';
import { WeaRightMenu, WeaPopoverHrm, WeaLeftRightLayout, WeaLeftTree, WeaReqTop, WeaHrmInput, WeaCheckbox, WeaDateGroup } from 'ecCom';
import { Icon, Button, Form, Spin, Menu, Dropdown, Tabs, Select, notification, BackTop, Alert, Table } from 'antd';
import { ModeTree, ModeTable, ModeTab, ModeLeftRightLayout } from 'mode';
import * as Actions from '../../actions/card';
import * as formAction from '../../actions/form';
import * as lodash from 'lodash';

const FormItem = Form.Item;

class LogList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    componentDidMount() {
        this.init();
    }
    init = () => {
        const { actions, billid, modeId, formId, logSearchData } = this.props;
        actions.loadLog({ billid, modeId, formId, current: 1, pagesize: 10, ...logSearchData });
        //actions.getLayoutBase({ billid, modeId, formId, type, layoutid, customid, viewfrom, opentype, fromSave, iscreate });   
    }
    handleRightMenuClick = (key) => {
        const { actions, billid, modeId, formId, logSearchData, pagesize } = this.props;
        actions.loadLog({ billid, modeId, formId, current: 1, pagesize, ...logSearchData });
    }
    getSearchs = () => {
        const { getFieldProps } = this.props.form;
        const { logSearchData } = this.props
        let items = new Array();

        items.push({
            com: (<FormItem
                label="发表人"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaHrmInput {...getFieldProps("operatorid", { "initialValue": logSearchData.operatorid }) } />
            </FormItem>),
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="操作类型"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaCheckbox {...getFieldProps("create", { "initialValue": logSearchData.create }) } />&nbsp;新建 &nbsp;&nbsp;
                <WeaCheckbox {...getFieldProps("edit", { "initialValue": logSearchData.edit }) } />&nbsp;编辑 &nbsp;&nbsp;
                <WeaCheckbox {...getFieldProps("view", { "initialValue": logSearchData.view }) } />&nbsp;查看 &nbsp;&nbsp;
                <WeaCheckbox {...getFieldProps("munedit", { "initialValue": logSearchData.munedit }) } />&nbsp;批量修改
            </FormItem>),
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="操作日期"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaDateGroup {...getFieldProps("createdateselect", {
                    initialValue: logSearchData.createdateselect
                }) } datas={[
                    { value: '0', selected: true, name: '全部' },
                    { value: '1', selected: false, name: '今天' },
                    { value: '2', selected: false, name: '本周' },
                    { value: '3', selected: false, name: '本月' },
                    { value: '4', selected: false, name: '本季' },
                    { value: '5', selected: false, name: '本年' },
                    { value: '6', selected: false, name: '指定日期范围' }
                ]} form={this.props.form} domkey={["createdateselect", "createdatefrom", "createdateto"]} />
            </FormItem>), //创建日期    createdateselect    ==6范围   createdatefrom---createdateto
            colSpan: 1
        });

        return [
            (<WeaSearchGroup col={2} needTigger={true} title="查询条件" showGroup={true} items={items} />),
        ]
    }

    onSearch = () => {
        const { actions, billid, modeId, formId, logSearchData, pagesize } = this.props;
        actions.loadLog({ billid, modeId, formId, current: 1, pagesize, ...logSearchData });
    }

    nextDatas = (pagination, filters, sorter) => {
        let current = pagination.current
        const { billid, modeId, formId, pagesize, logSearchData } = this.props
        this.props.actions.loadLog({ billid, modeId, formId, current, pagesize, ...logSearchData });
    }

    changePageSize = (current, pageSize) => {
        const { billid, modeId, formId, logSearchData } = this.props
        this.props.actions.loadLog({ billid, modeId, formId, current: 1, pagesize: pageSize, ...logSearchData });
    }

    showSearch = (bool) => {
        const { logSearchData, actions } = this.props;
        actions.setShowSearchAd(logSearchData, bool);
    }
    onReset = () => {
        this.props.actions.loadLog({ operatorid: "", create: "1", edit: "1", view: "0", munedit: "1", createdateselect: "0", createdatefrom: "", createdateto: "" }, true);
    }
    render() {
        const { logSearchData, current, pagesize, total, logData, showSearchAd, actions, billid, modeId, formId, modename } = this.props;
        return (
            <WeaRightMenu
                datas={[
                    { key: 1, disabled: false, icon: <i className='icon-search' />, content: "搜索" },
                    { key: 2, disabled: false, icon: <i className='icon-search' />, content: "关闭" }
                ]}
                width={200}
                onClick={this.handleRightMenuClick}>
                <WeaTop
                    title={modename}
                    loading='loading'
                    icon={<i className='icon-portal-workflow' />}
                    iconBgcolor='#55D2D4'
                    buttons={[<Button type="primary" onClick={() => { this.handleRightMenuClick("1") }}>搜索</Button>]}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={[
                        { key: 1, disabled: false, icon: <i className='icon-search' />, content: "搜索" }
                    ]}
                    onDropMenuClick={this.handleRightMenuClick}>
                    <WeaTab
                        buttonsAd={[
                            <Button type="primary" onClick={this.onSearch}>搜索</Button>,
                            <Button type="ghost" onClick={this.onReset}>重置</Button>,
                            <Button type="ghost" onClick={() => { this.showSearch(false) }}>取消</Button>
                        ]}
                        searchType={['advanced']}
                        setShowSearchAd={bool => { this.showSearch(bool) }}
                        searchsAd={<Form horizontal>{this.getSearchs()}</Form>}
                        keyParam='key'
                        showSearchAd={showSearchAd}
                        searchsBaseValue=""
                        onChange={this.changeData}
                    />
                    <Table
                        columns={[
                            { title: '操作者', dataIndex: 'operator', key: 'operator', width: 20 },
                            { title: '日期', dataIndex: 'operatedate', key: 'operatedate', width: 20 },
                            { title: '时间', dataIndex: 'operatetime', key: 'operatetime', width: 20 },
                            { title: '类型', dataIndex: 'operatedesc', key: 'operatedesc', width: 20 },
                            { title: 'IP', dataIndex: 'clientaddress', key: 'clientaddress', width: 20 },
                        ]}
                        loading={false}
                        pagination={{
                            current: current,
                            pageSizeOptions: ['10', '20', '30', '40', '50', '100'],
                            total: total,
                            defaultPageSize: pagesize,
                            showSizeChanger: true,
                            showQuickJumper: true,
                            showTotal: (total) => {
                                return ` 共 ${total} 行 `;
                            },
                            onShowSizeChange: (current, pageSize) => { this.changePageSize(current, pageSize) }
                        }}
                        onChange={this.nextDatas}
                        dataSource={logData}
                        heightSpace={-15}
                        height={this._layout && this._tabs && this._layout.clientHeight - this._tabs.clientHeight}
                        style={{ marginLeft: 10, marginRight: 10 }}
                    />
                </WeaTop>
            </WeaRightMenu>
        )
    }
}
LogList = Form.create({
    onFieldsChange(props, fields) {
        const { logSearchData, actions } = props;
        for (var item in fields) {
            logSearchData[item] = fields[item].value;
        }
        console.log("logSearchData", logSearchData);
        actions.setShowSearchAd(logSearchData, true)
    },
    mapPropsToFields(props) {
        return props.orderFields || {};
    }
})(LogList)

const mapStateToProps = (state, props) => {
    const { log } = state;
    return {
        billid: props.location.query.billid,
        formId: props.location.query.formId,
        modeId: props.location.query.modeId,
        modename: props.location.query.modename,
        showSearchAd: log.showSearchAd,
        current: log.current,
        pagesize: log.pagesize,
        logData: log.logData,
        total: log.total ? log.total : 0,
        logSearchData: log.logSearchData ? log.logSearchData : { operatorid: "", create: "1", edit: "1", view: "0", munedit: "1", createdateselect: "0", createdatefrom: "", createdateto: "" }
    }
}
mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators({ ...Actions, ...formAction }, dispatch)
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(LogList);