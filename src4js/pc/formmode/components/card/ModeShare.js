import {
    WeaDialog,
    WeaTop,
    WeaRightMenu,
    WeaUpload,
    WeaSearchGroup,
    WeaSelect,
    WeaInput,
    WeaDepInput,
    WeaComInput,
    WeaBrowser
} from 'ecCom';

import { Button, Collapse, Upload, Icon, Form, Input, Col, Table, Modal } from 'antd';

import * as Util from '../../util/modeUtil';

const FormItem = Form.Item;

const InputGroup = Input.Group;

const confirm = Modal.confirm;

/**
 * 建模共享组件
 */
class ModeShare extends React.Component {

    onClose = () => {
        this.props.actions.showShare(false);
    }

    getRightMenu = () => {
        let menu = new Array();
        menu.push({ key: "1", disabled: false, icon: <i className='icon-search' />, content: "添加共享" });
        menu.push({ key: "2", disabled: false, icon: <i className='icon-search' />, content: "删除共享" });
        menu.push({ key: "3", disabled: false, icon: <i className='icon-search' />, content: "关闭窗口" });
        return menu;
    }

    getHeadBtn = () => {
        let btns = new Array();
        btns.push(<Button type="primary" onClick={() => { this.handleRightMenuClick("1") }}>添加共享</Button>);
        btns.push(<Button type="primary" onClick={() => { this.handleRightMenuClick("2") }}>删除共享</Button>);
        return btns;
    }

    checkSubmit = () => {
        const { shareData } = this.props;
        let shareType = shareData.shareType ? shareData.shareType : "1";
        if (shareType == "1") {
            let relatedid1 = shareData.relatedid1;
            return relatedid1
        } else if (shareType == "2") {
            let relatedid2 = shareData.relatedid2;
            let showlevel = shareData.showlevel;
            return relatedid2 && showlevel;
        } else if (shareType == "3") {
            let relatedid3 = shareData.relatedid3;
            let showlevel = shareData.showlevel;
            return relatedid3 && showlevel;
        } else if (shareType == "4") {
            let relatedid4 = shareData.relatedid4;
            let showlevel = shareData.showlevel;
            if (!relatedid4 || !showlevel)
                return false;
        } else if (shareType == "5") {
            let showlevel = shareData.showlevel;
            return showlevel;
        } else if (shareType == "6") {
            let relatedid6 = shareData.relatedid6;
            let showlevel = shareData.showlevel;
            let joblevel = shareData.joblevel;
            if (joblevel == "1") {
                let jobleveltext1 = shareData.jobleveltext1;
                return relatedid6 && showlevel && jobleveltext1;
            } else if (joblevel == "2") {
                let jobleveltext0 = shareData.jobleveltext0;
                return relatedid6 && showlevel && jobleveltext0;
            } else {
                return relatedid6 && showlevel;
            }

        }
    }

    handleRightMenuClick = (key) => {
        if (key == "1") {
            if (this.checkSubmit()) {
                const { billid, formId, modeId, actions } = this.props;
                let shareData = this.props.shareData;
                if (shareType == "6") {
                    if (joblevel == "1") {
                        shareData.jobleveltext = shareData.jobleveltext0;
                    } else if (joblevel == "2") {
                        shareData.jobleveltext = shareData.jobleveltext1;
                    }
                }
                actions.shareData({ billid, formId, modeId, ...shareData })
            } else {
                Util.printMessage("warning", "必填内容未填写", "必填内容未填写");
            }

        } else if (key == "2") {
            const { billid, formId, modeId, actions, selectedRowKeys } = this.props;
            if (selectedRowKeys.length == 0) {
                Modal.info({
                    title: '系统提示',
                    content: (
                        <div>
                            <p>请先选择需要删除的权限项！</p>
                        </div>
                    ),
                    onOk() { },
                });
            } else {
                confirm({
                    title: '确认需要删除这些共享吗？',
                    onOk() {
                        actions.shareDataDel({ billid, formId, modeId, delid: selectedRowKeys });
                    },
                    onCancel() { },
                });
            }

        } else if (key == "3") {
            this.props.actions.showShare(false);
        }
    }

    changeSelect = (field, value) => {
        const { shareData } = this.props;
        shareData[field] = value;
        this.props.actions.changeShareData(shareData);
    }

    changeBrowserField = (field, ids, names, datas) => {
        const { shareData } = this.props;

        shareData[field] = datas;
        shareData["relatedid"] = ids;

        this.props.actions.changeShareData(shareData);
    }

    remderShareType = (text, record, index) => {
        if (record.islink) {
            return (
                <span>
                    {record.typeTopStr}<a href={record.typeHrefStr}>{record.typeTitleStr}</a>{record.typeEndStr}
                </span>
            );
        } else {
            return text;
        }
    }

    render() {
        const { getFieldProps } = this.props.form;
        const { selectedRowKeys, actions, shareDataList, shareData, visible, title } = this.props;
        let shareType = shareData.shareType ? shareData.shareType : "1";
        let style = { marginBottom: 10 }
        //const { shareData } = this.props;
        return (
            <WeaDialog
                visible={visible}
                title={"共享"}
                style={{ width: '1000px', height: '600px' }}
                closable={true}
                iconBgcolor='#1a57a0'
                maskClosable={false}
                onCancel={this.onClose}>
                <WeaRightMenu
                    datas={this.getRightMenu()}
                    width={200}
                    onClick={this.handleRightMenuClick}
                >
                    <WeaTop
                        title={title}
                        loading='loading'
                        icon={<i className='icon-portal-workflow' />}
                        iconBgcolor='#55D2D4'
                        buttons={this.getHeadBtn()}
                        showDropIcon={true}
                        dropMenuDatas={this.getRightMenu()}
                        buttonSpace={10}
                        showDropIcon={true}
                        onDropMenuClick={this.handleRightMenuClick}
                        style={{ height: "auto" }}
                    >
                        <div style={{ overflowY: "scroll", height: "530px" }}>
                            <Form style={{ marginTop: 10 }}>
                                <FormItem
                                    style={style}
                                    label="共享类型"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 6 }}>
                                    <WeaSelect
                                        fieldName="shareType"
                                        options={[
                                            { key: "1", selected: shareData.shareType == "1" || !shareData.shareType, showname: "人员" },
                                            { key: "2", selected: shareData.shareType == "2", showname: "分部" },
                                            { key: "3", selected: shareData.shareType == "3", showname: "部门" },
                                            { key: "4", selected: shareData.shareType == "4", showname: "角色" },
                                            { key: "5", selected: shareData.shareType == "5", showname: "所有人" },
                                            { key: "6", selected: shareData.shareType == "6", showname: "岗位" }
                                        ]}
                                        value={shareData.shareType}
                                        style={{ width: "50%" }}
                                        onChange={(vlaue) => { this.changeSelect("shareType", vlaue) }}
                                    >
                                    </WeaSelect>
                                </FormItem>

                                {(shareType == "2" || shareType == "3") && <FormItem
                                    style={style}
                                    label="关系类型"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 6 }}>
                                    <WeaSelect
                                        fieldName="orgrelation"
                                        options={[
                                            { key: "1", selected: shareData.orgrelation == "1", showname: "所有上级" },
                                            { key: "2", selected: shareData.orgrelation == "2", showname: "所有下级" }
                                        ]}
                                        value={shareData.orgrelation}
                                        style={{ width: "50%" }}
                                        onChange={(vlaue) => { this.changeSelect("orgrelation", vlaue) }}
                                    >
                                    </WeaSelect>
                                </FormItem>}

                                {shareType == "1" && <FormItem
                                    style={style}
                                    label="选择"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 10 }}>
                                    <WeaBrowser
                                        type="1"
                                        title="人员"
                                        fieldName="relatedid1"
                                        viewAttr="3"
                                        replaceDatas={shareData.relatedid1 ? shareData.relatedid1 : new Array()}
                                        onChange={(ids, names, datas) => { this.changeBrowserField("relatedid1", ids, names, datas) }}
                                    />
                                </FormItem>}

                                {shareType == "2" && <FormItem
                                    style={style}
                                    label="选择"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 10 }}>
                                    <WeaBrowser
                                        type="164"
                                        title="分部"
                                        fieldName="relatedid2"
                                        viewAttr="3"
                                        replaceDatas={shareData.relatedid2 ? shareData.relatedid2 : new Array()}
                                        onChange={(ids, names, datas) => { this.changeBrowserField("relatedid2", ids, names, datas) }}
                                    />
                                </FormItem>}

                                {shareType == "3" && <FormItem
                                    style={style}
                                    label="选择"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 10 }}>
                                    <WeaBrowser
                                        type="4"
                                        title="部门"
                                        fieldName="relatedid3"
                                        viewAttr="3"
                                        replaceDatas={shareData.relatedid3 ? shareData.relatedid3 : new Array()}
                                        onChange={(ids, names, datas) => { this.changeBrowserField("relatedid3", ids, names, datas) }}
                                    />
                                </FormItem>}

                                {shareType == "4" && <FormItem
                                    style={style}
                                    label="选择"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 10 }}>
                                    <WeaBrowser
                                        type="267"
                                        title="角色"
                                        fieldName="relatedid4"
                                        viewAttr="3"
                                        replaceDatas={shareData.relatedid4 ? shareData.relatedid4 : new Array()}
                                        onChange={(ids, names, datas) => { this.changeBrowserField("relatedid4", ids, names, datas) }}
                                    />
                                </FormItem>}

                                {shareType == "6" && <FormItem
                                    style={style}
                                    label="选择"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 10 }}>
                                    <WeaBrowser
                                        type="24"
                                        title="岗位"
                                        fieldName="relatedid6"
                                        viewAttr="3"
                                        replaceDatas={shareData.relatedid6 ? shareData.relatedid6 : new Array()}
                                        onChange={(ids, names, datas) => { this.changeBrowserField("relatedid6", ids, names, datas) }}
                                    />
                                </FormItem>}

                                {shareType == "4" && <FormItem
                                    style={style}
                                    label="角色是否受范围限制"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 6 }}>

                                    <WeaSelect
                                        fieldName="isRoleLimited"
                                        options={[
                                            { key: "0", selected: shareData.isRoleLimited == "0" || !shareData.isRoleLimited, showname: "否" },
                                            { key: "1", selected: shareData.isRoleLimited == "1", showname: "是" }
                                        ]}
                                        value={shareData.isRoleLimited}
                                        style={{ width: "50%" }}
                                        onChange={(vlaue) => { this.changeSelect("isRoleLimited", vlaue) }}
                                    >
                                    </WeaSelect>
                                </FormItem>}

                                {shareType == "4" && shareData.isRoleLimited == "1" && <FormItem
                                    style={style}
                                    label="字段类型"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 16 }}>
                                    <InputGroup>
                                        <Col span="5">
                                            <WeaSelect
                                                fieldName="rolefieldtype"
                                                options={[
                                                    { key: "1", selected: shareData.rolefieldtype == "1" || !shareData.rolefieldtype, showname: "人员" },
                                                    { key: "2", selected: shareData.rolefieldtype == "2", showname: "部门" },
                                                    { key: "3", selected: shareData.rolefieldtype == "3", showname: "分部" }
                                                ]}
                                                value={shareData.rolefieldtype}
                                                style={{ width: "50%" }}
                                                onChange={(vlaue) => { this.changeSelect("rolefieldtype", vlaue) }}
                                            >
                                            </WeaSelect>
                                        </Col>
                                        <Col span="3">
                                            <p className="ant-form-split">限制字段</p>
                                        </Col>
                                    </InputGroup>
                                </FormItem>}

                                {shareType == "4" && <FormItem
                                    style={style}
                                    label="共享级别"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 6 }}>
                                    <WeaSelect
                                        fieldName="rolelevel"
                                        options={[
                                            { key: "1", selected: shareData.rolelevel == "1", showname: "部门" },
                                            { key: "2", selected: shareData.rolelevel == "2", showname: "分部" },
                                            { key: "3", selected: shareData.rolelevel == "3" || !shareData.rolelevel, showname: "总部" },
                                        ]}
                                        value={shareData.rolelevel}
                                        style={{ width: "50%" }}
                                        onChange={(vlaue) => { this.changeSelect("rolelevel", vlaue) }}
                                    >
                                    </WeaSelect>
                                </FormItem>}

                                {shareType == "6" && <FormItem
                                    style={style}
                                    label="岗位级别"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 16 }}>
                                    <InputGroup>
                                        <Col span="5">
                                            <WeaSelect
                                                fieldName="joblevel"
                                                options={[
                                                    { key: "1", selected: shareData.joblevel == "1", showname: "指定部门" },
                                                    { key: "2", selected: shareData.joblevel == "2", showname: "指定分部" },
                                                    { key: "3", selected: shareData.joblevel == "3" || !shareData.joblevel, showname: "总部" },
                                                ]}
                                                value={shareData.joblevel}
                                                style={{ width: "50%" }}
                                                onChange={(vlaue) => { this.changeSelect("joblevel", vlaue) }}
                                            >
                                            </WeaSelect>
                                        </Col>
                                        <Col span="10">
                                            {shareType == "6" && shareData.joblevel == "1" &&
                                                <WeaBrowser type="4" title="部门" fieldName="jobleveltext1" viewAttr="3" {...getFieldProps("jobleveltext1", { "initialValue": shareData.jobleveltext1 }) } />
                                            }
                                            {shareType == "6" && shareData.joblevel == "2" &&
                                                <WeaBrowser type="164" title="分部" fieldName="jobleveltext0" viewAttr="3" {...getFieldProps("jobleveltext0", { "initialValue": shareData.jobleveltext0 }) } />
                                            }
                                        </Col>
                                    </InputGroup>
                                </FormItem>}

                                {shareType && shareType != "1" && <FormItem
                                    style={style}
                                    label="安全级别"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 12 }}>
                                    <InputGroup>
                                        <Col span="5">
                                            <WeaInput id="showlevel" fieldName="showlevel" name="showlevel" viewAttr="3" {...getFieldProps("showlevel", { "initialValue": shareData.showlevel }) } />
                                        </Col>
                                        <Col span="2">
                                            <p className="ant-form-split">-</p>
                                        </Col>
                                        <Col span="5">
                                            <WeaInput id="showlevel2" fieldName="showlevel2" name="showlevel2" {...getFieldProps("showlevel2", { "initialValue": shareData.showlevel2 }) } />
                                        </Col>
                                    </InputGroup>
                                </FormItem>}

                                <FormItem
                                    style={{ marginTop: -5, ...style }}
                                    label="权限项"
                                    labelCol={{ span: 6 }}
                                    wrapperCol={{ span: 6 }}>
                                    <WeaSelect
                                        fieldName="rightType"
                                        options={[
                                            { key: "1", selected: shareData.rightType == "1" || !shareData.rightType, showname: "查看" },
                                            { key: "2", selected: shareData.rightType == "2", showname: "编辑" },
                                            { key: "3", selected: shareData.rightType == "3", showname: "完全控制" },
                                        ]}
                                        value={shareData.rightType}
                                        style={{ width: "50%" }}
                                        onChange={(vlaue) => { this.changeSelect("rightType", vlaue) }}
                                    >
                                    </WeaSelect>
                                </FormItem>
                            </Form>


                            <WeaTop
                                title={"共享权限"}
                                loading='loading'
                                icon={<i className='icon-portal-workflow' />}
                                iconBgcolor='#55D2D4'
                                buttons={[
                                    <Button icon="plus" onClick={() => { this.handleRightMenuClick("1") }}></Button>,
                                    <Button icon="minus" onClick={() => { this.handleRightMenuClick("2") }}></Button>
                                ]}
                                showDropIcon={false}
                                style={{ height: "auto" }}
                            />



                            <Table
                                columns={[{
                                    title: '共享来源',
                                    dataIndex: 'source',
                                    width: 80
                                }, {
                                    title: '共享类型',
                                    dataIndex: 'type',
                                    render: this.remderShareType,
                                    width: 280
                                }, {
                                    title: '共享级别',
                                    dataIndex: 'level',
                                    width: 280
                                }, {
                                    title: '共享对象',
                                    dataIndex: 'object',
                                    render: (text, record) => (
                                        <span>
                                            <a href={record.href}>{record.title}</a>
                                        </span>
                                    )
                                }, {
                                    title: '权限',
                                    dataIndex: 'right',
                                }]}
                                dataSource={shareDataList}
                                pagination={false}
                                rowSelection={{
                                    getCheckboxProps: share => ({
                                        disabled: !share.showcheckbox,
                                    }),
                                    onChange(selectedRowKeys, selectedRows) {
                                        actions.changeShareSelectrd(selectedRowKeys);
                                    }
                                }}
                            />

                        </div>
                    </WeaTop>
                </WeaRightMenu>
            </WeaDialog >
        )
    }

}

ModeShare = Form.create({
    onFieldsChange(props, fields) {
        const { shareData, actions } = props;
        for (var item in fields) {
            shareData[item] = fields[item].value;
            if (item.indexOf("relatedid") > -1) {
                shareData["relatedid"] = fields[item].value;
            }
        }
        actions.changeShareData(shareData)
    },
    mapPropsToFields(props) {
        return props.orderFields || {};
    }
})(ModeShare);

export default ModeShare;