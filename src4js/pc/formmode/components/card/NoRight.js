import React from 'react';

/**
 * 无权限页面
 */
class NoRight extends React.Component {

    render() {
        return (
            <div style={{ width: "100%", position: "absolute", top: "20%", textAlign: "center", verticalAlign: "middle" }}>
                <img src="/images/ecology8/noright_wev8.png" width="162px" height="162px" />
                <div style={{ color: "rgb(255,187,14)" }}>对不起，您暂时没有权限！</div>
            </div>
        )
    }
}

export default NoRight