import { WeaDialog, WeaTop, WeaRightMenu, WeaUpload } from 'ecCom';

import { Button, Collapse, Upload, Icon } from 'antd';

const Panel = Collapse.Panel;

/**
 * 建模明细导入组件
 */
class ImportDetail extends React.Component {

    handleRightMenuClick = (key) => {
        if (key == "1") {
            const { modeId, billid, formId, excelData } = this.props;
            if (excelData.length > 0) {
                let filelink = excelData[0].filelink;
                this.props.actions.detailImport({ modeId, billid, formId, filelink });
                this.props.actions.showImportDetail(false);
            } else {
                alert("请选择")
            }

        }
        if (key == "2") {
            this.props.actions.showImportDetail(false);
        }
    }

    onClose = () => {
        this.props.actions.showImportDetail(false);
    }

    change = (idsT, idsB) => {
        let data = new Array();
        if (idsB.length > 0) {
            data.push(idsB[idsB.length - 1]);
        }
        this.props.actions.changeExcelFile(data);
    }

    uploading = (status) => {
        alert(status);
    }

    render() {
        const { title, visible, excelData } = this.props;
        let viewType = excelData.length > 0 ? "1" : "2"
        return (
            <WeaDialog
                visible={visible}
                title={title}
                style={{ width: '550px', height: '700px' }}
                closable={true}
                iconBgcolor='#1a57a0'
                maskClosable={false}
                onCancel={this.onClose}>
                <WeaRightMenu
                    datas={[
                        { key: 1, disabled: false, icon: <i className='icon-search' />, content: "明细导入" },
                        { key: 2, disabled: false, icon: <i className='icon-search' />, content: "关闭" }
                    ]}
                    width={200}
                    onClick={this.handleRightMenuClick}
                >
                    <WeaTop
                        title="明细导入"
                        loading='loading'
                        icon={<i className='icon-portal-workflow' />}
                        iconBgcolor='#55D2D4'
                        buttons={[<Button type="primary" onClick={() => { this.handleRightMenuClick("1") }}>明细导入</Button>]}
                        buttonSpace={10}
                        showDropIcon={true}
                        onDropMenuClick={this.handleRightMenuClick}
                        style={{ height: "auto" }}
                    >
                        <Collapse defaultActiveKey={['1', '2', '3']} >
                            <Panel header={'下载模板'} key="1">
                                <table>
                                    <colgroup>
                                        <col width="20%" />
                                        <col width="80%" />
                                    </colgroup>
                                    <tr>
                                        <td>
                                            1、下载模板
                                        </td>
                                        <td>
                                            <a href="/weaver/weaver.file.ExcelOut" style={{ color: "blue" }}>excel布局测试模块</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            2、上传Excel
                                        </td>
                                        <td>
                                            <WeaUpload
                                                value=""
                                                uploadId="excelfile"
                                                uploadUrl="/api/formmode/card/detailImportUpload?category=1"
                                                autoUpload={true}
                                                viewAttr={viewType}
                                                category="1"
                                                btnSize="small"
                                                limitType="xls,xlt,xlsx,xlsm"
                                                onChange={this.change}
                                                datas={excelData}
                                            />
                                        </td>
                                    </tr>
                                </table>
                            </Panel>
                            <Panel header={'操作步骤'} key="2">
                                <div className="item-precautions"><p>1）请先下载excel布局测试模块进行填写导入明细内容，第一行为表单明细字段的名称。</p></div>
                                <div className="item-precautions0"><p>2）请在第2步中选择填写好的Excel文件，并点击右键菜单中明细导入</p></div>
                            </Panel>
                            <Panel header={'使用注意事项：'} key="3">
                                <div className="item-precautions"><p>1）导入明细数据时会将原有明细内容删除，导入新的模板中内容。</p></div>
                                <div className="item-precautions"><p>2）只有可编辑的字段才能进行导入，如果查看的字段有默认值的会自动加上默认值。</p></div>
                                <div className="item-precautions"><p>3）模板中第一行为表单明细字段名称，从第二行开始导入明细数据。</p></div>
                                <div className="item-precautions"><p>4）明细数据之间不能有空行。</p></div>
                                <div className="item-precautions"><p>5）如果有多个明细时模板中会有多个SHEET，一个明细一个SHEET。</p></div>
                                <div className="item-precautions"><p>6）数字类型字段不要有特殊格式，例如：科学计数法，千分位，货币符号等。</p></div>
                                <div className="item-precautions"><p>7）浏览类型字段直接输入名称或主键，例如：人力资源字段直接输入人员名称或主键。</p></div>
                                <div className="item-precautions"><p>8）check类型字段输入“1/0”或“是/否”。</p></div>
                                <div className="item-precautions"><p>9）下拉选择框类型字段输入下拉选择框显示名称。(百分比格式的需要改为文本格式)</p></div>
                                <div className="item-precautions"><p>10）日期类型字段导入时模板中需改为日期格式。</p></div>
                                <div className="item-precautions"><p>11）人力资源字段支持编号导入，excel模板中的格式为：workcode_具体编号。</p></div>
                                <div className="item-precautions"><p>12）部门字段支持编号导入，excel模板中的格式为：deptcode_具体编号。</p></div>
                                <div className="item-precautions"><p>13）<span style={{ color: "red" }}>导入虚拟部门、分部，数据需以virtual_开头，例如：virtual_泛微</span></p></div>
                                <div className="item-precautions"><p>14）<span style={{ color: "red" }}>模板中红色区域的内容不允许修改，新增行的数据ID必须为空。</span></p></div>
                                <div className="item-precautions0"><p>15）<span style={{ color: "red" }}>浏览类型字段根据名称导入，如果名称为整数，则需要加上前缀name_，例如：name_名称。</span></p></div>
                            </Panel>
                        </Collapse>
                    </WeaTop>
                </WeaRightMenu>
            </WeaDialog>
        )
    }
}

export default ImportDetail;