import React from 'react';
import FormLayoutTr from './FormLayoutTr'
import Immutable from 'immutable'
import ImageTr from './ImageTr'
import { is } from 'immutable'

/**
 * 表单建模布局组件
 */
class FormLayout extends React.Component {

    shouldComponentUpdate(nextProps) {
        return !is(this.props.mainData, nextProps.mainData)
            || !is(this.props.detailData, nextProps.detailData)
            || !is(this.props.mainFields, nextProps.mainFields)
            || !is(this.props.detailTable, nextProps.detailTable)
            || !is(this.props.modeField, nextProps.modeField)
            || !is(this.props.modeInfo, nextProps.modeInfo)
            || !is(this.props.colheads, nextProps.colheads)
            || !is(this.props.rowheads, nextProps.rowheads)
            || !is(this.props.etables, nextProps.etables)
            || !is(this.props.ecMap, nextProps.ecMap)
            || this.props.className !== nextProps.className
            || this.props.type !== nextProps.type
            || this.props.symbol !== nextProps.symbol;
    }

    render() {
        const { getFieldProps } = this.props;

        const { symbol, className, etables, colheads, rowheads, rowattrs, colattrs } = this.props;
        const { ecMap, mainFields, detailTable, mainData, detailData, type, modeInfo } = this.props;
        const { functionAttr, modeField, orderlyjson, backgroundImage, floatingObjectArray } = this.props;

        let sumWidth = 0;
        let _colheads = new Array();
        let find = false;
        for (var item in colheads) {
            _colheads.push({ id: item, h: colheads[item] });
            sumWidth += parseInt(colheads[item]);
            if (colheads[item].indexOf("%") >= 0) {
                find = true;
            }
        }

        _colheads = _colheads.sort((a, b) => {
            return parseInt(a.id.substring(4)) - parseInt(b.id.substring(4));
        });
        let style = {};
        if (symbol != "replyTable") {
            style = { margin: "0 auto" };
        }
        if (find) style.width = "100%";

        let _rowheads = new Array();
        for (var item in rowheads) {
            _rowheads.push({ id: item, h: rowheads[item] });
        }

        _rowheads = _rowheads.sort((a, b) => {
            return parseInt(a.id.substring(4)) - parseInt(b.id.substring(4));
        });

        let cols = _colheads ? _colheads.length : 0;
        let trArr = new Array();
        if (backgroundImage || floatingObjectArray) {
            trArr.push(<ImageTr
                backgroundImage={backgroundImage}
                floatingObjectArray={floatingObjectArray}
                cols={cols}
            />);
        }
        _rowheads.map((o) => {
            const r = o.h;
            const k = o.id;
            const rowArr = k.split("_");
            const row = rowArr.length > 0 ? rowArr[1] : -1;
            const rowHeight = r;
            trArr.push(<FormLayoutTr
                getFieldProps={getFieldProps}
                type={type}
                modeInfo={modeInfo}
                symbol={symbol}
                row={row}
                cols={cols}
                rowHeight={rowHeight}
                ecMap={ecMap}
                style={style}
                mainFields={mainFields}
                detailTable={detailTable}
                mainData={mainData}
                detailData={detailData}
                etables={etables}
                rowattrs={rowattrs}
                colattrs={colattrs}
                functionAttr={functionAttr}
                modeField={modeField}
                orderlyjson={orderlyjson}
                actions={this.props.actions} />);
        })

        return (
            <div>
                <table className={className} style={style}>
                    <colgroup>
                        {
                            _colheads.map((o) => {
                                return (
                                    <col style={{ width: o.h }} />
                                )
                            })
                        }
                    </colgroup>
                    <tbody>
                        {trArr}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default FormLayout