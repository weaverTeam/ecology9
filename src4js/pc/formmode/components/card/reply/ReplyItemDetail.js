import React from 'react';

import { WeaRichText } from 'ecCom'

import { Button, Icon } from 'antd';

import FormLayout from '../FormLayout'

class ReplyItemDetail extends React.Component {

    onComment = () => {
        const { data, dataid, replyListData } = this.props;
        let replyData = replyListData[dataid] ? replyListData[dataid] : {};

        replyData.commentid = data.id;
        replyData.userid = data.replyor;
        replyData.dealType = "comment";
        replyListData[dataid] = replyData;

        this.props.actions.changeEditId(replyListData, dataid);
    }
    /**
     * 当type是comment时候  需要传顶层id、replyListData、actions！ 否则评论功能无法使用
     */
    render() {
        const { className, data, type } = this.props;
        return (<div className={className} >
            {className != "" && <div className='content-right-other-html'>
                {type == "quote" ? "引用 " + data.floornum + "# " : ""}{data.replydate} {data.replytime}
            </div>}
            {type == "comment" && <div style={{ position: "relative", float: "right", top: 0 }}>
                <Icon type="file-text" onClick={this.onComment} />
            </div>}
            <div className='content-right-remark-html' dangerouslySetInnerHTML={{ __html: (data.replycontent ? data.replycontent : '') }} />
            {data.rdocument &&
                <div className='content-right-other-html'>
                    相关文档：	{data.rdocument}
                </div>
            }
            {data.rworkflow &&
                <div className='content-right-other-html'>
                    相关流程：	{data.rworkflow}
                </div>
            }
            {data.rcustomer &&
                <div className='content-right-other-html'>
                    相关客户：	{data.rcustomer}
                </div>
            }
            {data.rproject &&
                <div className='content-right-other-html'>
                    相关项目：	${data.rproject}
                </div>
            }
        </div>);
    }
}

export default ReplyItemDetail