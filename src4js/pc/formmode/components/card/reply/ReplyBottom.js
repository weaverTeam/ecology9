import React from 'react';

import { WeaRichText } from 'ecCom'

import { Row, Col, Icon, Pagination, Menu, Form, Button, Spin, BackTop, Popover } from 'antd';

/**
 * 回复评论浮动评论区域
 */
class ReplyBottom extends React.Component {

    changeText = (text) => {
        this.props.changeText(text);
    }
    render() {
        const { positionX, positionY, textId, bottomBarConfig } = this.props;
        return (
            <div className="org_box" style={{
                position: "fixed", zIndex: "10", height: 'auto', borderRadius: '5px', background: "#F0F0F0",
                marginBottom: "30px", paddingLeft: "2em", left: positionX, top: positionY
            }}>
                <span className="org_bot_cor_2" style={{
                    height: "30px", lineHeight: 0, fontSize: "60px", color: "#F0F0F0", position: "absolute",
                    right: "30px", top: "0px"
                }}>◆</span>
                <div className='remarkDiv' style={{ marginRight: 30, marginTop: 30, marginBottom: 30 }}>
                    <div className='formModeReplyContent'  >

                        <WeaRichText
                            id={textId}
                            ref={textId}
                            style={{ padding: "8px 10px 0 10px" }}
                            ckConfig={{
                                toolbar: [
                                    { nane: "document", items: ["Source"] },
                                    { name: "paragraph", items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "-", "NumberList", "BulletedList"] },
                                    { name: "styles", items: ["Format", "Font", "FontSize"] },
                                    { name: "color", items: ["TextColor"] },
                                    { name: "basicstyles", items: ["Bold", "Italic", "Underline", "Strike"] },
                                    { name: "insert", items: ["Image", "Table", "Smiley"] },
                                ],
                                extraPlugins: 'autogrow',
                                height: 100,
                                autoGrow_minHeight: 100,
                                autoGrow_maxHeight: 600,
                                removePlugins: 'resize'
                            }}
                            bottomBarConfig={bottomBarConfig}
                            onChange={v => this.changeText(v)}
                        />
                    </div>
                    <br />
                    <div className="btnDiv" style={{ position: "absolute", right: 30, bottom: 10 }}>

                        <Button type="ghost" icon="cross" onClick={() => { this.props.actions.changeReplyAttr() }} style={{ marginLeft: 10 }}>
                            &nbsp;取消
                                    </Button>

                        <Button type="primary" icon="save" onClick={this.props.doSubmit} style={{ marginLeft: 10 }}>
                            &nbsp;提交
                                    </Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default ReplyBottom