import React from 'react';

import FormLayout from '../FormLayout'

import { Button, Icon, Popconfirm } from 'antd';

import { WeaInput, WeaRichText } from 'ecCom'

import ReplyItemDetail from './ReplyItemDetail'

import * as Util from '../../../util/modeUtil'

class ReplyItem extends React.Component {

    /**
     * 按钮事件
     */
    onReload = (event, type) => {
        var e = event || window.event;
        let data = this.props.data;
        let replyListData = this.props.replyListData;

        let replyData = replyListData[data.id] ? replyListData[data.id] : {};
        if (type == "comment") {
            replyData.commentid = data.id;
            replyData.userid = data.replyor;
        }
        replyData.dealType = type;
        replyListData[data.id] = replyData;
        this.props.actions.changeEditId(replyListData, data.id, type == "comment" ? "1" : "0", e.screenX, e.screenY, (type == "quote" || type == "edit") ? "1" : "0", type, this.props.index);
    }

    /**
     * 删除
     */
    onDelete = () => {
        const { modeId, formId, billid } = this.props;
        this.props.actions.deleteReply({ id: this.props.data.id, modeId, formId, billid });
    }

    /**
     * 点赞
     */
    onLike = () => {
        const { modeId, formId, billid } = this.props;
        this.props.actions.likeReply({ id: this.props.data.id, modeId, formId, billid });
    }

    render() {
        const { actions, data, modeId, formId, billid } = this.props;
        const { replyModeInfo, replyFieldList, replayContentFieldId, replyIndex, replyListData, index } = this.props;
        let replyData = replyListData[data.id] ? replyListData[data.id] : {};
        let dealType = replyData.dealType;
        let content;

        if (dealType == "edit") {
            let realDate = replyData[dealType] ? replyData[dealType] : {};
            let realFieldData = realDate[replyModeInfo.replayContentFieldId] ? realDate[replyModeInfo.replayContentFieldId] : {}
            content = realFieldData.value ? realFieldData.value : data.replycontent;
        }
        let commentAray = new Array();
        if (data.comments) {
            for (let i = 0; i < data.comments.length; i++) {
                let comment = data.comments[i];
                commentAray.push(
                    <ReplyItemDetail
                        className={(i == data.comments.length - 1) ? "content-right-lastComment" : "content-right-comment"}
                        type="comment"
                        data={comment}
                        dataid={data.id}
                        replyListData={replyListData}
                        actions={actions}
                    />
                )
            }
        }
        let showlike = false;
        let showmore;
        let likeObj = "";
        if (data.likeList.length > 0) {
            showlike = true;
            for (let i = 0; i < (data.likeList.length > 5 ? 5 : data.likeList.length); i++) {
                let obj = data.likeList[i];
                likeObj += (i == 0 ? "" : ",") + obj.name;
            }
            showmore = (data.likeList.length > 5 ? " ... 等" + data.likeList.length + "人" : " ") + "觉得赞！";
        }
        const dpurl = "/hrm/company/HrmDepartmentDsp.jsp?id=" + data.replyor;
        const img_path = data.img_path;

        return (
            <div className='wea-formmode-reply-list-content'>
                <div className='content-left'>
                    <img src={img_path} className='content-text-left-user-img' />
                    <div style={{ 'width': '132px' }}>
                        <p>
                            <a href={`javaScript:openhrm(${data.replyor})`} onClick={event => window.pointerXY(event)}>{data.replyorname ? data.replyorname : "系统管理员"}</a>
                        </p>
                        <span>
                            <a href={dpurl} target="_blank" style={{ color: '#9b9b9b', 'white-space': 'pre-wrap' }}>
                                {data.replyorname ? data.replyorname : "系统管理员"}
                            </a>
                        </span>
                    </div>
                </div>
                <div className='content-right'>

                    {data.quotes && <ReplyItemDetail
                        className="content-right-quotes"
                        type="quote"
                        data={data.quotes}
                    />}

                    <ReplyItemDetail
                        className=""
                        type=""
                        data={data}
                    />
                    {showlike && <div style={{ marginTop: 5 }}>
                        <Icon type="heart-o" />&nbsp;{likeObj}
                        {showmore}
                    </div>}
                    {commentAray}

                    <p style={{ lineHeight: '22px', marginTop: 10, color: '#9a9a9a' }}>
                        <span style={{ marginRight: 8 }}>{`${data.replydate}`}</span>
                        <span>{`${data.replytime}`}</span>
                        <span className="btnspan">
                            #{data.floornum}  &nbsp; &nbsp;
                            {data.showEdit && <span><Icon type="edit" />
                                <a onClick={() => this.onReload(event, "edit")}>编辑</a>&nbsp; &nbsp;
                            <Icon type="delete" />
                                <Popconfirm title="确定要删除这个任务吗？" onConfirm={this.onDelete}>
                                    <a href="#">删除</a>
                                </Popconfirm>&nbsp; &nbsp;
                            </span>}
                            <Icon type="like" />
                            <a onClick={() => this.onLike()}>点赞</a> ({data.likeList.length})&nbsp; &nbsp;
                            <Icon type="copy" />
                            <a onClick={() => this.onReload(event, "quote")}>引用</a> &nbsp; &nbsp;
                            <Icon type="file-text" />
                            <a onClick={() => this.onReload(event, "comment")}>评论</a>
                        </span>
                    </p>
                </div>
            </div >
        )
    }
}
export default ReplyItem