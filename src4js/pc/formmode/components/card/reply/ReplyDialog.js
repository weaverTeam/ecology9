import React from 'react';

import { WeaRichText, WeaDialog } from 'ecCom'

import { Row, Col, Icon, Pagination, Menu, Form, Button, Spin, BackTop, Popover } from 'antd';

/**
 * 回复评论弹出区域
 */
class ReplyDialog extends React.Component {

    changeText = (text) => {
        this.props.changeText(text);
    }

    render() {
        const { actions, visible, showTitle, textId, viewContent, bottomBarConfig, onClose, doSubmit } = this.props;
        return (
            <WeaDialog
                visible={visible}
                title={showTitle}
                style={{ width: '900px', height: '350px' }}
                closable={true}
                iconBgcolor='#1a57a0'
                maskClosable={false}
                onCancel={onClose}>
                <div className='remarkDiv' style={{ marginLeft: 30, marginRight: 30, marginTop: 30, marginBottom: 30 }}>
                    <div className='formModeReplyContent'  >

                        <WeaRichText
                            id={textId}
                            ref={textId}
                            value={viewContent}
                            style={{ padding: "12px 10px 0 10px" }}
                            ckConfig={{
                                toolbar: [
                                    { nane: "document", items: ["Source"] },
                                    { name: "paragraph", items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "-", "NumberList", "BulletedList"] },
                                    { name: "styles", items: ["Format", "Font", "FontSize"] },
                                    { name: "color", items: ["TextColor"] },
                                    { name: "basicstyles", items: ["Bold", "Italic", "Underline", "Strike"] },
                                    { name: "insert", items: ["Image", "Table", "Smiley"] },
                                ],
                                extraPlugins: 'autogrow',
                                height: 180,
                                autoGrow_minHeight: 180,
                                autoGrow_maxHeight: 600,
                                removePlugins: 'resize'
                            }}
                            bottomBarConfig={bottomBarConfig}
                            onChange={v => this.changeText(v)}
                        />
                    </div>
                    <br />
                    <div className="btnDiv" style={{ position: "absolute", right: 30, bottom: 20 }}>

                        <Button type="ghost" icon="cross" onClick={() => { this.props.actions.changeReplyAttr() }} style={{ marginLeft: 10 }}>
                            &nbsp;取消
                                    </Button>

                        <Button type="primary" icon="save" onClick={doSubmit} style={{ marginLeft: 10 }}>
                            &nbsp;提交
                                    </Button>
                    </div>
                </div>
            </WeaDialog>
        )
    }
}

export default ReplyDialog