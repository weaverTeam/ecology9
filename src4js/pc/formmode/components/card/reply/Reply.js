import React from 'react';

import { WeaTab, WeaSearchGroup, WeaInput, WeaDateGroup, WeaHrmInput, WeaDepInput, WeaComInput, WeaDatePicker, WeaRichText, } from 'ecCom'

import { Row, Col, Icon, Pagination, Menu, Form, Button, Spin, BackTop, Popover } from 'antd';

import ReplyItem from './ReplyItem'

import FormLayout from '../FormLayout'

import ReplyBottom from './ReplyBottom';

import ReplyDialog from './ReplyDialog';

import * as Util from '../../../util/modeUtil'

const FormItem = Form.Item;

/**
 * 表单建模回复评论入口
 * actions 
 * replyList 评论结果集合 
 * replyFieldList 评论表单字段 
 * modeId 模块id
 * formId 表单id
 * billid 数据id
 * replyModeInfo 回复评论模块信息
 * replyData 回复内容数据（新建）
 * replyType 回复弹出框是否显示
 * replyShowType 弹出框编辑模式 new comment quote
 * showType 高级搜索是否展开
 * replySearchData 高级搜索条件
 * top 点击对象的位置高度 暂时不用
 * replyListData 明细列表数据集合
 * dataIndex 正在编辑、引用、评论的记录的index
 * replyIndex 正在编辑、引用、评论的记录的id
 * showBottom 是否显示评论下拉区域
 * showX 下拉区域显示位置X
 * showY 下拉区域显示位置Y
 * total 回复评论列表总数
 * current 回复评论列表当前页码
 * pagesize 回复评论列表单页数量
 */
class Reply extends React.Component {

    /**
     * 改变文本内容回调
     */
    changeText = (text) => {
        const { replyShowType, dataIndex, replyModeInfo, replyIndex } = this.props;
        if (replyShowType == "new") {
            let replyData = this.props.replyData;
            replyData.data[replyModeInfo.replayContentFieldId].value = text;
            this.props.actions.changeReply(replyData, "1", this.props.top, "0", "0", "new");
        } else {
            let replyListData = this.props.replyListData;

            let replyData = replyListData[replyIndex];
            let dealType = replyData.dealType;
            let realData = replyData[dealType] ? replyData[dealType] : {};
            realData[replyModeInfo.replayContentFieldId] = { fieldid: replyModeInfo.replayContentFieldId, fieldname: "replycontent", value: text };

            replyData[dealType] = realData;
            replyListData[replyIndex] = replyData;
            this.props.actions.changeReplyList(replyListData);
        }
    }

    /**
     * 关闭按钮事件
     */
    onClose = () => {
        this.props.actions.changeReplyAttr()
    }

    /**
     * 回复评论提交
     */
    doSubmit = () => {
        const { replyData, replyModeInfo, replyShowType, replyIndex, replyListData } = this.props;
        const { modeId, formId, billid } = this.props;
        let submitData = {};

        if (replyShowType == "new") {
            for (let item in replyData.data) {
                let obj = replyData.data[item];
                let value = obj.value;
                if (value) {
                    submitData["field" + item] = value;
                }
            }

            let replycontent = replyData.data[replyModeInfo.replayContentFieldId].value

            this.props.actions.saveReply({
                layoutid: replyModeInfo.layoutid, formId: replyModeInfo.formid, modeId: replyModeInfo.id, type: "1",
                reqModeId: modeId, reqBillid: billid, reqFormid: formId, from: "reply", src: "submit", iscreate: "1", isFormMode: "1",
                JSONStr: JSON.stringify(submitData), replycontent
            })
        } else {
            let detaType = replyListData[replyIndex].dealType;
            let realData = replyListData[replyIndex][detaType];
            for (let item in realData) {
                let realFieldData = realData[item];
                let value = realFieldData.value;
                if (value) {
                    submitData["field" + item] = value;
                }
            }

            let replycontent = replyListData[replyIndex][detaType][replyModeInfo.replayContentFieldId].value;
            this.props.actions.saveReply({
                billid: replyIndex, layoutid: replyModeInfo.layoutid, formId: replyModeInfo.formid, modeId: replyModeInfo.id, type: detaType == "edit" ? "2" : "1",
                reqModeId: modeId, reqBillid: billid, reqFormid: formId, from: "reply", src: detaType, iscreate: detaType == "edit" ? "0" : "1", isFormMode: "1",
                temp_Quotesid: detaType == "quote" ? replyIndex : "",
                temp_CommentTopid: detaType == "comment" ? replyIndex : "", temp_Commentid: detaType == "comment" ? replyListData[replyIndex].commentid : "",
                temp_CommentUsersid: detaType == "comment" ? replyListData[replyIndex].userid : "",
                JSONStr: JSON.stringify(submitData), replycontent
            })
        }
    }

    /**
     * 回复评论部分显示
     */
    changeReply = (event) => {
        var e = event || window.event;
        let replyData = this.props.replyData;
        let replyType = this.props.replyType;
        replyType = replyType == "1" ? "0" : "1";
        this.props.actions.changeReply(replyData, "1", e.screenY, "0", "0", "new");
    }

    /**
     * 翻页
     */
    onPageChange = (n) => {
        const { modeId, billid, formId, pagesize, replySearchData } = this.props
        this.props.actions.searchReply({ modeId, billid, formId, pagesize, current: n, ...replySearchData });
    }

    /**
     * 改变数量
     */
    onPageSizeChange = (current, pageSize) => {
        const { modeId, billid, formId, replySearchData } = this.props
        this.props.actions.searchReply({ modeId, billid, formId, current: 1, pagesize: pageSize, ...replySearchData });
    }

    /**
     * 高级搜索
     */
    onSearch = () => {
        const { modeId, billid, formId, replySearchData, pagesize } = this.props
        this.props.actions.searchReply({ modeId, billid, formId, current: 1, pagesize, ...replySearchData });
    }

    /**
     * 重置
     */
    onReset = () => {
        this.props.actions.replyCondition({ content: "", operatorid: "", createdateselect: "0", createdatefrom: "", createdateto: "", floorNum: "" }, true);
    }

    render() {
        const { actions, replyList, modeId, formId, billid, replyFieldList, replyModeInfo, replyData, replyType, showType, replySearchData, top, replyIndex, replyListData } = this.props
        const { showBottom, showX, showY, replyShowType, dataIndex, total, current, pagesize } = this.props;

        let viewContent = "";
        if (replyShowType) {
            if (replyShowType == "new") {
                viewContent = replyData.data[replyModeInfo.replayContentFieldId].value
            } else if (replyShowType == "edit") {
                let rData = replyListData[replyIndex] ? replyListData[replyIndex] : {};
                let realDate = rData[replyShowType] ? rData[replyShowType] : {};
                let realFieldData = realDate[replyModeInfo.replayContentFieldId] ? realDate[replyModeInfo.replayContentFieldId] : {}
                viewContent = realFieldData.value ? realFieldData.value : replyList[dataIndex].replycontent;
            }
        }
        let showTitle = replyShowType == "quote" ? "评论（引用）" : (replyShowType == "edit" ? "评论（编辑）" : "回复评论");

        let replayContentFieldId = "field" + replyModeInfo.replayContentFieldId;

        let replys = new Array();
        for (let i = 0; i < replyList.length; i++) {
            let obj = replyList[i];
            replys.push(
                <ReplyItem data={obj}
                    index={i}
                    actions={this.props.actions}
                    modeId={modeId}
                    formId={formId}
                    billid={billid}
                    replyModeInfo={replyModeInfo}
                    replyFieldList={replyFieldList}
                    replayContentFieldId={replayContentFieldId}
                    replyIndex={replyIndex}
                    replyListData={replyListData}
                />
            )
        }

        //计算下拉区域位置
        let positionX = -99999;
        let positionY = -99999;
        let winHeight = window.innerHeight;
        if (showBottom == "1") {
            positionX = parseInt(showX) - 713;
            positionY = parseInt(showY) - 55;
            if (positionY + 300 > winHeight)
                positionY = winHeight - 300;
        }

        let dropMenu = {};
        if (total > 0) {
            dropMenu = {
                hasDropMenu: true,
                dropIcon: <i className='icon-search-search' style={{ color: '#77da88' }
                } onClick={() => actions.changeShowSearchDrop()
                } />,
                searchType: ['drop'],
                searchsDrop: <Form horizontal> {this.getSearchs()}</Form >,
                buttonsDrop:
                [
                    <Button type="primary" onClick={this.onSearch}>搜索</Button>,
                    <Button type="ghost" onClick={this.onReset} >重置</Button>,
                    <Button type="ghost" onClick={() => { actions.changeShowSearchDrop() }} >取消</Button>
                ]
            }
        }

        return (
            <div className='wea-formmode-reply'>
                <div>
                    <ReplyDialog
                        visible={replyType != "0"}
                        textId={replayContentFieldId}
                        showTitle={showTitle}
                        bottomBarConfig={Util.getbottomBar()}
                        actions={actions}
                        viewContent={viewContent}
                        doSubmit={this.doSubmit}
                        changeText={this.changeText}
                        onClose={this.onClose}
                    />
                    {showBottom == "1" &&
                        <ReplyBottom
                            positionX={positionX}
                            positionY={positionY}
                            textId={replayContentFieldId}
                            bottomBarConfig={Util.getbottomBar()}
                            actions={actions}
                            doSubmit={this.doSubmit}
                            changeText={this.changeText}
                        />
                    }

                    <WeaTab
                        selectedKey={"1"}
                        datas={[
                            { title: '评论相关', key: "1" }
                        ]}
                        keyParam='key'
                        showSearchDrop={showType}
                        {...dropMenu}
                        buttons={this.getTabButtons()}
                    />
                    <div id="replyList" className='wea-formmode-reply-list'>
                        {replys}
                        {replys && replys.length == 0 &&
                            <div className='ant-table-placeholder' style={{ borderBottom: 0 }}>暂时没有数据</div>
                        }
                        {replys && replys.length > 0 &&
                            <Pagination
                                current={current}
                                total={total}
                                pageSize={pagesize}
                                defaultPageSize={pagesize}
                                showSizeChanger={true}
                                showQuickJumper={true}
                                showTotal={total => `共 ${total} 条`}
                                onChange={this.onPageChange}
                                total={total}
                                pageSizeOptions={['5', '10', '20', '50']}
                                onShowSizeChange={this.onPageSizeChange}
                            />
                        }
                    </div>
                </div>
            </div>

        )
    }

    /**
     * 加载高级搜索查询条件
     */
    getSearchs = () => {
        const { getFieldProps } = this.props.form;
        const replySearchData = this.props.replySearchData;
        let items = new Array();

        items.push({
            com: (<FormItem
                label="内容"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaInput {...getFieldProps("content", { "initialValue": replySearchData.content }) } />
            </FormItem>),
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="发表人"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaHrmInput {...getFieldProps("operatorid", { "initialValue": replySearchData.operatorid }) } />
            </FormItem>),
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="操作日期"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaDateGroup {...getFieldProps("createdateselect", {
                    initialValue: replySearchData.createdateselect
                }) } datas={[
                    { value: '0', selected: true, name: '全部' },
                    { value: '1', selected: false, name: '今天' },
                    { value: '2', selected: false, name: '本周' },
                    { value: '3', selected: false, name: '本月' },
                    { value: '4', selected: false, name: '本季' },
                    { value: '5', selected: false, name: '本年' },
                    { value: '6', selected: false, name: '指定日期范围' }
                ]} form={this.props.form} domkey={["createdateselect", "createdatefrom", "createdateto"]} />
            </FormItem>), //创建日期    createdateselect    ==6范围   createdatefrom---createdateto
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="楼号"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 6 }}>
                <WeaInput {...getFieldProps("floornum", { "initialValue": replySearchData.floornum }) } />
            </FormItem>),
            colSpan: 1
        });

        return [
            (<WeaSearchGroup col={2} needTigger={true} title="查询条件" showGroup={true} items={items} />),
        ]
    }

    getTabButtons = () => {
        return [(
            <Icon type="plus-circle-o" style={{ color: '#7287f2' }} onClick={this.changeReply} />
        ), (<span style={{ fontSize: 16, color: '#d6d6d6' }}>|</span>)]
    }
}

Reply = Form.create({
    onFieldsChange(props, fields) {
        const { replySearchData, actions } = props;
        for (var item in fields) {
            replySearchData[item] = fields[item].value;
        }
        actions.replyCondition(replySearchData, true)
    },
    mapPropsToFields(props) {
        return props.orderFields || {};
    }
})(Reply);

export default Reply