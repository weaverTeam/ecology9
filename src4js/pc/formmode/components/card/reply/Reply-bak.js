import React from 'react';
import {
    WeaTab,
    WeaTable,
    WeaSearchGroup,
    WeaInput,
    WeaDateGroup,
    WeaHrmInput,
    WeaDepInput,
    WeaComInput,
    WeaDatePicker,
    WeaSelect,
    WeaRichText,
    WeaTop,
    WeaDialog
} from 'ecCom'

import ReplyItem from './ReplyItem'

import { Row, Col, Icon, Pagination, Menu, Form, Button, Spin, BackTop, Popover } from 'antd';

import FormLayout from '../FormLayout'

import * as Util from '../../../util/modeUtil'

const FormItem = Form.Item;

class Reply extends React.Component {

    /**
     * 加载更多 暂时没有做异步发分页加载 后续看数据量
     */
    loadMore = () => {
        let dataLength = parseInt(this.props.dataLength);
        this.props.actions.showMore((dataLength + 5) + "");
    }

    changeText = (text) => {
        console.log("--0", "1");
        let replyModeInfo = this.props.replyModeInfo;
        let replyData = this.props.replyData;
        replyData.data[replyModeInfo.replayContentFieldId].value = text;
        this.props.actions.changeReply(replyData, "1", this.props.top, "0", "0");
    }

    changeText0 = () => {

    }

    /**
     * 回复评论提交
     */
    doSubmit = () => {
        const { replyData, replyModeInfo } = this.props;
        const { modeId, formId, billid } = this.props;
        let submitData = {};

        for (let item in replyData.data) {
            let obj = replyData.data[item];
            let value = obj.value;
            if (value) {
                submitData["field" + item] = value;
            }
        }

        let replycontent = replyData.data[replyModeInfo.replayContentFieldId].value
        //submitData['field' + replyModeInfo.replayContentFieldId] = content;

        console.log(submitData);
        this.props.actions.saveReply({
            layoutid: replyModeInfo.layoutid, formId: replyModeInfo.formid, modeId: replyModeInfo.id, type: "1",
            reqModeId: modeId, reqBillid: billid, reqFormid: formId, from: "reply", src: "submit", iscreate: "1", isFormMode: "1",
            JSONStr: JSON.stringify(submitData), replycontent
        })
    }

    /**
     * 回复评论部分显示
     */
    changeReply = (event) => {
        var e = event || window.event;
        let replyData = this.props.replyData;
        let replyType = this.props.replyType;
        replyType = replyType == "1" ? "0" : "1";
        this.props.actions.changeReply(replyData, "1", e.screenY, "0", "0");
    }

    /**
     * 高级搜索
     */
    onSearch = () => {
        const { modeId, billid, formId, replySearchData } = this.props
        this.props.actions.searchReply({ modeId, billid, formId, ...replySearchData });
    }

    /**
     * 加载高级搜索查询条件
     */
    getSearchs = () => {
        const { getFieldProps } = this.props.form;
        const replySearchData = this.props.replySearchData;
        let items = new Array();

        items.push({
            com: (<FormItem
                label="内容"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaInput {...getFieldProps("content", { "initialValue": replySearchData.content }) } />
            </FormItem>),
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="发表人"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaHrmInput {...getFieldProps("operatorid", { "initialValue": replySearchData.operatorid }) } />
            </FormItem>),
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="操作日期"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 18 }}>
                <WeaDateGroup {...getFieldProps("createdateselect", {
                    initialValue: replySearchData.createdateselect
                }) } datas={[
                    { value: '0', selected: true, name: '全部' },
                    { value: '1', selected: false, name: '今天' },
                    { value: '2', selected: false, name: '本周' },
                    { value: '3', selected: false, name: '本月' },
                    { value: '4', selected: false, name: '本季' },
                    { value: '5', selected: false, name: '本年' },
                    { value: '6', selected: false, name: '指定日期范围' }
                ]} form={this.props.form} domkey={["createdateselect", "createdatefrom", "createdateto"]} />
            </FormItem>), //创建日期    createdateselect    ==6范围   createdatefrom---createdateto
            colSpan: 1
        });

        items.push({
            com: (<FormItem
                label="楼号"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 6 }}>
                <WeaInput {...getFieldProps("floornum", { "initialValue": replySearchData.floornum }) } />
            </FormItem>),
            colSpan: 1
        });

        return [
            (<WeaSearchGroup col={2} needTigger={true} title="查询条件" showGroup={true} items={items} />),
        ]
    }

    getTabButtons = () => {
        return [(
            <Icon type="plus-circle-o" style={{ color: '#7287f2' }} onClick={this.changeReply} />
        ), (<span style={{ fontSize: 16, color: '#d6d6d6' }}>|</span>)]
    }

    render() {
        const { actions, replyList, modeId, formId, billid, replyFieldList, getFieldProps, replyModeInfo, replyData, replyType, showType, replySearchData, top, replyIndex, dataLength, replyListData } = this.props
        const { showBottom, showX, showY } = this.props;
        let replayContentFieldId = "field" + replyModeInfo.replayContentFieldId;

        let staticWidth = parseInt(document.body.clientWidth, 0);
        let labelWidth = staticWidth * 0.52;
        let fieldWidth = staticWidth * 0.26;
        let btnright = staticWidth * 0.22;

        let replys = [];
        let rsize = 0;
        let showMore = true;
        if (replyList.length > parseInt(dataLength)) {
            rsize = parseInt(dataLength);
        } else {
            rsize = replyList.length;
            showMore = false;
        }
        for (let i = 0; i < rsize; i++) {
            let obj = replyList[i];
            replys.push(
                <ReplyItem data={obj}
                    actions={this.props.actions}
                    modeId={modeId}
                    formId={formId}
                    billid={billid}
                    replyModeInfo={replyModeInfo}
                    replyFieldList={replyFieldList}
                    replayContentFieldId={replayContentFieldId}
                    replyIndex={replyIndex}
                    replyListData={replyListData}
                />
            )
        }

        let positionX = -99999;
        let positionY = -99999;
        if (showBottom == "1") {
            positionX = parseInt(showX) - 713;
            positionY = parseInt(showY) - 55;
        }
        return (
            <div className='wea-formmode-reply'>
                <div>
                    {replyType == "0" && false && <div id="remarkShadowDiv" name="remarkShadowDiv" >
                        <div id="remarkShadowDivInnerDiv" onClick={this.changeReply} title="请填写评论内容">
                            <span style={{ margin: "0 5px" }}>
                                <img style={{ verticalAlign: "middle" }} src="/images/sign/sign_wev8.png" />
                            </span>
                            <span style={{ lineHeight: "30px" }}>请填写评论内容</span>
                        </div>
                    </div>}
                    {replyType != "0" && <div id="remark_div" style={{
                        border: "1px solid #d6d6d6", background: "#FFFFFF",
                        position: "fixed", zIndex: "99998", height: 'auto', left: '25%', top: 300, borderRadius: '5px',//浮动效果暂时不用
                        marginRight: '0px',boxShadow: "2px 2px 2px #B8B8B8,-2px -2px 2px #B8B8B8,-2px 2px 2px #B8B8B8,2px -2px 2px #B8B8B8"
                    }} >
                        <WeaTop
                            title="回复评论"
                            loading='loading'
                            icon={<i className='icon-portal-workflow' />}
                            iconBgcolor='#55D2D4'
                            buttons={[]}
                            buttonSpace={10}
                        />
                        <div className='remarkDiv' style={{ marginLeft: 30, marginRight: 30, marginTop: 30, marginBottom: 30, display: replyType == "0" ? "none" : "" }}>
                            <div className='formModeReplyContent'  >

                                <WeaRichText
                                    id={replayContentFieldId}
                                    ref={replayContentFieldId}
                                    value={replyData.data[replyModeInfo.replayContentFieldId].value}
                                    style={{ padding: "12px 10px 0 10px" }}
                                    ckConfig={{
                                        toolbar: [
                                            { nane: "document", items: ["Source"] },
                                            { name: "paragraph", items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "-", "NumberList", "BulletedList"] },
                                            { name: "styles", items: ["Format", "Font", "FontSize"] },
                                            { name: "color", items: ["TextColor"] },
                                            { name: "basicstyles", items: ["Bold", "Italic", "Underline", "Strike"] },
                                            { name: "insert", items: ["Image", "Table", "Smiley"] },
                                        ],
                                        extraPlugins: 'autogrow',
                                        height: 100,
                                        autoGrow_minHeight: 100,
                                        autoGrow_maxHeight: 600,
                                        removePlugins: 'resize'
                                    }}
                                    bottomBarConfig={Util.getbottomBar()}
                                    onChange={v => this.changeText(v)}
                                />
                            </div>
                            <br />
                            <div className="btnDiv" style={{ position: "absolute", right: 30, bottom: 10 }}>

                                <Button type="ghost" icon="cross" onClick={() => { this.props.actions.changeReplyAttr() }} style={{ marginLeft: 10 }}>
                                    &nbsp;取消
                                    </Button>

                                <Button type="primary" icon="save" onClick={this.doSubmit} style={{ marginLeft: 10 }}>
                                    &nbsp;提交
                                    </Button>
                            </div>
                        </div>

                    </div>}

                    {showBottom == "1" && <div className="org_box" style={{
                        position: "fixed", zIndex: "99998", height: 'auto', borderRadius: '5px', background: "#F0F0F0",
                        marginBottom: "30px", paddingLeft: "2em", left: positionX, top: positionY
                    }}>
                        <span className="org_bot_cor_2" style={{
                            height: "30px", lineHeight: 0, fontSize: "60px", color: "#F0F0F0", position: "absolute",
                            right: "30px", top: "0px"
                        }}>◆</span>
                        <div className='remarkDiv' style={{ marginRight: 30, marginTop: 30, marginBottom: 30 }}>
                            <div className='formModeReplyContent'  >

                                <WeaRichText
                                    id={replayContentFieldId}
                                    ref={replayContentFieldId}
                                    style={{ padding: "8px 10px 0 10px" }}
                                    ckConfig={{
                                        toolbar: [
                                            { nane: "document", items: ["Source"] },
                                            { name: "paragraph", items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "-", "NumberList", "BulletedList"] },
                                            { name: "styles", items: ["Format", "Font", "FontSize"] },
                                            { name: "color", items: ["TextColor"] },
                                            { name: "basicstyles", items: ["Bold", "Italic", "Underline", "Strike"] },
                                            { name: "insert", items: ["Image", "Table", "Smiley"] },
                                        ],
                                        extraPlugins: 'autogrow',
                                        height: 100,
                                        autoGrow_minHeight: 100,
                                        autoGrow_maxHeight: 600,
                                        removePlugins: 'resize'
                                    }}
                                    bottomBarConfig={Util.getbottomBar()}
                                    onChange={v => this.changeText0(v)}
                                />
                            </div>
                            <br />
                            <div className="btnDiv" style={{ position: "absolute", right: 30, bottom: 10 }}>

                                <Button type="ghost" icon="cross" onClick={() => { this.props.actions.changeReplyAttr() }} style={{ marginLeft: 10 }}>
                                    &nbsp;取消
                                    </Button>

                                <Button type="primary" icon="save" onClick={this.doSubmit} style={{ marginLeft: 10 }}>
                                    &nbsp;提交
                                    </Button>
                            </div>
                        </div>
                    </div>}
                    <WeaTab
                        selectedKey={"1"}
                        datas={[
                            { title: '评论相关', key: "1" }
                        ]}
                        keyParam='key'
                        showSearchDrop={showType}
                        hasDropMenu={true}
                        dropIcon={<i className='icon-search-search' style={{ color: '#77da88' }} onClick={() => actions.changeShowSearchDrop()} />}
                        searchType={['drop']}
                        searchsDrop={<Form horizontal>{this.getSearchs()}</Form>}
                        buttonsDrop={[
                            <Button type="primary" onClick={this.onSearch}>搜索</Button>,
                            <Button type="ghost" >重置</Button>,
                            <Button type="ghost" onClick={() => { actions.changeShowSearchDrop() }} >取消</Button>
                        ]}
                        buttons={this.getTabButtons()}
                    />
                    <div id="replyList" className='wea-formmode-reply-list'>
                        {replys}
                        {replys && replys.length == 0 &&
                            <div className='ant-table-placeholder' style={{ borderBottom: 0 }}>暂时没有数据</div>
                        }
                        {replys && showMore &&
                            <div classnName="moreFoot" style={{ display: "block", textAlign: "center" }}>
                                <a hidefocus="" onClick={this.loadMore}>
                                    <em className="ico_load"></em>更多<em className="more_down"></em>
                                </a>
                            </div>
                        }
                    </div>
                </div>
            </div>

        )
    }
}

Reply = Form.create({
    onFieldsChange(props, fields) {
        const { replySearchData, actions } = props;
        for (var item in fields) {
            replySearchData[item] = fields[item].value;
        }
        actions.replyCondition(replySearchData, true)
    },
    mapPropsToFields(props) {
        return props.orderFields || {};
    }
})(Reply);

export default Reply