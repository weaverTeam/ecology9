import keyMirror from 'keymirror';

const Constants = keyMirror({
    /**
     * 查询列表数据
     */
    LIST_SET_DATA: null,

    /**
     * 查询列表字段信息
     */
    LIST_SET_FIELD: null,

    /**
     * 高级查询是否显示
     */
    SEARCH_ADVANCE_VISIBLE: null,

    /**
     * 查询列表基本信息
     */
    SEARCH_BASE: null,

    /**
     * 查询列表字段
     */
    SEARCH_FIELDS: null,

    /**
     * 查询列表数据
     */
    SEARCH_DATAS: null,

    /**
     * loading
     */
    SEARCH_LOADING: null,

    /**
    * 卡片页面权限
    */
    CARD_RIGHT: null,

    /**
    * 卡片页面布局信息(excel模板)
    */
    CARD_LAYOUTEXCEL: null,

    /**
     * 卡片页面布局信息(excel模板)
     */
    CARD_PAGEINIT: null,

    /**
     * 卡片页面布局信息（html模板）
     */
    CARD_LAYOUTHTML: null,

    /**
    * 卡片页面布局字段信息
    */
    CARD_LAYOUTFIELD: null,

    /**
    * 卡片页面模块字段配置信息
    */
    CARD_MODEFIELD: null,

    /**
    * 卡片页面主表数据
    */
    CARD_MAINDATA: null,

    /**
    * 卡片页面明细数据
    */
    CARD_DETAILDATA: null,

    /**
    * 卡片页面右键菜单
    */
    CARD_RIGHTMENU: null,

    /**
    * 卡片页面接口动作
    */
    CARD_MENUACTION: null,

    /**
    * 卡片页面评论回复
    */
    CARD_REPLY: null,

    /**
    * 卡片页面数据保存
    */
    CARD_DOCUBMIT: null,

    /**
    * 卡片页面日志增查
    */
    CARD_MODELOG: null,

    /**
    * 卡片页面数据导入
    */
    CARD_IMPORT: null,

    /**
    * 卡片页面共享数据
    */
    CARD_SHAREDATA: null,

    /**
    * 卡片页面触发提醒
    */
    CARD_TRIGGER: null,

    /**
     * 卡片页面改变内容
     */
    CARD_FIELDLOAD: null,

    /**
     * 回复评论刷新
     */
    CARD_REPLYLOAD: null,

    /**
     * 回复评论高级搜索
     */
    CARD_REPLSHOWDROP: null,

    /**
     * 回复评论列表条件更新
     */
    CARD_REPLYCONDITION: null,

    /**
     * 回复评论列表更新
     */
    CARD_REPLYLISTLOAD: null,

    /**
     * 回复评论列表编辑、评论、引用
     */
    CARD_REPLYEDIT: null,

    /**
     * 回复评论编辑、评论、引用字段变动
     */
    CARD_REPLYLISTDATA: null,

    /**
     * 回复评论下拉输入框
     */
    CARD_BOTTOMRELOADA: null,

    /**
     * 显示导入dialog
     */
    CARD_SHOWIMPORT: null,

    /**
     * 变更导入excel
     */
    CARD_CHANGEEXCEL: null,

    /**
     * 变更高级搜索显示
     */
    LOG_SHOWSEARCH: null,

    /**
     * 变更高级搜索显示
     */
    LOG_INIT: null,

    /**
     * 显示共享
     */
    CARD_SHOWSHARE: null,

    /**
     * 编辑共享
     */
    CARD_CHANGESHAREDATA: null,

    /**
     * 共享选中
     */
    CARD_CHANGESHARESELECTED: null,

    /**
     * 条形码,二维码是否展现
     */
    CARD_SHOW: null,
});
export default Constants