import { WeaTools } from 'ecCom';

export const url = "/api/formmode/card/";

export const right = (params) => {
    return WeaTools.callApi(`${url}right`, 'POST', params);
}
export const layoutBase = (params) => {
    return WeaTools.callApi(`${url}layoutBase`, 'POST', params);
}
export const layoutField = (params) => {
    return WeaTools.callApi(`${url}layoutField`, 'POST', params);
}
export const modeField = (params) => {
    return WeaTools.callApi(`${url}modeField`, 'POST', params);
}
export const mainData = (params) => {
    return WeaTools.callApi(`${url}mainData`, 'POST', params);
}
export const detailData = (params) => {
    return WeaTools.callApi(`${url}detailData`, 'POST', params);
}
export const rightMenu = (params) => {
    return WeaTools.callApi(`${url}rightMenu`, 'POST', params);
}
export const menuAction = (params) => {
    return WeaTools.callApi(`${url}menuAction`, 'POST', params);
}
export const reply = (params) => {
    return WeaTools.callApi(`${url}reply`, 'POST', params);
}
export const doSubmit = (params) => {
    return WeaTools.callApi(`${url}doSubmit`, 'POST', params);
}
export const modeLog = (params) => {
    return WeaTools.callApi(`${url}modeLog`, 'POST', params);
}
export const importData = (params) => {
    return WeaTools.callApi(`${url}import`, 'POST', params);
}
export const shareData = (params) => {
    return WeaTools.callApi(`${url}shareData`, 'POST', params);
}
export const shareDataDel = (params) => {
    return WeaTools.callApi(`${url}shareDataDel`, 'POST', params);
}
export const trigger = (params) => {
    return WeaTools.callApi(`${url}trigger`, 'POST', params);
}
export const getUploadFile = (params) => {
    return WeaTools.callApi(`${url}getUploadFile`, 'POST', params);
}
export const dataInput = (params) => {
    return WeaTools.callApi(`${url}dataInput`, 'POST', params);
}
export const fieldAttr = (params) => {
    return WeaTools.callApi(`${url}fieldAttr`, 'POST', params);
}
export const replySubmit = (params) => {
    return WeaTools.callApi(`${url}replySubmit`, 'POST', params);
}
export const deleteReply = (params) => {
    return WeaTools.callApi(`${url}deleteReply`, 'POST', params);
}
export const likeReply = (params) => {
    return WeaTools.callApi(`${url}likeReply`, 'POST', params);
}
export const detailImport = (params) => {
    return WeaTools.callApi(`${url}detailImport`, 'POST', params);
}


