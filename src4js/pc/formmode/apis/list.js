import { WeaTools } from 'ecCom';

export const url = "/api/formmode/list/";

export const fetchListDatas = (params) => {
    return WeaTools.callApi('/api/formmode/view/list/datas', 'POST', params);
}

export const fetchListFields = (params) => {
    return WeaTools.callApi('/api/formmode/view/list/fields', 'POST', params);
}

export const base = (params) => {
    return WeaTools.callApi(`${url}base`, 'POST', params);
}

export const fields = (params) => {
    return WeaTools.callApi(`${url}fields`, 'POST', params);
}
export const datas = (params) => {
    return WeaTools.callApi(`${url}data`, 'POST', params);
}