import Route from 'react-router/lib/Route';

import reducer from './reducers';
import actions from './actions';
import Search from './components/search';
import Card from './components/card';
import LogList from './components/log';

require("./interface");

const router = (
    <Route path="formmode" breadcrumbName="建模">
        <Route path="search" breadcrumbName="查询列表" component={Search} />
        <Route path="card" breadcrumbName="卡片页面" component={Card} />
        <Route path="logList" breadcrumbName="日志" component={LogList} />
    </Route>
);


export default {
    reducer,
    router,
    actions
}