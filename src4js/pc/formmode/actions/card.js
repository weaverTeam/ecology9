import * as API from '../apis/card';
import Constants from '../constants/ActionTypes';
import { Message, Modal } from 'antd';
import * as Util from '../util/modeUtil';

export const checkRight = (params) => {
    return (dispatch, getState) => {
        API.base(params).then(({ api_errormsg, api_status, isRight, isEdit, isDel, MaxShare }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_RIGHT,
                    isRight,
                    isEdit,
                    isDel,
                    MaxShare
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const initPage = (params) => {
    return (dispatch, getState) => {
        const base = new Promise((resolve, reject) => {
            API.layoutBase(params).then((base) => {
                resolve(base);
            })
        });

        Promise.all([base]).then(([base]) => {
            dispatch({
                type: Constants.CARD_PAGEINIT,
                loading: false,
                base
            })
        })
    }
}

export const getLayoutBase = (params) => {
    return (dispatch, getState) => {
        const base = new Promise((resolve, reject) => {
            API.layoutBase(params).then((base) => {
                resolve(base);
            })
        });
        const main = new Promise((resolve, reject) => {
            API.mainData(params).then((main) => {
                resolve(main);
            })
        })
        const detail = new Promise((resolve, reject) => {
            API.detailData(params).then((detail) => {
                resolve(detail);
            })
        })
        Promise.all([base, main, detail]).then(([base, main, detail]) => {
            dispatch({
                type: Constants.CARD_LAYOUTEXCEL,
                loading: false,
                base, main, detail
            })
        })
    }
}

export const changeField = (params) => {
    var mainData = params.mainData
    var detailData = params.detailData
    var mainFields = params.mainFields
    let index = mainFields.loadtimes ? mainFields.loadtimes : 0;
    index = index + 1
    mainFields.loadtimes = index;
    return (dispatch, getState) => {
        dispatch({
            type: Constants.CARD_FIELDLOAD,
            mainData,
            detailData,
            mainFields
        });
    }
}

export const getUploadFileInfo = (params) => {
    return (dispatch, getState) => {
        var mainData = params.mainData
        var detailData = params.detailData
        var mainFields = params.mainFields
        var symbol = params.symbol;
        var detailIndex = params.detailIndex;
        var fieldid = params.fieldid;
        var detailtype = params.detailtype;
        var isprint = params.isprint;
        var fieldvalue = params.fieldvalue;

        const dataParams = { fieldvalue: fieldvalue, detailtype: detailtype, isprint: isprint };

        API.getUploadFile(params).then(({ api_errormsg, api_status, filedatas, showBatchLoad }) => {
            let specialobj = {};
            specialobj.filedatas = filedatas;
            specialobj.showBatchLoad = showBatchLoad;
            if (symbol == "emaintable") {
                mainData.data[fieldid].value = fieldvalue;
                mainData.data[fieldid].specialobj = specialobj;
            } else {
                detailData.data[symbol].data[detailIndex][fieldid].value = fieldvalue;
                detailData.data[symbol].data[detailIndex][fieldid].specialobj = specialobj;
            }
            let s = mainFields.loadtimes ? mainFields.loadtimes : 0;
            s = s + 1
            mainFields.loadtimes = s;
            dispatch({
                type: Constants.CARD_FIELDLOAD,
                mainData,
                detailData,
                mainFields
            });
        });
    }
}
export const addrow0 = (params) => {
    var mainData = params.mainData
    var detailData = params.detailData
    return { type: Constants.CARD_FIELDLOAD, mainData: mainDatam, detailData: detailData };
}

export const getLayoutField = (params) => {
    return (dispatch, getState) => {
        API.layoutField(params).then(({ api_errormsg, api_status, mainFields, detailTable }) => {
            if (api_status) {
                dispatch({
                    type: Constants.SEARCH_BASE,
                    mainFields,
                    detailTable
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const getModeField = (params) => {
    return (dispatch, getState) => {
        API.modeField(params).then(({ api_errormsg, api_status, rowcalstr, colcalstr, inputentry, linkage, defaultvalue }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_MODEFIELD,
                    rowcalstr,
                    colcalstr,
                    inputentry,
                    linkage,
                    defaultvalue
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const getMainData = (params) => {
    return (dispatch, getState) => {
        API.mainData(params).then(({ api_errormsg, api_status, data }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_MAINDATA,
                    data
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const getDetailData = (params) => {
    return (dispatch, getState) => {
        API.detailData(params).then(({ api_errormsg, api_status, data }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_DETAILDATA,
                    data
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const getRightMenu = (params) => {
    return (dispatch, getState) => {
        API.rightMenu(params).then(({ api_errormsg, api_status, RCMenuHeight, RCMenu, url, cornerMenu }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_RIGHTMENU,
                    RCMenuHeight,
                    RCMenu,
                    url,
                    cornerMenu
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const ExcuteMenuAction = (params) => {
    return (dispatch, getState) => {
        API.menuAction(params).then(({ api_errormsg, api_status }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_MENUACTION,
                    flag: api_status
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const getReply = (params) => {
    return (dispatch, getState) => {
        API.reply(params).then(({ api_errormsg, api_status, replyList }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_REPLY,
                    replyList
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

/**
 * 回复评论列表 搜索、翻页、改变数量
 */
export const searchReply = (params) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let replySearchData = card.replySearchData;
        API.reply(params).then(({ api_errormsg, api_status, replyList, current, pagesize, total }) => {
            dispatch({
                type: Constants.CARD_REPLYLISTLOAD,
                replyList,
                showType: false,
                replyType: "0",
                replyIndex: "0",
                replySearchData,
                replyListData: {},
                replyData: card.replyData,
                replyShowType: "",
                showBottom: "0",
                current,
                pagesize,
                total
            });
        });
    }
}

/**
 * 回复评论提交方法  新建 编辑 引用 评论同一入口
 */
export const saveReply = (params) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let replyData = card.replyData;
        let replyModeInfo = card.replyModeInfo;
        replyData.data[replyModeInfo.replayContentFieldId].value = "";
        API.replySubmit(params).then(({ api_errormsg, api_status, replyList, total }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_REPLYLISTLOAD,
                    replyList,
                    showType: false,
                    replyType: "0",
                    replyIndex: "0",//重置当前操作行标
                    replySearchData: { content: "", operatorid: "", createdateselect: "0", createdatefrom: "", createdateto: "", floorNum: "" },//清空查询条件
                    replyData: replyData,
                    replyListData: {},//重置列表操作数据
                    replyShowType: "",
                    showBottom: "0",
                    current: 1,
                    pagesize: 5,
                    total
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

/**
 * 删除回复评论
 */
export const deleteReply = (params) => {
    return (dispatch, getState) => {
        let card = getState().card;
        API.deleteReply(params).then(({ api_errormsg, api_status, replyList, total }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_REPLYLISTLOAD,
                    replyList,
                    showType: false,
                    replyType: "0",
                    replySearchData: { content: "", operatorid: "", createdateselect: "0", createdatefrom: "", createdateto: "", floorNum: "" },//清空查询条件
                    replyIndex: "0",//重置当前操作行标
                    replyListData: {},//重置列表操作数据
                    replyData: card.replyData,
                    replyShowType: "",
                    showBottom: "0",
                    current: card.current,
                    pagesize: card.pagesize,
                    total
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

/**
 * 回复评论点赞
 */
export const likeReply = (params) => {
    return (dispatch, getState) => {
        let card = getState().card;
        params.current = card.current;
        params.pagesize = card.pagesize;
        API.likeReply(params).then(({ api_errormsg, api_status, userid, username, replyList, total }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_REPLYLISTLOAD,
                    replyList,
                    showType: card.showType,
                    replyType: card.replyType,
                    replyIndex: card.replyIndex,
                    replySearchData: card.replySearchData,
                    replyListData: card.replyListData,
                    replyData: card.replyData,
                    replyShowType: "",
                    showBottom: "0",
                    current: card.current,
                    pagesize: card.pagesize,
                    total
                });
            } else {
                alert(api_errormsg);
            }
        });
    }
}

/**
 * 加载日志列表数据
 */
export const loadLog = (params) => {
    return (dispatch, getState) => {
        API.modeLog(params).then(({ api_errormsg, api_status, logData, total }) => {
            if (api_status) {
                dispatch({
                    type: Constants.LOG_INIT,
                    logData,
                    total,
                    current: params.current,
                    pagesize: params.pagesize,
                    showSearchAd: false
                });
            } else {
                alert(api_errormsg);
            }
        });
    }
}

/**
 * 明细表导入
 */
export const detailImport = (params) => {
    return (dispatch, getState) => {
        API.detailImport(params).then(({ api_errormsg, api_status, msg }) => {
            if (api_status) {
                //Util.printMessage("warning", msg, msg);
                alert(msg);
                let card = getState().card;
                let newParan = {
                    billid: params.billid,//区分保存 还是保存并新建
                    modeId: params.modeId,
                    formId: params.formId,
                    type: "2",
                    customid: card.customid,
                    viewfrom: card.viewfrom,
                    opentype: card.opentype,
                    fromSave: card.fromSave,
                    iscreate: card.iscreate
                }
                const base = new Promise((resolve, reject) => {
                    API.layoutBase(newParan).then((base) => {
                        //console.log(base)
                        resolve(base);
                    })
                });
                const main = new Promise((resolve, reject) => {
                    API.mainData(newParan).then((main) => {
                        // console.log(main)
                        resolve(main);
                    })
                })
                const detail = new Promise((resolve, reject) => {
                    API.detailData(newParan).then((detail) => {
                        // console.log(detail)
                        resolve(detail);
                    })
                })
                Promise.all([base, main, detail]).then(([base, main, detail]) => {
                    //console.log(base, main, detail);
                    dispatch({
                        type: Constants.CARD_LAYOUTEXCEL,
                        loading: false,
                        base, main, detail
                    })
                })
            }
        })
    }
}

/**
 * 提交方法
 */
export const doSubmit = (params) => {
    return (dispatch, getState) => {
        API.doSubmit(params).then(({ api_errormsg, api_status, returnJs, billid }) => {

            let newParan = {
                billid: params.btntype == "3" ? "" : billid,//区分保存 还是保存并新建
                modeId: params.modeId,
                formId: params.formId,
                type: params.btntype == "5" ? 2 : (params.btntype == "3" ? 1 : 0),
                customid: params.customid,
                viewfrom: params.viewfrom,
                opentype: params.opentype,
                fromSave: params.fromSave,
                iscreate: params.iscreate,

            }
            
            const base = new Promise((resolve, reject) => {
                API.layoutBase(newParan).then((base) => {
                    //console.log(base)
                    resolve(base);
                })
            });
            const main = new Promise((resolve, reject) => {
                API.mainData(newParan).then((main) => {
                    // console.log(main)
                    resolve(main);
                })
            })
            const detail = new Promise((resolve, reject) => {
                API.detailData(newParan).then((detail) => {
                    // console.log(detail)
                    resolve(detail);
                })
            })
            Promise.all([base, main, detail]).then(([base, main, detail]) => {
                //console.log(base, main, detail);
                dispatch({
                    type: Constants.CARD_LAYOUTEXCEL,
                    loading: false,
                    base, main, detail
                })
            })
        });
    }
}

export const modeLog = (params) => {
    return (dispatch, getState) => {
        API.modeLog(params).then(({ api_errormsg, api_status, logData }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_MODELOG,
                    logData
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const importData = (params) => {
    return (dispatch, getState) => {
        API.importData(params).then(({ api_errormsg, api_status }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_IMPORT,
                    flag: api_status
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const shareData = (params) => {
    return (dispatch, getState) => {
        API.shareData(params).then(({ api_errormsg, api_status, shareDataList }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_SHAREDATA,
                    shareDataList,
                    shareData: { shareType: "1", rightType: "1", isRoleLimited: "0", rolelevel: "3", showlevel: "10", joblevel: "3" }
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const shareDataDel = (params) => {
    return (dispatch, getState) => {
        API.shareDataDel(params).then(({ api_errormsg, api_status, shareDataList }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_SHAREDATA,
                    shareDataList,
                    shareData: { shareType: "1", rightType: "1", isRoleLimited: "0", rolelevel: "3", showlevel: "10", joblevel: "3" }
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}

export const trigger = (params) => {
    return (dispatch, getState) => {
        API.trigger(params).then(({ api_errormsg, api_status }) => {
            if (api_status) {
                dispatch({
                    type: Constants.CARD_TRIGGER,
                    flag: api_status
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}