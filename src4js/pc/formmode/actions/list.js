import * as API from '../apis/list';
import Constants from '../constants/ActionTypes';
import { Message } from 'antd';

export const fetchListDatas = (params) => {
    return (dispatch, getState) => {
        API.fetchListDatas(params)
            .then(({ datas, count }) => {
                dispatch({
                    type: Constants.LIST_SET_DATA,
                    datas,
                    count
                });
            }).catch((msg) => {

            });
    }
}

export const fetchListFields = (params) => {
    return (dispatch, getState) => {
        API.fetchListFields(params)
            .then(({ fields }) => {
                dispatch({
                    type: Constants.LIST_SET_FIELD,
                    fields,
                })
            }).catch((msg) => {

            })
    }
}

export const base = (params) => {
    return (dispatch, getState) => {
        API.base(params).then(({ api_errormsg, api_status, baseset }) => {
            if (api_status) {
                dispatch({
                    type: Constants.SEARCH_BASE,
                    baseset,
                });
            } else {
                throw api_errormsg;
            }
        });
        API.fields(params).then(({ api_errormsg, api_status, fields, count }) => {
            if (api_status) {
                dispatch({
                    type: Constants.SEARCH_FIELDS,
                    fields
                });
            } else {
                throw api_errormsg;
            }
        });
    }
}
export const datas = (params) => {
    return (dispatch, getState) => {
        dispatch({
            type: Constants.SEARCH_LOADING,
            loading: true,
        });
        API.datas(params).then(({ api_errormsg, api_status, datas, pageindex: current, perpage: pageSize, totalCounts: total }) => {
            dispatch({
                type: Constants.SEARCH_LOADING,
                loading: false,
            });
            if (api_status) {
                dispatch({
                    type: Constants.SEARCH_DATAS,
                    datas,
                    current,
                    pageSize,
                    total
                })
            } else {
                throw api_errormsg;
            }
        });
    }
}


export const setSearchAdvanceVisible = (searchAdvanceVisible) => {
    return dispatch => {
        dispatch({
            type: Constants.SEARCH_ADVANCE_VISIBLE,
            searchAdvanceVisible
        });
    }
}