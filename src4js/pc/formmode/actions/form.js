import * as API from '../apis/card';
import Constants from '../constants/ActionTypes';
import { Message } from 'antd';
import * as Util from '../util/modeUtil'

//-------------------------------回复评论相关js  start-----------------------------------------//

export const changeShowSearchDrop = () => {
    return (dispatch, getState) => {
        let card = getState().card;
        var showType = card.showType;
        dispatch({
            type: Constants.CARD_REPLSHOWDROP,
            showType: !showType
        });
    }
}


export const replyCondition = (replySearchData, flag) => {
    return (dispatch, getState) => {
        dispatch({
            type: Constants.CARD_REPLYCONDITION,
            replySearchData,
            showType: flag
        });
    }
}

/**
 * 刷新评论部分
 */
export const changeReply = (replyData, replyType, top, replyIndex, showBottom, replyShowType) => {
    return (dispatch, getState) => {
        dispatch({
            type: Constants.CARD_REPLYLOAD,
            replyData,
            replyType,
            top,
            replyIndex,
            showBottom,
            replyShowType
        });
    }
}


export const changeReplyAttr = () => {
    return (dispatch, getState) => {
        let card = getState().card;
        let replyData = card.replyData;
        let replyType = card.replyType;
        let top = card.top;
        let replyIndex = card.replyIndex;
        let showBottom = card.showBottom;
        if (replyType != "0" || showBottom != "0") {
            dispatch(changeReply(replyData, "0", top, replyIndex, "0", ""));
        }
    }
}

export const changeReplyList = (replyListData) => {
    return (dispatch, getState) => {
        dispatch({
            type: Constants.CARD_REPLYLISTDATA,
            replyListData
        });
    }
}
/**
 * 刷新编辑、评论、引用
 */
export const changeEditId = (replyListData, replyIndex, showBottom, showX, showY, replyType, replyShowType, dataIndex) => {
    return (dispatch, getState) => {
        dispatch({
            type: Constants.CARD_REPLYEDIT,
            replyListData,
            replyType,
            replyShowType,
            replyIndex,
            dataIndex,
            showBottom,
            showX,
            showY
        });
    }
}

//-------------------------------回复评论相关js  end-----------------------------------------//

export const changeField = (params) => {
    let mainData = params.mainData;
    let detailData = params.detailData;
    let mainFields = params.mainFields;
    let index = mainFields.loadtimes ? mainFields.loadtimes : 0;
    index = index + 1
    mainFields.loadtimes = index;
    return (dispatch, getState) => {
        dispatch({
            type: Constants.CARD_FIELDLOAD,
            mainData,
            detailData,
            mainFields
        });
    }
}

/**
 * 修改字段信息 field  字段id 主表字段 field1000 明细 field1000_0
 */
export const changeFieldInfo = (field, value) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let mainData = jQuery.extend(true, {}, card.mainData);
        let detailData = jQuery.extend(true, {}, card.detailData);
        let mainFields = jQuery.extend(true, {}, card.mainFields);

        field = field.replace("field", "");
        let drowIndex = null;
        let fid = field
        if (field.indexOf("_") > -1) {
            fid = field.split("_")[0];
            drowIndex = field.split("_")[1];
        }
        let fieldObj = mainFields[fid];
        returnObj = Util.changeField(fieldObj, value, fieldObj.detailtable ? fieldObj.detailtable : "emaintable", drowIndex, mainData, detailData, mainFields, card);
        dispatch(changeField(returnObj));

    }
}

/**
 * 修改显示样式 field  字段id 主表字段 field1000 明细 field1000_0   viewattr：1：编辑 2：必填 3：只读 4：隐藏
 */
export const changeShowAttr = (field, viewattr) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let mainData = jQuery.extend(true, {}, card.mainData);
        let detailData = jQuery.extend(true, {}, card.detailData);
        let mainFields = jQuery.extend(true, {}, card.mainFields);
        if (!!field && !!viewattr) {
            field = field.replace("field", "");
            let drowIndex = null;
            let fid = field
            if (field.indexOf("_") > -1) {
                fid = field.split("_")[0];
                drowIndex = field.split("_")[1];
            }
            mainFields[fid].isedit = ((viewattr == "3" || viewattr == "4") ? "0" : "1");
            mainFields[fid].ishide = (viewattr == "4" ? "1" : "0");
            mainFields[fid].isview = (viewattr == "4" ? "0" : "1");
            mainFields[fid].ismandatory = (viewattr == "2" ? "1" : "0");

            dispatch({
                type: Constants.CARD_FIELDLOAD,
                mainData,
                detailData,
                mainFields
            });
        }

    }
}


/**
 * 添加行的方法  现在的逻辑是直接在调用的detailTable的addObj对象操作detailData数据 加一组明细并重新渲染  
 * addObj对象页面init时封装好 changeField方法可能会在字段值计算的时候改变addObj中字段的值
 * tablename 添加明细的表名
 * initialData 被添加的明细行字段初始值 传入JSON  格式 字段id：value    默认添加明细的值会从detailTable取addObj  initialData会追加到addObj中
 */
export const addrow = (tablename, initialData) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let mainData = jQuery.extend(true, {}, card.mainData);
        let detailData = jQuery.extend(true, {}, card.detailData);
        let mainFields = jQuery.extend(true, {}, card.mainFields);
        let detailTable = card.detailTable;

        try {
            let rowCount = detailData.data[tablename].data.length;
            let addObj = JSON.parse(detailTable[tablename].addObj);
            var key = rowCount + "";
            addObj.index = key;

            let defaultObj = detailData.data[tablename].defaultValue;
            if (defaultObj) {//处理当前页面其他字段联动计算出来的默认值
                for (let item in defaultObj) {
                    try {
                        let value = defaultObj[item];
                        addObj[item].value = value;
                    } catch (e) { console.log("addrow error on defaultValue!", e); }
                }
            }

            for (let field in initialData) {//处理addrow方法传递的默认值
                try {
                    let value = initialData[field];
                    addObj[field].value = value;
                } catch (e) { console.log("addrow error on initialData!", e); }
            }
            detailData.data[tablename].data.push(addObj);
            detailData.data[tablename].size = (parseInt(rowCount) + 1);
            let drs = detailData.detailRowSize;
            detailData.detailRowSize = (parseInt(drs) + 1);
        } catch (e) {
            console.log("addrow error!" + e);
        }

        dispatch(changeField({ mainData, detailData, mainFields }));
    }
}

/**
 * 删除行的方法 现在的逻辑是直接从detailData中把需要删除的对象删掉 并重新渲染 
 */
export const delrow = (tablename) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let mainData = jQuery.extend(true, {}, card.mainData);
        let detailData = jQuery.extend(true, {}, card.detailData);
        let mainFields = jQuery.extend(true, {}, card.mainFields);

        let rowCount = detailData.data[tablename].data.length;
        let drs = detailData.detailRowSize;
        let detail = new Array();
        let index = 0;
        let delcount = 0;
        for (let i = 0; i < rowCount; i++) {
            let detailObj = detailData.data[tablename].data[i];
            let isdel = detailObj.isChecked;
            if (isdel == "1") {
                delcount++;
            } else {
                detail.push(detailObj);
                index++;
            }
        }
        detailData.detailRowSize = (parseInt(drs) - delcount);
        detailData.data[tablename].size = (parseInt(index));
        detailData.data[tablename].data = detail;

        dispatch(changeField({ mainData, detailData, mainFields }));
    }
}

/**
 * 定向删除行方法 删除指定明细表 指定index列
 */
export const delRealRow = (tablename, indexes) => {
    return (dispatch, getState) => {
        let card = getState().card;
        let mainData = jQuery.extend(true, {}, card.mainData);
        let detailData = jQuery.extend(true, {}, card.detailData);
        let mainFields = jQuery.extend(true, {}, card.mainFields);
        let ia = indexes.split(",");

        let rowCount = detailData.data[tablename].data.length;
        let drs = detailData.detailRowSize;
        let detail = new Array();
        let index = 0;
        let delcount = 0;
        for (let i = 0; i < rowCount; i++) {
            let detailObj = detailData.data[tablename].data[i];
            if (Util.contains(ia, i + "")) {
                delcount++;
            } else {
                detail.push(detailObj);
                index++;
            }
        }
        detailData.detailRowSize = (parseInt(drs) - delcount);
        detailData.data[tablename].size = (parseInt(index));
        detailData.data[tablename].data = detail;

        dispatch(changeField({ mainData, detailData, mainFields }));
    }
}

/**
 * 复制行方法
 */
export const copyRow = (tablename) => {
    return (dispatch, getState) => {
        let card = getState().card;

        let mainData = jQuery.extend(true, {}, card.mainData);
        let detailData = jQuery.extend(true, {}, card.detailData);
        let mainFields = jQuery.extend(true, {}, card.mainFields);

        let rowCount = detailData.data[tablename].data.length;
        let drs = detailData.detailRowSize;
        let CopyDetail = new Array();
        let index = rowCount;
        for (let i = 0; i < rowCount; i++) {
            let detailObj = detailData.data[tablename].data[i];
            let isChecked = detailObj.isChecked;
            if (isChecked == "1") {
                var copyObj = jQuery.extend(true, {}, detailObj);//深度复制所有元素
                delete copyObj["id"];
                delete copyObj["isChecked"];
                copyObj.index = index;
                CopyDetail.push(copyObj);
                index++;
            }
        }
        detailData.data[tablename].size = (parseInt(rowCount) + CopyDetail.length);
        detailData.detailRowSize = (parseInt(drs) + CopyDetail.length);

        let data = detailData.data[tablename].data;
        let detail = data.concat(CopyDetail);
        detailData.data[tablename].data = detail;

        dispatch(changeField({ mainData, detailData, mainFields }));
    }
}

export const del = () => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        window.close();
    }
}
/************************************************************************** */

/**
 * 条形码 二维码区域切换显示
 */
export const show = (showBar, showQr) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.CARD_SHOW,
            showBar,
            showQr
        });
    }
}


/******************************表单相关js end*********************************/


export const showImportDetail = (showImport) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.CARD_SHOWIMPORT,
            showImport
        });
    }
}


export const changeExcelFile = (excelData) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.CARD_CHANGEEXCEL,
            excelData
        });
    }
}


export const setShowSearchAd = (logSearchData, showSearchAd) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.LOG_SHOWSEARCH,
            logSearchData,
            showSearchAd
        });
    }
}


export const showShare = (showShare) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.CARD_SHOWSHARE,
            showShare
        });
    }
}


export const changeShareData = (shareData) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.CARD_CHANGESHAREDATA,
            shareData
        });
    }
}

export const changeShareSelectrd = (selectedRowKeys) => {
    return (dispatch, getState) => {
        //reqUtil.listDoingRefresh();
        dispatch({
            type: Constants.CARD_CHANGESHARESELECTED,
            selectedRowKeys
        });
    }
}

