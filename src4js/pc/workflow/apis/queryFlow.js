import {WeaTools} from 'ecCom'

export const getQueryCondition = () => {
    return WeaTools.callApi('/api/workflow/search/condition');
}

export const getCustomQueryCondition = (params) => {
    return WeaTools.callApi('/api/workflow/customsearch/condition', 'POST', params);
}

export const queryFieldsSearch = (params) => {
    return WeaTools.callApi('/api/workflow/search/pagingresult', 'POST', params);
}

export const queryFieldsTree = (params) => {
    return WeaTools.callApi('/api/workflow/search/resulttree', 'POST', params);
}