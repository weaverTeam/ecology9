import {WeaTools} from 'ecCom'

export const getWfRequestList = params => {
    return WeaTools.callApi('/api/workflow/reqlist/getList', 'POST', params);
}

//批量将列表数据全部置为已读
export const doReadIt = params =>{
	return WeaTools.callApi('/api/workflow/reqlist/doReadIt', 'POST', params);
}

//加载流程短语
export const loadPhrasesDatas = (params) => {
	return WeaTools.callApi('/api/workflow/phrases/getPhrases', 'POST', params);
}

//保存流程短语
export const savePhrasesText = (text) =>{
	let params = {
		operation: "add",
		phraseShort: text,
		phraseDesc: text
	}
	return WeaTools.callApi('/workflow/sysPhrase/PhraseOperate.jsp', 'GET', params, 'text');
}

//批量签字意见
export const OnMultiSubmitNew2 = (remark, _reqIds, datas) => {
	let belongtoUserids = "";
	let datamap = {};
	datas && datas.map(v =>{
		datamap[v.get("requestid")] = v.get("requestnamespan");
	});
	_reqIds.split(',').map(r=>{
		let belUserid = "";
		if(!!r && r in datamap){
			const requestnamespan = datamap[r];
			const reg = new RegExp("&f_weaver_belongto_userid=(\\d+)&", "g");
			let strArr = "";
			if(strArr = reg.exec(requestnamespan)){
				belUserid = strArr[1];
			}
		}
		belongtoUserids += belUserid+",";
	})
	let params = {
		multiSubIds: _reqIds,
		remark: remark,
		pagefromtype:1,
		belongtoUserids: belongtoUserids
	};
	return WeaTools.callApi('/workflow/request/RequestListOperation.jsp', 'POST', params);
}
