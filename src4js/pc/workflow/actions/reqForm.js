import * as types from '../constants/ActionTypes'
import * as API_REQ from '../apis/req'
import * as formUtil from '../util/formUtil'
import * as formulaUtil from '../util/formulaUtil'
import * as linkageUtil from '../util/linkageUtil'
import Immutable from 'immutable'
import * as lodash from 'lodash'

export const initFormInfo = (data) =>{
    return (dispatch, getState) => {
        //设计器后台解析的样式信息，必须dispatch前append到body，否则IE下保存后跳转异常
        jQuery("body").append(data.cellInfo.style.layoutStyle).append(data.cellInfo.style.scriptStyle);
        dispatch({
            type: types.REQFORM_INIT_INFO,
            layout: data.datajson.eformdesign,
            conf: {
                tableInfo: data.tableInfo,
                cellInfo: data.cellInfo,
                browserInfo: data.browserInfo,
                fieldDependShip: data.fieldDependShip,
                linkageCfg: data.linkageCfg,
                propFileCfg: data.propFileCfg,
                codeInfo:data.codeInfo,
                fnaInfo:data.fnaInfo
            },
            mainData: data.maindata
        });
        //单页标题
        if(window.location.pathname.indexOf('/spa/workflow/index') >= 0){
            const params = data.params;
            if(params && parseInt(params.iscreate) === 1){
                document.title = `创建 - ${params.workflowname}`;
            }else if("field-1" in data.maindata){
                document.title = data.maindata["field-1"]["value"];
            }else{
                document.title = `处理 - ${params.workflowname}`;
            }
        }
    }
}

export const loadDetailValue = (queryParams) =>{
    return (dispatch, getState) => {
        window.scancodeOperate.readyAdjustSize();   //二维条码宽度自适应绑事件
        const params = getState().workflowReq.get("params").toJS();
        const submitParams = getState().workflowReq.get("submitParams").toJS();
        const formState = getState().workflowReqForm;
        let detailmark = "";
        formState.getIn(["conf","tableInfo"]).map((v,k) => {
            if(k !== "main")
                detailmark += k+",";
        });
        if(detailmark !== "") {
            const reqParams = {
                workflowid: params.workflowid,
                nodeid: params.nodeid,
                formid: params.formid,
                isbill: params.isbill,
                isagent: params.isagent,
                beagenter: params.beagenter,
                f_weaver_belongto_userid: params.f_weaver_belongto_userid,
                f_weaver_belongto_usertype: params.f_weaver_belongto_usertype,
                ismode: params.ismode,
                modeid: params.modeid,
                iscreate: params.iscreate,
                isprint: params.isprint,
                isviewonly: params.isviewonly,
                nodetype:params.nodetype
            };
            const reqDetailParams = {...queryParams};
            reqDetailParams["reqParams"] = JSON.stringify(reqParams);
            reqDetailParams["detailmark"] = detailmark.substring(0,detailmark.length-1);
            API_REQ.loadDetailData(reqDetailParams).then((data)=>{
                dispatch({type: types.REQFORM_SET_DETAILVALUE, detailData:data});
                dispatch(triAllLinkage(2));     //页面加载触发联动应在默认新增明细前，新增加的行联动在添加时会自动触发
                //节点操作者-默认新增空明细
                if(parseInt(params.isviewonly || 0) !== 1){
                    detailmark.split(",").map(detailMark =>{
                        const rowDatas = getState().workflowReqForm.getIn(["detailData",detailMark,"rowDatas"]);
                        if(!rowDatas || rowDatas.size === 0){
                            const detailtableattr = formState.getIn(["conf","tableInfo",detailMark,"detailtableattr"]);
                            if(detailtableattr && detailtableattr.get("isdefault") === 1 && detailtableattr.get("isadd") === 1){
                                const defaultrows = detailtableattr.get("defaultrows")||0;
                                for(let i=0; i<defaultrows; i++){
                                    dispatch(addDetailRow(detailMark));
                                }
                            }
                        }
                    });
                }
                dispatch(loadScript(params,submitParams));  //DOM都渲染后加载代码块
            });
        }else {
            dispatch(triAllLinkage(2));
            dispatch(loadScript(params,submitParams));
        }
    }
}

export const loadScript = (params,submitParams)=> {
	return (dispatch, getState) => {
		Promise.all([
			API_REQ.loadScriptContent({
				layoutid:params.modeid
			}),
			API_REQ.copyCustomPageFile({
				custompage:params.custompage,
				workflowid:params.workflowid
			}).then(data=>{
				if(data.custompagee9=="") {
					return new Promise((resolve)=>{
						resolve("");
					});
				}else {
					return API_REQ.loadCustompage(data.custompagee9, submitParams);
				}
			})
		]).then((result)=>{
			jQuery(".excelTempDiv #scriptArea").html("").append(formUtil.manageScriptContent(result[0]));
			jQuery(".excelTempDiv #customPageArea").html("").append(formUtil.manageScriptContent(result[1]));
			//typeof window.formReady === "function" && window.formReady();
		});
	};
}

//明细行选中、全选(all)
export const setDetailRowChecked = (detailMark, rowIndexMark, isChecked) =>{
    return (dispatch, getState) => {
        if(!rowIndexMark && rowIndexMark !== 0)
            return;
        const formState = getState().workflowReqForm;
        let rowIndexStr = rowIndexMark === "all" ? formUtil.getDetailAllRowIndexStr(formState, detailMark) : rowIndexMark.toString();
        dispatch({type:types.REQFORM_SET_DETAIL_ROW_CHECKED, detailMark, rowIndexStr, isChecked});
    }
}

//添加明细
export const addDetailRow = (detailMark, initRowData={}, fromLinkage=false) =>{
    return (dispatch, getState) => {
        const formState = getState().workflowReqForm;
        const curDetailInfo = formState.getIn(["detailData",detailMark]);
        const indexnum = parseInt(curDetailInfo.get("indexnum")||0);
        let addRowObj = curDetailInfo.has("addRowDefValue") ? lodash.assign({}, curDetailInfo.get("addRowDefValue").toJS()) : {};    //新增行字段默认值
        //新增行字段赋值
        for(const key in initRowData){
            if(key.indexOf("field")>-1)
                addRowObj[key] = initRowData[key];
        }
        //新增行时复制最后一行数据
        if(!fromLinkage && formState.getIn(["variableArea", `${detailMark}_copyCfg`])){
            const lastRowKey = formUtil.getDetailLastRowKey(formState.get("detailData"), detailMark);
            if(lastRowKey !== ""){
                addRowObj = formState.getIn(["detailData", detailMark, "rowDatas", lastRowKey]).filter((v,k) => {
                    if(k.indexOf("field") > -1){
                        const fieldInfo = formState.getIn(["conf", "tableInfo", detailMark, "fieldinfomap", k.substring(5)]);
                        if(fieldInfo && fieldInfo.get("htmltype") !== 6)
                            return true;
                    }
                    return false;
                }).toJS();
            }
        }
        //暂时明细插入都在最后行后插入，如果是指定行插入，则下方行是更新组件，最后行是初始加载组件,值变化联动触发判断加上行标示解决，同时明细字段不能依赖componentWillMount事件
        addRowObj["orderid"] = indexnum+1;
        const datas = {indexnum:(indexnum+1), fromAddRow:true, rowDatas:{[`row_${indexnum}`]:addRowObj}};
        dispatch({type:types.REQFORM_ADD_DETAIL_ROW, detailMark, datas});
        //触发新增行的联动
        dispatch(triAllLinkage(3, detailMark, "", indexnum));
    }
}

//删除明细
export const delDetailRow = (detailMark) =>{
    return (dispatch, getState) => {
        dispatch({type:types.REQFORM_DEL_DETAIL_ROW, detailMark});
        dispatch(triAllLinkage(4, detailMark));
    }
}

//修改单个字段值
export const changeSingleFieldValue = (fieldMark, valueInfo, variableInfo) =>{
    return (dispatch, getState) => {
        const changeDatas = {};
        if(!!valueInfo && !jQuery.isEmptyObject(valueInfo))
            changeDatas[fieldMark] = valueInfo;
        const changeVariable = {};
        if(!!variableInfo && !jQuery.isEmptyObject(variableInfo))
            changeVariable[fieldMark] = variableInfo;
        dispatch(changeMoreFieldData(changeDatas, changeVariable));
    }
}

//修改Redux字段值/字段变量的唯一入口，修改字段值同时需触发联动事件
//修改多个字段(主字段/明细字段)值信息,只做一次dispatch渲染
export const changeMoreFieldData = (changeDatas, changeVariable={}) =>{
    return (dispatch, getState) => {
        if(jQuery.isEmptyObject(changeDatas) && jQuery.isEmptyObject(changeVariable))
            return;
        //if(window.console)  console.log("changeMoreFieldData--", changeDatas, changeVariable);
        const formStateBeforeChange = getState().workflowReqForm;
        //修改Redux字段值
        dispatch({type:types.REQFORM_CHANGE_MORE_FIELD_DATA, changeDatas, changeVariable});
        //值变化触发联动事件
        for(const key in changeDatas){
            const isDetail = key.indexOf("_") > -1;
            const fieldid = isDetail ? key.substring(5, key.indexOf("_")) : key.substring(5);
            const rowIndex = isDetail ? key.substring(key.indexOf("_")+1) : "-1";
            const tableMark =  isDetail ? formUtil.getBelTableMark(formStateBeforeChange, fieldid) : "main";
            const fieldObj = formUtil.getFieldInfoObj(formStateBeforeChange, fieldid, tableMark);
            if(!fieldObj)
                continue;
            let oldValue = formUtil.getFieldValue(formStateBeforeChange, fieldid, rowIndex, tableMark);
            let newValue = changeDatas[key].value;
            if(fieldObj.get("htmltype") === 1 && fieldObj.get("detailtype") !== 1){     //数值类型
                if(oldValue !== "")     oldValue = parseFloat(oldValue);
                if(newValue !== "")     newValue = parseFloat(newValue);
            }
            const existChange = oldValue !== newValue;    //标示字段值是否发生变化，必须三个等号判断类型
            //触发标准功能的联动
            dispatch(triAllLinkage(1, tableMark, fieldid, rowIndex, existChange));
        }
    }
}


/***************** 触发联动事件，triType含义（1：字段值变化触发  2：页面加载触发  3：新增行触发  4、删除行触发） ****************/
/***************** 页面加载触发联动每个类型只发一次请求，值变化触发联动非请求后台的联动只最终做一次dispatch *****************/
/***************** 联动dispatch次数要尽量降到最低，考虑计算所有行情况 ****************/
export const triAllLinkage = (triType, tableMark, triFieldid, rowIndex, existChange) =>{
    return (dispatch, getState) => {
        if(window.console)  console.log("triAllLinkage--"+triType, tableMark, triFieldid, rowIndex, existChange);
        const formState = getState().workflowReqForm;
        const params = getState().workflowReq.get("params");
        const isviewonly = parseInt(params.get("isviewonly")||0);
        const iscreate = parseInt(params.get("iscreate")||0);
        const propFileCfg = formState.getIn(["conf", "propFileCfg"]);
        let changeDatas = {};
        let changeVariable = {};
        //是否触发联动判断
        let needTriRowRule = false;
        let needTriColRule = false;
        let needTriFieldMath = false;
        let needTriFormula = false;
        let needTriSelectChange = false;
        let needTriViewAttr = false;
        let needTriFieldSql = false;
        let needTriDateTime = false;
        let needTriDataInput = false;
        if(triType === 1){      //字段值变更
            if(tableMark === "main"){
                needTriFieldMath = true;
            }else if(tableMark.indexOf("detail_") > -1){
                const triFieldObj = formUtil.getFieldInfoObj(formState, triFieldid, tableMark);
                if(triFieldObj.get("htmltype") === 1 && triFieldObj.get("detailtype") !== 1){   //数值字段触发行列规则
                    needTriRowRule = true;
                    needTriColRule = true;
                }
            }
            needTriFormula = true;
            needTriSelectChange = true;
            needTriViewAttr = true;
            needTriFieldSql = true;
            needTriDateTime = true;
            needTriDataInput = true;
        }else if(triType === 2){    //页面加载
            if((isviewonly === 0 && propFileCfg.get("tri_rowRule_edit")) || (isviewonly === 1 && propFileCfg.get("tri_rowRule_view")))
                needTriRowRule = true;
            if(isviewonly === 0){
                needTriFieldMath = true;
                needTriFormula = true;
                needTriSelectChange = true;
                needTriFieldSql = true;
                needTriDateTime = true;
                needTriDataInput = true;
            }
            needTriColRule = true;
            needTriViewAttr = true; //已办也需要执行显示属性联动隐藏
        }else if(triType === 3){    //新增行
            needTriRowRule = true;
            needTriColRule = true;
            needTriFormula = true;
            needTriSelectChange = true;
            needTriViewAttr = true;
            needTriFieldSql = true;
            needTriDateTime = true;
            needTriDataInput = true;
        }else if(triType === 4){    //删除行
            needTriColRule = true;
            needTriFormula = true;
        }
        if(needTriRowRule){     //行规则
            const rowRule_result = linkageUtil.calDetailRowRule(formState, triType, tableMark, triFieldid, rowIndex);
            if(!!rowRule_result)
                changeDatas = lodash.assign(changeDatas, rowRule_result);
        }
        if(needTriColRule){     //列规则
            const colRult_result = linkageUtil.calDetailColRule(formState, triType, tableMark, triFieldid);
            if(!!colRult_result){
                changeDatas = lodash.assign(changeDatas, colRult_result["changeDatas"]||{});
                changeVariable = lodash.merge(changeVariable, colRult_result["changeVariable"]||{});
            }
        }
        if(needTriFieldMath){   //字段赋值
            const fieldMath_result = linkageUtil.calFieldMath(formState, triType, triFieldid);
            if(!!fieldMath_result){
                changeDatas = lodash.assign(changeDatas, fieldMath_result["changeDatas"]||{});
                changeVariable = lodash.merge(changeVariable, fieldMath_result["changeVariable"]||{});
            }
        }
        if(needTriFormula){     //公式
            const formula_result = linkageUtil.calFormula(formState, triType, tableMark, triFieldid, rowIndex);
            if(!!formula_result)
                changeDatas = lodash.assign(changeDatas, formula_result);
        }
        if(needTriSelectChange){    //选择框联动
            const selectChange_result = linkageUtil.controlSelectChange(formState, triType, tableMark, triFieldid, rowIndex);
            if(!!selectChange_result){
                changeDatas = lodash.assign(changeDatas, selectChange_result["changeDatas"]||{});
                changeVariable = lodash.merge(changeVariable, selectChange_result["changeVariable"]||{});
            }
        }
        if(needTriViewAttr){    //显示属性联动
            const viewAttr_result = linkageUtil.controlViewAttr(formState, triType, tableMark, triFieldid, rowIndex);
            if(!!viewAttr_result)
                changeVariable = lodash.merge(changeVariable, viewAttr_result);     //需使用lodash.merge深度合并，避免覆盖optionRange等
        }
        if(formState.getIn(["conf", "fnaInfo", "belFna"]) === true){   //选择框联动浏览框类型(财务相关)
            const browTypeChange_result = linkageUtil.controlBrowTypeChange(formState, triType, tableMark, triFieldid, rowIndex);
            if(!!browTypeChange_result){
                changeDatas = lodash.assign(changeDatas, browTypeChange_result["changeDatas"]||{});
                changeVariable = lodash.merge(changeVariable, browTypeChange_result["changeVariable"]||{});
            }
        }
        //非请求后台联动结果统一修改Redux
        dispatch(changeMoreFieldData(changeDatas, changeVariable));
        /************** 以下为需要发请求的联动 **************/
        if(needTriFieldSql){    //SQL联动
            const fieldSql_triInfo = linkageUtil.generateFieldSqlTriInfo(formState, triType, tableMark, triFieldid, rowIndex);
            dispatch(reqLinkageResult(triType, fieldSql_triInfo, "fieldsql"));
        }
        if(needTriDateTime){    //日期时间计算
            const dateTime_triInfo = linkageUtil.generateDateTimeTriInfo(formState, triType, tableMark, triFieldid, rowIndex);
            dispatch(reqLinkageResult(triType, dateTime_triInfo, "datetime"));
        }
        if(needTriDataInput){   //字段联动
            const dataInput_triInfo = linkageUtil.generateDataInputTriInfo(formState, triType, tableMark, triFieldid, rowIndex, iscreate);
            dispatch(reqLinkageResult(triType, dataInput_triInfo, "datainput"));
        }
    }
}

//请求后台获取字段联动结果
const reqLinkageResult = (triType, triInfo, linkageType) =>{
    return (dispatch, getState) => {
        if(jQuery.isEmptyObject(triInfo))   return;
        //if(window.console)  console.log("reqLinkageResult--"+triType+"--"+linkageType, triInfo);
        const formState = getState().workflowReqForm;
        const paramsObj = getState().workflowReq.get("params");
        const iscreate = parseInt(paramsObj.get("iscreate")||0);
        const reqInfoArr = new Array();
        if(triType === 2 && formState.getIn(["conf", "propFileCfg", "split_req_linkageData"]) === true){      //页面加载触发并设置拆分发请求情况
            for(const key in triInfo){
                const singleTriInfo = {[key] : triInfo[key]};
                reqInfoArr.push(linkageUtil.generateReqLinkageParams(formState, paramsObj, singleTriInfo, linkageType));
            }
        }else{
            reqInfoArr.push(linkageUtil.generateReqLinkageParams(formState, paramsObj, triInfo, linkageType));
        }
        for(let i=0; i<reqInfoArr.length; i++){
            const params = reqInfoArr[i];
            params["triSource"] = triType;
            if(!params.linkageid)
                continue;
            let API_FUNCTION = null;
            if(linkageType === "datainput")
                API_FUNCTION = API_REQ.reqDataInputResult(params);
            else if(linkageType === "fieldsql")
                API_FUNCTION = API_REQ.reqFieldSqlResult(params);
            else if(linkageType === "datetime")
                API_FUNCTION = API_REQ.reqDateTimeResult(params);
            API_FUNCTION.then(data => {
                try{
                    const dataMap = Immutable.fromJS(data);
                    const changeDatas = {};
                    dataMap.map((v,k) =>{
                        const triTableMark = params[`triTableMark_${k.substring(11)}`];
                        let changeValue;
                        if(linkageType === "datainput"){
                            changeValue = v && v.get("changeValue");
                            //主表联动明细
                            if(triType === 1 || (triType === 2 && iscreate === 1)){
                                const addRows = v && v.get("addDetailRow");
                                addRows && addRows.map((rowDatas,detailMark) =>{
                                    rowDatas && rowDatas.map(rowData =>{
                                        dispatch(addDetailRow(detailMark, rowData.toJS(), true));
                                    });
                                });
                            }
                        }else{
                            changeValue = v;
                        }
                        //联动赋值
                        changeValue && changeValue.map((valueInfo, key) =>{
                            const fieldArr = key.split("_");
                            const fieldid = fieldArr[0].substring(5);
                            const rowIndex = fieldArr.length>1 ? fieldArr[1] : -1;
                            let tableMark = triTableMark;
                            if(!tableMark || linkageType === "fieldsql")      //SQL联动主字段触发可赋值明细字段
                                tableMark = formUtil.getBelTableMark(formState, fieldid);
                            const fieldMark = tableMark.indexOf("detail_") > -1 ? `field${fieldid}_${rowIndex}` : `field${fieldid}`;
                            changeDatas[fieldMark] = valueInfo.toJS();
                        });
                    });
                    dispatch(changeMoreFieldData(changeDatas));
                }catch(e){
                    if(window.console)  console.log("reqLinkageResult "+linkageType+" Exception:", params, data, e);
                }
            });
        }
    }
}

//修改其它变量属性
export const controlVariableArea = (changeInfo) =>{
    return (dispatch, getState) =>{
        if(!jQuery.isEmptyObject(changeInfo))
            dispatch({type:types.REQFORM_CONTROL_VARIABLE_AREA, changeInfo});
    }
}

//以合并方式修改变量区
export const controlVariableArea_merge = (mergeInfo) =>{
    return (dispatch, getState) =>{
        if(!jQuery.isEmptyObject(mergeInfo))
            dispatch({type:types.REQFORM_CONTROL_VARIABLE_AREA_MERGE, mergeInfo});
    }
}

/**
 * 控制提示组件
 * @param {*} mark  提示字段则为field110_3、提示明细表则是detail_2
 * @param {*} promptType   1提示必填字段，2提示行规则赋值字段不可编辑，3提示未新增明细
 */
export const showPrompt = (mark, promptType) =>{
    return (dispatch, getState) =>{
        if(!mark)
            return;
        let changeInfo = {};
        let changeTabMark = "";
        let lableText = "";
        let fieldid = "";
        const formState = getState().workflowReqForm;
        if(promptType === 1 || promptType === 2){
            fieldid = mark.indexOf("_") > -1 ? mark.substring(5,mark.indexOf("_")) : mark.substring(5);
            const fieldObj = formUtil.getFieldInfoObj(formState, fieldid);
            lableText = fieldObj.get("fieldlabel");
        }else if(promptType === 3){
            lableText = `明细表${mark.substring(7)}`;
        }
        changeInfo["promptBox"] = {mark, lableText, promptType:promptType, visible:true};
        if(promptType === 1){
            changeInfo["promptRequiredField"] = mark;
            //如果字段是标签页字段，需默认选择标签页
            const belSymbol = formState.getIn(["conf", "cellInfo", "fieldCellInfo", fieldid, "symbol"]) || "";
            if(belSymbol.indexOf("mc_") > -1 || belSymbol.indexOf("detail_") > -1)   //多字段、明细字段再取上一层
                changeTabMark = formState.getIn(["conf", "cellInfo", "subPanelInfo", belSymbol, "symbol"]) || "";
            else
                changeTabMark = belSymbol;
        }else if(promptType === 3){
            changeTabMark = formState.getIn(["conf", "cellInfo", "subPanelInfo", mark, "symbol"]) || "";
        }
        //提示区域在标签页中，需要切换标签页选中项
        if(changeTabMark.indexOf("tab_") > -1){
            const tabPanelInfo = formState.getIn(["conf", "cellInfo", "subPanelInfo", changeTabMark]);
            if(tabPanelInfo){
                const tabAreaMark = `tabarea_${tabPanelInfo.get("rowid")}_${tabPanelInfo.get("colid")}_showid`;
                changeInfo[tabAreaMark] = changeTabMark;
            }
        }
        dispatch(controlVariableArea(changeInfo));
        //延时清除状态
        window.setTimeout(function(){
            let recoverData = {};
            recoverData["promptBox"] = {visible:false};
            if(promptType === 1)
                recoverData["promptRequiredField"] = "";
            dispatch(controlVariableArea(recoverData));
        },1500);
    }
}

//表单触发生成流程编号
export const setWfCode = (params) => {
	return (dispatch, getState) => {
		const fieldCode = getState().workflowReqForm.getIn(['conf','codeInfo','fieldCode']);
		const reqParams = getState().workflowReq.get('params');
		
		params.requestid = reqParams.get('requestid');
		params.workflowid = reqParams.get('workflowid');
		params.formid = reqParams.get('formid');
		params.isbill = reqParams.get('isbill');
		params.creater = reqParams.get('creater');
		params.creatertype = reqParams.get('creatertype');
		
		API_REQ.createWfCode(params).then(data=>{
			dispatch(changeSingleFieldValue(`field${fieldCode}`,{value:data.wfcode}));	
		});
	}
}

//选择预留编号
export const chooseReservedCode = (params) => {
	return (dispatch, getState) => {
		const fieldCode = getState().workflowReqForm.getIn(['conf','codeInfo','fieldCode']);
		const reqParams = getState().workflowReq.get('params');
		
		params.requestid = reqParams.get('requestid');
		params.workflowid = reqParams.get('workflowid');
		params.formid = reqParams.get('formid');
		params.isbill = reqParams.get('isbill');
		params.creater = reqParams.get('creater');
		params.creatertype = reqParams.get('creatertype');

		let tempurl = "/workflow/workflow/showChooseReservedCodeOperate.jsp?workflowId="+reqParams.get('workflowid')+"&formId="+reqParams.get('formid')+"&isBill="+reqParams.get('isbill');
		API_REQ.loadWfCodeFieldValueInfo(params).then(data=>{
			const mapdata = Immutable.Map(data);
			mapdata.map((val,key) => tempurl = tempurl + "&"+key+"="+val);
		
			var dialognew = new window.top.Dialog();
			dialognew.currentWindow = window;
			dialognew.URL = "/systeminfo/BrowserMain.jsp?url="+escape(tempurl);
			dialognew.callbackfun = function (paramobj, con) {
				params.codeSeqReservedIdAndCode = con.id+"~~wfcodecon~~"+con.name;
				params.operation = "chooseReservedCode";
				API_REQ.createWfCode(params).then(data=>{
					dispatch(changeSingleFieldValue(`field${fieldCode}`,{value:data.wfcode}));	
				});
			};
			dialognew.Title = "选择预留号";
			dialognew.Modal = true;
			dialognew.Width = 550 ;
			dialognew.Height = 500 ;
			dialognew.isIframe=false;
			dialognew.show();
		});
	}
}

//新建预留编号
export const newReservedCode =(params)=>{
	return (dispatch, getState) => {
		const fieldCode = getState().workflowReqForm.getIn(['conf','codeInfo','fieldCode']);
		const reqParams = getState().workflowReq.get('params');
		
		params.requestid = reqParams.get('requestid');
		params.workflowid = reqParams.get('workflowid');
		params.formid = reqParams.get('formid');
		params.isbill = reqParams.get('isbill');
		params.creater = reqParams.get('creater');
		params.creatertype = reqParams.get('creatertype');
		
		let tempurl = "/workflow/workflow/showNewReservedCodeOperate.jsp?workflowId="+reqParams.get('workflowid')+"&formId="+reqParams.get('formid')+"&isBill="+reqParams.get('isbill');
		API_REQ.loadWfCodeFieldValueInfo(params).then(data=>{
			const mapdata = Immutable.Map(data);
			mapdata.map((val,key) => tempurl = tempurl + "&"+key+"="+val);
			
			var dialognew = new window.top.Dialog();
			dialognew.currentWindow = window;
			dialognew.URL = "/systeminfo/BrowserMain.jsp?url="+escape(tempurl);
			dialognew.Title = "新建预留号";
			dialognew.Modal = true;
			dialognew.Width = 550 ;
			dialognew.Height = 500 ;
			dialognew.isIframe=false;
			dialognew.show();
		});
	}
}
