import { Modal,message} from 'antd';
import {verifyMustAddDetail,verifyRequiredEmptyField,saveJudgeRequestNameEmpty} from '../util/formUtil'
const confirm = Modal.confirm;
const success = Modal.success;
const warning = Modal.warning;

import {WeaTable} from 'comsRedux'

const WeaTableAction = WeaTable.action;

import * as types from '../constants/ActionTypes'
import * as API_REQ from '../apis/req'
import * as API_TABLE from '../apis/table'
import * as ReqFormAction from './reqForm'
import * as ReqLogListAction from './reqLogList'
import objectAssign from 'object-assign'
import Immutable from 'immutable'
import * as reqUtil from '../util/reqUtil'

import cloneDeep from 'lodash/cloneDeep'

//初始化表单
export const initFormLayout = (queryParams) => {
	return (dispatch, getState) => {
		dispatch({type:types.FORM_LOADING,loading:true});
		const apiLoadStart = new Date().getTime();
		const reqId = queryParams.requestid;
		API_REQ.loadForm(queryParams).then((data)=>{
			
			const dispatchStart = new Date().getTime();
			window.API_DURATION = dispatchStart - apiLoadStart;
			
			const params = data.params;
			if(params.verifyRight === false){	//无权限跳转
				weaWfHistory && weaWfHistory.push("/main/workflow/ReqNoRight");
			}
			const apiDuration = dispatchStart - apiLoadStart;
			//布局、表单内容
			dispatch({type: types.REQ_INIT_PARAMS, params:params, submitParams:data.submitParams});
			dispatch(ReqFormAction.initFormInfo(data));
			//性能测试
			window.DISPATCH_DURATION = new Date().getTime() - dispatchStart;
			window.REQ_LOAD_DURATION = new Date().getTime() - window.REQ_PAGE_LOAD_START;
//			dispatch({type: 'TEST_PAGE_LOAD_DURATION',
//				reqLoadDuration:(new Date().getTime() - (window.REQ_PAGE_LOAD_START ? window.REQ_PAGE_LOAD_START : 0)),
//				jsLoadDuration: window.JS_LOAD_DURATION ? window.JS_LOAD_DURATION : 0,
//				apiDuration: apiDuration,
//				dispatchDuration: new Date().getTime() - dispatchStart
//			})
			//图片懒加载
			formImgLazyLoad(jQuery('.wea-new-top-req-content'));
			//明细数据加载
			dispatch(ReqFormAction.loadDetailValue(queryParams));
			
			if(data.reqOperationInfo){
				const {type,resultInfo={},messagehtml=''} = data.reqOperationInfo;
				const {isaffirmance=''} = resultInfo;
				if(type === 'FAILD' || (type === 'SUCCESS' && isaffirmance == "1")){
					dispatch(setReqSubmitErrorMsgHtml(messagehtml));
				}
			}
			
			//加载签字意见输入框
			if(params.isHideInput != "1"){
				const signParams  = {requestid:reqId,workflowid:params.workflowid,nodeid:params.nodeid};
				API_REQ.getSignInput(signParams).then((data)=>{
					data.hasLoadMarkInfo = true;
					dispatch(setMarkInputInfo(data));
				});
			}
			
			//获取右键菜单
			params.isaffirmance = queryParams.isaffirmance || '';
			params.reEdit = queryParams.reEdit || '';
			dispatch(loadRightMenu(params));
			
			//其它处理，前端不用处理
			if(queryParams.iscreate != '1'){
				API_REQ.updateReqInfo({
					requestid:reqId,
					currentnodetype:params.currentnodetype,
					wfmonitor:params.wfmonitor,
					isurger:params.isurger
				}).then(data=>{});
				
				//显示签字意见区域
				if(params.isHideArea != "1"){
					dispatch(ReqLogListAction.initLogParams(params));
				}
			}
			reqUtil.listDoingRefresh();
		});
	}
}

const loadRightMenu = (params) => {
	return (dispatch, getState) => {
		API_REQ.getRightMenu(params).then(data=>{
			data.rightMenus.push({isTop:"0",menuFun:"openFavouriteBrowser(-9)",menuIcon:"icon-workflow-Right-menu-Collection",menuName:"收藏",order:100});
			data.rightMenus.sort((o1,o2)=>{
				return o1.order - o2.order;
			});
			dispatch(setRightMenuInfo(data));
		});
	}
}

export const clearForm = () => {
	return (dispatch, getState) => {
		dispatch({type:types.REQ_CLEAR_INFO});
		dispatch({type:types.REQFORM_CLEAR_INFO});
		dispatch({type:types.LOGLIST_CLEAR_INFO})
	}
}

export const setCustompageHtml = (custompagehtml) => {
	return {type:types.SET_CUSTOMPAGE_HTML,custompagehtml:custompagehtml}
}

//设置表单tabkey
export const setReqTabKey = key => {
	return {type:types.SET_REQ_TABKEY,reqTabKey:key}
}

//设置签字意见输入框信息
export const setMarkInputInfo = value => {
	return {type:types.SET_MARK_INPUT_INFO,markInfo:value};
}

//设置右键菜单信息
export const setRightMenuInfo = value => {
	return {type:types.SET_RIGHT_MENU_INFO,rightMenu:value};
}

//加载流程状态数据
export const loadWfStatusData = (params, cardid, isfirst) => {
	return (dispatch, getState) => {
		dispatch({type:types.FORM_LOADING,loading:true});
		const statusState = getState().workflowReq.get('wfStatus');
		let newParams = {...params, ...{desremark:cardid, isfirst:isfirst, pageSize:50}};
		if(!isfirst)
			newParams["parameter"] = JSON.stringify(statusState.getIn([cardid,"parameter"]));
		API_REQ.getWfStatus(newParams).then(data => {
			let statusDatas = {cardid:cardid};
			statusDatas[cardid] = data;
			dispatch({type:types.SET_WORKFLOW_STATUS, wfStatus:statusDatas});
		});
		if(cardid === "all" && isfirst){
			API_REQ.getWfStatusCount(params).then(data => {
				dispatch({type:types.SET_WORKFLOW_STATUS, wfStatus:{counts:data}});
			});
		}
	}
}

//流程状态-切换Card
export const switchWfStatusCard = cardid => {
	return (dispatch, getState) => {
		dispatch({type:types.SET_WORKFLOW_STATUS, wfStatus:{cardid:cardid}});
	}
}

//流程状态-全部Card控制节点行隐藏显示
export const controlWfStatusHideRow = hideRowKeys =>{
	return (dispatch, getState) =>{
		dispatch({type:types.SET_WORKFLOW_STATUS, wfStatus:{hideRowKeys:hideRowKeys}});
	}
}

//相关资源
export const getResourcesKey = (requestid, tabindex) => {
	return (dispatch, getState) => {
		const params = getState().workflowReq.get("params");
		const isrequest = params && params.get("isrequest") || "";
		const desrequestid = params && params.get("desrequestid") || "";
		const authStr = params && params.get("authStr") || "";
		const authSignatureStr = params && params.get("authSignatureStr") || "";
		API_REQ.getResourcesKey({requestid,tabindex,isrequest,desrequestid,authStr,authSignatureStr}).then((data)=>{
			dispatch(WeaTableAction.getDatas(data.sessionkey, 1));
			dispatch({type: types.SET_RESOURCES_KEY,key:data.sessionkey,tabindex});
		});
	}
}

//表单提交
//actiontype,src,needwfback
export const doSubmit = (para) => {
	return (dispatch, getState) => {
		const {actiontype,src,needwfback='',isaffirmance=''} = para;
		const formState = getState().workflowReqForm;
		const submitParams = getState().workflowReq.get('submitParams');
		const isSubmitDirectNode = submitParams.get('isSubmitDirectNode');
		const lastnodeid  = submitParams.get('lastnodeid');
		const tnodetype  = submitParams.get('nodetype');
		const fromFlowDoc  = submitParams.get('fromFlowDoc');
		const isworkflowdoc  = submitParams.get('isworkflowdoc');
		const isuseTemplate  = submitParams.get('isuseTemplate');
		const isFormSignature = submitParams.get('isFormSignature');
		const isHideInput = submitParams.get('isHideInput');
		const iscreate  = submitParams.get("iscreate");
		const isFirstSubmit = iscreate == "1"?"":'0';
		const formParams = {
			"src": src,
			"needwfback":needwfback,
			"isaffirmance":isaffirmance,
			"isFirstSubmit":isFirstSubmit,
			"actiontype":actiontype
		};
		
		if(src == 'submit' || isaffirmance == '1'){
			const isViewOnly = parseInt(getState().workflowReq.getIn(['params', "isviewonly"]) || 0);
			const isAffirmanceSubmit = parseInt(getState().workflowReq.getIn(['params', "isaffirmance"]) || 0);
			if(isViewOnly !== 1 && isAffirmanceSubmit !== 1){
				//校验必须新增明细
				const needAddDetailMark = verifyMustAddDetail(formState);
				if(needAddDetailMark > 0){
					dispatch(ReqFormAction.showPrompt(`detail_${needAddDetailMark}`, 3));
					return false;
				}
				//校验必填空值范围
				const emptyField = verifyRequiredEmptyField(formState);
				if(emptyField !== ""){
					dispatch(ReqFormAction.showPrompt(emptyField, 1));
					return false;
				}
			}
			if(isSubmitDirectNode == "1"){
				formParams.SubmitToNodeid = lastnodeid;
			} 
			saveSignature_of_doSubmit();
			dispatch(doSubmitApi(formParams));
		}else{
			isFormSignature == "1" && isHideInput != "1" && typeof SaveSignature_save == "function" && SaveSignature_save();
			//src == 'save'
			if(src == 'save'){
				if(saveJudgeRequestNameEmpty(formState)){
					dispatch(ReqFormAction.showPrompt("field-1", 1));
					message.warning('请求标题字段未填值！');
					return false;
				}
			}
			dispatch(doSubmitApi(formParams));
		}
	}
}

//保存签章
const saveSignature_of_doSubmit = () => {
	if(typeof SaveSignature == "function") {
		if(!SaveSignature()) {
		}
	}
}

export const beforeSubmitValidRemark = (isCheckRemark,src) =>{
	return (dispatch, getState) => {
		const isSignMustInput = getState().workflowReq.getIn(['params','markInfo','isSignMustInput']);
		const isHideInput = getState().workflowReq.getIn(['params','isHideInput']);
		let ischeckok  = true;
		if(isHideInput != "1"){
			//验证签字意见必填
			if((isSignMustInput == "1" && isCheckRemark) || (src && src == "reject" && isSignMustInput == "2")){
				let remarkcontent  = FCKEditorExt.getText("remark");
				
				ischeckok = reqUtil.chekcremark(remarkcontent);
			}
			const signinputinfo = isHideInput != "1" ? reqUtil.getSignInputInfo() : {};
			signinputinfo.remark = FCKEditorExt.getHtml('remark');
			dispatch(updateSubmitParams(signinputinfo));
		}
		return ischeckok;
	}
}

//表单提交接口
export const doSubmitApi = (formParams) => {
	return (dispatch, getState) => {
		const {src,actiontype,needwfback,isaffirmance} = formParams;
		const tempParams  = getState().workflowReq.get('params').toJS();
		if(!reqUtil.doBefore(src,tempParams,isaffirmance)){
			return;
		}
		const isCheckRemark  = (!(src == 'save' && actiontype == 'requestOperation')) || "1" == isaffirmance;
		const ischeckok = dispatch(beforeSubmitValidRemark(isCheckRemark,src));
		if(ischeckok){
			//签字意见相关流程，相关文档
			//暂时屏蔽接口
			if(actiontype == "remarkOperation"){
				const needconfirm = getState().workflowReq.getIn(['params','needconfirm']);
				if("1" ==  needconfirm){
					confirm({
						content:"确认是否提交?",
						onOk(){
							dispatch(doFileUpload(formParams));
						},
						onCancel(){}
					});
				}else{
					dispatch(doFileUpload(formParams));
				}
			}else{
				dispatch(doFileUpload(formParams));
			}
		}else{
			dispatch({type:types.CONTROLL_SIGN_INPUT,bool:true});
			message.warning('"签字意见"未填写',2);
			reqUtil.signMustInputTips();
		}
	}
}

const doFileUpload = (formParams) =>{
	return (dispatch, getState) => {
		dispatch({type:types.FORM_LOADING,loading:true});
		if(typeof(startUploadAll) == "function"){
			startUploadAll();	
		}
		let allUploaded  = false;
		let timer = setInterval(()=>{
			//检测附件是否上传完成
			const variableArea = getState().workflowReqForm.get("variableArea");
			let arr = [];
			variableArea && variableArea.map((fieldValObj,k)=>{
				if(k.indexOf("field") > -1 && k.indexOf("_") == -1 && fieldValObj && fieldValObj.has("fileUploadStatus")){
					arr.push(fieldValObj.get("fileUploadStatus"));
				}
			});
			let newArr = arr.filter(o=> o !== "uploaded");
			//处理上传失败的情况
			allUploaded = newArr.length === 0;
			if(allUploaded){
				dispatch(doSubmitPost(formParams));
				clearInterval(timer);
			}
		},100);
	}
}

//表单提交
export const doSubmitPost = (formParams) =>{
	return (dispatch, getState) => {
		const {src,actiontype} = formParams;
		const submitParams = getState().workflowReq.get('submitParams').toJS();
		const formDatas = (actiontype === 'remarkOperation') ? {} : reqUtil.getformdatas(getState().workflowReqForm);
		const signatureAttributesStr = getState().workflowReq.getIn(["params","signatureAttributesStr"])||"";
		const signatureSecretKey = getState().workflowReq.getIn(["params","signatureSecretKey"])||"";
		//处理自由流程数据
		const freeNodeInfo  = reqUtil.getFreeNodeData(getState().workflowReq.getIn(["rightMenu","isFree"]));
		API_REQ.reqOperate(actiontype,objectAssign({},submitParams,formParams,formDatas,{src:src,signatureAttributesStr:signatureAttributesStr,signatureSecretKey:signatureSecretKey},freeNodeInfo)).then(result=>{
			let data  = result.data;
			let resultInfo  = data.resultInfo;
			const type =  data.type;
			//更新操作信息
	    	dispatch(updateSubmitParams(data.submitParams));
	    	if(type == 'FAILD'){
	    		dispatch(reloadRequestPage(resultInfo));
	    	}else if(type == 'SUCCESS'){
	    		const {isaffirmanceSrc='',sessionkey,isaffirmance='',isNextNodeOperator=false,isShowChart='0'} = resultInfo;
	    		if(resultInfo.isaffirmance == "1" || (actiontype == 'requestOperation' && 'save' == src) || isNextNodeOperator){
	    			dispatch(reloadRequestPage(resultInfo));
	    		}else{
	    			if(isShowChart === '1'){
						dispatch(setReqIsUnbeforeunload(false));
						//刷新流程图
						let {requestid,workflowid,isbill,formid,isfromtab,f_weaver_belongto_userid,f_weaver_belongto_usertype} = submitParams;
						requestid = (requestid && -1 === parseInt(requestid)) ? resultInfo.requestid : requestid;
						window.location.href = `/workflow/request/WorkflowDirection.jsp?requestid=${requestid}&workflowid=${workflowid}&isbill=${isbill}&formid=${formid}&isfromtab=${isfromtab}&f_weaver_belongto_userid=${f_weaver_belongto_userid}&f_weaver_belongto_usertype&showE9Pic=1`;
						reqUtil.listDoingRefresh();
	    			}else{
	    				dispatch(reqIsSubmit(true));
	    			}
	    		}
	    	}else if(type == "SEND_PAGE"){ //无权限
				window.location.href = data.sendPage;
			} else if(type == "WF_LINK_TIP"){ //出口提示
				confirm({
				    content: resultInfo.msgcontent,
				    onOk() {
						//更新isFirstSubmit
						dispatch(updateSubmitParams({isFirstSubmit:1,requestid:resultInfo.requestid}));
						dispatch(doSubmitApi({"src":"submit","actiontype":"requestOperation"}));
				    },
				    onCancel(){}
				});
			} else if(type == "R_CHROSE_OPERATOR"){ //
				const needChooseOperator = resultInfo.needChooseOperator;
				//暂时用老的方式处理
				if(needChooseOperator == 'y'){
					let eh_dialog = null;
					if(window.top.Dialog)
						eh_dialog = new window.top.Dialog();
					else
						eh_dialog = new Dialog();
					eh_dialog.currentWindow = window;
					eh_dialog.Width = 650;
					eh_dialog.Height = 500;
					eh_dialog.Modal = true;
					eh_dialog.maxiumnable = false;
					eh_dialog.Title = "请选择";
					eh_dialog.URL = "/workflow/request/requestChooseOperator.jsp";
					eh_dialog.callbackfun = function(paramobj, datas) {
						let chrostoperatorinfo = {
							eh_setoperator:'y',
							eh_relationship:datas.relationship,
							eh_operators:datas.operators,
							isFirstSubmit:1
						};
						dispatch(updateSubmitParams(chrostoperatorinfo));
						dispatch(doSubmitApi({"src":"submit","actiontype":"requestOperation"}));
					};
					eh_dialog.closeHandle = function(paramobj, datas){
						const eh_setoperator = getState().workflowReq.get('submitParams').get('eh_setoperator');
						if(eh_setoperator != 'y'){
							dispatch(updateSubmitParams({eh_setoperator:'n',isFirstSubmit:1}));
							dispatch(doSubmitApi({"src":"submit","actiontype":"requestOperation"}));
						}
					};
					eh_dialog.show();
				}
			} else if(type == "DELETE"){
				warning({
				    content: resultInfo.label,
				    okText: '确定',
				    showClose:false,
				    onOk() {
				    	dispatch(reqIsSubmit(true));
				    }
				});
			}
			dispatch({type:types.FORM_LOADING,loading:false});
		});
	}		
}

export const updateSubmitParams = (updateinfo) =>{
	return {type:types.REQ_UPDATE_SUBMIT_PARAMS,updateinfo:updateinfo};
}

//更新Req params
export const updateReqParams = (updateinfo) =>{
	return {type:types.REQ_UPDATE_PARAMS, updateinfo:updateinfo};
}


//设置表单tabkey
export const reqIsSubmit = bool => {
	return (dispatch, getState) => {
		dispatch({type:types.REQ_IS_SUBMIT,bool:bool});
		if(bool){
			reqUtil.listDoingRefresh();
			try{
				window.close();
			}catch(e){
				window.location.reload();
			}
		}
	}
}

//重新加载
export const reqIsReload = (bool,data) => {
	return (dispatch, getState) => {
		dispatch({type:types.REQ_IS_RELOAD,bool:bool});
		if(bool) {
			const {routing,workflowReq} = getState();
			//let {search} = routing.locationBeforeTransitions;
			const params = workflowReq.get("submitParams");
			
			let editorArr = cloneDeep(UE.editors);
			editorArr.map(editorid=>{
				UE.getEditor(editorid).destroy();
			});
			UE.tmpList = [];
			let search = "";
			if(params.get('requestid') == '-1'){
				search += "?requestid="+data.requestid;
			}else{
				search += "?requestid="+params.get("requestid");
			}
			if(data && data.isaffirmance === "1"){
				search += "&isaffirmance=1"
			}
			if(data && data.reEdit){
				search += "&reEdit="+data.reEdit;
			}
			//console.log("search",search);
			dispatch({type:types.CLEAR_ALL});
			//console.log("CLEAR_ALL success");
			dispatch({type:types.REQFORM_CLEAR_INFO});
			//console.log("REQFORM_CLEAR_INFO success");
			weaWfHistory && weaWfHistory.push("/main/workflow/ReqReload"+search);
		}
	}
}

export const controlSignInput = bool =>{
	return(dispatch, getState) => {
		dispatch({type:types.CONTROLL_SIGN_INPUT,bool:bool});
	}
}

export const setReqSubmitErrorMsgHtml = msghtml =>{
	return {type:types.SET_REQ_SUBMIT_ERROR_MSG_HTML,msghtml:msghtml}
}

//流程暂停
export const doStop = () => {
	return(dispatch, getState) => {
		const params = getState().workflowReq.get('params');
		const requestid = params.get('requestid');
		const userid = params.get('f_weaver_belongto_userid');
		
		confirm({
			content: '您确定要暂停当前流程吗？',
			onOk() {
				API_REQ.functionLink({
					requestid: requestid,
					f_weaver_belongto_userid: userid,
					f_weaver_belongto_usertype: 0,
					flag: 'stop'
				}).then(data => {
					dispatch(reqIsSubmit(true));
				});
			}
		});
	}
}

//流程撤销
export const doCancel = () => {
	return(dispatch, getState) => {
		const params = getState().workflowReq.get('params');
		const requestid = params.get('requestid');
		const userid = params.get('f_weaver_belongto_userid');
		
		confirm({
			content: '您确定要撤销当前流程吗 ？',
			onOk() {
				API_REQ.functionLink({
					requestid: requestid,
					f_weaver_belongto_userid: userid,
					f_weaver_belongto_usertype: 0,
					flag: 'cancel'
				}).then(data => {
					dispatch(reqIsSubmit(true));
				});
			}
		});
	}	
}

//流程启用
export const doRestart = () => {
	return(dispatch, getState) => {
		const params = getState().workflowReq.get('params');
		const requestid = params.get('requestid');
		const userid = params.get('f_weaver_belongto_userid');
		
		confirm({
			content: '您确定要启用当前流程吗？',
			onOk() {
				API_REQ.functionLink({
					requestid: requestid,
					f_weaver_belongto_userid: userid,
					f_weaver_belongto_usertype: 0,
					flag: 'restart'
				}).then(data => {
					dispatch(reloadRequestPage({requestid:requestid}));
				});
			}
		});
	}		
}

//强制收回
export const doRetract = () => {
	return(dispatch, getState) => {
		const params = getState().workflowReq.get('params');
		const requestid = params.get('requestid');
		const userid = params.get('f_weaver_belongto_userid');
		const workflowid = params.get('workflowid');
		
		API_REQ.functionLink({
			requestid: requestid,
			f_weaver_belongto_userid: userid,
			f_weaver_belongto_usertype: 0,
			workflowid:workflowid,
			flag: 'rb'
		}).then(data => {
			dispatch(reloadRequestPage({requestid:requestid,'f_weaver_belongto_userid':userid,'f_weaver_belongto_usertype':0}));
		});
	}
}


//强制归档
export const doDrawBack = () =>{
	return(dispatch, getState) => {
		const needconfirm = getState().workflowReq.getIn(["params","needconfirm"])
		if(needconfirm == '1'){
			confirm({
			    content: '您确定将该流程强制归档吗？',
			    onOk() {
			    	dispatch(doingDrawBack());
			    },
			    onCancel(){
			    	return;
			    }
			});
		}else{
			dispatch(doingDrawBack());
		}
	}
}

export const doingDrawBack = (hiddenparams) => {
	return(dispatch, getState) => {
		const ischeckok = dispatch(beforeSubmitValidRemark(true));
		if(ischeckok){
			const hiddenparams = getState().workflowReq.get("submitParams").toJS();
			const formValue = reqUtil.getformdatas(getState().workflowReqForm);
			let params = objectAssign({},hiddenparams,formValue,{
				flag:'ov',
				fromflow:1
			});

			API_REQ.functionLink(params).then(data => {
				if(data.isShowChart && "1" == data.isShowChart){
					dispatch({type:types.REQ_IS_SUBMIT,bool:true});
					let {requestid,workflowid,isbill,formid,isfromtab,f_weaver_belongto_userid,f_weaver_belongto_usertype} = hiddenparams;
					window.location.href = `/workflow/request/WorkflowDirection.jsp?requestid=${requestid}&workflowid=${workflowid}&isbill=${isbill}&formid=${formid}&isfromtab=${isfromtab}&f_weaver_belongto_userid=${f_weaver_belongto_userid}&f_weaver_belongto_usertype&showE9Pic=1`;
					reqUtil.listDoingRefresh();
				}else{
					//重新加载列表
					dispatch(reqIsSubmit(true));
				}
			});
		}else{
			dispatch({type:types.CONTROLL_SIGN_INPUT,bool:true});
			message.warning('"签字意见"未填写',2);
			reqUtil.signMustInputTips();
		}
	}
}


//退回
export const doReject = () => {
	return(dispatch, getState) => {
        const ischeckok = dispatch(beforeSubmitValidRemark(true,"reject"));
        if(ischeckok){
			dispatch(loadRejectNodeInfo());
		}else{
            dispatch({type:types.CONTROLL_SIGN_INPUT,bool:true});
            message.warning('"签字意见"未填写',2);
            reqUtil.signMustInputTips();
		}
	}
}

export const loadRejectNodeInfo = () => {
	return(dispatch, getState) => {
		const params = getState().workflowReq.get('params').toJS();
		const {nodeid,workflowid,requestid,currentnodeid,f_weaver_belongto_userid} = params;
		API_REQ.getRejectInfo({
			nodeid:nodeid,
			workflowid:workflowid,
			requestid:requestid,
			currentnodeid:currentnodeid,
			f_weaver_belongto_userid:f_weaver_belongto_userid,
			f_weaver_belongto_usertype:'0'
		}).then(data=>{
			const type = data.type;
			if(type == '0'){
				dispatch(sureReject());
			} else if(type =='1'){
				const {RejectNodes,RejectToNodeid} = data;
				dispatch(sureReject(RejectNodes,RejectToNodeid));
			} else if(type =='2'){
				const {paramurl,dialogurl} = data;
				var dialog = new window.top.Dialog();
			    dialog.currentWindow = window;
			    dialog.callbackfunParam = null;
			    dialog.URL = dialogurl + escape(paramurl);
			    dialog.callbackfun = function (paramobj, id1) {
			    	if(id1){
				    	var array1  = id1.name.split('|');
				    	dispatch(sureReject(array1[0],array1[1],array1[2]));
			    	}
			    }
			    dialog.Title = "请选择 ";
			    dialog.Height = 400 ;
			    dialog.Drag = true;
			    dialog.show();
			}
		});
	}
}


export const sureReject = (RejectNodes,RejectToNodeid,RejectToType) => {
	return(dispatch, getState) => {
        const needconfirm = getState().workflowReq.getIn(['params','needconfirm']);
        let params = objectAssign({},{"RejectNodes":RejectNodes,"RejectToNodeid":RejectToNodeid,"RejectToType":RejectToType,"src":"reject","actiontype":"requestOperation"});
        if("1" == needconfirm){
           params.isaffirmance = "1";
           params.src = "save";
           params.isaffirmanceSrc = "reject";
		}
		dispatch(doSubmitApi(params));
	}	
}

//转发 意见征询 转办 控制
export const setShowForward = (forwardParams) => {
	return(dispatch, getState) => {
		dispatch({type: types.SET_SHOW_FORWARD,forwardParams : forwardParams});
	}
}


//流程删除
export const doDeleteE9 = () => {
	return (dispatch, getState) => {
		confirm({
			title:'信息确认',
			content:' 你确定删除该工作流吗？ ',
			onOk(){
				dispatch({type:types.FORM_LOADING,loading:true});
				const params  = {src: "delete",actiontype:"requestOperation"};
				dispatch(doSubmitPost(params));
			}
		});
	}
}

export const setShowBackToE8 = bool =>{
	return {type:types.SET_SHOWBACK_TO_E8,bool:bool};
}

export const aboutVersion  = (versionid) =>{
	warning({
	    content: "当前是V" + versionid + "版本",
	    okText: '确定',
	    onOk() {
	    	return;
	    }
	});
}

export const doEdit = () => {
	return (dispatch, getState) => {
		dispatch(reloadRequestPage({reEdit:"1",requestid:getState().workflowReq.getIn(["params","requestid"])}));
	}
}

export const updateSubmitToNodeId = () =>{
	return (dispatch, getState) => {
		const isSubmitDirectNode = getState().workflowReq.getIn(['rightMenu','isSubmitDirectNode']) || '';
		if("1" == isSubmitDirectNode){
			const lastnodeid = getState().workflowReq.getIn(['rightMenu','lastnodeid']) || ''; 
			dispatch(updateSubmitParams({SubmitToNodeid:lastnodeid}));
		}
	}
}


export const getAllFileUploadField = () =>{
	return (dispatch, getState) => {
		const fieldinfomap = getState().workflowReqForm.getIn(["conf","tableInfo","main","fieldinfomap"]);
		let allUploaded  = "uploaded";
		fieldinfomap.map((o)=>{
			if(o.get('htmltype') === 6){
				const type  = getState().workflowReqForm.getIn(["mainData","field"+o.get("fieldid"),"type"]); 
				if(type === "error"){
					allUploaded = "error";
				}else if(type === "uploading"){
					allUploaded = "uploading"
				}
			}
		});
		return allUploaded;
	}
}

//触发子流程
export const triggerSubWf =(subwfid,workflowNames) =>{
	return (dispatch, getState) => {
		workflowNames = workflowNames.replace(new RegExp(',',"gm"),'\n');
		confirm({
			content:"确定触发:"+workflowNames+"流程吗?",
			onOk(){
				dispatch({type:types.FORM_LOADING,loading:true});
				const formParams = getState().workflowReq.get("params");
				const params  = {
					"f_weaver_belongto_userid":formParams.get("f_weaver_belongto_userid"),
					"f_weaver_belongto_usertype":formParams.get("f_weaver_belongto_usertype"),
					requestId:formParams.get("requestid"),
					nodeId:formParams.get("nodeid"),
					paramSubwfSetId:subwfid
				};
				API_REQ.triggerSubWf(params).then(data => {
					warning({
					    content: "触发子流程成功",
					    okText: '确定',
					    showClose:false,
					    onOk() {
					    	dispatch({type:types.FORM_LOADING,loading:false});
					    	dispatch(reloadRequestPage({requestid:getState().workflowReq.getIn(["params","requestid"])}));
					    }
					});
				});
			}
		});
	}
}

//获取明细实时上传的附件信息
export const getUploadFileInfo =(fieldvalue,detailtype,fieldMark) =>{
	return (dispatch, getState) => {
		const params = getState().workflowReq.get("params").toJS();
		const reqParams = {
		    workflowid: params.workflowid,
		    f_weaver_belongto_userid: params.f_weaver_belongto_userid,
		    f_weaver_belongto_usertype: params.f_weaver_belongto_usertype,
		    isprint: params.isprint,
		    requestid:params.requestid,
		    nodetype:params.nodetype||0
		};
      	const reqDetailParams = {"fieldvalue":fieldvalue,"detailtype":detailtype,"reqParams":JSON.stringify(reqParams)};
        API_REQ.getUploadFileInfo(reqDetailParams).then(data=>{
        	dispatch(ReqFormAction.changeSingleFieldValue(fieldMark,{value:fieldvalue,specialobj:data.specilObj},{"fileUploadStatus":"uploaded"}));
        });
	}
}

//流程导入
export const requestImport =(imprequestid) =>{
	return (dispatch, getState) => {
		const params = getState().workflowReq.get("params");
		const importParams = {
			src:"import",
			imprequestid:imprequestid,
			workflowid:params.get("workflowid"),
			formid:params.get("formid"),
			isbill:params.get("isbill"),
			nodeid:params.get("nodeid"),
			"nodetype":params.get("nodetype"),
			isagent:params.get("isagent"),
			beagenter:params.get("beagenter")
		};
		API_REQ.requestImport(importParams).then(data=>{
			dispatch(reloadRequestPage({requestid:data.requestid}));
		});
	}
}


export const doImportDetail =() =>{
	return (dispatch, getState) => {
		const params = getState().workflowReq.get('submitParams');
		const userid = params.get('f_weaver_belongto_userid');
		const usertype  = params.get("f_weaver_belongto_usertype");
		const requestid  = params.get("requestid");
		const workflowid = params.get("workflowid");
		const nodeid = params.get("nodeid");
		
		if(requestid  == "-1"){
			confirm({
				content:"流程数据还未保存，现在保存吗? ",
				onOk(){
					const para = {actiontype:"requestOperation",src:"save"};
					dispatch(doSubmit(para));
				}
			});
		}else{
		    var dialog = new window.top.Dialog();
			dialog.currentWindow = window;
			var url = "/workflow/workflow/BrowserMain.jsp?url=/workflow/request/RequestDetailImport.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype="+usertype+"&requestid="+requestid+"&workflowid="+workflowid+"&nodeid="+nodeid+"&isfromE9=1";
			var title = "明细导入";
			dialog.Width = 550;
			dialog.Height = 550;
			dialog.Title=title;
			dialog.Drag = true;
			dialog.maxiumnable = true;
			dialog.URL = url;
			dialog.show();
		}
	}
}

//流转设置
export const doFreeWorkflow =() =>{
	return (dispatch, getState) => {
		const params = getState().workflowReq.get('params');
		const userid = params.get('f_weaver_belongto_userid');
		const usertype = params.get('f_weaver_belongto_usertype');
		const requestid  = params.get('requestid');
		const workflowid  = params.get('workflowid');
		const nodeid  = params.get('nodeid');
		const iscreate  = params.get('iscreate');
		const iscnodefree = getState().workflowReq.getIn(['rightMenu','iscnodefree'])||"";
		const isFree = getState().workflowReq.getIn(["rightMenu","isFree"]);
		const freewftype = getState().workflowReq.getIn(["rightMenu","freewftype"]);
		const initroad = getState().workflowReq.getIn(["rightMenu","freeNode","initroad"])||"-1";
		const initfrms = getState().workflowReq.getIn(["rightMenu","freeNode","initfrms"])||"-1";
		
		//自由流程
		let url = ""; 
		if(isFree == "1"){
			let dialog = new window.top.Dialog();
			if(freewftype == "2"){
				dialog.Width = 1000;
	    　			dialog.Height = 550;
				url = "/workflow/request/FreeNodeShow.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype="+	usertype+"&workflowid="+workflowid+"&requestid="+requestid+"&isroutedit="+initroad+"&istableedit="+initfrms+"&nodeid="+nodeid;
			}else{
				dialog.Width = 850;
		 		dialog.Height = 600;
				url = "/workflow/request/FreeNodeWin.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype="+	usertype+"&workflowid="+workflowid+"&requestid="+requestid+"&nodeid="+nodeid+"&isdialog=1&isset=1&tt="+new Date().getTime();
			}
			dialog.URL =url;
			dialog.Title = "自由节点";
	        dialog.maxiumnable = false;
	    　		dialog.show();
	      	window.top.freewindow = window;
	        window.top.freedialog = dialog;
		}else{
			if(iscreate == "1"){
				confirm({
					content:"流程数据还未保存，现在保存吗? ",
					onOk(){
						const para = {actiontype:"requestOperation",src:"save"};
						dispatch(doSubmit(para));
					}
				});
			}else{
				url="/workflow/workflow/BrowserMain.jsp?&url=/workflow/request/FreeWorkflowSet.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype=0&requestid="+requestid+"&iscnodefree="+iscnodefree+"&nodeid="+nodeid;
				let dialog = new window.top.Dialog();
				dialog.currentWindow = window;
				dialog.Width = 550;
				dialog.Height = 550;
				dialog.Modal = true;
				dialog.Title = '流转设定';
				dialog.URL =url;
				dialog.isIframe=false;
				dialog.show();
			}
		}
	}
}

//修改loading状态
export const updateFormLoading = (flag) =>{
    return (dispatch, getState) => {
        dispatch({type: types.FORM_LOADING, loading: flag});
    }
}

export const reloadRequestPage = (params) =>{
	 return (dispatch, getState) => {
	 	dispatch(setReqIsUnbeforeunload(false));
		let para = "";
		for(let i in params){
			para += `&${i}=${params[i]}`;
		}
		if(para != ""){
			para = para.substr(1);
		}
		window.location.href = "/workflow/request/ViewRequestForwardSPA.jsp?"+para;
	}
}

//离开表不需要保存提示
export const setReqIsUnbeforeunload =(bool) =>{
	return (dispatch, getState) => {
        dispatch({type: types.REQ_IS_UNBEFOREUNLOAD, bool: bool});
    }
}
//附件下载
export const setFileDownloadUrl =(fileDownloadUrl) =>{
	return (dispatch, getState) => {
        dispatch({type: types.REQ_FILEDOWNLOAD, fileDownloadUrl: `${fileDownloadUrl}&_random=${new Date().getTime()}`});
    }
}
