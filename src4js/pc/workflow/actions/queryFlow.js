import * as types from '../constants/ActionTypes'
import * as API_QUERY from '../apis/queryFlow'
import * as API_TABLE from '../apis/table'

import {WeaTable} from 'comsRedux'

const WeaTableAction = WeaTable.action;

import {WeaTools} from 'ecCom'
import {Modal} from 'antd'

//是否加载中
export const setLoading = (value) => {
    return (dispatch, getState) => {
        dispatch({type: types.QUERY_FLOW_LOADING, value:value});
    }
}

//设置覆盖redux状态
export const coverState = (value = {}) =>{
    return (dispatch, getState) => {
        dispatch({type: types.QUERY_FLOW_COVER_STATE, value:value});
    }
}

//初始化查询条件
export const initCondition = (params) => {
    return (dispatch, getState) => {
        const {ls} = WeaTools;
        API_QUERY.getQueryCondition().then((data)=>{
            ls.set("queryFlowCondition", data.condition);
            dispatch({type: types.QUERY_FLOW_INIT_BASE, value: data.condition});
        });
    }
}

//加载自定义查询条件
export const loadCustomCondition = (params) =>{
    return (dispatch, getState) => {
        dispatch(setLoading(true));
        API_QUERY.getCustomQueryCondition(params).then(data =>{
            let changeInfo = {cusCondition: data.condition};
            if(data && data.option){
                changeInfo.cusOption = data.option;
                if(changeInfo.cusOption && changeInfo.cusOption.length > 0){
                    const customid = changeInfo.cusOption[0].key;
                    dispatch({type: types.QUERY_FLOW_SET_CUSTOMID, value: customid});
                }
            }
            dispatch({type: types.QUERY_FLOW_SET_CUS_CONDITION, value:changeInfo});
        });
    }
}

//切换自定义查询类型
export const changeCustomid = (customid) =>{
    return (dispatch,getState) =>{
        dispatch({type: types.QUERY_FLOW_SET_CUSTOMID, value: customid});
        dispatch(loadCustomCondition({type:"changeType", customid:customid}));
    }
}

//初始化查询结果树
export const initTree = () => {
    return (dispatch, getState) => {
        const {ls} = WeaTools;
        const searchParamsUrl = getState()['workflowqueryFlow'].get("searchParamsUrl").toJS();
        API_QUERY.queryFieldsTree({...searchParamsUrl}).then((data)=>{
            ls.set("queryFlowleftTree", data.treedata);
            dispatch({type: types.QUERY_FLOW_INIT_TREE, value: data.treedata});
        })
	}
}

//树选中
export const setSelectedTreeKeys = (value = []) => {
	return (dispatch, getState) => {
		dispatch({type: types.QUERY_FLOW_SET_SELECTED_TREEKEYS,value:value})
	}
}

//查询参数
export const setSearchUrlParams = (params = {}) => {
    return (dispatch, getState) => {
          dispatch({type: types.QUERY_FLOW_SET_SEARCH_URLPARAMS, value: params});
	}
}

//保存查询条件值信息
export const setFormFields = (value = {}) => {
    return (dispatch, getState) => {
        dispatch({type: types.QUERY_FLOW_SET_FORM_FIELDS, value: value})
    }
}

//追加查询条件值信息
export const appendFormFields = (value = {}) => {
    return (dispatch, getState) => {
        const originalFields = getState()['workflowqueryFlow'].get("fields").toJS();
        dispatch(setFormFields({...originalFields, ...value}));
    }
}

//清除查询条件值信息
export const clearFormFields = (type = "all") =>{
    return (dispatch, getState) => {
        if(type === "all")      //清除全部条件
            dispatch(setFormFields({}));
        else if(type === "clearcustom"){    //清除自定义查询条件

        }
    }
}

//查询数据
export const doSearch = (params = {}) => {
    return (dispatch, getState) => {
        let searchParamsAd = {};    //高级搜索的查询条件
        const fields = getState()['workflowqueryFlow'].get("fields");
        fields && fields.map((v,k) =>{
            searchParamsAd[v.get("name")] = v.get("value");
        });
        const searchParamsUrl = getState()['workflowqueryFlow'].get("searchParamsUrl").toJS();
        API_QUERY.queryFieldsSearch({...searchParamsUrl,...searchParamsAd}).then((data)=>{
            dispatch(WeaTableAction.getDatas(data.sessionkey, params.current || 1));
            dispatch({type: types.QUERY_FLOW_SEARCH_RESULT, value: data.sessionkey});
        });
    }
}

//是否显示高级搜索界面
export const setShowSearchAd = (value) => {
    return (dispatch, getState) => {
        dispatch({type: types.QUERY_FLOW_SET_SHOW_SEARCHAD, value: value})
    }
}

//清除所有状态
export const clearAllState = () =>{
    return (dispatch, getState) => {
        dispatch({type: types.QUERY_FLOW_CLEAR});
    }
}

//查询-批量共享
export const batchShareWf = (ids = '') => {
    return (dispatch, getState) =>{
        let eh_dialog = null;
        if(window.top.Dialog)
            eh_dialog = new window.top.Dialog();
        else
            eh_dialog = new Dialog();
        eh_dialog.currentWindow = window;
        eh_dialog.Width = 650;
        eh_dialog.Height = 500;
        eh_dialog.Modal = true;
        eh_dialog.maxiumnable = false;
        eh_dialog.Title = "批量共享";
        eh_dialog.URL = "/workflow/request/AddWorkflowBatchShared.jsp?ids="+ids;
        eh_dialog.show();
    }
}

