import Immutable from 'immutable'
import * as types from '../constants/ActionTypes'
// import cloneDeep from 'lodash/cloneDeep'
import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

const initState = {
    title: '查询流程',
    loading: false,
    condition: ls.getJSONObj('queryFlowCondition') || [],
    btninfo: [],
    fields: {},
    dataKey: '',
    showSearchAd: false,
    leftTree: ls.getJSONObj('queryFlowleftTree') || [],
    searchParamsUrl: {},
    selectedTreeKeys: [],
    customid: "",
    cusOption: [],
    cusCondition: []
};

let initialState = Immutable.fromJS(initState);

export default function list(state = initialState, action) {
    switch (action.type) {
        case types.QUERY_FLOW_LOADING:
            return state.set("loading", action.value);
        case types.QUERY_FLOW_COVER_STATE:
            return function(){      //直接设置覆盖
                state = state.filterNot((v,k) => k in action.value);
                return state.merge(action.value);
            }();
    	case types.QUERY_FLOW_SET_SELECTED_TREEKEYS:
      		return state.merge({selectedTreeKeys: action.value});
    	case types.QUERY_FLOW_SET_SEARCH_URLPARAMS:
      		return state.merge({searchParamsUrl: action.value});
    	case types.QUERY_FLOW_INIT_TREE:
      		return state.merge({leftTree: action.value});
        case types.QUERY_FLOW_INIT_BASE:
            return state.merge({condition:action.value});
        case types.QUERY_FLOW_SET_FORM_FIELDS:
            return state.set("fields", Immutable.fromJS(action.value));  //条件值需覆盖
        case types.QUERY_FLOW_SEARCH_RESULT:
            return state.merge({dataKey: action.value, loading: false});
        case types.QUERY_FLOW_SET_SHOW_SEARCHAD:
            return state.merge({showSearchAd: action.value});
        case types.QUERY_FLOW_SET_CUS_CONDITION:
            return function(){
                state = state.filterNot((v,k) => k in action.value);    //条件需覆盖
                return state.merge(action.value).set("loading", false);
            }();
        case types.QUERY_FLOW_SET_CUSTOMID:
            return state.set("customid", action.value);
        case types.QUERY_FLOW_CLEAR:
            return initialState;
        default:
            return state;
    }
}