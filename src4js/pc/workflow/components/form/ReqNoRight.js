import {WeaAlertPage} from 'ecCom';

class ReqNoRight extends React.Component {
    componentDidMount() {
        document.title = "无权限";
    }
    render() {
        const paddingTop = (jQuery(window).height()-420)/2;
        return <WeaAlertPage iconSize={200} style={{paddingTop:paddingTop}}>
            <div style={{fontSize: "16px", color:"#FFBB0E"}}>
                对不起，您暂时没有权限！
            </div>
        </WeaAlertPage>
    }
}

export default ReqNoRight