import {
    WeaTab,
    WeaSearchGroup,
    WeaInput,
    WeaDateGroup,
    WeaHrmInput,
    WeaDepInput,
    WeaComInput,
    WeaSelect,
    WeaBrowser
} from 'ecCom'

import {Row, Col, Icon, Pagination, Menu, Form, Button,Spin} from 'antd';

import objectAssign from 'object-assign'

import Immutable from 'immutable'

import SignListItem from './SignListItem'
const is = Immutable.is;

const createForm = Form.create;
const FormItem = Form.Item;

let sb = ''
class Sign extends React.Component {
	constructor(props) {
		super(props);
        sb = this;
    }
	shouldComponentUpdate(nextProps,nextState) {
        return  !is(this.props.markInfo,nextProps.markInfo)||
	        !is(this.props.logList,nextProps.logList)||
	        !is(this.props.isShowSignInput,nextProps.isShowSignInput)||
	        !is(this.props.logListTabKey,nextProps.logListTabKey)||
	        !is(this.props.signFields,nextProps.signFields)||
	        !is(this.props.showSearchDrop,nextProps.showSearchDrop)||
	        !is(this.props.isShowUserheadimg,nextProps.isShowUserheadimg)||
	        this.props.showuserlogids !== nextProps.showuserlogids||
	        this.props.reqRequestId !== nextProps.reqRequestId||
	        this.props.pagesize !== nextProps.pagesize||
	        this.props.isLoadingLog !== nextProps.isLoadingLog||
	        this.props.forward !== nextProps.forward;
    }
	componentDidMount() {
		const {actions} = this.props;
    	actions.setLoglistTabKey('1');
    }
	componentWillUnmount(){
		const {actions} = this.props;
		actions.saveSignFields({});
		actions.setShowSearchDrop(false);
	}
	renderTitle(text,arr){
		return
	}
    render() {
    	const {actions,params,logList,current,total,pagesize,forward,showuserlogids,
    		logListTabKey,requestLogParams,requestType,isShowSignInput,isShowUserheadimg,signFields,showSearchDrop,reqRequestId,isLoadingLog} = this.props;
    	let tabDatas = [{title:'流转意见',key:"1"}];
    	const isRelatedTome = requestLogParams.get('isRelatedTome');
    	const hasMainWfRight = requestLogParams.get('hasMainWfRight');
    	const hasChildWfRight = requestLogParams.get('hasChildWfRight');
    	const hasParallelWfRight = requestLogParams.get('hasParallelWfRight');
    	const isReadParallel  = requestLogParams.get('isReadParallel');
    	const requestid = params.get('requestid');
    	const workflowid = params.get('workflowid');
    	const signListType = params.get('signListType');
    	const authStr = params.get("authStr");
    	const authSignatureStr = params.get("authSignatureStr");
    	const userSupportEdit = params.get("userSupportEdit");

    	//设置主子流程名称连接信息
    	let signshowname = '';
	  	logListTabKey > 2 && requestLogParams.get('allrequestInfos').filter(reqinfo => {
			if(reqinfo.get('requestid') == reqRequestId){
				signshowname = reqinfo.get('signshowname');
			}
		});
		
		let mainWf = hasMainWfRight ? requestLogParams.get('allrequestInfos').filter(w=>w.get('type') == 'main') : '';
		let childWf = hasChildWfRight ? requestLogParams.get('allrequestInfos').filter(w=>w.get('type') == 'sub') : '';
		let parallelWf = hasParallelWfRight ?　requestLogParams.get('allrequestInfos').filter(w=>w.get('type') == 'parallel') : '';

		isRelatedTome && tabDatas.push({title:'与我相关',key:"2"});
		mainWf && tabDatas.push({title:'主流程意见',key:"3"});
		childWf && tabDatas.push({title:<span>子流程意见 <i className='icon-top-Arrow' /></span>,dropMenu:childWf.toJS(),key:"4"});
		parallelWf && '1' == isReadParallel && tabDatas.push({title:<span>平行流程意见 <i className='icon-top-Arrow' /></span>,dropMenu:parallelWf.toJS(),key:"5"});

		let listShow = [];
		logList && logList.forEach(obj=>{
			listShow.push(<SignListItem userSupportEdit={userSupportEdit} authStr={authStr} authSignatureStr={authSignatureStr} data={obj} isShowUserheadimg={isShowUserheadimg} actions={actions} forward={forward} requestid={requestid} workflowid={workflowid} showuserlogids={showuserlogids} f_weaver_belongto_userid={params.get('f_weaver_belongto_userid')}/>);
		});
		const signListH = (showSearchDrop && jQuery('.wea-workflow-req-sign-list').height() < 297) ? {height:'297px'}:{};
        return (
            <div className='wea-workflow-req-sign'>
            	<div>
	            	<WeaTab
	                    buttons={this.getTabButtons()}
	                    datas={tabDatas}
	                    selectedKey={logListTabKey}
	                    keyParam="key"  //主键
	                    searchType={['drop']}
	                    countParam='count'
	                    hasDropMenu={true}
	                    showSearchDrop={showSearchDrop}
	                    dropIcon={<i className='icon-search-search' style={{color:'#77da88'}} onClick={()=>actions.setShowSearchDrop(true)}/>}
	                    searchsDrop={<Form horizontal>{this.getSearchs()}</Form>}
	                    buttonsDrop={this.getTabButtonsDrop()}
	                    setShowSearchDrop={bool => actions.setShowSearchDrop(bool)}
	                    onChange={this.changeData.bind(this)} />
	                {logListTabKey > 2 &&  logListTabKey != reqRequestId  &&
		                <div className='wea-workflow-main-sub-req'>
		                	<span dangerouslySetInnerHTML={{__html:signshowname}}></span>
		                </div>
	                }
	            	<div className='wea-workflow-req-sign-list' style={signListH}>
		            	{listShow}
		            	{listShow && !signListType && listShow.length > 0 && <Pagination defaultCurrent={1} showQuickJumper
		            		pageSize={pagesize}
		            		onChange={this.onPageChange.bind(this)}
		            		current={current}
		            		total={total}
		            		showSizeChanger
		            		pageSizeOptions={['10', '20', '50', '100']}
		            		onShowSizeChange={this.onPageSizeChange.bind(this)}
		            		showTotal={total => `共 ${total} 条`}
		            	/>}
		            	{listShow && !isLoadingLog && listShow.length == 0 &&
		            		<div className='ant-table-placeholder' style={{borderBottom:0}}>暂时没有数据</div>
		            	}
		            	{isLoadingLog &&
		            		<div className='ant-table-placeholder' style={{borderBottom:0}}>
		            			<Spin tip="正在读取数据..."></Spin>
		            		</div>
		            	}
		            </div>
            	</div>
            </div>
        )
    }
    onPageChange(n){
    	const {actions,logListTabKey} = this.props;
    	if(logListTabKey < 3){
	    	if(typeof this.props.onChange == 'function') {
	    		this.props.onChange(n);
	    	}
    	}else{
    		actions.loadRefReqSignInfo({pgnumber:n});
    	}
    }
    onPageSizeChange(current, pageSize){
    	if(typeof this.props.onPageSizeChange == 'function')
    		this.props.onPageSizeChange(pageSize);
    }
    getTabButtons(){
    	const {isShowUserheadimg,actions} = this.props;
    	return [(
    		<i className='icon-search-Customer' style={{color:isShowUserheadimg?'#7287f2':''}} onClick={()=>actions.updateUserTxStatus(!isShowUserheadimg)}/>
    	),(<span style={{fontSize:16,color:'#d6d6d6'}}>|</span>)]
    }
    changeData(key,selecttabkey){
    	if(key === '4' || key  === '5') return;
    	const {actions,userId,requestLogParams,pagesize,logListTabKey} = this.props;
    	//计算tabkey
    	let tmpkey = key;
    	let params = {pgnumber:1,maxrequestlogid:0};
    	requestLogParams.get('allrequestInfos') && requestLogParams.get('allrequestInfos').filter(reqinfo =>{
    		if(reqinfo.get('requestid') == key || key === '3'){
    			if('main' == reqinfo.get('type')){
    				tmpkey = '3';
    				key = reqinfo.get('requestid');
    			} 
    			if('sub'  == reqinfo.get('type')) tmpkey = '4';
    			if('parallel' ==reqinfo.get('type')) tmpkey = '5';
   				params.requestid  = reqinfo.get('requestid');
				params.workflowid = reqinfo.get('relwfid');
				params.requestLogParams = JSON.stringify({
					viewLogIds:reqinfo.get('relviewlogs'),
					wfsignlddtcnt:requestLogParams.get('wfsignlddtcnt')
				});
				params.pgnumber = 1;
				params.firstload = false;
				params.maxrequestlogid = 0;
				params.loadmethod = 'split';
				params.logpagesize = pagesize;
    		}
    	});
    	actions.saveSignFields({});	
    	actions.setLoglistTabKey(tmpkey,key);
    	if(key === '2') params.atmet = userId;
    	if(key === '1') params.atmet = '';
    	if(key === '1' || key === '2'){
    		actions.setlogParams(params);
    	}else{
			actions.loadRefReqSignInfo(params);
    	}
    }

    getTabButtonsDrop(){
    	const {actions} = this.props;
        return [
            (<Button type="primary" onClick={()=>{actions.clearLogData();actions.setMarkInfo();actions.setShowSearchDrop(false)}}>搜索</Button>),
            (<Button type="ghost" onClick={()=>{actions.saveSignFields({})}}>重置</Button>),
            (<Button type="ghost" onClick={()=>{actions.setShowSearchDrop(false)}}>取消</Button>)
        ]
    }
    getSearchs() {
    	const {form, requestLogParams} = this.props;
        const {getFieldProps} = this.props.form;
        const nodes = [{key:'',showname:''}];
        let items = new Array();
        if(requestLogParams && requestLogParams.has("logCondition")){
        	const logCondition = requestLogParams.get("logCondition").toJS();
        	logCondition.map((o,i)=>{
        		if(o.conditionType === "BROWSER"){
        			const hrmprop  = getFieldProps(o.domkey[0]);
        			const hrmid  = hrmprop.value||"";
        			let replaceDatas  = ("" === hrmid) ? [] : [{id:hrmid,lastname:hrmprop.valueSpan,name:hrmprop.valueSpan}];
        			items.push({
        				com:(<FormItem label={o.label} labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}> <WeaBrowser {...o.browserConditionParam} replaceDatas={replaceDatas} {...getFieldProps(o.domkey[0])} /></FormItem>),
        				colSpan:1
        			})
        		}else if(o.conditionType === "INPUT"){
        			items.push({
        				com:(<FormItem label={o.label} labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}> <WeaInput {...getFieldProps(o.domkey[0])} /></FormItem>),
        				colSpan:1
        			});
        		}else if(o.conditionType === "SELECT"){
        			items.push({
        				com:(<FormItem label={o.label} labelCol={{ span: 6 }} wrapperCol={{ span: 18 }}> <WeaSelect options={o.options} {...getFieldProps(o.domkey[0])} /></FormItem>),
        				colSpan:1
        			});
        		}else if(o.conditionType === "DATE"){
        			items.push({
			            com:(<FormItem
			            label={o.label}
			            labelCol={{ span: 6 }}
			            wrapperCol={{ span: 18 }}>
			            	<WeaDateGroup {...getFieldProps("createdateselect",{
			                	initialValue: '0'
			                })} datas={ [
					            {value: '0',selected: true,name: '全部'},
					            {value: '1',selected: false,name: '今天'},
					            {value: '2',selected: false,name: '本周'},
					            {value: '3',selected: false,name: '本月'},
					            {value: '4',selected: false,name: '本季'},
					            {value: '5',selected: false,name: '本年'},
					            {value: '6',selected: false,name: '指定日期范围'}
					        ]} form={this.props.form} domkey={["createdateselect","createdatefrom","createdateto"]} />
			            </FormItem>),
			            colSpan:1
			        });
        		}
        	})
        }
        return [<WeaSearchGroup title="查询条件" showGroup={true} items={items}/>]
    }
}

Sign = createForm({
	onFieldsChange(props, fields){
		const orderFields = objectAssign({},props.signFields.toJS(),fields);
		props.actions.saveSignFields(orderFields);
	},
	mapPropsToFields(props) {
		return props.signFields.toJS();
  	}
})(Sign);

export default Sign