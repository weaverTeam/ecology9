class CustomContent extends React.Component{
    shouldComponentUpdate(nextProps) {
        return false;   //此组件存储代码块及custompage信息，不做render判断，只通过操作DOM方式append
    }

    render(){
        return <div id="customDiv">
            <div id="scriptArea"></div>
            <div id="customPageArea"></div>
        </div>
    }
}

export default CustomContent