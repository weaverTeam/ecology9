import {is} from 'immutable'
import debounce from 'lodash/debounce'
import trim from 'lodash/trim'
import objectAssign from 'object-assign'
import * as formUtil from '../../../util/formUtil'
import {WeaBrowser,WeaDatePicker,WeaTimePicker,WeaSelect,WeaCheckbox,WeaUpload} from 'ecCom'
import {Row, Col, Button, Icon, Upload, DatePicker, TimePicker} from 'antd'
import WeaSystemField from '../../cloudComponents/wea-system-field/index'
import WeaWfCode from '../../cloudComponents/wea-wfcode/index'
import WeaWfInput from '../../cloudComponents/wea-wf-input/index'
import WeaTextarea from '../../cloudComponents/wea-textarea/index'
import * as DocUtil from '../../../util/docUtil'
import {updateHiddenSysFieldname} from '../../../util/reqUtil'

class FieldContent extends React.Component {
    componentDidMount(){
        const {symbol,cellObj,conf} = this.props;
        const tableMark = symbol.indexOf("detail_")>-1 ? symbol : "main";
        const fieldid = cellObj.get("field").toString();
        const fieldObj = conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid]);
        //特殊字段--链接含JS情况下通过Append方式渲染
        if(fieldObj && fieldObj.get("htmltype") === 7 && fieldObj.get("detailtype") === 1){
            const displayname = fieldObj.get("specialattr") ? fieldObj.getIn(["specialattr","displayname"]) : "";
            const linkaddress = fieldObj.get("specialattr") ? fieldObj.getIn(["specialattr","linkaddress"]) : "";
            jQuery(this.refs.specialFieldSpan).append("<a href='"+linkaddress+"' target='_blank'>"+displayname+"</a>");
        }
    }
    shouldComponentUpdate(nextProps) {
		return this.props.symbol !== nextProps.symbol
            || this.props.rowIndex !== nextProps.rowIndex
            || !is(this.props.fieldValueObj, nextProps.fieldValueObj)
            || !is(this.props.dependValueObj, nextProps.dependValueObj)
            || !is(this.props.fieldVar, nextProps.fieldVar)
            || !is(this.props.cellObj, nextProps.cellObj)
            || !is(this.props.params, nextProps.params)      //只有表单签字意见信息才会更改params
            || this.props.conf.size !== nextProps.conf.size;
    }
    componentDidUpdate(prevProps){
        //触发开发代码的bindPropertyChange相关(放在渲染后触发解决避免bindPropertyChange函数体又修改字段本身值情况)
        if(this.props.cellObj.get("field") === prevProps.cellObj.get("field") && this.props.symbol === prevProps.symbol && this.props.rowIndex === prevProps.rowIndex){   //表示同一字段
            const isDetail = this.props.symbol.indexOf("detail_")>-1;
            const fieldid = this.props.cellObj.get("field").toString();
            const rowIndex = isDetail ? this.props.rowIndex.toString() : "-1";
            const tableMark = isDetail ? this.props.symbol : "main";
            const fieldObj = this.props.conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid]);
            let oldValue = prevProps.fieldValueObj && prevProps.fieldValueObj.has("value") ? prevProps.fieldValueObj.get("value").toString() : "";
            let newValue = this.props.fieldValueObj && this.props.fieldValueObj.has("value") ? this.props.fieldValueObj.get("value").toString() : "";
            if(fieldObj.get("htmltype") === 1 && fieldObj.get("detailtype") !== 1){     //数值类型
                if(oldValue !== "")     oldValue = parseFloat(oldValue);
                if(newValue !== "")     newValue = parseFloat(newValue);
            }
            if(oldValue !== newValue){      //三等号判断值是否发生变化
                const fieldMark = isDetail ? `field${fieldid}_${rowIndex}` : `field${fieldid}`;
                const __propertyChangeFnObj = WfForm.__propertyChangeFnObj;
                if(fieldMark in __propertyChangeFnObj){
                    __propertyChangeFnObj[fieldMark].map(fn =>{
                        try{
                            fn(jQuery("#"+fieldMark)[0], fieldMark, newValue);
                        }catch(e){}
                    });
                }
                if(isDetail){   //明细字段触发监听事件
                    const __detailFieldChangeFnObj = WfForm.__detailFieldChangeFnObj;
                    if(`field${fieldid}` in __detailFieldChangeFnObj){
                        __detailFieldChangeFnObj[`field${fieldid}`].map(fn =>{
                            try{
                                fn(`field${fieldid}`, rowIndex, newValue);
                            }catch(e){}
                        });
                    }
                }
            }
        }
    }
    getFieldMark(){
        const {symbol,cellObj,rowIndex} = this.props;
        const fieldid = cellObj.get("field").toString();
        return symbol.indexOf("detail_")>-1 ? `field${fieldid}_${rowIndex}` : `field${fieldid}`;
    }
	doChangeEvent(value){
		const valueInfo = { value: value || ''};
		this.props.actions.changeSingleFieldValue(this.getFieldMark(), valueInfo);
	}
	doBrowChangeEvent(ids,names,datas){
		const valueInfo = { value: ids || '',specialobj:datas||[]};
		const {conf,symbol,cellObj,rowIndex,params} = this.props;
		const groupInfo = formUtil.getMultiUserGroupInfo(conf,symbol,cellObj,datas,ids)||{};
		const fieldid = cellObj.get("field").toString();
		const isDetail = symbol.indexOf("detail_")>-1;
		const tableMark = isDetail ? symbol : "main";
		const fieldObj = conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid]);
		const detailtype = fieldObj ? fieldObj.get("detailtype") : 0;
		if(detailtype === 141){
			valueInfo.value = formUtil.spellResourceCondition(datas);
		}
		this.props.actions.changeSingleFieldValue(this.getFieldMark(), objectAssign({},valueInfo,groupInfo));
		formUtil.doDefinitionCallBackFun(fieldid,rowIndex,ids,datas,fieldObj,params,isDetail);
	}
    doDisablePromptEvent(){
        this.props.actions.showPrompt(this.getFieldMark(), 2);
    }
	clickAddBtn(){
    	const {conf,symbol,cellObj,rowIndex} = this.props;
		const fieldid = cellObj.get("field").toString();
		const isDetail = symbol.indexOf("detail_")>-1;
		const tableMark = isDetail ? symbol : "main";
		const fieldObj =  conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid]);
        const htmltype = fieldObj.get("htmltype")||0;
        const detailtype = fieldObj.get("detailtype")||0;
        //添加文档
        if(htmltype === 3 && (detailtype ===9 || detailtype === 37)){ 
        	let createDocParmas = {fieldid:isDetail?`${fieldid}_${rowIndex}`:fieldid};
        	DocUtil.createDoc(createDocParmas);	
        }
	}
	
	fileChangeEvent(ids,filedatas){
		let {fieldValueObj} = this.props;
		//const fieldvalue  = idsT.concat(idsB).join(',');
		//const fieldid = cellObj.get("field").toString();
        //symbol = symbol.indexOf("detail_") > -1 ? symbol : "main";
		//const fieldObj = conf.getIn(["tableInfo", symbol, "fieldinfomap", fieldid])
		//const detailtype = fieldObj.get("detailtype")||0;
		//this.props.actions.getUploadFileInfo(fieldvalue,detailtype,this.getFieldMark());
        const specialobj = (fieldValueObj && fieldValueObj.has("specialobj"))? fieldValueObj.get("specialobj").toJS():{};
        specialobj.filedatas = filedatas;
		this.props.actions.changeSingleFieldValue(this.getFieldMark(),{value:ids.join(","),specialobj:specialobj},null);
	}

	onUploading(type){
		let valueInfo = null;
		const {fieldValueObj} = this.props;
		let fieldValue = fieldValueObj && fieldValueObj.has("value") ? fieldValueObj.get("value").toString() : "";
		const fileUploadStatus = {};
		//type = true 标记改字段值为空 type = false 标记该字段值不为空 这个时候不需要更新状态
		if("false" == type && "" == fieldValue){
			valueInfo = {value:"-1"};
		}else if("true" == type){ 
			valueInfo = {value:""};
		}
		if(type != "false" && type != "true"){
			fileUploadStatus.fileUploadStatus = type;
		}
        this.props.actions.changeSingleFieldValue(this.getFieldMark(),valueInfo,fileUploadStatus);
	}

    dateChangeEvent(value,dateString){
    	const valueInfo = { value: dateString };
		this.props.actions.changeSingleFieldValue(this.getFieldMark(), valueInfo);
    }
    render() {
        const {actions,conf,params,symbol,rowIndex,cellObj,fieldValueObj,dependValueObj,fieldVar} = this.props;
        let {fieldStyle} = this.props;
        const fieldid = cellObj.get("field").toString();
        const isDetail = symbol.indexOf("detail_")>-1;
        const tableMark = isDetail ? symbol : "main";
        let fieldValue = fieldValueObj && fieldValueObj.has("value") ? fieldValueObj.get("value").toString() : "";
        const fieldObj = conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid]);
        //if(window.console) console.log("fieldid--",fieldid, "rowIndex--",rowIndex,"fieldValue--",fieldValue,"fieldObj---",fieldObj.toJS());
        const htmltype = fieldObj.get("htmltype")||0;
        let detailtype = fieldObj.get("detailtype")||0;
        const fieldName  = isDetail ? ('field'+fieldid+'_'+rowIndex) : ('field'+fieldid);
        //当前字段的只读、必填属性
        const viewAttr = formUtil.getFieldCurViewAttr(fieldObj, tableMark, conf, fieldVar, params);
       	const baseProps = {};
        baseProps.isDetail = isDetail;
        fieldValue = formUtil.restoreSpeicalChar(fieldValue, htmltype, detailtype, viewAttr, fieldid, conf, params);    //特殊字符转换
        baseProps.value = fieldValue;
        baseProps.detailtype = detailtype;
        baseProps.fieldName  = fieldName;
        baseProps.viewAttr  = viewAttr;
        baseProps.fieldid = fieldid;
        baseProps.fieldlabel = fieldObj && fieldObj.get("fieldlabel");
        baseProps.rowIndex = rowIndex;
        baseProps.style = fieldStyle;
        baseProps.defaultFocus = fieldVar && fieldVar.get("promptRequired") === true ? true : false;
        if(viewAttr !== 1 && (htmltype === 1 || htmltype === 2)){   //文本类型字段空值显示的默认值
            baseProps.emptyShowContent = fieldVar && fieldVar.get("emptyShowContent") || "";
        }
        //console.log("baseProps",baseProps);
        if(parseInt(fieldid) < 0){      //系统字段
            const markInfo = params.get("markInfo");
            const requestType = params.get("requestType");
            baseProps.fieldName  = updateHiddenSysFieldname(fieldName);
            return (
                <WeaSystemField requestType={requestType}
                				markInfo={markInfo}
                				onChange={this.doChangeEvent.bind(this)}
                				{...baseProps}/>
            )
        }else if(htmltype === 1){
            //判断是否为流程编号
            const codeInfo = conf.get("codeInfo");
            const isUse = codeInfo && codeInfo.get("isUse");
            const fieldCode = codeInfo && codeInfo.get("fieldCode");
            const relateCodeFields = codeInfo && codeInfo.get("codefields");
            const hasHistoryCode = codeInfo && codeInfo.get("hasHistoryCode");
            if(isUse && fieldCode === fieldid){
                const relateFieldValues = this.getCodeRelateFieldValues(relateCodeFields);
                return <WeaWfCode actions={actions}
                            relateFieldValues={relateFieldValues}
                            hasHistoryCode={hasHistoryCode}
                            {...baseProps}
                            iscreate={params.get("iscreate")}
                            onChange={this.doChangeEvent.bind(this)}
                        />
            }
            const qfws = fieldObj.get("qfws");
            const length = fieldObj.get("length");
            let format = cellObj.has("format") ? cellObj.get("format").toJS():{};
            const transtype = parseInt(conf.getIn(["linkageCfg", "transTypeCfg", fieldid]) || 0);   //字段赋值转格式，模拟用格式化实现
            //判断当前字段是否为行规则计算赋值字段，不允许修改
            const isRowCalAssignField = symbol.indexOf("detail_") > -1 && formUtil.judgeFieldBelRowCalAssignField(conf, symbol, fieldid);
            const enableInput = !isRowCalAssignField;
            if(transtype === 1){    //转金额大写
                format = {numberType:101};
            }else if(transtype === 2){      //转金额千分位，模拟数值格式化
                format = {numberType:2,decimals:qfws,formatPattern:2,thousands:1};
            }
            if(cellObj.has("financial")){      //财务格式优先级最高
                const financial = cellObj.get("financial");
                if(financial.indexOf("2-") > -1){
                    const finaNum = parseInt(financial.split("-")[1] || "3");
                    format = {numberType:100,finaNum:finaNum};
                }else if(financial === "3"){        //转金额大写
                    format = {numberType:101};
                }else if(financial === "4"){        //转金额千分位，模拟数值格式化两位
                    format = {numberType:2,decimals:2,formatPattern:2,thousands:1};
                }
            }
            baseProps.showhtml = fieldValueObj && fieldValueObj.has("showhtml") ? fieldValueObj.get("showhtml") : "";
            return(
                <WeaWfInput length={length}
                            qfws={qfws}
                            onChange={this.doChangeEvent.bind(this)}
                            format={format}
                            enableInput={enableInput}
                            onDisablePrompt={this.doDisablePromptEvent.bind(this)}
                            {...baseProps}
                    />
            );
        }else if(htmltype === 2){
            const textheight = fieldObj ? fieldObj.get('textheight') : 4;
            const length = fieldObj ? fieldObj.get('length') : 4000;
            return( <WeaTextarea textheight = { textheight }
	                             length = { length }
	                             onChange = {this.doChangeEvent.bind(this)}
	                             {...baseProps}
                    />
            )
        }else if(htmltype === 3){
            if(fieldVar && fieldVar.has("changeBrowType"))    //选择框联动修改浏览框类型
                detailtype = fieldVar.get("changeBrowType");
            const browInfo = this.getBrowserInfo(detailtype, fieldObj.get("fielddbtype"));
            let specialobj  = fieldValueObj && fieldValueObj.has("specialobj") ? fieldValueObj.get("specialobj").toJS():[];
            const linkUrl = browInfo && browInfo.get('linkUrl');
            const browserProp  = browInfo ? browInfo.toJS() : {};
            browserProp.type = detailtype;
            browserProp.replaceDatas = specialobj;
            this.setBrowserProps(browserProp,detailtype,isDetail);
            this.getBrowserParams(fieldObj,browserProp,fieldid,isDetail);
            this.getAddDocRight(baseProps,browserProp);
            let inputStyle = {};
            if(viewAttr !== 1){
                //inputStyle["margin-right"] = "12px";
                if(symbol.indexOf("mc_") > -1)
                    inputStyle["min-width"] = "150px";
            }
            browserProp.inputStyle = inputStyle;
            browserProp.layout = isDetail ? jQuery('#oTable'+(parseInt(symbol.substr(7)) -1)).parent()[0] : jQuery('.wea-new-top-req-content')[0];
           	if(detailtype !== 34) browserProp.resize= (viewAttr !== 1);//请假类型
            let el;
            let format = cellObj.has("format") ? cellObj.get("format").toJS():{};
            if(detailtype === 2){
            	baseProps.formatPattern = format["formatPattern"]||"";
            	baseProps.noInput  = true;
                el = <WeaDatePicker {...baseProps} layout={jQuery('.wea-popover-hrm-relative-parent')[0]} onChange={this.doChangeEvent.bind(this)} />
            }else if(detailtype === 19){
                baseProps.value = formUtil.formatTimeValue(baseProps.value);
            	baseProps.formatPattern = format["formatPattern"]||"";
            	baseProps.noInput  = true;
                el = <WeaTimePicker {...baseProps} layout={jQuery('.wea-popover-hrm-relative-parent')[0]} onChange={this.doChangeEvent.bind(this)} />
            }else if(detailtype === 118 || detailtype === 124 || detailtype === 125 || detailtype === 126){
                //只读，特殊字段处理： 空会议室使用情况、计划、目标、报告
                let valueSpan = '';
                specialobj.map(o=>{
                    if(linkUrl){
                        valueSpan = valueSpan + `<a href='${linkUrl}${o.id}' target='_new'>${o.name||o.lastname}</a> `
                    }else{
                        valueSpan = valueSpan + o.name + ' ';
                    }
                });
                if(detailtype === 118){
                    valueSpan =  <a href='/meeting/report/MeetingRoomPlan.jsp' target='blank'>{specialobj[0].name}</a>
                }
                el =  (
                    <div>
                        <span dangerouslySetInnerHTML={{__html: valueSpan}}></span>
                        <input type='hidden' name={fieldName} id={fieldName} value={fieldValue}/>
                    </div>
                )
            }else{
                el = <WeaBrowser fieldName={fieldName}
                        {...objectAssign({},browserProp,baseProps)}
                        selectSize='default'
                        onChange={this.doBrowChangeEvent.bind(this)}
                        addOnClick={this.clickAddBtn.bind(this)}
                         />
            }
            return el;
        }else if(htmltype === 4){
            if(fieldValue === "")     fieldValue = "0";
            return <WeaCheckbox {...baseProps} value={fieldValue} onChange={this.doChangeEvent.bind(this)} />
        }else if(htmltype === 5){
            const optionRange = fieldVar && fieldVar.has("optionRange") ? fieldVar.get("optionRange") : null;
            const filterOptionRange = fieldVar && fieldVar.has("filterOptionRange") ? fieldVar.get("filterOptionRange") : null;
            let options = [];
            if(detailtype === 0 || detailtype === 1)    //下拉选择框才需要空选项
                options.push({ 'key': '', 'showname': '' });
            const itemList = fieldObj.getIn(['selectattr', 'selectitemlist']);
            itemList && itemList.map(item => {
                const selectvalue = item.get('selectvalue').toString();		//必须转字符串
                if(item.get("cancel") === 1 && (viewAttr === 2 || viewAttr === 3))
                    return true;
                if(optionRange !== null && !optionRange.includes(selectvalue))	//选择框联动控制范围
                    return true;
                if(filterOptionRange !== null && filterOptionRange.includes(selectvalue))   //接口控制remove选项
                    return true;
                options.push({ 'key': selectvalue, 'showname': item.get('selectname') });
            });
            const fieldshowtypes = fieldObj.getIn(['selectattr','fieldshowtypes']);

            baseProps.options = options;
            baseProps.fieldshowtypes = fieldshowtypes;
            //baseProps.widthMatchOptions = true;
            baseProps.dropdownMatchSelectWidth  = false;
            if(viewAttr !== 1)
                baseProps.style = {...baseProps.style, ...{'min-width':`${isDetail ? 55 : 100}px`}};
            baseProps.layout = jQuery('.wea-popover-hrm-relative-parent')[0];

            return(
                <WeaSelect  {...baseProps} onChange={this.doChangeEvent.bind(this)}/>
            )
        }else if(htmltype === 6){
            const specialobj = fieldValueObj && fieldValueObj.get("specialobj");
            const filedatas = specialobj && specialobj.get("filedatas");
            const fileattr = fieldObj && fieldObj.get('fileattr').toJS();
            const datas  = specialobj && specialobj.get('filedatas').toJS();
            const showBatchLoad = specialobj && specialobj.get('showBatchLoad');
            //计算目录
            let category = "";
            let errorMsg = "";
            let maxUploadSize = fileattr.maxUploadSize;
            //catelogType = 0:固定目录 1：选择目录
            let {catelogType,docCategory,selectedCateLog}  = fileattr;
            if(viewAttr > 1){
            	if(catelogType === 0){
            		if(trim(docCategory).length > 0){
            			category = trim(docCategory);
            		}else{
            			errorMsg = "附件上传目录未设置!";
            		}
            	}else{
            	    const selectedCatalog = this.getFileSelectedCatalog();
            	    if(selectedCatalog.errorMsg){
            	    	errorMsg = selectedCatalog.errorMsg;
            	    }else{
            	    	category = selectedCatalog && selectedCatalog.docCategory;
            	    	maxUploadSize = selectedCatalog && selectedCatalog.maxUploadSize;
            	    }
            	}
            }else{
            	category = "-1";
            }
            const workflowid  = params.get("workflowid");
            const authSignatureInfo  = `&authStr=${params.get('authStr')||''}&authSignatureStr=${params.get('authSignatureStr')||''}`;
            const belUserInfo = `&f_weaver_belongto_userid=${params.get("f_weaver_belongto_userid")}&f_weaver_belongto_usertype=${params.get("f_weaver_belongto_usertype")}`;
            let uploadUrl = `/api/workflow/reqform/docUpload?category=${category}&workflowid=${workflowid}&listType=${detailtype == "2" ? "img":"list"}${authSignatureInfo}${belUserInfo}`;
            //计算附件是上传目录

            baseProps.showBatchLoad = isDetail ? false:showBatchLoad;
            baseProps.showClearAll = !isDetail;
            baseProps.uploadUrl = uploadUrl;
            baseProps.maxUploadSize = maxUploadSize;
            baseProps.category = category;
            baseProps.datas = datas;
            baseProps.btnSize = isDetail ? "small":"default";
            baseProps.autoUpload = isDetail;
            baseProps.listType = detailtype === 2 ? "img":"list";
            baseProps.uploadId = fieldName;
            baseProps.errorMsg = errorMsg;
            baseProps.display = fieldVar && fieldVar.get("fileVisible") === true ? true : false;

            if(showBatchLoad && !isDetail){
				const requestid  = params.get("requestid");
            	let batchLoadUrl  = `/weaver/weaver.file.FileDownload?fieldvalue=${fieldValue}&download=1&downloadBatch=1&desrequestid=0&requestid=${requestid}${authSignatureInfo}${belUserInfo}`;
            	baseProps.batchLoadUrl = batchLoadUrl;
            }
            //console.log("baseProps",baseProps,"fileattr",fileattr);
            return(
            	<WeaUpload ref={fieldName} uploadId={fieldName} {...objectAssign({},fileattr,baseProps)} onChange={this.fileChangeEvent.bind(this)} onUploading={this.onUploading.bind(this)}/>
            )
        }else if(htmltype === 7){
            let specialFieldDom;
            if(detailtype === 1){
                // const displayname = fieldObj&&fieldObj.get("specialattr") ? fieldObj.getIn(["specialattr","displayname"]) : "";
                // const linkaddress = fieldObj&&fieldObj.get("specialattr") ? fieldObj.getIn(["specialattr","linkaddress"]) : "";
                // specialFieldDom = <a href={linkaddress} target="_blank" dangerouslySetInnerHTML={{__html: displayname}}></a>
            }else if(detailtype === 0 || detailtype === 2){   //老表单有type为0的描述性文字
                const descriptivetext = fieldObj&&fieldObj.get("specialattr") ? fieldObj.getIn(["specialattr","descriptivetext"]) : "";
                specialFieldDom = <span dangerouslySetInnerHTML={{__html:descriptivetext}}></span>;
            }
            return <span className="specialField" ref="specialFieldSpan">{specialFieldDom}</span>;
        }else if(htmltype === 8){

        }else if(htmltype === 9){

        }
        return <span>No Support</span>
    }
    //获取流程编号字段相关字段信息
    getCodeRelateFieldValues(relateCodeFields){
        const {dependValueObj} = this.props;
        const relateFieldValues = {};
        relateCodeFields && relateCodeFields.map(field =>{
            const relateFieldid = `field${field.split("_")[0]}`;
            const relateFieldValue = dependValueObj && dependValueObj.getIn([relateFieldid, "value"]);
            relateFieldValues[relateFieldid] = relateFieldValue;
        });
        return relateFieldValues;
    }
    //获取附件字段对应的选择框目录信息
    getFileSelectedCatalog(){
        const {symbol,conf,cellObj,dependValueObj} = this.props;
        const tableMark = symbol.indexOf("detail_") > -1 ? symbol : "main";
        const fileFieldid = cellObj.get("field").toString();
        const fileattr = conf.getIn(["tableInfo", tableMark, "fieldinfomap", fileFieldid, "fileattr"]);
        if(!fileattr || fileattr.get("catelogType") !== 1 || !fileattr.has("selectedCateLog"))      //附件字段非选择框控制目录
            return {};
        const selectedFieldid = fileattr.get("selectedCateLog");
        const selectedFieldValue = dependValueObj && dependValueObj.getIn([`field${selectedFieldid}`, "value"]);
        const selectedFieldLabel = conf.getIn(["tableInfo", "main", "fieldinfomap",selectedFieldid,"fieldlabel"]);
        let selectedCatalog = {};
        if(selectedFieldid !== "" && selectedFieldValue != null && selectedFieldValue !==""){
            conf.getIn(["tableInfo", "main", "fieldinfomap", selectedFieldid, "selectattr", "selectitemlist"]).map(v =>{
                if(v && v.get("selectvalue").toString() === selectedFieldValue.toString()){
                    selectedCatalog = {docCategory:v.get("docCategory"), maxUploadSize:v.get("maxUploadSize")||5};
                }
            });
        }else{
        	selectedCatalog.errorMsg = '请先选择"'+selectedFieldLabel+'"';
        }
        return selectedCatalog;
    }
    //浏览数据定义获取表单字段数据
    getBrowserDataDefinition(relateFields,detailtype){
    	const {dependValueObj,conf} = this.props;
    	let relateFieldValue = {};
    	if(relateFields){
    		const currenttime = new Date().getTime();
    		if(detailtype === 161 || detailtype === 162){
    			relateFieldValue.currenttime = currenttime;
    		}
    		relateFields.map(field=>{
    			const fieldid = field.split("_")[0];
    			const dependFieldValue = dependValueObj && dependValueObj.getIn([`field${fieldid}`, "value"]);
    			if(detailtype === 161 || detailtype === 162){
    				const isDetail = field.split("_")[1] === "1";
    				const tableMark = isDetail ? symbol : "main";
    				let key = conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid, "fieldname"]);
    				if(key){
    					if(isDetail)  key = conf.getIn(["tableInfo", tableMark, "tablename"])+"_"+key;
    				}
    				relateFieldValue[key.toLocaleLowerCase()+"_"+currenttime] = dependFieldValue || "";
    			}else{
    				relateFieldValue[`field${fieldid}`] = dependFieldValue || "";	
    			}
    		});
    	}
    	return relateFieldValue;
    }

    //获取浏览框相关的表单字段值
    getBrowserInfo(detailtype, fielddbtype){
        const {symbol,conf,dependValueObj} = this.props;
        const isCusBrow = (detailtype === 161 || detailtype ===162);
        const browKey = (isCusBrow || detailtype === 256 || detailtype === 257) ? detailtype +"_"+fielddbtype : detailtype.toString();
        let browInfo = conf.getIn(["browserInfo", browKey]);
        let relateFieldValue = {};
        browInfo && browInfo.get("relateFieldid") && browInfo.get("relateFieldid").map(field =>{
            const fieldid = field.split("_")[0];
            const isDetail = field.split("_")[1] === "1";
            const tableMark = isDetail ? symbol : "main";
            let key = conf.getIn(["tableInfo", tableMark, "fieldinfomap", fieldid, "fieldname"]);
            if(key){
                if(isDetail)
                    key = conf.getIn(["tableInfo", tableMark, "tablename"])+"_"+key;
                const dependFieldValue = dependValueObj ? dependValueObj.getIn([`field${fieldid}`, "value"]):"";
                //避免参数重复
                if(isCusBrow){
                	relateFieldValue[key.toLocaleLowerCase()+"_"+browInfo.getIn(['dataParams','currenttime'])] = dependFieldValue||"";
                }else{
                	relateFieldValue[key] = dependFieldValue||"";
                }
            }
        });
        //console.log("browInfo",browInfo.toJS(), "relateFieldValue",relateFieldValue);
        if(browInfo){
            browInfo = browInfo.update("dataParams", val=>{
                return val && val.merge(relateFieldValue);
            })
        }
        return browInfo;
    }
    //设置文档浏览框添加按钮及公文浏览框条件
    getAddDocRight(baseProps,browserProp){
    	const {isDetail,fieldid,value='',detailtype,viewAttr} = baseProps;
    	const {params} = this.props;
    	const isWorkflowDoc = params.get("isWorkflowDoc")||false;
    	const flowDocFieldId = params.get("flowDocFieldId")||0;
    	if((viewAttr === 1)) return;
    	//判断是否有正文添加按钮
    	if(!isDetail && isWorkflowDoc && flowDocFieldId == fieldid && detailtype === 9) {
    		if(trim(value).length === 0){
    			browserProp.hasAddBtn = true;
    		}
    		browserProp.dataParams.isWord  = "1";
    	}
    	if(detailtype == 37) {
    		browserProp.hasAddBtn = true;
    	}
    }
    
    //设置浏览框表单参数
    getBrowserParams(fieldObj,browserProp,fieldid,isDetail){
    	const {params,fieldVar} = this.props;
      	const relateFieldid = fieldObj && fieldObj.getIn(["browserattr","relateFieldid"]);
      	const sqlWhereField = fieldObj && fieldObj.getIn(["browserattr","sqlWhereField"]);
      	const browserParams = fieldObj && fieldObj.getIn(["browserattr","browserParams"]).toJS();
      	let tempConditionDataParams = browserProp.conditionDataParams||{};
      	let relateFieldDatas = relateFieldid ? this.getBrowserDataDefinition(relateFieldid,browserProp.type) :{};
      	let sqlWhereFieldDatas = sqlWhereField ? this.getBrowserDataDefinition(sqlWhereField,browserProp.type) :{};
      	let completeParams = browserProp.completeParams||{}
        let dataParams = browserProp.dataParams||{};
        let customDataParam = fieldVar.has("customDataParam") ? fieldVar.get("customDataParam").toJS() : {};
      	
      	const reqinfo = {
            wfid: params.get("workflowid"),
            billid: params.get("formid"),
            isbill: params.get("isbill"),
            requestid: params.get("requestid"),
            f_weaver_belongto_userid: params.get("f_weaver_belongto_userid"),
            f_weaver_belongto_usertype: params.get("f_weaver_belongto_usertype"),
            fieldid: fieldid,
            viewtype: isDetail ? "1" : "0"
        };
        browserProp.conditionDataParams = objectAssign({},tempConditionDataParams,reqinfo,relateFieldDatas,browserParams);
        browserProp.completeParams = objectAssign({},(browserProp.type === 161 || browserProp.type == 162) ? dataParams : relateFieldDatas,reqinfo,completeParams,browserParams,sqlWhereFieldDatas,customDataParam);
        browserProp.dataParams = objectAssign({},reqinfo,dataParams,browserParams,sqlWhereFieldDatas,customDataParam);
        
        if(browserProp.type == 16 || browserProp.type == 152  || 171 == browserProp.type){
        	let linkUrl  = browserProp.linkUrl;
        	linkUrl += "&fieldid="+fieldid+"&requestid=";
        	browserProp.linkUrl = linkUrl;
        }
        //前端自定义浏览框属性覆盖
        let customBrowserProp = fieldVar.has("customBrowserProp") ? fieldVar.get("customBrowserProp").toJS() : {};
        for(let key in customBrowserProp){
            browserProp[key] = customBrowserProp[key];
        }
    }
    
    setBrowserProps(browserProp,detailtype,isDetail){
        if(detailtype === 17 && !isDetail){
        	browserProp.hasAddGroup = true;
        }
        
        if(detailtype === 1 || detailtype === 17){
        	browserProp.isShowGroup  = !isDetail;
        }
    }

}

export default FieldContent