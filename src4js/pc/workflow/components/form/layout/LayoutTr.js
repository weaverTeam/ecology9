import CellType from './CellType'
import * as parse from '../../../util/parseAttr'
import Immutable from 'immutable'
import {is} from 'immutable'
import {Checkbox} from 'antd'

class LayoutTr extends React.Component {
    shouldComponentUpdate(nextProps) {
        return this.props.symbol !== nextProps.symbol
            || this.props.colNum !== nextProps.colNum
            || this.props.rowid !== nextProps.rowid
            || this.props.rowHeight !== nextProps.rowHeight
            || !is(this.props.rowCusAttr, nextProps.rowCusAttr)
            || !is(this.props.colattrs, nextProps.colattrs)
            || !is(this.props.ecMap, nextProps.ecMap)
            || !is(this.props.mainData, nextProps.mainData)
            || !is(this.props.detailData, nextProps.detailData)
            || !is(this.props.variableArea, nextProps.variableArea)
            || this.props.addColType !== nextProps.addColType
            || !is(this.props.curDetailInfo, nextProps.curDetailInfo)
            || !is(this.props.detailRowData, nextProps.detailRowData)
            || !is(this.props.params, nextProps.params)
            || this.props.conf.size !== nextProps.conf.size;
    }
    render() {
        const {actions,params,symbol,colNum,rowid,rowHeight,rowCusAttr,colattrs,ecMap,layout,conf,mainData,detailData,variableArea} = this.props;
        const {addColType,curDetailInfo,detailRowData} = this.props;      //明细解析相关
        let colid = 0;
        let cellArr = new Array();
        const isVirtualSum = symbol.indexOf("detail_") > -1 && addColType === 4;   //低级模式，虚拟生成合计行 
        while(colid < colNum) {     //从行第一列正序解析
            const isVirtualSerial = colid === 0 && symbol.indexOf("detail_") > -1 && addColType > 0;     //低级模式，虚拟生成的序号列
            const isRowMergeCell = parse.judgeCellBelRowSpan(symbol,rowid,colid,ecMap);    //是否行合并单元格
            let cellObj = ecMap.get(rowid+","+colid);
            if((isRowMergeCell || !cellObj) && !isVirtualSerial && !isVirtualSum) {
                if(!isRowMergeCell){    //行合并单元格不生成td
                    const colCusAttr = colattrs ? colattrs.get("col_"+colid) : null;
                    const tdCellAttr = parse.getCellAttr(null, colCusAttr, rowHeight);
                    cellArr.push(<td className={tdCellAttr.tdClass} style={tdCellAttr.tdStyle}></td>);
                }
                colid += 1;
            } else {
                let tdClassName = `${symbol === "main" ? "mainTd" : symbol}_${rowid}_${colid}`;
                //低级模式扩展序号列
                if(isVirtualSerial){
                    if(isRowMergeCell || !cellObj)    //扩展序号单元格为空时虚拟个CellObj对象
                        cellObj = Immutable.fromJS({}).set("id",`${rowid},0`).set("etype", "1");
                    if(addColType === 1){
                        cellArr.push(<td className={tdClassName}></td>);
                    }else if(addColType === 2){     //全选，模拟高级模式etype=20
                        cellArr.push(<td className={tdClassName}>
                                        <CellType
                                            actions={actions}
                                            params={params}
                                            symbol={symbol}
                                            cellObj={cellObj.set("etype", "20")}
                                            conf={conf}
                                            curDetailInfo={curDetailInfo} />
                                        <span>序号</span>
                                    </td>);
                    }else if(addColType === 3){     //单选，模拟高级模式etype=21
                        cellArr.push(<td className={tdClassName}>
                                        <CellType
                                            actions={actions}
                                            params={params}
                                            symbol={symbol}
                                            cellObj={cellObj.set("etype", "21")}
                                            conf={conf}
                                            detailRowData={detailRowData} />
                                        <span className="detailSerial">{detailRowData.get("serialNum")}</span>
                                    </td>);
                    }else if(addColType === 4){     //合计行
                        cellArr.push(<td className={tdClassName}><span>合计</span></td>);
                    }
                    if(isRowMergeCell){
                        colid += 1;
                        continue;
                    }
                }
                if(isVirtualSum)    //取字段内容行模拟生成合计字段
                    cellObj = ecMap.get((rowid+2)+","+colid).set("etype", "19").set("rowspan", "1").set("colspan", "1");
                const rowSpan = cellObj.get("rowspan") || 1;
                const colSpan = cellObj.get("colspan") || 1;
                const colCusAttr = colattrs ? colattrs.get("col_"+colid) : null;
                const tdCellAttr = parse.getCellAttr(cellObj, colCusAttr, rowHeight);
                tdClassName += ` etype_${cellObj.get("etype")}`;
                if(tdCellAttr.tdClass)
                    tdClassName += " "+tdCellAttr.tdClass;
                if(isVirtualSum)
                    tdCellAttr.divStyle["color"] = "#FF0000";
                const cellAttr = parse.buildCellAttr(tdCellAttr, tdCellAttr["cusclass"], tdCellAttr["divStyle"]);
                cellArr.push(<td rowSpan={rowSpan} colSpan={colSpan} className={tdClassName} style={tdCellAttr.tdStyle}>
                                <CellType
                                    actions={actions}
                                    params={params}
                                    symbol={symbol}
                                    cellAttr={cellAttr}
                                    cellObj={cellObj} 
                                    layout={layout} 
                                    conf={conf}
                                    mainData={mainData}
                                    detailData={detailData}
                                    curDetailInfo={curDetailInfo}
                                    detailRowData={detailRowData}
                                    variableArea={variableArea} />
                            </td>);
                colid += parseInt(colSpan);
            }
        }
        const rowAttr = parse.getRowAttr(rowHeight, rowCusAttr);
        let trClass = rowAttr["trClass"] || "";
        if(symbol.indexOf("detail_") === -1 && this.judgeNeedHideRow())
            trClass += " linkage_hide";
        if(symbol.indexOf("detail_") > -1 && detailRowData)      //明细奇偶行加class方便二开重写
            trClass += " " + (detailRowData.get("serialNum")%2 === 0 ? "detail_even_row" : "detail_odd_row");
        return (
            <tr id={rowAttr.id} name={rowAttr.name} className={trClass} style={rowAttr.trStyle}>{cellArr}</tr>
        )
    }
    judgeNeedHideRow(){     //显示属性联动隐藏行
        const {symbol,rowid,conf,variableArea} = this.props;
        let needHideRow = false;
        const mainRowFields = conf.getIn(["cellInfo", "mainRowFields", `${symbol}_row_${rowid}`]);
        mainRowFields && mainRowFields.map(fieldid =>{
            if(variableArea.getIn([`field${fieldid}`, "viewAttr"]) === 5)
                needHideRow = true;
        });
        return needHideRow;
    }
}

export default LayoutTr