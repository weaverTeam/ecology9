import MainLayout from './MainLayout'
import Immutable from 'immutable'
import {is} from 'immutable'

class TabLayout extends React.Component {
    shouldComponentUpdate(nextProps) {
        return !is(this.props.mainData, nextProps.mainData)
                || !is(this.props.detailData, nextProps.detailData)
                || !is(this.props.variableArea, nextProps.variableArea)
                || !is(this.props.tab, nextProps.tab)
                || this.props.cellMark !== nextProps.cellMark
                || !is(this.props.params, nextProps.params)
                || this.props.conf.size !== nextProps.conf.size;
    }
    componentDidMount(){
        const tabBar = jQuery(this.refs.tabBar);
        const tab_head = tabBar.parent();
        const tab_top = tab_head.parent();
        const tab_turnleft = tab_top.find(".tab_turnleft");
        const tab_turnright = tab_top.find(".tab_turnright");
		let totalWidth = 0;
		tabBar.children().each(function(){
			totalWidth += jQuery(this).width();
		});
		if(totalWidth > tab_top.width()){
            tab_top.find(".tab_movebtn").css("display", "block");
            tab_head.width(tab_top.width() - tab_turnleft.width() - tab_turnright.width());
			tabBar.width(totalWidth+1);
		}else{
			tab_top.find(".tab_movebtn").css("display", "none");
			tab_head.width("100%");
			tabBar.width("100%");
		}
    }
    componentDidUpdate(prevProps, prevState){
        const tabShowSymbol = this.props.variableArea && this.props.variableArea.get(`tabarea_${this.props.cellMark}_showid`) || "";
        const tabShowSymbol_prev = prevProps.variableArea && prevProps.variableArea.get(`tabarea_${prevProps.cellMark}_showid`) || "";
        if(tabShowSymbol !== tabShowSymbol_prev){   //切换了标签显示区域
            formImgLazyLoad(jQuery(`div#${tabShowSymbol}_content`));    //执行图片懒加载
        }
    }
    render() {
        const {actions,params,cellMark,tab,layout,conf,mainData,detailData,variableArea} = this.props;
        const style = conf.getIn(["cellInfo",`main_${cellMark}_stylejson`]);
        if(!style)
            return <div>Empty TabArea Style</div>
        
        let contentStyle = {};
        const areaheight = tab.get("areaheight");
        if(areaheight && areaheight.indexOf("2,") > -1){     //固定高度
            contentStyle["height"] = areaheight.split(",")[1]+"px";
            contentStyle["overflow-y"] = "auto";
        }
        let tabArr = new Array();
        tab.map((v,k)=>{
            if(k.indexOf("order_") === 0 && v && v.indexOf(",") > -1) {
                const arr = v.split(",");
                tabArr.push({
                    id: arr[0],
                    name: arr[1],
                    order: parseInt(k.substring(6))
                });
            }
        });
        tabArr = tabArr.sort((a,b) =>{
            return a.order - b.order;
        });
        let defShowIndex = parseInt(tab.get("defshow") || 0);
        if(defShowIndex >= tabArr.length)
            defShowIndex = 0;
        const tabShowSymbol = variableArea.has(`tabarea_${cellMark}_showid`) ? variableArea.get(`tabarea_${cellMark}_showid`) : tabArr[defShowIndex].id;
        let tabHeadBar = new Array();
        let tabContent = new Array();
        tabArr.map((v,k) =>{
            const tabid = v.id;
            //拼标签头部bar
            const sel = tabid === tabShowSymbol ? "sel" : "unsel";
            let middleStyle = {
                backgroundImage:"url('"+style.get(sel+"_bgmiddle")+"')",
                fontSize:style.get(sel+"_fontsize")+"px",
                color:style.get(sel+"_color"),
                fontFamily:style.get(sel+"_family")
            };
            if(style && style.get(sel+"_bold") == "1")
                middleStyle["font-weight"] = "bold";
            if(style && style.get(sel+"_italic") == "1")
                middleStyle["font-style"] = "italic";
            tabHeadBar.push(
                <div id={tabid} className={"t_"+sel} onClick={this.changeTab.bind(this, tabid)}>
                    <div className={"t_"+sel+"_left norepeat"}
                        style={{
                            backgroundImage:"url('"+style.get(sel+"_bgleft")+"')",
                            width:style.get(sel+"_bgleftwidth")+"px"
                        }}></div>
                    <div className={"t_"+sel+"_middle xrepeat lineheight30"} style={middleStyle}>
                        <span>{v.name}</span>
                    </div>
                    <div className={"t_"+sel+"_right norepeat"}
                        style={{
                            backgroundImage:"url('"+style.get(sel+"_bgright")+"')",
                            width:style.get(sel+"_bgrightwidth")+"px"
                        }}></div>
                </div>
            );
            if(k !== tabArr.length-1) {
                tabHeadBar.push(<div className="t_sep norepeat" style={{width:style.get("image_sepwidth")+"px"}}></div>);
            }
            //拼接标签页内容
            tabContent.push(
                <div className="tab_content" id={tabid+"_content"} style={tabid===tabShowSymbol ? {} : {display:"none"}}>
                    <MainLayout 
                        actions={actions}
                        params={params}
                        symbol={tabid}
                        layout={layout}
                        conf={conf}
                        mainData={mainData}
                        detailData={detailData}  
                        variableArea={variableArea} />
                </div>
            );
        });
        return (
            <div className={`tab_area tabarea_${cellMark}`}>
                <div className="tab_top">
                    <div className="tab_movebtn tab_turnleft" style={{display:"none"}} onClick={this.turnBar.bind(this, "left")}></div>
                    <div className="tab_head" style={{width:"100%"}}>
                        <div className="t_area xrepeat" ref="tabBar"
                            style={{
                                backgroundImage:"url("+style.get("image_bg")+")",
                                width:"100%"
                            }}>
                            {tabHeadBar}
                        </div>
                    </div>
                    <div className="tab_movebtn tab_turnright" style={{display:"none"}} onClick={this.turnBar.bind(this, "right")}></div>
                </div>
                <div className="tab_bottom" style={contentStyle}>
                    {tabContent}
                </div>
            </div>
        )
    }
    changeTab(selectedTabId) {
        const {actions,cellMark} = this.props;
        actions.controlVariableArea({[`tabarea_${cellMark}_showid`]:selectedTabId});
    }
    turnBar(flag){
        const tab_head = jQuery(this.refs.tabBar).parent();
        if(flag === "left")
            tab_head.scrollLeft(tab_head.scrollLeft() - 80);
        else if(flag === "right")
            tab_head.scrollLeft(tab_head.scrollLeft() + 80);
    }
}

export default TabLayout