import LayoutTr from './LayoutTr'
import Immutable from 'immutable'
import {is} from 'immutable'
import {judgeDetailContainSum,getBrowserVersion} from '../../../util/formUtil'
import {judgeDetailContainColHide,convertRowHeads,convertColHeads,parseLayoutImage} from '../../../util/parseAttr'

class DetailLayout extends React.Component {
    shouldComponentUpdate(nextProps) {
        return this.props.symbol !== nextProps.symbol
            || !is(this.props.curDetailInfo, nextProps.curDetailInfo)
            || !is(this.props.variableArea, nextProps.variableArea)
            || !is(this.props.mainData, nextProps.mainData)
            || this.props.conf.size !== nextProps.conf.size
            || !is(this.props.params, nextProps.params)
            || !is(this.props.detailLayout, nextProps.detailLayout);
    }
    componentDidUpdate(prevProps, prevState){
        const oldDetailInfo = prevProps.curDetailInfo;
        const newDetailInfo = this.props.curDetailInfo;
        if(prevProps.symbol !== this.props.symbol || is(oldDetailInfo, newDetailInfo))
            return;
        const groupid = parseInt(this.props.symbol.substring(7)) - 1;
        const oldIndexNum = oldDetailInfo ? parseInt(oldDetailInfo.get("indexnum")) : 0;
        const newIndexNum = newDetailInfo ? parseInt(newDetailInfo.get("indexnum")) : 0;
        if(newDetailInfo.get("fromAddRow") === true && newIndexNum > oldIndexNum){  //此次渲染存在新增明细
            let addIndexStr = "";
            for(let i=oldIndexNum; i<newIndexNum; i++){
                addIndexStr += ","+i;
            }
            if(addIndexStr !== "")
                addIndexStr = addIndexStr.substring(1);
            try{		//DOM结构出来后执行自定义新增行接口，必须try-catch
                eval("_customAddFun"+groupid+"("+addIndexStr+")");
            }catch(e){}
        }
        let existDelDetail = false;
        oldDetailInfo && oldDetailInfo.get("rowDatas").map((v,k) =>{
            if(!newDetailInfo.hasIn(["rowDatas", k]))
                existDelDetail = true;
        });
        if(existDelDetail){     //此次渲染存在删除明细
            try{		//界面删除成功后执行自定义删除行接口，必须try-catch
                eval("_customDelFun"+groupid+"()");
            }catch(e){}
        }
    }
    render() {
        const {actions,params,symbol,conf,detailLayout,mainData,curDetailInfo,variableArea} = this.props;
        const detailIndex = parseInt(symbol.substring(7));
        const headRow = parseInt(detailLayout && detailLayout.get("edtitleinrow") || 0);
        const tailRow = parseInt(detailLayout && detailLayout.get("edtailinrow") || 0);
        if(headRow == 0 || tailRow == 0 || tailRow <= headRow) {
            return <div>对不起，表头标识、表尾标识设置异常，无法渲染明细！</div>
        }
        const isSeniorSet = detailLayout.get("seniorset") === "1";
        const allowXScroll = conf.getIn(["tableInfo", symbol, "detailtableattr", "allowscroll"]) === 1;
        let divStyle = allowXScroll ? {"overflow-x": getBrowserVersion() === "IE9" ? "scroll" : "auto"} : {};
        let tableStyle = judgeDetailContainColHide(detailLayout.get("colattrs")) ? {"table-layout":"fixed"} : {};
        const rowHeads = convertRowHeads(detailLayout.get("rowheads"));
        const colHeads = convertColHeads(detailLayout.get("colheads"), symbol, tableStyle, allowXScroll, isSeniorSet);
        const rowattrs = detailLayout.get("rowattrs");
        const colattrs = detailLayout.get("colattrs");
        const ec = detailLayout.get("ec");
        const ecMap = Immutable.Map(ec.map((v,k) => {
            return [v.get('id'), v]
        }));

        let addColType = 0;
        let detailArr = [];
        const indexnum = curDetailInfo ? parseInt(curDetailInfo.get("indexnum")) : 0;
        indexnum > 0 && curDetailInfo.get("rowDatas").map((v,k) =>{
            let rowData = v;
            rowData = rowData.set("rowIndex", parseInt(k.substring(4)));
            detailArr.push(rowData);
        });
        detailArr = detailArr.sort((a,b)=>{
            return parseInt(a.get("orderid"))-parseInt(b.get("orderid"));
        });
        let trArr = new Array();
        parseLayoutImage(detailLayout, tableStyle, trArr);    //解析背景图、悬浮图
        rowHeads.map((o)=>{
            const rowid = parseInt(o.id);
            const rowHeight = o.height;
            const rowCusAttr = rowattrs ? rowattrs.get("row_"+rowid) : null;
            if(rowid === headRow+1) {    //明细数据行
                if(!isSeniorSet)
                    addColType = 3;
                detailArr.map((rowData,index)=>{
                    rowData = rowData.set("serialNum",index+1);
                    for(let i=rowid; i<tailRow; i++) {  //高级模式可多行
                        trArr.push(<LayoutTr 
                            actions={actions}
                            params={params}
                            symbol={symbol}
                            colNum={colHeads.length}
                            rowid={i}
                            rowHeight={rowHeight}
                            rowCusAttr={rowCusAttr}
                            colattrs={colattrs}
                            ecMap={ecMap}
                            conf={conf}
                            mainData={mainData}
                            variableArea={variableArea}
                            addColType={addColType}
                            detailRowData={rowData}
                        />);
                    }
                });
            }else if(rowid < headRow || (isSeniorSet && rowid > tailRow)){  //头尾非数据行
                if(!isSeniorSet)
                    addColType = (rowid === headRow-1) ? 2 : 1;
                trArr.push(<LayoutTr 
                    actions={actions}
                    params={params}
                    symbol={symbol}
                    colNum={colHeads.length}
                    rowid={rowid} 
                    rowHeight={rowHeight}
                    rowCusAttr={rowCusAttr}
                    colattrs={colattrs} 
                    ecMap={ecMap} 
                    conf={conf} 
                    mainData={mainData}
                    variableArea={variableArea}
                    addColType={addColType}
                    curDetailInfo={curDetailInfo}
                />);
            }
        });
        if(!isSeniorSet && judgeDetailContainSum(conf,symbol)){     //扩充添加合计行
            trArr.push(<LayoutTr 
                actions={actions}
                params={params}
                symbol={symbol}
                colNum={colHeads.length}
                rowid={headRow-1}   //合计行样式取字段标签所在行
                rowHeight={30}
                colattrs={colattrs}
                ecMap={ecMap}
                conf={conf}
                variableArea={variableArea}
                addColType={4}
            />);
        }
        return (
            <div className="excelDetailTableDiv" style={divStyle} data-allowxscroll={allowXScroll} >
                <table className="excelDetailTable" id={`oTable${detailIndex-1}`} name={`oTable${detailIndex-1}`} style={tableStyle}>
                    <colgroup>
                    {
                        !isSeniorSet && <col style={{width:allowXScroll ? "60px" : "6%"}} />
                    }
                    {
                        colHeads.map((o)=>{
                            let colStyle = {width:o.width};
                            if(detailLayout.getIn(["colattrs", `col_${o.id}`, "hide"]) === "y")
                                colStyle["display"] = "none";
                            return <col style={colStyle} />;
                        })
                    }
                    </colgroup>
                    <tbody>
                    {trArr}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default DetailLayout