import LayoutTr from './LayoutTr'
import CustomContent from './CustomContent'
import PromptBox from './PromptBox'
import Immutable from 'immutable'
import {is} from 'immutable'
import {judgeExistXScrollDetail,getDetailAllRowIndexString} from '../../../util/formUtil'
import {convertRowHeads,convertColHeads,parseLayoutImage} from '../../../util/parseAttr'
import {updateHiddenSysFieldname} from '../../../util/reqUtil'

class MainLayout extends React.Component {
    shouldComponentUpdate(nextProps) {
        return this.props.symbol !== nextProps.symbol
            || !is(this.props.mainData, nextProps.mainData)
            || !is(this.props.detailData, nextProps.detailData)
            || !is(this.props.variableArea, nextProps.variableArea)
            || this.props.conf.size !== nextProps.conf.size
            || !is(this.props.params, nextProps.params)
            || !is(this.props.submitParams, nextProps.submitParams)
            || !is(this.props.layout, nextProps.layout);
    }
    componentDidMount() {
        //模板列宽超过一屏时，计算div宽度撑开body的横向滚动条
        const layoutWidth = parseFloat(jQuery("table.excelMainTable").width());
        const formDiv = jQuery("div.wea-req-workflow-form");
        if(layoutWidth > formDiv.width()){
            formDiv.width(layoutWidth);
            formDiv.parent().width(layoutWidth);    //不用加边距，默认加上了padding
        }
        //系统提醒工作流，备注信息处理
        if(this.props.params.get("workflowid") == 1){
            sysWfUtil.readyHandle();
        }
    }
    render() {
        const {actions,params,submitParams,symbol,layout,conf,mainData,detailData,variableArea} = this.props;
        const isMain = symbol === "main" ? true : false;
        const etable = layout && layout.hasIn(["etables",isMain?"emaintable":symbol]) ? layout.getIn(["etables",isMain?"emaintable":symbol]) : null;
        if(etable == null)
            return <div style={{"display": "none"}}>Layout error</div>

        let className = isMain ? "excelMainTable" : "excelTabTable";
        let tableStyle = isMain? {margin:"0px auto"} : {};
        if(symbol.indexOf("detail") === -1 && judgeExistXScrollDetail(conf))
            tableStyle["table-layout"] = "fixed";
        const rowHeads = convertRowHeads(etable.get("rowheads"));
        const colHeads = convertColHeads(etable.get("colheads"), symbol, tableStyle);
        const rowattrs = etable.get("rowattrs");
        const colattrs = etable.get("colattrs");
        const ec = etable.get("ec");
        const ecMap = ec ? Immutable.Map(ec.map(v => [v.get('id'), v])) : null;
        let trArr = new Array();
        parseLayoutImage(etable, tableStyle, trArr);    //解析背景图、悬浮图
        rowHeads.map((o)=>{
            const rowid = o.id;
            const rowCusAttr = rowattrs ? rowattrs.get("row_"+rowid) : null;
            trArr.push(<LayoutTr 
                actions={actions}
                params={params}
                symbol={symbol}
                colNum={colHeads.length} 
                rowid={rowid} 
                rowHeight={o.height}
                rowCusAttr={rowCusAttr}
                colattrs={colattrs}
                ecMap={ecMap}
                layout={layout}
                conf={conf}
                mainData={mainData}
                detailData={detailData}
                variableArea={variableArea}
            />);
        });

        let formHiddenArea = [];
        if(isMain){
            submitParams && submitParams.mapEntries(o =>{
                formHiddenArea.push(<input type="hidden" id={o[0]} name={o[0]} value={o[1]}/>)
            });
            //字段不在模板上生成隐藏域
            const fieldCellInfo = conf.getIn(["cellInfo", "fieldCellInfo"]);
            conf.get("tableInfo").map((tableInfo,tableMark) =>{
                const isDetail = tableMark.indexOf("detail_") > -1;
                const rowIndexStr = isDetail ? getDetailAllRowIndexString(detailData, tableMark) : "-1";
                tableInfo.get("fieldinfomap").map((v,fieldid) =>{
                    if(fieldCellInfo.has(fieldid))     //字段存在模板上，组件内生成隐藏域
                        return "";
                    !!rowIndexStr && rowIndexStr.split(",").map(rowIndex =>{
                        let domfieldid = "";
                        let domfieldvalue = "";
                        if(isDetail){
                            domfieldid = `field${fieldid}_${rowIndex}`;
                            if(detailData && detailData.hasIn([tableMark, "rowDatas", `row_${rowIndex}`, `field${fieldid}`, "value"]))
                                domfieldvalue = detailData.getIn([tableMark, "rowDatas", `row_${rowIndex}`, `field${fieldid}`, "value"]);
                        }else{
                            domfieldid = `field${fieldid}`
                            if(mainData && mainData.hasIn([domfieldid, "value"]))
                                domfieldvalue = mainData.getIn([domfieldid, "value"]).toString();
                            domfieldid = updateHiddenSysFieldname(domfieldid);  //转换系统字段
                        }
                        formHiddenArea.push(<input type="hidden" id={domfieldid} name={domfieldid} value={domfieldvalue} />);
                    });
                })
            });
            //明细相关隐藏域
            detailData && detailData.map((v,k) => {
                const detailIndex = parseInt(k.substring(7))-1;
                let submitdtlid = "";
                v.get("rowDatas").map((datas, rowKey) => {
                    const rowIndex = parseInt(rowKey.substring(4));
                    submitdtlid += rowIndex+",";
                    if(datas.has("keyid"))
                        formHiddenArea.push(<input type="hidden" name={`dtl_id_${detailIndex}_${rowIndex}`} value={datas.get("keyid")} />);
                });
                formHiddenArea.push(<input type="hidden" id={`nodesnum${detailIndex}`} name={`nodesnum${detailIndex}`} value={v.get("rowDatas").size} />);
                formHiddenArea.push(<input type="hidden" id={`indexnum${detailIndex}`} name={`indexnum${detailIndex}`} value={v.get("indexnum")} />);
                formHiddenArea.push(<input type="hidden" id={`submitdtlid${detailIndex}`} name={`submitdtlid${detailIndex}`} value={submitdtlid} />);
                formHiddenArea.push(<input type="hidden" id={`deldtlid${detailIndex}`} name={`deldtlid${detailIndex}`} value={v.get("deldtlid")||""} />);
            });
        }
        const promptBoxProp = variableArea && variableArea.has("promptBox") ? variableArea.get("promptBox").toJS() : {};
        return (
            <div className={isMain ? "excelTempDiv" : ""}>
                <table className={className} style={tableStyle}>
                    <colgroup>
                    {
                        colHeads.map((o)=><col style={{width: o.width}} />)
                    }
                    </colgroup>
                    <tbody>
                    {trArr}
                    </tbody>
                </table>
                {isMain && <CustomContent />}
                {isMain && <div class="hiddenArea" style={{display:"none"}}>
                    {formHiddenArea}
                </div>}
                {isMain && <PromptBox {...promptBoxProp} />}
            </div>
        )
    }
}

export default MainLayout