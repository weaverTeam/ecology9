import '../../../css/promptBox.less'
import {Icon,message} from 'antd'

class PromptBox extends React.Component{
    constructor(props) {
		super(props);
		this.state = {
            visible: false,
			top: 0,
			left: 0
		};
	}
    componentWillReceiveProps(nextProps) {
        if(!nextProps.visible || !nextProps.mark){  //直接隐藏即可
            this.setState({visible:false});
            return;
        }
        if(this.props.mark !== nextProps.mark || this.props.promptType !== nextProps.promptType){
            const promptType = nextProps.promptType;
            const mark = nextProps.mark;
            let locateObj;
            if(promptType === 1 || promptType ===2)
                locateObj = jQuery("[data-fieldmark='"+mark+"']");
            else if(promptType === 3)
                locateObj = jQuery("table#oTable"+(parseInt(mark.substring(7))-1));
            const existLocateObj = locateObj.size() > 0;
            if(existLocateObj && locateObj.is(":visible")){     //定位元素可见
                const totalOffsetTop = locateObj.offset().top + jQuery(".wea-new-top-req-content").scrollTop();
                const promptOffsetTop = locateObj.offset().top + locateObj.height() + 38;
                //控制竖向滚动条，将定位DOM放置在可视区域
                if(promptOffsetTop <= (jQuery("div.wea-new-top-req").height()+20) || promptOffsetTop>= jQuery(window).height()){
                    const setOffsetTop = jQuery(window).height()/2 - locateObj.height() - 60;      //设置字段区域底部距离浏览器顶部偏移一半
                    jQuery(".wea-new-top-req-content").scrollTop(totalOffsetTop-setOffsetTop);      //滚动至可视区域
                }
                //控制明细字段横向滚动条，将定位DOM放置在可视区域
                if((promptType === 1 || promptType ===2) && mark.indexOf("_")>-1){
                    const excelDetailTableDiv = locateObj.closest("div.excelDetailTableDiv");
                    if(excelDetailTableDiv.size() > 0 && excelDetailTableDiv.attr("data-allowxscroll") === "true"){    //开启明细横向滚动条
                        const detailDivOffsetLeft = excelDetailTableDiv.offset().left;
                        const relativeOffsetLeft = locateObj.offset().left + excelDetailTableDiv.scrollLeft() - detailDivOffsetLeft;    //相对明细区域的左偏移
                        if(relativeOffsetLeft <= 0 || relativeOffsetLeft >= excelDetailTableDiv.width()){   //不在可见区域
                            excelDetailTableDiv.scrollLeft(relativeOffsetLeft - excelDetailTableDiv.width()/2);
                        }
                    }
                }
                const top = totalOffsetTop + locateObj.height() - jQuery("div.wea-new-top-req").height();
                const left = locateObj.offset().left + jQuery(".wea-new-top-req-content").scrollLeft();
                this.setState({visible:true, top:top, left:left});
            }else{      //定位元素不可见
                let noticeInfo = "";
                if(promptType === 1){
                    noticeInfo = "字段未填值,字段内容被隐藏！";
                }else if(promptType === 3){
                    if(!existLocateObj)
                        noticeInfo = "未新增数据,明细表未放在模板上！";
                    else
                        noticeInfo = "未新增数据,明细区域被隐藏！";
                }
                if(noticeInfo !== ""){
                    message.warning(<span>
                        <span className="prompt-content-label">"{nextProps.lableText}"</span>
                        <span className="prompt-content-info">{noticeInfo}</span>
                    </span>, 2);
                }
                this.setState({visible:false});
            }
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        return this.state.visible !== nextState.visible
            || this.state.top !== nextState.top
            || this.state.left !== nextState.left;
    }
    render() {
        const {lableText,promptType} = this.props;
        const display = this.state.visible === true ? "" : "none";
        let promptContent = "";
        if(promptType === 1)
            promptContent = "未填写";
        else if(promptType === 2)
            promptContent = "为行规则赋值字段，修改无效";
        else if(promptType === 3)
            promptContent = "未新增数据";
        return <div className="prompt-box" ref="promptbox" style={{display:display, top:this.state.top, left:this.state.left}}>
            <div className="prompt-arrow"></div>
            <div className="prompt-content">
                <Icon type="exclamation-circle" />
                <span className="prompt-content-label">"{lableText}"</span>
                <span className="prompt-content-info">{promptContent}</span>
            </div>
        </div>
    }
}

export default PromptBox