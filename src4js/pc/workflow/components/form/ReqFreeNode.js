import Immutable from 'immutable'
const is = Immutable.is;

class ReqFreeNode extends React.Component{
	constructor(props) {
		super(props);
    }
	
	shouldComponentUpdate(nextProps,nextState) {
		return !is(this.props.rightMenu,nextProps.rightMenu);
	}
	
	render(){
		const {rightMenu} = this.props;
		const hasFreeNode = rightMenu && rightMenu.has("freeNode");
		let freeNodeHidearea = [];
		if(hasFreeNode){
			const hasFreeNodeInfo = rightMenu.hasIn(["freeNode","freeNodeInfo"]);
			if(hasFreeNodeInfo){
				const freeNodeInfo = rightMenu.getIn(["freeNode","freeNodeInfo"]);
				freeNodeInfo && freeNodeInfo.map((nodeInfo,key)=>{
					nodeInfo && nodeInfo.map((v,k)=>{
						freeNodeHidearea.push(<input type="hidden" name={k} value={v||""} />)
					})
				});
			}
			const hasFreeNum  = rightMenu.hasIn(["freeNode","rownum"]);
			if(hasFreeNum){
				freeNodeHidearea.push(<input type="hidden" id="rownum" name="rownum" value={rightMenu.getIn(["freeNode","rownum"])||""} />);
				freeNodeHidearea.push(<input type="hidden" id="indexnum" name="indexnum" value={rightMenu.getIn(["freeNode","indexnum"])||""} />);
				freeNodeHidearea.push(<input type="hidden" id="checkfield" name="checkfield" value={rightMenu.getIn(["freeNode","checkfield"])||""} />);
				freeNodeHidearea.push(<input type="hidden" id="freeNode" name="freeNode" value={rightMenu.getIn(["freeNode","freeNode"])||""} />);
				freeNodeHidearea.push(<input type="hidden" id="freeDuty" name="freeDuty" value={rightMenu.getIn(["freeNode","freeDuty"])||""} />);
			}else{
				freeNodeHidearea.push(<input type="hidden" name="freeNode" value="0" />);
			}
		}else{
			freeNodeHidearea.push(<input type="hidden" name="freeNode" value="0" />);
			freeNodeHidearea.push(<input type="hidden" name="freeDuty" value="1" />);
		}
		return (
			<div className="freeNode" style={{"display":"none"}}>
				{freeNodeHidearea}
			</div>
		)
	}
}
export default ReqFreeNode