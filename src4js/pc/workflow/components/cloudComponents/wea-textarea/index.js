import './style/index.css'
import {Input,message} from 'antd'
import trim from 'lodash/trim'

class main extends React.Component {
	static defaultProps={
		detailtype:1,
		style:{},
		viewAttr:2,
	}
	constructor(props) {
		super(props);
		this.state = {
			value: props.value ? props.value : "",
			isonfocus: false
		};
	}

	componentWillReceiveProps(nextProps) {
		if(this.state.value !== nextProps.value) {
			this.setState({
				value: nextProps.value
			});

			const {fieldName,detailtype,viewAttr} = this.props;
			if(detailtype === 2 && viewAttr !== 1){
				const arr  = UE.editors.filter(o=>o==fieldName);
				arr.length > 0 && UE.getEditor(fieldName).setContent(nextProps.value,false);
			}
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		if(this.props.defaultFocus && this.props.defaultFocus !== nextProps.defaultFocus)	//延时失去焦点时不渲染组件
			return false;
		return nextProps.value !== this.props.value ||
			   nextProps.viewAttr !== this.props.viewAttr ||
			   this.state.value !== nextState.value ||
			   this.state.isonfocus !== nextState.isonfocus ||
			   nextProps.defaultFocus !== this.props.defaultFocus ||
			   nextProps.emptyShowContent !== this.props.emptyShowContent;
	}
	
	componentDidUpdate(){
		const {value} = this.state;
		const {defaultFocus,detailtype,viewAttr,fieldName} = this.props;
		if(defaultFocus && viewAttr === 3){
			if(detailtype == 1){
				const input = this.refs.inputEle.refs.input;
				if(input != document.activeElement){
					input.focus();
					const index = trim(value).length;
					input.setSelectionRange(index, index);	//光标定位到最后
				}
			}else{
				const arr  = UE.editors.filter(o=>o==fieldName);
				if(arr.length > 0){
					UE.getEditor(fieldName).focus(true);
				}
			}
		}
		this.adaptHtmlShowDivWidth();
	}

	componentDidMount() {
		const { detailtype, fieldName ,viewAttr} = this.props;
		const that = this;
		if(detailtype === 2 && viewAttr !== 1) {
			let _ue = UEUtil.initEditor(fieldName);
			_ue.addListener('contentChange', function() {
				const html = UE.getEditor(fieldName).getContent();
				that.setState({ value: html });
				that.props.onChange && that.props.onChange(html);
			});
		}
		this.adaptHtmlShowDivWidth();
	}
	adaptHtmlShowDivWidth(){
		//富文本只读显示区域宽度需固定才能出横向滚动条
		if(this.props.detailtype === 2 && this.props.viewAttr === 1){
			const obj = jQuery(this.refs.htmlShowDiv);
			//obj.width(obj.parent().width()).css("display", "block");
			obj.css("display", "block");
		}
	}

	render() {
		let {value,isonfocus} = this.state;
		const { viewAttr, fieldid, detailtype, textheight, length ,fieldName, style} = this.props;
		let {emptyShowContent} = this.props;
		if(viewAttr === 2 || viewAttr === 3){
			style["padding-right"] = "10px";
		}
		let styleInfo = { 'word-break': 'break-all', 'word-wrap': 'break-word' ,'width':'100%'};
		let requiredSpan = null;
		if(viewAttr !== 1){
			const isRequired = viewAttr === 3 && trim(value).length === 0;
			requiredSpan = <span id={`${fieldName}span`} className="wf-textarea-required">
				{isRequired ? <img src="/images/BacoError_wev9.png" align="absmiddle" /> : ""}
			</span>;
		}
		if(detailtype === 2) {
			return (
				<div className="wea-textarea" style={style}>
					<div className="wea-textarea-html-show" ref="htmlShowDiv" style={{overflowX:"auto",overflowY:"hidden",display:"none"}} dangerouslySetInnerHTML={{__html: value}} />
					<div style={{"display":viewAttr !== 1?"block":"none"}}>
						<textarea id={fieldName} name={fieldName} rows={textheight} value={value} style={styleInfo}/>
					</div>
					{requiredSpan}
				</div>
			)
		} else {
			if(viewAttr === 1){
				return <span className="wea-textarea">
							<span style={style} id={fieldName+"span"} dangerouslySetInnerHTML={{__html: value}}></span>
							<input type="hidden" id={fieldName} name={fieldName} value={value} />
					   </span>
			}else{
				//支持可编辑无值时显示灰色默认值
				emptyShowContent = emptyShowContent || "";
				if(!isonfocus && emptyShowContent !== "" && value === ""){
					styleInfo["color"] = "#A5A5A5";
					value = emptyShowContent;
				}else{
					styleInfo["color"] = "";
				}
				return <div className="wea-textarea" style={style}>
							<Input ref="inputEle"  type="textarea"
									autosize={{minRows:textheight}}
									value={value}
									id={fieldName}
									name={fieldName}
									className={`wea-textarea-${fieldid}`}
									style={styleInfo}
									onFocus={this.doFocusEvent.bind(this)}
									onBlur={this.doBlurEvent.bind(this)}
									onKeyDown={this.doKeyDown.bind(this)}
									onChange={this.setTextAreaValue.bind(this)} />
							{requiredSpan}
						</div>
			}
		}
	}
	doFocusEvent(e) {
		this.setState({isonfocus: true});
	}
	doBlurEvent(e) {
		this.setState({isonfocus: false});
		this.props.onChange && this.props.onChange(e.target.value);
	}
	doKeyDown(e){
		e.stopPropagation();	//阻止冒泡，form有keyDowm事件禁止enter键
	}
	setTextAreaValue(e){
		const {fieldlabel} = this.props;
		let value = e.target.value;
		let valLen = window.realLength(value);
		let length = 4000;
		let specialChar = value.match(/\"| /ig);
		if(specialChar)
			length = length - specialChar.length*5;		//后台转译成6个字符
		specialChar = value.match(/&|<|>|\n/ig);
		if(specialChar)		//后台转译成4个字符
			length = length - specialChar.length*3;
		if(length && valLen > length) {
			message.warning(`${fieldlabel}字段值长度不能超过4000(1个中文字符等于3个长度)`);
			value  =  window.checkLength(value, valLen, length);
		}
		this.setState({value: value});
	}
}

export default main