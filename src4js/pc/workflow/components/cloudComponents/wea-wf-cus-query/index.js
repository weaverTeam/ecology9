import {WeaSelect,WeaBrowser,WeaDatePicker,WeaTimePicker} from 'ecCom'
import './index.less'

class Main extends React.Component{
    render(){
        const {customType,domkey,options,browserConditionParam} = this.props;
        console.log("browserConditionParam--", browserConditionParam);
        const {getFieldProps} = this.props.form;
        let domArr = [];
        if(customType === "SELECT_AFTER_BROWSER" || customType === "SELECT_RANGE"){
            domArr.push(<div className="select-area">
                <WeaSelect {...getFieldProps(domkey[0])}
                    initialValue = {this.getSelectDefaultValue()}
                    options = {options}
                />
            </div>);
        }
        if(customType === "SELECT_AFTER_BROWSER"){
            const  showDropMenu = browserConditionParam.type == '4' || browserConditionParam.type == '164'; //部门分部显示维度菜单
            domArr.push(<div className="browser-area">
                <WeaBrowser {...getFieldProps(domkey[1])}
                    {...browserConditionParam}
                    showDropMenu = {showDropMenu} layout = {document.body}
                />
            </div>);
        }else if(customType === "DATE_TO_DATE"){
            domArr.push(<span className="form-span">从</span>);
            domArr.push(<span><WeaDatePicker {...getFieldProps(domkey[0])} noInput={true} /></span>);
            domArr.push(<span className="to-span">到</span>);
            domArr.push(<span><WeaDatePicker {...getFieldProps(domkey[1])} noInput={true} /></span>);
        }else if(customType === "TIME_TO_TIME"){
            domArr.push(<span className="form-span">从</span>);
            domArr.push(<span><WeaTimePicker {...getFieldProps(domkey[0])} noInput={true} /></span>);
            domArr.push(<span className="to-span">到</span>);
            domArr.push(<span><WeaTimePicker {...getFieldProps(domkey[1])} noInput={true} /></span>);
        }else if(customType === "SELECT_RANGE"){

        }
        return <div className="cus-query-condition">{domArr}</div>;
    }
    getSelectDefaultValue(){
        const {options} = this.props;
        let value = "";
        options && options.map(v =>{
            if(v.selected && value === "")
                value = v.key;
        });
        return value;
    }
}

export default Main;