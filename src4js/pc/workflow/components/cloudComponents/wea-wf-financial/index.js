import './index.less'

class main extends React.Component {
    shouldComponentUpdate(nextProps,nextState) {
        return this.props.type !== nextProps.type
            || this.props.className !== nextProps.className
            || this.props.viewAttr !== nextProps.viewAttr
            || this.props.value !== nextProps.value;
    }
    render() {
        const {type,finNum,className} = this.props;
        let showTds = new Array();
        if(type === 1){     //财务表头
            let {fontSize,label} = this.props;
            if(!fontSize)   fontSize = "9pt";   //字体大小为pt单位，默认9pt
            const headArr = ["分","角","元","十","百","千","万","十","百","千","亿","十"];
            for(let i = finNum - 1; i >= 0; i--) {
                let itemBorder = "fborder1";
                if(i === 0)
                    itemBorder = "";
                else if(i === 2)
                    itemBorder = "fborder2";
                else if((i-2)%3 === 0)
                    itemBorder = "fborder3";
                itemBorder = "finborder " + itemBorder;
                showTds.push(<td className={itemBorder}>{headArr[i]}</td>)
            }
            return (
                <div className="wf-findiv">
                    <div className="fintop">{label}</div>
                    <div className="finbottom">
                        <table className={`fintable ${className}`}>
                            <tr>{showTds}</tr>
                        </table>
                    </div>
                    <style>{`.${className} td{width:${parseInt(fontSize.replace("pt",""))*2}px;}`}</style>
                </div>
            )
        }else if(type === 2){   //财务表览
            const {viewAttr,value,clickEvent} = this.props;
            const valArr = this.transFinancialArr(value);
            for(let i = finNum - 1; i >= 0; i--) {
                let itemBorder = "fborder1";
                if(i === 0)
                    itemBorder = "";
                else if(i === 2)
                    itemBorder = "fborder2";
                else if((i - 2) % 3 === 0)
                    itemBorder = "fborder3";
                itemBorder = "finborder " + itemBorder;
                showTds.push(<td className={itemBorder}>{valArr[i]||""}</td>)
            }
            return(
                <div className="wf-findiv" onClick={clickEvent}>
                    <table className={`fintable ${className}`}>
                        <tr>{showTds}</tr>
                    </table>
                </div>
            )
        }else
            return <span></span>;
    }
	transFinancialArr(fieldVal) {
        var valArr = new Array();
		if(fieldVal === "" || fieldVal === null){
            if(this.props.viewAttr === 3)   //必填标示
                valArr.push(<span style={{color: "red !important", fontSize:"14px !important"}}>*</span>);
            return valArr;
        }
		fieldVal = fieldVal.replace(/,/g, "");
		var reg1 = /^(-?\d+)(\.\d+)?$/;
		var reg2 = /^(-?\d*)(\.\d+)$/;      //解决类型-.22这种格式
		if(reg1.test(fieldVal) || reg2.test(fieldVal)) {
			fieldVal = parseFloat(fieldVal).toFixed(2);
			if((fieldVal.length > 2 && fieldVal.substring(0, 2) == "0.") || (fieldVal.length > 3 && fieldVal.substring(0, 3) == "-0.")) {
				fieldVal = fieldVal.replace("0.", ".");
			}
			for(var i = fieldVal.length - 1; i >= 0; i--) {
				var valc = fieldVal.charAt(i);
				if(valc != ".")
					valArr.push(valc);
			}
		}
		return valArr;
	}
}

export default main