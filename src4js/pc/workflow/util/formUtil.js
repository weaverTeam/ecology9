import Immutable from 'immutable'
import {WeaTools} from 'ecCom'

//获取字段对象
export const getFieldInfoObj = (formState, fieldid, tableMark) =>{
    if(!!!tableMark)    tableMark = getBelTableMark(formState, fieldid);
    if(tableMark === "" || !formState.hasIn(["conf", "tableInfo", tableMark, "fieldinfomap", fieldid.toString()]))
        return null;
    return formState.getIn(["conf", "tableInfo", tableMark, "fieldinfomap", fieldid.toString()]);
}

//获取字段value值
export const getFieldValue = (formState, fieldid, rowIndex, tableMark, needDefNull) =>{
    const obj = getFieldValueObj(formState, fieldid, rowIndex, tableMark);
    let fieldval;
    if(obj && obj.has("value"))
        fieldval = obj.get("value").toString();
    else
        fieldval = needDefNull === true ? null : "";    //不存在值
    if(fieldval === ""){    //空值处理
        const fieldObj = getFieldInfoObj(formState, fieldid, tableMark);
        if(fieldObj && fieldObj.get("htmltype") === 4)      //check框空值置0
            fieldval = "0";
    }
    return fieldval;
}

//获取数值类型字段value值
export const getNumFieldValue = (formState, fieldid, rowIndex, tableMark) =>{
    const obj = getFieldValueObj(formState, fieldid, rowIndex, tableMark);
    const val = obj ? obj.get("value").toString().replace(/,/g, "") : "";
    return judgeIsNumber(val) ? parseFloat(val) : 0;
}

//获取字段值对象
export const getFieldValueObj = (formState, fieldid, rowIndex, tableMark) =>{
    let obj = null;
    if(!!!tableMark)    tableMark = getBelTableMark(formState, fieldid);
    if(tableMark === "main"){
        obj = formState.getIn(["mainData", `field${fieldid}`]);
    }else if(tableMark.indexOf("detail_") > -1){
        obj = formState.getIn(["detailData", tableMark, "rowDatas", `row_${rowIndex}`, `field${fieldid}`]);
    }
    return obj;
}

//获取明细字段合计值
export const getDetailFieldSumValue = (formState, fieldid, tableMark) =>{
    let sum = 0;
    formState && formState.getIn(["detailData", tableMark, "rowDatas"]).map(rowData=>{
        let obj = rowData.get(`field${fieldid}`);
        let val = obj ? obj.get("value").toString().replace(/,/g, "") : "";
        val = judgeIsNumber(val) ? parseFloat(val) : 0;
        sum += val;
    });
    return sum;
}

//获取明细所有行标示，以逗号分隔
export const getDetailAllRowIndexStr = (formState, detailMark) =>{
    if(!formState)
        return "";
    return getDetailAllRowIndexString(formState.get("detailData"), detailMark);
}

//获取明细所有行标示，以逗号分隔
export const getDetailAllRowIndexString = (detailData, detailMark) =>{
    if(detailMark.indexOf("detail_") === -1 || !detailData || !detailData.hasIn([detailMark, "rowDatas"]))
        return "";
    let str = "";
    detailData.getIn([detailMark, "rowDatas"]).map((v,k) =>{
        str += k.substring(4)+",";
    });
    if(str !== "")
        str = str.substring(0, str.length-1);
    return str;
}

//获取明细最后一行的rowKey
export const getDetailLastRowKey = (detailData, detailMark) =>{
    let rowArr = new Array();
    detailData && detailData.hasIn([detailMark, "rowDatas"]) && detailData.getIn([detailMark, "rowDatas"]).map((v,k) =>{
        rowArr.push({rowKey: k, orderid: v.get("orderid")});
    });
    rowArr = rowArr.sort((a,b) =>{
        return b.orderid - a.orderid;
    });
    return rowArr.length>0 ? rowArr[0].rowKey : "";
}

//根据字段ID获取所属具体所属主表/明细表
export const getBelTableMark = (formState, fieldid) =>{
    let tableMark = "";
    fieldid = fieldid.toString();
    formState && formState.getIn(["conf", "tableInfo"]).map((v,k) =>{
        if(v.hasIn(["fieldinfomap", fieldid]))
            tableMark = k;
    });
    return tableMark;
}

//判断字段是否属于指定标示(主表/明细表)
export const judgeFieldBelTable = (formState, tableMark, fieldid) =>{
    if(formState && formState.hasIn(["conf", "tableInfo", tableMark, "fieldinfomap", fieldid.toString()]))
        return true;
    else
        return false;
}

//判断字段是否属于明细行规则赋值字段
export const judgeFieldBelRowCalAssignField = (conf, detailMark, fieldid) =>{
    if(!conf.hasIn(["linkageCfg", "rowCalCfg", "rowCalAssignInfo", fieldid]))
        return false;
    let isValid = true;
    conf.getIn(["linkageCfg", "rowCalCfg", "rowCalAssignInfo", fieldid]).map(id =>{
        if(!conf.hasIn(["cellInfo", "fieldCellInfo", id]) || conf.getIn(["tableInfo", detailMark, "fieldinfomap", id, "viewattr"]) <= 0)
            isValid = false;    //取值字段不在模板上认为是无效行规则
    });
    return isValid;
}

//判断明细表是否存在模板上
export const judgeDetailUnExistTemplate = (formState, detailMark) =>{
    if(formState && formState.hasIn(["conf", "cellInfo", "subPanelInfo", detailMark]))
        return false;
    else
        return true;
}

//判断明细是否含有合计字段(用于低级模式是否需要扩充合计行)
export const judgeDetailContainSum = (conf, detailMark) =>{
    let contailSum = false;
    conf.getIn(["tableInfo", detailMark, "fieldinfomap"]).map((v,fieldid)=>{
        if(v.get("viewattr") > 0 && conf.hasIn(["cellInfo", "fieldCellInfo", fieldid]) && conf.hasIn(["linkageCfg", "colCalCfg", fieldid]))
            contailSum = true;
    });
    return contailSum;
}

//判断模板是否包含开启横向滚动条的明细
export const judgeExistXScrollDetail = (conf) =>{
    let exist = false;
    conf.get("tableInfo").map((v,k) =>{
        if(k.indexOf("detail_") > -1 && v.getIn(["detailtableattr", "allowscroll"]) === 1){
            exist = true;
        }
    });
    return exist;
}

//判断字段是否再模板上
export const judgeFieldUnExistTemplate = (formState, tableMark, fieldid) =>{
    fieldid = fieldid.toString();
    if(formState && formState.hasIn(["conf", "cellInfo", "fieldCellInfo", fieldid]) 
        && formState.getIn(["conf", "tableInfo", tableMark, "fieldinfomap", fieldid, "viewattr"]) > 0)
        return false;
    else
        return true;
}

//判断字段是否在可视区域(标签页情况)
export const judgeFieldExistVisibleArea = (conf, fieldid) =>{
    let visible = false;
    let fieldBelArea = conf.getIn(["cellInfo", "fieldCellInfo", fieldid, "symbol"]) || "";
    if(fieldBelArea.indexOf("mc_") > -1 || fieldBelArea.indexOf("detail_") > -1)    //多字段和明细字段取上一级
        fieldBelArea = conf.getIn(["cellInfo", "subPanelInfo", fieldBelArea, "symbol"]) || "";
    if(fieldBelArea === "main")
        visible = true;
    else if(fieldBelArea.indexOf("tab_") > -1){
        const belTabObj = jQuery(`div.tab_content#${fieldBelArea}_content`);
        if(belTabObj.length > 0 && belTabObj.is(":visible"))
            visible = true;
    }
    return visible;
}

//计算字段当前时刻的只读/必填属性
export const getFieldCurViewAttr = (fieldObj, tableMark, conf, fieldVar, params) =>{
    const fieldid = fieldObj.get("fieldid").toString();
    let viewAttr = parseInt(fieldObj.get("viewattr")) || 0;     //后台设置的字段属性
    if(viewAttr === 0 && conf.hasIn(["cellInfo", "fieldCellInfo", fieldid]))    //字段放在模板上但未设置属性的默认给只读属性
        viewAttr = 1;   
    if(fieldVar && fieldVar.has("viewAttr")){    //显示属性联动改变的字段属性
        const changeViewAttr = parseInt(fieldVar.get("viewAttr")) || 0;
        if(changeViewAttr === 1 || changeViewAttr === 2 || changeViewAttr === 3)
            viewAttr = changeViewAttr;
    }
    if(viewAttr === 2 || viewAttr === 3){
        //已办、提交确认置为只读
        if(parseInt(params.get("isviewonly") || 0) === 1 || parseInt(params.get("isaffirmance") || 0) === 1)
            viewAttr = 1;
        //不允许修改已有明细置为只读
        if(tableMark.indexOf("detail_") > -1 && fieldVar && fieldVar.get("isDetailExistField") === true && conf.getIn(["tableInfo",tableMark,"detailtableattr","isedit"]) !== 1)
            viewAttr = 1;
    }
    return viewAttr;
}

//提交校验字段必填范围
export const verifyRequiredEmptyField = (formState) =>{
    let emptyField = "";
    const variableArea = formState.get("variableArea");
    //计算联动隐藏行中的其它字段，不校验必填
    let ignoreVerifyRange = Immutable.fromJS([]);
    variableArea && variableArea.map((v,k) =>{
        if(k.indexOf("field") > -1 && v && v.get("viewAttr") === 5){
            const rowMark = formState.getIn(["conf", "cellInfo", "fieldCellInfo", k.substring(5), "rowMark"]);
            const rowFields = formState.getIn(["conf", "cellInfo", "mainRowFields", rowMark]);
            ignoreVerifyRange = ignoreVerifyRange.concat(rowFields);
        }
    });
    //计算明细需要校验的行号
    let detailVerifyRowIndexStr = {};
    formState.getIn(["conf", "tableInfo"]).map((tableInfo, tableMark) =>{
        if(tableMark.indexOf("detail_") > -1){
            let rowIndexStr = "";
            const allowEditDetailExistRow = formState.getIn(["conf", "tableInfo", tableMark, "detailtableattr", "isedit"]) === 1;   //允许修改已有明细
            formState.getIn(["detailData", tableMark, "rowDatas"]).map((v,k) =>{
                const keyid = parseInt(v.get("keyid")||0);
                if(keyid <= 0 || (keyid > 0 && allowEditDetailExistRow))
                    rowIndexStr +=  k.substring(4)+",";
            });
            if(rowIndexStr !== "")
                rowIndexStr = rowIndexStr.substring(0, rowIndexStr.length-1);
            detailVerifyRowIndexStr[tableMark] = rowIndexStr;
        }
    });
    //校验必填字段的范围
    let verifyRange = [];
    formState.getIn(["conf", "cellInfo", "fieldCellInfo"]).map((v,fieldid) =>{
        const orderid = v.get("orderid") || 0;
        const isDetail = v.get("symbol").indexOf("detail_")>-1;
        const tableMark = isDetail ? v.get("symbol") : "main";
        if(!formState.hasIn(["conf", "tableInfo", tableMark, "fieldinfomap", fieldid]))
            return "";
        const fieldInfo = formState.getIn(["conf", "tableInfo", tableMark, "fieldinfomap", fieldid]);
        const htmltype = fieldInfo.get("htmltype");
        if(htmltype === 4 || htmltype === 7 || htmltype === 8 || htmltype === 9 || fieldid === "-9"
            || (!isDetail && ignoreVerifyRange.includes(fieldid)))     //Check框、联动隐藏的行字段不校验必填
            return "";
        //附件上传未设置固定目录的不校验必填
        if(htmltype === 6 && fieldInfo.getIn(["fileattr","catelogType"]) === 0 && trim(fieldInfo.getIn(["fileattr","docCategory"])) === "")
            return "";
        const rowIndexStr = isDetail ? detailVerifyRowIndexStr[tableMark] : "-1";
        !!rowIndexStr && rowIndexStr.split(",").map(rowIndex =>{
            const fieldMark = isDetail ? `field${fieldid}_${rowIndex}` : `field${fieldid}`;
            let needVerify = false;
            if(variableArea.hasIn([fieldMark, "viewAttr"]) && variableArea.getIn([fieldMark, "viewAttr"]) > 0){    //显示属性联动控制必填
                needVerify = variableArea.getIn([fieldMark, "viewAttr"]) === 3;
            }else{
                needVerify = fieldInfo.get("viewattr") === 3;
            }
            if(needVerify)
                verifyRange.push({fieldid, orderid, tableMark, rowIndex});     
        });
    });
    verifyRange = verifyRange.sort((a,b) =>{
        return a.orderid-b.orderid;
    });
    //根据范围依次校验值是否填写
    for(let i=0; i<verifyRange.length; i++){
        const obj = verifyRange[i];
        const fieldid = obj.fieldid;
        const tableMark = obj.tableMark;
        const rowIndex = obj.rowIndex;
        const fieldMark = tableMark.indexOf("detail_")>-1 ? `field${fieldid}_${rowIndex}` : `field${fieldid}`;
        let fieldValue = getFieldValue(formState, fieldid, rowIndex, tableMark);
        fieldValue = jQuery.trim(fieldValue);
        if(fieldValue === ""){
            emptyField = fieldMark;
            break;
        }
    }
    return emptyField;
}

//验证流程标题必填
export const saveJudgeRequestNameEmpty = (formState) =>{
    let isEmpty = false;
    const fieldValue = getFieldValue(formState, "-1", 0, "main");
    if(jQuery.trim(fieldValue) === "")
        isEmpty = true;
    return isEmpty;
}

//判断表单内容是否存在修改
export const judgeFormDataExistModify = (formState) =>{
    const mainData = formState.get("mainData").toJS();
    const mainData_load = formState.get("mainData_load").toJS();
    if(!compareObject(mainData, mainData_load))
        return true;
    const detailData = formState.get("detailData").toJS();
    const detailData_load = formState.get("detailData_load").toJS();
    if(!compareObject(detailData, detailData_load))
        return true;
    return false;
}

const compareObject = (o1,o2) =>{
  	if(typeof o1 != typeof o2)      return false;
  	if(typeof o1 == 'object'){
	    for(var o in o1){
	      	if(typeof o2[o] == 'undefined')     return false;
	      	if(!compareObject(o1[o], o2[o]))     return false;
	    }
	    return true;
  	}else if(typeof o1 == 'function'){
  		return true;
  	}else{
    	return o1 === o2;
  	}
}

//提交校验明细必须添加是否满足
export const verifyMustAddDetail = (formState) =>{
    let needAddDetailMark = 0;
    formState.getIn(["conf", "tableInfo"]).map((v,k) =>{
        if(needAddDetailMark > 0 || k.indexOf("detail_") === -1)
            return "";  //map循环return代表返回值，无法阻断循环
        if(v.getIn(["detailtableattr", "isneed"]) === 1){
            const rowDatas = formState.getIn(["detailData", k, "rowDatas"]);
            if(!rowDatas || rowDatas.size === 0)
                needAddDetailMark = parseInt(k.substring(7));
        }
    });
    return needAddDetailMark;
}

//计算表达式结果
export const calExpressionResult = (expression) =>{
    let result = "";
    try{
        result = eval(expression);
    }catch(e){
        if(window.console)  console.log("Eval Appear Exception:"+expression);
    }
    return result.toString();
}

//修改Redux字段值前统一对字段做格式化
export const formatDatasBeforeChangeRedux = (formState, changeFieldDatas, tableMark) =>{
    for(const key in changeFieldDatas){
        const fieldid = key.substring(5);
        const fieldObj = getFieldInfoObj(formState, fieldid, tableMark);
        if(!fieldObj)
            continue;
        const htmltype = fieldObj.get("htmltype");
        const detailtype = fieldObj.get("detailtype");
        let fieldValue = changeFieldDatas[key].value;
        if(htmltype === 1){
            if(detailtype === 1){
                fieldValue = convertSpecialChar(fieldValue);
            }else{  //数值字段格式化
                fieldValue = formatNumFieldValue(fieldValue, fieldObj);
            }
        }else if(htmltype === 2 && detailtype === 1){
            fieldValue = convertSpecialChar(fieldValue);
        }else if(htmltype === 3 && detailtype === 2){   //日期字段格式化
            const reg = /^\d{4}-\d{1,2}-\d{1,2}$/;
            if(!!fieldValue && !reg.test(fieldValue))
                fieldValue = "";
        }else if(htmltype === 3 && detailtype === 19){  //时间字段格式化
            const reg = /^\d{1,2}:\d{1,2}$/;
            if(!!fieldValue && !reg.test(fieldValue))
                fieldValue = "";
        }else if(htmltype === 4){   //check框只允许0/1值入库
            if(!fieldValue || (fieldValue.toString() !== "0" && fieldValue.toString() !== "1"))
                fieldValue = "0";
        }
        changeFieldDatas[key].value = fieldValue;
    }
    return changeFieldDatas;
}

//时间字段值补零
export const formatTimeValue = (value) =>{
    const reg = /^\d{1,2}:\d{1,2}$/;
    if(!!value && reg.test(value)){
        let arr = value.split(":");
        if(arr[0].length === 1)     arr[0] = "0"+arr[0];
        if(arr[1].length === 1)     arr[1] = "0"+arr[1];
        value = arr[0]+":"+arr[1];
    }else
        value = "";
    return value;
}

//修改Redux时，替换特殊字符,转换成提交时需要的格式
const convertSpecialChar = (str) =>{
    str = str.toString();
    str = str.replace(/<br>/ig, "\n");
    str = str.replace(/&nbsp;/ig, " ");
    str = str.replace(/&lt;/ig, "<");
    str = str.replace(/&gt;/ig, ">");
    str = str.replace(/&quot;/ig, "\"");
    str = str.replace(/&amp;/ig, "&");
    return str;
}

//渲染组件时，字符还原成可显示DOM格式
export const restoreSpeicalChar = (str, htmltype, detailtype, viewAttr, fieldid, conf, params) =>{
    if(viewAttr === 1 && ((htmltype === 1 && detailtype ===1) || (htmltype === 2 && detailtype ===1))){     //只读时字符转换成DOM结构
        //部分多行文本框由程序插入html源码，只读情况需显示转译后html内容
        if(htmltype === 2 && detailtype === 1){
            const fieldKey = `field${fieldid}_${params.get("isbill")}`;
            const support_html_textarea_field = conf.getIn(["propFileCfg", "support_html_textarea_field"]) || "";
            if(support_html_textarea_field.indexOf(fieldKey) > -1){
                str = str.replace(/\n/ig, "<br>");
                return str;
            }
        }
        //需先转译
        str = str.replace(/&/ig, "&amp;");
        str = str.replace(/</ig, "&lt;");
        str = str.replace(/>/ig, "&gt;");
        //后转译
        str = str.replace(/\n/ig, "<br>");
        str = str.replace(/ /ig, "&nbsp;");
        str = str.replace(/\"/ig, "&quot;");
    }
    return str;
}

//根据字段信息格式化字段值(浮点数位数、转千分位等)
export const formatNumFieldValue = (realval, fieldObj) =>{
	if(realval === null || typeof realval === "undefined" || !fieldObj)
        return "";
    const isqfw = fieldObj.get("detailtype") === 5;
    if(isqfw)
        realval = realval.toString().replace(/,/g, "");
    if(!judgeIsNumber(realval))
        return "";
    return convertFloatValue(realval, fieldObj.get("qfws"), isqfw);
}

/**
 * 数值转换(精度、千分位)等
 * @param {String} realval   真实值
 * @param {int} decimals  精度
 * @param {Boolean} transQfw  是否千分位
 * @param {Boolean} transAbs  是否绝对值
 */
export const convertFloatValue = (realval, decimals, transQfw, transAbs) =>{
	if(!judgeIsNumber(realval))     //非数值直接返回原始值
		return realval;
    realval = realval.toString();
	var formatval = "";
	if(decimals === 0){		//需取整
		formatval = Math.round(parseFloat(realval)).toString();
	}else{
        var n = Math.pow(10, decimals);
        formatval = (Math.round(parseFloat(realval)*n)/n).toString();
		var pindex = formatval.indexOf(".");
		var pointLength = pindex>-1 ? formatval.substr(pindex+1).length : 0;	//当前小数位数
		if(decimals > pointLength){		//需补零
            if(pindex == -1)
			    formatval += ".";
			for(var i=0; i<decimals-pointLength; i++){
				formatval += "0";
			}
		}
	}
	var index = formatval.indexOf(".");
	var intPar = index>-1 ? formatval.substring(0,index) : formatval;
	var pointPar = index>-1 ? formatval.substring(index) : "";
	//取绝对值
	if(transAbs === true){		//取绝对值
		intPar = Math.abs(intPar).toString();
	}
	if(transQfw === true){				//整数位format成千分位
   		var reg1 = /(-?\d+)(\d{3})/;
        while(reg1.test(intPar)) {   
        	intPar = intPar.replace(reg1, "$1,$2");   
        } 
	}
	formatval = intPar + pointPar;
	return formatval;
}

//判断值是否属于数字
export const judgeIsNumber = (val) =>{
    if(val === null || typeof val === "undefined")
        return false;
    const reg = /^(-?\d+)(\.\d+)?$/;
    return reg.test(val.toString());
}

//处理流程多人力操作组信息
export const getMultiUserGroupInfo =(conf,symbol,cellObj,datas,ids)=>{
	const isDetail = symbol.indexOf("detail_")>-1;
	const multiUserType = {"subcom":2,"dept":3,"group":4,"virtual_subcom":5,"virtual_dept":7,"all":9};
	let groupInfo = {};
	if(!isDetail){
		const fieldid = cellObj.get("field").toString();
		const fieldObj = conf.getIn(["tableInfo", "main", "fieldinfomap", fieldid]);
		const detailtype = fieldObj.get("detailtype")||0;
		if(detailtype === 17){ //多人力
            let i = 0;
			datas && datas.map((v,k)=>{
				const type  =  v.type;
				const typeid  = v.id||0;
				if(type  == "subcom" || type == "dept" || type  == "group"){
					const users = v.users;
					let userIds  = [];
					users && users.map((o)=>{
						userIds.push(o.id);							
					});
                    groupInfo[`group_${i}`] = multiUserType[type]+"|"+typeid+"|"+userIds.join(",");
                    i++;
				}else if(type == "all"){
                    groupInfo[`group_${i}`] = multiUserType[type]+"|"+typeid+"|"+ids;
                    i++;
                }
			});
            groupInfo.groupnum = i;
		}
	}
	return groupInfo;
}

//调用客户自定义回掉函数
export const doDefinitionCallBackFun =(fieldid,rowindex,ids,datas,fieldObj,params,isDetail) => {
    if(typeof(wfbrowvaluechange) === "function"){
        wfbrowvaluechange(null,fieldid, rowindex);
    }
    const detailtype = fieldObj ? fieldObj.get("detailtype") : 0;
    if(typeof(__browVal4CustomCheck) === "function"){
    	const workflowid  = params ? params.get("workflowid") : -1;
    	const requestid  = params ? params.get("requestid") : -1;
    	const fieldMark  = isDetail ? `field${fieldid}_${rowindex}`:`field${fieldid}`;
     	__browVal4CustomCheck(fieldid,workflowid,requestid,detailtype,ids,datas,fieldMark);
    }
    
    //报销费用类型 
    if(detailtype === 22){
    	try{
    		WeaTools.callApi('/api/fna/budgetfeeTypeBrowser/updateBudgetfeeTypeUsed','GET',{subjectId:ids});	
    	}catch(e){if(window.console) console.log(e)}
    	
    }
}

//判断浏览器版本
export const getBrowserVersion = () =>{
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isChrome = userAgent.indexOf("Chrome") > -1 && userAgent.indexOf("Safari") > -1; //判断Chrome浏览器
    if (isChrome)
        return "Chrome"; 
    var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器
    if (isIE) {
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");  
        reIE.test(userAgent);  
        var fIEVersion = parseFloat(RegExp["$1"]);  
        if(fIEVersion == 7)
            return "IE7";
        else if(fIEVersion == 8)
            return "IE8";
        else if(fIEVersion == 9)
            return "IE9";
        else if(fIEVersion == 10)
            return "IE10";
        else if(fIEVersion == 11)
            return "IE11";
        else
            return "IE";
    }
    var isEdge = userAgent.indexOf("Windows NT 6.1; Trident/7.0;") > -1 && !isIE; //判断是否IE的Edge浏览器
    var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器
    var isSafari = userAgent.indexOf("Safari") > -1 && userAgent.indexOf("Chrome") == -1; //判断是否Safari浏览器  
    if (isOpera)
        return "Opera"; 
    if (isEdge)
        return "Edge";
    if (isFF) 
        return "FF";
    if (isSafari)
        return "Safari";
}

//代码块中<script id>标签处理
export const manageScriptContent = (content) =>{
    if(!content)
        return "";
    const arr1 = content.match(new RegExp("<script .*?id=.*?></script>", "ig")) || new Array();
    const arr2 = content.match(new RegExp("<script .*?id=.*?/>", "ig")) || new Array();
    const arr = arr1.concat(arr2);
    let appendScriptSpan = "";
    arr !== null && arr.map(v =>{
        appendScriptSpan += v.replace(new RegExp("(<|/)script","ig"), "$1span");
    });
    return content+appendScriptSpan;
}
//人力资源条件数据处理
export const spellResourceCondition = (datas) =>{
	if(datas){
		let value  = [];
		datas.map((item,index)=>{
			if(item.sharetype  ==  "1"){
				value.push(`1_${item.relatedshare}_0_0`);
			}else if(item.sharetype == "2" || item.sharetype == "3"){
				value.push(`${item.sharetype}_${item.relatedshare}_0_${item.seclevel.replace('-','|@|')}`);
			}else if(item.sharetype == "4"){
				value.push(`4_${item.relatedshare}_${item.rolelevel}_${item.seclevel.replace('-','|@|')}`);
			}else if(item.sharetype == "5"){
				value.push(`5_0_0_${item.seclevel.replace('-','|@|')}`);
			}else if(item.sharetype == "6"){
				if(item.joblevel == '2'){
					value.push(`6_${item.relatedshare}_0_2|@|2`);
				}else{
					value.push(`6_${item.relatedshare}_0_${item.joblevel}|@|${item.jobfield}`)					
				}
			}
		});
		return value.join('~');
	}
	return '';
}