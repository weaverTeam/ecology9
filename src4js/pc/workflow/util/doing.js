import Immutable from 'immutable'

window.reqListUtil = (function(){
	return {
		//转发
		doForward(requestid, userid , resourceids) {
			resourceids = resourceids == null ?"":resourceids;	
			const forwardurl = "/workflow/request/Remark.jsp?f_weaver_belongto_userid=" + userid + "&f_weaver_belongto_usertype=0&requestid=" + requestid+"&resourceids="+resourceids;
			let dlg = new window.top.Dialog(); // 定义Dialog对象
			dlg.Model = false;
			dlg.Width = 1060;
			dlg.Height = 500;
			dlg.URL = forwardurl;
			dlg.Title = SystemEnvLabel.getHtmlLabelName(24964);
			dlg.maxiumnable = true;
			dlg.show();
		},
		//打印
		doPrint(requestid, beluserid, otherpara='') {
			const printurl = false ? "/spa/workflow/index.html#/main/workflow/req" : "/workflow/request/PrintRequest.jsp";
			const urlParams = `?requestid=${requestid}&isprint=1&f_weaver_belongto_userid=${beluserid}&f_weaver_belongto_usertype=0${otherpara}`;
			const width = screen.availWidth - 10;
			const height = screen.availHeight - 50;
			let szFeatures = "top=0,";
			szFeatures += "left=0,";
			szFeatures += "width=" + width + ",";
			szFeatures += "height=" + height + ",";
			szFeatures += "directories=no,";
			szFeatures += "status=yes,toolbar=no,location=no,";
			szFeatures += "menubar=no,";
			szFeatures += "scrollbars=yes,";
			szFeatures += "resizable=yes";
			window.open(printurl+urlParams, "", szFeatures);
		},
		//查看表单日志
		seeFormLog(requestid, nodeid) {
			let dialog = new window.top.Dialog();
			dialog.currentWindow = window;
			const url = "/workflow/request/RequestModifyLogView.jsp?requestid=" + requestid + "&nodeid=" + nodeid + "&isAll=0&ismonitor=0&urger=0";
			dialog.Title = SystemEnvLabel.getHtmlLabelName(21625);
			dialog.Width = 550;
			dialog.Height = 550;
			dialog.Drag = true;
			dialog.maxiumnable = true;
			dialog.URL = url;
			dialog.show();
		}
	}
})();

//分页控件---设为只读
const doReadIt = (requestid, otherpara, obj) => {
	const viewScope = window.store_e9_workflow.getState().workflowlistDoing.get('nowRouterWfpath');
	try{
		document.getElementById("wflist_"+requestid+"span").innerHTML = "";
	}catch(e){}
	if(viewScope == 'queryFlow')
		return;
    const state = window.store_e9_workflow.getState()['workflow' + viewScope];
	const flag = state && state.getIn(['sharearg','flag']);
	const viewType = state && state.getIn(['sharearg','viewType']);
	const overtimetype = state && state.getIn(['sharearg','overtimetype']);
	jQuery.ajax({
		type : "GET",
		url : "/workflow/search/WFSearchResultReadIt.jsp?f_weaver_belongto_userid=&f_weaver_belongto_usertype=0&requestid=" + requestid + ",&flag="+flag,
		success : () => {
			if (flag == "newWf") {
				parent.window.location.href = "/system/sysRemindWfLink.jsp?f_weaver_belongto_userid=" + otherpara + "&f_weaver_belongto_usertype=0&flag=newWf";
			} else if (flag == "rejectWf") {
				parent.window.location.href = "/system/sysRemindWfLink.jsp?f_weaver_belongto_userid=" + otherpara + "&f_weaver_belongto_usertype=0&flag=rejectWf";
			} else if (flag == "overtime") {
				parent.window.location.href = "/workflow/search/WFTabForOverTime.jsp?f_weaver_belongto_userid="
						+ otherpara + "&f_weaver_belongto_usertype=0&isovertime=1&viewType="+viewType+"&isFromMessage=1&flag=overtime&overtimetype="+overtimetype;
			} else if (flag == "endWf") {
				parent.window.location.href = "/system/sysRemindWfLink.jsp?f_weaver_belongto_userid=" + otherpara + "&f_weaver_belongto_usertype=0&flag=endWf";
			} else {
				window._table.reLoad();
			}
		}
	});
}
window.doReadIt = doReadIt;

//待办-全部只读
// export const doAllReadIt = (requestidStr, sharearg) => {
// 	const flag = sharearg && sharearg.get("flag");
// 	const viewType = sharearg && sharearg.get("viewType");
// 	const overtimetype = sharearg && sharearg.get("overtimetype");
// 	jQuery.ajax({
// 		type : "GET",
// 		url : "/workflow/search/WFSearchResultReadIt.jsp?requestid="+requestidStr+"&flag="+flag,
// 		success : () => {
// 			if (flag == "newWf") {
// 				parent.window.location.href = "/system/sysRemindWfLink.jsp?flag=newWf";
// 			} else if (flag == "rejectWf") {
// 				parent.window.location.href = "/system/sysRemindWfLink.jsp?flag=rejectWf";
// 			} else if (flag == "overtime") {
// 				parent.window.location.href = "/workflow/search/WFTabForOverTime.jsp?isovertime=1&viewType="+viewType+"&isFromMessage=1&flag=overtime&overtimetype="+overtimetype;
// 			} else if (flag == "endWf") {
// 				parent.window.location.href = "/system/sysRemindWfLink.jsp?flag=endWf";
// 			} else {
// 				window._table.reLoad();
// 			}
// 		}
// 	});
// }


const openFullWindowHaveBarForWFList = (url, requestid) => {
	try {
		document.getElementById("wflist_" + requestid + "span").innerHTML = "";
	} catch (e) {}
	const redirectUrl = url;
	const width = screen.availWidth - 10;
	const height = screen.availHeight - 50;
	let szFeatures = "top=0,";
	szFeatures += "left=0,";
	szFeatures += "width=" + width + ",";
	szFeatures += "height=" + height + ",";
	szFeatures += "directories=no,";
	szFeatures += "status=yes,toolbar=no,location=no,";
	szFeatures += "menubar=no,";
	szFeatures += "scrollbars=yes,";
	szFeatures += "resizable=yes"; // channelmode
	window.open(redirectUrl, "", szFeatures);
}

window.openFullWindowHaveBarForWFList = openFullWindowHaveBarForWFList;

const showallreceived = (requestid, returntdid) => {
	jQuery.ajax({
		url : "/workflow/search/WorkflowUnoperatorPersons.jsp?requestid="+ requestid + "&returntdid=" + returntdid,
		type : "post",
		success : function(res) {
			try {
				jQuery("#" + returntdid).html(jQuery.trim(res));
				jQuery("#" + returntdid).parent().attr("title", jQuery.trim(res).replace(/[\r\n]/gm, ""))
			} catch (e) {}
		}
	});
}

window.showallreceived = showallreceived;

window._table = {
	reLoad: () => {
		const globalState = window.store_e9_workflow.getState();
		const viewScope = globalState.workflowlistDoing.get('nowRouterWfpath');
	    const comsWeaTable = globalState.comsWeaTable;
		const dataKey  = globalState['workflow' + viewScope].get("dataKey");
        const tablekey = dataKey ? dataKey.split('_')[0] : 'init';
		const current = comsWeaTable.getIn([tablekey, 'current']);
		
		if(viewScope == 'queryFlow'){
			window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowQueryAction.doSearch({current}));
		}else{
			window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowListAction.initDatas());
			window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowListAction.doSearch({current}));
		}
	}
}

window._ListenerCtrl = {
	hasPress: false,
	keyDown: function(e){
		if(e.keyCode === 17)
			_ListenerCtrl.hasPress = true;
	},
	keyUp: function(e){
		if(e.keyCode === 17) 
			_ListenerCtrl.hasPress = false;
	},
	bind: function(){
		jQuery(window).bind("keydown", _ListenerCtrl.keyDown).bind("keyup", _ListenerCtrl.keyUp);
	},
	unbind: function(){
		jQuery(window).unbind("keydown", _ListenerCtrl.keyDown).unbind("keyup", _ListenerCtrl.keyUp);
	}
}


