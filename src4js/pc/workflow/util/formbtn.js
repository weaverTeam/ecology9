import Immutable from 'immutable'
const openFullWindowHaveBar = (url) => {
	var redirectUrl = url;
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 50;
	var szFeatures = "top=0,";
	szFeatures += "left=0,";
	szFeatures += "width=" + width + ",";
	szFeatures += "height=" + height + ",";
	szFeatures += "directories=no,";
	szFeatures += "status=yes,toolbar=no,location=no,";
	szFeatures += "menubar=no,";
	szFeatures += "scrollbars=yes,";
	szFeatures += "resizable=yes"; //channelmode
	window.open(redirectUrl, "", szFeatures);
}

window.openFullWindowHaveBar = openFullWindowHaveBar;

const openUnFullWindow = (url,width,height,top,left) => {
	var szFeatures = "top="+top+"," ;
	szFeatures +="left="+left+"," ;
	szFeatures +="width="+width+"," ;
	szFeatures +="height="+height+"," ;
	szFeatures +="directories=no," ;
	szFeatures +="status=yes,toolbar=no,location=no," ;
	szFeatures +="menubar=no," ;
	szFeatures +="scrollbars=yes," ;
	szFeatures +="resizable=yes" ; //channelmode
	window.open(url,"",szFeatures) ;
}

window.openUnFullWindow  = openUnFullWindow;

/**
 * 提交需返回
 */
export const doSubmitBack = () => { 
	const para = {actiontype:"requestOperation",src:"submit",needwfback:"1"};
	const isFree = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','isFree']);
	const freewftype = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','freewftype']);
	const isremark = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','isremark']);
	const takisremark = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','takisremark']);
	const nodetype = window.store_e9_workflow.getState().workflowReq.getIn(['params','nodetype']);
	const isCptwf = window.store_e9_workflow.getState().workflowReq.getIn(['params','isCptwf']);
	
	if(isCptwf){
		
	}if(isFree == "1" && freewftype == "1" && nodetype == "0" && isremark == "0" && takisremark == "0"){
		doFreeWorkflow();
	}else{
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.updateSubmitToNodeId());
		doBeforeSubmit(para);
	}
}

window.doBeforeSubmit = function(para){
	doSubmit(para);
}

const doSubmit =(para) =>{
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doSubmit(para)); 
}

window.doSubmit = doSubmit;
/**
 * 
 */
export const doSubmit_Pre = (src) => {
	doBeforeSubmit({actiontype:"requestOperation",src:src||"submit"});
}

/**
 * 提交后不需返回
 */
export  const doSubmitNoBack = () => {
	const isFree = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','isFree']);
	const freewftype = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','freewftype']);
	const isremark = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','isremark']);
	const takisremark = window.store_e9_workflow.getState().workflowReq.getIn(['rightMenu','takisremark']);
	const nodetype = window.store_e9_workflow.getState().workflowReq.getIn(['params','nodetype']);
	const isCptwf = window.store_e9_workflow.getState().workflowReq.getIn(['params','isCptwf']);
	
	if(isCptwf){
		
	}if(isFree == "1" && freewftype == "1" && nodetype == "0" && isremark == "0" && takisremark == "0"){
		doFreeWorkflow();
	}else{
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.updateSubmitToNodeId());
		doBeforeSubmit({actiontype:"requestOperation",src:"submit",needwfback:"0"});
	}
}

/**
 * 流程保存
 */
export const doSave_nNew = () => { 
	doBeforeSubmit({actiontype:"requestOperation",src:"save"});
}


//流程督办
export const doSupervise = () => { 
	doBeforeSubmit({actiontype:"requestOperation",src:"supervise"});
}

//表单---转发，zdialog
export const doReviewE9 = (destuserid) => {
	const userSupportEdit = window.store_e9_workflow.getState().workflowReq.getIn(['params','userSupportEdit']);
	if(userSupportEdit){
		const params = {showForward: true,forwardflag: "1"};
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setShowForward(params));
	}else{
		const requestid = window.store_e9_workflow.getState().workflowReq.getIn(['params', 'requestid']);
		const userid = window.store_e9_workflow.getState().workflowReq.getIn(['params', 'f_weaver_belongto_userid']);
		reqListUtil.doForward(requestid, userid);
	}
}

//批注需反馈
export const doRemark_nBack = () => { 
	doBeforeSubmit({actiontype:"remarkOperation",src:"save",needwfback:"1"});
}

window.doRemark_nBack = doRemark_nBack;

//批注不需反馈
export const doRemark_nNoBack = () => {
	doBeforeSubmit({actiontype:"remarkOperation",src:"save",needwfback:"0"});
}

/**
 * 流程打印
 */
export const doPrintRequest = () => {
	const formstate = window.store_e9_workflow.getState().workflowReq.get('submitParams');
	const isrequest = window.store_e9_workflow.getState().workflowReq.getIn(['params','isrequest'])||0;
	const authStr = window.store_e9_workflow.getState().workflowReq.getIn(['params','authStr']);
	const authSignatureStr = window.store_e9_workflow.getState().workflowReq.getIn(['params','authSignatureStr']);
	let otherpara = "&authStr="+authStr+"&authSignatureStr="+authSignatureStr+"&isrequest="+isrequest;
	const requestid = formstate.get('requestid');
	const userid = formstate.get('f_weaver_belongto_userid');
	reqListUtil.doPrint(requestid, userid,otherpara);
}


//流程暂停
export const doStop = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doStop()) }


//流程撤销
export const doCancel = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doCancel()) }


//流程启用
export const doRestart = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doRestart()) }


//流程强制收回
export const doRetract = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doRetract()) }


//强制归档
export const doDrawBack = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doDrawBack()) }


//打印日志
export const doPrintViewLog = () => {
	var diag_vote = null;
	if(window.top.Dialog) {
		diag_vote = new window.top.Dialog();
	} else {
		diag_vote = new Dialog();
	}

	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const requestid = params.get('requestid');
	const userid = params.get('f_weaver_belongto_userid');
	const usertype = params.get('f_weaver_belongto_usertype');

	diag_vote.currentWindow = window;
	diag_vote.Width = 800;
	diag_vote.Height = 550;
	diag_vote.Modal = true;
	diag_vote.maxiumnable = true;
	diag_vote.Title = SystemEnvLabel.getHtmlLabelName(21533);
	diag_vote.URL = "/workflow/request/WorkflowLogNew.jsp?f_weaver_belongto_userid=" + userid + "&f_weaver_belongto_usertype=" + usertype + "&requestid=" + requestid;
	diag_vote.show();
}


//新建任务列表
export const openTaskList = () => {
	const requestid = window.store_e9_workflow.getState().workflowReq.getIn(['params', 'requestid']);
	openWin("/workrelate/task/data/TaskList.jsp?requestids=" + requestid);
}


//新建任务
export const doCreateTask = () => {
	const requestid = window.store_e9_workflow.getState().workflowReq.getIn(['params', 'requestid']);
	openWin("/workrelate/task/data/Add.jsp?requestids=" + requestid);
}


const openWin = (url, showw, showh) => {
	if(showw == null || typeof(showw) == "undefined") showw = 900;
	if(showh == null || typeof(showh) == "undefined") showh = 520;
	var redirectUrl = url;
	var height = screen.height;
	var width = screen.width;
	var top = (height - showh) / 2;
	var left = (width - showw) / 2;
	var szFeatures = "top=" + top + ",";
	szFeatures += "left=" + left + ",";
	szFeatures += "width=" + showw + ",";
	szFeatures += "height=" + showh + ",";
	szFeatures += "directories=no,";
	szFeatures += "status=yes,";
	szFeatures += "menubar=no,";
	szFeatures += "scrollbars=yes,";
	szFeatures += "resizable=yes"; //channelmode
	window.open(redirectUrl, "", szFeatures);
}
//window.openWin = openWin;

//退回
export const doReject_New = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doReject()) }


//
export const doViewModifyLog = () => {
	const { f_weaver_belongto_userid, requestid, nodeid, wfmonitor } = window.store_e9_workflow.getState().workflowReq.get('params').toJS();
	var dialog = new window.top.Dialog();
	dialog.currentWindow = window;
	const ismonitor = wfmonitor ? "1" : "0";
	var url = "/workflow/request/RequestModifyLogView.jsp?f_weaver_belongto_userid=" + f_weaver_belongto_userid + "&f_weaver_belongto_usertype=0&requestid=" + requestid + "&nodeid=" + nodeid + "&isAll=0&ismonitor=" + ismonitor + "&urger=0";
	dialog.Title = "表单日志";
	dialog.Width = 1000;
	dialog.Height = 550;
	dialog.Drag = true;
	dialog.maxiumnable = true;
	dialog.URL = url;
	dialog.show();
}


//流程删除
export const doDelete = () => { window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doDeleteE9()) }


//
export const doSubmitDirect = (subfun) => {
	const ifchangstatus  = window.store_e9_workflow.getState().workflowReq.getIn(['submitParams','ifchangstatus']);
	const { hasback, hasnoback,lastnodeid } = window.store_e9_workflow.getState().workflowReq.get('rightMenu').toJS();
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.updateSubmitParams({SubmitToNodeid:lastnodeid}));
	if("" != ifchangstatus && ("1" == hasback || ("" == hasback && "" == hasnoback))) {
		if("Affirmance" == subfun) {
			doAffirmanceBack();
		} else {
			doSubmitBack();
		}
	} else {
		if("Affirmance" == subfun) {
			doAffirmanceNoBack();
		} else {
			doSubmitNoBack();
		}
	}
}

//转办需反馈
export const doReviewback3 = (resourceid) => {
	const userSupportEdit = window.store_e9_workflow.getState().workflowReq.getIn(['params','userSupportEdit']);
	if(userSupportEdit){
			const params = {showForward: true,forwardflag: "3",needwfback: "1"};
			window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setShowForward(params));
	}else{
		doLocationHref(resourceid,3);
	}

}

//转办
export const doReview3 = (resourceid) =>{
	const userSupportEdit = window.store_e9_workflow.getState().workflowReq.getIn(['params','userSupportEdit']);
	if(userSupportEdit){
		const params = {showForward: true,forwardflag: "3"};
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setShowForward(params));
	}else{
		doLocationHref(resourceid,3);
	}
}

//意见征询
export const doReview2 = (resourceid) =>{
	const userSupportEdit = window.store_e9_workflow.getState().workflowReq.getIn(['params','userSupportEdit']);
	if(userSupportEdit){
		const params = {showForward: true,forwardflag: "2"};
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setShowForward(params));
	}else{
		doLocationHref(resourceid,2);
	}
}

const doLocationHref = (resourceid, forwardflag,needwfback) => {
	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const requestname = window.store_e9_workflow.getState().workflowReqForm.getIn(['mainData','field-1', 'value']);

	const id = params.get('requestid');
	const f_weaver_belongto_userid = params.get('f_weaver_belongto_userid');
	const workflowRequestLogId = '';
	
	const  _url = "/workflow/request/Remark.jsp?f_weaver_belongto_userid=" + f_weaver_belongto_userid + "&f_weaver_belongto_usertype=0&requestid=" + id + "&workflowRequestLogId=" + workflowRequestLogId +"&forwardflag=" + forwardflag + "&requestname=" + requestname+"&resourceid="+resourceid+"&needwfback="+needwfback;
	let dialogtitle = '流程转发';
	if(forwardflag == 2){
		dialogtitle = '意见征询';
	}else if(forwardflag == 3){
		dialogtitle = '流程转办';
	}
	try {
		openDialog(dialogtitle,_url);
	} catch(e) {
		var forwardurl = "/workflow/request/Remark.jsp?f_weaver_belongto_userid=" + f_weaver_belongto_userid + "&f_weaver_belongto_usertype=0&requestid=" + id + "&forwardflag=" + forwardflag + param + "&workflowRequestLogId=" + workflowRequestLogId;
		openFullWindowHaveBar(forwardurl);
	}
}
window.doLocationHref = doLocationHref;

//转办不需反馈
export const doReviewnoback3 = (resourceid) =>{
	const userSupportEdit = window.store_e9_workflow.getState().workflowReq.getIn(['params','userSupportEdit']);
	if(userSupportEdit){
		const params = {showForward: true,forwardflag: "3",needwfback:"0"};
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setShowForward(params));
	}else{
		doLocationHref(resourceid,3,0);
	}
}

//超时设置 
export const onNewOverTime = () =>{
	const params = window.store_e9_workflow.getState().workflowReq.get('params').toJS();
	const {f_weaver_belongto_userid,workflowid,nodeid,requestid,formid,billid} = params;
	var redirectUrl =  "/workflow/request/OverTimeSetByNodeUser.jsp?f_weaver_belongto_userid="+f_weaver_belongto_userid+"&f_weaver_belongto_usertype=0&workflowid="+workflowid+"&nodeid="+nodeid+"&requestid="+requestid+"&formid="+formid+"&isbill=0&billid="+billid;
	var dialog = new window.top.Dialog();
    dialog.currentWindow = window;
    dialog.URL = redirectUrl;
	dialog.Title ="超时设置 ";
    dialog.Height = screen.availHeight/2;
    dialog.Width = screen.availWidth/2;
    dialog.Drag = true;
    dialog.show();
}


//新建流程
export const onNewRequest = (workflowid,requestid,agent) => {
	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const userid = params.get('f_weaver_belongto_userid');
	var redirectUrl =  "/workflow/request/CreateRequestForward.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype=0&workflowid="+workflowid+"&isagent="+agent+"&reqid="+requestid;
	openFullWindowHaveBar(redirectUrl);
}

//新建短信
export const onNewSms = (wfid, nodeid, reqid, menuid) => {
	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const userid = params.get('f_weaver_belongto_userid');
	const redirectUrl =  "/sms/SendRequestSms.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype=0&workflowid="+wfid+"&nodeid="+nodeid+"&reqid="+reqid + "&menuid=" + menuid;
	const width = screen.availWidth/2;
	const height = screen.availHeight/2;
	const top = height/2;
	const left = width/2;
	openUnFullWindow(redirectUrl,width,height,top,left);
}

//新建微信
export const onNewChats =(wfid, nodeid,reqid, menuid) =>{ 
	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const userid = params.get('f_weaver_belongto_userid');
	var redirectUrl =  "/wechat/sendWechat.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype=0&workflowid="+wfid+"&nodeid="+nodeid+"&reqid="+reqid+"&actionid=dialog&menuid=" + menuid;
	var dialog = new window.top.Dialog();
	dialog.currentWindow = window;
	dialog.Title = "新建微信 ";
	dialog.Width = 1000;
	dialog.Height = 350;
	dialog.Drag = true;
	dialog.maxiumnable = true;
	dialog.URL = redirectUrl;
	dialog.show();
} 

//关于版本
export const aboutVersion = (versionid) =>{ window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.aboutVersion(versionid)); }


//流转设定
export const doFreeWorkflow = () =>{
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doFreeWorkflow());
}

//触发子流程
export const triSubwf2 = (subwfid,workflowNames) =>{
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.triggerSubWf(subwfid,workflowNames));
}

export const doAffirmanceBack = () =>{
	const para = {actiontype:"requestOperation",src:"save",needwfback:"1","isaffirmance":"1"};
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.updateSubmitToNodeId());
	doBeforeSubmit(para);
}

export const doAffirmanceNoBack = () =>{
	const para = {actiontype:"requestOperation",src:"save",needwfback:"0","isaffirmance":"1"};
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.updateSubmitToNodeId());
	doBeforeSubmit(para);
}

//流程共享
export const doShare  = () =>{
	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const requestid  = params.get("requestid");
	const requestname = params.get("requestname");
	
	var returnjson = '[{"sharetype":"workflow","sharetitle":"'+requestname+'","objectname":"FW:CustomShareMsg","shareid":"'+requestid+'"}]';
    var url="/social/im/SocialHrmBrowserForShare.jsp?sharejson="+encodeURIComponent(returnjson);
    var diag =new window.top.Dialog();
    diag.currentWindow = window; 
    diag.Modal = true;
    diag.Drag=true;
    diag.Width =400;	
    diag.Height =500;
    diag.ShowButtonRow=false;
    diag.Title = "分享";
    diag.URL =url;
    diag.openerWin = window;
    diag.show();
    document.body.click();
}

//流程导入
export const doImportWorkflow =() =>{
	const params = window.store_e9_workflow.getState().workflowReq.get('params');
	const userid = params.get('f_weaver_belongto_userid');
	const formid = params.get('formid');
	const isbill = params.get('isbill');
	const workflowid = params.get('workflowid');
	const ismode = params.get('ismode');
	var dialog = new window.top.Dialog();
	dialog.currentWindow = window;
	var url = "/systeminfo/BrowserMain.jsp?url="+escape("/workflow/request/RequestImport.jsp?f_weaver_belongto_userid="+userid+"&f_weaver_belongto_usertype=0&formid="+formid+"&isbill="+isbill+"&workflowid="+workflowid+"&status=2&ismode="+ismode+"&isfromE9=1");
	var title = "导入流程";
	dialog.Width = 600;
	dialog.Height = 600;
	dialog.Title=title;
	dialog.Drag = true;
	dialog.maxiumnable = true;
	dialog.URL = url;
	dialog.callbackfun = function(paramobj, datas) {
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.requestImport(datas));					
	};
	dialog.show();
}
//流程编辑
export const doEdit = () =>{
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doEdit());
}

//明细导入
export const doImportDetail =() =>{
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.doImportDetail());
}

//收藏
export const openFavouriteBrowser = (flag)=> {  
	var pagename = window.store_e9_workflow.getState().workflowReq.getIn(["params","titlename"]);
	pagename = escape(jQuery.trim(pagename)); 
	var fav_querystring = "";
	var fav_uri = ""
	var __requestid = window.store_e9_workflow.getState().workflowReq.getIn(["params","requestid"]);
	if(__requestid == "-1"){
		const workflowid =  window.store_e9_workflow.getState().workflowReq.getIn(["params","workflowid"]);
		const isagent =  window.store_e9_workflow.getState().workflowReq.getIn(["params","isagent"]);
		const beagenter =window.store_e9_workflow.getState().workflowReq.getIn(["params","beagenter"])||0;
		fav_uri = "/workflow/request/AddRequest.jsp"; 
		fav_querystring = escape("workflowid="+workflowid+"&isagent="+isagent+"&beagenter="+beagenter);
	}else{
		fav_uri = "/workflow/request/ViewRequest.jsp";
		fav_querystring = escape("requestid="+__requestid+"&isrequest=0");
	}
	var dialogurl = '/systeminfo/BrowserMain.jsp?url=/favourite/FavouriteBrowser.jsp&fav_pagename='+pagename+'&fav_uri='+fav_uri+'&fav_querystring='+fav_querystring+'&mouldID=doc';
	var dialog = new window.top.Dialog();
	dialog.currentWindow = window;
	dialog.URL = dialogurl;
	dialog.Title = "收藏夹";
	dialog.Width = 550 ;
	dialog.Height = 600;
	dialog.Drag = true;
	dialog.show();
}

//
export const openDialog = (title, url) => {　
	var dlg = new window.top.Dialog();
	dlg.currentWindow = window;
	dlg.Model = false;　　　
	dlg.Width = 1060;
	dlg.Height = 500;　　　
	dlg.URL = url;　　　
	dlg.Title = title;
	dlg.maxiumnable = true;　　　
	dlg.show();
	window.dialog = dlg;
}


export const openProductSuggest =() =>{
		let url="/formmode/view/AddFormMode.jsp?mainid=0&modeId=125&formId=-290&type=1";
        let dialog = new window.top.Dialog();
        dialog.currentWindow = window;
        dialog.Width = 1200;
        dialog.Height = 600;
        dialog.Modal = true;
        dialog.Title = '改进建议';
        dialog.URL =url;
        dialog.isIframe=false;
        dialog.show();
        let hasinit  = false;
        let count = 1;
		try{
            var timer = setInterval(function(){
                var doc1 = $(window.top.document).find("iframe[id^=_DialogFrame_]");
                if(doc1 && doc1.length > 0){
                    var doc2 = $(doc1[0].contentWindow.document).find("iframe[name='tabcontentframe']");
                    if(doc2 && doc2.length > 0){
						var _tempdocument = doc2[0].contentWindow.document;
                        if(_tempdocument){
                            var hbbox  = $(_tempdocument).find('.hbBox');
                            if(hbbox && hbbox.length > 0){
                                hasinit = true;
                                hbbox.css("padding","10px 40px");
							}
                        }
					}
				}
				if(hasinit || count > 20){
                    clearInterval(timer);
				}
                count++;
            },20);
		}catch(e){}
}

//返回到待办
export const doBack = () => {
	window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setReqIsUnbeforeunload(false));
	window.location.href = "/spa/workflow/index.html#/main/workflow/listDoing";
}

export const doTd253574 = (resourceids) =>{
	const userSupportEdit = window.store_e9_workflow.getState().workflowReq.getIn(['params','userSupportEdit']);
	if(userSupportEdit){
		const params = {showForward: true,forwardflag: "1",forwardOperators:resourceids,feedbacksign:'4'};
		window.store_e9_workflow.dispatch(window.action_e9_workflow.WorkflowReqAction.setShowForward(params));
	}else{
		const requestid = window.store_e9_workflow.getState().workflowReq.getIn(['params', 'requestid']);
		const userid = window.store_e9_workflow.getState().workflowReq.getIn(['params', 'f_weaver_belongto_userid']);
		
		const forwardurl = "/workflow/request/Remark_253574.jsp?f_weaver_belongto_userid=" + userid + "&f_weaver_belongto_usertype=0&requestid=" + requestid+"&resourceids="+resourceids;
		let dlg = new window.top.Dialog(); // 定义Dialog对象
		dlg.Model = false;
		dlg.Width = 1060;
		dlg.Height = 500;
		dlg.URL = forwardurl;
		dlg.Title = SystemEnvLabel.getHtmlLabelName(24964);
		dlg.maxiumnable = true;
		dlg.show();
	}
}


