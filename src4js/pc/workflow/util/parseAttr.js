const cellBelRowSpan = {};

//判断单元格是否属于被行合并的单元格
export const judgeCellBelRowSpan = (symbol,rowid,colid,ecMap) =>{
    const key = `${symbol}_${rowid}_${colid}`;
    if(key in cellBelRowSpan)
        return cellBelRowSpan[key];
    let belong = false;
    for(let j=colid; j>=0&&!belong; j--) {
        for(let i=rowid-1; i>=0&&!belong; i--) {
            const cellObj = ecMap.get(i+","+j);
            const rowSpan = cellObj ? (cellObj.get("rowspan")||1) : 0;
            const colSpan = cellObj ? (cellObj.get("colspan")||1) : 0;
            if(rowSpan>1 && rowSpan>=(rowid-i+1) && colSpan>=(colid-j+1))   //单元格属于被上方行合并的单元格
                belong = true;
        }
    }
    cellBelRowSpan[key] = belong;   //缓存存储，降低布局很大时的遍历性能
    return belong;
}

//判断明细布局是否含有列隐藏
export const judgeDetailContainColHide = (colattrs) =>{
    let contain = false;
    colattrs && colattrs.map((v,k) =>{
        if(v && v.get("hide") === "y")
            contain = true;
    });
    return contain;
}

export const getCellAttr = (cellObj,colCusAttr,rowHeight) =>{
    let cellAttr = {};
    let tdStyle = {};
    let divStyle = {};
    let tdClass = "";
    const financial = cellObj && cellObj.get("financial");
    //财务格式所在单元格div增加高度100%
    if(financial && financial.indexOf("2-") > -1){
        tdStyle["height"] = rowHeight+"px";
        divStyle["height"] = "100%";
        divStyle["padding"] = "0px";
    }
    //列自定义属性
    if(colCusAttr && colCusAttr.get("hide")==="y")
        tdClass += " detail_hide_col";
    if(colCusAttr && colCusAttr.get("class"))
        tdClass += " "+colCusAttr.get("class");
    //单元格自定义属性(放最后可覆盖之前的CSS)
    const cusattrs = cellObj && cellObj.get("attrs");
    cusattrs && appendCusAttrObj(cellAttr, cusattrs, divStyle);
    
    cellAttr.tdStyle = tdStyle;
    cellAttr.tdClass = jQuery.trim(tdClass);
    cellAttr.divStyle = divStyle;
    return cellAttr;
}

export const getMcCellAttr = (cellObj) =>{
    let mcCellAttr = {};
    let spanStyle = {};
    //自定义属性
    const cusattrs = cellObj && cellObj.get("attrs");
    cusattrs && appendCusAttrObj(mcCellAttr, cusattrs, spanStyle);
    mcCellAttr.spanStyle = spanStyle;
    return mcCellAttr;
}

export const getRowAttr = (rowHeight, rowCusAttr) =>{
    let rowAttr = {};
    rowAttr.id = rowCusAttr ? rowCusAttr.get("id") : "";
    rowAttr.name = rowCusAttr ? rowCusAttr.get("name") : "";
    rowAttr.trClass = rowCusAttr ? rowCusAttr.get("class") : "";
    let trStyle = {};
    trStyle["height"] = rowHeight;
    if(rowCusAttr && rowCusAttr.get("hide")==="y")
        trStyle["display"] = "none";
    if(rowCusAttr && rowCusAttr.get("style"))
        appendCusAttrStyle(trStyle, rowCusAttr.get("style"));
    rowAttr.trStyle = trStyle;
    return rowAttr;
}

/*
const appendBorder = (cellObj, styleObj)=>{
    const eBorder = cellObj.get("eborder");
    const etype = cellObj && cellObj.get("etype");
    eBorder && eBorder.map((v,k)=>{
        const kind = v.get("kind");
        if(etype === "7" && (kind === "top" || kind === "bottom"))  //明细所在区域不解析上下边框
            return true;
        if(v.get("color"))
            styleObj["border-"+kind+"-color"] = v.get("color");
        const borderstyle = v.get("style") ? parseInt(v.get("style")) : 0;
        if(borderstyle === 0){
        }else if(borderstyle === 2){
            styleObj["border-"+kind+"-width"] = "2px";
            styleObj["border-"+kind+"-style"] = "solid";
        }else if(borderstyle === 3){
            styleObj["border-"+kind+"-width"] = "1px";
            styleObj["border-"+kind+"-style"] = "dashed";
        }else if(borderstyle === 5){
            styleObj["border-"+kind+"-width"] = "3px";
            styleObj["border-"+kind+"-style"] = "solid";
        }else if(borderstyle === 6){
            styleObj["border-"+kind+"-width"] = "3px";
            styleObj["border-"+kind+"-style"] = "double";
        }else if(borderstyle === 7){
            styleObj["border-"+kind+"-width"] = "1px";
            styleObj["border-"+kind+"-style"] = "dotted";
        }else if(borderstyle === 8){
            styleObj["border-"+kind+"-width"] = "2px";
            styleObj["border-"+kind+"-style"] = "dashed";
        }else{
            styleObj["border-"+kind+"-width"] = "1px";
            styleObj["border-"+kind+"-style"] = "solid";
        }
    })
    return styleObj;
}

const appendStyle = (cellObj, styleObj, innerStyleObj, isMc) => {
    const etype = cellObj && cellObj.get("etype");
    if(etype === "7" || etype === "12")
        return;
    //背景色
    if(cellObj.get("backgroundColor"))
        styleObj["backgroundColor"] = cellObj.get("backgroundColor");
    //单元格图片
    if(etype === "6"){
        styleObj["backgroundImage"] = "url('"+cellObj.get("field")+"') !important";
        styleObj["backgroundRepeat"] = "no-repeat !important";
    }
    const font = cellObj.get("font");
    if(font) {
        font.map((v,k)=>{
            if(v === "")
                return true;
            if(k === "text-align")
                styleObj["text-align"] = v;
            else if(k === "valign")
                styleObj["vertical-align"] = v;
            else if(k === "bold") {
                if(v === "true")
                    innerStyleObj["font-weight"] = "bold";
            }else if(k === "italic"){
                if(v === "true")
                    innerStyleObj["font-style"] = "italic";
            }else if(k === "underline"){
                if(v === "true")
                    innerStyleObj["text-decoration"] = "underline";
            }else if(k === "deleteline"){
                if(v === "true")
                    innerStyleObj["text-decoration"] = "line-through";
            }else if(k === "font-size") {
                innerStyleObj["font-size"] = v;
            }else if(k === "color") {
                innerStyleObj["color"] = v;
            }else if(k === "font-family") {
                innerStyleObj["font-family"] = v;
            }
        });
    }
    //默认样式
    if(!styleObj["text-align"])     styleObj["text-align"] = "left";
    if(!styleObj["vertical-align"])  styleObj["vertical-align"] = isMc?"middle":"top";
    if(!innerStyleObj["font-size"])  innerStyleObj["font-size"] = "9pt";
    if(!innerStyleObj["font-family"])    innerStyleObj["font-family"] = "Microsoft YaHei";
    //缩进
    let etxtindent = cellObj.get("etxtindent");
    if(etxtindent && etxtindent !== "0"){
        etxtindent = parseFloat(etxtindent)*8 + "px";
        if(styleObj["text-align"] === "left")
            styleObj["padding-left"] = etxtindent;
        else if(styleObj["text-align"] === "right")
            styleObj["padding-right"] = etxtindent;
    }
    innerStyleObj["word-break"] = "break-all";
    innerStyleObj["word-wrap"] = "break-word";
}
*/

const appendCusAttrObj = (obj, cusattrs, styleObj) =>{
    cusattrs && cusattrs.map((v,k) =>{
        if(k === "hide"){
            if(v === "y")
                styleObj["display"] = "none";
        }else if(k === "id"){
            obj.cusid = v;
        }else if(k === "name"){
            obj.cusname = v;
        }else if(k === "class"){
            obj.cusclass = v;
        }else if(k === "style"){
            appendCusAttrStyle(styleObj, v);
        }else{  //自定义属性加data-并且变小写
            obj[`data-${k.toLowerCase()}`] = v;
        }
    });
}

const appendCusAttrStyle = (styleObj, cusstylestr) => {
    let styleArr = cusstylestr.split(";");
    for(let i=0; i<styleArr.length; i++){
        let item = styleArr[i];
        let itemArr = item.split(":");
        if(itemArr.length >= 2)
            styleObj[itemArr[0]] = itemArr[1];
    }
    return styleObj;
}

//根据解析的自定义属性生成单元格属性
export const buildCellAttr = (tdCellAttr, className, style) =>{
    let cellAttr = {};
    if(className)
        cellAttr["className"] = className;
    if(style)
        cellAttr["style"] = style;
    if(!tdCellAttr)
        return cellAttr;
    for(let key in tdCellAttr){
        const value = tdCellAttr[key];
        if(key === "cusid")
            cellAttr["id"] = value;
        else if(key === "cusname")
            cellAttr["name"] = value;
        else if(key.length > 5 && key.substring(0,5) === "data-")     //自定义属性
            cellAttr[key] = value;
    }
    return cellAttr
}

//布局解析时生成行头
export const convertRowHeads = (rowHeads) =>{
    rowHeads = rowHeads.map((v,k)=>{
        return {id:k.substring(4), height:v}
    }).toArray();
    rowHeads = rowHeads.sort((a,b)=>{
        return parseInt(a.id)-parseInt(b.id);
    });
    return rowHeads;
}

//布局解析时生成列头并控制Table宽度
export const convertColHeads = (colHeads, symbol, tableStyle, allowXScroll, isSeniorSet) =>{
    if(typeof allowXScroll === "undefined")
        allowXScroll = false;
    let hasPercent = false;
    let hasAutoAdapt = false;
    let totalPx = 0;
    let totalPercent = 0;
    colHeads.map((v,k) =>{
        v = v==="0%" ? "*": v.toString();
        if(v.indexOf("*") > -1){
            hasAutoAdapt = true;
        }else if(v.indexOf("%") > -1){
            hasPercent = true;
            totalPercent += parseFloat(v.replace("%",""));
        }else{  //绝对宽度
            totalPx += parseFloat(v);
        }
    });
    if(!hasPercent && !hasAutoAdapt && (symbol === "main" || allowXScroll))
        tableStyle["width"] = totalPx+"px";
    else
        tableStyle["width"] = "100%";
    let needTransPercent = false;      //标签页及明细表不开启横向滚动条情况转百分比宽度
    if(!hasAutoAdapt && (symbol.indexOf("tab_") > -1 || (symbol.indexOf("detail_") > -1 && !allowXScroll)))
        needTransPercent = true;
    let validPercent = 100;
    if(symbol.indexOf("detail_") > -1 && !allowXScroll && !isSeniorSet)     //低级模式预留6%给序号列
        validPercent = 94;
    colHeads = colHeads.map((v,k)=>{
        let colWidth = v==="0%" ? "*": v.toString();
        if(needTransPercent && colWidth.indexOf("%") === -1){
            colWidth = (parseFloat(v)/totalPx) * (validPercent-totalPercent);
            colWidth = colWidth.toFixed(2)+"%";
        }
        return {id:k.substring(4), width:colWidth}
    }).toArray();
    colHeads = colHeads.sort((a,b)=>{
        return parseInt(a.id)-parseInt(b.id);
    });
    return colHeads;
}

//解析模板背景图、悬浮图
export const parseLayoutImage = (layout, tableStyle, trArr) =>{
    if(layout.has("backgroundImage")){   //背景图
        tableStyle["background-image"] = `url(${layout.get("backgroundImage")})`;
        tableStyle["background-repeat"] = "no-repeat";
    }
    if(layout.hasIn(["floatingObjectArray", "floatingObjects"])){   //悬浮图
        let imgArr = new Array();
        layout.getIn(["floatingObjectArray", "floatingObjects"]).map(imgInfo =>{
            const imgStyle = {width:imgInfo.get("width"), height:imgInfo.get("height"), top:imgInfo.get("y"), left:imgInfo.get("x")};
            imgArr.push(<img src={imgInfo.get("src")} className="floatImg" style={imgStyle} />);
        });
        trArr.push(<tr style={{height:"0px"}}><td colSpan={layout.get("colheads").size} className="floatImgTd">{imgArr}</td></tr>);
    }
}