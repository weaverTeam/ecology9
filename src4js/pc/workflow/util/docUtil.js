import objectAssign from 'object-assign'
//显示文档 、正文
/**
 * params：
 * docid: 文档id
 * requestid:请求id
 * type:0 公文，1 文档
 */
export const showDoc = (params) =>{
	workflow_form_doc_operation.showDoc(params);
}

//创建文档、正文
export const createDoc = (params) =>{
	//workflow_form_doc_operation.createDoc(params);
	//doBeforeSubmit({actiontype:"requestOperation",src:"save",isMultiDoc:params.fieldid,method:`docnew_${params.fieldid}`});
	//openFullWindowHaveBar(`/docs/docs/DocList.jsp?hasTab=1&workflowid=${workflowid}&docfileid=${params.fieldid}&iscome9=1`);
	const reqparams = window.store_e9_workflow.getState().workflowReq.get("params");
	if(reqparams){
		params.wfid  = reqparams.get("workflowid");
		params.f_weaver_belongto_userid = reqparams.get("f_weaver_belongto_userid");
		params.f_weaver_belongto_usertype = reqparams.get("f_weaver_belongto_usertype");
	}
	params.docfileid = params.fieldid;
	workflow_form_doc_operation.createDoc(params); 
}
