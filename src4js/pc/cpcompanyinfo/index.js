import Route from "react-router/lib/Route"

import Home from "./components/Home.js"
import Test from "./components/Test.js"

import "./css/icon.css"

import reducer from "./reducers/"
import * as TestAction from "./actions/test"

const cpcompanyinfoRoute = (
	<Route path="cpcompanyinfo" component={ Home }>
	    <Route name="test" path="test" component={ Test } />
  	</Route>
)

module.exports = {
	Route: cpcompanyinfoRoute,
	reducer,
	action: {
		TestAction
	}
}