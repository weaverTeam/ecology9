import * as types from '../constants/ActionTypes'

let initialState = {
	title: "证照测试",
	test: ""
};

export default function test(state = initialState, action) {
	switch(action.type) {
		case types.TEST:
			console.log("reduce ", action.data)
			return {...state, test: action.data};
		default:
			return state
	}
}