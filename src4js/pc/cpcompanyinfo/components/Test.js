import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as TestAction from '../actions/test'

import { WeaErrorPage, WeaTools } from 'ecCom'

class Test extends React.Component {
	static contextTypes = {
		router: PropTypes.routerShape
	}
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		//一些初始化请求
		const { actions } = this.props;
		actions.test("anruo")
	}
	render() {
		const { test } = this.props;
		console.log(test)
		return (
			<div>
				{test.name||""}
            </div>
		)
	}

}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

Test = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(Test);

// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	return { ...state.cpcompanyinfoTest }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(TestAction, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Test);