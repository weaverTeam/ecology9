import * as types from '../constants/ActionTypes'
import * as API_LIST from '../apis/totalBudget'
// import * as API_TABLE from '../apis/table'
import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.TOTALBUDGET_LOADING,
			loading
		});
	}
}


export const changeTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SET_TABKEY,data:value
		});
	}
}

//高级搜索是否显示
export const setShowSearchAd = bool => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SET_SHOW_SEARCHAD,data:bool
		});
	}
}

//modal是否显示
export const setModalVisible = bool => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SET_MODAL_VISIBLE,data:bool
		});
	}
}


//高级搜索展示列
export const totalBudgetInfo = (value) =>{
	return (dispatch, getState) =>{
		API_LIST.totalBudgetInfoP({}).then(data =>{
			dispatch({
			type: types.TOTALBUDGET_INFO,data:data
			});
		});
	}
}


//历史查询初始化页面
export const queryData = (value) =>{
	return (dispatch, getState) =>{
		API_LIST.queryDataP(value).then(data =>{
			// dispatch({
			// type: types.TOTALBUDGET_INFO,data:data
			// });
			dispatch(WeaTableAction.getDatas(data.datas, 1));
			dispatch({type:types.SAVE_SESSIONKEY, value: data.datas });
		});
	}
}

//查询报表
export const totalBudgetReport = (value) =>{
	return (dispatch, getState) =>{
		API_LIST.totalBudgetReportP(value).then(data =>{
			// dispatch({
			// type: types.TOTALBUDGET_REPORT,data:data
			// });
			dispatch(WeaTableAction.getDatas(data.datas, 1));
			dispatch({type:types.SAVE_SESSIONKEY, value: data.datas });
			
		});
	}
}

//表单内容存储到store
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		dispatch({type:types.SAVE_ORDER_FIELDS, value: value })
	}
}

//weatab右侧按钮显隐
export const setShowButtons = (value) => {
	return(dispatch, getState) => {
		dispatch({type:types.SET_SHOW_BUTTONS, value: value })
	}
}

//共享MODAL显隐
export const setShareModal = (value) => {
	return(dispatch, getState) => {
		dispatch({type:types.SET_SHARE_MODAL, value: value })
	}
}


//共享MODAL初始化页面
export const queryShareModalData = (value) =>{
	return (dispatch, getState) =>{
		API_LIST.queryShareModalDataP(value).then(data =>{
			dispatch(WeaTableAction.getDatas(data.datas, 1));
			dispatch({type:types.SAVE_SESSIONKEY_MODAL, value: data.datas }); 
		});
	}
}
