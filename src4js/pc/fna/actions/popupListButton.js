import * as API_LIST from '../apis/popupListButton'
import { WeaTable } from '../../coms/index'
import * as types from '../constants/ActionTypes'

const WeaTableAction = WeaTable.action;


//调整明细列表初始化
export const amountLogData = (value) => {
	return(dispatch,getState) =>{
		API_LIST.amountLogData(value).then(data =>{
			dispatch(WeaTableAction.getDatas(data.datas, 1));
			dispatch({type:types.SAVE_SESSIONKEY, value: data.datas });
		});
	}
}


//调整明细列表显隐
export const setModalVisible = bool =>{
	return (dispatch, getState) => {
		dispatch({
			type: types.SET_MODAL_VISIBLE,data:bool
		});
	}
}
