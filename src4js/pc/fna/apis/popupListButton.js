import { WeaTools } from 'ecCom'


export const amountLogData = params => {
	return WeaTools.callApi('/api/fna/workflow/amountLog', 'GET', params);
}