import { WeaTools } from 'ecCom'


export const getDatas = params => {
	return WeaTools.callApi('/', 'GET', params);
}


export const totalBudgetInfoP = params => {
    return WeaTools.callApi('/api/fna/reportInner/totalBudgetInfo', 'GET', params);
}


export const totalBudgetReportP = params => {
    return WeaTools.callApi('/api/fna/report/totalBudgetReport', 'GET', params);
}

export const queryDataP = params => {
    return WeaTools.callApi('/api/fna/rptHistory/queryData', 'GET', params);
}

export const queryShareModalDataP = params => {
    return WeaTools.callApi('/api/fna/rptHistory/shareData', 'GET', params);
}