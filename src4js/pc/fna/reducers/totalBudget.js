import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'

let initialState = {
	title: "预算总额表",
	loading: false,
	tabkey:1,
	showSearchAd:false,
	info:'',
	showModal:false,
	orderFields:{},
	showButtons:false,
	showShareModal:false,
	sessionkey:'',
	sessionkeymodal:'',
};
// let initialState = Immutable.fromJS(initState);

export default function totalBudget(state = initialState, action) {
	// console.log(action.value);
	switch(action.type) {
		case types.TOTALBUDGET_LOADING:
			// return state.merge({loading: action.loading});
			return {...state, loading: action.loading};
		case types.SET_TABKEY:
			// return state.merge({tabkey: action.data});
			return {...state, tabkey: action.data};
		case types.SET_SHOW_SEARCHAD:
			// return state.merge({showSearchAd: action.data});
			return {...state, showSearchAd: action.data};
		case types.SET_MODAL_VISIBLE:
			// return state.merge({showModal: action.data});
			return {...state, showModal: action.data};
		case types.TOTALBUDGET_INFO:
			// return state.merge({info: action.data.conditions});
			return {...state, info: action.data.conditions};
		case types.TOTALBUDGET_REPORT:
			// return state.merge({report: action.data.datas});
			return {...state, report: action.data.datas};
		case types.SAVE_ORDER_FIELDS:
			// return state.merge({orderFields: action.value});
			return {...state, orderFields: action.value};
		case types.SET_SHOW_BUTTONS:
			// return state.merge({showButtons: action.value});
			return {...state, showButtons: action.value};
		case types.SET_SHARE_MODAL:
			// return state.merge({showShareModal: action.value});
			return {...state, showShareModal: action.value};
		case types.SAVE_SESSIONKEY:
			// return state.merge({sessionkey: action.value});
			return {...state, sessionkey: action.value};
		case types.SAVE_SESSIONKEY_MODAL:
			// return state.merge({sessionkey: action.value});
			return {...state, sessionkeymodal: action.value};
		default:
			return state
	}
}