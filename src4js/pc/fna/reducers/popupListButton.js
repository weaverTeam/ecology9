import * as types from '../constants/ComponentTypes'
import Immutable from 'immutable'


let initialState = {
	showModal:false,
	sessionkey:"",
};


export default function popupListButton(state = initialState, action){

	switch(action.type){
		case types.SET_MODAL_VISIBLE:
			return {...state, showModal: action.data};
		case types.SAVE_SESSIONKEY:
			return {...state, sessionkey: action.value};
		default:
			return state
	}

}