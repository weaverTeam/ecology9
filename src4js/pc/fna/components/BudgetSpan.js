
class BudgetSpan extends React.Component{

	constructor(props){
		super(props);
	}

	componentDidMount(){
		
	}

	render(){
		const {available,occurred,approve} = this.props;
		return(
			<span>
				可用预算：{available}
				<br>
				<font color="red">已发生费用：{occurred}</font>
				<br>
				<font color="green">审批中费用:{approve}</font>
			</span>

		);
	}

}

export default BudgetSpan