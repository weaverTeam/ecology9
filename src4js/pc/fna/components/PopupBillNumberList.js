import {Modal, Button,Icon,Row,Col,Input} from 'antd';
import {WeaTable} from 'comsRedux';
const InputGroup = Input.Group;

export default class PopupBillNumberList extends React.Component{

	constructor(props) {
		super(props);
		this.state = { visible: false };
    }

    showModal = () => {
	    this.setState({
	    	visible: true,
	    });
	}

	handleCancel = (e) => {
    	this.setState({
      		visible: false,
    	});
  	}

  	handleInputChange = (e) => {
  		this.setState({
  			value: e.target.value,
  		});
  	}

  	handleFocusBlur = (e) => {
  		this.setState({
      		focus: e.target === document.activeElement,
    	});
  	}

 	

  	render(){
  		const {sessionkey} = this.props;
  		
  		return(
  			<div>
  				<InputGroup >
		          <Input value={this.state.value} onChange={this.handleInputChange}
		            onFocus={this.handleFocusBlur} onBlur={this.handleFocusBlur} onPressEnter={this.handleSearch}
		          />
		          <div className="ant-input-group-wrap">
		            <Button icon="search"  onClick={this.showModal} />
		          </div>
		        </InputGroup>
		        <Modal ref="modal"
    	          title="请选择"
    	          visible={this.state.visible}
                  width="626"
                  onCancel={this.handleCancel}
                  footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.handleCancel} >取 消</Button>,
                  ]}
    	        >

                 <Row className="wea-title" style={{borderBottom:'1px solid #e9e9e9',height:'44px'}} type="flex" justify="space-around" align="middle">
                    <Col span="16" >
                    	<div className="icon-circle-base " style={{background:"#55D2D4"}}>
                            <i className='icon-portal-workflow' /> 
                        </div>   
                        <span >借款明细</span>
                    </Col>
                    <Col span="8" style={{textAlign:'right'}}>
                        {/*下拉选项*/}
                    </Col>
                 </Row>
    		        <WeaTable 
                        hasOrder={true}
                        needScroll={true}
                        sessionkey={sessionkey}
                    />
                    
    	        </Modal>

  			</div>	
  		);
  	}	
}