import PopupListButton from './PopupListButton'
import PopupEditButton from './PopupEditButton'
import WindowButton from './WindowButton'
import PopupBillNumberList from './PopupBillNumberList'


export default class FnaComponents extends React.Component{

	constructor(props) {
		super(props);
		
    }


    render(){
    	const {type,form} = this.props;

    	switch(type){
			case 1://按钮弹出列表
				return(
					<PopupListButton
	            	sessionkey={sessionkeymodal2}
	            	/>
				);
			case 2://按钮弹出编辑框
				return(
					<PopupEditButton
	            	fieldId="9663"
	            	loanAmount="100"
					readonly="readonly"
	            	form={this.props.form}
	            	/>
				);
			case 3://按钮弹出新页面
				return(
					<WindowButton
	            	detailRecordId="19"
	            	requestid="414"
	            	/>
				);
			case 5://单内序号列表
				return(
					<PopupBillNumberList	
	                sessionkey={sessionkeymodal3}
					/>
				);
			default:
				return ();
		}
 	
    }

}