import {Modal, Button, input, Row, Col, Collapse, Form, Input} from 'antd';
const Panel = Collapse.Panel;
const FormItem = Form.Item;


export default class PopupEditButton extends React.Component{

	constructor(props) {
		super(props);
		this.state = { visible: false };
    }

    componentDidMount(){
    	//
    }

    showModal = () => {
	    this.setState({
	    	visible: true,
	    });
	}
    

    handleCancel = (e) => {
    	this.setState({
      		visible: false,
    	});
  	}

    /*checkPass(rule, value, callback) {
        const form = this.props.form;
        this.getPassStrenth(value, 'pass');

        if (form.getFieldValue('pass')) {
          form.validateFields(['rePass'], { force: true });
        }

        callback();
    },*/


    handleSubmit = (e) => {
        e.preventDefault();
        const {form} = this.props;
        form.validateFields((errors, values) =>{
            if (!!errors) {
                console.log('Errors in form!!!');
                return;
            }
            console.log('Submit!!!');
            console.log(form.getFieldsValue());
           
            let changedAmount = form.getFieldValue("amount");
            //console.log(form.getFieldValue("amount"));
            console.log(this.props.loanAmount);
            this.props.loanAmount = changedAmount;
            console.log(this.props.loanAmount);
            this.handleCancel();
        });
    }


    showInfo = (e) => {
        console.log(this.refs);
        //console.log(this.refs["button"]);
        //console.log(this._Row);
        //console.log(this.refs["row_title"]);
        //console.log(this.refs["row_title"].props.style.height);
    }

    checkNumber = (rule, value, callback) => {

        callback();
        //callback(new Error('2222222'));
        /*if(true){
            callback(new Error('2222222'));
        }else{
            callback();
        }*/
    }


  	render(){
  		//workflowid   objId   amountBorrowBefore   amountBorrowAfter  memo1  requestid
    	const {fieldId,loanAmount,readonly} = this.props;
    	const {form} = this.props;
        //console.log(form);
        const { getFieldProps, getFieldError, isFieldValidating } = form;

        const textareaProps = getFieldProps('adjustInfo', {
            rules: [
                { required: true, message: '请填写调整说明' },
            ],
        }); 


        const amountProps = getFieldProps('amount', {
            validate: [{
                rules: [
                  { required: true , message: '请输入正确的金额' },
                  { validator: this.checkNumber },
                ],
                trigger: ['onBlur', 'onChange'],
            }],
        });


        return(
        <div>
            {readonly ?
                <input id={fieldId} name={fieldId} value={loanAmount} readonly onClick={this.showModal}/>
                :
                <input id={fieldId} name={fieldId} value={loanAmount} />
            }

            <Modal ref="modal"
              title="调整明细"
              visible={this.state.visible}
              width="626"
              onCancel={this.handleCancel}
              footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.handleCancel} >取 消</Button>,
                    
              ]}
            >

                <Row ref={Row =>{this._Row = Row}} className="wea-title" style={{borderBottom:'1px solid #e9e9e9',height:'44px'}} type="flex" justify="space-around" align="middle">
                    <Col span="16" >
                        <div className="icon-circle-base " style={{background:"#55D2D4"}}>
                            <i className='icon-portal-workflow' /> 
                        </div>
                        <span>调整明细</span> 
                    </Col>
        
                    <Col span="8" style={{textAlign:'right'}}>
                        <Button type="primary" style={{marginRight:10}} onClick={this.handleSubmit} >确定</Button>
                    </Col>

                </Row>

                <Row>
                    <Col span="24">
                        <Collapse defaultActiveKey={['1']} >
                            <Panel header="基本信息" key="1">
                            <Form horizontal form={form}>
                                <FormItem
                                  id="control-input"
                                  label="调整前金额"
                                  labelCol={{ span: 6 }}
                                  wrapperCol={{ span: 14 }}
                                >
                                <Input id="control-input"  value={loanAmount} readonly />
                                </FormItem>

                                <FormItem
                                  id="control-input"
                                  label="调整后金额"
                                  labelCol={{ span: 6 }}
                                  wrapperCol={{ span: 14 }}
                                >
                                <Input  id="amount"  {...amountProps} />
                                </FormItem>

                                <FormItem
                                  id="control-input"
                                  label="调整说明"
                                  labelCol={{ span: 6 }}
                                  wrapperCol={{ span: 14 }}
                                >
                                <Input id="adjustInfo" {...textareaProps}  />
                                </FormItem>
                            </Form>
                            </Panel>
   
                        </Collapse>
                    </Col>

                </Row>


            </Modal>

        </div>
        );       
    	
    }   

}

