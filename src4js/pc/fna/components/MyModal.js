import {Row,Col,Input,Tabs,Button,Alert,Spin,Icon,Form,Modal} from 'antd';
const InputGroup = Input.Group;

export default class MyModal extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            showDrop: props.showDrop ? props.showDrop : false,

        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.showDrop !== nextProps.showDrop) {
            this.setState({showDrop: nextProps.showDrop});
        }
    }

	render(){
		// console.log(this);
		// console.log(this.props.title);
		const {title,persontitle,needTigger,items,showModal,footer,iconBgcolor,icon} = this.props;
		const {showDrop} = this.state;
		return(
				<Modal
			        title={title}
			        wrapClassName="vertical-center-modal fnaModal-vertical-center"
			        visible={showModal}
          			footer={footer}
          			className={`fnaModal`}
		        >
		        {iconBgcolor ?
		                		<div className="icon-circle-base fna-modal" style={{background:iconBgcolor ? iconBgcolor : ""}}>
					   				{icon}
								</div>
								:
								<span style={{verticalAlign:'middle',marginRight:10}}>
						   			{icon}
								</span>
	                    	}
							

		        <Row className="wea-title" style={{borderBottom:'1px solid #e9e9e9',marginBottom:'20'}}>
                    <Col span="20" style={{height:'32px'}}>
                        <div>{persontitle}</div>
                    </Col>
                    {needTigger &&
	                    <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
	                        <Icon type={showDrop ? 'up' : 'down'} onClick={()=>this.setState({showDrop:!showDrop})}/>
	                    </Col>
                    }
                </Row>

					{items?
                        items.map((obj)=>{
                            return (
                                <Row className="wea-content" style={showDrop?{marginBottom:'10px'}:{height:0,overflow:'hidden'}}>
									<Col span="8">
							        	<p style={{textAlign:'right',lineHeight:'32px',paddingRight:'20px'}}>{obj.a}</p>
							        </Col>
							        <Col span="16">
							        	<InputGroup size="large">
									        <Input defaultValue={obj.b} />
									     </InputGroup>
									</Col>
								</Row>
                            )
                        })
                        :''
                    }

       			</Modal>
		)
	}

}

