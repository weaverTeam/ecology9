import {Modal, Button,Icon,Row,Col} from 'antd';
import {WeaTable} from 'comsRedux';

export default class PopupListButton extends React.Component{

	constructor(props) {
		super(props);
		this.state = { visible: false };
    }

    componentDidMount(){
    	//先空表单，然后查列表数据，再渲染
    	//const {workflowid, detailRecordId, requestid, actions} = this.props;
    	//console.log(actions);
        //actions.amountLogData({workflowid:workflowid,detailRecordId:detailRecordId,requestid:requestid});//查询列表
    }

    showModal = () => {
	    this.setState({
	    	visible: true,
	    });
	}
    

    handleCancel = (e) => {
    	this.setState({
      		visible: false,
    	});
  	}
    

    render(){
        
    	const {sessionkey} = this.props;

    	return(
            <div>
    		   	<Button onClick={this.showModal}>查看金额修改历史</Button>
    	        <Modal ref="modal"
    	          title="日志"
    	          visible={this.state.visible}
                  width="626"
                  onCancel={this.handleCancel}
                  footer={[
                    <Button key="back" type="ghost" size="large" onClick={this.handleCancel} >取 消</Button>,
                  ]}
    	        >

                 <Row className="wea-title" style={{borderBottom:'1px solid #e9e9e9',height:'44px'}} type="flex" justify="space-around" align="middle">
                    <Col span="16" >
                        <div className="icon-circle-base " style={{background:"#55D2D4"}}>
                            <i className='icon-portal-workflow' /> 
                        </div>
                        <span >调整明细</span>
                    </Col>
                    <Col span="8" style={{textAlign:'right'}}>
                        {/*下拉选项*/}
                    </Col>
                 </Row>
    		        <WeaTable 
                        hasOrder={true}
                        needScroll={true}
                        sessionkey={sessionkey}
                    />
                    
    	        </Modal>
		    </div>
    	);

    }   

}

