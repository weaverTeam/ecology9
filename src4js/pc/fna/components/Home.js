class Home extends React.Component {
    render() {
        return (
            <div className="testdiv" style={{height:"100%",overflow:'hidden'}}>
            	{this.props.children}
            </div>
        )
    }
}

export default Home