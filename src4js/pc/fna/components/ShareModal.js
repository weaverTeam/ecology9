import {Row,Col,Input,Tabs,Button,Alert,Spin,Icon,Form,Modal} from 'antd';
import {WeaTable} from 'comsRedux'
const InputGroup = Input.Group;


export default class MyModal extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            showDrop: props.showDrop ? props.showDrop : false,

        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.showDrop !== nextProps.showDrop) {
            this.setState({showDrop: nextProps.showDrop});
        }
    }

	render(){
		// console.log(this);
		// console.log(this.props);
		const {title,persontitle,needTigger,items,showModal,footer,iconBgcolor,icon,sessionkey} = this.props;
		const {showDrop} = this.state;
		return(
				<Modal
			        title={title}
			        wrapClassName="vertical-center-modal zzfModal"
			        visible={showModal}
          			footer={footer}
          			className={`fnaModal shareModal`}
          			style={{height:'600px'}}
		        >
		        {iconBgcolor ?
		                		<div className="icon-circle-base fna-modal" style={{background:iconBgcolor ? iconBgcolor : ""}}>
					   				{icon}
								</div>
								:
								<span style={{verticalAlign:'middle',marginRight:10}}>
						   			{icon}
								</span>
	                    	}
							

		        <Row className="wea-title" style={{borderBottom:'1px solid #e9e9e9',marginBottom:'20'}}>
                    <Col span="16" style={{height:'32px'}}>
                        <div>{persontitle}</div>
                    </Col>
                    <Col span="8" style={{height:'32px',textAlign:'right'}}>
                        <Button type="primary">添加共享</Button>
                        <Button type="ghost">删除共享</Button>
                    </Col>
                </Row>

 				<WeaTable 
                    hasOrder={true}
                    needScroll={true}
                    sessionkey={sessionkey}

                />

       			</Modal>
		)
	}

}

