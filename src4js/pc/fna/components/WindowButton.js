import {Button, Modal} from 'antd';

class WindowButton extends React.Component{


	constructor(props) {
		super(props);
		this.state = { visible: false }
    }

	handle = () => {
		const {detailRecordId,requestid} = this.props;
		const w = window.open('about:blank');
		let url = "/workflow/search/WFCustomSearchMiddleHandler.jsp?offical=&officalType=-1&fromleftmenu=-1&typeid=&workflowid="+
			"&_isFromShowRelatedProcess=1"+
			"&_borrowDetailRecordId="+detailRecordId+
			"&_borrowRequestid="+requestid;

		w.location.href = url;
	}

    render(){
    	return(
		   	<Button onClick={this.handle}>查 看</Button>
    	);

    }   
}

export default WindowButton