/**
 * 财务报表
 */

//预算总额表
export const TOTALBUDGET_LOADING = 'totalbudget_loading'

export const SET_TABKEY = 'set_tabkay' //tab切换

export const SET_SHOW_SEARCHAD = 'set_show_searchad' //高级搜索受控

export const SET_MODAL_VISIBLE = 'set_modal_visible' //modal受控

export const TOTALBUDGET_INFO = 'totalbudget_info' //

export const TOTALBUDGET_REPORT = 'totalbudget_report' //

export const SAVE_ORDER_FIELDS = 'saveorderfields' //高级搜索数据

export const SET_SHOW_BUTTONS = 'setshowbuttons' //weatab右侧按钮显隐

export const SET_SHARE_MODAL = 'setsharemodal' //共享MODAL显隐

export const SAVE_SESSIONKEY = 'savesessionkey' //保存sessionkey

export const SAVE_SESSIONKEY_MODAL = 'savesessionkeymodal' //保存sessionkeyMODAL

