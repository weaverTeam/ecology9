import * as types from '../constants/ActionTypes';
import Immutable from 'immutable';
import isEmpty from 'lodash/isEmpty';
import { WeaTools } from 'ecCom';
import * as Utils from '../utils';

let initState = {
    title: "在线人员",
    conditions:[],
    fields: {},
    dataKey:'',
    showSearchAd:false,
    searchParamsAd: {},
    leftTree: WeaTools.ls.getJSONObj('hrmSearchLeftTree')||[],
};

let initialState = Immutable.fromJS(initState);
export default function online(state = initialState, action) {
    switch(action.type) {
        /*搜索条件浏览框*/
        case types.HRM_ONLINE_SEARCH_CONDITIONS:
            const condition = action.condition;
            let params = Utils.getParamsByConditions(condition);
            return state.merge({condition, searchParamsAd: params});
        /*保存表单域*/
        case types.HRM_ONLINE_SEARCH_SAVE_FIELDS:
            // console.log('action.fields',action.fields);
            return state.merge({
                fields:action.fields,
                searchParamsAd: Utils.translateFormFields(action.fields)
            });
        /*Table数据*/
        case types.HRM_ONLINE_SEARCH_RESULT:
            return state.merge({dataKey: action.value});
        /*高级搜索显隐*/
        case types.HRM_ONLINE_SEARCH_SET_SHOW_SEARCHAD:
            return state.merge({showSearchAd: action.value});
        default:
            return state
    }
}
