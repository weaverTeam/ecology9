import * as types from '../../constants/ActionTypes';
import Immutable from 'immutable';
import isEmpty from 'lodash/isEmpty';
import * as Utils from '../../utils';

let initState = {
    fields: {},
    fieldParams: {},
};

let initialState = Immutable.fromJS(initState);
export default function myCard(state = initialState, action) {
    switch(action.type) {
        /*Tabs*/
        case types.HRM_CARD_SAVE_FIELDS:
            return state.merge({
                fields: action.value,
                fieldParams: Utils.translateFormFields(action.value)
            });
        /*activeKey*/
        case types.HRM_CARD_SAVE_PARAMS:
            return state.merge({fieldParams: action.value});
        default:
            return state
    }
}
