import * as types from '../constants/ActionTypes';
import Immutable from 'immutable';

let initState = {
    title: "的下属",
    dataKey:'',
    quickSearch: '',
};

let initialState = Immutable.fromJS(initState);
export default function online(state = initialState, action) {
    switch(action.type) {
        /*Table数据*/
        case types.HRM_UNDERLING_SEARCH_RESULT:
            return state.merge({dataKey: action.value});
        case types.HRM_UNDERLING_SAVE_QUICK_SEARCH:
            return state.merge({quickSearch: action.value});
        default:
            return state
    }
}
