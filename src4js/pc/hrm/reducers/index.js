import hrmSearch from "./search"
import hrmAdd from "./add"
import hrmMyCard from "./myCard/myCard-basicInfo"
import hrmMyCardPersonInfo from "./myCard/personalInfo"
import hrmOnline from "./online"
import hrmUnderling from "./underling"
import hrmMyCardCore from "./myCard/index"

export default {
    hrmSearch,
    hrmAdd,
    hrmMyCard,
    hrmMyCardPersonInfo,
    hrmOnline,
    hrmUnderling,
    hrmMyCardCore,
}