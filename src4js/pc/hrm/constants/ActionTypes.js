/*查询人员*/
export const HRM_SEARCH_Conditions = 'hrm_search_conditions';//查询人员条件
export const HRM_SEARCH_SAVE_FIELDS = 'hrm_search_save_fields'; //查询人员表单
export const HRM_SEARCH_SEARCH_RESULT = 'hrm_search_search_result' ;//查询人员查询结果
export const HRM_SEARCH_LOADING = 'hrm_search_loading'; //加载状态
export const HRM_SEARCH_UPDATE_DISPLAY_TABLE = 'hrm_search_update_display_table'; //重置数据
export const HRM_SEARCH_SET_SHOW_SEARCHAD = 'hrm_search_set_show_searchad'; //高级搜索受控
export const HRM_SEARCH_SET_SELECTED_TREEKEYS = 'hrm_search_set_selected_treekeys'; //树选中
export const HRM_SEARCH_SET_AdSEARCH_PARAMS = 'hrm_search_set_adsearch_params' ;//搜索条件
export const HRM_SEARCH_RIGHT_MENU_CONFIG = 'hrm_search_right_menu_config' ;//结果页面右键菜单
/*新建人员*/
export const HRM_ADD_LOADING = 'hrm_add_loading';// 新建人员
export const HRM_SEARCH_SAVE_TEMPLATE = 'hrm_search_save_template';//存为模板
export const HRM_SEARCH_TEMPLATE_ID = 'hrm_search_template_id';//存为模板 response id
export const HRM_SEARCH_TEMPLATE_SELECT = 'hrm_search_template_select';//模板的下拉菜单
export const HRM_SEARCH_TEMPLATE_SELECTEDKEY = 'hrm_search_template_selectedkey';//模板的下拉菜单选中key
export const HRM_SEARCH_CUSTOM_QUERYCONDITION = 'hrm_search_custom_querycondition';//查询条件定制
export const HRM_SEARCH_SAVE_CUSTOM_QUERYCONDITION_FIELDS = 'hrm_search_save_custom_querycondition_fields';//查询条件定制保存表单
/*人员卡片*/
export const HRM_MYCARD_TABS = 'hrm_mycard_tabs';// tabs
export const HRM_MYCARD_ACTIVEKEY = 'hrm_mycard_activekey';//activekey
export const HRM_MYCARD_DATAS = 'hrm_mycard_datas'//卡片数据
export const HRM_MYCARD_ITEM = 'hrm_mycard_item'//各模块数据
export const HRM_MYCARD_SHOWSQR = 'hrm_mycard_showsqr'//二维码show
export const HRM_MYCARD_SHOWBIGIMG = 'hrm_mycard_showbigimg'//放大照片
export const HRM_MYCARD_SHOWACCINFO = 'hrm_mycard_showAccountInfo';
export const HRM_MYCARD_TOPBUTTON_SWITCH = 'hrm_mycard_topbutton_switch';
export const HRM_CARD_SAVE_FIELDS = 'hrm_card_save_fields'; // 保存form表单
export const HRM_CARD_SAVE_PARAMS = 'hrm_card_save_params'; // 保存form表单参数

/*在线人员*/
export const HRM_ONLINE_SEARCH_CONDITIONS = 'hrm_online_search_conditions';// 搜索条件
export const HRM_ONLINE_SEARCH_RESULT = 'hrm_online_search_result';// 搜索
export const HRM_ONLINE_SEARCH_SAVE_FIELDS = 'hrm_online_search_save_fields';// 表单域值
export const HRM_ONLINE_SEARCH_SET_SHOW_SEARCHAD = 'hrm_online_search_set_show_searchad';// 高级搜索显隐

/*我的下属*/
export const HRM_UNDERLING_SEARCH_RESULT = 'hrm_underling_search_result';// 列表
export const HRM_UNDERLING_SAVE_QUICK_SEARCH = 'hrm_underling_save_quick_search';// 快捷搜索字段

/*新建人员*/
export const HRM_ADD_SEARCH_CONDITIONS = 'hrm_add_search_conditions';// 搜索条件
export const HRM_ADD_SEARCH_RESULT = 'hrm_add_search_result';// 搜索
export const HRM_ADD_SEARCH_SAVE_FIELDS = 'hrm_add_search_save_fields';// 表单域值
export const HRM_ADD_SEARCH_SET_SHOW_SEARCHAD = 'hrm_add_search_set_show_searchad';// 高级搜索显隐