import React from 'react'
import { render } from 'react-dom'
import {combineReducers,applyMiddleware,createStore } from 'redux';
import { Provider } from 'react-redux'
import { createHashHistory } from 'history'
import thunkMiddleware from 'redux-thunk/lib/index'
import {Route, IndexRedirect, Router , hashHistory} from 'react-router';
import {WeaErrorPage} from 'ecCom';
import objectAssign from 'object-assign'
import {routerReducer} from 'react-router-redux/lib/reducer'
import useRouterHistory from 'react-router/lib/useRouterHistory'
import { comsReducer } from 'comsRedux'
import syncHistoryWithStore from 'react-router-redux/lib/sync'

import Hrm from 'weaHrm';

const HrmRoute = Hrm.Route;
const HrmReducer = Hrm.reducer;

let reducers = objectAssign({},HrmReducer,{routing:routerReducer});
const reducer = combineReducers({...reducers, ...comsReducer});
const store = createStore(reducer, applyMiddleware(
    thunkMiddleware
));
const browserHistory  = useRouterHistory(createHashHistory)({
    queryKey: '_key',
    basename: '/'
});
const history = syncHistoryWithStore(browserHistory, store);
class Error extends React.Component {
    render() {
        return (
            <WeaErrorPage msg="对不起，无法找到该页面！" />
        )
    }
}

render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/">
                <IndexRedirect to="error"/>
                <Route path="main">
                    {HrmRoute}
                </Route>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('container')
);