import isEmpty from 'lodash/isEmpty';
import {message} from 'antd';
import {Modal} from 'antd';

function translateFormFields(fields){
    let params = {};
    if(fields && !isEmpty(fields)){
        for (let key in fields) {
            params[fields[key].name] = fields[key].value
        }
    }
    return params
}
function createQRCode(data){
    //生成二维码
    var    txt = "BEGIN:VCARD \n"+
    "VERSION:3.0 \n"+
    "N:"+data.lastname+" \n"+
    "TEL;CELL;VOICE:"+data.mobile+"\n"+
    "TEL;WORK;VOICE:"+data.telephone+"\n"+
    "EMAIL:"+data.email+" \n"+
    "TITLE:"+data.jobtitle+" \n"+
    "ROLE:"+data.department+"\n"+
    "ADR;WORK:"+data.locationname+" \n"+
    "END:VCARD";
    jQuery('.hrm-my-card-basicInfo-sqr').qrcode({
        render: 'canvas',
        background:"#ffffff",
        foreground:"#000000",
        msize:0.3,
        size:120,
        mode:0,
        label:data.lastname,
        image:"/images/hrm/weixin_wev8.png",
        text: utf16to8(txt)
    });
    function utf16to8(str) {
      var out, i, len, c;
      out = "";
      len = str.length;
      for(i = 0; i < len; i++) {
      c = str.charCodeAt(i);
      if ((c >= 0x0001) && (c <= 0x007F)) {
          out += str.charAt(i);
      } else if (c > 0x07FF) {
          out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
          out += String.fromCharCode(0x80 | ((c >>  6) & 0x3F));
          out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
      } else {
          out += String.fromCharCode(0xC0 | ((c >>  6) & 0x1F));
          out += String.fromCharCode(0x80 | ((c >>  0) & 0x3F));
      }
      }
      return out;
    }
}
/**
图片放大
*/
function imgZoom(dom, parentSelector){
    let imgList = jQuery(parentSelector + " img",dom);
    imgList.each((index,img) => {
        let $img = $(img);
        if ($img.attr("resize")) {
            return;
        }
        if ($img.parents(".ui-coomixPic").length > 0) {
            return;
        }
        $img.css({"max-width": "800px", "max-height" : "800px"}).attr("resize", 1).coomixPic({
            path:'/blog/js/weaverImgZoom/images',
            preload:true,
            blur: true,
            left: '向左转',
            right: '向右转',
            source: '查看原图',
            hide: '收起'
        });
    });
};
const getSelectDefaultValue = (options) => {
    let value = '';
    options.forEach((option) => {
        if(option.selected){
            value = option.key;
        }
    })
    return value;
}
function getParamsByConditions(conditions) {
    let params = {};
    !isEmpty(conditions) && conditions.forEach((cd) => {
        cd.items.forEach((c)=>{
            if(c) {
                const conditionType = c.conditionType;
                if(conditionType == 'INPUT'){
                    if (c.value) params[c.domkey[0]] = c.value;
                }
                if(conditionType == 'DATEPICKER'){
                    if (c.value) params[c.domkey[0]] = c.value;
                }
                if(conditionType == 'TIMEPICKER'){
                    if (c.value) params[c.domkey[0]] = c.value;
                }
                if(conditionType == 'SELECT'){
                    let value = getSelectDefaultValue(c.options);
                    if (value) params[c.domkey[0]] = value;
                }
                if(conditionType == 'BROWSER'){
                    if (!isEmpty(c.browserConditionParam)) {
                        const replacesDatas = c.browserConditionParam.replaceDatas;
                        let values = [], value;
                        !isEmpty(replacesDatas) && replacesDatas.forEach((d) => {
                            values.push(d.id);
                        })
                        value = values.join(',');
                        if (value) params[c.domkey[0]] = value;
                    }
                }
                if(conditionType == 'DATE'){
                    if (!isEmpty(c.value) && isArray(c.domkey)) {
                        c.domkey.forEach(d => {
                            if (c.value[d]) {
                                params[d] = c.value[d];
                            }
                        })
                    }
                }
            }
        })
    })
    return params;
}

function formatFieldsByParams(params) {
    let res = {};
    if (!isEmpty(params)) {
        for (let key in params) {
            let obj = {name: key, value: params[key]};
            res[key] = obj;
        }
    }
    return res;
}

// function validate(conditions, params = {}) {
//     !isEmpty(conditions) && conditions.forEach((cd) => {
//         cd.items.forEach((c)=>{
//             if(c) {
//                 let domkey = c.domkey[0];
//                 let viewAttr = c.viewAttr;
//                 if (c.conditionType == 'BROWSER') viewAttr = c.browserConditionParam.viewAttr;
//                 if (viewAttr == '3' && !params[domkey]) {
//                     message.warning(`请填写${c.label}!`);
//                     return false;
//                 }
//             }
//         })
//     })
//     return true;
// }

function validate(conditions, params = {}) {
    let errorflag = false;
    !isEmpty(conditions) && conditions.forEach((cd) => {
        if(!errorflag){
            for (let key in cd.items) {
                let c = cd.items[key];
                if(c) {
                    let domkey = c.domkey[0];
                    let viewAttr = c.viewAttr;
                    if (c.conditionType == 'BROWSER') viewAttr = c.browserConditionParam.viewAttr;
                    if (viewAttr == '3' && !params[domkey]) {
                        //message.warning(`请填写${c.label}!`);
                        errorflag = true;
                        break;
                    }
                }
            } 
        }
    })
    if(errorflag){
        // Modal.error({
        //     title: '系统提示',
        //     content: '必要信息不完整！',
        //     okText: '确定',
        // });
        message.warning('必要信息不完整！');
    }
    return !errorflag;
}

export {
    translateFormFields,
    createQRCode,
    imgZoom,
    getParamsByConditions,
    formatFieldsByParams,
    validate,
}