import * as types from '../constants/ActionTypes';
import * as Add from '../apis/add';
import {WeaTable} from 'comsRedux'
const WeaTableAction = WeaTable.action;
import {WeaTools} from 'ecCom'
import isEmpty from 'lodash/isEmpty'

/*搜索条件*/
export const getHrmSearchCondition = () => {
    return (dispatch,getState) => {
        return Add.getSearchCondition().then((data) => {
            dispatch({type:types.HRM_ADD_SEARCH_CONDITIONS, condition:data.condition})
        })
    }
}

/*搜索*/
export const doSearch = (params = {}) => {
    return (dispatch, getState) => {
        const {searchParamsAd} = getState()['hrmAdd'].toJS();
        Add.queryFieldsSearch({...params, ...searchParamsAd}).then((data)=>{
            dispatch(WeaTableAction.getDatas(data.sessionkey, params.current || 1));
            dispatch({type: types.HRM_ADD_SEARCH_RESULT, value: data.sessionkey});
        })
    }
}

/*表单域值*/
export const saveFormFields = (value = {}) => {
    return (dispatch, getState) => {
        dispatch({type: types.HRM_ADD_SEARCH_SAVE_FIELDS, fields: value})
    }
}

/*高级搜索显隐*/
export const setShowSearchAd = (value) => {
    return (dispatch, getState) => {
        dispatch({type: types.HRM_ADD_SEARCH_SET_SHOW_SEARCHAD, value: value})
    }
}