import * as types from '../constants/ActionTypes';
import * as underling from '../apis/underling';
import {WeaTable} from 'comsRedux'
const WeaTableAction = WeaTable.action;

/*我的下属列表*/
export const queryData = (params = {}) => {
    return (dispatch, getState) => {
        const {quickSearch} = getState()['hrmUnderling'].toJS();
        if (quickSearch) params.lastname = quickSearch;
        underling.queryData(params).then((data)=>{
            dispatch(WeaTableAction.getDatas(data.sessionkey, params.current || 1));
            dispatch({type: types.HRM_UNDERLING_SEARCH_RESULT, value: data.sessionkey});
        })
    }
}

/*我的下属列表*/
export const saveQuickSearch = (v = '') => {
    return (dispatch, getState) => {
        dispatch({type: types.HRM_UNDERLING_SAVE_QUICK_SEARCH, value: v});
    }
}
