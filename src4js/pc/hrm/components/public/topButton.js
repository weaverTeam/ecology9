import {Row, Col,Button,Dropdown,Menu}from 'antd'
const Item = Menu.Item
import cloneDeep from 'lodash/cloneDeep'

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDrop:false,
        }
    }
    render() {
        const {showDrop} = this.state;
        const {icon,buttons,buttonSpace,showDropIcon,dropMenuDatas} = this.props;//isFixed:hrm-my-card-top是否固定
        const menu = dropMenuDatas ?
        <Menu mode='vertical' onClick={o => {if(typeof this.props.onDropMenuClick == 'function') this.props.onDropMenuClick(o.key)}}>
            {
                dropMenuDatas.map((d, i)=> {
                    return (
                        <Item key={d.key || i} disabled={d.disabled}>
                            <span className='wea-right-menu-icon'>{d.icon}</span>
                            {d.content}
                        </Item>
                    )
            })}
        </Menu> : '';
        return (
            <div className="hrm-my-card-top hrm-my-card-top-fixed">
                {
                    buttons.map((data)=>{
                        return (
                            <span style={{display:'inline-block',lineHeight:'28px',verticalAlign:'middle',marginLeft:!!buttonSpace ? buttonSpace : 20}}>{data}</span>
                        )
                    })
                }
                {
                    showDropIcon &&
                    <span className='hrm-my-card-top-drop-btn' onClick={()=>this.setState({showDrop:true})}>
                        <i className="hrm-my-card-top-icon-button icon-New-Flow-menu" />
                    </span>
                }
                <div className='hrm-my-card-top-drop-menu wea-right-menu' onMouseLeave={()=>this.setState({showDrop:false})} style={{display:showDrop ? 'block' : 'none'}}>
                    <span className='hrm-my-card-top-drop-btn' onClick={()=>this.setState({showDrop:false})}>
                        <i className="hrm-my-card-top-icon-button icon-New-Flow-menu" />
                    </span>
                    <div className='wea-right-menu-icon-background'></div>
                    {menu}
                </div>
            </div>
        )
    }
}


export default Main;

