import {Row,Col,Icon} from 'antd'


class InfoGroup extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            showGroup: props.showGroup ? props.showGroup : true
        }
    }
    componentWillReceiveProps(nextProps) {

    }
	render() {
        const {title,items,colLeft=6,colRight=18} = this.props;
        const {showGroup} = this.state;
        const colArr = [];
        items && items.forEach((obj,index)=>{
            const rightLabel = index%2 !== 0;
            const config = rightLabel ? { span:colLeft-2,offset:2} : {span:colLeft}
            colArr.push(
                <div style={{display: 'inline-block', width: '50%'}}>
                    <Col className="wea-info-group-content-cell-label" {...config}>
                        {obj.label}
                    </Col>
                    <Col className="wea-info-group-content-cell-value" span={colRight} dangerouslySetInnerHTML={{__html:obj.value}}>
                    </Col>
                </div>
            );
        })
        return (
            <div className="wea-info-group">
                <Row className="wea-info-group-title">
                    <Col span={20}>
                        <div className='wea-info-group-title-info'>{title}</div>
                    </Col>
                    <Col span={4} style={{textAlign:"right",paddingRight:10,fontSize:12,lineHeight:'32px'}}>
                        <i style={{cursor: 'pointer'}} className={showGroup ? 'icon-coms-up' : 'icon-coms-down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                    </Col>
                </Row>
                <Row className="wea-info-group-content" style={showGroup ? null : {display:'none'}} >
                    {colArr}
                </Row>
            </div>
        )
    }

}

export default InfoGroup