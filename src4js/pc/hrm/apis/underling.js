import { WeaTools } from 'ecCom'

export const queryData = (params) => {
    return WeaTools.callApi('/api/hrm/subordinateinfo/getSearchResult', 'POST', params);
}

export const getQuickMenu = () => {
    return WeaTools.callApi('/api/hrm/subordinateinfo/getQuickMenu', 'GET');
}

export const saveQuickMenu = (params) => {
    return WeaTools.callApi('/api/hrm/subordinateinfo/saveQuickMenu', 'POST', params);
}