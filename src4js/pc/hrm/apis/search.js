import { WeaTools } from 'ecCom'

export const getHrmSearchCondition = (params) => {
	return  WeaTools.callApi('/api/hrm/search/getHrmSearchCondition','GET',params);
}
export const queryFieldsSearch = (params) => {
    return WeaTools.callApi('/api/hrm/search/getHrmSearchResult', 'POST', params);
}
export const queryFieldsTree = (params) => {
    return WeaTools.callApi('/api/hrm/base/getHrmSearchTree', 'GET', params);
}
export const saveHrmSearchCondition = (params) => {
	return WeaTools.callApi('/api/hrm/search/saveHrmSearchCondition','POST',params)
}
export const getHrmSearchMoudleList = (params) => {
	return WeaTools.callApi('/api/hrm/search/getHrmSearchMoudleList','GET',params)
}
export const customQueryCondition = (params) => {
	return WeaTools.callApi('/api/hrm/search/getHrmSearchUserDefine','GET',params)
}
export const saveQueryCondition = (params) => {
	return WeaTools.callApi('/api/hrm/search/saveHrmSearchUserDefine','POST',params)
}
export const getRightMenuConfig = () => {
    return WeaTools.callApi('/api/hrm/search/getRightMenu','GET')
}
export const exportExcel = (dataKey) => {
    return WeaTools.callApi('/api/ec/dev/table/export', 'POST', {dataKey: dataKey})
}