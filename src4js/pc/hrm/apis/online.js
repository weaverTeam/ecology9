import { WeaTools } from 'ecCom'

export const getSearchCondition = () => {
    return  WeaTools.callApi('/api/hrm/online/getSearchCondition','GET');
}
export const queryFieldsSearch = (params) => {
    return WeaTools.callApi('/api/hrm/online/getSearchResult', 'POST', params);
}