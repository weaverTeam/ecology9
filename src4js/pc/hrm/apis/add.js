import { WeaTools } from 'ecCom'

export const getSearchCondition = () => {
    return  WeaTools.callApi('/api/hrm/search/getHrmSearchCondition','GET');
}
export const queryFieldsSearch = (params) => {
    return WeaTools.callApi('/api/hrm/search/getHrmSearchResult', 'POST', params);
}