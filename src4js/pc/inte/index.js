import Route from "react-router/lib/Route"

import Home from "./components/Home.js"
import Test from "./components/Test.js"

import "./css/icon.css"

import reducer from "./reducers/"
import * as TestAction from "./actions/test"

const inteRoute = (
  <Route path="inte" component={ Home }>
    <Route name="test" path="test" component={Test}/>
  </Route>
)

module.exports = {
  Route: inteRoute,
  reducer,
  action: {
  	TestAction
  }
}