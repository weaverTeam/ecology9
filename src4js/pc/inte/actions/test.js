import * as types from '../constants/ActionTypes'

import {
    message
} from 'antd'

import * as Apis from '../apis/test'

export const test = params => {
    return (dispatch, getState) => {
        Apis.test(params).then(result => {
            dispatch({
                type: types.TEST,
                data: result
            })
        }).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const saveTest = params => {

    return (dispatch, getState) => {
        Apis.savesetting(params).then(result => {
            dispatch({
                type: types.TEST2,
                data: result
            })

        }).catch(
            error => {
                message.error(error)
            }
        )
    }
}