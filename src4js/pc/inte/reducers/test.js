import * as types from '../constants/ActionTypes'

let initialState = {
	title: "集成测试",
	data: [],
	sysid: "",
	fields: {},
	flag: "false",
	selectMap: {}
};

export default function test(state = initialState, action) {
	switch (action.type) {
		case types.TEST:
			console.log("reduce ", action.data)
			return {...state,
				data: action.data.data,
				sysid: action.data.sysid,
				selectMap: action.data.selectMap
			};
		case types.TEST2:
			console.log("reduce===tYPES2 ", action.data)
			console.log("state ", state)
			return {...state,
				flag: action.data.flag

			};
		default:
			return state
	}
}