import React from 'react'
import {
  Form,
  Input,
  Button,
  Row,
  Col
} from 'antd';
import {
  WeaSelect
} from 'ecCom';
import {
    InteTableEle
} from './InteTableEle.js'
const FormItem = Form.Item;

class InteTable extends React.Component {

  render() {
      const formItemLayout = {
          labelCol: {span: 3},
          wrapperCol: {span: 3},
      };
    const{datas,getFieldProps,selectMap} = this.props;
    const{sysid,title,itemList} = datas;
      let abc;
      abc = itemList.map(function(item,i) {
          return <InteTableEle key={i} formItemLayout={formItemLayout} getFieldProps={getFieldProps} item={item} sysid={sysid} selectMap={selectMap}/>
      })

    return (
    <div>
    <table className="LayoutTable" style={{'table-layout':'fixed','display':'','width':'100%'}}>
        <thead>
        <tr className="groupHeadHide">
            <td className="interval">
                <span className="groupbg"> </span>
                <span className="e8_grouptitle">{title}</span>
            </td>
            <td className="interval" colspan="2" style={{'text-align':'right'}}>
                <span className="toolbar"></span>
                <span className="hideBlockDiv" style={{'display':''}}>
            </span>
            </td>
        </tr>
        </thead>
    </table>
        {abc}
    </div>


    );
  }
}
export {
    InteTable
};