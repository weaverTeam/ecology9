import PropTypes from 'react-router/lib/PropTypes'
import {
    bindActionCreators
} from 'redux'
import {
    connect
} from 'react-redux'

import * as TestAction from '../actions/test'
import {
    WeaErrorPage,
    WeaTools
} from 'ecCom'
import {
    WeaTop,
    WeaRightMenu
} from 'ecCom';
import {
    Form,
    Input,
    Button,
    Row,
    Col,
    Alert,
    message,
    Icon,
    Checkbox
} from 'antd';

import {
    InteTable
} from './InteTable.js'
const FormItem = Form.Item;
const createForm = Form.create;

import Immutable from 'immutable'
const is = Immutable.is;
const fromJS = Immutable.fromJS;
class Test extends React.Component {
        static contextTypes = {
            router: PropTypes.routerShape
        }

        constructor(props) {
            super(props);
        }

        componentDidMount() {
            //一些初始化请求
            const {
                actions
            } = this.props;
            actions.test("nc6x")
        }



        render() {
                const formItemLayout = {
                    labelCol: {
                        span: 6
                    },
                    wrapperCol: {
                        span: 14
                    },
                };
                const {
                    getFieldProps,
                    getFieldValue
                } = this.props.form;
                const {
                    data,
                    selectMap,
                    flag
                } = this.props;
                let alldata = data.map(function(datas, i) {
                    return <InteTable key={i}  datas={datas} getFieldProps={getFieldProps} selectMap={selectMap} />
                })
                const dropMenuDatas = [{
                    key: 1,
                    icon: <i className='icon-search' />,
                    content: '保存'
                }];
                const btns = [
                    (<Button type="glost" disabled={false} onClick={()=>this.handleSubmit()}>保存</Button>)
                ];

                return (
                    <WeaRightMenu datas={dropMenuDatas} onClick={this.onDropMenuClick.bind(this)}>
                <WeaTop
                    title={'账号设置'}
                    icon={<i className='icon-search' />}
                    iconBgcolor='#55D2D4'
                    buttons={btns}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={dropMenuDatas}

                >
            <Form horizontal >
                {alldata}
            </Form>
                </WeaTop>
            </WeaRightMenu>

                );
            }
            // handleSubmit = (e) => {
            //     e.preventDefault();
            //     this.props.form.validateFields((err, values) => {
            //             console.log('Received values of form: ', values);
            //         this.props.actions.saveTest(values);
            //     });
            // }

        onDropMenuClick(key) {
            const {
                actions,
                fields
            } = this.props;
            if (key == '1') {
                actions.saveTest({
                    "JsonStr": JSON.stringify(fields)
                });
            }
        }
        handleSubmit() {
            const {
                actions,
                fields
            } = this.props;
            actions.saveTest({
                "JsonStr": JSON.stringify(fields)
            });
        }


    }
    //组件检错机制
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
        return (
            <WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}

Test = WeaTools.tryCatch(React, MyErrorHandler, {
    error: ""
})(Test);
//表单双向绑定数据
Test = createForm({
    onFieldsChange(props, fields) {
        for (var name in fields) {
            props.fields[name] = fields[name].value
        }

    },
    mapPropsToFields(props) {
        return props;
    }
})(Test);

// 把 state map 到组件的 props 上
const mapStateToProps = state => {

    if (state.inteTest.flag != "false") {
        message.success('保存成功', 3);
    }
    return {
        ...state.inteTest
    }
}

// 把 dispatch map 到组件的 props 上d
const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(TestAction, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Test);