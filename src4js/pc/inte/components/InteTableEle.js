import React from 'react'
import {
  Form,
  Input,
  Button,
  Select,
  Row,
  Col
} from 'antd';
import {
  WeaSelect
} from 'ecCom';

const FormItem = Form.Item;
const Option = Select.Option;
class InteTableEle extends React.Component {

  render() {
      const{getFieldProps,formItemLayout,item,sysid,selectMap} = this.props;
      let formItemLayouts = formItemLayout;
      const {
          selectOptions,
          selectTiltle
      } = selectMap;
    let{title,name,value,type,isRead} = item;
    let abc;
    let optionAll;
    let last;
    optionAll = selectOptions.map(function(options,i) {
         return <Option key={i} value={options.value}>{options.name} </Option>;
     })

      const formItemLayout2 = {
          labelCol: {span: 3},
          wrapperCol: {span: 3},
      };
    if(isRead == 'N'){
      if(type == 'text'|| type == 'password') {
          name = name + sysid;
          abc = <Input type={type} name={name} {...getFieldProps(name, {initialValue: value})} ></Input>
          last =   <FormItem {...formItemLayouts} label={title}>{abc}</FormItem>;
      } else{
        name = "logintype_999_" + sysid
        abc =  <Select name={name} {...getFieldProps(name, {initialValue: value})} >{optionAll}</Select>;
        last =   <FormItem {...formItemLayout2} label={title}>{abc}</FormItem>;
     }

    }else {
        if (!isRead) {
            abc = null;
            last = null;
        } else {
            last = <FormItem {...formItemLayouts} label={title}><span>{isRead}</span></FormItem>;
        }
    }
    return (
        <div>
            {last}
        </div>

    );
  }
}
export {
    InteTableEle
};