import {
   WeaTools
} from 'ecCom'
import {
   INTEGRA_URL,
   INTEGRA_SAVE_URL
} from '../constants/ActionTypes';
export const test = params => {
   console.log("api ", params)

   return WeaTools.callApi(INTEGRA_URL, 'GET', params);
}

export const savesetting = params =>  {
   console.log("api======== ", params)

   return WeaTools.callApi(INTEGRA_SAVE_URL, 'POST', params);
}

// return Promise.resolve({


//    "selectOptions": [{
//       "name": "内网访问",
//       "value": "1"
//    }, {
//       "name": "外网访问",
//       "value": "2"
//    }],
//    "selectTitle": "访问类型",
//    "nameTitle": "帐号",
//    "data": [{
//       "title": "e8utf",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "susu001",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "e8utf",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "",
//          "value": "&lt;145236",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "e8gbk",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "20089006072",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "e8gbk",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "111111111111111111111111111111",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "133131JTYSBGHYJY",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "12321",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "1222223213",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "NCtest1",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "NCtest1",
//       "selectMap": {},
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "263test",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "263test",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "testMD5",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "testMD5",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "测试",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [{
//          "paramname": "b",
//          "labelName": "b",
//          "paramvalue": ""
//       }],
//       "sysid": "20161031",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "20161114",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "20161114",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "testPBE",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "testPBE",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "testZDY",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "testZDY",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "test",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "test",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "测试1214",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "swq1",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "ceshi1214",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "",
//          "value": "1",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "tx1220",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "tx1220",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "eas1220",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "eas1220",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "e7utf",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "雨停",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "e7utf",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "2"
//       },
//       "pwdMap": {
//          "judgeRead": "",
//          "value": "1",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "test1122",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "test1222",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "TEST2018",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "swq3",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "TEST2018",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "",
//          "value": "1",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "测试*&……&",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "ceshi",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "u8test",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "u8test",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "u8test1",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "u8test1",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "e7gbk",
//       "nameMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "e7gbk",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "k3we",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "k3tes14",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "test20001",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "test20001",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "testtest",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "testtest",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "CoreMail邮箱",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "CoreMail",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "使用ecology账号",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "test20170523",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "test20170523",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "test0523",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "test0523",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }, {
//       "title": "test2017052301",
//       "nameMap": {
//          "judgeRead": "",
//          "value": "",
//          "judgeView": "1",
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "test2017052301",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "judgeRead": "",
//          "value": "",
//          "judgeView": "1",
//          "type": "password"
//       }
//    }, {
//       "title": "127.0.0.1",
//       "nameMap": {
//          "type": "text"
//       },
//       "labelList": [],
//       "sysid": "111",
//       "selectMap": {
//          "judgeView": "1",
//          "selectValue": "1"
//       },
//       "pwdMap": {
//          "type": "password"
//       }
//    }],
//    "sysid": "all",
//    "pwdTitle": "密码"



// })
//}