import React from 'react';
import Immutable from 'immutable';
import { Button } from 'antd';
import { WeaErrorPage, WeaTools, WeaDialog, WeaLeftRightLayout, WeaRightMenu, WeaNewScroll } from 'ecCom';
import HpBaseElement from './HpBaseElement';
import Homepage from './Homepage';
import PortalMenu from './PortalMenu';
import { handlePortalSynize, onShowOrHideAllE, openFavouriteBrowser, showHelp } from '../util/rcmenu';
//门户组件
class PortalSetting extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            params: {},
            visible: false,
            title:'设置元素',
            data: {
                hpbaseelements:[],
                hpsettingmenu:[],
            },
            showLeft: true,
            portalmenu: {},
        }
        this.getButtons = this.getButtons.bind(this);
        this.getRightMenu = this.getRightMenu.bind(this);
        this.onRightMenuClick = this.onRightMenuClick.bind(this);
        this.handleOnSetting = this.handleOnSetting.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleRefreshPortal = this.handleRefreshPortal.bind(this);
        this.handleAddElementToPortal = this.handleAddElementToPortal.bind(this);
    }
    getButtons(){
        let btns = [];
        const { hpsettingmenu } = this.state.data;
        hpsettingmenu.map((menu, i)=>{
            btns.push(<Button type={i === 0 ? 'primary' : ''} onClick={this.onRightMenuClick.bind(this,menu.key)}>{menu.label}</Button>); 
        });
        return btns;
    }
    getRightMenu(){
        let btns = [];
        const { rcmenu } = this.state.data;
        if(rcmenu){
            rcmenu.map(menu=>{
                btns.push({
                    key: menu.key,
                    icon: <i className={menu.icon}/>,
                    content: menu.label,
                }); 
            });
        }
        return btns
    }
    onRightMenuClick(key){
        const { hpid, subCompanyId, isSetting, showLeft } = this.state;
        switch(key){
            case 'synihp':
            case 'synihpnormal':
                handlePortalSynize({
                    hpid: hpid,
                    subCompanyId: subCompanyId,
                    method: key
                });
                break;
            case 'shrinkAll':
                onShowOrHideAllE(isSetting);
                break;
            case 'store':
                openFavouriteBrowser();
                break;
            case 'help':
                showHelp();
                break;
            case 'hiddenElementLib':
                this.setState({
                    showLeft: !showLeft
                });
                break;
        }
    }
    handleOnSetting(params){
        if(params.opt === 'priview'){
            if(params.module === 'mainpage'){
                WeaTools.callApi('/api/portal/hpsetting/portalmenu', 'POST', {hpid: params.hpid}).then((data) => {
                    window.isPortalRender = true;
                    if(!data.status){
                        this.setState({
                            visible: true,
                            params,
                            portalmenu: data,
                        });
                    }
                });
            }else{
                this.setState({
                    visible: true,
                    params,
                    data: {
                        hpbaseelements:[],
                        hpsettingmenu:[],
                    },
                    portalmenu: {},
                });
            }

        }else{
            WeaTools.callApi('/api/portal/hpsetting/hpsettinginfo', 'POST', params).then((data) => {
                window.isPortalRender = true;
                if(!data.status){
                    this.setState({
                        visible: true,
                        params,
                        data,
                        portalmenu: {},
                    });
                }
            });
        }
    }
    handleCancel(){
        this.setState({
            visible: false,
        });
    }
    handleRefreshPortal(params){
        this.refs.homepage.handleRefresh(params);
    }
    handleAddElementToPortal(element, areaflag, index){
        this.refs.homepage.handleAddElement(element, areaflag, index);
    }
    componentDidMount(){
        window.handlePortalSetting = this.handleOnSetting.bind(this);
    }
    render() {
        const { params, visible, data, portalmenu, showLeft } = this.state;
        const { hpid, subCompanyId, isSetting, opt, module } = params;
        let title = '预览';
        let html = <div/>;
        if(hpid){
             html = <WeaNewScroll scrollId="e9PortalSetting" height={top.document.body.clientHeight - 150}>
                              <div id="e9PortalSetting" className="portal-e9setting">
                                <Homepage {...params} ref="homepage"/>
                              </div>
                          </WeaNewScroll>
            if(opt !== 'priview'){
                title = '设置元素';
                html = visible && <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick}>
                      <WeaLeftRightLayout showLeft={showLeft} leftWidth={1} leftCom={<HpBaseElement handleAdd={this.handleAddElementToPortal} params={params} list={data.hpbaseelements}/>}>
                        {html}
                      </WeaLeftRightLayout>
                   </WeaRightMenu>;
            }
        }
        return <WeaDialog title={title} 
                  visible={visible}
                  closable={true}
                  onCancel={this.handleCancel}
                  icon='icon-coms-portal'
                  iconBgcolor='#1a57a0'
                  maskClosable={false}
                  buttons={this.getButtons()}
                  style={{width:top.document.body.clientWidth - 100,height:top.document.body.clientHeight - 150, overflow:'hidden' }}>
                  {module === 'mainpage' ? <PortalMenu hpid={hpid} subCompanyId={subCompanyId} portalmenu={portalmenu} handleRefresh={this.handleRefreshPortal}/> : ''}
                  {html}
            </WeaDialog>
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
        return ( <WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
PortalSetting = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(PortalSetting);
export default PortalSetting;




