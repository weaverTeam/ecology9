import React from 'react';
import ReactDOM from 'react-dom';
//引入元素组件
import { Element } from '../elements/';
class CustomLayoutFlags extends React.Component {
	//加载自定义布局门户
	renderCustomLayout() {
		const { layoutid, layoutFlags, layoutHtml } = this.props;
        let _config = this.props.config;
   		if (layoutHtml) {
	        layoutFlags.map((layoutFlag, i) => {
	       		let lhtml = layoutFlag.areaElements.map(config => <Element key={"element-"+config.item.eid} config={{...config, layoutid, ..._config}}/>);
                ReactDOM.render(<div>{lhtml}</div>,$(".homepage[data-issetting="+_config.isSetting+"]").find(".group[areaflag='" + layoutFlag.areaflag + "']")[0]);
	        });
        }
    }
    componentDidMount() {
        this.renderCustomLayout();
    }
    componentDidUpdate(prevProps) {
        const prelayoutid = prevProps.layoutid;
        const { layoutid, config } = this.props;
        if (prevProps.hpid !== this.props.hpid || prelayoutid !== layoutid || config.isRender) {
            this.renderCustomLayout();
        }
    }
    render() {
        let eHtml = <div></div>;
        const layoutHtml = this.props.layoutHtml;
        if (layoutHtml) {
            eHtml = <div dangerouslySetInnerHTML = {{ __html: layoutHtml }}></div>;
        }
        return <div>{eHtml}</div>
    }
}

import { WeaErrorPage, WeaTools } from 'ecCom';
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
        return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
CustomLayoutFlags = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(CustomLayoutFlags);
export default CustomLayoutFlags;
