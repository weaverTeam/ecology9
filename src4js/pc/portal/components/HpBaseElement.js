import Immutable from 'immutable';
import { Button, Icon, message } from 'antd';
import { WeaErrorPage, WeaTools, WeaNewScroll, WeaInputSearch } from 'ecCom';
import { dragBaseElement } from '../util/rcmenu';

class HpBaseElement extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            value: ''
        }
        this.handleAddElement = this.handleAddElement.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.handleOnSearch = this.handleOnSearch.bind(this);
    }
    handleAddElement(ebaseid, areaflag = 'A', index = 0){
    	if($("#e9PortalSetting").find(".item").length === 10) {
    		if(confirm("继续添加会影响页面性能，还要继续吗？")){
    			this.onAdd(ebaseid, areaflag, index);
    		}
    	}else{
    		this.onAdd(ebaseid, areaflag, index);
    	}
    }
    onAdd(ebaseid, areaflag, index){
        let { params, handleAdd } = this.props;
        const { hpid, isSetting } = params;
        params.ebaseid = ebaseid;
        params.areaflag = areaflag;
        params.index = index;
        WeaTools.callApi('/api/portal/hpbaseelement/addElement', 'POST', params).then((data) => {
            if(data.status === 'failed'){
                message.error("添加元素失败，请您联系管理员！",2);
            }else{
                try{
                    handleAdd(data, areaflag, index);
                } catch(e){
                    window.console ? console.log('添加元素后刷新门户异常： ',e) : alert('添加元素后刷新门户异常： '+e);
                }
            }
        });
    }
    handleOnSearch(value){
        this.setState({
            value,
        });
    }
    render(){
        const { list } = this.props;
        const { value } = this.state;
        let htmlArr = new Array;
        list.map((item)=>{
            if(value && item.title.toLowerCase().indexOf(value.toLowerCase()) == -1 && item.id.toLowerCase() !== value.toLowerCase()){
                return;
            }
            htmlArr.push(<div className="portal-setting-left-hpbaseelement-item" onMouseDown={dragBaseElement.bind(this,item.id, this.handleAddElement)}><div style={{float:'left'}}><img src={item.icon}/></div><div><a href="javascript:void(0);" style={{marginLeft:'10px'}} onClick={this.handleAddElement.bind(this,item.id,'A',0)}>{item.title}</a></div></div>);
        });
        return <div className="portal-setting-left-hpbaseelement">
            <WeaInputSearch placeholder="输入关键词" type="contacts" onSearchChange={this.handleOnSearch} style={{width:'100%',backgroundColor:'#e1e5eb',height:'28px'}} />
            <WeaNewScroll scrollId="hpbaseelement" height={top.document.body.clientHeight - 178}>{htmlArr}</WeaNewScroll>
        </div>
    }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}

HpBaseElement = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(HpBaseElement);
export default HpBaseElement;
