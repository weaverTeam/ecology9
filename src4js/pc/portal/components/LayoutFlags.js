import React from 'react';
import CustomLayoutFlags from './CustomLayoutFlags';
//引入元素组件
import { Element } from '../elements/';
class LayoutFlags extends React.Component {
 render() {
     //定义布局id全局变量
     if (this.props.layoutHtml !== undefined) {
         return <CustomLayoutFlags {...this.props}/>
     } else {
         return <CommonLayoutFlags {...this.props}/>
     }
 }
}

import { WeaErrorPage, WeaTools } from 'ecCom';

class MyErrorHandler extends React.Component {
 render() {
     const hasErrorMsg = this.props.error && this.props.error !== "";
     return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
     );
 }
}

LayoutFlags = WeaTools.tryCatch(React, MyErrorHandler, {
 error: ""
})(LayoutFlags);

export default LayoutFlags;

class CommonLayoutFlags extends React.Component{
    render(){
        let eHtml = <div/>;
        const { config, layoutid, layoutFlags, layoutHtml } = this.props;
        switch (layoutid) {
           case "1": //一栏布局
           case "2": //两栏布局
           case "3": //三栏布局
               let lfhtml = <td></td>;
               if (layoutFlags && layoutFlags.length) {
                   lfhtml = layoutFlags.map((layoutFlag, i) => {
                       return <td width={layoutFlag.areasize} className="valign"><GroupDiv key={layoutFlag.areaflag} config={{...config, layoutid}} layoutFlag={layoutFlag}/></td>
                   });
               }
               eHtml = <table id="Container"><tbody><tr>{lfhtml}</tr></tbody></table>
               break;
           case "4": //其他布局1
               eHtml = <SYSOtherRowOne config={{...config, layoutid}} layoutFlags={layoutFlags}/>;
               break;
           case "5": //其他布局2
               eHtml = <SYSOtherRowTwo config={{...config, layoutid}} layoutFlags={layoutFlags}/>;
               break;
           default:
               break;
        }
        return <div>{eHtml}</div>;
    }
}

//其他布局1
class SYSOtherRowOne extends React.Component{
    render(){
     const layoutFlag = new Object;
     const { config, layoutFlags } = this.props;
     layoutFlags.map(item => layoutFlag['flag'+item.areaflag] = item);
     const aflag = layoutFlag.flagA;
     const bflag = layoutFlag.flagB;
     const cflag = layoutFlag.flagC;
     const dflag = layoutFlag.flagD;
     return <table id="Container">
      <tr>
        <td width={aflag.areasize} className="valign" >
          <table width="100%" className="valign" border="0" >
            <tr>
              <td colspan="2" className="valign">
                <GroupDiv config={config} key={aflag.areaflag} layoutFlag={aflag}/>
              </td>
            </tr>
          </table>
          <table width="100%" className="valign" border="0" >
            <tr>
              <td width={cflag.areasize} className="valign" border="0">
                <GroupDiv config={config} key={cflag.areaflag} layoutFlag={cflag}/>
              </td>
              <td width={dflag.areasize} className="valign" border="0">
                <GroupDiv config={config} key={dflag.areaflag} layoutFlag={dflag}/>
              </td>
            </tr>
          </table>
        </td>
        <td width={bflag.areasize} className="valign">
          <table width="100%" className="valign"  border="0">
            <tr>
              <td className="valign" >
                <GroupDiv config={config} key={bflag.areaflag} layoutFlag={bflag}/>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    }
}


//其他布局2
class SYSOtherRowTwo extends React.Component{
    render(){
     const layoutFlag = new Object;
     const { config, layoutFlags } = this.props;
     layoutFlags.map(item => layoutFlag['flag'+item.areaflag] = item);
     const aflag = layoutFlag.flagA;
     const bflag = layoutFlag.flagB;
     const cflag = layoutFlag.flagC;
     const dflag = layoutFlag.flagD;
     return <table id="Container">
          <tr>
            <td width={dflag.areasize} className="valign">
              <table width="100%" className="valign">
                <tr>
                  <td width={aflag.areasize} className="valign">
                     <GroupDiv config={config} key={aflag.areaflag} layoutFlag={aflag}/>
                  </td>
                   <td width={bflag.areasize} className="valign">
                      <GroupDiv config={config} key={bflag.areaflag} layoutFlag={bflag}/>
                   </td>
                 </tr>
                </table>
                <table width="100%" className="valign">
                <tr className="valign">
                  <td colspan={2} className="valign">
                      <GroupDiv config={config} key={dflag.areaflag} layoutFlag={dflag}/>
                  </td>
                </tr>
                </table></td>
                 <td width={cflag.areasize} className="valign">
                 <table width="100%" className="valign">
                  <tr>
                    <td className="valign">
                      <GroupDiv config={config} key={cflag.areaflag} layoutFlag={cflag}/>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
    }
}

//group Div组件
class GroupDiv extends React.Component{
  render(){
    const { layoutFlag } = this.props;
    const _config = this.props.config;
    return <div className="group" data-areaflag={layoutFlag.areaflag}>
        {layoutFlag.areaElements.map(config => <Element key={"element-"+config.item.eid} config={{...config, ..._config}}/>)}
    </div>
  }
}



