import React from 'react';
import { Menu, Icon } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
class PortalMenu1 extends React.Component{
	constructor(props) {
        super(props);
        this.state = {
        	key: props.hpid+'-'+props.subCompanyId,
        }
        this.handleList = this.handleList.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick({item, key, keyPath}){
    	const { handleRefresh } = this.props;
    	if(key){
    		this.setState({
	        	key,
	        })
    		var arr = key.split("-");
    		handleRefresh({hpid: arr[0], subCompanyId: arr[1]});
    	}
    }
	handleList(list){
		let html = list.map(menu => {
			const { hpid, subCompanyId, name, children } = menu;
			if(children && children.length) {
				return <SubMenu key={hpid+'-'+subCompanyId} title={name}>{this.handleList(children)}</SubMenu>
			}else{
				return <Menu.Item key={hpid+'-'+subCompanyId}>{name}</Menu.Item>
			}
			
		});
		return html;
	}
	render(){
		const { portalmenu } = this.props;
		const { pList } = portalmenu;
		return <div className="portal-portalmenu">
				<Menu
			   		theme='dark'
			        selectedKeys={[this.state.key]}
			        mode="horizontal"
			        onClick={this.handleClick}>
					{this.handleList(pList)}
		      </Menu></div>
	}
}
class PortalMenu extends React.Component{
	constructor(props) {
        super(props);
		let num = props.portalmenu.pList.length;
		let isshow = false;
		if(120 * num > (top.document.body.clientWidth - 100)){
			isshow = true;
		}
        this.state = {
        	hpid: props.hpid,
        	isshow,
        }
        this.handleList = this.handleList.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleIconClick = this.handleIconClick.bind(this);
        this.handleMouseLeave = this.handleMouseLeave.bind(this);
        this.doScroll = this.doScroll.bind(this);
    }
	handleClick(hpid,subCompanyId,e){
		e.stopPropagation();
		this.setState({
			hpid
		});
    	this.props.handleRefresh({hpid,subCompanyId});
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.state.hpid !== nextState.hpid;
    }
    handleIconClick(hpid, e){
    	e.stopPropagation();
    	$(".portal-portalmenu-menu[data-pid="+hpid+"]").css({display:'inline-block'});
    }
   	handleMouseLeave(pid, hpid, e){
   		if(pid == '0') return;
    	e.stopPropagation();
    	$(".portal-portalmenu-menu[data-hpid="+hpid+"]").css({display:'none'});
    }
	handleList(list, _pid = '0'){
		const _hpid = this.state.hpid;
		let html = list.map((menu, i) => {
			const { pid, hpid, subCompanyId, name, children } = menu;
			let style = _pid == '0' ? {left: i*120, top: 0} : {left: i*120, top: 42,display:'none'};
			let icon = '';
			let sub = '';
			if(children && children.length) {
				icon = <i onClick={this.handleIconClick.bind(this,hpid)} className="icon-coms-organization-down portal-menu-sub-icon"/>;
				sub = this.handleList(children, hpid);
			}
			return <div id={`portalmenu_${hpid}`} data-pid={pid} data-hpid={hpid} onMouseLeave={this.handleMouseLeave.bind(this,_pid, hpid)} onClick={this.handleClick.bind(this,hpid,subCompanyId)} className={`portal-portalmenu-menu ${_hpid == hpid ? 'portal-portalmenu-selected' : ''}`} style={style}>{name}{icon}{sub}</div>
		});
		return html;
	}
	doScroll(type){
		const { pList } = this.props.portalmenu;
		let width = type === 'prev' ? -120 : 120;
		$("#portalMenuContainer").scrollTo( {top: '0px', left:($("#portalMenuContainer").get(0).scrollLeft + width + 'px')}, 500 );
		if(type === 'prev'){
			$("#menuNext").css({color:'#FFFFFF',cursor:'pointer'});
			if($("#portalMenuContainer").get(0).scrollLeft <= 120){
				$("#menuPrev").css({color:'#acacac',cursor:'default'});	
			}
		}else{
			$("#menuPrev").css({color:'#FFFFFF',cursor:'pointer'});
			if($("#portalMenuContainer").get(0).scrollLeft >= (pList.length * 120 + 40 - top.document.body.clientWidth + 100)) {
				$("#menuNext").css({color:'#acacac',cursor:'default'});
			}
		}
	}
	render(){
		const { portalmenu } = this.props;
		const { isshow } = this.state;
		const { pList } = portalmenu;
		let html = this.handleList(pList);
		if(isshow){
			html = <div id="portalMenuContainer" style={{width:(top.document.body.clientWidth - 140)+'px'}} className="portal-portalmenu-container">
				<div style={{width:(pList.length* 120)+'px', height:'40px',position:'relative'}}>
					{html}
				</div>
			</div>
		}
		return <div className="portal-portalmenu">
			{isshow ? <div id="menuPrev" className="portal-portalmenu-nav menu-prev">
				<Icon type="caret-left" onClick={this.doScroll.bind(this,'prev')}/>
			</div> : ''}
			{html}
			{isshow ? <div id="menuNext" className="portal-portalmenu-nav menu-next">
				<Icon type="caret-right" onClick={this.doScroll.bind(this,'next')}/>
			</div> : ''}
		</div>
	}
}

module.exports = PortalMenu;

