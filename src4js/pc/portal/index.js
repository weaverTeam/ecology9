//门户样式
import './css/';
//门户相关
import Homepage from './components/Homepage';
//协同区组件
import Synergy from './components/Synergy';
//门户设置
import PortalSetting from './components/PortalSetting';
module.exports = {
	Homepage,
	Synergy,
	PortalSetting,
};




