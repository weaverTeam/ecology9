import { message } from 'antd';
import { WeaTools } from 'ecCom';
import { handleAttr } from './common';
const { ls } = WeaTools;
let homepage;
let isSetting;
let hpid;
let subCompanyId;
let intMoveEid;
let srcItem;
let srcGroupFlag;
let divLoc;
let dragobj={}
let _callback;
let onerror = function(){return false}
function on_ini(){
	String.prototype.inc=function(s){
		return this.indexOf(s)>-1?true:false;
	}
	let agent = navigator.userAgent
	window.isOpr=agent.inc("Opera")
	window.isIE=agent.inc("IE")&&!isOpr
	window.isMoz=agent.inc("Mozilla")&&!isOpr&&!isIE
	if(isMoz){
	   Event.prototype.__defineGetter__("x",function(){return this.clientX+2})
	   Event.prototype.__defineGetter__("y",function(){return this.clientY+2})
	}
}
let oDel = function(obj){if($(obj)!=null){$(obj).remove()}}
$(function(){
	on_ini();
	//初始化拖动
	let o = $(".header");
});
function dragStart(e, target, callback){
	_callback = callback;
	//alert("target");
	e = e || event;
	srcElement = e.srcElement || e.target;
	if(!(srcElement.className=="header" || srcElement.className=="title")) return;
	if(!(e.button==1||e.button==0)){
		return;
	}
	if(dragobj.o){
		return;
	}
	if(target.className=="title" ) target = target.parentNode;

	homepage = $(target.parentNode).parents(".homepage")[0];
	isSetting = $(homepage).attr("data-issetting");
	hpid = $(homepage).attr("data-hpid");
	subCompanyId = $(homepage).attr("data-subcompanyid");
	dragobj.o = target.parentNode;
	//alert(dragobj.o.className+"------"+target.className);
	srcGroupFlag=$($(target).parents(".group")[0]).attr(handleAttr('areaflag'));
	//对象左上的坐标
	dragobj.xy = getxy(dragobj.o);
	//鼠标的相对位置
	dragobj.xx = new Array((e.x-dragobj.xy[1]),(e.y-dragobj.xy[0]))
	dragobj.o.style.width=dragobj.xy[2]+"px";
	dragobj.o.style.height=dragobj.xy[3]+"px";
	dragobj.o.style.left=(e.x-dragobj.xx[0])+"px";
	dragobj.o.style.top=(e.y-dragobj.xx[1])+"px";   
	dragobj.o.style.position="absolute";
	//创建临时对象用于记住其原始位置，占位
	
	var om=document.createElement("div");
	dragobj.otemp=om
	om.style.width=dragobj.xy[2]+"px"
	om.style.height=dragobj.xy[3]+"px"
	om.className="location";
	om.style.width="auto";
	dragobj.o.parentNode.insertBefore(om,dragobj.o)
	return false
}
document.onselectstart=function(){
	if(dragobj.o!=null){
		return false;
	}
}
document.onmouseup=function(e){
	if(dragobj.o!=null){
		var _eid = -1;
		try{
			_eid = dragobj.o.id.split("item_")[1];
		}catch(e){
			
		}
	   	dragobj.otemp.parentNode.insertBefore(dragobj.o,dragobj.otemp);
	   	dragobj.o.style.position="static";
		dragobj.o.style.width="100%";
	  	dragobj.o.style.height="auto";
	   	var targetAreaFlag = $(dragobj.otemp).parents(".group").attr(handleAttr('areaflag'));
	   	//自定义页面拖动后刷新元素
	   	if(_callback) _callback();
	   	MoveEData(srcGroupFlag, targetAreaFlag, _eid);
	   	oDel(dragobj.otemp)
	   	//停止拖放
	   	dragobj = {}
	}
}
document.onmousemove=function(e){
	e=e||event;
	if(dragobj.o!=null){
		dragobj.o.style.zIndex="1";
	   	dragobj.o.style.left=(e.x-dragobj.xx[0])+"px";
	   	dragobj.o.style.top=(e.y-dragobj.xx[1])+"px";
	   	dragobj.o.style.left=e.clientX;
		dragobj.o.style.top=e.clientY+document.body.scrollTop;
	   	//自动调整布局，显示拖放效果
	  	createtmpl(e);
	}
}
//取得鼠标的坐标
function mouseCoords(ev){
	let dom = document.getElementById("e9routeMain");
	if(isSetting === 'true'){
		dom = document.getElementById("e9PortalSetting");
	}
	if(ev.pageX || ev.pageY){
		return {x:ev.pageX, y:ev.pageY};
	}
	return {
		x:ev.clientX + dom.scrollLeft - dom.clientLeft,
		y:ev.clientY + dom.scrollTop  - dom.clientTop
	};
}
//取得位置分别为上，左，宽，高,所有元素必须在同一容器内
function getxy(e){
	var a=new Array();
	var t=e.offsetTop;
	var l=e.offsetLeft;
	var w=e.offsetWidth;
	var h=e.offsetHeight;
	while(e=e.offsetParent){
	   t+=e.offsetTop;
	   l+=e.offsetLeft;
	}
	a[0]=t;a[1]=l;a[2]=w;a[3]=h;
	   return a;
}
//判断e与o的位置
function inner(o,e){
	var a=getxy(o)
	//鼠标是否在o的内部
	if(e.x>a[1]&&e.x<(a[1]+a[2])&&e.y>a[0]&&e.y<(a[0]+a[3])){	
	   if(e.y<(a[0]+a[3]/2))
		   //上半部分
			return 1;
	   else
			return 2;
	}else{
		return 0;
	}
}
//
function createtmpl(e){
	var items = $(homepage).find(".item");
	for(var i=0;i<items.length;i++){
	   if(!items[i] || items[i]==dragobj.o)
		continue;
		//修正鼠标坐标
		var mousePos  = mouseCoords(e);
	   var b=inner(items[i],mousePos);
	   if(b==0)
			continue;
	  		dragobj.otemp.style.width="auto";
	   if(b==1){
			items[i].parentNode.insertBefore(dragobj.otemp,items[i]);
	   }
	   else{
			if(items[i].nextSibling==null){
				items[i].parentNode.appendChild(dragobj.otemp);
			}else{
				items[i].parentNode.insertBefore(dragobj.otemp,items[i].nextSibling);
			}
	   }
	}
	//处理拖放至边界外的情况
	var groups = $(homepage).find(".group");
	for(var j=0;j<groups.length;j++){
	   if(groups[j].innerHTML.inc("div")||groups[j].innerHTML.inc("DIV"))
			continue;
	   var op=getxy(groups[j]);
	   if(e.x>(op[1]+10)&&e.x<(op[1]+op[2]-10)){
			groups[j].appendChild(dragobj.otemp);
			dragobj.otemp.style.width=(op[2]-10)+"px";
	   }
	}
}

function MoveEData(srcFlag,targetFlag, eid){
	var srcItemEids = ""; 
	var areaflag = handleAttr("areaflag");
	$(homepage).find(".group["+areaflag+"="+srcFlag+"]>.item").each(function(i){
		if(this.className == "item")	{
			srcItemEids += $(this).attr("data-eid")+",";
		}
	})
	var targetItemEids="";
	$(homepage).find(".group["+areaflag+"="+targetFlag+"]>.item").each(function(i){
		if(this.className == "item")	{
			targetItemEids += $(this).attr("data-eid")+",";
		}
	})  
	if(targetItemEids == "") return;
	const params = {
		hpid,
		subCompanyId,
		eid,
		srcFlag,
		targetFlag,
		srcStr: srcItemEids,
		targetStr: targetItemEids, 
	}
    WeaTools.callApi('/api/portal/elementtoolbar/drag', 'POST', params).then(result=> {
    	if(result.status === 'failed'){
    		message.error(result.errormsg);
    	} else {
    		ls.set('portal-'+hpid+'-'+isSetting, null);
    	}
    });
}

module.exports = {
	dragStart
}
