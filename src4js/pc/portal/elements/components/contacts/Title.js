import React from 'react';
import Immutable from 'immutable';
const { is, fromJS } = Immutable; 
import { WeaErrorPage, WeaTools } from 'ecCom';
import OpToolbar from '../common/OpToolbar';
import { doPrev, doNext, handleWidth } from '../../util/common';
//tab的标题组件
class Title extends React.Component {
    componentDidMount() {
        const { config, tabids } = this.props;
        const { eid, header } = config.item;
        handleWidth(eid,config.params.hpid,tabids.length,header.canHeadbar);
    }
    componentDidUpdate() {
        const { config, tabids } = this.props;
        const { eid, header } = config.item;
        handleWidth(eid,config.params.hpid,tabids.length,header.canHeadbar);
    }
    shouldComponentUpdate(nextProps){
        return !is(fromJS(this.props),fromJS(nextProps))
    }
    render() {
        const tdStyle={width:'77px', wordWrap: 'break-word', paddingTop: '5px', verticalAlign: 'top', height: '32px' };
        const { currentTab, config, tabids, titles, handleRefresh, handleDelete, handleTabData } = this.props;
        const { eid, ebaseid } = config.params;
        let tHtml = titles.map((title, i)=> {
            const className = tabids[i] == currentTab ? "tab2selected" : "tab2unselected";
            return <td title={title} data-tabid={tabids[i]} style={tdStyle} className={className} onClick={handleTabData.bind(this,tabids[i],'')}>{title}</td>
        });
        let className = tabids.length <= 1 ? 'nodisplay' : '';
        return <div id={ `titleContainer_${eid}` } className={`titlecontainer ${className}`}>
                {<div id={`tabnavprev_${eid}`} className="picturebackhp" onClick={doPrev.bind(this,eid)}></div>}
                <div id={ `tabContainer_${eid}` } className="tabcontainer tab2">
                    <table style={ { tableLayout: "fixed", borderCollapse: 'collapse'} } height="32">
                      <tbody height="32">
                        <tr height="32">
                            {tHtml} 
                        </tr> 
                      </tbody>
                    </table>
                </div> 
                {<div id={`tabnavnext_${eid}`} className="picturenexthp" onClick={doNext.bind(this,eid)}></div>} 
                {!config.item.header.canHeadbar ? <OpToolbar config={config} clsname={'nodisplay'} handleRefresh={handleRefresh} handleDelete={handleDelete}/> : ''} 
              </div>
      }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg=this.props.error && this.props.error !=="";
            return ( <WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
        );
    }
}
Title = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(Title);
export default Title;
