import React from 'react';
import Immutable from 'immutable';
const { is, fromJS } = Immutable; 
import { WeaTools, WeaErrorPage } from 'ecCom';
const { ls } = WeaTools;
//自定义页面元素
class Iframe extends React.Component {
    constructor(props) {
        super(props);
        this.setFrameData = this.setFrameData.bind(this);
        this.wirteDataToFrame = this.wirteDataToFrame.bind(this);
    }
    setFrameData(curProps){
        let that = this;
        const { eid, data, tabid, isSetting } = curProps;
        $(".homepage[data-issetting="+isSetting+"]").find("#ifrm_" + eid).attr({
            scrolling: "auto",
            noresize: "noresize",
            border: "0"
        });
        const ifrmUrl = data.url;
        if (ifrmUrl) {
            if (ifrmUrl.indexOf("http://") == 0 || ifrmUrl.indexOf("https://") == 0) {
                setTimeout(() => {
                    $(".homepage[data-issetting="+isSetting+"]").find("#ifrm_" + eid).attr({ src: ifrmUrl });
                }, 500);
            } else {
                var arr = ifrmUrl.split(".");
                var last = arr[arr.length - 1]
                const index = last.indexOf("htm");
                const ifrmsdata = ls.getStr("custompage-" + eid + "-" + tabid);
                let aa = ifrmsdata ? true : false;
                if (ifrmsdata) {
                    //解决刷新加载缓存空白的问题
                    setTimeout(() => {
                        that.wirteDataToFrame(ifrmsdata, eid, isSetting);
                        //解决改变url或URL返回的内容改变时不更新的问题
                        $.ajax({
                            type: "GET",
                            url: ifrmUrl,
                            dataType: "html",
                            success: function(data) {
                                //判断返回的数据是否有变化，有的话更新缓存并重新渲染
                                if (data !== ifrmsdata) {
                                    ls.set("custompage-" + eid + "-" + tabid, data);
                                    that.wirteDataToFrame(data, eid, isSetting);
                                }
                            },
                            error: function(xhr) {
                                that.wirteDataToFrame(xhr.responseText, eid, isSetting);
                            }
                        });
                    }, 0.1);
                } else {
                    $.ajax({
                        type: "GET",
                        url: ifrmUrl,
                        dataType: "html",
                        success: function(data) {
                            if (index >= 0) {
                                ls.set("custompage-" + eid + "-" + tabid, data);
                            }
                            that.wirteDataToFrame(data, eid, isSetting);
                        },
                        error: function(xhr) {
                            that.wirteDataToFrame(xhr.responseText, eid, isSetting);
                        }
                    });

                }
            }
        }
    }
    wirteDataToFrame(data, eid, isSetting){
        let _ifrm = $(".homepage[data-issetting="+isSetting+"]").find("#ifrm_" + eid)[0];
        if(_ifrm){
            _ifrm.contentWindow.document.write(data)
            _ifrm.contentWindow.document.close()
            try {
                _ifrm.contentWindow.customPageInit()
            } catch (e) {}
        }
    }
    componentDidMount() {
        const { erefresh, refresh } = this.props;
        if(erefresh !== null){
            if(!erefresh) this.setFrameData(this.props);
        } else {
            if(!refresh) this.setFrameData(this.props);
        }
    }
    componentWillUnmount() {
        const { eid, isSetting } = this.props;
        $(".homepage[data-issetting="+isSetting+"]").find("#ifrm_" + eid).attr({src:'about:blank'});
    }
    render() {
        const { eid, data, tabid } = this.props;
        let html = <iframe id={`ifrm_${eid}`}
            frameBorder="no" 
            className="custompageIframe" 
            height={data.height}
            src="about:blank" 
            style={{borderCollapse:'collapse',width:'100%'}} 
            name={`ifrm_${eid}`}/>
        return <div>{html}</div>
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
        );
    }
}

Iframe = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(Iframe);
export default Iframe;

