import React from 'react';
import { Row, Col } from 'antd';
import { intervalMap, openLinkUrl } from '../../util/common';
//集成登录元素
class OutterSysCom extends React.Component {
	render() {
		const { data, esetting } = this.props;
		const { disouttyName, disouttyimag, displaytype, linkmode, displayLayout } = esetting;
		const num = parseInt(displayLayout); //显示布局 几列
		const { imgs } = data;
		const list = data.data;
		const colArr = new Array;
		for (var i = 0; i < num; i++) {
			colArr.push(i);
		}
		let html = intervalMap(colArr, (i) => {
			if ('1' === displaytype) { //左图式
				const dhtml = new Array;
				const ihtml = new Array;
				list.map((obj, j) => {
					const n = i + j;
					const item = list[n];
					let img = imgs[n];
					if (item && img) {
						let onClickFn = openLinkUrl.bind(this,item.linkUrl,linkmode);
						if(item.routeUrl !== ''){
							onClickFn = () => weaHistory.push({pathname: '/main'+item.routeUrl});
						}
						if(disouttyName === '1') dhtml.push(<div style={{height:img.height,lineHeight:img.height+'px',marginLeft:'10px'}}><a style={{color:'#000000'}} href="javascript:void(0);" onClick={onClickFn}>{item.name}</a></div>);
						if(disouttyimag === '1') ihtml.push(<div><a href="javascript:void(0);" onClick={onClickFn}><img height={img.height} width={img.width} src={img.url}/></a></div>);
					}
				}, num);
				return <div>
				<Col span={1}>{ihtml}</Col>
          		<Col span={24/num - 1}>{dhtml}</Col>
      		</div>
			} else { //上图式
				let ohtml = intervalMap(list, (obj, j) => {
					const n = i + j;
					const item = list[n];
					let img = imgs[n];
					let dHtml = <div/>;
					let iHtml = <div/>;
					if (item && img) {
						let onClickFn = openLinkUrl.bind(this,item.linkUrl,linkmode);
						if(item.routeUrl !== ''){
							onClickFn = () => weaHistory.push({pathname: '/main'+item.routeUrl});
						}
						dHtml = '1' === disouttyName ? <div style={{height:img.height,lineHeight:img.height+'px'}}><a style={{color:'#000000'}} href="javascript:void(0);" onClick={onClickFn}>{item.name}</a></div> : null;
						iHtml = '1' === disouttyimag ? <div><a href="javascript:void(0);" onClick={onClickFn}><img height={img.height} width={img.width} src={img.url}/></a></div> : null;
					}
					return <div>
						{iHtml}{dHtml}
					</div>
				}, num);
				return <div style={{textAlign:'center'}}>
          		<Col span={24/num}>{ohtml}</Col>
      		</div>
			}
		});
		return <Row>{html}</Row>
	}
}

import { WeaErrorPage, WeaTools } from 'ecCom';
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return (
			<WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
		);
	}
}
OutterSysCom = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(OutterSysCom);
export default OutterSysCom;