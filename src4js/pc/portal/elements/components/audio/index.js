import { Spin } from 'antd';
import { WeaErrorPage, WeaTools } from 'ecCom';
const { ls } = WeaTools;
import Immutable from 'immutable';
const { is, fromJS } = Immutable;
import EHeader from '../common/EHeader';
import { NoRightCom, _isEmpty } from '../../util/common';
import AudioCom from './Audio';
//元素组件
class Audio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edata: null,
            refresh: false,
        }
        this.handleEData = this.handleEData.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }
    handleRefresh(){
        this.setState({
            refresh: true,
        });
        this.handleEData(this.props.config.params);
    }
    handleEData(params){
        const { eid } = params;
        this.setState({
            edata: ls.getJSONObj('edata-'+eid),
        });
        WeaTools.callApi('/api/portal/element/audio', 'POST', params).then(result=> {
            ls.set('edata-'+eid, result);
            this.setState({
                edata: result,
                refresh: false,
            });
       });
    }
    handleDelete(){
        this.setState({
            edata: null,
        });
        ls.set('edata-'+this.props.config.params.eid, null);
    }
    componentDidMount(){
        this.handleEData(this.props.config.params);
    }
    componentWillReceiveProps(nextProps){
        const { item, params, isHasRight } = this.props.config;
        const config = { item, params, isHasRight };
        const _config = { 
            item: nextProps.config.item, 
            params: nextProps.config.params, 
            isHasRight: nextProps.config.isHasRight,
        };
        if(!is(fromJS(config),fromJS(_config)) || nextProps.config.isRender){
            this.handleEData(nextProps.config.params);
        }
    }
    shouldComponentUpdate(nextProps, nextState){
        return !is(fromJS(this.state), fromJS(nextState))
    }
    render() {
        const { config } = this.props;
        const { edata, refresh } = this.state;
        if(!config || !edata) return <div></div>
        const { isHasRight, item } = config;
        const { eid, ebaseid, content, header, contentview } = item;
        let EContentHtml = <div/>;
        if(isHasRight){
            if(!_isEmpty(edata)) EContentHtml = <AudioCom data={edata.data} eid={eid} esetting={edata.esetting}/>;
        }else{
            EContentHtml = <NoRightCom/>
        }
        if (refresh) EContentHtml = <Spin>{EContentHtml}</Spin>
        return <div className="item" 
                style={{marginTop: '10px'}} 
                id={`item_${eid}`} 
                data-eid={eid} 
                data-ebaseid={ebaseid} 
                data-needRefresh={item.needRefresh} 
                data-cornerTop={item.cornerTop} 
                data-cornerTopRadian={item.cornerTopRadian} 
                data-cornerBottom={item.cornerBottom} 
                data-cornerBottomRadian = {item.cornerBottomRadian}>
                <EHeader config={config} handleDelete={this.handleDelete} handleRefresh={this.handleRefresh}/>
                <div className="setting" id={`setting_${eid}`}></div>
                <div className="content" id={`content_${eid}`} style={{width:'auto',_width:'100%'}}>
                    <div className="content_view" id={`content_view_id_${eid}`} style={contentview.style}>
                        {EContentHtml}
                    </div>
                    <div style={{textAlign: 'right'}} id={`footer_${eid}`}></div>
                </div>
            </div>;
    }
}


class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，流程元素加载异常，请联系管理员！" }/>
        );
    }
}
Audio = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(Audio);
export default Audio;



