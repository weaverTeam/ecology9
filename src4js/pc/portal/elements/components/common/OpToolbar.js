import React from 'react';
import { Spin, Popconfirm } from 'antd';
import { WeaErrorPage, WeaTools } from 'ecCom';
import ESetting from '../common/setting/ESetting';
import { isContain, onSetting, handleAttr, openMoreWin } from '../../util/common';
class OpToolbar extends React.Component {
        constructor(props) {
            super(props);
            this.state = {
                islock: '',
                icon: '',
            }
            this.handleLock = this.handleLock.bind(this);
            this.handleOnSetting = this.handleOnSetting.bind(this);
            this.handleDelete = this.handleDelete.bind(this);
        }
        handleDelete(){
            const { config, handleDelete } = this.props;
            const { eid, hpid, subCompanyId } = config.params;
            var group = $($("#item_"+eid).parents(".group")[0]);
            var flag = group.attr(handleAttr("areaflag",config.layoutid));
            var eids = "";
            group.find(".item").each(function(){
               if($(this).attr("data-eid") != eid) eids += $(this).attr("data-eid")+",";
            });
            WeaTools.callApi('/api/portal/elementtoolbar/delete', 'POST', { 
                hpid,
                eid,
                delFlag: flag,
                delAreaElement: eids,
                subCompanyId,
            }).then(result=> {
                if(result.status == "sucess")  {
                    handleDelete();
                } else {
                    message.error("删除元素失败，请您联系管理员！", 3);
                }
            });
        }
        handleLock(){
            const { islock } = this.state;
            const { eid, hpid, subCompanyId } = this.props.config.params;
            WeaTools.callApi('/api/portal/elementtoolbar/'+ islock ? 'unlock' : 'lock', 'POST', {
               eid,
               hpid,
               subCompanyId,
            }).then(result=> {
                if(result.status == "sucess")  {
                    this.setState({
                        islock: !islock,
                        icon: result.icon
                    });
                } else {
                    message.error((islock ? '解锁' : '锁定') + '元素失败，请您联系管理员！', 3);
                }
            });
        }
        handleOnSetting() {
            const { config, handleRefresh } = this.props;
            const { eid, ebaseid, hpid, subCompanyId } = config.params;
            const readyElement = [
                '1', '7', '8', '11', '12', '6', '13', '14', '15', '16', '9', '10', '17', '18', '19', '25', '29', '32', '34',
                'audio', 'blogStatus', 'favourite', 'Flash', 'menu', 'notice', 'picture', 'reportForm', 'searchengine', 'Slide',
                'video', 'weather', 'scratchpad', 'DataCenter', 'MyCalendar', 'Task', 'plan', 'jobsinfo', 'outterSys', 'contacts',
                'addwf', 'imgSlide', 'newNotice', 'OutData'
            ];
            if (isContain(readyElement, ebaseid)) {
                React.render(<div style={{textAlign: 'center'}}><Spin /></div>,  document.getElementById(`setting_${eid}`));
                WeaTools.callApi('/api/portal/setting/esetting', 'POST', {eid, ebaseid, hpid, subCompanyId}, 'json').then(function (result) {
                    result.hpid = hpid;
                    result.subCompanyId = subCompanyId;
                    result.eid = eid;
                    result.ebaseid = ebaseid;
                    React.render(<ESetting data={result} handleRefresh={handleRefresh}/>,  document.getElementById(`setting_${eid}`));
                });
            } else {
                onSetting(eid, ebaseid, hpid, subCompanyId);
            }
        }
        render() {
           const { config, clsname, islock, icon, handleRefresh } = this.props;
           const { eid, ebaseid, header } = config.item;
           const { toolbar } = header;
           const { lock, refresh, setting, close, more } = toolbar;
           return <div className={'optoolbar ' + clsname} id={`toolbar_${eid}`}>
                  <ul>
                    {lock ? <li style={{width:'11px'}}>
                      <Popconfirm title="此操作可能花较长的时间，是否继续？" onConfirm={this.handleLock}>
                        <a data-status={islock?'locked':'unlocked'} href="javascript:void(0);" title={lock.title}>
                        <img src={icon} border="0"/></a>
                        </Popconfirm>
                    </li> : ''}
                    {refresh ? <li style={{width:'11px'}}>
                      <a href="javascript:void(0);" name="refreshbtn" id={`refresh_${eid}`} onClick={handleRefresh} title={refresh.title}>
                        <img src={refresh.img} border="0"/></a>
                      </li> : ''}
                    {setting ? <li style={{width:'12px'}}>
                      <a href="javascript:void(0)" onClick={this.handleOnSetting} title={setting.title}>
                        <img className="img_setting" src={setting.img} border="0"/></a>
                    </li> : ''}
                    {close ? <li style={{width:'12px'}}>
                        <Popconfirm title="此元素被删除后将不能被恢复，是否继续？" onConfirm={this.handleDelete}>
                          <a href="javascript:void(0);" title={close.title}>
                            <img src={close.img} border="0"/></a>
                        </Popconfirm>
                    </li> : ''}
                    {more ? <li style={{width:'34px'}}>
                        <a id={`more_${eid}`} href="javascript:void(0);" data-morehref={more.morehref} onClick={openMoreWin.bind(this,eid)}>
                          <img className="img_more" id="imgMore" border="0" src={more.img} title={more.title}/></a>
                    </li> : ''}
                </ul>
              </div>
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
           return (<WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
        );
    }
}
OpToolbar = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(OpToolbar);
export default OpToolbar;



