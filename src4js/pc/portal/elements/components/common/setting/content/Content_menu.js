import React from 'react';
import {Form, Select, Radio} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import FormItem4LinkMode from './base/FormItem4LinkMode';

// 自定义菜单
export default class Content_menu extends React.Component {
    state = {
        eMenuFrom: this.props.data.eContent.eMenuFrom,
        eMenuStyleType: this.props.data.eContent.eMenuStyleType,
        eMenuHStyle: this.props.data.eContent.eMenuHStyle,
        eMenuVStyle: this.props.data.eContent.eMenuVStyle,
    };

    onMenuStyleTypeChange(e) {
        this.setState({
            eMenuStyleType: e.target.value
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4MenuFrom = <div></div>;
        let eFormItem4MenuStyle = <div></div>;
        if (eShareLevel == '2') {
            const eMenuFromProps = getFieldProps('eContentMenuFrom', {initialValue: this.state.eMenuFrom.selected});
            const eMenuFromOptions = this.state.eMenuFrom.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });
            eFormItem4MenuFrom = (
                <FormItem label="自定义菜单" {...formItemLayout}>
                    <Select size="default" style={{width: '80%'}} {...eMenuFromProps}>
                        {eMenuFromOptions}
                    </Select>
                </FormItem>
            );

            const eMenuStyleTypeProps = getFieldProps('eContentMenuStyleType', {initialValue: this.state.eMenuStyleType});
            const eMenuHStyleProps = getFieldProps('eContentMenuHStyle', {initialValue: this.state.eMenuHStyle.selected});
            const eMenuHStyleOptions = this.state.eMenuHStyle.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });
            const eMenuVStyleProps = getFieldProps('eContentMenuVStyle', {initialValue: this.state.eMenuVStyle.selected});
            const eMenuVStyleOptions = this.state.eMenuVStyle.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });
            eFormItem4MenuStyle = (
                <FormItem label="菜单样式" {...formItemLayout}>
                    <RadioGroup style={{width: '100%'}} {...eMenuStyleTypeProps} onChange={this.onMenuStyleTypeChange.bind(this)}>
                        <Radio value="menuh">横向样式(仅支持一级菜单)</Radio>
                        <Radio value="menuv">纵向样式(仅支持二级菜单)</Radio>
                    </RadioGroup>
                    {this.state.eMenuStyleType == 'menuh' ?
                        <Select size="default" style={{width: '80%', marginTop: '5px'}} {...eMenuHStyleProps}>
                            {eMenuHStyleOptions}
                        </Select> :
                        <Select size="default" style={{width: '80%', marginTop: '5px'}} {...eMenuVStyleProps}>
                            {eMenuVStyleOptions}
                        </Select>
                    }
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    <FormItem4LinkMode {...this.props}/>
                    {eFormItem4MenuFrom}
                    {eFormItem4MenuStyle}
                </FormGroup>
            </Form>
        );
    }
}