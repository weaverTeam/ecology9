import React from 'react';
import {Modal, Input, Button, Table} from 'antd';

import MaterialLibrary from './base/MaterialLibrary';

// 图片元素详细设置
export default class Content_picture_detail extends React.Component {
    state = {
        imageSource: this.props.eImageSource
    };

    onOk() {
        this.props.onOk(this.state.imageSource);
    }

    onCancel() {
        this.props.onCancel();
    }

    onDetailImgChange(key, type, value) {
        let imageSource = this.state.imageSource;
        imageSource[key][type] = value;

        this.setState({
            imageSource: imageSource
        });
    }

    onDetailTextChange(key, type, e) {
        let imageSource = this.state.imageSource;
        imageSource[key][type] = e.target.value;

        this.setState({
            imageSource: imageSource
        });
    }

    onDelete(key) {
        let imageSource = this.state.imageSource;
        for (let i = 0, len = imageSource.length; i < len; i++) {
            if (imageSource[i].key == key) {
                imageSource.splice(i, 1);
                break;
            }
        }

        this.setState({
            imageSource: imageSource
        });
    }

    onAdd() {
        let imageSource = this.state.imageSource;
        let key = imageSource.length;

        imageSource.push({
            "key": key,
            "src": "",
            "title": "",
            "link": "",
            "orderNum": "0"
        });

        this.setState({
            imageSource: imageSource
        });
    }

    render() {
        const columns = [{
            title: '图片',
            dataIndex: 'src',
            render: (text, record) => (
                <MaterialLibrary value={text} onChange={this.onDetailImgChange.bind(this, record.key, 'src')}/>
            )
        }, {
            title: '图片名称',
            dataIndex: 'title',
            render: (text, record) => (
                <Input size="default" defaultValue={text} onChange={this.onDetailTextChange.bind(this, record.key, 'title')}/>
            )
        }, {
            title: '图片链接 ',
            dataIndex: 'link',
            render: (text, record) => (
                <Input size="default" defaultValue={text} onChange={this.onDetailTextChange.bind(this, record.key, 'link')}/>
            )
        }, {
            title: '顺序',
            dataIndex: 'orderNum',
            width: '100',
            render: (text, record) => (
                <Input size="default" defaultValue={text} onChange={this.onDetailTextChange.bind(this, record.key, 'orderNum')}/>
            )
        }, {
            title: '操作',
            dataIndex: 'operation',
            render: (text, record) => (
                <span className="esetting-dso-button" onClick={this.onDelete.bind(this, record.key)}>删除</span>
            )
        }];

        let dataSource = this.state.imageSource;

        const buttonDivStyle = {
            height: '30px',
            lineHeight: '25px',
            paddingRight: '20px',
            borderBottom: '1px solid #e9e9e9',
            textAlign: 'right'
        };

        return (
            <Modal
                title="详细设置"
                width="80%"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <div style={buttonDivStyle}>
                    <Button type="primary" size="small" onClick={this.onAdd.bind(this)}>添加</Button>
                </div>
                <Table className="esetting-table" columns={columns} dataSource={dataSource} size="middle" pagination={false}/>
            </Modal>
        );
    }
}