import React from 'react';
import {Form, Input, Checkbox, Radio} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import MaterialLibrary from './base/MaterialLibrary';

// 视频元素
export default class Content_video extends React.Component {
    state = {
        eVideoShow: this.props.data.eContent.eVideoShow,
        eVideoForm: this.props.data.eContent.eVideoForm
    };

    onEVideoFormChange(type, value) {
        let eVideoForm = this.state.eVideoForm;
        eVideoForm[type] = value;

        this.setState({
            eVideoForm: eVideoForm
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4ShowSetting = <div></div>;
        let eFormItem4VideoFrom = <div></div>;
        if (eShareLevel == '2') {

            const eVideoShow_height = getFieldProps('eContentVideoShow_height', {initialValue: this.state.eVideoShow.height});
            const eVideoShow_showFullScreen = getFieldProps('eContentVideoShow_showFullScreen', {initialValue: this.state.eVideoShow.showFullScreen, valuePropName: 'checked'});
            const eVideoShow_autoPlay = getFieldProps('eContentVideoShow_autoPlay', {initialValue: this.state.eVideoShow.autoPlay, valuePropName: 'checked'});
            eFormItem4ShowSetting = (
                <FormItem label="显示设置" {...formItemLayout}>
                    <span>高度：</span>
                    <Input size="default" style={{width: '60px'}} {...eVideoShow_height}/>
                    <Checkbox size="default" style={{marginLeft: '10px'}} {...eVideoShow_showFullScreen}>是否可全屏显示</Checkbox>
                    <Checkbox size="default" style={{marginLeft: '10px'}} {...eVideoShow_autoPlay}>自动播放</Checkbox>
                </FormItem>
            );

            const eVideoForm_type = getFieldProps('eContentVideoForm_type', {initialValue: this.state.eVideoForm.type});
            getFieldProps('eContentVideoForm_src', {initialValue: this.state.eVideoForm.src});
            getFieldProps('eContentVideoForm_url', {initialValue: this.state.eVideoForm.url});
            const radioStyle = {
                display: 'block',
                height: '35px',
                marginRight: '0'
            };
            const labelStyle = {
                display: 'inline-block',
                width: '60px'
            };
            const mlStyle = {
                display: 'inline-block',
                width: '350px'
            };
            eFormItem4VideoFrom = (
                <FormItem label="视频来源" {...formItemLayout}>
                    <RadioGroup style={{width: '100%'}} {...eVideoForm_type}>
                        <Radio style={radioStyle} value="1">
                            <span style={labelStyle}>选择文件</span>
                            <div style={mlStyle}>
                                <MaterialLibrary value={this.state.eVideoForm.src} onChange={this.onEVideoFormChange.bind(this, 'src')}/>
                            </div>
                        </Radio>
                        <Radio style={radioStyle} value="2">
                            <span style={labelStyle}>选择文件</span>
                            <div style={mlStyle}>
                                <MaterialLibrary value={this.state.eVideoForm.url} onChange={this.onEVideoFormChange.bind(this, 'url')}/>
                            </div>
                        </Radio>
                    </RadioGroup>
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    {eFormItem4ShowSetting}
                    {eFormItem4VideoFrom}
                </FormGroup>
            </Form>
        );
    }
}