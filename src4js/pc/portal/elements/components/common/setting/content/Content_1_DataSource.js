import React from 'react';
import {Modal, Form, Input, Select, Button, Tooltip, Icon} from 'antd';
const FormItem = Form.Item;

import FormGroup from './base/FormGroup';

// RSS阅读器，数据来源
class Content_1_DataSource extends React.Component {
    state = {
        id: this.props.tabData.id,
        title: this.props.tabData.title,
        showContent: this.props.tabData.showContent,
        position: this.props.tabData.position || this.props.tabNew.position.selected,
        searchGroup: this.props.tabData.searchGroup || this.props.tabNew.searchGroup.selected,
        readType: this.props.tabData.readType || this.props.tabNew.readType.selected
    };

    onOk(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.onOk(values);
        });
    }

    onCancel() {
        this.props.onCancel();
    }

    render() {
        const {getFieldProps} = this.props.form;
        const formItemLayout = this.props.formItemLayout;
        const tabNew = this.props.tabNew;

        getFieldProps('id', {initialValue: this.state.id});

        const titleProps = getFieldProps('title', {
            initialValue: this.state.title,
            rules: [
                {required: true, message: '请填写标题'},
            ]
        });

        const showContentProps = getFieldProps('showContent', {initialValue: this.state.showContent});

        const positionProps = getFieldProps('position', {initialValue: this.state.position});
        const positionOptions = tabNew.position.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        const searchGroupProps = getFieldProps('searchGroup', {initialValue: this.state.searchGroup});
        const searchGroupOptions = tabNew.searchGroup.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        const readTypeProps = getFieldProps('readType', {initialValue: this.state.readType});
        const readTypeOptions = tabNew.readType.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        return (
            <Modal
                title="内容设置"
                width="660"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <Form horizontal className="esetting-form">
                    <FormGroup title="设置项">
                        <FormItem label="标题" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...titleProps}/>
                        </FormItem>
                        <FormItem label="显示内容" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...showContentProps}/>
                        </FormItem>
                        <FormItem label="所在位置" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...positionProps}>
                                {positionOptions}
                            </Select>
                            <span style={{marginLeft: '10px'}}>（显示内容为关键字时此选项才起作用）</span>
                        </FormItem>
                        <FormItem label="查询组合" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...searchGroupProps}>
                                {searchGroupOptions}
                            </Select>
                            <Tooltip placement="bottom" title="关键字之间以空格分隔，选择 and 搜索包含全部关键词的信息；选择 or 搜索包含任意一个关键词的信息。">
                                <Icon type="question-circle" style={{marginLeft: '10px', color: '#fa0'}}/>
                            </Tooltip>
                        </FormItem>
                        <FormItem label="读取方式" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...readTypeProps}>
                                {readTypeOptions}
                            </Select>
                        </FormItem>
                    </FormGroup>
                </Form>
            </Modal>
        );
    }
}

Content_1_DataSource = Form.create()(Content_1_DataSource);

export default Content_1_DataSource;