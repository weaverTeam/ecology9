import React from 'react';
import {Form, Input, Radio} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import FormItem4ShowNum from './base/FormItem4ShowNum';
import FormItem4LinkMode from './base/FormItem4LinkMode';
import FormItem4Field from './base/FormItem4Field';

// 未读文档
export default class Content_6 extends React.Component {
    state = {
        docSourceType: this.props.data.eContent.eDocSourceType.selected,
        docCatalogs: this.props.data.eContent.eDocCatalogs,
        virtualCatalogs: this.props.data.eContent.eVirtualCatalogs
    };

    render() {
        const {eShareLevel, eContent} = this.props.data;
        const {eDocSourceType} = eContent;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4DocFrom = <div></div>;
        if (eShareLevel == '2') {
            const docCatalogsProps = getFieldProps('eContentDocCatalogs', {initialValue: this.state.docCatalogs});
            const virtualCatalogsProps = getFieldProps('eContentVirtualCatalogs', {initialValue: this.state.virtualCatalogs});
            const eDocSourceTypeProps = getFieldProps('eContentDocSourceType', {initialValue: this.state.docSourceType});
            const eDocSourceTypeOptions = eDocSourceType.options.map((item, index) => {
                const fieldProps = {"docCatalogs": docCatalogsProps, "virtualCatalogs": virtualCatalogsProps}[item.type];
                return (
                    <Radio key={index} style={{display: 'block'}} value={item.value}>
                        {item.label}
                        {item.type != 'allDocs' ?
                            <div style={{display: 'inline-block', width: '410px', margin: '5px', verticalAlign: 'middle'}}>
                                <Input type="text" size="default" {...fieldProps}/>
                            </div> : ''
                        }
                    </Radio>
                );
            });

            eFormItem4DocFrom = (
                <FormItem label="文档来源" {...formItemLayout}>
                    <RadioGroup {...eDocSourceTypeProps}>
                        {eDocSourceTypeOptions}
                    </RadioGroup>
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    <FormItem4ShowNum {...this.props}/>
                    <FormItem4LinkMode {...this.props}/>
                    <FormItem4Field {...this.props}/>
                    {eFormItem4DocFrom}
                </FormGroup>
            </Form>
        );
    }
}