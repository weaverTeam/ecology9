import React from 'react';
import {Modal, Form, Input, Select, Radio, Button} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';

// 文档中心，数据来源
class Content_7_DataSource extends React.Component {
    state = {
        id: this.props.tabData.id,
        title: this.props.tabData.title,
        topDocs: this.props.tabData.topDocs,
        docSourceType: this.props.tabData.docSourceType || this.props.tabNew.docSourceType.selected,
        newsCenters: this.props.tabData.newsCenters,
        docCatalogs: this.props.tabData.docCatalogs,
        virtualCatalogs: this.props.tabData.virtualCatalogs,
        appointDocs: this.props.tabData.appointDocs,
        showMode: this.props.tabData.showMode || this.props.tabNew.showMode.selected,
        scrollDirection: this.props.tabData.scrollDirection || this.props.tabNew.scrollDirection.selected,
        isOpenAttachment: this.props.tabData.isOpenAttachment || this.props.tabNew.isOpenAttachment.selected
    };

    onOk(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.onOk(values);
        });
    }

    onCancel() {
        this.props.onCancel();
    }

    render() {
        const {getFieldProps} = this.props.form;
        const formItemLayout = this.props.formItemLayout;
        const tabNew = this.props.tabNew;

        getFieldProps('id', {initialValue: this.state.id});

        const titleProps = getFieldProps('title', {
            initialValue: this.state.title,
            rules: [
                {required: true, message: '请填写标题'},
            ]
        });

        const topDocsProps = getFieldProps('topDocs', {initialValue: this.state.topDocs});

        const newsCentersProps = getFieldProps('newsCenters', {initialValue: this.state.newsCenters});
        const docCatalogsProps = getFieldProps('docCatalogs', {initialValue: this.state.docCatalogs});
        const virtualCatalogsProps = getFieldProps('virtualCatalogs', {initialValue: this.state.virtualCatalogs});
        const appointDocsProps = getFieldProps('appointDocs', {initialValue: this.state.appointDocs});
        const docSourceTypeProps = getFieldProps('docSourceType', {initialValue: this.state.docSourceType});
        const docSourceTypeOptions = tabNew.docSourceType.options.map((item, index) => {
            const fieldProps = {"newsCenters": newsCentersProps, "docCatalogs": docCatalogsProps, "virtualCatalogs": virtualCatalogsProps, "appointDocs": appointDocsProps}[item.type];
            return (
                <Radio key={index} style={{display: 'inline-block'}} value={item.value}>
                    {item.label}
                    <div style={{display: 'inline-block', width: '410px', margin: '5px', verticalAlign: 'middle'}}>
                        <Input type="text" size="default" {...fieldProps}/>
                    </div>
                </Radio>
            );
        });

        const showModeProps = getFieldProps('showMode', {initialValue: this.state.showMode});
        const showModeOptions = tabNew.showMode.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        const scrollDirectionProps = getFieldProps('scrollDirection', {initialValue: this.state.scrollDirection});
        const scrollDirectionOptions = tabNew.scrollDirection.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        const isOpenAttachmentProps = getFieldProps('isOpenAttachment', {initialValue: this.state.isOpenAttachment});
        const isOpenAttachmentOptions = tabNew.isOpenAttachment.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        return (
            <Modal
                title="内容设置"
                width="660"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <Form horizontal className="esetting-form">
                    <FormGroup title="设置项">
                        <FormItem label="标题" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '485px'}} {...titleProps}/>
                        </FormItem>
                        <FormItem label="文档置顶" {...formItemLayout}>
                            <div style={{width: '485px'}}>
                                <Input type="text" size="default" style={{width: '485px'}} {...topDocsProps}/>
                            </div>
                        </FormItem>
                        <FormItem label="文档来源" {...formItemLayout}>
                            <RadioGroup {...docSourceTypeProps}>
                                {docSourceTypeOptions}
                            </RadioGroup>
                        </FormItem>
                        <FormItem label="显示方式" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...showModeProps}>
                                {showModeOptions}
                            </Select>
                        </FormItem>
                        <FormItem label="滚动方向" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...scrollDirectionProps}>
                                {scrollDirectionOptions}
                            </Select>
                        </FormItem>
                        <FormItem label="直接打开附件" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...isOpenAttachmentProps}>
                                {isOpenAttachmentOptions}
                            </Select>
                            <span style={{marginLeft: '10px'}}>（当文档内容为空，且此文档仅有一个附件时）</span>
                        </FormItem>
                    </FormGroup>
                </Form>
            </Modal>
        );
    }
}

Content_7_DataSource = Form.create()(Content_7_DataSource);

export default Content_7_DataSource;