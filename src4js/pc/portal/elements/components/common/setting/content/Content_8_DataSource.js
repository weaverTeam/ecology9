import React from 'react';
import {Modal, Form, Input, Select, Checkbox, Button} from 'antd';
const FormItem = Form.Item;

import FormGroup from './base/FormGroup';

// 流程中心，数据来源
class Content_8_DataSource extends React.Component {
    state = {
        id: this.props.tabData.id,
        title: this.props.tabData.title,
        viewType: this.props.tabData.viewType || this.props.tabNew.viewType.selected,
        isComplete: this.props.tabData.isComplete || this.props.tabNew.isComplete.selected,
        showCopy: this.props.tabData.showCopy,
        showCount: this.props.tabData.showCount,
        isExclude: this.props.tabData.isExclude || this.props.tabNew.isExclude.selected,
        workflowSource: this.props.tabData.workflowSource
    };

    onOk(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.onOk(values);
        });
    }

    onCancel() {
        this.props.onCancel();
    }

    onViewTypeChange(value) {
        this.setState({
            viewType: value
        });
    }

    render() {
        const {getFieldProps} = this.props.form;
        const formItemLayout = this.props.formItemLayout;
        const tabNew = this.props.tabNew;

        const viewType = this.state.viewType;

        getFieldProps('id', {initialValue: this.state.id});

        const titleProps = getFieldProps('title', {
            initialValue: this.state.title,
            rules: [
                {required: true, message: '请填写标题'},
            ]
        });

        const viewTypeProps = getFieldProps('viewType', {initialValue: viewType});
        const viewTypeOptions = tabNew.viewType.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        let isComplete = <span></span>;
        if (viewType == '4') {
            const isCompleteProps = getFieldProps('isComplete', {initialValue: this.state.isComplete});
            const isCompleteOptions = tabNew.isComplete.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });

            isComplete = (
                <Select size="default" style={{width: '100px', marginLeft: '10px'}} {...isCompleteProps}>
                    {isCompleteOptions}
                </Select>
            );
        }

        let showCopy = <span></span>;
        if (viewType == '1') {
            const showCopyProps = getFieldProps('showCopy', {initialValue: this.state.showCopy, valuePropName: 'checked'});

            showCopy = (
                <Checkbox size="default" style={{marginLeft: '10px'}} {...showCopyProps}>显示抄送事宜</Checkbox>
            );
        }

        let showCount = <span></span>;
        if (viewType == '1' || viewType == '2' || viewType == '3' || viewType == '4' || viewType == '5' || viewType == '10') {
            const showCountProps = getFieldProps('showCount', {initialValue: this.state.showCount, valuePropName: 'checked'});

            showCount = (
                <Checkbox size="default" style={{marginLeft: '10px'}} {...showCountProps}>显示未读数</Checkbox>
            );
        }

        const isExcludeProps = getFieldProps('isExclude', {initialValue: this.state.isExclude});
        const isExcludeOptions = tabNew.isExclude.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        return (
            <Modal
                title="内容设置"
                width="660"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <Form horizontal className="esetting-form">
                    <FormGroup title="设置项">
                        <FormItem label="标题" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...titleProps}/>
                        </FormItem>
                        <FormItem label="查看类型" {...formItemLayout}>
                            <Select size="default" style={{width: '100px'}} {...viewTypeProps} onChange={this.onViewTypeChange.bind(this)}>
                                {viewTypeOptions}
                            </Select>
                            {isComplete}
                            {showCopy}
                            {showCount}
                        </FormItem>
                        <FormItem label="流程来源" {...formItemLayout}>
                            <Select size="default" style={{width: '80%'}} {...isExcludeProps}>
                                {isExcludeOptions}
                            </Select>
                        </FormItem>
                    </FormGroup>
                </Form>
            </Modal>
        );
    }
}

Content_8_DataSource = Form.create()(Content_8_DataSource);

export default Content_8_DataSource;