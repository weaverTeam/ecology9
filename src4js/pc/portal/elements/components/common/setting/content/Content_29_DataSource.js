import React from 'react';
import {Modal, Form, Input, Button} from 'antd';
const FormItem = Form.Item;

import FormGroup from './base/FormGroup';

// 自定义页面，数据来源
class Content_29_DataSource extends React.Component {
    state = {
        id: this.props.tabData.id,
        title: this.props.tabData.title,
        href: this.props.tabData.href,
        moreHref: this.props.tabData.moreHref,
        pageHeight: this.props.tabData.pageHeight
    };

    onOk(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.onOk(values);
        });
    }

    onCancel() {
        this.props.onCancel();
    }

    render() {
        const {getFieldProps} = this.props.form;
        const formItemLayout = this.props.formItemLayout;

        getFieldProps('id', {initialValue: this.state.id});

        const titleProps = getFieldProps('title', {
            initialValue: this.state.title,
            rules: [
                {required: true, message: '请填写标题'},
            ]
        });

        const hrefProps = getFieldProps('href', {initialValue: this.state.href});

        const moreHrefProps = getFieldProps('moreHref', {initialValue: this.state.moreHref});

        const pageHeightProps = getFieldProps('pageHeight', {initialValue: this.state.pageHeight});

        return (
            <Modal
                title="内容设置"
                width="660"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <Form horizontal className="esetting-form">
                    <FormGroup title="设置项">
                        <FormItem label="标题" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...titleProps}/>
                        </FormItem>
                        <FormItem label="引用地址" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...hrefProps}/>
                            <div>外部地址请加上http://</div>
                        </FormItem>
                        <FormItem label="More地址" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...moreHrefProps}/>
                            <div>外部地址请加上http://</div>
                        </FormItem>
                        <FormItem label="页面高度" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '100px'}} {...pageHeightProps}/> px
                        </FormItem>
                    </FormGroup>
                </Form>
            </Modal>
        );
    }
}

Content_29_DataSource = Form.create()(Content_29_DataSource);

export default Content_29_DataSource;
