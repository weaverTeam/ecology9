import React from 'react';
import {Form, Input, Select, Radio, Button} from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import Content_Slide_detail from './Content_Slide_detail';

// 幻灯片
export default class Content_Slide extends React.Component {
    state = {
        eAutoScrollTime: this.props.data.eContent.eAutoScrollTime,
        eChangeEffect: this.props.data.eContent.eChangeEffect,
        eSmoothMoveTime: this.props.data.eContent.eSmoothMoveTime,
        eNavDisplayPosition: this.props.data.eContent.eNavDisplayPosition,
        eSlideDetail: this.props.data.eContent.eSlideDetail,
        detailModalVisible: false
    };

    onOk(value) {
        this.setState({
            eSlideDetail: value,
            detailModalVisible: false
        });

        this.props.form.setFieldsValue('eContentSlideDetail', {initialValue: value});
    }

    onCancel() {
        this.setState({
            detailModalVisible: false
        });
    }

    onDetailSetting() {
        this.setState({
            detailModalVisible: true
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4AutoScrollTime = <div></div>;
        let eFormItem4ChangeEffect = <div></div>;
        let eFormItem4SmoothMoveTime = <div></div>;
        let eFormItem4NavDisplayPosition = <div></div>;
        if (eShareLevel == '2') {
            const eAutoScrollTimeProps = getFieldProps('eContentAutoScrollTime', {initialValue: this.state.eAutoScrollTime});
            eFormItem4AutoScrollTime = (
                <FormItem label="自动滚动时间" {...formItemLayout}>
                    <Input size="default" style={{width: '150px'}} {...eAutoScrollTimeProps}/><span>&nbsp;毫秒</span>
                </FormItem>
            );

            const eChangeEffectProps = getFieldProps('eContentChangeEffect', {initialValue: this.state.eChangeEffect.selected});
            const eChangeEffectOptions = this.state.eChangeEffect.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });
            eFormItem4ChangeEffect = (
                <FormItem label="切换效果" {...formItemLayout}>
                    <Select size="default" style={{width: '150px'}} {...eChangeEffectProps}>
                        {eChangeEffectOptions}
                    </Select>
                </FormItem>
            );

            const eSmoothMoveTimeProps = getFieldProps('eContentSmoothMoveTime', {initialValue: this.state.eSmoothMoveTime});
            eFormItem4SmoothMoveTime = (
                <FormItem label="平滑移动时间" {...formItemLayout}>
                    <Input size="default" style={{width: '150px'}} {...eSmoothMoveTimeProps}/><span>&nbsp;毫秒</span>
                </FormItem>
            );

            getFieldProps('eContentSlideDetail', {initialValue: this.state.eSlideDetail});
            let modal2Detail = <div></div>;
            if (this.state.detailModalVisible) {
                const detailProps = {
                    eSlideDetail: this.state.eSlideDetail,
                    onOk: this.onOk.bind(this),
                    onCancel: this.onCancel.bind(this)
                };
                modal2Detail = <Content_Slide_detail {...detailProps}/>
            }

            const eNavDisplayPositionProps = getFieldProps('eContentNavDisplayPosition', {initialValue: this.state.eNavDisplayPosition});
            eFormItem4NavDisplayPosition = (
                <FormItem label="导航显示位置" {...formItemLayout}>
                    <RadioGroup style={{width: '100%'}} {...eNavDisplayPositionProps}>
                        <Radio value="1">左</Radio>
                        <Radio value="2">右</Radio>
                        <Radio value="3">下</Radio>
                    </RadioGroup>
                    <Button type="primary" size="small" onClick={() => this.onDetailSetting(true)}>详细设置</Button>
                    {modal2Detail}
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    {eFormItem4AutoScrollTime}
                    {eFormItem4ChangeEffect}
                    {eFormItem4SmoothMoveTime}
                    {eFormItem4NavDisplayPosition}
                </FormGroup>
            </Form>
        );
    }
}