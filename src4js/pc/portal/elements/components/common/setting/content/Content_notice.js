import React from 'react';
import {Form, Input, Select} from 'antd';
const FormItem = Form.Item;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import MaterialLibrary from './base/MaterialLibrary';

// 公告栏元素
export default class Content_notice extends React.Component {
    state = {
        eNoticeIcon: this.props.data.eContent.eNoticeIcon,
        eNoticeTitle: this.props.data.eContent.eNoticeTitle,
        eNoticeContent: this.props.data.eContent.eNoticeContent,
        eNoticeScrollDirection: this.props.data.eContent.eNoticeScrollDirection,
        eNoticeScrollSpeed: this.props.data.eContent.eNoticeScrollSpeed,
        eNoticeIsText: this.props.data.eContent.eNoticeIsText
    };

    onENoticeIconChange(value) {
        this.setState({
            eNoticeIcon: value
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4NoticeIcon = <div></div>;
        let eFormItem4NoticeTitle = <div></div>;
        let eFormItem4NoticeContent = <div></div>;
        let eFormItem4NoticeScrollDirection = <div></div>;
        let eFormItem4NoticeScrollSpeed = <div></div>;
        let eFormItem4NoticeIsText = <div></div>;
        if (eShareLevel == '2') {
            getFieldProps('eContentNoticeIcon', {initialValue: this.state.eNoticeIcon});
            eFormItem4NoticeIcon = (
                <FormItem label="图标" {...formItemLayout}>
                    <div style={{width: '80%'}}>
                        <MaterialLibrary value={this.state.eNoticeIcon} onChange={this.onENoticeIconChange.bind(this)}/>
                    </div>
                </FormItem>
            );

            const eNoticeTitleProps = getFieldProps('eContentNoticeTitle', {initialValue: this.state.eNoticeTitle});
            eFormItem4NoticeTitle = (
                <FormItem label="标题" {...formItemLayout}>
                    <Input type="text" size="default" style={{width: '80%'}} {...eNoticeTitleProps}/>
                </FormItem>
            );

            const eNoticeContentProps = getFieldProps('eContentNoticeContent', {initialValue: this.state.eNoticeContent});
            eFormItem4NoticeContent = (
                <FormItem label="内容" {...formItemLayout}>
                    <Input type="text" size="default" style={{width: '80%'}} {...eNoticeContentProps}/>
                </FormItem>
            );

            const eNoticeScrollDirectionProps = getFieldProps('eContentNoticeScrollDirection', {initialValue: this.state.eNoticeScrollDirection.selected});
            const eNoticeScrollDirectionOptions = this.state.eNoticeScrollDirection.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });
            eFormItem4NoticeScrollDirection = (
                <FormItem label="滚动方向" {...formItemLayout}>
                    <Select size="default" style={{width: '80px'}} {...eNoticeScrollDirectionProps}>
                        {eNoticeScrollDirectionOptions}
                    </Select>
                </FormItem>
            );

            const eNoticeScrollSpeedProps = getFieldProps('eContentNoticeScrollSpeed', {initialValue: this.state.eNoticeScrollSpeed});
            eFormItem4NoticeScrollSpeed = (
                <FormItem label="滚动速度" {...formItemLayout}>
                    <Input type="text" size="default" style={{width: '80px'}} {...eNoticeScrollSpeedProps}/><span>&nbsp;毫秒</span>
                </FormItem>
            );

            const eNoticeIsTextProps = getFieldProps('eContentNoticeIsText', {initialValue: this.state.eNoticeIsText.selected});
            const eNoticeIsTextOptions = this.state.eNoticeIsText.options.map((item, index) => {
                return <Option key={index} value={item.key}>{item.value}</Option>;
            });
            eFormItem4NoticeIsText = (
                <FormItem label="纯文本格式" {...formItemLayout}>
                    <Select size="default" style={{width: '80px'}} {...eNoticeIsTextProps}>
                        {eNoticeIsTextOptions}
                    </Select>
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    {eFormItem4NoticeIcon}
                    {eFormItem4NoticeTitle}
                    {eFormItem4NoticeContent}
                    {eFormItem4NoticeScrollDirection}
                    {eFormItem4NoticeScrollSpeed}
                    {eFormItem4NoticeIsText}
                </FormGroup>
            </Form>
        );
    }
}