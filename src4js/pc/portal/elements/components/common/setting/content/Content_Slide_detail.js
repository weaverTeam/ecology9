import React from 'react';
import {Modal, Input, Button, Table} from 'antd';

import MaterialLibrary from './base/MaterialLibrary';

// 幻灯片详细设置
export default class Content_Slide_detail extends React.Component {
    state = {
        slideDetail: this.props.eSlideDetail
    };

    onOk() {
        this.props.onOk(this.state.slideDetail);
    }

    onCancel() {
        this.props.onCancel();
    }

    onDetailImgChange(key, type, value) {
        let slideDetail = this.state.slideDetail;
        slideDetail[key][type] = value;

        this.setState({
            slideDetail: slideDetail
        });
    }

    onDetailTextChange(key, type, e) {
        let slideDetail = this.state.slideDetail;
        slideDetail[key][type] = e.target.value;

        this.setState({
            slideDetail: slideDetail
        });
    }

    render() {
        const columns = [{
            title: '主图片',
            dataIndex: 'mainImage',
            render: (text, record) => (
                <MaterialLibrary value={text} onChange={this.onDetailImgChange.bind(this, record.key, 'mainImage')}/>
            )
        }, {
            title: '导航样式背景图',
            dataIndex: 'bgImage',
            render: (text, record) => (
                <MaterialLibrary value={text} onChange={this.onDetailImgChange.bind(this, record.key, 'bgImage')}/>
            )
        }, {
            title: '导航样式前景图',
            dataIndex: 'fgImage',
            render: (text, record) => (
                <MaterialLibrary value={text} onChange={this.onDetailImgChange.bind(this, record.key, 'fgImage')}/>
            )
        }, {
            title: '标题',
            dataIndex: 'title',
            render: (text, record) => (
                <Input size="default" name="title" defaultValue={text} onChange={this.onDetailTextChange.bind(this, record.key, 'title')}/>
            )
        }, {
            title: '描述',
            dataIndex: 'desc',
            render: (text, record) => (
                <Input size="default" name="desc" defaultValue={text} onChange={this.onDetailTextChange.bind(this, record.key, 'desc')}/>
            )
        }, {
            title: '图片链接',
            dataIndex: 'link',
            render: (text, record) => (
                <Input size="default" name="link" defaultValue={text} onChange={this.onDetailTextChange.bind(this, record.key, 'link')}/>
            )
        }];

        let dataSource = this.state.slideDetail;

        return (
            <Modal
                title="详细设置"
                width="80%"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <Table className="esetting-table" columns={columns} dataSource={dataSource} size="middle" pagination={false}/>
            </Modal>
        );
    }
}