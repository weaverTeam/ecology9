import React from 'react';
import {Form, Input, Checkbox, Radio} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import MaterialLibrary from './base/MaterialLibrary';

// 音频元素
export default class Content_audio extends React.Component {
    state = {
        eAudioShow: this.props.data.eContent.eAudioShow,
        eAudioForm: this.props.data.eContent.eAudioForm
    };

    onEAudioFormChange(type, value) {
        let eAudioForm = this.state.eAudioForm;
        eAudioForm[type] = value;

        this.setState({
            eAudioForm: eAudioForm
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4ShowSetting = <div></div>;
        let eFormItem4AudioFrom = <div></div>;
        if (eShareLevel == '2') {
            const eAudioShow_height = getFieldProps('eContentAudioShow_height', {initialValue: this.state.eAudioShow.height});
            const eAudioShow_autoPlay = getFieldProps('eContentAudioShow_autoPlay', {initialValue: this.state.eAudioShow.autoPlay, valuePropName: 'checked'});
            eFormItem4ShowSetting = (
                <FormItem label="显示设置" {...formItemLayout}>
                    <span>高度：</span>
                    <Input size="default" style={{width: '60px'}} {...eAudioShow_height}/>
                    <Checkbox size="default" style={{marginLeft: '10px'}} {...eAudioShow_autoPlay}>自动播放</Checkbox>
                </FormItem>
            );

            const eAudioForm_type = getFieldProps('eContentAudioForm_type', {initialValue: this.state.eAudioForm.type});
            getFieldProps('eContentAudioForm_src', {initialValue: this.state.eAudioForm.src});
            getFieldProps('eContentAudioForm_url', {initialValue: this.state.eAudioForm.url});
            const radioStyle = {
                display: 'block',
                height: '35px',
                marginRight: '0'
            };
            const labelStyle = {
                display: 'inline-block',
                width: '60px'
            };
            const mlStyle = {
                display: 'inline-block',
                width: '350px'
            };
            eFormItem4AudioFrom = (
                <FormItem label="音频来源" {...formItemLayout}>
                    <RadioGroup style={{width: '100%'}} {...eAudioForm_type}>
                        <Radio style={radioStyle} value="1">
                            <span style={labelStyle}>选择文件</span>
                            <div style={mlStyle}>
                                <MaterialLibrary value={this.state.eAudioForm.src} onChange={this.onEAudioFormChange.bind(this, 'src')}/>
                            </div>
                        </Radio>
                        <Radio style={radioStyle} value="2">
                            <span style={labelStyle}>选择文件</span>
                            <div style={mlStyle}>
                                <MaterialLibrary value={this.state.eAudioForm.url} onChange={this.onEAudioFormChange.bind(this, 'url')}/>
                            </div>
                        </Radio>
                    </RadioGroup>
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    {eFormItem4ShowSetting}
                    {eFormItem4AudioFrom}
                </FormGroup>
            </Form>
        );
    }
}