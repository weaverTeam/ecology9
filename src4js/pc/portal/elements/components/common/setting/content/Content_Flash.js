import React from 'react';
import {Form, Input, Radio} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';
import MaterialLibrary from './base/MaterialLibrary';

// Flash元素
export default class Content_Flash extends React.Component {
    state = {
        eFlashShow: this.props.data.eContent.eFlashShow,
        eFlashForm: this.props.data.eContent.eFlashForm
    };

    onEFlashFormChange(type, value) {
        let eFlashForm = this.state.eFlashForm;
        eFlashForm[type] = value;

        this.setState({
            eFlashForm: eFlashForm
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4ShowSetting = <div></div>;
        let eFormItem4FlashFrom = <div></div>;
        if (eShareLevel == '2') {
            const eFlashShow_height = getFieldProps('eContentFlashShow_height', {initialValue: this.state.eFlashShow.height});
            eFormItem4ShowSetting = (
                <FormItem label="显示设置" {...formItemLayout}>
                    <span>高度：</span>
                    <Input size="default" style={{width: '60px'}} {...eFlashShow_height}/>
                </FormItem>
            );

            const eFlashForm_type = getFieldProps('eContentFlashForm_type', {initialValue: this.state.eFlashForm.type});
            getFieldProps('eContentFlashForm_src', {initialValue: this.state.eFlashForm.src});
            getFieldProps('eContentFlashForm_url', {initialValue: this.state.eFlashForm.url});
            const radioStyle = {
                display: 'block',
                height: '35px',
                marginRight: '0'
            };
            const labelStyle = {
                display: 'inline-block',
                width: '60px'
            };
            const mlStyle = {
                display: 'inline-block',
                width: '350px'
            };
            eFormItem4FlashFrom = (
                <FormItem label="Flash来源" {...formItemLayout}>
                    <RadioGroup style={{width: '100%'}} {...eFlashForm_type}>
                        <Radio style={radioStyle} value="1">
                            <span style={labelStyle}>选择文件</span>
                            <div style={mlStyle}>
                                <MaterialLibrary value={this.state.eFlashForm.src} onChange={this.onEFlashFormChange.bind(this, 'src')}/>
                            </div>
                        </Radio>
                        <Radio style={radioStyle} value="2">
                            <span style={labelStyle}>选择文件</span>
                            <div style={mlStyle}>
                                <MaterialLibrary value={this.state.eFlashForm.url} onChange={this.onEFlashFormChange.bind(this, 'url')}/>
                            </div>
                        </Radio>
                    </RadioGroup>
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    {eFormItem4ShowSetting}
                    {eFormItem4FlashFrom}
                </FormGroup>
            </Form>
        );
    }
}