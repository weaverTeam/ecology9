import React from "react";
import {Input, Button} from "antd";

// 素材库
export default class MaterialLibrary extends React.Component {
    state = {
        disabled: this.props.disabled || false,
        value: this.props.value
    };

    onShowDialog() {
        let dialog = new Dialog();
        dialog.Title = '图片素材库';
        dialog.URL = '/page/maint/common/CustomResourceMaint.jsp?isDialog=1&?file=none&isSingle=';
        dialog.Width = top.document.body.clientWidth - 100;
        dialog.Height = top.document.body.clientHeight - 100;
        dialog.ShowCloseButton = true;
        dialog.opacity = 0.8;
        dialog.callbackfun = (obj, datas) => {
            this.onChange(datas.id);
        };
        dialog.show();
    }

    onInput(e) {
        this.onChange(e.target.value);
    }

    onChange(value) {
        this.setState({
            value: value
        });

        this.props.onChange(value);
    }

    render() {
        return (
            <div style={{position: 'relative'}}>
                <Input type="text" size="default" style={{paddingRight: '28px'}} disabled={this.state.disabled} value={this.state.value} onChange={this.onInput.bind(this)}/>
                <Button icon="search" size="default" style={{position: 'absolute', right: 0, zIndex: 2, background: 'none', border: 'none'}} disabled={this.state.disabled} onClick={this.onShowDialog.bind(this)}/>
            </div>
        );
    }
}