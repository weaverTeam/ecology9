import React from 'react';
import {Modal, Form, Input, Select, Button, Table} from 'antd';
const FormItem = Form.Item;

import FormGroup from './base/FormGroup';

// 图表元素，数据来源
class Content_reportForm_DataSource extends React.Component {
    state = {
        id: this.props.tabData.id,
        title: this.props.tabData.title,
        reportFormType: this.props.tabData.reportFormType || this.props.tabNew.reportFormType.selected,
        decimalDigits: this.props.tabData.decimalDigits || this.props.tabNew.decimalDigits,
        reportFormWidth: this.props.tabData.reportFormWidth || this.props.tabNew.reportFormWidth,
        reportFormHeight: this.props.tabData.reportFormHeight || this.props.tabNew.reportFormHeight,
        dataSource: this.props.tabData.dataSource || this.props.tabNew.dataSource.selected,
        reportFormSql: this.props.tabData.reportFormSql,
        paramModalVisible: false
    };

    onOk(e) {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                return;
            }

            this.props.onOk(values);
        });
    }

    onCancel() {
        this.props.onCancel();
    }

    onChangeParamModalVisible(visible) {
        this.setState({
            paramModalVisible: visible
        });
    }

    render() {
        const {getFieldProps} = this.props.form;
        const formItemLayout = this.props.formItemLayout;
        const tabNew = this.props.tabNew;

        getFieldProps('id', {initialValue: this.state.id});

        const titleProps = getFieldProps('title', {
            initialValue: this.state.title,
            rules: [
                {required: true, message: '请填写标题'},
            ]
        });

        const reportFormTypeProps = getFieldProps('reportFormType', {initialValue: this.state.reportFormType});
        const reportFormTypeOptions = tabNew.reportFormType.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        const decimalDigitsProps = getFieldProps('decimalDigits', {initialValue: this.state.decimalDigits});

        const reportFormWidthProps = getFieldProps('reportFormWidth', {initialValue: this.state.reportFormWidth});
        const reportFormHeightProps = getFieldProps('reportFormHeight', {initialValue: this.state.reportFormHeight});

        const dataSourceProps = getFieldProps('dataSource', {initialValue: this.state.dataSource});
        const dataSourceOptions = tabNew.dataSource.options.map((item, index) => {
            return <Option key={index} value={item.key}>{item.value}</Option>;
        });

        const reportFormSqlProps = getFieldProps('reportFormSql', {initialValue: this.state.reportFormSql});

        const columns = [{
            title: '参数名',
            dataIndex: 'paramName'
        }, {
            title: '参数显示名',
            dataIndex: 'paramShowName'
        }, {
            title: '参数类型',
            dataIndex: 'paramType'
        }];

        const dataSource = [
            {
                "key": "1",
                "paramName": "$P_sys.userself",
                "paramShowName": "当前登录用户",
                "paramType": "系统变量",
            }, {
                "key": "2",
                "paramName": "$P_sys.usersuperior",
                "paramShowName": "直属上级",
                "paramType": "系统变量",
            }, {
                "key": "3",
                "paramName": "$P_sys.usersubordinate",
                "paramShowName": "直接下级",
                "paramType": "系统变量",
            }, {
                "key": "4",
                "paramName": "$P_sys.currentyear",
                "paramShowName": "本年",
                "paramType": "系统变量",
            }, {
                "key": "5",
                "paramName": "$P_sys.currentmonth",
                "paramShowName": "本月",
                "paramType": "系统变量",
            }, {
                "key": "6",
                "paramName": "$P_sys.currentday",
                "paramShowName": "今天",
                "paramType": "系统变量",
            }, {
                "key": "7",
                "paramName": "$P_sys.currentdepart",
                "paramShowName": "本部门",
                "paramType": "系统变量",
            }, {
                "key": "8",
                "paramName": "$P_sys.currentsubcompany",
                "paramShowName": "本分部",
                "paramType": "系统变量",
            }
        ];

        return (
            <Modal
                title="内容设置"
                width="660"
                wrapClassName="esetting-modal"
                visible={true}
                footer={
                    <div className="esetting-confirm">
                        <Button type="primary" onClick={this.onOk.bind(this)}>确定</Button>
                        <span>&nbsp;&nbsp;&nbsp;</span>
                        <Button type="ghost" onClick={this.onCancel.bind(this)}>取消</Button>
                    </div>
                }
                onOk={this.onOk.bind(this)}
                onCancel={this.onCancel.bind(this)}
            >
                <Form horizontal className="esetting-form">
                    <FormGroup title="设置项">
                        <FormItem label="标题" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '80%'}} {...titleProps}/>
                        </FormItem>
                        <FormItem label="图表类型" {...formItemLayout}>
                            <Select size="default" style={{width: '200px'}} {...reportFormTypeProps}>
                                {reportFormTypeOptions}
                            </Select>
                        </FormItem>
                        <FormItem label="小数位数" {...formItemLayout}>
                            <Input type="text" size="default" style={{width: '200px'}} {...decimalDigitsProps}/>
                        </FormItem>
                        <FormItem label="图表大小" {...formItemLayout}>
                            <span>宽度：</span>
                            <Input type="text" size="default" style={{width: '60px', marginRight: '8px'}} {...reportFormWidthProps}/>
                            <span>高度：</span>
                            <Input type="text" size="default" style={{width: '60px', marginRight: '8px'}} {...reportFormHeightProps}/>
                        </FormItem>
                        <FormItem label="数据来源" {...formItemLayout}>
                            <Select size="default" style={{width: '200px'}} {...dataSourceProps}>
                                {dataSourceOptions}
                            </Select>
                        </FormItem>
                        <FormItem label="图表来源" {...formItemLayout}>
                            <Input type="textarea" size="default" rows={6} style={{width: '90%'}} {...reportFormSqlProps}/>
                            <Button type="primary" size="small" onClick={() => this.onChangeParamModalVisible(true)}>参数信息</Button>
                            <Modal
                                title="图表参数信息"
                                width="600px"
                                wrapClassName="esetting-modal"
                                visible={this.state.paramModalVisible}
                                footer={
                                    <div className="esetting-confirm">
                                        <Button type="ghost" onClick={() => this.onChangeParamModalVisible(false)}>关闭</Button>
                                    </div>
                                }
                                onCancel={() => this.onChangeParamModalVisible(false)}
                            >
                                <Table className="esetting-table esetting-table-rf-param" columns={columns} dataSource={dataSource} pagination={false}/>
                            </Modal>
                        </FormItem>
                    </FormGroup>
                </Form>
            </Modal>
        );
    }
}

Content_reportForm_DataSource = Form.create()(Content_reportForm_DataSource);

export default Content_reportForm_DataSource;