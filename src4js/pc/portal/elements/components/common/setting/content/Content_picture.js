import React from 'react';
import {Form, Checkbox, Radio, Input, Button} from 'antd';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;

import FormGroup from './base/FormGroup';
import FormItem4Title from './base/FormItem4Title';

import Content_picture_detail from './Content_picture_detail';

// 图片元素
export default class Content_picture extends React.Component {
    state = {
        ePopupSetting: this.props.data.eContent.ePopupSetting,
        eDisplayMode: this.props.data.eContent.eDisplayMode,
        eImageWidth: this.props.data.eContent.eImageWidth,
        eImageHeight: this.props.data.eContent.eImageHeight,
        eScrollAuto: this.props.data.eContent.eScrollAuto,
        eScrollSpeed: this.props.data.eContent.eScrollSpeed,
        eScrollPoint: this.props.data.eContent.eScrollPoint,
        eInfoWordNum: this.props.data.eContent.eInfoWordNum,
        eImageSource: this.props.data.eContent.eImageSource,
        detailModalVisible: false
    };

    onChangeDisplayMode(e) {
        this.setState({
            eDisplayMode: e.target.value
        });
    }

    onOk(value) {
        this.setState({
            eImageSource: value,
            detailModalVisible: false
        });

        this.props.form.setFieldsValue('eContentImageSource', {initialValue: value});
    }

    onCancel() {
        this.setState({
            detailModalVisible: false
        });
    }

    onDetailSetting() {
        this.setState({
            detailModalVisible: true
        });
    }

    render() {
        const {eShareLevel} = this.props.data;
        const {getFieldProps} = this.props.form;
        const {formItemLayout} = this.props;

        let eFormItem4PopupSetting = <div></div>;
        let eFormItem4DisplayMode = <div></div>;
        let eFormItem4ImageSize = <div></div>;
        let eFormItem4PictureFrom = <div></div>;
        if (eShareLevel == '2') {
            const ePopupSettingProps = getFieldProps('eContentPopupSetting', {initialValue: this.state.ePopupSetting, valuePropName: 'checked'});
            eFormItem4PopupSetting = (
                <FormItem label="弹出设置" {...formItemLayout}>
                    <Checkbox size="default" {...ePopupSettingProps}>弹出图片</Checkbox>
                </FormItem>
            );

            const eDisplayModeProps = getFieldProps('eContentDisplayMode', {initialValue: this.state.eDisplayMode});
            eFormItem4DisplayMode = (
                <FormItem label="显示方式" {...formItemLayout}>
                    <RadioGroup {...eDisplayModeProps} onChange={this.onChangeDisplayMode.bind(this)}>
                        <Radio value="1">单张显示</Radio>
                        <Radio value="2">多张显示</Radio>
                    </RadioGroup>
                </FormItem>
            );

            const eImageWidthProps = getFieldProps('eContentImageWidth', {initialValue: this.state.eImageWidth});
            const eImageHeightProps = getFieldProps('eContentImageHeight', {initialValue: this.state.eImageHeight});
            eFormItem4ImageSize = (
                <FormItem label="图片大小" {...formItemLayout}>
                    <span>宽度：</span>
                    <Input type="text" size="default" style={{width: '40px', marginRight: '8px'}} {...eImageWidthProps}/>
                    <span>高度：</span>
                    <Input type="text" size="default" style={{width: '40px', marginRight: '8px'}} {...eImageHeightProps}/>
                </FormItem>
            );

            getFieldProps('eContentImageSource', {initialValue: this.state.eImageSource});
            let modal2Detail = <div></div>;
            if (this.state.detailModalVisible) {
                const detailProps = {
                    eImageSource: this.state.eImageSource,
                    onOk: this.onOk.bind(this),
                    onCancel: this.onCancel.bind(this)
                };
                modal2Detail = <Content_picture_detail {...detailProps}/>
            }

            const eScrollAutoProps = getFieldProps('eContentScrollAuto', {initialValue: this.state.eScrollAuto, valuePropName: 'checked'});
            const eScrollPointProps = getFieldProps('eContentScrollPoint', {initialValue: this.state.eScrollPoint, valuePropName: 'checked'});
            const eScrollSpeedProps = getFieldProps('eContentScrollSpeed', {initialValue: this.state.eScrollSpeed});
            const eInfoWordNumProps = getFieldProps('eContentInfoWordNum', {initialValue: this.state.eInfoWordNum});
            eFormItem4PictureFrom = (
                <FormItem label="图片来源" {...formItemLayout}>
                    {this.state.eDisplayMode == '2' ?
                        <div>
                            <Checkbox size="default" {...eScrollAutoProps}>自动滚动</Checkbox>
                            <span>滚动速度：</span>
                            <Input type="text" size="default" style={{width: '40px'}} {...eScrollSpeedProps}/>
                            <div style={{height: '5px'}}></div>
                            <Checkbox size="default" {...eScrollPointProps}>滚动指向</Checkbox>
                            <span>信息字数：</span>
                            <Input type="text" size="default" style={{width: '40px'}} {...eInfoWordNumProps}/>
                        </div> : ''
                    }
                    <Button type="primary" size="small" onClick={() => this.onDetailSetting(true)}>图片设置</Button>
                    {modal2Detail}
                </FormItem>
            );
        }

        return (
            <Form horizontal className="esetting-form">
                <FormGroup title="基本信息">
                    <FormItem4Title {...this.props}/>
                    {eFormItem4PopupSetting}
                    {eFormItem4DisplayMode}
                    {eFormItem4ImageSize}
                    {eFormItem4PictureFrom}
                </FormGroup>
            </Form>
        );
    }
}