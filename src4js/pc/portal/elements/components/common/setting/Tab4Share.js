import React from 'react';
import {Form, Select, Input, Checkbox, Table, Icon, message} from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

import FormGroup from './content/base/FormGroup';

export default class Tab4Share extends React.Component {
    state = {
        key: 0,
        shareType: '7',
        shareValue: '',
        includeSub: false,
        roleLevel: '2',
        securityMinLevel: '0',
        securityMaxLevel: '100',
        jobShareLevel: '1',
        jobShareValue: '',
        shareTypeName: '',
        shareValueName: '',
        securityLevel: '',
        eHasShareRight: this.props.data.eShare.eHasShareRight,
        eOldShareList: this.props.data.eShare.eOldShareList || [],   // 老版共享数据
        eShareList: this.props.data.eShare.eShareList || []   // 新版共享数据
    };

    onAdd() {
        let eShareList = this.state.eShareList;
        let key = --this.state.key;
        let shareType = this.state.shareType;
        const securityMinLevel = this.state.securityMinLevel;
        const securityMaxLevel = this.state.securityMaxLevel;

        let shareTypeName = this.state.shareTypeName;
        let shareValueName = '';
        let securityLevel = '';
        // 部门(3)、分部(2)、所有人(7)、角色(6)
        if (shareType == '3' || shareType == '2' || shareType == '7' || shareType == '6') {
            securityLevel = securityMinLevel + ' - ' + securityMaxLevel;
        }

        if (shareType == '7') {
            shareTypeName = '所有人';
            shareValueName = '所有人';
        }

        eShareList.push({
            key: key,
            shareType: this.state.shareType,
            shareValue: this.state.shareValue,
            includeSub: this.state.includeSub,
            roleLevel: this.state.roleLevel,
            securityMinLevel: securityMinLevel,
            securityMaxLevel: securityMaxLevel,
            jobShareLevel: this.state.jobShareLevel,
            jobShareValue: this.state.jobShareValue,
            shareTypeName: shareTypeName,
            shareValueName: shareValueName,
            securityLevel: securityLevel
        });

        this.setState({
            key: key,
            eShareList: eShareList
        });
    }

    onDelete() {
        let eOldShareList = this.state.eOldShareList;
        let eShareList = this.state.eShareList;

        let isOld = false;
        if (this.selectedRowKeys && this.selectedRowKeys.length) {
            const selectedRowKeys = this.selectedRowKeys.slice();
            for (let i = 0, len = selectedRowKeys.length; i < len; i++) {
                this.selectedRowKeys.splice(i, 1);

                for (let j = 0, len2 = eOldShareList.length; j < len2; j++) {
                    if (eOldShareList[j].key == selectedRowKeys[i]) {
                        eOldShareList.splice(j, 1);
                        isOld = true;
                        break;
                    }
                }

                if (!isOld) {
                    for (let k = 0, len3 = eShareList.length; k < len3; k++) {
                        if (eShareList[k].key == selectedRowKeys[i]) {
                            eShareList.splice(k, 1);
                            break;
                        }
                    }
                }
            }
        } else {
            message.warn('请选择需要删除的行！', 3);
        }

        this.setState({
            eOldShareList: eOldShareList,
            eShareList: eShareList
        });
    }

    onShareTypeChange(value, option) {
        this.setState({
            shareType: value,
            shareValue: '',
            shareTypeName: option.props.children
        });
    }

    onShareValueChange(e) {
        this.setState({
            shareValue: e.target.value
        });
    }

    onIncludeSubChange(e) {
        this.setState({
            includeSub: e.target.checked
        });
    }

    onRoleLevelChange(value) {
        this.setState({
            roleLevel: value
        });
    }

    onSecurityMinLevelChange(e) {
        this.setState({
            securityMinLevel: e.target.value
        });
    }

    onSecurityMaxLevelChange(e) {
        this.setState({
            securityMaxLevel: e.target.value
        });
    }

    onJobShareLevelChange(value) {
        this.setState({
            jobShareLevel: value
        });
    }

    onJobShareValueChange(e) {
        this.setState({
            jobShareValue: e.target.value
        });
    }

    render() {
        const eHasShareRight = this.state.eHasShareRight;
        const {getFieldProps} = this.props.form;

        const formItemLayout = {
            labelCol: {span: 4},
            wrapperCol: {span: 20},
        };

        if (eHasShareRight) {
            getFieldProps('eOldShareList', {initialValue: this.state.eOldShareList});
            getFieldProps('eShareList', {initialValue: this.state.eShareList});

            let shareValueItem = <span></span>;
            let includeSubItem = <span></span>;
            let roleLevelItem = <span></span>;
            let shareTarget4FormItem = <span></span>;
            let securityLevel4FormItem = <span></span>;
            let jobShareLevel4FormItem = <span></span>;

            const shareType = this.state.shareType;
            // 部门(3)、分部(2)、人力资源(1)、角色(6)、岗位(8)
            if (shareType == '3' || shareType == '2' || shareType == '1' || shareType == '6' || shareType == '8') {
                shareValueItem = (
                    <Input type="text" size="default" style={{width: '300px'}} value={this.state.shareValue} onChange={this.onShareValueChange.bind(this)}/>
                );

                // 部门(3)、分部(2)
                if (shareType == '3' || shareType == '2') {
                    includeSubItem = (
                        <Checkbox size="default" style={{marginLeft: '10px'}} checked={this.state.includeSub} onChange={this.onIncludeSubChange.bind(this)}>含下级</Checkbox>
                    );
                }

                // 角色(6)
                if (shareType == '6') {
                    roleLevelItem = (
                        <span style={{marginLeft: '10px'}}>
                            <span>级别：</span>
                            <Select size="default" style={{width: '100px'}} value={this.state.roleLevel} onChange={this.onRoleLevelChange.bind(this)}>
                                <Option value="2">总部</Option>
                                <Option value="1">分部</Option>
                                <Option value="0">部门</Option>
                            </Select>
                        </span>
                    );
                }

                shareTarget4FormItem = (
                    <FormItem label="对象" {...formItemLayout}>
                        {shareValueItem}
                        {includeSubItem}
                        {roleLevelItem}
                    </FormItem>
                );
            }

            // 部门(3)、分部(2)、所有人(7)、角色(6)
            if (shareType == '3' || shareType == '2' || shareType == '7' || shareType == '6') {
                securityLevel4FormItem = (
                    <FormItem label="安全级别" {...formItemLayout}>
                        <Input type="text" size="default" style={{width: '50px'}} value={this.state.securityMinLevel} onChange={this.onSecurityMinLevelChange.bind(this)}/>
                        <span> - </span>
                        <Input type="text" size="default" style={{width: '50px'}} value={this.state.securityMaxLevel} onChange={this.onSecurityMaxLevelChange.bind(this)}/>
                    </FormItem>
                );
            }

            // 岗位(8)
            if (shareType == '8') {
                let jobShareValueItem = <span></span>;

                const jobShareLevel = this.state.jobShareLevel;
                // 指定部门(2)、指定分部(3)
                if (jobShareLevel == '2' || jobShareLevel == '3') {
                    jobShareValueItem = (
                        <Input type="text" size="default" style={{width: '300px', marginLeft: '10px'}} value={this.state.jobShareValue} onChange={this.onJobShareValueChange.bind(this)}/>
                    );
                }

                jobShareLevel4FormItem = (
                    <FormItem label="岗位级别" {...formItemLayout}>
                        <Select size="default" style={{width: '100px'}} value={this.state.jobShareLevel} onChange={this.onJobShareLevelChange.bind(this)}>
                            <Option value="1">总部</Option>
                            <Option value="2">指定部门</Option>
                            <Option value="3">指定分部</Option>
                        </Select>
                        {jobShareValueItem}
                    </FormItem>
                );
            }

            // 表格数据源
            const dataSource = this.state.eOldShareList.concat(this.state.eShareList);

            // 记录选中的tab数据行
            const _self = this;
            const rowSelection = {
                onChange(selectedRowKeys) {
                    _self.selectedRowKeys = selectedRowKeys;
                }
            };

            // 表格列
            const columns = [{
                title: '对象类型',
                dataIndex: 'shareTypeName'
            }, {
                title: '对象',
                dataIndex: 'shareValueName'
            }, {
                title: '安全级别',
                dataIndex: 'securityLevel'
            }];

            const formGroupButtons = (
                <div className="esetting-dso">
                    <div className="esetting-dso-add" title="添加" onClick={this.onAdd.bind(this)}></div>
                    <div className="esetting-dso-deletes" title="删除" onClick={this.onDelete.bind(this)}></div>
                </div>
            );

            return (
                <Form horizontal className="esetting-form">
                    <FormGroup title="基本信息">
                        <FormItem label="对象类型" {...formItemLayout}>
                            <Select size="default" style={{width: '200px'}} value={this.state.shareType} onSelect={this.onShareTypeChange.bind(this)}>
                                <Option value="3">部门</Option>
                                <Option value="2">分部</Option>
                                <Option value="7">所有人</Option>
                                <Option value="1">人力资源</Option>
                                <Option value="6">角色</Option>
                                <Option value="8">岗位</Option>
                            </Select>
                        </FormItem>
                        {shareTarget4FormItem}
                        {securityLevel4FormItem}
                        {jobShareLevel4FormItem}
                    </FormGroup>
                    <FormGroup title="共享信息" buttons={formGroupButtons}>
                        <Table className="esetting-table" rowSelection={rowSelection} columns={columns} dataSource={dataSource} pagination={false}/>
                    </FormGroup>
                </Form>
            );
        } else {
            const noShareRightStyle = {
                textAlign: 'center',
                width: '100%',
                height: '50px',
                lineHeight: '50px',
                borderBottom: '1px solid #e9e9e9',
            };

            return (
                <div style={noShareRightStyle}><Icon type="exclamation-circle" style={{marginRight: '8px', color: '#fa0'}}/><span>对不起，您暂时没有权限！</span></div>
            );
        }
    }
}