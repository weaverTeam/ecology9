import React from 'react';
import { Table } from 'antd';
import { WeaErrorPage, WeaTools } from 'ecCom';
import { formatData } from '../../util/common';
//建模查询中心元素
class FmcsTable extends React.Component {
	render() {
		let { eid, data, esetting } = this.props;
		if (data.isRight == false) {
			return <div style={{height:'40px',lineHeight:'40px',textAlign:'center'}}>{data.data}</div>
		}
		const { tabsetting } = data;
		const list = data.data;

		const { widths, titles } = tabsetting;
		esetting['widths'] = widths;
		esetting['titles'] = titles;
		let showHeader = true;
		var index = 0;
		for (var k in list[0]) {
			index += 1;
		}
		if (index === 1) {
			showHeader = false;
		}
		let html = list.length ? <Table columns={formatData(list[0], esetting)} showHeader={showHeader} pagination={false} dataSource={list} size="small"/> : '';
		return <div>{html}</div>;
	}
}

class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return (
			<WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
		);
	}
}
FmcsTable = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(FmcsTable);
export default FmcsTable;