import React from 'react';
import Immutable from 'immutable';
const { is, fromJS } = Immutable; 
import { WeaErrorPage, WeaTools } from 'ecCom';
import OpToolbar from '../common/OpToolbar';
import { doPrev, doNext, handleWidth } from '../../util/common';
//tab的标题组件
class Title extends React.Component {
     constructor(props) {
        super(props);
        this.setMoreHref = this.setMoreHref.bind(this);
    }
    componentDidMount() {
        const { config, tabids } = this.props;
        const { eid, header } = config.item;
        this.setMoreHref();
        handleWidth(eid,config.params.hpid,tabids.length,header.canHeadbar);
    }
    componentDidUpdate() {
        const { config, tabids } = this.props;
        const { eid, header } = config.item;
        handleWidth(eid,config.params.hpid,tabids.length,header.canHeadbar);
    }
    setMoreHref() {
        const { config, more } = this.props;
        const { eid } = config.item;
        if (more && JSON.parse(more).viewType !=="6") {
            $("#more_" + eid).attr("data-morehref", '/spa/workflow/index.html#/main/workflow/queryFlowResult?fromwhere=jsonFilter&jsonstr=' + escape(more));
        } else {
            $("#more_" + eid).attr("data-morehref", more.morehref);
        }
    }
    shouldComponentUpdate(nextProps){
        return !is(fromJS(this.props),fromJS(nextProps))
    }
    render() {
        const tdStyle={width:'77px', wordWrap: 'break-word', paddingTop: '5px', verticalAlign: 'top', height: '32px' };
        const { currentTab, config, tabids, titles, counts, handleRefresh, handleDelete, handleTabData } = this.props;
        const { eid, ebaseid } = config.params;
        let tHtml=titles.map((title, i)=> {
            if (counts && counts[i]) {
                 title +=' (' + counts[i] + ')';  
            }
            const className = tabids[i] == currentTab ? "tab2selected" : "tab2unselected";
            return <td title={title} data-tabid={tabids[i]} style={tdStyle} className={className} onClick={handleTabData.bind(this,tabids[i])}>{title}</td>
        });
        let className = tabids.length <= 1 ? 'nodisplay' : '';
        return <div id={`titleContainer_${eid}`} className={`titlecontainer ${className}`}>
                {<div id={`tabnavprev_${eid}`} className="picturebackhp" onClick={doPrev.bind(this,eid)}></div>}
                <div id={`tabContainer_${eid}`} className="tabcontainer tab2">
                    <table style={ { tableLayout:"fixed",borderCollapse:'collapse'}} height="32">
                      <tbody height="32">
                        <tr height="32">
                            {tHtml} 
                        </tr> 
                      </tbody>
                    </table>
                </div> 
                {<div id={`tabnavnext_${eid}`} className="picturenexthp" onClick={doNext.bind(this,eid)}></div>} 
                {!config.item.header.canHeadbar ? <OpToolbar config={config} clsname={className} handleRefresh={handleRefresh} handleDelete={handleDelete}/> : ''} 
              </div>
      }
}
class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg=this.props.error && this.props.error !=="";
            return ( <WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
        );
    }
}
Title = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(Title);
export default Title;
