import React from 'react';
import { Spin, Table } from 'antd';
import Immutable from 'immutable';
const { is, fromJS } = Immutable;
import { WeaErrorPage, WeaTools } from 'ecCom';
const { ls } = WeaTools;
import Title from './Title';
import MarqueeCom from '../common/MarqueeCom';
import { formatData } from '../../util/common';
import SignView from './SignView';
//内容组件
class EContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tabdata: props.edata.data,
            currentTab: props.edata.currenttab,
            refresh: false,
        }
        this.handleTabData = this.handleTabData.bind(this);
    }
    componentDidMount(){
        try{
            _ListenerCtrl.bind();
        } catch(e){
            window.console ? console.log('流程元素Ctrl绑定出错了： ',e) : alert('流程元素Ctrl绑定出错了： '+e);
        }
        window.elmentReLoad = this.handleTabData.bind(this, this.state.currentTab);
    }
    componentWillUnmount(){
        try{
            _ListenerCtrl.unbind();
        } catch(e){
            window.console ? console.log('流程元素Ctrl解除绑定出错了： ',e) : alert('流程元素Ctrl解除绑定出错了： '+e);
        }
    }
    componentWillReceiveProps(nextProps, nextState){
        if(!is(fromJS(this.props),fromJS(nextProps))){
            this.setState({
                tabdata: nextProps.edata.data,
                currentTab: nextProps.edata.currenttab,
                refresh: false,
            }); 
        }
    }
    shouldComponentUpdate(nextProps, nextState){
        return !is(fromJS(this.state), fromJS(nextState)) || !is(fromJS(this.props),fromJS(nextProps))
    }
    handleTabData(tabid){
        window.elmentReLoad = this.handleTabData.bind(this, tabid);
        const { params, isremembertab } = this.props.config;
        const { currentTab } = this.state;
        this.setState({
            refresh: tabid === currentTab,
            currentTab: tabid,
            tabdata: ls.getJSONObj('tabdata-' + params.eid + '-' + tabid),
        });
        WeaTools.callApi('/api/portal/element/workflowtab', 'POST', {...params, tabid}).then(result => {
            ls.set('tabdata-' + params.eid + '-' + tabid, result);
            if(tabid === this.state.currentTab){
                this.setState({
                    tabdata: result,
                    refresh: false,
                });
                if(isremembertab){
                    let edata = ls.getJSONObj('edata-' + params.eid);
                    if(edata){
                        edata.currenttab = tabid;
                        edata.data = result;
                        ls.set('edata-' + params.eid, edata);
                    }
                }
            }
       });
    }
    render() {
        const { config, edata, handleRefresh, handleDelete } = this.props;
        const { tabdata, currentTab, refresh } = this.state;
        if(!tabdata) return <div/>;
        const { tabids, titles, counts, esetting } = edata;
        const { eid } = config.params;
        let contentHtml = <div/>;
        let more = {};
        let currCount = "";
        if (tabdata) {
            currCount = tabdata.tabsetting.count;
            for (var i = 0; i <tabids.length; i++) {
                if (tabids[i] === currentTab) {
                    counts[i] = currCount;
                }
            }
            more = tabdata.tabsetting.more;
            const list = tabdata.data;
            if(list.length > 0) contentHtml = <MarqueeCom eid={eid} scolltype={esetting.scolltype}><Table columns={formatData(list[0], esetting)} showHeader={false} pagination={false} dataSource={list} size="small" /></MarqueeCom>
        }
        if(refresh) contentHtml = <Spin>{contentHtml}</Spin>
        const _titleProps = {
            currentTab,
            config,
            titles,
            tabids,
            counts,
            more,
            currCount,
            handleRefresh,
            handleDelete,
            handleTabData: this.handleTabData
        }
        return <div>
            <Title {..._titleProps}/>
            <div className="tabContant" id={`tabcontant_${eid}`}>
                {contentHtml}
                <SignView eid={eid} tabid={currentTab} refresh={refresh}/>
            </div>
         </div>;
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
EContent = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(EContent);
export default EContent;
