import React from 'react';
import { WeaScroll, WeaErrorPage, WeaTools } from 'ecCom';
import { Spin } from 'antd';
import Immutable from 'immutable';
const { is, fromJS } = Immutable;
import { openLinkUrl } from '../../util/common';
class SignView extends React.Component{
	constructor(props) {
        super(props);
        this.state = {
            data: null,
            isMore: false,
        }
        this.signviewPoistion = this.signviewPoistion.bind(this);
        this.getMoreSignData = this.getMoreSignData.bind(this);
    }
    signviewPoistion(eid){
    	let that = this;
		var _contentDiv = jQuery("#signPreview_" + eid).find(".signContent");
		jQuery("#tabcontant_" + eid).find(".reqdetail").parent().hover(function() {
		    $(this).find(".imgdetail").show();
		},
		function() {
		    $(this).find(".imgdetail").hide();
		});
		jQuery("#tabcontant_" + eid).find(".imgdetail").bind("click",function() {
		    var signPreview = jQuery("#signPreview_" + eid);
		    signPreview.find(".signMore").hide();
		    signPreview.height(372);
		    var $this = jQuery(this).parents("tr:first").find(".reqdetail");
		    var adom = jQuery(this).parents("tr:first").find("a");
		    _contentDiv.css("height", "332px");
		    $(".signPreview").hide();
		    var requestid = $(adom).attr("data-requestid");
		    var link = $(adom).attr("data-link");
	        var title = $(adom).attr("title");
		    signPreview.find(".signTitle").html("<a style='color:#000' title='"+title+"' href='javascript:openLinkUrl(\""+link+"\",\"2\")'>"+title+"</a>");
		    let pagesize = 3;
		    let curPage = 1;
		    let totalCount = 0;
		   	WeaTools.callApi('/api/workflow/reqform/requestLog', 'POST', {
		   		requestid,
		   		maxrequestlogid:'',
		   		loadmethod: 'portal',
		   		firstload: true,
		   		requestLogParams: null,
		   		wfsignlddtcnt: pagesize,
		   		pgnumber: curPage,
		   	}).then((data) => {
		   		that.setState({
		            data: data.log_loglist,
		            isMore: false,
		        });
	            totalCount = parseInt(data.totalCount);
		        var etop = $this.parents(".item:first").offset().top;
		        var eleft = $this.parents(".item:first").offset().left;
		        //alert(eleft)
		        if ($this.parents(".item:first").css("position") == "static") {
		            etop = 0;
		            eleft = 0;
		        }
		        var left = $this.offset().left;
		        left = left - eleft;
		        var top = $this.offset().top;
		        var rtop = top - etop;
		        if (top < signPreview.height() || top - $(document).scrollTop() + 350 < $(window).height()) {
		            $(".arrowsblockup").hide();
		            $(".arrowsblock").show();
		            $(".signContainer").removeClass("signContainerup");
		            $(".signMore").removeClass("signMoreup");
		        } else {
		            $(".arrowsblockup").show();
		            $(".arrowsblock").hide();
		            $(".signContainer").addClass("signContainerup");
		            $(".signMore").addClass("signMoreup");
		            rtop = rtop - 332 - 20;
		            if ((curPage*3) < totalCount) {
		                rtop = rtop - 60;
		            } else {
		                rtop = rtop - 20;
		            }
		            rtop = rtop + 8 - 40;
		        }
		        //alert(top+rtop-$(document).scrollTop())
		        if (top + rtop - $(document).scrollTop() < 0) {
		            rtop = top - etop;
		            $(".arrowsblockup").hide();
		            $(".arrowsblock").show();
		            $(".signContainer").removeClass("signContainerup");
		            $(".signMore").removeClass("signMoreup");
		        }
		        rtop = rtop + 31;
		        signPreview.css("top", rtop);
		        signPreview.css("left", left);
		        signPreview.attr("top", rtop);
		        var ulheight = _contentDiv.find(".content").height();
		        if (ulheight > 332) {
		            ulheight = 332;
		            _contentDiv.height(ulheight);
		        }
		        _contentDiv.perfectScrollbar();
		        if ((curPage*3) >= totalCount) {
		            signPreview.find(".signMore").hide();
		            signPreview.find(".signMore").prev().css("border-bottom-width", "1px");
		        } else {
		            signPreview.find(".signMore").show();
		            const signHeight = signPreview.height();
		            signPreview.height(412);
		            var paramsDom = signPreview.find(".params");
		            $(paramsDom).find("input[name='pageno']").val("1");
		            $(paramsDom).find("input[name='requestid']").val(requestid);
		            $(paramsDom).find("input[name='maxrequestlogid']").val(data.maxrequestlogid);
		            $(paramsDom).find("input[name='requestLogParams']").val(JSON.stringify(data.requestLogParams));
		            signPreview.find(".signMore").prev().css("border-bottom-width", "0px");
		        }
		        signPreview.show();
		    });
		})
		jQuery("#signPreview_" + eid).find(".close").bind("click",function() {
		    jQuery("#signPreview_" + eid).hide();
		})
	}
	getMoreSignData(eid){
		this.setState({
		     isMore: true,
		});
		var signPreview = jQuery("#signPreview_" + eid);
		var paramsDom = signPreview.find(".params");
      	var curPage = $(paramsDom).find("input[name='pageno']").val();
      	var requestid = $(paramsDom).find("input[name='requestid']").val();
        var maxrequestlogid = $(paramsDom).find("input[name='maxrequestlogid']").val();
       	var requestLogParams = $(paramsDom).find("input[name='requestLogParams']").val();	
		var nextPage = parseInt(curPage)+1;
		var pagesize = 3;
	   	WeaTools.callApi('/api/workflow/reqform/requestLog', 'POST', {
		   		requestid,
		   		maxrequestlogid,
		   		loadmethod: 'portal',
		   		firstload: false,
		   		requestLogParams,
		   		wfsignlddtcnt: pagesize,
		   		pgnumber: nextPage,
		   	}).then((data) => {
	   		if(!data.api_status){
				var odata = this.state.data;
		   		var newList = odata.concat(data.log_loglist);
		   		this.setState({
		            data: newList,
		            isMore: false,
			    });
	            $(paramsDom).find("input[name='pageno']").val(nextPage);
	            $(paramsDom).find("input[name='requestid']").val(requestid);
	            $(paramsDom).find("input[name='maxrequestlogid']").val(data.maxrequestlogid);
	            $(paramsDom).find("input[name='requestLogParams']").val(requestLogParams);
		   		if(nextPage*3>parseInt(data.totalCount)){
		   			 signPreview.height(370);
		   			 signPreview.find(".signMore").hide();
		   		}
	   		}else{
	   			window.console ? console.error(" errormsg : ",data.api_errormsg) : alert(" errormsg : ",data.api_errormsg);
	   		}
	   	});
	}
	componentDidMount(){
		this.signviewPoistion(this.props.eid);
	}
	componentDidUpdate(preProps){
		const { eid, tabid, refresh } = this.props;
		if(preProps.tabid !== tabid || refresh !== preProps.refresh){
			$("#signPreview_"+eid).css("display","none");
			this.signviewPoistion(eid);
		}
	}
	shouldComponentUpdate(nextProps, nextState){
        return !is(fromJS(nextProps),fromJS(this.props)) || !is(fromJS(nextState),fromJS(this.state))
    }
	render(){
		const { eid, tabid } = this.props;
		const { data, isMore } = this.state;
		const none = { display: 'none' };
		const sdata = data || [];
		let html = null;
		if(sdata.length > 0){
			html = sdata.map((item,i)=>{
      			return <SignContent item={item}/>
  			});
		}else{
			html = <div id="nosigndiv" style={{lineHeight: '20em', fontSize: '16px', color: 'rgb(74, 99, 121)', textAlign: 'center'}}>
				<img src="/images/ecology8/workflow/noSignNotice_wev8.png" style={{position:'relative', top:'4px'}}/>&nbsp;
				<span>没有可以显示的数据</span>
			</div>
		}
		let ahtml = <div className="signContent" style={{position:'relative',height: '332px'}}>
			<div className="content" style={{border: '0px'}}>
				{html}
			</div>
		</div>
		if(!data || isMore){
			ahtml = <Spin>{ahtml}</Spin>
		}
		ahtml = <WeaScroll typeClass="scrollbar-macosx" className="signcontent-scroll" conClass="signcontent-scroll" conHeightNum={0}>{ahtml}</WeaScroll>
		return <div id={`signPreview_${eid}`} className="signPreview" style={{height:'372px',boxShadow:'0 0 2px #000000',background:'#FFFFFF', display: 'none',zIndex:9999, position: 'absolute',width: '550px' }}>
		<div className="arrowsblock" style={none}><img src="/images/ecology8/homepage/arrows_wev8.png" width="13px" height="8px"/></div>
		<div className="arrowsblockup" style={none}><img src="/images/ecology8/homepage/arrows_up_wev8.png" width="13px" height="8px"/></div>
		<div className="signContainer" style={{position: 'relative',height:'392px'}}>
		<div style={{height:'40px',lineHeight:'40px',fontSize: '16px',color: '#242424',paddingLeft:'20px',borderBottom: '1px solid #ced0d2'}}>
			<div className="signTitle" style={{lineHeight: '40px',height: '40px',overflow: 'hidden', marginRight:'38px',textOverflow: 'ellipsis',whiteSpace: 'nowrap'}}></div>
			<img style={{position: 'absolute',top:'10px',right: '8px',cursor: 'pointer'}} className="close" src="/images/ecology8/homepage/close_wev8.png" width="20px" height="20px"/>
		</div>
		{ahtml}
		{/*<iframe height='450px' width="100%" border="0px" style={{border: '0px'}}></iframe>*/}
		<div className="signMore" onClick={this.getMoreSignData.bind(this,eid)} style={{display:'none',fontSize: '15px',background: '#f4f7f7',height: '40px',lineHeight: '40px',textAlign: 'center', cursor: 'pointer',borderTop: '1px solid #ced0d2'}}>加载更多</div>
		<div className="params" style={none}>
			<input type="hidden" value="1" name="pageno"/>
			<input type="hidden" value="" name="requestid"/>
			<input type="hidden" value="" name="maxrequestlogid"/>
			<input type="hidden" value="" name="requestLogParams"/>
		</div>
		</div>
	</div>
	}
}

class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return (
			<WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
		);
	}
}
SignView = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(SignView);
export default SignView;

const SignContent = ({ item }) => {
	const {log_nodename ,operationname, displayname, displaydepid, displaydepname, displayid, img_path, receiveUser, log_operatedate, log_operatetime, log_remarkHtml } = item;
	var nameStr = '接收人 : ' + receiveUser;
	return <div className="wea-workflow-req-sign-list-content" >
          <div className="content-left">
        <img src={img_path} className="content-text-left-user-img"/>
        <div style={{width:'132px'}}>
          <p>
            <a href={`javascript:openhrm(${displayid})`} onClick={pointerXY.bind(this)}>{displayname}</a>
          </p>
          <span>
            <a href="/hrm/company/HrmDepartmentDsp.jsp?id=36" target="_blank" style={{color:'#9b9b9b',whiteSpace:'pre-wrap'}}>{displaydepname}</a>
          </span>
        </div>
      </div>
      <div className="content-right">
        <div className="content-right-remark-html"  dangerouslySetInnerHTML={{__html:log_remarkHtml}}>

        </div>
        <p style={{lineHeight:'24px',color:'#8a8a8a'}}>
          <span style={{overflow: 'hidden',textOverflow: 'ellipsis', whiteSpace:'nowrap',maxWidth:'295px', height: '20px', lineHeight: '20px',display:'block'}}>{nameStr}</span>
        </p>
        <p style={{lineHeight:'22px',marginTop:'10px',color:'#9a9a9a'}}>
          <span style={{marginRight:'8px'}}>{log_operatedate} {log_operatetime} </span>
          <span>[{log_nodename} / {operationname}]</span>
        </p>
      </div>
    </div>
}


