import React from 'react';
import { Spin } from 'antd';
import Immutable from 'immutable';
const { is, fromJS } = Immutable;
import { WeaErrorPage, WeaTools } from 'ecCom';
const { ls } = WeaTools;
import Title from './Title';
import HighChart from './HighChart';
//内容组件
class EContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tabdata: props.edata.data,
            currentTab: props.edata.currenttab,
            refresh: false,
        }
        this.handleTabData = this.handleTabData.bind(this);
    }
    componentWillReceiveProps(nextProps){
        if(!is(fromJS(nextProps),fromJS(this.props))){
            this.setState({
                tabdata: nextProps.edata.data,
                currentTab: nextProps.edata.currenttab,
                refresh: false,
            }); 
        }
    }
    shouldComponentUpdate(nextProps, nextState){
        return !is(fromJS(this.state), fromJS(nextState)) || !is(fromJS(this.props),fromJS(nextProps))
    }
    handleTabData(tabid){
        const { params, isremembertab } = this.props.config;
        const { currentTab } = this.state;
        this.setState({
            refresh: tabid === currentTab,
            currentTab: tabid,
            tabdata: ls.getJSONObj('tabdata-' + params.eid + '-' + tabid),
        });
        WeaTools.callApi('/api/portal/element/newstab', 'POST', {...params, tabid}).then(result => {
            ls.set('tabdata-' + params.eid + '-' + tabid, result);
            if(tabid === this.state.currentTab){
                this.setState({
                    tabdata: result,
                    refresh: false,
                });
                if(isremembertab){
                    let edata = ls.getJSONObj('edata-' + params.eid);
                    if(edata){
                        edata.currenttab = tabid;
                        edata.data = result;
                        ls.set('edata-' + params.eid, edata);
                    }
                }
            }
       });
    }
    render() {
        const { config, edata, handleRefresh, handleDelete } = this.props;
        const { tabdata, currentTab, refresh } = this.state; 
        const { tabids, titles, esetting } = edata;
        const { eid } = config.params;
        let contentHtml = tabdata ? <HighChart eid={eid} data={tabdata}/> : <div/>;
        if(refresh) contentHtml = <Spin>{contentHtml}</Spin>
        const _titleProps = {
            currentTab,
            config,
            titles,
            tabids,
            handleRefresh,
            handleDelete,
            handleTabData: this.handleTabData
        }
        return <div>
            <Title {..._titleProps}/>
            <div className="tabContant" id={`tabcontant_${eid}`}>
                {contentHtml}
            </div>
         </div>;
    }
}

class MyErrorHandler extends React.Component {
    render() {
        const hasErrorMsg = this.props.error && this.props.error !== "";
            return ( <WeaErrorPage msg = { hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" }/>
        );
    }
}
EContent = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(EContent);
export default EContent;