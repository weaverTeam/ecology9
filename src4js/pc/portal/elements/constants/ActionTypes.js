//元素类型统一管理
const ELEMENT_TYPES = {
    /** ---------- 可变tab元素 ---------- **/
    RSS: '1',                                       // RSS元素
    NEWS: '7',                                      // 文档元素
    WORKFLOW: '8',                                  // 流程元素
    CUSTOMPAGE: '29',                               // 自定义页面元素
    REPORTFORM: 'reportForm',                       // 图表元素
    OUTDATA: 'OutData',                             // 外部数据元素
    FORMMODECUSTOMSEARCH: 'FormModeCustomSearch',   // 建模查询中心元素

    /** ---------- 不可变tab元素 ---------- **/
    MAIL: '16',                                     // 我的邮件
    ADDWF: 'addwf',                                 // 新建流程
    TASK: 'Task',                                   // 任务元素
    BLOGSTATUS: 'blogStatus',                       // 微博元素
    CONTACTS: 'contacts',                           // 通讯录

    /** ---------- 以view.jsp页面显示的元素 ---------- **/
    UNREAD_DOCS: '6',                               // 未读文档
    MESSAGE_REMINDING: '9',                         // 消息提醒 
    MY_PROJECTS: '10',                              // 我的项目 
    NEW_CUSTOMERS: '11',                            // 最新客户 
    NEW_MEETING: '12',                              // 最新会议 
    UNREAD_COOPERATION: '13',                       // 未读协作  
    MONTH_TARGET: '14',                             // 当月目标 
    DAY_PLAN: '15',                                 // 当日计划
    SUBSCRIBE_KONWLEDG: '34',                       // 知识订阅

    /** ---------- 元素 ---------- **/
    MORE_NEWS: '17',                                // 多新闻中心
    MAGAZINE: '18',                                 // 期刊元素
    STOCK: '19',                                    // 股票元素
    DOC_CONTENT: '25',                              // 文档内容
    AUDIO: 'audio',                                 // 音频元素 
    FAVOURITE: 'favourite',                         // 收藏元素
    FLASH: 'Flash',                                 // Flash元素
    PICTURE: 'picture',                             // 图片元素
    MYCALENDAR: 'MyCalendar',                       // 日历日程
    IMGSLIDE: 'imgSlide',                           // 多图元素
    NEWNOTICE: 'newNotice',                         // 最新通告  
    OUTTERSYS: 'outterSys',                         // 集成登录
    SCRATCHPAD: 'scratchpad',                       // 便签元素
    WEATHER: 'weather',                             // 天气元素
    VIDEO: 'video',                                 // 视频元素 
    SLIDE: 'Slide',                                 // 幻灯片元素 
    DATACENTER: 'DataCenter',                       // 个人中心
    JOBSINFO: 'jobsinfo',                           // 多岗位办理事项
    SEARCHENGINE: 'searchengine',                   // 搜索元素
    NOTICE: 'notice',                               // 通告栏元素
    PLAN: 'plan',                                   // 计划元素
    CUSTOMMENU: 'menu',                             // 自定义菜单
    WORKTASK: '32'                                  // 任务计划
}

module.exports = {
    ELEMENT_TYPES
}
