import { message, Modal } from 'antd';
import { WeaTools } from 'ecCom';
function handlePortalSynize(params){
    Modal.confirm({
        title: '信息确认',
        content: '此操作可能需要较长时间，是否继续？',
        onOk: () => {
            WeaTools.callApi('/api/portal/rightclick/doPortalSynize', 'POST', params).then((data) => {
                if (data.status === 'failed') {
                    message.error(data.errormsg);
                } else {
                    message.success("同步首页完成");
                }
            });
        },
    });
}

function onShowOrHideAllE(isSetting){    
    $(".homepage[data-issetting="+isSetting+"]").find(".content").toggle();
}

function openFavouriteBrowser(){
    var BacoTitle = jQuery("#BacoTitle");
    var pagename = "";
    var navName = "";
    var fav_uri = "%2Fhomepage%2FHomepage.jsp";
    var fav_querystring = "isfromportal%3D1%5Ee71494489609747%3D%5Ehpid%3D26%5EsubCompanyId%3D34%5Eisfromhp%3D0";
    try{
        var e8tabcontainer = jQuery("div[_e8tabcontainer='true']",parent.document);
        if(e8tabcontainer.length > 0) {
            fav_uri = escape(parent.window.location.pathname);
            fav_querystring = escape(parent.window.location.search);
            navName = e8tabcontainer.find("#objName").text();
        }else{
            navName = jQuery("#objName").text();
        }
    } catch(e) {

    }
    if(BacoTitle) {
        pagename = BacoTitle.text();
    }
    if(!pagename){
        pagename = navName;
    }
    pagename = escape(pagename);
    //window.showModalDialog('/systeminfo/BrowserMain.jsp?url=/favourite/FavouriteBrowser.jsp&fav_pagename='+pagename+'&fav_uri='+fav_uri+'&fav_querystring='+fav_querystring+'&mouldID=doc');
    var e8tabcontainer2 = jQuery("div[_e8tabcontainer='true']",parent.document);
    var dialogurl = "";
    if(e8tabcontainer2.length > 0){
        dialogurl = '/systeminfo/BrowserMain.jsp?url=/favourite/FavouriteBrowser.jsp&fav_pagename='+pagename+'&fav_uri='+fav_uri+'&fav_querystring='+fav_querystring+'&mouldID=doc';
    }else{
        dialogurl = '/systeminfo/BrowserMain.jsp?url=/favourite/FavouriteBrowser.jsp&fav_uri='+fav_uri+'&mouldID=doc';  //fav_pagename和fav_querystring不通过url传值，而通过session获取，避免url过长时，导致问题
    }
    var dialog = new window.top.Dialog();
    dialog.currentWindow = window;
    dialog.URL = dialogurl;
    dialog.Title = "收藏夹";
    dialog.Width = 550 ;
    dialog.Height = 600;
    dialog.Drag = true;
    dialog.show();
}

function showHelp(){
    var pathKey = "%2Fhomepage%2FHomepage.jsp%3Fhpid%3D1%26subCompanyId%3D1%26isfromportal%3D1%26isfromhp%3D0%26e71494487903719%3D";
    var operationPage = "http://help.e-cology.com.cn/help/RemoteHelp.jsp";
    var screenWidth = window.screen.width*1;
    var screenHeight = window.screen.height*1;
    var isEnableExtranetHelp = 1;
    if(isEnableExtranetHelp==1){
        //operationPage = "http://e-cology.com.cn/formmode/apps/ktree/ktreeHelp.jsp";
        operationPage = 'http://www.e-cology.com.cn/formmode/apps/ktree/ktreeHelp.jsp';
    }
    window.open(operationPage+"?pathKey="+pathKey,"_blank","top=0,left="+(screenWidth-800)/2+",height="+(screenHeight-90)+",width=1000,status=no,scrollbars=yes,toolbar=yes,menubar=no,location=no");
}

module.exports = {
    handlePortalSynize,
    onShowOrHideAllE,
    openFavouriteBrowser,
    showHelp,
    dragBaseElement
}


/**-----------左边栏baseelement拖动特效------------**/
function dragBaseElement(ebaseid, callback){
    return;
    let hoveritem;
    let itemholder=$("#itemholder");
    let itemholderClass=$(".itemholderClass");
    let index = 0;
    $(".portal-setting-left-hpbaseelement-item div a" ).draggable({
        helper: "clone",
        start: function( event, ui ) {
            var helper = ui.helper;
            helper.addClass("dragitemholder");
        }, drag:function( event, ui ){
            if(hoveritem!==undefined){
                var items=hoveritem.find(".item");
                var itemtemp;
                var offset=ui.offset;
                var itemoffset;
                var itemheight;
                var flag=true;
                for(var i=0,len=items.length;i<len;i++){
                    itemtemp=$(items[i]);
                    itemoffset=itemtemp.offset();
                    itemheight=itemtemp.height();
                    if(offset.top>itemoffset.top  && offset.top<itemoffset.top+itemheight){
                        if(itemtemp.prev()===itemholder) {return;}
                        itemholder.insertBefore(itemtemp);
                        itemholder.show();
                        index=i;
                        return;
                    }
                    if(i===(len-1))
                        flag=false;
                }
                if(!flag || items.length===0){
                    hoveritem.find(".group").append(itemholder);
                    itemholder.show();
                }
            }
        }
    });
    //将td注册drop事件
    $(".group").parent().droppable({
        over: function( event, ui ) {
            hoveritem=$(event.target);
        },
        drop: function( event, ui ) {
            var areaflag = hoveritem.find(".group").attr("data-areaflag");
            if(ebaseid==null){
                return;
            }
            callback(ebaseid,areaflag,index);
        //鼠标移除则隐藏占位框
        },out: function(event,ui) {
        // itemholderClass.hide();
            $(".dragitemholder").hide();
        }
    });
}