import coworkMine from "./mine"
import coworkCollect from "./collect"
import coworkItemMonitor from "./itemMonitor"

export default {
	coworkMine,
	coworkCollect,
	coworkItemMonitor
}