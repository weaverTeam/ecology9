import * as types from '../constants/ActionTypes'

// 初始状态值
let initialState = {
	loading: false,

	topTab: [],
	selectedKey: 'all',
	conditioninfo: [],
	showSearchAd: false,
	fields: {},
	searchParamsAd: {},

	sessionkey: ""
};

export default function mine(state = initialState, action) {
	switch (action.type) {
		case types.SET_LOADING:
			return { ...state, 
				loading: action.loading
			};
		case types.MINE_TABLE_LOADING:
			return { ...state,
				loading: action.loading,
				sessionkey: action.sessionkey
			};
		case types.MINE_INIT_CONDITION:
			return { ...state,
				topTab: action.topTab,
				conditioninfo: action.conditioninfo
			};
		case types.MINE_SHOW_SEARCHAD:
			return { ...state,
				showSearchAd: action.value
			};
		case types.MINE_SELECTED_KEY:
			return { ...state,
				selectedKey: action.value
			}
		case types.MINE_SAVE_SEARCH_FIELDS:
			return { ...state, 
				fields: action.value,
				searchParamsAd: function(){
					let params = {};
					if(action.value){
						for (let key in action.value) {
							params[action.value[key].name] = action.value[key].value
						}
					}
					return params;
				}()
			};
		default:
			return state;
	}
}