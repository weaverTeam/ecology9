import * as types from '../constants/ActionTypes'

let initialState = {
	title: "集成测试",
	data: [],
	sysid: "",
	fields: {},
	flag: "false",
	selectMap: {}
};

export default function test(state = initialState, action) {
	switch (action.type) {
		case types.TEST:
			return {...state,
				data: action.data.data,
				sysid: action.data.sysid,
				selectMap: action.data.selectMap
			};
		case types.TEST2:
			return {...state,
				flag: action.data.flag

			};
		default:
			return state
	}
}