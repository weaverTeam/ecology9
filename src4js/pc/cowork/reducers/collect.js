import * as types from '../constants/ActionTypes'

// 初始状态值
let initialState = {
	loading: false,

	conditioninfo: [],
	showSearchAd: false,
	fields: {},
	searchParamsAd: {},
	
	sessionkey: ""
};

export default function collect(state = initialState, action) {
	switch (action.type) {
		case types.SET_LOADING:
			return { ...state, 
				loading: action.loading
			};
		case types.COLLECT_TABLE_LOADING:
			return { ...state,
				loading: action.loading,
				sessionkey: action.sessionkey
			};
		case types.COLLECT_INIT_CONDITION:
			return { ...state,
				conditioninfo: action.conditioninfo
			};
		case types.COLLECT_CANCEL_COLLECT:
			return { ...state};
		case types.COLLECT_SHOW_SEARCHAD:
			return { ...state,
				showSearchAd: action.value
			};
		case types.COLLECT_SAVE_SEARCH_FIELDS:
			return { ...state, 
				fields: action.value,
				searchParamsAd: function(){
					let params = {};
					if(action.value){
						for (let key in action.value) {
							params[action.value[key].name] = action.value[key].value
						}
					}
					return params
				}()
			};
		default:
			return state;
	}
}