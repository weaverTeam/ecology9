import * as types from '../constants/ActionTypes'

// 初始状态值
let initialState = {
	loading: false,

	topTab: [],
	selectedKey: 'all',
	conditioninfo: [],
	showSearchAd: false,
	fields: {},
	searchParamsAd: {},

	selectedTreeKeys: [],
	treeDatas: [],
	treeTypes: {},
	treeCounts: [],

	sessionkey: ""
};

export default function itemMonitor(state = initialState, action) {
	switch (action.type) {
		case types.SET_LOADING:
			return { ...state, 
				loading: action.loading
			};
		case types.ITEM_MONITOR_TABLE_LOADING:
			return { ...state,
				loading: action.loading,
				sessionkey: action.sessionkey
			};
		case types.ITEM_MONITOR_INIT_CONDITION:
			return { ...state,
				topTab: action.topTab,
				conditioninfo: action.conditioninfo
			};
		case types.ITEM_MONITOR_SHOW_SEARCHAD:
			return { ...state,
				showSearchAd: action.value
			};
		case types.ITEM_MONITOR_SELECTED_TREE_KEYS:
			return { ...state,
				selectedTreeKeys: action.value
			};
		case types.ITEM_MONITOR_LOADING_LEFT_TREE:
			return { ...state,
				treeDatas: action.treeDatas,
				treeTypes: action.treeTypes,
				treeCounts: action.treeCounts
			};
		case types.ITEM_MONITOR_SELECTED_KEY:
			return { ...state,
				selectedKey: action.value
			};
		case types.ITEM_MONITOR_SAVE_SEARCH_FIELDS:
			return { ...state, 
				fields: action.value,
				searchParamsAd: function(){
					let params = {};
					if(action.value){
						for (let key in action.value) {
							params[action.value[key].name] = action.value[key].value
						}
					}
					return params;
				}()
			};
		default:
			return state;
	}
}