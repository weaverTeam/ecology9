import Route from "react-router/lib/Route"
import {IndexRedirect} from 'react-router';
import reducer from "./reducers/"

import "./css/index.less"
import './css/icon.css';

import Cowork from './components/cowork/';

import Mine from './components/Mine';
import * as MineAction from "./actions/mine"

import Collect from './components/Collect';
import * as CollectAction from "./actions/collect"

import ItemMonitor from './components/ItemMonitor';
import * as ItemMonitorAction from "./actions/itemMonitor"

class Home extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		return (
			<div style={{height: '100%'}}>{this.props.children}</div>
		);
	}
}
const coworkRoute = (
    <Route path="cowork" component={Home}>
        <IndexRedirect to="main"></IndexRedirect>
        <Route name="main" path="main" component={Cowork}/>
		<Route name="mine" path="mine" component={ Mine }/>  //我的帖子
        <Route name="collect" path="collect" component={ Collect }/>  //收藏的帖子
		<Route name="itemMonitor" path="itemMonitor" component={ ItemMonitor }/>  //协作申请
    </Route>
)

module.exports = {
    Route: coworkRoute,
    reducer,
    action: {
		MineAction,
		CollectAction,
		ItemMonitorAction
    }
}