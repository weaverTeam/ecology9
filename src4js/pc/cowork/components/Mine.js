import {WeaTable} from 'comsRedux';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import PropTypes from 'react-router/lib/PropTypes'
import {
    WeaTop,
    WeaTab,
    WeaLeftTree,
    WeaLeftRightLayout,
    WeaSearchGroup,
    WeaRightMenu,
	WeaPopoverHrm,
	WeaErrorPage,
	WeaTools
} from 'ecCom'
import {
	Button, Form, Modal
}
from 'antd'
import Immutable from 'immutable'
import forEach from 'lodash/forEach'
import isEmpty from 'lodash/isEmpty'

import CoworkConstant from '../constants/CoworkConstant';
import * as MineApis from '../apis/mine';
import * as MineAction from '../actions/mine'

const confirm = Modal.confirm;
const is = Immutable.is;
const createForm = Form.create;
const FormItem = Form.Item;
const WeaTableAction = WeaTable.action;

/**
 * 我的帖子
 */
class Mine extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		const { actions } = this.props;

        //初始化高级搜索区域
        actions.initConditionDatas();
        //初始化列表
        actions.doTableLoading();
	}
	componentWillReceiveProps(nextProps) {
    }
    componentWillUnmount() {
		//组件卸载时一般清理一些状态
		const { actions } = this.props;

		actions.setShowSearchAd(false);
		let selectedKey = 'all';
		actions.setSelectedKey(selectedKey);
		actions.saveSearchParamsFields();
    }
	render() {
		const { loading, title, actions, topTab, selectedKey, conditioninfo, showSearchAd, fields, searchParamsAd, sessionkey } = this.props;
		return (
            <div className="wea-cowork-mine">
				<WeaRightMenu datas={ this.getRightMenu() } onClick={ this.onRightMenuClick.bind(this) }>
					<WeaTop
						title="我的帖子"
						loading={ loading }
						icon={ CoworkConstant.icon }
						iconBgcolor={ CoworkConstant.iconBgColor }
						buttonSpace={ 10 }
						showDropIcon={ true }
						dropMenuDatas={ this.getRightMenu() }
						onDropMenuClick={ this.onRightMenuClick.bind(this) }
						buttons={ [] }
					>
						<WeaTab
							datas={ topTab }
							keyParam="groupid"
							onChange={ this.changeTab.bind(this) }
							selectedKey={ selectedKey }

							searchType={['base','advanced']}
							searchsBaseValue={ searchParamsAd.remark }
							setShowSearchAd={ bool => { actions.setShowSearchAd(bool) } }
							showSearchAd={ showSearchAd }
							hideSearchAd={ () => actions.setShowSearchAd(false) }
							searchsAd={ <Form horizontal>{this.getSearchs()}</Form> }
							buttonsAd={ this.getTabButtonsAd() }
							onSearch={ v => { actions.doTableLoading() } }
							onSearchChange={ v => { actions.saveSearchParamsFields({remark: {name: "remark", value: v}}) } }
						/>
						<WeaTable 
							loading={ loading }
							sessionkey={ sessionkey } 
							hasOrder={ true } 
							needScroll={ false }
							getColumns={ this.getNewColumns }
						/>
                	</WeaTop>
				</WeaRightMenu>
            </div>
		);
	}

	// 切换tab事件
	changeTab(theKey) {
		const { actions } = this.props;

		actions.setSelectedKey(theKey)
		actions.saveSearchParamsFields();

		actions.setShowSearchAd(false);
		actions.doTableLoading();
	}

	// 右键菜单
    getRightMenu() {
		let btns = [];
        btns.push({
			key: "search",
            icon: <i className='icon-coms-search'/>,
            content: '搜索'
        });
		btns.push({
			key: "ShowCustomColumns",
            icon: <i className='icon-coms-Custom'/>,
            content: '显示定制列'
		});
		/*
		btns.push({
			key: "collection",
            icon: <i className='icon-coms-Custom'/>,
            content: '收藏'
		});
		btns.push({
			key: "help",
            icon: <i className='icon-coms-Custom'/>,
            content: '帮助'
		});
		*/
        return btns;
	}
	//右键菜单事件绑定
    onRightMenuClick(key) {
		const { actions, sessionkey, selectedKey, searchParamsAd } = this.props;
		if (key == "search") {
			actions.setShowSearchAd(false);
			actions.doTableLoading();
		} else if (key == "ShowCustomColumns") {
			actions.setColSetVisible(sessionkey, true);
			actions.tableColSet(sessionkey, true)
		} 
	}

	//高级搜索group定义
    getSearchs() {
        return [
            (<WeaSearchGroup needTigger={true} title={this.getTitle()} showGroup={this.isShowFields()} items={this.getFields()}/>)
        ]
	}
	//高级搜索分组标题
	getTitle(index = 0) {
        const {conditioninfo} = this.props;
        return !isEmpty(conditioninfo) && conditioninfo[index].title
	}
	//高级搜索分组是否展示
    isShowFields(index = 0) {
        const {conditioninfo} = this.props;
        if(conditioninfo.length == 0){
            return true;
        }
        return !isEmpty(conditioninfo) && conditioninfo[index].defaultshow
    }
    // 0 常用条件，1 其他条件
    getFields(index = 0) {
        const {conditioninfo} = this.props;
        const fieldsData = !isEmpty(conditioninfo) && conditioninfo[index].items;
		let items = [];
        fieldsData && fieldsData.forEach( (field) => {
            items.push({
				com:(
					<FormItem
						label={`${field.label}`}
						labelCol={{span: `${field.labelcol}`}}
						wrapperCol={{span: `${field.fieldcol}`}}>
                    {WeaTools.getComponent(field.conditionType, field.browserConditionParam, field.domkey, this.props,field)}
                    </FormItem>),
                colSpan:1
			});
        });
        return items;
    }
	//高级搜索底部按钮定义
    getTabButtonsAd() {
        const { actions } = this.props;
        return [
            (<Button type="primary" onClick={()=>{ actions.setShowSearchAd(false); actions.doTableLoading(); }}>搜索</Button>),
            (<Button type="ghost" onClick={()=>{actions.saveSearchParamsFields()}}>重置</Button>),
            (<Button type="ghost" onClick={()=>{actions.setShowSearchAd(false)}}>取消</Button>)
        ]
	}

	//自定义过滤渲染
	getNewColumns(columns) {
        let newColumns = '';
        newColumns = columns.map(column => {
                let newColumn = column;
				newColumn.render = (text, record, index) => { //前端元素转义
					//console.info(text, record, index);
                    let valueSpan = record[newColumn.dataIndex + "span"] !== undefined ? record[newColumn.dataIndex + "span"] : record[newColumn.dataIndex];
                    let url = '';
					if(newColumn.dataIndex == 'coworkname') {
						url = '/cowork/ViewCoWork.jsp?id=' + record.coworkid;
					}
                    return(
						url == '' ?
							<div className="wea-url" dangerouslySetInnerHTML={{__html: valueSpan}} />
						:
							<div className="wea-url-name" style={{cursor:'pointer'}} title={text} onClick={()=>{window.open(url)}} dangerouslySetInnerHTML={{__html: valueSpan}} />
                    )
                }
                return newColumn;
			});
		return columns;
	}
}


// form 表单与 redux 双向绑定
Mine = Form.create({
	onFieldsChange(props, fields) {
		props.actions.saveSearchParamsFields({...props.fields, ...fields});
	}, 
	mapPropsToFields(props) {
		return props.fields;
	}
})(Mine);

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}
Mine = WeaTools.tryCatch(React, MyErrorHandler, { error: "" })(Mine);

// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	const { loading, title, actions, topTab, selectedKey, conditioninfo, showSearchAd, fields, searchParamsAd, sessionkey } = state.coworkMine;
	const { comsWeaTable } = state;
	return { loading, title, actions, topTab, selectedKey, conditioninfo, showSearchAd,  fields, searchParamsAd, sessionkey, comsWeaTable };
	
}
// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators({...MineAction, ...WeaTableAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Mine);
