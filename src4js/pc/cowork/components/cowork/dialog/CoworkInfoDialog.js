import {Button} from 'antd';
import {WeaDialog} from 'ecCom';
import CoworkConstant from '../../../constants/CoworkConstant';
class CoworkInfoDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }
    render() {
        let {visible} = this.state;
        return (
            <WeaDialog visible={visible} title="新建协作"
                       icon={CoworkConstant.iconClassName} iconBgcolor={CoworkConstant.iconBgColor}
                       style={{width: 650, height: 600}}
                       onCancel={this.back}
                       buttons={[
                           <Button type="primary" onClick={this.submit}>提交</Button>,
                           <Button type="primary" onClick={this.back}>返回</Button>,
                       ]}>
                新建协作（和编辑合在一起的话注意title要修改）
            </WeaDialog>
        );
    }
    
    open = () => {
        this.setState({
            visible: true
        });
    }
    back = () => {
        this.setState({
            visible: false
        });
    }
    
    submit = () => {
        console.log("submit");
        this.back();
    }
}

export default CoworkInfoDialog;