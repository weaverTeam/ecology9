import {Button} from 'antd';
import {WeaDialog} from 'ecCom';
import CoworkConstant from '../../../constants/CoworkConstant';
class CoworkTagDialog extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            visible: false
        }
	}
	render() {
	    let {visible} = this.state;
		return (
			<WeaDialog visible={visible} title="协作标签设置"
                       icon={CoworkConstant.iconClassName} iconBgcolor={CoworkConstant.iconBgColor}
                       style={{width: 650, height: 600}}
                       onCancel={this.back}
                       buttons={[
                           <Button type="primary" onClick={this.submit}>提交</Button>,
                           <Button type="primary" onClick={this.back}>返回</Button>,
                       ]}>
                协作标签设置
            </WeaDialog>
		);
	}
	
	open = () => {
		this.setState({
		    visible: true
        });
	}
	back = () => {
        this.setState({
            visible: false
        });
	}
	
	submit = () => {
        console.log("submit");
        this.back();
	}
}

export default CoworkTagDialog;