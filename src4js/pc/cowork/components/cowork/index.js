import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {Button, Tooltip} from 'antd';
import { WeaErrorPage, WeaTools,WeaTop,WeaRightMenu } from 'ecCom'
import CoworkConstant from '../../constants/CoworkConstant';
import CoworkInfoDialog from './dialog/CoworkInfoDialog';
import CoworkTagDialog from './dialog/CoworkTagDialog';
import CoworkField from './CoworkField';
import CoworkTable from './CoworkTable';


const dropMenuList = [
    {
        key: 'add',
        icon: <span className="icon-coms-plus"></span>,
        content: '新建协作'
    }, {
        key: 'blockSet',
        icon: <span className="icon-coms-Print-log"></span>,
        content: '版块设置'
    }, {
        key: 'typeSet',
        icon: <span className="icon-coms-Journal"></span>,
        content: '类别设置'
    }
];

class Main extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            type: 'field'
        }
	}
	render() {
	    let {type} = this.state;
		return (
			<div className="wea-cowork-main">
                <WeaRightMenu datas={dropMenuList} onClick={this.onDropMenuClick}>
                    <WeaTop title="协作交流"
                            icon={CoworkConstant.icon}
                            iconBgcolor={CoworkConstant.iconBgColor}
                            buttons={[
                                <Button type="primary" onClick={this.add} >新建协作</Button>,
                                <Button type="primary" onClick={this.setTag}>标签设置</Button>,
                                <span className="view-change">
                                    <Tooltip title="两栏视图">
                                        <i className={`icon-coms-Column ${type=='field' ? 'cur':''}`} onClick={() => this.setState({type: 'field'})}/>
                                    </Tooltip>
                                    <Tooltip title="列表视图">
                                        <i className={`icon-coms-Tile-o ${type=='list' ? 'cur':''}`} onClick={() => this.setState({type:'list'})}/>
                                    </Tooltip>
                                </span>
                                ]}
                            showDropIcon={true}
                            dropMenuDatas={dropMenuList}
                            onDropMenuClick={this.onDropMenuClick}
                    >
                        <div className="h100">
                            {type == 'field' ?
                                <CoworkField></CoworkField>
                                : <CoworkTable></CoworkTable>
                            }
                        </div>
                    </WeaTop>
                </WeaRightMenu>
                <CoworkInfoDialog ref="info"/>
                <CoworkTagDialog ref="tag"/>
            </div>
		);
	}
	
	add = () => {
	    this.refs.info.open();
	}
	
	setTag = () => {
        this.refs.tag.open();
	}
    
    onDropMenuClick = (key) => {
    	if (key == 'add') {
    	    this.add();
        } else {
            console.log("key", key);
        }
    }
}

//组件检错机制
class MyErrorHandler extends Component {
    render () {
        const hasErrorMsg = this.props.error && this.props.error !== ''
        return (
            <WeaErrorPage msg={ hasErrorMsg ? this.props.error : '对不起，该页面异常，请联系管理员！' }/>
        )
    }
}

// 把 state map 到组件的 props 上
const mapStateToProps = state => {
    return {};
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators({}, dispatch)
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(
    WeaTools.tryCatch(
        React,
        MyErrorHandler,
        {error: ''}
    )(Main)
)