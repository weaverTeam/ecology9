import * as types from '../constants/ActionTypes'
import * as ItemMonitorApis from '../apis/itemMonitor'
import { message } from 'antd';
import isEmpty from 'lodash/isEmpty';
import { WeaTools } from 'ecCom';
import { WeaTable } from 'comsRedux';
import * as CoworkUtil from '../util/coworkUtil'
const WeaTableAction = WeaTable.action;

// 加载列表数据
export const doTableLoading = (params) => {
	return (dispatch, getState) => {
		dispatch({ type:types.SET_LOADING, loading: true });
		
		const allState = getState();
		const coworkItemMonitorState = allState.coworkItemMonitor;

		let resultParams = {};
		if(coworkItemMonitorState) {
			let newFiells = CoworkUtil.getNewFiels(coworkItemMonitorState.fields);

			let selectedTreeKeys = coworkItemMonitorState.selectedTreeKeys.length > 0 ? coworkItemMonitorState.selectedTreeKeys[0] : "";
			let key = selectedTreeKeys ? selectedTreeKeys.substring(selectedTreeKeys.indexOf("_") + 1) : "";

			let selectedKey = coworkItemMonitorState.selectedKey ? coworkItemMonitorState.selectedKey : 'all';  

			let commonParams = {
				mainid : (selectedTreeKeys && selectedTreeKeys.indexOf("cMainType_") != -1) ? key : "",  //协作类型
				datetype : selectedKey   //时间类型
			};
			resultParams = {...newFiells, ...commonParams };
		}
		resultParams = { ...resultParams, ...params };

		ItemMonitorApis.getCoworkItemMonitorList(resultParams).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey));
			dispatch({
				type: types.ITEM_MONITOR_TABLE_LOADING,
				sessionkey: data.sessionkey
			});
			dispatch({ type:types.SET_LOADING, loading: false });
		});
	}
}

// 初始化收藏的帖子页面查询条件
export const initConditionDatas = params => {
	return (dispatch, getState) => {
		ItemMonitorApis.getCoworkItemMonitorShareCondition(params).then((data) => {
			const { conditioninfo, topTab } = data;
			dispatch({
				type: types.ITEM_MONITOR_INIT_CONDITION,
				topTab: topTab,
				conditioninfo: conditioninfo
			});
		});
	}
}

// 控制高级搜索是否显示
export const setShowSearchAd = bool => {
	return (dispatch, getState) => {
		dispatch({
			type: types.ITEM_MONITOR_SHOW_SEARCHAD,
			value: bool
		})
	}
}

// 保存当前所选协作主题树  key
export const setSelectedTreeKeys = (value = []) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.ITEM_MONITOR_SELECTED_TREE_KEYS,
			value: value
		})
	}
}

// 保存当前所选tab  key
export const setSelectedKey = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.ITEM_MONITOR_SELECTED_KEY,
			value: value
		})
	}
}

// 保存高级搜索查询条件值
export const saveSearchParamsFields = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.ITEM_MONITOR_SAVE_SEARCH_FIELDS,
			value: value
		})
	}
}

// 加载左侧 板块树
export const loadLeftTree = params => {
	return (dispatch, getState) => {
		ItemMonitorApis.getCoworkTreeDatas(params).then((data) => {
			const { treeDatas, treeTypes, treeCounts } = data;
			dispatch({
				type: types.ITEM_MONITOR_LOADING_LEFT_TREE,
				treeDatas: treeDatas,
				treeTypes: treeTypes,
				treeCounts: treeCounts
			});
		});
	}
}

export const batchCoworkItemMonitorDel = params => {
	return (dispatch, getState) => {
		ItemMonitorApis.batchCoworkItemMonitorDel(params);
	}
}

export const batchCoworkItemMonitorEnd = params => {
	return (dispatch, getState) => {
		ItemMonitorApis.batchCoworkItemMonitorEnd(params);
	}
}

export const batchCoworkItemMonitorTop = params => {
	return (dispatch, getState) => {
		ItemMonitorApis.batchCoworkItemMonitorTop(params);
	}
}

export const batchCoworkItemMonitorCancelTop = params => {
	return (dispatch, getState) => {
		ItemMonitorApis.batchCoworkItemMonitorCancelTop(params);
	}
}