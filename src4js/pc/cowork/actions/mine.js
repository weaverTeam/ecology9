import * as types from '../constants/ActionTypes'
import * as MineApis from '../apis/mine'
import { WeaTable } from 'comsRedux'
import isEmpty from 'lodash/isEmpty';
import * as CoworkUtil from '../util/coworkUtil'
const WeaTableAction = WeaTable.action;

const getNewFiels=(fields={}) => {
	let params = {};
    if(!isEmpty(fields)){
    	for (let key in fields) {
	    	params[fields[key].name] = fields[key].value
    	}
	}
    return params
}

// 加载列表数据
export const doTableLoading = params => {
	return (dispatch, getState) => {
		dispatch({ type:types.SET_LOADING, loading: true });

		const coworkMineState = getState()['coworkMine'];
		const { selectedKey } = coworkMineState;

		let resultParams = {};
		if(coworkMineState) {
			let newFiells = CoworkUtil.getNewFiels(coworkMineState.fields);

			let commonParams = {
				searchType : selectedKey ? selectedKey : 'all'
			};
			resultParams = {...newFiells, ...commonParams };
		}
		resultParams = { ...resultParams, ...params };


		MineApis.getCoworkMineList(resultParams).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey));
			dispatch({
				type: types.MINE_TABLE_LOADING,
				selectedKey: selectedKey,
				searchParamsAd: { ...params },
				sessionkey: data.sessionkey
			});
			dispatch({ type:types.SET_LOADING, loading: false });
		});
	}
}

// 初始化收藏的帖子页面查询条件
export const initConditionDatas = params => {
	return (dispatch, getState) => {
		MineApis.getCoworkMineShareCondition(params).then((data) => {
			const { conditioninfo, topTab } = data;
			dispatch({
				type: types.MINE_INIT_CONDITION,
				topTab: topTab,
				conditioninfo: conditioninfo
			});
		});
	}
}

// 控制高级搜索是否显示
export const setShowSearchAd = bool => {
	return (dispatch, getState) => {
		dispatch({
			type: types.MINE_SHOW_SEARCHAD,
			value: bool
		})
	}
}

// 保存当前所选tab  key
export const setSelectedKey = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.MINE_SELECTED_KEY,
			value: value
		})
	}
}

// 保存高级搜索查询条件值
export const saveSearchParamsFields = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.MINE_SAVE_SEARCH_FIELDS,
			value: value
		})
	}
}