import * as types from '../constants/ActionTypes'
import * as CollectApis from '../apis/collect'
import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action;

// 加载列表数据
export const doTableLoading = params => {
	return (dispatch, getState) => {
		dispatch({ type:types.SET_LOADING, loading: true });
		CollectApis.getCoworkCollectList(params).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey));
			dispatch({
				type: types.COLLECT_TABLE_LOADING,
				sessionkey: data.sessionkey
			});
			dispatch({ type:types.SET_LOADING, loading: false });
		});
	}
}

// 初始化收藏的帖子页面查询条件
export const initConditionDatas = params => {
	return (dispatch, getState) => {
		CollectApis.getCoworkCollectShareCondition(params).then((data) => {
			const { conditioninfo } = data;
			dispatch({
				type: types.COLLECT_INIT_CONDITION,
				conditioninfo: conditioninfo
			});
		});
	}
}

// 初始化收藏的帖子页面查询条件
export const cancelCoworkCollect = params => {
	return (dispatch, getState) => {
		dispatch({ type:types.SET_LOADING, loading: true });
		let collectIds = {collectIds: params.collectIds};
		let searchParamsAd = params.searchParamsAd;
		CollectApis.cancelCoworkCollect(collectIds).then((data) => {
			CollectApis.getCoworkCollectList(searchParamsAd).then((data) => {
				dispatch(WeaTableAction.getDatas(data.sessionkey));
				dispatch({
					type: types.COLLECT_TABLE_LOADING,
					searchParamsAd: { ...params },
					sessionkey: data.sessionkey
				});
				dispatch({ type:types.SET_LOADING, loading: false });
			});
		});
	}
}

// 控制高级搜索是否显示
export const setShowSearchAd = bool => {
	return (dispatch, getState) => {
		dispatch({
			type: types.COLLECT_SHOW_SEARCHAD,
			value: bool
		})
	}
}

// 保存高级搜索查询条件值
export const saveSearchParamsFields = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.COLLECT_SAVE_SEARCH_FIELDS,
			value: value
		})
	}
}