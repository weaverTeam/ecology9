import {WeaTools} from 'ecCom'

//协作 - 我的帖子 - 列表
export const getCoworkMineList = params => {
	// searchType : 全部：空， "approve" : 待审批

	return WeaTools.callApi('/api/cowork/mine/getCoworkMineList', 'GET', params);
}

//协作 - 我的帖子 - 高级查询条件
export const getCoworkMineShareCondition = params => {
	return WeaTools.callApi('/api/cowork/mine/getCoworkMineShareCondition', 'GET', params);
}
