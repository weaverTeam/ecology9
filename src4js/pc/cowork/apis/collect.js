import {WeaTools} from 'ecCom'

//协作-收藏的帖子-列表
export const getCoworkCollectList = params => {
	return WeaTools.callApi('/api/cowork/collect/getCoworkCollectList', 'GET', params);
}

//协作-收藏的帖子-高级搜索条件
export const getCoworkCollectShareCondition = params => {
	return WeaTools.callApi('/api/cowork/collect/getCoworkCollectShareCondition', 'GET', null);
}

//协作-收藏的帖子-取消收藏
export const cancelCoworkCollect = params => {
	return WeaTools.callApi('/api/cowork/collect/cancelCoworkCollect', 'POST', params);
}
