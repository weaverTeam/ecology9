import {WeaTools} from 'ecCom'

//协作 - 主题监控 - 列表
export const getCoworkItemMonitorList = params => {
	// mainid ： 协作类别类型
	// datetype : 日期类型（全部:all;负责人一年未更新:principal;参与人一年未更新:partner;)
	return WeaTools.callApi('/api/cowork/itemMonitor/getCoworkItemMonitorList', 'GET', params);
}

//协作 - 主题监控 - 高级查询条件
export const getCoworkItemMonitorShareCondition = params => {
	return WeaTools.callApi('/api/cowork/itemMonitor/getCoworkItemMonitorShareCondition', 'GET', params);
}

//协作 - 主题监控 - 批量删除
export const batchCoworkItemMonitorDel = params => {
	return WeaTools.callApi('/api/cowork/itemMonitor/batchCoworkItemMonitorDel', 'POST', params);
}

//协作 - 主题监控 - 批量结束
export const batchCoworkItemMonitorEnd = params => {
	return WeaTools.callApi('/api/cowork/itemMonitor/batchCoworkItemMonitorEnd', 'POST', params);
}

//协作 - 主题监控 - 置顶
export const batchCoworkItemMonitorTop = params => {
	return WeaTools.callApi('/api/cowork/itemMonitor/batchCoworkItemMonitorTop', 'POST', params);
}

//协作 - 主题监控 - 取消置顶
export const batchCoworkItemMonitorCancelTop = params => {
	return WeaTools.callApi('/api/cowork/itemMonitor/batchCoworkItemMonitorCancelTop', 'POST', params);
}

//协作 - 主题监控 - 左侧板块树
export const getCoworkTreeDatas = params => {
	return WeaTools.callApi('/api/cowork/base/getCoworkTreeDatas?menuType=themeMonitor', 'GET', params);
}
