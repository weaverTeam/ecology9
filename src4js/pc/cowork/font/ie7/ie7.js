/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icon-cooperation\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-cooperation-BatchOperation': '&#xe900;',
		'icon-cooperation-Collection-o': '&#xe901;',
		'icon-cooperation-Collection': '&#xe902;',
		'icon-cooperation-comment': '&#xe903;',
		'icon-cooperation-delete': '&#xe904;',
		'icon-cooperation-edit': '&#xe905;',
		'icon-cooperation-Fabulous': '&#xe906;',
		'icon-cooperation-follow-o': '&#xe907;',
		'icon-cooperation-follow': '&#xe908;',
		'icon-cooperation-Quote': '&#xe909;',
		'icon-cooperation-Set-up': '&#xe90a;',
		'icon-cooperation-top': '&#xe90b;',
		'icon-cooperation-PendingApproval': '&#xe90c;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
