import objectAssign from 'object-assign';
import { Route } from 'react-router';

import "./css/icon.css";

import reducers from "./reducers";

// UI建模
import UIMode from "./components/uimode";

const { Applications } = UIMode;

let reducer = objectAssign({},
    reducers
);

const Routes = (
    <Route path="mobilengine">
        <Route path="uimode">
            <Route path="app" component={ Applications } />
        </Route>
    </Route>
);

module.exports = {
    reducer,
    Route:Routes
}

