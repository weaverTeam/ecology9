import Immutable from 'immutable'
import { APP_LIST, SKIN_LIST, STATUS_CHANGE, CLASSIFY_CHANGE, SEARCHTEXT_CHANGE, SORTTYPE_CHANGE, MODIFY,  PUBLISH, DELETE, TOGGLE_IMPORT, SHOW_APPSKIN, HIDE_APPSKIN } from "../constants/app";

const initialState = Immutable.fromJS({
    data: {
        items: [],
        appcount: 0,
        publishedapp: 0,
        unpublishedapp: 0,
    },
    skinlist: [],
    statusType: -1,
    classify: -1,
    searchText: "",
    isImport: false,
    isSetSkin: false,
    appid: -1
}); 

export default app = (state = initialState, action ) => {
    switch(action.type) {
        case APP_LIST: 
        case MODIFY:
        case PUBLISH:
        case DELETE:
        case SORTTYPE_CHANGE:
            return state.merge({
                data: action.data
            });
        case SKIN_LIST: 
            return state.merge({
                skinlist: action.skinlist
            });
        case TOGGLE_IMPORT:
            return state.merge({
                isImport: action.isImport
            });
        case HIDE_APPSKIN: 
            return state.merge({
                isSetSkin: action.isSetSkin,
                skinlist: action.skinlist
            });
        case SHOW_APPSKIN: 
            return state.merge({
                isSetSkin: action.isSetSkin,
                appid: action.appid
            })
        case STATUS_CHANGE: 
            return state.merge({
                statusType: action.statusType
            });
        case CLASSIFY_CHANGE: 
            return state.merge({
                classify: action.classify
            });
        case SEARCHTEXT_CHANGE: 
            return state.merge({
                searchText: action.searchText
            });
        default: 
            return state;
    }
}