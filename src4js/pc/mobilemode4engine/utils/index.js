import { message } from "antd"
import { WeaTools } from 'ecCom';
import { API_URL, SPRIT } from "../constants";

const { success } = message;
const { callApi } = WeaTools;

module.exports = {
    _api: (mod, settings) => { // 封装callApi
        let { data, action, type, dataType, message, url } = settings;

        url = url || (API_URL + mod + SPRIT + action); // url
        dataType = dataType || "json";
        type = type || "GET";
        
        return callApi(url, type, data, dataType)
            .then(data => {
                message && success(message);
                return data;
            });
    }
}