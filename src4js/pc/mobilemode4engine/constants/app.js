// actions type
export const APP_LIST = "applist";
export const SKIN_LIST = "skinlist";
export const MODIFY = "modify";
export const PUBLISH = "publish";
export const WASTE = "waste";
export const DELETE = "delete";
export const TOGGLE_IMPORT = "toggle_import";
export const HIDE_APPSKIN = "hide_appskin";
export const SHOW_APPSKIN = "show_appskin";
export const STATUS_CHANGE = "status_change";
export const CLASSIFY_CHANGE = "classify_change";
export const SEARCHTEXT_CHANGE = "search_text_change";
export const SORTTYPE_CHANGE = "sorttype_change";

// app sort type
export const sortType = {
    DEFAULT: "default",
    DESC: "desc",
    ASC: "asc"
};

// app property 
export const CREATE_DATE = "createdate";
export const MODIFY_DATE = "modifydate";