import ObjectAssign from "object-assign";
import { applist, skinlist, setSkin, publish, del, waste, appExport, modify } from "../apis/app";
import {
    APP_LIST,
    SKIN_LIST,
    STATUS_CHANGE,
    CLASSIFY_CHANGE,
    SEARCHTEXT_CHANGE,
    sortType,
    SORTTYPE_CHANGE,
    PUBLISH, DELETE,
    WASTE,
    TOGGLE_IMPORT,
    HIDE_APPSKIN,
    SHOW_APPSKIN,
    MODIFY,
    CREATE_DATE,
    MODIFY_DATE
} from "../constants/app";

const getAppList = params => {
    return (dispatch, getState) => {
        applist(params).then(data => {
            dispatch({
                type: APP_LIST,
                data: data
            });
        })
    }
};

const getSkinList = params => {
    return (dispatch, getState) => {
        skinlist(params).then(res => {
            dispatch({
                type: SKIN_LIST,
                skinlist: res.data
            })
        });
    }
}

const toggleImport = params => {
    return (dispatch, getState) => {
        dispatch({
            type: TOGGLE_IMPORT,
            isImport: params
        });
    }
}

const hideAppSkin = () => {
    return (dispatch, getState) => {
        dispatch({
            type: HIDE_APPSKIN,
            isSetSkin: false,
            skinlist: []
        });
    }
}

const showAppSkin = params => {
    return (dispatch, getState) => {
        dispatch({
            type: SHOW_APPSKIN,
            isSetSkin: true,
            appid: params
        });
    }
}

const setAppSkin = params => {
    return (dispatch, getState) => {
        return setSkin(params);
    }
}

const updateApp = params => {
    return (dispatch, getState) => {
        return modify(params).then(() => {
            const { app } = getState();
            let data = app.toJSON().data;

            data.items.every((app, index) => {
                if (app.id == params.appid) {
                    data.items[index] = ObjectAssign({}, app, params);
                    return false;
                }
                return true;
            });

            dispatch({
                type: MODIFY,
                data: data
            });
        });
    }
}

const publishApp = params => {
    return (dispatch, getState) => {
        publish(params).then(() => {
            const { app } = getState();
            let data = app.toJSON().data;
            let ispublish = params.optType;
            let exp = ispublish == 1 ? -1 : 1; // 发布 -1 未发布 +1

            data.items.every(app => {
                if (app.id == params.appid) {
                    app.ispublish = ispublish;
                    return false;
                }
                return true;
            });

            data.publishedapp -= exp;
            data.unpublishedapp -= exp;
            data.appcount -= exp;

            dispatch({
                type: PUBLISH,
                data: data
            });
        });

    }
};

const exportApp = params => {
    return (dispatch, getState) => {
        appExport(params).then((data) => {
            window.open(data.url, "_self");
        });
    }
};

const removeApp = (params, actionType) => {
    let _remove = actionType == DELETE ? del : waste;

    return (dispatch, getState) => {
        _remove(params).then(() => {
            const { app } = getState();
            let data = app.toJSON().data;
            let ispublish = 1;

            data.items.every((app, index) => {
                if (app.id == params.appid) {
                    data.items.splice(index, 1);
                    ispublish = app.ispublish;

                    return false;
                }
                return true;
            });

            if (ispublish == 1) {
                data.publishedapp -= 1;
            } else {
                data.unpublishedapp -= 1;
            }

            dispatch({
                type: actionType,
                data: data
            });
        });
    };
}

const wasteApp = params => {
    return removeApp(params, WASTE);
};

const deleteApp = params => {
    return removeApp(params, DELETE);
}

const statusChange = statusType => {
    return (dispatch, getState) => {
        dispatch({
            type: STATUS_CHANGE,
            statusType: statusType
        });
    }
}

const classifyChange = classify => {
    return (dispatch, getState) => {
        dispatch({
            type: CLASSIFY_CHANGE,
            classify: classify
        });
    }
}

const searchTextChange = searchText => {
    return (dispatch, getState) => {
        dispatch({
            type: SEARCHTEXT_CHANGE,
            searchText: searchText
        });
    }
}

const { DEFAULT, DESC, ASC } = sortType;

const sortTypeChange = sortType => {
    return (dispatch, getState) => {
        const { app } = getState();
        let data = app.toJSON().data;

        dispatch({
            type: SORTTYPE_CHANGE,
            data: _sort(data, sortType)
        });
    }
}

const _sort = (data, sortType) => {
    let prop = "createdate";
    let sign = 1;

    if(sortType == DESC) {
        prop = "modifydate";
        sign = -1;
    }

    data.items.sort((app1, app2) => {
        let date1 = app1[prop];
        let date2 = app2[prop];

        if(!date1 && date2) return 1;
        if(date1 && !date2) return -1;
        if(date1 == date2) return 0;

        return new Date(date1) < new Date(date2) ? -1*sign : 1*sign;
    });
    return data;
}

module.exports = {
    getAppList,
    getSkinList,
    updateApp,
    setAppSkin,
    publishApp,
    exportApp,
    wasteApp,
    deleteApp,
    toggleImport,
    hideAppSkin,
    showAppSkin,
    statusChange,
    classifyChange,
    searchTextChange,
    sortTypeChange
}