import { Modal } from "antd";
import { WeaUpload } from 'ecCom';

export default class AppImport extends React.Component {
    uploadApp() {
        const { hideAppUpload } = this.props;

        //startUploadAll();
        hideAppUpload()
    }

    render() {
        const { visible, hideAppUpload } = this.props;

        return (
            <Modal
                title="应用导入"
                visible={visible}
                onOk={this.uploadApp.bind(this)}
                onCancel={hideAppUpload}>
                <WeaUpload 
                    uploadId="appupload"
                    autoUpload={false}
                    uploadUrl="/api/mobilemode/admin/app/import"
                    category="0">
                </WeaUpload>
            </Modal>
        );
    }
}