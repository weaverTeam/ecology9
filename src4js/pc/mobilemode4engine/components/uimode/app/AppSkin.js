import { Modal, Card, Col } from "antd";
import { WeaNewScroll } from 'ecCom';

export default class AppSkin extends React.Component {
    constructor() {
        super();
        this.selectSkin = this.selectSkin.bind(this);
        this.state = {
            selectedId: -1,
            isConfirmLoading: false
        }
    }

    selectSkin(skinid, isFisrtInit) {
        const { selectedId } = this.state;

        if(isFisrtInit && selectedId != -1) return;

        if(selectedId == skinid) {
            skinid = 0;
        }
        this.setState({
            selectedId: skinid
        });
    }

    setAppSkin() {
        const { appid, setAppSkin, hideAppSkin } = this.props;
        const { selectedId } = this.state;
        const params = {
            appid: appid,
            skinid: selectedId
        };
        
        this.setState({
            isConfirmLoading: true
        });
        setAppSkin(params).then(hideAppSkin);
    }

    render() {
        const { selectSkin, props, state } = this;
        const { visible, skinlist, hideAppSkin, appid } = props;
        const { selectedId, isConfirmLoading } = state;
        const using = (
            <a href="javascript:;" style={{position:"relative",top:"-15px",right:"-24px"}}>
                <i className="icon-mobilemode" style={{fontSize:"45px"}}>&#xe610;</i>
            </a>
        );
        
        return (
            <Modal
                title="皮肤设置"
                width="650"
                wrapClassName="appskin-modal"
                visible={visible}
                confirmLoading={isConfirmLoading}
                onOk={this.setAppSkin.bind(this)}
                onCancel={hideAppSkin}>
                <WeaNewScroll scrollId="skinlist" height="500">
                    {
                        skinlist.map( (skin,index) => {
                            if(skin.inuse == 1) {
                                selectSkin(skin.id, true); // 初始化选中皮肤
                            }
                            return (
                                <Col offset={ index%3==0 ? 0 : 1} span={7}>
                                    <Card 
                                        onClick={()=>selectSkin(skin.id)}
                                        className="app-skin" 
                                        bodyStyle={{padding: 0}} 
                                        style={{marginBottom:"15px"}} 
                                        extra={ selectedId == skin.id ? using : null }>
                                        <img src={ skin.prevImg || "/mobilemode/images/noImg.jpg" }/>
                                        <p>{skin.name}</p>
                                    </Card>
                                </Col>
                            )
                        })
                    }
                </WeaNewScroll>
            </Modal>
        );
    }
}