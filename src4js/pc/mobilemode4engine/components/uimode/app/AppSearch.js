import { Tag, Menu, Dropdown, Row, Col } from "antd";

import { sortType } from "../../../constants/app";

const { DEFAULT, DESC, ASC } = sortType;
const MenuItem = Menu.Item;

export default class AppSearch extends React.Component {
    constructor() {
        super();
        this.selectClassify = this.selectClassify.bind(this);
        this.selectSortType = this.selectSortType.bind(this);
        this.state = {
            selectedClassify: -1,
            selectedSortType: DEFAULT
        };
    }

    selectClassify(classify) {
        const { selectedClassify } = this.state;
        const { classifyChange } = this.props;

        if(selectedClassify == classify) return;

        this.setState({
            selectedClassify: classify
        });
        classifyChange(classify);
    }

    selectSortType(sortType) {
        const { selectedSortType } = this.state;
        const { getAppList, sortTypeChange } = this.props;

        if(selectedSortType == sortType) return;

        this.setState({
            selectedSortType: sortType
        });

        if(sortType == DEFAULT) {
            return getAppList();
        }
        sortTypeChange(sortType);
    }

    render() {
        const { selectClassify, selectSortType, props, state } = this;
        const { classifies } = props;
        const { selectedClassify, selectedSortType } = state;
        const menuItems = [
            {key: DEFAULT, text: "默认排序"},
            {key: ASC, text: "最早创建在前"},
            {key: DESC, text: "最后使用在前"}
        ];
        const menu = (
            <Menu>
                {
                    menuItems.map(item => {
                        return (
                            <MenuItem key={item.key}>
                                <a onClick={()=>selectSortType(item.key)} href="javascript:;">
                                    <i className={"icon-mob-sm3 icon-mobilemode icon-sort-by-" + item.key}></i>{item.text}</a>
                            </MenuItem>
                        );
                    })
                }
            </Menu>
        );
        
        return (
            <div className="app-search">
                <Row>
                    <Col span="1" style={{lineHeight:"2.2", width: "32px"}}>类型:</Col>
                    <Col span="23">
                        <Tag color={ selectedClassify == -1 ? "blue" : "" } onClick={()=>selectClassify(-1)}>不限</Tag>
                        {
                            (classifies || []).map( classify => {
                                return (
                                    <Tag 
                                        color={selectedClassify == classify ? "blue" : ""}
                                        onClick={()=>selectClassify(classify)}>
                                        <a href="javascript:;">{classify}</a>
                                    </Tag>
                                );
                            })
                        }
                    </Col>
                </Row>
                {
                    menuItems.map(item=>{
                        if(item.key == selectedSortType) {
                            return (
                                <Dropdown overlay={menu} trigger={["click"]}>
                                    <a className="app-sort" href="javascript:;">
                                        <i className={"icon-mob-sm3 icon-mobilemode icon-sort-by-" + item.key}></i>{item.text}</a>
                                </Dropdown>
                            );
                        }
                    })
                }
            </div>
        );
    }
}