import { bindActionCreators } from "redux";
import { connect } from 'react-redux';

import * as AppAction from "../../../actions/app";

import "./index.less";

import { Button, Dropdown, Menu } from "antd";
import QueueAnim from 'rc-queue-anim';
import { WeaNewScroll } from 'ecCom';

import AppHeader from "./AppHeader";
import AppImport from "./AppImport";
import AppSkin  from "./AppSkin";
import AppSearch from "./AppSearch";
import AppCard from "./AppCard";

class Applications extends React.Component {
    componentWillMount() {
        const { actions, data } = this.props;
        // 初始化应用列表数据
        actions.getAppList();
    }

    render() {
        const { data, statusType, classify, searchText, actions, isImport, skinlist, isSetSkin, appid } = this.props;
        const { items, classifies, appcount, publishedapp, unpublishedapp } = data.toJSON();
        
        return (
            <div className="mob-container">
                <AppHeader 
                    statusChange={actions.statusChange.bind(this)} 
                    showAppImport={()=>actions.toggleImport(true)}
                    searchTextChange={actions.searchTextChange.bind(this)}
                    statusType={statusType}
                    total={appcount} 
                    published={publishedapp} 
                    unpublished={unpublishedapp}>
                </AppHeader>
                <AppSearch 
                    classifies={classifies}
                    getAppList={actions.getAppList.bind(this)}
                    classifyChange={actions.classifyChange.bind(this)}
                    sortTypeChange={actions.sortTypeChange.bind(this)}>
                </AppSearch>
                <div className="apps-wrapper">
                    <WeaNewScroll scrollId="mob-apps">
                        <div className="app-cards">
                            {
                                items.map( (app, index) => {
                                    if((statusType == -1 || statusType == app.ispublish) && // 发布状态过滤
                                       (classify == -1 || classify == app.industry) && // 行业类型过滤
                                       (!searchText || ~app.appname.indexOf(searchText) || ~app.pinyin.indexOf(searchText))) { //搜索关键字过滤
                                        return <AppCard key={index} app={app} actions={actions}></AppCard>;
                                    }
                                })
                            }
                        </div>
                    </WeaNewScroll>
                </div>
                <AppImport
                    visible={isImport}
                    hideAppImport={()=>actions.toggleImport(false)}>
                </AppImport>
                { isSetSkin?
                    <AppSkin
                        appid={appid}
                        visible={isSetSkin}
                        skinlist={skinlist.toJSON()}
                        hideAppSkin={actions.hideAppSkin}
                        setAppSkin={actions.setAppSkin}>
                    </AppSkin> : null
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { app } = state;
    
    return ({
        data: app.get("data"),
        skinlist: app.get("skinlist"),
        statusType: app.get("statusType"),
        classify: app.get("classify"),
        searchText: app.get("searchText"),
        isImport: app.get("isImport"),
        isSetSkin: app.get("isSetSkin"),
        appid: app.get("appid")
    });
}

const mapDispatchToProps = (dispatch) => {
    return { actions: bindActionCreators(AppAction, dispatch) }
}

export default connect(mapStateToProps, mapDispatchToProps)(Applications);