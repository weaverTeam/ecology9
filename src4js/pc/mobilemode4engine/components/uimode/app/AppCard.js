import { Card, Tooltip, Popconfirm, Form, Button, Select, Option } from "antd";
import QueueAnim from 'rc-queue-anim';
import { WeaInput, WeaTextarea } from 'ecCom';
import QRCode from "../../QRCode";
import { DELETE, WASTE } from "../../../constants/app";
import objectAssign from "object-assign";

export default class AppCard extends React.Component {
    render() {
        const { app, actions } = this.props;
        const thum = app.prewImg || "/mobilemode/admin/img/app_default2.png";
        const desc = app.descriptions || "暂无描述";
        const industry = app.industry || "暂无分类";
        
        return (
            <Card className="app-card">
                <AppCardOverlay 
                    app={app} 
                    showAppSkin={(appid)=>{ actions.getSkinList(appid); actions.showAppSkin(appid)}}
                    updateApp={actions.updateApp}
                    publishApp={actions.publishApp}
                    exportApp={actions.exportApp}
                    wasteApp={actions.wasteApp}
                    deleteApp={actions.deleteApp}>
                </AppCardOverlay>
                <div className="info">
                    <div className={app.prewImg ? "thum" : "thum default"}><img src={thum} /></div>
                    <div className="basic">
                        <h3 className="name">{app.appname}</h3>
                        <p className="industry">{industry}</p>
                    </div>
                </div>
                {
                    app.ispublish == "1" && 
                        <div className="published"><i className="icon-mobilemode icon-mobilemode-publish"></i></div>
                }
                <p className="desc">{desc}</p>
                {
                    app.modifydate && 
                        <p className="modify-date">{app.modifydate}</p>
                }
            </Card>
        );
    }
}

class AppCardOverlay extends React.Component {
    constructor() {
        super();
        this.state = {
            isOverlayVisible: false,
            isQRCodeVisible: false,
            isEditing: false
        };
    }

    resetState() {
        this.setState({
            isQRCodeVisible: false,
            isEditing: false
        })
    }

    toggleOverlay(isvisible) {
        this.setState({
            isOverlayVisible: isvisible
        });
    }

    toggleQRCode(isshow) {
        this.setState({
            isQRCodeVisible: isshow
        });
    }

    toggleAppEdit(isshow) {
        this.setState({
            isEditing: isshow
        })
    }

    updateApp(params) {
        const { updateApp } = this.props;

        return updateApp(params)
            .then(this.toggleAppEdit.bind(this, false));
    }

    publishApp() {
        const { publishApp, app } = this.props;

        publishApp({
            appid: app.id, 
            optType: Math.abs(app.ispublish - 1) // 去当前发布状态相反的值
        });
    }

    removeApp(actionType) {
        const { wasteApp, deleteApp, app } = this.props;
        const _removeApp =  actionType == DELETE ? deleteApp : wasteApp;

        _removeApp({
            appid: app.id
        });
    }

    openDesigner() {
        const { app } = this.props;

        window.open("/mobilemode/admin/appDesigner.jsp?appid=" + app.id);
    }

    render() {
        const { app, exportApp, showAppSkin } = this.props;
        const { isOverlayVisible, isQRCodeVisible, isEditing } = this.state;
        const params = { appid: app.id };

        return (
            <div className={ isOverlayVisible ? "overlay active" : "overlay"  } onMouseLeave={this.resetState.bind(this)}>
                <p className="name">{app.appname}</p>
                <div className="icon-mobilemode-qrcode qr-icon" onClick={this.toggleQRCode.bind(this, true)}></div>
                <div className="actions">
                    <div onClick={this.publishApp.bind(this)}>{ app.ispublish == 1 ? "发布" : "未发布" }</div>
                    <div onClick={this.openDesigner.bind(this)} className="active">打开应用</div>
                </div>
                <div className="icons">
                    <Tooltip placement="bottom" title="编辑"> 
                        <a href="javascript:;" onClick={this.toggleAppEdit.bind(this, true)}><i className="icon-mobilemode icon-mobilemode-edit"></i></a>
                    </Tooltip>
                    <Tooltip placement="bottom" title="导出"> 
                        <a onClick={exportApp.bind(this, params)} href="javascript:;"><i className="icon-mobilemode icon-mobilemode-export"></i></a>
                    </Tooltip>
                    <Tooltip placement="bottom" title="皮肤" > 
                        <a href="javascript:;" onClick={ () => showAppSkin(app.id) }><i className="icon-mobilemode icon-mobilemode-skin"></i></a>
                    </Tooltip>
                    <Popconfirm title="你确定要废弃该应用吗?" 
                        onConfirm={ this.removeApp.bind(this, WASTE) }
                        onVisibleChange={ isvisible => { !isvisible && setTimeout(this.toggleOverlay.bind(this, false), 400) }}> 
                        <Tooltip placement="bottom" title="废弃"> 
                            <a onClick={this.toggleOverlay.bind(this, true)} href="javascript:;"><i className="icon-mobilemode icon-mobilemode-hidden"></i></a>
                        </Tooltip>
                    </Popconfirm>
                    <Popconfirm title="你确定要删除该应用吗?" 
                        onConfirm={ this.removeApp.bind(this, DELETE) }
                        onVisibleChange={ isvisible => { !isvisible && setTimeout(this.toggleOverlay.bind(this, false), 400) }}> 
                        <Tooltip placement="bottom" title="删除" > 
                            <a href="javascript:;" onClick={this.toggleOverlay.bind(this, true)}><i className="icon-mobilemode icon-mobilemode-delete"></i></a>
                        </Tooltip>
                    </Popconfirm>
                </div>

                <AppEditorForm 
                    app={app}
                    active={isEditing}
                    hideEditorForm={this.toggleAppEdit.bind(this, false)}
                    updateApp={this.updateApp.bind(this)}>
                </AppEditorForm>
                <AppQRCode 
                    appid={app.id} 
                    active={isQRCodeVisible}
                    hideQRCode={this.toggleQRCode.bind(this, false)}>
                </AppQRCode>
                
            </div>
        );
    }
}

class AppEditorForm extends React.Component {
    constructor() {
        super();
        this.toggleLoading = this.toggleLoading.bind(this);
        this.state = {
            loading: false
        };
    }

    toggleLoading(isloading) {
        this.setState({
            loading: isloading
        });
    }

    updateApp() {
        const { toggleLoading, props } = this;
        const { app, updateApp } = props;
        const params = objectAssign({},{appid: app.id}, this.props.form.getFieldsValue());
        
        toggleLoading(true);
        updateApp(params).then(()=>toggleLoading(false));
    }
    render() {
        const { app, active, hideEditorForm } = this.props;
        const { getFieldProps, setFieldsValue } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 7 },
            wrapperCol: { span: 14 },
        };

        const nameProps = getFieldProps("appname", {
            rules: [{
                required: true,
                message: "请输入应用名称",
            }],
            initialValue: app.appname
        });
        // const industries = ['IT通信业', '商业贸易', '金融业', '服务业', '制造业', '房地产行业', '服装行业', '其他'];
        return (
            <QueueAnim 
                animConfig = {[
                    {opacity:[1, 0],top: [0, "-100%"]},
                    {opacity:[0, 1], top: [0, "-100%"]}
                ]}>
            { active ?
                <div key="1" className="app-editor">
                    <Form horizontal style={{lineHeight: 1.5}} ref={form => {this.Form = form;}} >
                        <Form.Item
                            {...formItemLayout}
                            label="名称">
                            <WeaInput {...nameProps} placeholder="请输入应用名称" />
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="行业">
                            <WeaInput {...getFieldProps("industry", {initialValue: app.industry})} />
                        </Form.Item>
                        <Form.Item
                            {...formItemLayout}
                            label="描述">
                            <WeaTextarea {...getFieldProps("descriptions", {initialValue: app.descriptions}) } style={{padding:0}} />
                        </Form.Item>
                        <Form.Item key="6" wrapperCol={{ span: 16, offset: 7 }}>
                            <Button style={{marginRight:"10px"}} type="primary" loading={this.state.loading} onClick={this.updateApp.bind(this)} >保存</Button>
                            <Button type="ghost" onClick={hideEditorForm}>取消</Button>
                        </Form.Item>
                    </Form>
                </div> : null
            }
            </QueueAnim>
        );
    }
}

AppEditorForm = Form.create()(AppEditorForm);

class AppQRCode extends React.Component {
    render() {
        const { appid, active, hideQRCode } = this.props;
        const text = '/mobilemode/appHomepageViewWrap.jsp?appid=' + appid + "&noLogin=1";

        return (
            <QueueAnim 
                animConfig = {[
                    {opacity:[1, 0],top: [0, "-100%"]},
                    {opacity:[0, 1], top: [0, "-100%"]}
                ]}>
            { active ?
                <div key="2" className="qrcode active" onClick={hideQRCode}>
                    <div className="content">
                        <QRCode Text={ text }></QRCode>
                    </div>
                    <p><img src="/mobilemode/admin/img/72dpi.png" /></p>
                    <p className="tip">扫码前请确认您的手机网络可以访问到当前的ecology</p>
                </div> : null
            }
            </QueueAnim>
        );
    }
}