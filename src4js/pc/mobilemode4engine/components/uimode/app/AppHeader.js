import { Button, Tabs } from "antd";
import { WeaInputSearch } from 'ecCom';

const TabPane = Tabs.TabPane;

export default class AppHeader extends React.Component {
    render() {
        const { showAppImport, statusChange, searchTextChange, statusType, total, published, unpublished } = this.props;
        
        return (
            <header className="mob-header">
                <div className="mob-content">
                    <img src="/mobilemode/admin/img/mobile-app.png" />
                    <div>
                        <h3>移动应用</h3>
                        <Tabs onChange={ key => statusChange(key) }>
                            <TabPane tab={"所有("+ total +")"} key="-1"></TabPane>
                            <TabPane tab={"发布("+ published +")"} key="1"></TabPane>
                            <TabPane tab={"未发布("+ unpublished +")"} key="0"></TabPane>
                        </Tabs>
                    </div>
                </div>
                <div className="mob-options">
                    <Button className="app-create" type="primary">创建应用</Button>
                    <Button type="primary" onClick={ showAppImport }>导入</Button>
                    <WeaInputSearch 
                        onSearchChange={val=>searchTextChange(val)}
                        placeholder="请输入应用名称" />
                </div>
            </header>
        );
    }
}