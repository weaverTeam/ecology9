import "../lib/jquery.qrcode.min";

//二维码组件
class QRCode extends React.Component {
    componentDidMount() {
        const $qrcontainer = $(this.qrContainer);
        const { Width, Height, Text, Render} = this.props;

        $qrcontainer.qrcode({
            width: Width, // int
            height: Height, // int
            text: Text,
            render: Render, // 'canvas' || 'table' 默认canvas
        });
    }

    render() {
        return (
            <div ref={(container) => { this.qrContainer = container } }></div>
        );
    }
}

export default QRCode;