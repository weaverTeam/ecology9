import React, {Component, PropTypes} from "react";
import {connect} from 'react-redux';
import FavouriteDir from "./FavouriteDir"
import SysFavourite from "./SysFavourite"
import {initPage} from "../actions/MyFavouriteActions";
import {loadTree} from "../actions/FavouriteDirActions";
import {loadFavouriteList} from "../actions/SysFavouriteActions";
import SearchFavourite from "../util/SearchFavourite";

class MyFavourite extends Component {
    componentWillMount() {
        const {dispatch} = this.props;
        //页面初始化的一些数据
        dispatch(initPage());
        //加载左侧目录树的数据
        dispatch(loadTree("0"));
        //初始化加载收藏内容第一页的数据
        let isinit = true;
        let searchFavourite = new SearchFavourite(dispatch);
        searchFavourite.load({isinit});
    }

    render() {
        return (
            <div>
                <FavouriteDir/>
                <SysFavourite/>
            </div>
        );
    }
}

export default connect()(MyFavourite);