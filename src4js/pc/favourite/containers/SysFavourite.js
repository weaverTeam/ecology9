import React, {Component, PropTypes} from "react";
import {connect} from 'react-redux';
import {Button, Spin} from "antd";
import {WeaScroll, WeaDialog, WeaPopoverHrm} from 'ecCom'
import SysFavouriteBar from "../components/SysFavouriteBar";
import SysFavouriteList from "../components/SysFavouriteList";
import {loadFavouriteList} from "../actions/SysFavouriteActions";
import {closeDialog} from "../actions/FavouriteDialogActions";
import SearchFavourite from "../util/SearchFavourite";
import EditSysFavourite from "../containers/EditSysFavourite";
import FavouriteBrowser from "../containers/FavouriteBrowser";


class SysFavourite extends Component {

    /**
     * 滚动加载收藏夹的内容
     */
    handleScroll = () => {
        let {dispatch} = this.props;
        let offset = jQuery(".sysfav-item-loadding").offset();
        let winHeight = jQuery(document.body).height();
        let {top} = offset;

        let conditions = SearchFavourite.getConditions();
        let {iscomplete, current, total} = conditions;

        if (top <= winHeight && iscomplete) {   //说明最下方的div可见，加载下一页的数据
            let searchFavourite = new SearchFavourite(dispatch);
            let isinit = false;
            let isreload = false;
            let params = {isinit, isreload};
            searchFavourite.loadPage(params);
        }
    }


    /**
     * 设置加载的提示信息
     */
    setLoaddingMsg = () => {
        let {spinning} = this.props;
        let {total, current, iscomplete} = SearchFavourite.getConditions();
        let conditions = SearchFavourite.getConditions();
        let {loadingmsg, nodata, nomoredata} = this.props.list;
        if (!spinning) {    //没有显示遮罩时，才显示这个div的提示信息
            if (iscomplete) {
                if (current == total) {
                    jQuery(".sysfav-item-loadding").html(nomoredata);
                }
                if (total <= 0) {
                    jQuery(".sysfav-item-loadding").html(nodata);
                }
                if (current < total) {
                    jQuery(".sysfav-item-loadding").html(loadingmsg);
                }
            } else {
                jQuery(".sysfav-item-loadding").html(loadingmsg);
            }
        } else {
            jQuery(".sysfav-item-loadding").html("");
        }
    }

    componentDidUpdate() {
        this.setLoaddingMsg();

        jQuery(".sysfav-main .ant-spin-container").css({"height": "100%"});
    }

    render() {
        let {spinning, tip, bar, list, dispatch, btns, favbrowser} = this.props;
        let props1 = {...bar, dispatch};
        let props2 = {...list, dispatch, ...btns};

        return (
            <div className="sysfav-main">
                <Spin spinning={spinning} tip={tip} size="large" style={{"height": "100%"}}>
                    <WeaPopoverHrm />
                    <SysFavouriteBar {...props1}/>
                    <div className="sysfav-main-list" onScroll={this.handleScroll}>
                        <WeaScroll className="sysfav-scroll-wrapper" typeClass="scrollbar-macosx"
                                   style={{"width": "100%"}}>
                            <SysFavouriteList {...props2}/>
                            <div className="sysfav-item-loadding"></div>
                        </WeaScroll>

                        <EditSysFavourite/>

                        <FavouriteBrowser/>
                    </div>
                </Spin>
            </div>
        );
    }
}

function mapStateToProps(state) {
    let {
        addBtnName, addBtnList, typeList,
        moveBtnName, deleteBtnName, repeatIconTitle,
        editIconTitle, moveIconTitle, deleteIconTitle,
        favTypeMap, loadingmsg, nodata, nomoredata
    } = state.myfavourite;

    let {selectedIds, favtype, spinning, tip} = state.sysfavourite;
    return {
        spinning,
        tip,
        bar: {
            addBtnName,
            addBtnList,
            typeList,
            moveBtnName,
            deleteBtnName,
            selectedIds,
            favtype
        },
        list: {...state.sysfavourite, favTypeMap, loadingmsg, nodata, nomoredata, selectedIds},
        btns: {repeatIconTitle, editIconTitle, moveIconTitle, deleteIconTitle},
        dialog: {...state.favouritedialog}
    };
}

export default connect(mapStateToProps)(SysFavourite);