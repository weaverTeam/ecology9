import React, {Component, PropTypes} from "react";
import objectAssign from 'object-assign'
import Immutable from "immutable";
import {connect} from "react-redux";
import {Form, Input, Select, Button, Modal} from "antd";
import {bindActionCreators} from "redux";
import {WeaDialog} from "ecCom";
import * as  SysFavFormActions from "../actions/SysFavFormActions";
import * as FavouriteDialogActions from "../actions/FavouriteDialogActions";

const Option = Select.Option;
const createForm = Form.create;
const FormItem = Form.Item;

const formItemLayout = {
    labelCol: {span: 6},
    wrapperCol: {span: 18},
};

class EditSysFavourite extends Component {

    /**
     * 关闭窗口
     */
    closeEditFormWin = () => {
        let {actions} = this.props;
        actions.closeDialog("1");
    }

    /**
     * 获取表单的元素
     * @returns {Array}
     */
    getFormItems = () => {
        const {getFieldDecorator} = this.props.form;
        let {pagename, favouritetype, favouriteObjid, favtypename, typelist} = this.props.fieldValues;
        let items = [];
        items.push(
            <FormItem {...formItemLayout} label="收藏标题">
                {getFieldDecorator("pagename", {
                    validateTrigger: ['onChange', 'onBlur'],
                    rules: [{
                        required: true,
                        message: "收藏标题必填"
                    }, {
                        max: 40,
                        message: "收藏标题长度不超过40"
                    }],
                    initialValue: pagename
                })(
                    <Input placeholder="请输入收藏标题"/>
                )}
            </FormItem>
        );


        favouriteObjid = Number(favouriteObjid);
        if (favouriteObjid > 0 || Immutable.is(favouritetype, "6")) {      //收藏的对象存在，或者消息类型，不可编辑
            items.push(
                <FormItem {...formItemLayout} label="收藏类型">
                    {getFieldDecorator('favouritetype', {initialValue: favouritetype})(
                        <Select value={favouritetype} style={{"min-width": "100px"}} disabled="disabled">
                            {typelist.map(item => {
                                return <Option key={item.id} value={item.id}>{item.name}</Option>;
                            })}
                        </Select>
                    )}
                </FormItem>
            );
        } else {
            items.push(
                <FormItem {...formItemLayout} label="收藏类型">
                    {getFieldDecorator('favouritetype', {initialValue: favouritetype})(
                        <Select value={favouritetype} style={{"min-width": "100px"}}>
                            {typelist.map(item => {
                                return <Option key={item.id} value={item.id}>{item.name}</Option>;
                            })}
                        </Select>
                    )}
                </FormItem>
            );
        }
        return items;
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        let {actions, fieldValues} = this.props;
        this.props.form.validateFields((err, values) => {
            if (!err) {    //没有错误，保存数据
                let nvals = objectAssign({}, fieldValues, values);
                actions.saveForm(nvals);
            } else {    //有错误，提示
                for (let key in err) {
                    let {errors} = err[key];
                    if (errors.length > 0) {
                        Modal.error({content: errors[0].message});
                    }
                }
            }
        });
    }

    render() {
        let formItems = this.getFormItems();

        let {visible} = (this.props.dialog)["1"];
        let title = "编辑收藏";
        let style = {width: 440, height: 200};
        let buttons = [
            (<Button onClick={this.handleSubmit}>确定</Button>),
            (<Button onClick={this.closeEditFormWin}>取消</Button>)
        ];

        let onCancel = this.closeEditFormWin;

        let params = {visible, title, style, buttons, onCancel};

        return (
            <WeaDialog {...params}>
                <div className="sysfav-form-container">
                    <Form>{formItems}</Form>
                </div>
            </WeaDialog>
        );
    }
}

EditSysFavourite = createForm({
    onFieldsChange(props, fields) {
        props.actions.saveFormFields({...props, fields});
    },
    mapPropsToFields(props) {
        return props;
    }
})(EditSysFavourite);

function mapStateToProps(state) {
    return {
        fieldValues: {...state.sysfavform},
        dialog: {...state.favouritedialog}
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...SysFavFormActions, ...FavouriteDialogActions}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditSysFavourite);