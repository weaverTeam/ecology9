import React, {Component, PropTypes} from "react";
import {connect} from "react-redux";
import {Modal, message} from "antd";
import FavDirBar from "../components/FavDirBar";
import FavDirTree from "../components/FavDirTree";
import SearchFavourite from "../util/SearchFavourite";
import {WeaScroll} from 'ecCom'

import {setResult} from "../actions/MyfavResultActions";
import {selectTreeNode} from "../actions/FavouriteDirActions";

class FavouriteDir extends Component {
    onTreeNodeSelect = (nodeid) => {
        let {dispatch} = this.props;
        if (nodeid.indexOf("add") == -1 && nodeid.indexOf("edit") == -1) {    //非编辑状态下
            /**
             * 记录下选中的节点
             */
            dispatch(selectTreeNode(nodeid));
            /**
             * 选中树节点后，刷新收藏的列表
             */
            let isinit = true;
            let isreload = false;
            SearchFavourite.setConditions({dirId: nodeid});
            let searchFavourite = new SearchFavourite(dispatch);
            searchFavourite.load({isinit, isreload});
        }
    }

    componentDidUpdate() {
        let {dispatch} = this.props;
        let {success, msg} = this.props.myfavresult;
        if (success == "0") {
            if (!!msg) {
                message.error(msg);
            }
            dispatch(setResult("1"));
        }
    }

    render() {
        return (
            <div className="fav-left">
                <FavDirBar {...this.props}/>
                <div className="fav-left-tree">
                    <WeaScroll className="sysfav-scroll-wrapper" typeClass="scrollbar-macosx" style={{"width": "100%"}}>
                        <FavDirTree {...this.props} onSelect={this.onTreeNodeSelect}/>
                    </WeaScroll>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    let {treeData, expandedKeys, selectedKey, operatingKey, clickExpanded, isover} = state.favouritedir;
    let {addDirTitle, allDirName} = state.myfavourite;
    let myfavresult = state.myfavresult;

    return {
        treeData,
        addDirTitle,
        allDirName,
        expandedKeys,
        selectedKey,
        operatingKey,
        myfavresult,
        clickExpanded,
        isover
    };
}

export default connect(mapStateToProps)(FavouriteDir);

