/************向服务器端发送请求的url************/
export const PAGE_INIT_URL = "/api/myfavourite/init";        //页面初始化


/**
 * 目录树的操作
 */
export const LOAD_TREE_URL = "/api/favourite/query";      //加载目录树

export const ADD_FAVOURITE_DIR = "/api/favourite/add";    //新增目录

export const EDIT_FAVOURITE_DIR = "/api/favourite/edit";    //编辑目录

export const DELETE_FAVOURITE_DIR = "/api/favourite/delete";    //删除目录

export const SAVE_FAVOURITE_DIR = "/api/favourite/save";    //保存目录，主要是节点拖拽后，保存

/**
 * 收藏列表的操作
 */
export const LOAD_FAVOURITE_LIST = "/api/sysfavourite/search";   //收藏内容列表搜索

export const EDIT_IMPORTANT_LEVEL = "/api/sysfavourite/editLevel";   //编辑重要程度

export const DELETE_FAVOURITE = "/api/sysfavourite/delete";     //删除收藏的内容

export const GET_FAVOURITE = "/api/sysfavourite/get";          //根据id查询具体的一条收藏的信息

export const EDIT_FAVOURITE = "/api/sysfavourite/edit";          //编辑收藏

export const MOVE_FAVOURITES = "/api/sysfavourite/move";       //将收藏内容，移动到另一目录

export const ADD_FAVOURITES = "/api/sysfavourite/add";         //保存收藏内容，可以是多条

