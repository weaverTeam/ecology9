export const FAVOURITE_INIT_PAGE = "favourite_init_page";   //收藏夹页面初始化操作

/**
 * 收藏夹目录
 */
export const FAVOURITE_ROOT_OVER = "favourite_root_over";  //根目录（全部目录）是否鼠标移入、移出
export const FAVOURITE_ROOT_SELECTED = "favourite_root_selected";  //根目录（全部目录）是否选中
export const FAVOURITE_ADD_DIR = "favourite_add_dir";   //添加目录
export const FAVOURITE_DELETE_DIR = "favourite_delete_dir";   //删除目录
export const FAVOURITE_EDIT_DIR = "favourite_edit_dir";    //编辑目录
export const FAVOURITE_SELECT_DIR = "favourite_select_dir";    //选中目录
export const FAVOURITE_EXPAND_DIR = "favourite_expand_dir";    //展开目录
export const FAVOURITE_LOAD_TREE = "favourite_load_tree";   //加载树
export const FAVOURITE_CLICK_EXPANDED = "favourite_click_expanded";   //点击，加载树的下级节点
export const FAVOURITE_TOGGLE_OPERATION = "favourite_toggle_operation";    //显示或者隐藏操作按钮(编辑、删除等)
export const FAVOURITE_UPDATE_NODE_DATA = "favourite_update_node_data";    //更新节点数据，比如，节点删除后，更新父节点的信息（是否仍然有子节点)
export const FAVOURITE_DRAG_NODE = "favourite_drag_node";      //节点拖动

/**
 * 弹出窗口，收藏夹目录
 */
export const FAVBROWSER_ADD_DIR = "favbrowser_add_dir";   //添加目录
export const FAVBROWSER_DELETE_DIR = "favbrowser_delete_dir";   //删除目录
export const FAVBROWSER_EDIT_DIR = "favbrowser_edit_dir";    //编辑目录
export const FAVBROWSER_SELECT_DIR = "favbrowser_select_dir";    //选中目录
export const FAVBROWSER_LOAD_TREE = "favbrowser_load_tree";   //加载树
export const FAVBROWSER_CLEAR_DATA = "favbrowser_clear_data";   //关闭窗口时，清空数据，因为是弹出窗口，保证每次打开，数据都是最新的
export const FAVBROWSER_SHOW_OR_HIDE_SPINNING = "favbrowser_show_or_hide_spinning";   //显示遮罩

/**
 * 收藏列表上方的操作按钮等
 */
export const SYSFAVOURITE_ADD_DOC = "sysfavourite_add_doc";    //添加收藏内容：文档
export const SYSFAVOURITE_ADD_WORKFLOW = "sysfavourite_add_workflow";    //添加收藏内容：流程
export const SYSFAVOURITE_ADD_CUSTOMER = "sysfavourite_add_customer";    //添加收藏内容：客户
export const SYSFAVOURITE_ADD_PROJECT = "sysfavourite_add_project";    //添加收藏内容：项目

export const SYSFAVOURITE_CHANGE_TYPE = "sysfavourite_change_type";   //收藏类型变更
export const SYSFAVOURITE_QUERY = "sysfavourite_query";     //查询操作
export const SYSFAVOURITE_MOVE_BTN = "sysfavourite_move_btn";    //目录移动操作：按钮
export const SYSFAVOURITE_DELETE_BTN = "sysfavourite_delete_btn";    //删除操作:按钮

/**
 * 收藏列表
 */
export const SYSFAVOURITE_ITEM_CHECKED = "sysfavourite_item_checked";   //单选按钮的点击事件
export const SYSFAVOURITE_ITEM_STATUS = "sysfavourite_item_status";  //收藏夹列表的状态，鼠标移入、移出
export const SYSFAVOURITE_ITEM_SELECTED = "sysfavourite_item_selected";  //收藏夹列表的状态，是否选中
export const SYSFAVOURITE_ITEM_BTNOVER = "sysfavourite_item_btnover";    //收藏列表，编辑、删除、移动等按钮，鼠标移入、移出
export const SYSFAVOURITE_ITEM_LEVELOVER = "sysfavourite_item_levelover";    //收藏列表，重要程度，鼠标移入、移出
export const SYSFAVOURITE_SET_MOVE_IDS = "sysfavourite_set_move_ids";      //设置需要移动的收藏条目的id
export const SYSFAVOURITE_TOGGLE_SPINING = "sysfavourite_toggle_spining";   //设置加载中的遮罩

export const SYSFAVOURITE_MOVE_ICON = "sysfavourite_move_icon";    //目录移动操作：每条记录中的按钮
export const SYSFAVOURITE_DELETE_ICON = "sysfavourite_delete_icon";    //删除操作：每条记录中的按钮
export const SYSFAVOURITE_EDIT_ICON = "sysfavourite_edit_icon";       //编辑
export const SYSFAVOURITE_SHARE_CLICK = "sysfavourite_share_click";       //点击分享
export const SYSFAVOURITE_SHARE_CALLBACK = "sysfavourite_share_callback";     //分享后的操作


export const SYSFAVOURITE_LOAD_PAGE = "sysfavourite_load_page";       //分页加载收藏内容
export const SYSFAVOURITE_RELOAD_PAGE = "sysfavourite_reload_page";       //重载载收藏内容


/**
 * 收藏的编辑页面
 */
export const SYSFAV_FORM_LOAD = "sysfav_form_load";         //收藏编辑页面，加载数据
export const SYSFAV_FORM_SAVE = "sysfav_form_save";     //保存表单
export const SYSFAV_FORM_FIELD_CHANGE = "sysfav_form_field_change";     //收藏编辑页面，字段值变更


/**
 * 弹出窗口
 */
export const DIALOG_SHOW_OR_HIDE = "dialog_show_or_hide";     //弹出窗口显示或者隐藏

/**
 * 执行结果的处理，各种编辑、保存、加载执行结果的处理,显示提示信息
 */
export const MYFAV_RESULT_SET = "myfav_result_set";

