export default {
    listBtns: {
        "1": {
            type: 37,
            isSingle: false,
            title: "文档",
            customized: true,
            icon: "icon-coms-doc",
            iconBgcolor: "#f14a2d"
        },
        "2": {
            type: 152,
            isSingle: false,
            title: "流程",
            customized: true,
            icon: "icon-coms-workflow",
            iconBgcolor: "#0079de"
        },
        "4": {
            type: 18,
            isSingle: false,
            title: "客户",
            customized: true,
            icon: "icon-coms-crm",
            iconBgcolor: "#96358a"
        },
        "3": {
            type: 135,
            isSingle: false,
            title: "项目",
            customized: true,
            icon: "icon-coms-project",
            iconBgcolor: "#217346"
        }
    }
}