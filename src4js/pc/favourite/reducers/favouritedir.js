import objectAssign from 'object-assign'
import * as types from "../constants/ActionTypes";

import {
    getNewTreeData,
    getAddTreeData,
    getOverTreeData,
    getEditTreeData,
    updateNodeData,
    handleDragNodeData
} from "../util/TreeUtil";

const initialTreeState = {
    treeData: [], /**树节点的数据*/
    clickExpanded: true, /**点击展开节点，默认展开*/
    selectedKey: "-1", /**选中的节点*/
    expandedKeys: [], /**展开的节点*/
    operatingKey: "", /**鼠标移入的节点*/
    isover: false     /**全部目录(根目录)是否鼠标移入*/
};

/**
 * 加载左侧收藏夹目录树
 * @param state
 * @param action
 * @returns {*}
 */
export default function favouritedir(state = initialTreeState, action) {
    let nstate = null;
    let newData = null;
    switch (action.type) {
        case types.FAVOURITE_LOAD_TREE:
            nstate = objectAssign({}, state);
            let {treeData, parentId} = action;
            newData = getNewTreeData(nstate.treeData, parentId, treeData);
            nstate.treeData = newData;
            return nstate;
        case types.FAVOURITE_SELECT_DIR:
            nstate = objectAssign({}, state);
            let {selectedKey} = action;
            objectAssign(nstate, {selectedKey});
            return nstate;
        case types.FAVOURITE_ADD_DIR:
            nstate = objectAssign({}, state);
            ({selectedKey} = action);
            newData = getAddTreeData(nstate.treeData, selectedKey);
            nstate.treeData = newData;
            return nstate;
        case types.FAVOURITE_TOGGLE_OPERATION:
            nstate = objectAssign({}, state);
            let {nodeKey, isover} = action;
            newData = getOverTreeData(nstate.treeData, nodeKey, isover);
            nstate.treeData = newData;
            if (isover) {
                nstate.operatingKey = nodeKey;
            } else {
                nstate.operatingKey = "";
            }
            return nstate;
        case types.FAVOURITE_EDIT_DIR:
            nstate = objectAssign({}, state);
            ({nodeKey} = action);
            newData = getEditTreeData(nstate.treeData, nodeKey);
            nstate.treeData = newData;
            return nstate;
        case types.FAVOURITE_UPDATE_NODE_DATA:
            nstate = objectAssign({}, state);
            let nodeData = action.params;
            ({nodeKey} = action);
            newData = updateNodeData(nstate.treeData, nodeKey, nodeData);
            nstate.treeData = newData;
            return nstate;
        case types.FAVOURITE_ROOT_OVER:
            nstate = objectAssign({}, state);
            ({isover} = action);
            nstate.isover = isover;
            return nstate;
        case types.FAVOURITE_ROOT_SELECTED:
            nstate = objectAssign({}, state);
            let {isselected} = action;
            nstate.selectedKey = "0";
            return nstate;
        case types.FAVOURITE_EXPAND_DIR:
            nstate = objectAssign({}, state);
            let {expandedKeyArr, expanded} = action;
            let expandedKeys = [...nstate.expandedKeys];
            expandedKeyArr.forEach(expandedKey => {
                if (expanded) {   //展开
                    if (expandedKeys.indexOf(expandedKey) == -1) {
                        expandedKeys.push(expandedKey);
                    }
                } else {   //收缩
                    let tempIds = [];
                    if (expandedKeys.indexOf(expandedKey) != -1) {
                        expandedKeys.forEach(eid => {
                            if (eid != expandedKey) {
                                tempIds.push(eid)
                            }
                        })
                    }
                    expandedKeys = [...tempIds];
                }
            });
            nstate.expandedKeys = expandedKeys;
            return nstate;
        case types.FAVOURITE_CLICK_EXPANDED:
            nstate = objectAssign({}, state);
            ({expanded} = action);
            objectAssign(nstate, {clickExpanded: expanded});
            return nstate;
        case types.FAVOURITE_DRAG_NODE:
            nstate = objectAssign({}, state);
            ({treeData} = action);
            nstate.treeData = treeData;
            return nstate;
        default:
            return state;
    }
}