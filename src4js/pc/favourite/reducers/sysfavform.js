import objectAssign from 'object-assign'
import * as types from "../constants/ActionTypes";

const initialTreeState = {
    favid: "",
    pagename: "",
    favouritetype: "",
    favtypename: "",
    dirid: "",
    favouriteObjid: "",
    typelist: []
};


export default function sysfavform(state = initialTreeState, action) {
    let nstate = null;
    switch (action.type) {
        case types.SYSFAV_FORM_LOAD:
            let {favid, pagename, favouritetype, favtypename, dirid, favouriteObjid, typelist} = action.data;
            nstate = objectAssign({}, state, {
                favid,
                pagename,
                favouritetype,
                favtypename,
                dirid,
                favouriteObjid,
                typelist
            });
            return nstate;
        case types.SYSFAV_TYPE_CHANGE:
            nstate = objectAssign({}, state, {favouritetype: action.value});
        case types.SYSFAV_FORM_FIELD_CHANGE:
            nstate = objectAssign({}, state, {...action.values});
            return nstate;
        default:
            return state;

    }
}