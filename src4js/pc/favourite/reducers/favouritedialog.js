import objectAssign from 'object-assign'
import * as types from "../constants/ActionTypes";

const initialTreeState = {
    "1": {visible: false}, /**收藏编辑窗口*/
    "2": {visible: false}  /**收藏夹目录窗口*/
};

export default function favouritedialog(state = initialTreeState, action) {
    let nstate = null;
    switch (action.type) {
        case types.DIALOG_SHOW_OR_HIDE:
            nstate = objectAssign({}, state, action.params);
            return nstate;
        default:
            return state;
    }
}