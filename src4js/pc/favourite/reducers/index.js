import favouritedir from "./favouritedir";
import sysfavourite from "./sysfavourite";
import myfavourite from "./myfavourite";
import sysfavform from "./sysfavform";
import favouritedialog from "./favouritedialog";
import myfavresult from "./myfavresult";
import favouritebrowser from "./favouritebrowser";

export default reducers = {
    favouritedir,
    sysfavourite,
    myfavourite,
    sysfavform,
    favouritedialog,
    myfavresult,
    favouritebrowser
};