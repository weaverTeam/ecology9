import objectAssign from "object-assign";
import * as types from "../constants/ActionTypes";

const initialPageState = {
    allDirName: "",
    addDirTitle: "",
    addBtnName: "",
    addBtnList: [],
    typeList: [],
    moveBtnName: "",
    deleteBtnName: "",
    repeatIconTitle: "",
    editIconTitle: "",
    moveIconTitle: "",
    deleteIconTitle: "",
    loadingmsg: "",
    nodata: "",
    nomoredata: ""
}

export default function myfavourite(state = initialPageState, action) {
    switch (action.type) {
        case types.FAVOURITE_INIT_PAGE:
            return objectAssign({}, state, action.data);
        default:
            return state;
    }
}