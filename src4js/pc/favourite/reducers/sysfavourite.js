import Immutable from 'immutable'
import objectAssign from 'object-assign'
import * as types from "../constants/ActionTypes";
import SearchFavourite from "../util/SearchFavourite";

const initialPageState = {
    data: [], /**收藏列表的数据*/
    favtype: "", /**收藏类型下拉框选中的值*/
    selectedIds: [], /**列表选择的数据*/
    moveids: [], /**移动操作时的id*/
    spinning: false, /**操作过程中的遮罩*/
    tip: ""     /**提示信息*/
}

export default function sysfavourite(state = initialPageState, action) {
    switch (action.type) {
        case types.SYSFAVOURITE_LOAD_PAGE:
            let {count, maxId, total, current} = action.data;
            SearchFavourite.setConditions({count, maxId, total, current});   //缓存分页的信息

            let nstate = objectAssign({}, state);
            /**
             *  将新加载的一页数据与现有的合并
             */
            let data1 = nstate.data;   //原有数据
            let data2 = action.data.data;   //现有数据
            data2.forEach(map => {
                map.isover = false;    //鼠标是否移入
                map.isselected = false;    //鼠标是否选中
                map.isrepeatover = false;   //转发按钮鼠标移入
                map.iseditover = false;     //编辑按钮鼠标移入
                map.ismoveover = false;     //移入按钮鼠标移入
                map.isdeleteover = false;   //删除按钮鼠标移入
                map.islevelover = false;    //重要程度鼠标移入
            });
            let data = [...data1, ...data2];
            nstate.data = data;
            return nstate;
        case types.SYSFAVOURITE_RELOAD_PAGE:
            ({count, maxId, total, current} = action.data);
            SearchFavourite.setConditions({count, maxId, total, current});   //缓存分页的信息
            nstate = objectAssign({}, state, {selectedIds: []});   //初始化已选中的数据
            data2 = action.data.data;   //现有数据
            data2.forEach(map => {
                map.isover = false;    //鼠标是否移入
                map.isselected = false;    //鼠标是否选中
                map.isrepeatover = false;   //转发按钮鼠标移入
                map.iseditover = false;     //编辑按钮鼠标移入
                map.ismoveover = false;     //移入按钮鼠标移入
                map.isdeleteover = false;   //删除按钮鼠标移入
                map.islevelover = false;    //重要程度鼠标移入
            });
            data = [...data2];
            nstate.data = data;
            return nstate;
        case types.SYSFAVOURITE_CHANGE_TYPE:
            let {favtype} = action;
            nstate = objectAssign({}, state);
            objectAssign(nstate, {favtype});
            return nstate;
        case types.SYSFAVOURITE_ITEM_CHECKED:
            let {checked, itemid} = action;
            nstate = objectAssign({}, state);
            let selectedIds = [...nstate.selectedIds];
            let tempIds = [];
            if (!checked) {   //取消选中
                if (selectedIds.indexOf(itemid) != -1) {
                    selectedIds.forEach(sid => {
                        if (sid != itemid) {
                            tempIds.push(sid)
                        }
                    })
                }
                selectedIds = [...tempIds];
            } else {
                if (selectedIds.indexOf(itemid) == -1) {
                    selectedIds.push(itemid);
                }
            }

            nstate.selectedIds = selectedIds;
            return nstate;
        case types.SYSFAVOURITE_ITEM_STATUS:
            let isover = action.isover;
            let itemid2 = action.itemid;
            nstate = objectAssign({}, state);
            data = nstate.data;
            data.forEach(map => {
                if (Immutable.is(map.favid, itemid2)) {   //当前一条记录，是否鼠标移入、移出
                    map.isover = isover;
                }
            });
            return nstate;
        case types.SYSFAVOURITE_ITEM_SELECTED:
            let isselected = action.isselected;
            let itemid3 = action.itemid;
            nstate = objectAssign({}, state);
            data = nstate.data;
            data.forEach(map => {
                if (Immutable.is(map.favid, itemid3)) {   //当前记录，是否鼠标选中
                    map.isselected = isselected;
                } else {
                    map.isselected = false;
                }
            });
            return nstate;
        case types.SYSFAVOURITE_ITEM_BTNOVER:
            let itemid4 = action.itemid;
            let {attr} = action;
            let isover2 = action.isover;
            nstate = objectAssign({}, state);
            data = nstate.data;
            data.forEach(map => {
                if (Immutable.is(map.favid, itemid4)) {
                    map[attr] = isover2;
                }
            });
            return nstate;
        case types.SYSFAVOURITE_ITEM_LEVELOVER:
            let itemid5 = action.itemid;
            let isover3 = action.isover;
            nstate = objectAssign({}, state);
            data = nstate.data;
            data.forEach(map => {
                if (Immutable.is(map.favid, itemid5)) {
                    map.islevelover = isover3;
                }
            })
            return nstate;
        case types.SYSFAVOURITE_SET_MOVE_IDS:
            let {moveids} = action;
            nstate = objectAssign({}, state, {moveids});
            return nstate;
        case types.SYSFAVOURITE_TOGGLE_SPINING:
            let {spinning, tip} = action;
            nstate = objectAssign({}, state, {spinning, tip});
            return nstate;
        default:
            return state;
    }
}