import objectAssign from 'object-assign'
import * as types from "../constants/ActionTypes";

import {getNewTreeData, getAddTreeData} from "../util/TreeUtil";

const initialTreeState = {
    treeData: [],
    selectedKey: "", /**选中的树节点*/
    spinning: false,
    tip: ""
};

/**
 * 加载左侧收藏夹目录树
 * @param state
 * @param action
 * @returns {*}
 */
export default function favouritebrowser(state = initialTreeState, action) {
    let nstate = null;
    switch (action.type) {
        case types.FAVBROWSER_LOAD_TREE:
            nstate = objectAssign({}, state);
            let {treeData, parentId} = action;
            let newData = getNewTreeData(nstate.treeData, parentId, treeData);
            nstate.treeData = newData;
            return nstate;
        case types.FAVBROWSER_SELECT_DIR:
            nstate = objectAssign({}, state);
            let {selectedKey} = action;
            objectAssign(nstate, {selectedKey});
            return nstate;
        case types.FAVBROWSER_ADD_DIR:
            nstate = objectAssign({}, state);
            ({selectedKey} = action);
            newData = getAddTreeData(nstate.treeData, selectedKey);
            nstate.treeData = newData;
            return nstate;
        case types.FAVBROWSER_CLEAR_DATA:
            return initialTreeState;
        case types.FAVBROWSER_SHOW_OR_HIDE_SPINNING:
            let {spinning, tip} = action;
            nstate = objectAssign({}, state, {spinning, tip});
            return nstate;
        default:
            return state;
    }
}