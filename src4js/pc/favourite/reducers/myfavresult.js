import objectAssign from 'object-assign'
import * as types from "../constants/ActionTypes";

const initialTreeState = {
    success: "1",
    msg: ""
};

export default function myfaverror(state = initialTreeState, action) {
    switch (action.type) {
        case types.MYFAV_RESULT_SET:
            let nstate = objectAssign({}, state);
            let {success, msg} = action;
            objectAssign(nstate, {success, msg});
            return nstate;
        default:
            return state;
    }
}