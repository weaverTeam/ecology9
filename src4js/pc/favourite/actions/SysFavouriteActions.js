import {WeaTools} from "ecCom";
import * as urls from "../constants/ServerUrls";
import * as types from "../constants/ActionTypes";
import SearchFavourite from "../util/SearchFavourite"


const {callApi} = WeaTools;
/**
 * 加载收藏夹列表
 * @param params
 * @returns {function(*)}
 */
export function loadFavouriteList(params) {
    return dispatch => {
        let listurl = urls.LOAD_FAVOURITE_LIST;
        callApi(listurl, "POST", params).then(resdata => {
            SearchFavourite.setComplete(true);
            dispatch(setPageData(resdata));
        });
    };
}

/**
 * 重新加载收藏夹列表
 * @param params
 * @returns {function(*)}
 */
export function reloadFavouriteList(params) {
    return dispatch => {
        let listurl = urls.LOAD_FAVOURITE_LIST;
        dispatch(toggleSpinning(true, "数据加载中，请稍候..."));

        callApi(listurl, "POST", params).then(resdata => {
            SearchFavourite.setComplete(true);
            dispatch(setReloadData(resdata));
            dispatch(toggleSpinning(false));
        });
    };
}

/**
 * 设置选中:查询条件中的，收藏类型下拉框选中
 * @param favtype
 * @returns {{type, favtype: *}}
 */
export function setFavouriteType(favtype) {
    return {
        type: types.SYSFAVOURITE_CHANGE_TYPE,
        favtype
    };
}

/**
 *
 * check框选中：收藏列表的check框选中
 * @param itemid
 * @param checked
 * @returns {{type, checked: *, itemid: *}}
 */
export function setCheckedId(itemid, checked) {
    return {
        type: types.SYSFAVOURITE_ITEM_CHECKED,
        checked,
        itemid
    }
}

/**
 * 每一行收藏记录的状态：鼠标移入、移出
 * @param itemid
 * @param isover
 * @returns {{type, isover: *, itemid: *}}
 */
export function setItemStatus(itemid, isover) {
    return {
        type: types.SYSFAVOURITE_ITEM_STATUS,
        isover,
        itemid
    }
}

/**
 * 每一行记录的选中状态设置
 * @param itemid
 * @param isselected
 * @returns {{type, isselected: *, itemid: *}}
 */
export function setItemSelect(itemid, isselected) {
    return {
        type: types.SYSFAVOURITE_ITEM_SELECTED,
        isselected,
        itemid
    }
}

/**
 * 按钮的鼠标移入、移出
 * @param itemid
 * @param attr
 * @param isover
 * @returns {{type, isover: *, itemid: *}}
 */
export function setBtnOver(itemid, attr, isover) {
    return {
        type: types.SYSFAVOURITE_ITEM_BTNOVER,
        attr,
        isover,
        itemid
    }
}

/**
 * 重要程度按钮鼠标移入、移出
 * @param itemid
 * @param isover
 * @returns {{type, isover: *, itemid: *}}
 */
export function setLevelOver(itemid, isover) {
    return {
        type: types.SYSFAVOURITE_ITEM_LEVELOVER,
        isover,
        itemid
    }
}

/**
 * 修改重要程度
 * @param favid
 * @param importlevel
 * @returns {function(*)}
 */
export function changeLevel(favid, importlevel) {
    return dispatch => {
        let levelurl = urls.EDIT_IMPORTANT_LEVEL;
        let params = {favid, importlevel};

        dispatch(toggleSpinning(true, "数据处理中，请稍候..."));

        callApi(levelurl, "POST", params).then(resdata => {
            let {success} = resdata;
            if (success == "1") {
                let isinit = false;
                let isreload = true;
                //刷新收藏夹列表，之所以刷新，是因为列表的排序，跟重要程序相关
                let searchFavourite = new SearchFavourite(dispatch);
                let params = {isinit, isreload};
                searchFavourite.load(params);
            }
        });
    };
}

/**
 * private function,设置异步查询的一页数据
 * @param data
 * @returns {{type, data: *}}
 */
function setPageData(data) {
    return {
        type: types.SYSFAVOURITE_LOAD_PAGE,
        data
    }
}


/**
 * 删除收藏
 * @param favid
 * @returns {function(*=)}
 */
export function deleteFavourite(favid) {
    return dispatch => {
        let favurl = urls.DELETE_FAVOURITE;
        let params = {favid};

        dispatch(toggleSpinning(true, "数据处理中，请稍候..."));

        callApi(favurl, "POST", params).then(resdata => {
            let {success} = resdata;
            if (success == "1") {
                let isinit = false;
                let isreload = true;
                //刷新收藏夹列表
                let searchFavourite = new SearchFavourite(dispatch);
                let params = {isinit, isreload};
                searchFavourite.load(params);
            }
        });
    }
}

export function setMoveIds(moveids) {
    return {
        type: types.SYSFAVOURITE_SET_MOVE_IDS,
        moveids
    }
}

/**
 * 移动收藏条目到另一目录
 * @param favids
 * @param dirid
 * @returns {function(*=)}
 */
export function moveToAnotherDir(favids, dirid) {
    let moveurl = urls.MOVE_FAVOURITES;
    let params = {favids, dirid};
    return dispatch => {
        dispatch(toggleSpinning(true, "数据处理中，请稍候..."));
        callApi(moveurl, "POST", params).then(resdata => {
            let {success} = resdata;
            if (success == "1") {
                //关闭收藏目录的弹出窗口
                let params = {"2": {visible: false}};
                dispatch({
                    type: types.DIALOG_SHOW_OR_HIDE,
                    params
                });

                //清空窗口的数据
                dispatch({
                    type: types.FAVBROWSER_CLEAR_DATA
                });

                //刷新收藏夹列表
                let isinit = false;
                let isreload = true;
                let searchFavourite = new SearchFavourite(dispatch);
                let params2 = {isinit, isreload};
                searchFavourite.load(params2);
            }
        });
    }
}

/**
 * 添加文档、流程、项目、客户的收藏，收藏的是具体的id
 * @param ids
 * @param names
 * @param favouritetype
 */
export function addSysFavourites(ids, names, favouritetype) {
    return (dispatch, getState) => {
        let allState = getState();
        let dirid = allState.favouritedir.selectedKey;
        if (dirid == "0") {   //这是选择了根目录，默认将收藏的内容，放到“我的收藏”
            dirid = "-1";
        }
        let importantlevel = 1;    //紧急程度：正常

        let favurl = urls.ADD_FAVOURITES;
        let params = {ids, names, dirid, importantlevel, favouritetype};

        dispatch(toggleSpinning(true, "数据处理中，请稍候..."));

        callApi(favurl, "POST", params).then(resdata => {
            //刷新收藏夹列表
            let isinit = true;
            let isreload = false;
            let searchFavourite = new SearchFavourite(dispatch);
            let params2 = {isinit, isreload};
            searchFavourite.load(params2);
        });
    }
}

/**
 * 显示加载中的遮罩
 * @param spinning
 * @param tip
 * @returns {{type, spinning: *, tip: string}}
 */
export const toggleSpinning = (spinning, tip = "") => {
    return {
        type: types.SYSFAVOURITE_TOGGLE_SPINING,
        spinning,
        tip
    }
}


/**
 * private function,设置异步查询，重载的数据
 * @param data
 * @returns {{type, data: *}}
 */
function setReloadData(data) {
    return {
        type: types.SYSFAVOURITE_RELOAD_PAGE,
        data
    }
}
