import * as types from "../constants/ActionTypes";


export const setResult = (success, msg = "") => {
    return {
        type: types.MYFAV_RESULT_SET,
        success,
        msg
    }
}