import * as types from "../constants/ActionTypes";

/**
 * 打开窗口
 * @param params
 */
export const showDialog = (dialogtype) => {
    return {
        type: types.DIALOG_SHOW_OR_HIDE,
        params: {[dialogtype]: {visible: true}}
    }
};

/**
 * 关闭窗口
 */
export const closeDialog = (dialogtype) => {
    return {
        type: types.DIALOG_SHOW_OR_HIDE,
        params: {[dialogtype]: {visible: false}}
    }
}