import {WeaTools} from 'ecCom';

import * as types from "../constants/ActionTypes";
import * as urls from "../constants/ServerUrls";

const {callApi} = WeaTools;

/**
 * 初始化收藏页面
 * @returns {function(*)}
 */
export function initPage() {
    return dispatch => {
        let initurl = urls.PAGE_INIT_URL;
        callApi(initurl, "POST", {}).then(resdata => {
            dispatch(init(resdata));

            let typeList = [...resdata.typeList];
            let value = "";
            for (let i = 0; i < typeList.length; i++) {
                let typeMap = typeList[i];
                let {selected, id} = typeMap;
                if (selected) {
                    value = id;
                    break;
                }
            }

            /**
             * 设置收藏类型默认选中的下拉选项
             */
            dispatch({
                type: types.SYSFAVOURITE_CHANGE_TYPE,
                favtype: value
            });
        });
    }
}

function init(data) {
    return {
        type: types.FAVOURITE_INIT_PAGE,
        data: data
    };
}