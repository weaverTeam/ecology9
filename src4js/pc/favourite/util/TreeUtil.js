import objectAssign from 'object-assign'
/**
 * 将加载的节点展开数据，与现有的数据组合，生成新的树节点的数据
 * @param treeData，现有数据
 * @param currentKey,当前加载数据的父节点
 * @param childData，当前加载的数据
 */
export function getNewTreeData(treeData, currentKey, childData) {
    const loop = data => {
        if (currentKey != "0") {
            data.map(item => {
                let {favid, child, children} = item;
                if (favid == currentKey) {
                    item.children = childData;
                } else {
                    if (child == "1" && !!children) {
                        loop(children);
                    }
                }
            });
        }
    }

    let ndata = [];
    loop(treeData, ndata);
    if (currentKey != "0") {
        ndata = [...treeData];
    } else {
        ndata = [...childData];
    }
    return ndata;
}

/**
 * 新增节点时的数据处理
 * @param treeData
 * @param selectedKey
 * @returns {[*]}
 */
export function getAddTreeData(treeData, selectedKey) {
    const loop = (data, nodeData) => {
        if (!selectedKey || selectedKey == "0") {     //没有选中任何节点，默认认为选中的是根节点，新增的目录在第一级
            data.push(nodeData);
        } else {
            data.map(item => {
                let {favid, child, children} = item;
                if (favid == selectedKey) {
                    if (!!item.children) {
                        item.children = [...item.children, nodeData];
                    } else {
                        item.child = "1";
                        item.children = [nodeData];
                    }
                } else {
                    if (child == "1" && !!children) {
                        loop(children, nodeData);
                    }
                }
            });
        }
    }

    let addNode = {favid: "-9", child: "0", isinput: true, parentid: selectedKey};
    loop(treeData, addNode);
    return [...treeData];
}

/**
 * 编辑节点时的数据处理
 * @param treeData
 * @param nodeKey
 * @returns {[*]}
 */
export function getEditTreeData(treeData, nodeKey) {
    const loop = (data, key) => {
        data.map(item => {
            let {favid, child, children} = item;
            if (favid == key) {
                item.isinput = true;
            } else {
                if (child == "1" && !!children) {
                    loop(children, key);
                }
            }
        });
    }

    loop(treeData, nodeKey);
    return [...treeData];
}

/**
 * 更新节点信息
 * @param treeData
 * @param nodeKey
 * @param nodeData
 * @returns {[*]}
 */
export function updateNodeData(treeData, nodeKey, nodeData) {
    const loop = (data, key, node) => {
        data.map(item => {
            let {favid, child, children} = item;
            if (favid == nodeKey) {
                objectAssign(item, {...node});  //更新节点的信息
            } else {
                if (child == "1" && !!children) {
                    loop(children, key, node);
                }
            }
        });
    }

    loop(treeData, nodeKey, nodeData);
    return [...treeData];
}


/**
 * 鼠标移动到节点上，显示 编辑、删除 操作按钮
 * @param treeData
 * @param selectedKey
 * @returns {[*]}
 */
export function getOverTreeData(treeData, nodeKey, isover) {
    const loop = (data, key) => {
        data.map(item => {
            let {favid, child, children} = item;
            if (favid == nodeKey) {
                item.isover = isover;
            } else {
                if (child == "1" && !!children) {
                    loop(children, key);
                }
            }
        });
    }

    loop(treeData, nodeKey);
    return [...treeData];
}

/**
 * 根据key，获取节点信息
 * @param treeData
 * @param nodeKey
 * @returns {{}}
 */
export function getNodeData(treeData, nodeKey) {
    const loop = (data, key, ndata) => {
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let {favid, child, children} = item;
            if (favid == key) {
                objectAssign(ndata, {...item});
                return;
            } else {
                if (child == "1" && !!children) {
                    loop(children, key, ndata);
                }
            }
        }
    }

    let nodeData = {};
    loop(treeData, nodeKey, nodeData);
    return nodeData;
}

/**
 * 处理节点拖动的数据
 * @param treeData
 * @param dragInfo
 * @returns {{treeData: *, list, node, dragParentId, dropParentId, islast}}
 */
export function handleDragNodeData(treeData, dragInfo) {
    /**
     * 如果拖动的节点，拖动到其他父节点下，原父节点就要移除这个节点
     * @param data
     * @param dragKey
     */
    const deleteFromParent = (data, dragKey) => {
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let {favid, child, children, parentid} = item;
            if (favid == dragKey) {
                let dragNode = data.splice(i, 1);   //删除这个节点
                return dragNode;
            } else {
                if (child == "1" && !!children) {
                    deleteFromParent(children, dragKey);
                }
            }
        }
    }

    /**
     * 节点放置在两个节点之间的间隙
     * @param data
     * @param dragNode
     * @param dropKey
     * @param dropPos
     */
    const handleDropToGap = (data, dragNode, dropKey, dropPos, handleResult) => {
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let {favid, child, children, parentid} = item;
            if (favid == dropKey) {   //拖动的节点，跟放置的节点，位于同一个父节点下
                let newNode = jQuery.extend({}, dragNode);
                newNode.parentid = parentid;
                if (dropPos < 0) {   //放置在第一个节点前，这个值为-1
                    dropPos = 0;
                }
                data.splice(dropPos, 0, newNode);
                objectAssign(handleResult, {list: data, node: newNode, dropParentId: parentid, islast: false});
                return;
            } else {
                if (child == "1" && !!children) {
                    handleDropToGap(children, dragNode, dropKey, dropPos, handleResult);
                }
            }
        }
    }

    /**
     * 节点放置在节点之上
     * @param data
     * @param dragNode
     * @param dropKey
     */
    const handleDropToNode = (data, dragNode, dropKey, handleResult) => {
        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let {favid, child, children, parentid} = item;
            if (favid == dropKey) {   //拖动的节点，是待放置的节点的子节点
                let newNode = jQuery.extend({}, dragNode);
                newNode.parentid = favid;
                let nodeList = [newNode];
                if (child == "1") {  //放置在末尾
                    if (!!children) {
                        children.push(newNode);
                    } else {
                        item.children = nodeList;
                    }
                } else {   //是唯一节点
                    item.child = "1";
                    item.children = nodeList;
                }
                objectAssign(handleResult, {list: nodeList, node: newNode, dropParentId: favid, islast: true});
                return;
            } else {
                if (child == "1" && !!children) {
                    handleDropToNode(children, dragNode, dropKey, handleResult);
                }
            }
        }
    }

    let {dragKey, dropKey, dropPos, dropToGap} = dragInfo;
    let result = {};
    let dragNode = getNodeData(treeData, dragKey);
    let dragParentId = dragNode.parentid;

    deleteFromParent(treeData, dragKey);   //先从原本的位置删除，然后再插入到新的位置

    if (dropToGap) {
        handleDropToGap(treeData, dragNode, dropKey, dropPos, result);
    } else {
        handleDropToNode(treeData, dragNode, dropKey, result);
    }
    let {list, node, islast, dropParentId} = result;
    return {treeData, list, node, dragParentId, dropParentId, islast};
}

export function stopEvent() {
    var event = window.event || arguments.callee.caller.arguments[0]
    if (!event) {
        return false;
    }
    if (event.stopPropagation) {
        event.stopPropagation();
    } else if (window.event) {
        window.event.cancelBubble = true;
    }
    return false;
}