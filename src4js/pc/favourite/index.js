import Route, {IndexRoute} from 'react-router/lib/Route';

import Home from "./containers/Home";
import  MyFavourite from "./containers/MyFavourite";
import FavouriteBrowser from "./containers/FavouriteBrowser";
import  * as SysFavouriteActions from "./actions/SysFavouriteActions";
import  * as FavouriteDirActions from "./actions/FavouriteDirActions";
import  * as MyFavouriteActions from "./actions/MyFavouriteActions";

import reducers from "./reducers/index";

const FavouriteRoute = (
    <Route path="favourite" breadcrumbName="收藏夹" component={Home}>
        <Route path="index" component={MyFavourite}></Route>
        <Route path="browser" component={FavouriteBrowser}></Route>
    </Route>
)

export default {
    route: FavouriteRoute,
    reducers,
    action: {
        SysFavouriteActions,
        FavouriteDirActions,
        MyFavouriteActions,
    }
};