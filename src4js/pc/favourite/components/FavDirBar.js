import React, {Component} from "react";
import {Modal} from "antd";

import {addTreeNode, setRootOver, setRootClick, expandTreeNode} from "../actions/FavouriteDirActions";
import {stopEvent} from "../util/TreeUtil";
import SearchFavourite from "../util/SearchFavourite";

export default class FavDirBar extends Component {
    /**
     * 新建目录事件
     * @param evt
     */
    addTreeNode = (evt) => {
        let {selectedKey, dispatch} = this.props;
        if (selectedKey == "-1") {    //我的收藏，不允许添加下级目录
            Modal.error({content: "当前目录不允许添加下级目录！"});
        } else {
            dispatch(addTreeNode(selectedKey));
            dispatch(expandTreeNode([selectedKey], true));  //默认展开节点
        }
    }

    /**
     * 全部目录，鼠标移入、移出、点击
     */
    handleRootNodeOver = (evt) => {
        let {dispatch} = this.props;
        dispatch(setRootOver(true));
    }

    handleRootNodeOut = (evt) => {
        let {dispatch} = this.props;
        dispatch(setRootOver(false));
    }

    handleRootNodeClick = (evt) => {
        let {dispatch} = this.props;
        dispatch(setRootClick(true));

        /**
         * 选中树根节点后，刷新收藏的列表
         */
        let isinit = true;
        let isreload = false;
        SearchFavourite.setConditions({dirId: "0"});
        let searchFavourite = new SearchFavourite(dispatch);
        searchFavourite.load({isinit, isreload});
    }

    componentDidMount() {
        let that = this;
        jQuery(".fav-left-bar-btn").on("click", function (e) {
            stopEvent();
            that.addTreeNode(e);
        });
    }

    render() {
        let {allDirName, addDirTitle, isover, selectedKey} = this.props;
        let cls = "fav-left-bar";
        if (isover) {
            cls += " fav-left-bar-over";
        }
        if (selectedKey == "0") {
            cls += " fav-left-bar-click";
        }

        return (
            <div className={cls} onClick={this.handleRootNodeClick} onMouseOver={this.handleRootNodeOver}
                 onMouseOut={this.handleRootNodeOut}>
                <div className="fav-left-bar-title fav-bar-common">
                    <span className="fav-left-bar-title-icon"></span>
                    <span>{allDirName}</span>
                </div>
                <div className="fav-left-bar-btn fav-bar-common" title={addDirTitle}></div>
            </div>
        );
    }
}
