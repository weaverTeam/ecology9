import React, {Component} from "react";

export default class ItemDate extends Component {
    render() {
        let {adddate} = this.props;
        if (!!adddate) {
            let arr = adddate.split(" ");
            if (arr.length > 0) {
                adddate = arr[0];
            }
        }

        return (
            <div className="common sysfav-list-item-info-time">{adddate}</div>
        );
    }
}