import React from "react";
import {Button, Menu, Dropdown, Icon, Select, Modal} from "antd";
import {WeaInputSearch} from "ecCom";
import SearchFavourite from "../util/SearchFavourite";
import {showDialog} from "../actions/FavouriteDialogActions";
import {loadTree, showOrHideSpinning} from "../actions/FavouriteBrowserActions";
import {deleteFavourite, setFavouriteType, setMoveIds, addSysFavourites} from "../actions/SysFavouriteActions";
import {WeaBrowser} from 'ecCom';
import BrowserDict from "../constants/BrowserDict";

const Option = Select.Option;
const confirm = Modal.confirm;
const listBtns = BrowserDict.listBtns;

class SysFavouriteBar extends React.Component {

    /**
     * 下拉按钮的点击事件
     * @param evt
     */
    handleMenuClick = evt => {
        this.setState({
            favType: evt.key
        });
    }

    /**
     * 下拉框点击选项的事件
     * @param value
     */
    handleTypeChange = value => {
        let {dispatch} = this.props;
        dispatch(setFavouriteType(value));
    }

    /**
     * 搜索的事件
     * @param value
     */
    handleOnSearch = value => {
        let {dispatch, favtype} = this.props;
        SearchFavourite.setConditions({pagename: value, favtype});
        let isinit = true;
        let isreload = false;
        let searchFavourite = new SearchFavourite(dispatch);
        let params = {isinit, isreload};
        searchFavourite.load(params);
    }

    /**
     * 删除操作
     */
    handleOnDelete = () => {
        let {dispatch, selectedIds} = this.props;
        confirm({
            content: '删除后不可恢复，您确认删除选中的记录吗？',
            onOk() {
                dispatch(deleteFavourite(selectedIds.join()));
            },
            onCancel() {
            }
        });
    }

    /**
     * 移动操作
     */
    handleOnMove = () => {
        let {dispatch, selectedIds} = this.props;

        //设置需要移动的数据
        dispatch(setMoveIds(selectedIds));
        //显示窗口
        dispatch(showDialog("2"));
        //显示加载中的遮罩
        dispatch(showOrHideSpinning(true, "数据加载中，请稍候..."));
        //加载目录树的数据
        dispatch(loadTree("0"));
    }

    handleBrowserChange = (ids, names, datas) => {
        let {favType} = this.state;
        let {dispatch} = this.props;
        console.log("favType:", favType);
        console.log("ids:", ids);
        console.log("names:", names);
        console.log("datas:", datas);
        let tempids = [];
        let tempnames = [];
        datas.forEach(item => {
            let {id, name} = item;
            tempids.push(id);
            tempnames.push(name);
        });

        dispatch(addSysFavourites(tempids, tempnames, favType));
    }


    /**
     * 添加按钮的下拉菜单
     * @returns {XML}
     */
    renderMenu = () => {
        let addBtnList = [...this.props.addBtnList];
        let menuItems = addBtnList.map(item => {
                let browserParams = listBtns[item.id];
                return (
                    <Menu.Item key={item.id} className="sysfav-bar-item">
                        <WeaBrowser {...browserParams} onChange={this.handleBrowserChange}
                                    className="sysfav-bar-item-browser">
                            {item.name}
                        </WeaBrowser>
                    </Menu.Item>
                );
            }
        );
        return (
            <Menu onClick={this.handleMenuClick}>{menuItems}</Menu>
        );
    }

    /**
     *  收藏类型的下拉框
     * @returns {*}
     */
    renderOpts = () => {
        let typeList = [...this.props.typeList];
        let options = typeList.map(item => {
            return <Option key={item.id} value={item.id}>{item.name}</Option>;
        });
        return options;
    }

    /**
     * 移动、删除按钮
     * @returns {Array}
     */
    renderBtns = () => {
        let {selectedIds, moveBtnName, deleteBtnName} = this.props;
        let btns = [];
        if (selectedIds.length > 0) {
            btns.push(<Button type="primary" onClick={this.handleOnMove}>{moveBtnName}</Button>);
            btns.push(<Button type="primary" style={{"margin-left": "15px"}}
                              onClick={this.handleOnDelete}>{deleteBtnName}</Button>);
        } else {
            btns.push(<Button type="primary" disabled="disabled">{moveBtnName}</Button>);
            btns.push(<Button type="primary" disabled="disabled"
                              style={{"margin-left": "15px"}}>{deleteBtnName}</Button>);
        }
        return btns;
    }

    render() {
        const menu = this.renderMenu();
        const opts = this.renderOpts();
        const btns = this.renderBtns();
        const {favtype} = this.props;

        return (
            <div className="sysfav-top-bar">
                <div className="sysfav-top-bar-common sysfav-top-bar-left">
                    <Dropdown overlay={menu}>
                        <Button>
                            <Icon type="plus"/>{this.props.addBtnName}
                        </Button>
                    </Dropdown>

                    <Select value={favtype} style={{"min-width": "100px", "margin-left": "50px"}}
                            onChange={this.handleTypeChange}>
                        {opts}
                    </Select>

                    <WeaInputSearch onSearch={this.handleOnSearch}/>
                </div>

                <div className="sysfav-top-bar-common sysfav-top-bar-right">
                    {btns}
                </div>
            </div>
        );
    }
}

export default SysFavouriteBar;