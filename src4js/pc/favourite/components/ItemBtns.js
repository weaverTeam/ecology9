import React, {Component} from "react";
import {Button, Modal} from "antd";
import {setBtnOver, deleteFavourite, setMoveIds} from "../actions/SysFavouriteActions";
import {loadForm} from "../actions/SysFavFormActions";
import {showDialog} from "../actions/FavouriteDialogActions";
import {loadTree, showOrHideSpinning} from "../actions/FavouriteBrowserActions";
const confirm = Modal.confirm;

export default class ItemBtns extends Component {
    handleMouseOver = (itemid, attr) => {
        let {dispatch} = this.props;
        dispatch(setBtnOver(itemid, attr, true));
    }

    handleMouseOut = (itemid, attr) => {
        let {dispatch} = this.props;
        dispatch(setBtnOver(itemid, attr, false));
    }

    /**
     * 转发按钮的移入移出
     */
    onRepeatOver = () => {
        let itemid = this.props.favid;
        let attr = "isrepeatover";
        this.handleMouseOver(itemid, attr);
    }
    onRepeatOut = () => {
        let itemid = this.props.favid;
        let attr = "isrepeatover";
        this.handleMouseOut(itemid, attr);
    }


    /**
     * 编辑按钮的移入、移出、点击
     */
    onEidtOver = () => {
        let itemid = this.props.favid;
        let attr = "iseditover";
        this.handleMouseOver(itemid, attr);
    }
    onEditOut = () => {
        let itemid = this.props.favid;
        let attr = "iseditover";
        this.handleMouseOut(itemid, attr);
    }
    onEditClick = () => {
        let dataid = this.props.favid;
        let {dispatch} = this.props;
        dispatch(loadForm(dataid));    //加载编辑窗口的数据
    }

    /**
     * 移动按钮的移入、移出、点击
     */
    onMoveOver = () => {
        let itemid = this.props.favid;
        let attr = "ismoveover";
        this.handleMouseOver(itemid, attr);
    }
    onMoveOut = () => {
        let itemid = this.props.favid;
        let attr = "ismoveover";
        this.handleMouseOut(itemid, attr);
    }
    onMoveClick = () => {
        let dataid = this.props.favid;
        let {dispatch} = this.props;

        //设置需要移动的数据
        dispatch(setMoveIds([dataid]));
        //显示窗口
        dispatch(showDialog("2"));
        //显示加载中的遮罩
        dispatch(showOrHideSpinning(true, "数据加载中，请稍候..."));
        //加载目录树的数据
        dispatch(loadTree("0"));
    }

    /**
     * 删除按钮的移入、移出、点击
     */
    onDeleteOver = () => {
        let itemid = this.props.favid;
        let attr = "isdeleteover";
        this.handleMouseOver(itemid, attr);
    }
    onDeleteOut = () => {
        let itemid = this.props.favid;
        let attr = "isdeleteover";
        this.handleMouseOut(itemid, attr);
    }
    onDeleteClick = () => {
        let itemid = this.props.favid;
        let {dispatch} = this.props;
        confirm({
            content: '删除后不可恢复，您确认删除该条记录吗？',
            onOk() {
                dispatch(deleteFavourite(itemid));
            },
            onCancel() {
            }
        });
    }


    renderBtns = () => {
        let btns = [];
        let {
            favouritetype, repeatIconTitle, editIconTitle,
            moveIconTitle, deleteIconTitle,
            isrepeatover, iseditover, ismoveover, isdeleteover
        } = this.props;

        let repeatCls = "";
        let editCls = "";
        let moveCls = "";
        let deleteCls = "";
        if (isrepeatover) {
            repeatCls = "over";
        }
        if (iseditover) {
            editCls = "over";
        }
        if (ismoveover) {
            moveCls = "over";
        }
        if (isdeleteover) {
            deleteCls = "over";
        }

        if (favouritetype == 6) {   //消息类型，不可编辑
            btns.push(<span className='move' title={moveIconTitle} onMouseOver={this.onMoveOver}
                            onMouseOut={this.onMoveOut} onClick={this.onMoveClick}>
                        <a href='javascript:void(0)' className={moveCls}></a></span>);    //移动
            btns.push(<span className='delete' title={deleteIconTitle} onMouseOver={this.onDeleteOver}
                            onMouseOut={this.onDeleteOut} onClick={this.onDeleteClick}>
                        <a href='javascript:void(0)' className={deleteCls}></a></span>);   //删除
        } else {
            btns.push(<span className='edit' title={editIconTitle} onMouseOver={this.onEidtOver}
                            onMouseOut={this.onEditOut} onClick={this.onEditClick}>
                        <a href='javascript:void(0)' className={editCls}></a></span>);    //编辑
            btns.push(<span className='move' title={moveIconTitle} onMouseOver={this.onMoveOver}
                            onMouseOut={this.onMoveOut} onClick={this.onMoveClick}>
                       <a href='javascript:void(0)' className={moveCls}></a></span>);    //移动
            btns.push(<span className='delete' title={deleteIconTitle} onMouseOver={this.onDeleteOver}
                            onMouseOut={this.onDeleteOut} onClick={this.onDeleteClick}>
                        <a href='javascript:void(0)' className={deleteCls}></a></span>);   //删除
        }
        return btns;
    }


    render() {
        const btns = this.renderBtns();

        let {isover} = this.props;
        let cls = "";
        if (!isover) {   //isover为false时，鼠标移出
            cls = "hidebtns";
        }

        return (
            <div className={`common sysfav-list-item-info-btns ${cls}`}>{btns}</div>
        );
    }
}