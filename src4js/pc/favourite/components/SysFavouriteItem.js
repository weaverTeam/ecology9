import React, {Component, PropTypes} from "react";
import {Checkbox} from 'antd';

import ItemLevel from "./ItemLevel";
import ItemDate from "./ItemDate";
import ItemUser from "./ItemUser";
import ItemBtns from "./ItemBtns";
import ItemIcon from "./ItemIcon";
import ItemContent from "./ItemContent";

import {setCheckedId, setItemStatus, setItemSelect} from "../actions/SysFavouriteActions";

class SysFavouriteItem extends Component {
    /**
     * check框状态的变更
     * @param evt
     */
    handleCheckBoxChange = evt => {
        let {checked, itemid} = evt.target;
        let {dispatch} = this.props;
        dispatch(setCheckedId(itemid, checked));
    }

    /**
     * 收藏列表每条记录的鼠标移出
     * @param itemid
     */
    handleItemMouseOut = () => {
        let {dispatch, isover} = this.props;
        let itemid = this.props.favid;
        isover = false;
        dispatch(setItemStatus(itemid, isover));
    }

    /**
     * 收藏列表每条记录的鼠标移入
     * @param itemid
     */
    handleItemMouseOver = () => {
        let {dispatch, isover} = this.props;
        let itemid = this.props.favid;
        isover = true;
        dispatch(setItemStatus(itemid, isover));
    }


    /**
     * 收藏列表每条记录的鼠标选中
     * @param itemid
     */
    handleItemClick = () => {
        let {dispatch} = this.props;
        let itemid = this.props.favid;
        let isselected = true;
        dispatch(setItemSelect(itemid, isselected));
    }


    renderComponent() {
        let {favid, isselected, isover, selectedIds} = this.props;
        let cls = "";
        if (isover) {    //鼠标移入
            cls += " over";
        }

        if (isselected) {
            cls += " click";
        }

        let checked = false;
        if (selectedIds.indexOf(favid) != -1) {
            checked = true;
        }

        return (
            <div className={`sysfav-list-item ${cls}`}
                 onMouseOver={this.handleItemMouseOver} onMouseOut={this.handleItemMouseOut}
                 onClick={this.handleItemClick}>
                <div className="sysfav-list-item-info">
                    <Checkbox className="common"
                              onChange={this.handleCheckBoxChange}
                              itemid={this.props.favid}
                              checked={checked}/>
                    <ItemUser {...this.props}/>
                    <ItemDate {...this.props}/>
                    <ItemLevel {...this.props}/>
                    <ItemBtns {...this.props}/>
                </div>
                <div className="sysfav-list-item-content">
                    <ItemIcon {...this.props}/>
                    <ItemContent {...this.props}/>
                </div>
            </div>
        );
    }

    render() {
        return this.renderComponent();
    }
}

export default SysFavouriteItem;