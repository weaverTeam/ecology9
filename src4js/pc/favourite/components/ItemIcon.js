import React, {Component} from "react";

export default class ItemIcon extends Component {
    getIcon = () => {
        let iconCls = "";
        let iconBgColor = "";
        let {favouritetype, favTypeName} = this.props;
        switch (favouritetype) {
            /**文档*/
            case 1:
                iconCls = "icon-coms-doc";
                iconBgColor = "#f14a2d";
                break;
            /**流程*/
            case 2:
                iconCls = "icon-coms-workflow";
                iconBgColor = "#0079de";
                break;
            /**项目*/
            case 3:
                iconCls = "icon-coms-project";
                iconBgColor = "#217346";
                break;
            /**客户*/
            case 4:
                iconCls = "icon-coms-crm";
                iconBgColor = "#96358a";
                break;
            /**其他*/
            case 5:
                iconCls = "icon-coms-Collection";
                iconBgColor = "#788f9d";
                break;
            /**消息*/
            case 6:
                iconCls = "icon-coms-message";
                iconBgColor = "#0079de";
                break;
            default:
                break;
        }

        return (
            <div className="sysfav-list-item-circle-base" style={{"background": `${iconBgColor}`}}
                 title={favTypeName}>
                <i className={iconCls}/>
            </div>
        );
    }

    render() {
        const iconHtml = this.getIcon();
        return (
            <div className="sysfav-list-item-content-icon">
                {iconHtml}
            </div>
        );
    }
}