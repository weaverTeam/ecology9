import React, {Component} from "react";

export default class ItemContent extends Component {
    getContent = () => {
        let {favname, favouritetype, favouriteObjid, extra, contentHtml, content, url} = this.props;
        let itemContent = favname;
        /***
         * 1：文档
         * 2：流程
         * 3：项目
         * 4：客户
         * 5：其他
         */
        if (favouritetype == 1 || favouritetype == 2 || favouritetype == 3 || favouritetype == 4) {
            /**
             *  原本这里应该是采用E9的方式打开每个页面的，
             *  但是因为目前并不是每个模块都完成了，因此先采用E8的方式打开
             */
            if (favouriteObjid > 0) {
                itemContent = <a href={`javascript:openFullWindowHaveBar('${url}')`} title={favname}>{favname}</a>;
            } else {
                if (!!url) {
                    itemContent = <a href={`javascript:openFullWindowHaveBar('${url}')`} title={favname}>{favname}</a>;
                }
            }
        } else if (favouritetype == 5) {   //其他
            if (!!url) {
                itemContent = <a href={`javascript:openFullWindowHaveBar('${url}')`} title={favname}>{favname}</a>;
            }
        } else if (favouritetype == 6) {
            let {msgInfo} = this.props;
            if (!!msgInfo) {
                let {msgtype, msgname, content} = msgInfo;
                let msgurl = msgInfo.url;
                let msgContent = msgInfo.content;
                let msgContentHtml = msgInfo.contentHtml;
                if (msgtype == "1" || msgtype == "3") {  //纯文本、公告
                    itemContent = msgname;
                } else if (msgtype == "2") {     //图片
                    itemContent = <img title={favname} src={`data:image/jpg;base64,${msgname}`}
                                       onClick={event => openFullWindowHaveBar(url)}/>;
                } else if (msgtype == "4" || msgtype == "6" || msgtype == "7" || msgtype == "8" || msgtype == "9") {
                    ////附件、流程、文档、任务、客户
                    itemContent = <a href={`javascript:openFullWindowHaveBar('${url}')`}>{msgname}</a>;
                } else if (msgtype == "5") {   //名片
                    let {hrmUserName, hrmUserIcon, hrmUserId, hrmJobTitle} = msgInfo;
                    itemContent = (
                        <div className="chatCardInfo">
                            <div className="left" onClick={event => viewHrmCard(hrmUserId)}>
                                <img src={hrmUserIcon} class="head35 userimage"/>
                            </div>
                            <div className="left" style="margin-left:10px;">
                                <div className="hrmName" style="line-height: 19px;cursor:pointer;"
                                     onClick={event => viewHrmCard(hrmUserId)}>
                                    {hrmUserName}
                                </div>
                                <div className="clear"></div>
                                <div style="color:#999;line-height: 12px;">{hrmJobTitle}</div>
                            </div>
                            <div className="clear"></div>
                        </div>
                    );
                } else if (msgtype == "10") {  //图文
                    if (!!extra) {
                        let {imageurl, image} = extra;
                        let imgurl = extra.url;
                        if (!imgurl) {
                            imageurl = "data:image/jpg;base64," + image;
                        }

                        itemContent = (
                            <div className="textImgMsgItem" title="打开分享" style="display: block;"
                                 onClick={event => openFullWindowHaveBar(imgurl, '_blank')}>
                                <div className="content">
                                    <div className="imgcot"><img src={imageurl}
                                                                 style="max-width:45px;max-height:45px;"/>
                                    </div>
                                    <div className="textcot ellipsis"
                                         style="padding: 9px;cursor: pointer;">{content}</div>
                                </div>
                            </div>
                        );
                    }
                } else if (msgtype == "11") {  //位置
                    if (!!extra) {
                        let {latitude, longitude, poi} = extra;
                        itemContent = (
                            <div>
                                <div className="locationdiv"
                                     onClick={event => top.ChatUtil.showIMLocation("'" + latitude + "'", "'" + longitude + "'", "'" + poi + "'")}>
                                    <img className="locationimg" src={`data:image/jpg;base64,${msgContentHtml}`}/>
                                    <div className="locationpoi">{poi}</div>
                                </div>
                            </div>
                        );
                    }
                } else if (msgtype == "12") {   //语音
                    if (!!extra) {
                        let {duration} = extra;
                        itemContent = (
                            <div>
                                <div className="chatVoice"
                                     onClick={ event => top.ChatUtil.playVoice(this, "'" + msgContentHtml + "'", true, duration)}>
                                    <img src="/social/images/chat_voice_l_wev8.png"/>
                                </div>
                                <div className="chatVoiceTime">{duration}</div>
                            </div>
                        );
                    }
                } else if (msgtype == "13") {  //必达【盯办】
                    itemContent = <div style="cursor:pointer;">{msgContent}</div>;
                }
            }
        }

        return itemContent;
    }

    render() {
        let content = this.getContent();
        return (
            <div className="sysfav-list-item-content-text">{content}</div>
        );
    }
}