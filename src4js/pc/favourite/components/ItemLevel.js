import React, {Component} from "react";
import {Menu, Dropdown} from 'antd';
import  {setLevelOver, changeLevel} from "../actions/SysFavouriteActions"

export default class ItemLevel extends Component {
    handleMouseOver = () => {
        let {dispatch} = this.props;
        let itemid = this.props.favid;
        dispatch(setLevelOver(itemid, true));
    }

    handleMouseOut = () => {
        let {dispatch} = this.props;
        let itemid = this.props.favid;
        dispatch(setLevelOver(itemid, false));
    }

    /**
     * 重要程序选中一个，即修改重要程度
     * @param obj
     */
    onLevelMenuSelect = (obj) => {
        let {dispatch, favid} = this.props;
        let importlevel = obj.key;
        dispatch(changeLevel(favid, importlevel));
    }

    renderLevelMenu = () => {
        let {favTypeMap} = this.props;
        let btn1 = null, btn2 = null, btn3 = null;
        for (key in favTypeMap) {
            let value = favTypeMap[key];
            if (key == "1") {
                btn1 = (
                    <Menu.Item key={key}>
                        <div className="sysfav-level-menu-common sysfav-level-menu-normal">
                            <div className="icon"></div>
                            <div className="text">{value}</div>
                        </div>
                    </Menu.Item>
                );
            } else if (key == "2") {
                btn2 = (
                    <Menu.Item key={key}>
                        <div className="sysfav-level-menu-common sysfav-level-menu-middle">
                            <div className="icon"></div>
                            <div className="text">{value}</div>
                        </div>
                    </Menu.Item>
                );
            } else if (key == "3") {
                btn3 = (
                    <Menu.Item key={key}>
                        <div className="sysfav-level-menu-common sysfav-level-menu-important">
                            <div className="icon"></div>
                            <div className="text">{value}</div>
                        </div>
                    </Menu.Item>
                );
            }
        }
        return [btn3, btn2, btn1];
    }

    render() {
        let cls1 = "";
        let cls2 = "";
        let {importantLevel, isover} = this.props;
        if (importantLevel == 3) {   //重要
            cls2 = "important";
        } else if (importantLevel == 2) {   //关注
            cls2 = "middle";
        } else {
            if (!isover) {
                cls1 = "hidelevel";
            }
            cls2 = "normal";
        }

        let {islevelover} = this.props;
        let cls3 = "";
        if (islevelover) {
            cls3 = "over";
        }

        const menu = (
            <Menu onSelect={this.onLevelMenuSelect}>
                {this.renderLevelMenu()}
            </Menu>
        );

        return (
            <Dropdown overlay={menu} trigger={['hover']}>
                <div className={`common sysfav-list-item-info-level ${cls1}`} onMouseOver={this.handleMouseOver}
                     onMouseOut={this.handleMouseOut}>
                    <div className={`text ${cls2}`}>
                        <span title={this.props.importtext} className={cls3}>{this.props.importtext}</span>
                    </div>
                </div>
            </Dropdown>
        );
    }
}