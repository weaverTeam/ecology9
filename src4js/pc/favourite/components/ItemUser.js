import React, {Component} from "react";

export default class ItemUser extends Component {
    render() {
        let {resourcename, resourceid} = this.props;
        return (
            <div className="common sysfav-list-item-info-user">
                <a href={`javaScript:openhrm(${resourceid});`} onClick={event => window.pointerXY(event)}
                   title={resourcename}>{resourcename}
                </a>
            </div>
        );
    }
}