import React from "react";

import SysFavouriteItem from "./SysFavouriteItem";

class SysFavouriteList extends React.Component {
    renderItems() {
        let list = this.props.data;
        let {dispatch, repeatIconTitle, editIconTitle, moveIconTitle, deleteIconTitle, favTypeMap, selectedIds} = this.props;
        return list.map(item => {
            let props = {
                ...item,
                dispatch,
                repeatIconTitle,
                editIconTitle,
                moveIconTitle,
                deleteIconTitle,
                favTypeMap,
                selectedIds
            };
            return <SysFavouriteItem key={item.favid} {...props}/>;
        });
    }


    render() {
        const items = this.renderItems();
        return (
            <div className="sysfav-list">
                {items}
            </div>
        );
    }
}

export default SysFavouriteList;