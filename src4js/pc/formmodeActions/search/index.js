class SearchActionTemplate {
    constructor(props) {
        this.guid = props.guid;
        this.store = props.store;
        this.layoutStore = props.layoutStore;
        this.layoutGuid = props.layoutGuid;
    }
    search = () => {
        this.store.toggleAdvancedQueryPanel(this.guid, false);
        this.store.search(this.guid);
    }
    add = () => {
        this.layoutStore.toggleRight(this.layoutGuid);
    }
    defaultAction = () => { }

    getAction = (isSystemFlag, expandId) => {
        switch (isSystemFlag) {
            case 100:
                return this.search;
            case 101:
                return this.add;
            default:
                return this.defaultAction;
        }
    }
    runAction = (isSystemFlag, expandId) => {
        console.log('expandId', expandId);
        this.getAction(isSystemFlag, expandId)();
    }
}

export default SearchActionTemplate;