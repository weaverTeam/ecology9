import Route from "react-router/lib/Route"

import Home from "./components/Home.js"
import Mine from "./components/Mine.js"
import All from "./components/All.js"
import JobExchange from "./components/JobExchange.js"
import Repeat from "./components/Repeat.js"
import Share from "./components/Share.js"
import Search from "./components/Search.js"
import TimeManage from "./components/TimeManage.js"

import "./css/icon.css"

import reducer from "./reducers/"
import * as MineAction from "./actions/mine"
import * as AllAction from "./actions/all"
import * as JobExchangeAction from "./actions/jobExchange"
import * as RepeatAction from "./actions/repeat"
import * as ShareAction from "./actions/share"
import * as SearchAction from "./actions/search"
import * as TimeManageAction from "./actions/timeManage"

const workplanRoute = (
  <Route path="workplan" component={ Home }>
    <Route name="mine" path="mine" component={ Mine }/>
    <Route name="all" path="all" component={ All }/>
    <Route name="jobExchange" path="jobExchange" component={ JobExchange }/>
    <Route name="repeat" path="repeat" component={ Repeat }/>
    <Route name="share" path="share" component={ Share }/>
    <Route name="search" path="search" component={ Search }/>
    <Route name="timeManage" path="timeManage" component={ TimeManage }/>
  </Route>
)

module.exports = {
  Route: workplanRoute,
  reducer,
  action: {
    MineAction,
    AllAction,
    JobExchangeAction,
    RepeatAction,
    ShareAction,
    SearchAction,
    TimeManageAction
  }
}