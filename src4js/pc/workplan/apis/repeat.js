import { WeaTools } from 'ecCom'

export const getRepeatDatas = params => {
	return WeaTools.callApi('/api/meeting/repeat/getData', 'post', params);//与后台通讯获取数据
}

export const getCondition = params => {
	return WeaTools.callApi('/api/meeting/repeat/getCondition', 'post', params);//与后台通讯获取数据
}