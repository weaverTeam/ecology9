import { WeaTools } from 'ecCom'

export const getDatas = params => {
	return WeaTools.callApi('/api/meeting/search/getData', 'post', params);//与后台通讯获取数据
}

export const getCondition = params => {
	return WeaTools.callApi('/api/meeting/search/getCondition', 'post', params);//与后台通讯获取数据
}