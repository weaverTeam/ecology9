import workplanMine from "./mine"
import workplanAll from "./all"
import workplanJobExchange from "./jobExchange"
import workplanRepeat from "./repeat"
import workplanShare from "./share"
import workplanSearch from "./search"
import workplanTimeManage from "./timeManage"

export default {
	workplanMine,
    workplanAll,
    workplanJobExchange,
    workplanRepeat,
    workplanShare,
    workplanSearch,
    workplanTimeManage
}