import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '张三',
    loading: false,
    detailTabKey:"1",
    conditioninfo:[],
	nowRouterCmpath:"contacterView",
	orderFields:{},      //表单对象
	canEdit:true,
	contacterId :1,
    customerId:1,
    hasright:"-1",
    trailinfo:[]
}

let initialState = Immutable.fromJS(initState);

export default function contacterView(state = initialState, action) {
    switch (action.type) {
		case types.CONTACTERVIEW_INIT_CONDITION:
          	return state.merge({conditioninfo:action.conditioninfo,title:action.title,canEdit:action.canEdit,customerId:action.customerId,contacterId:action.contacterId,hasright:action.hasright});
	    case types.CONTACTERVIEW_SAVE_ORDER_FIELDS:
          	return state.merge({orderFields: action.value});
        case types.SET_DETAIL_TABKEY:
            return state.merge({detailTabKey:action.detailTabKey});
		case types.CONTACTERVIEW_INIT_TRAILINFO:
          	return state.merge({trailinfo: action.trailinfo});
        default:
            return state
    }
}