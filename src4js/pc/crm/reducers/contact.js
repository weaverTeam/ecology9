import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '客户联系',
    loading: false,
    dataKey:"",
    nowRouterCmpath:"contact",
    paramsBase:{
       selectType:"info"
    },
    searchParams:{
    },
    conditioninfo:[],
    showSearchAd:false,     //高级搜索展开
    searchParamsAd:{},    //高级搜索条件对象
    orderFields:{},      //表单对象
    isClearNowPageStatus:false,
    detailTabKey:"info",
    remindCount:0,
    remindInfo:{"before":"30","isRemind":"0"},
    customerId:""
}

let initialState = Immutable.fromJS(initState);

export default function contact(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
		case types.CONTACT_LOADING: 
      		return state.merge({loading: action.loading});
      	case types.CONTACT_INIT_TABLE: 
            return state.merge({dataKey: action.dataKey,loading:action.loading,paramsBase:action.paramsBase});
        case types.CONTACT_INIT_REMIND_TABLE:
            return state.merge({dataKey: action.dataKey,loading:action.loading});
        case types.CONTACT_SET_DETAIL_TABKEY: 
      		return state.merge({detailTabKey: action.key,orderFields:{},searchParamsAd:{},showSearchAd:false});
        case types.CONTACT_INIT_CONDITION:
            return state.merge({conditioninfo: action.conditioninfo});
        case types.CONTACT_SET_SHOW_SEARCHAD:
          	return state.merge({showSearchAd:action.value});
       case types.CONTACT_SAVE_ORDER_FIELDS:
          	return state.merge({orderFields: action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()});
        case types.CONTACT_CLEAR_PAGE_STATUS:
      		return state.merge({isClearNowPageStatus:action.isToReq});
    	case types.CONTACT_UNMOUNT_CLEAR:
      		return state.merge({searchParamsAd:{},showSearchAd:false}).merge(
      			action.isToReq ? {} : {searchParams: initState.searchParams,orderFields:{}});
        case types.CONTACT_SAVE_COUNT:
                return state.merge({remindCount:action.remindCount});
        case types.CONTACT_SAVE_REMINDINFO:
            return state.merge({remindInfo:action.remindInfo,customerId:action.customerId});
        default:
            return state
    }
}