import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'

let initState = {
    loading: false,
    selectTabKey:"1",
    showSearchAd:false,
    contactLogCondition:[],
    contacterCardCondition:[],
    leaveMessageCondition:[],
    sellchanceCardCondition:[],
    highSearchParamsAd:{
        remark:"",
        firstname:"",
        remarkM:"",
        subject:"",

    },
    orderFields:{},
    baseParams:{customerId:""},
    contacterCardData:[],
    sellChanceCardData:[],
    addContacterCondition:[],
}

let initialState = Immutable.fromJS(initState);

export default function cardManage(state = initialState, action) {
    switch (action.type) {
        case types.CARDMANAGE_SET_DETAIL_TABKEY:
              return state.merge({selectTabKey: action.key,orderFields:{},highSearchParamsAd:{}});
        case types.CARDMANAGE_SAVE_LEFT_PARAMS:
            return state.merge({baseParams: action.value});
        case types.CARDMANAGE_SET_SHOW_SEARCHAD:
            return state.merge({showSearchAd: action.value});
        case types.CARDMANAGE_SET_CONTACTLOG_CONDITION:
            return state.merge({contactLogCondition: action.contactLogCondition});
        case types.CARDMANAGE_SET_CONTACTERCARD_CONDITION:
            return state.merge({contacterCardCondition: action.contacterCardCondition});
        case types.CARDMANAGE_SET_LEAVEMESSAGE_CONDITION:
            return state.merge({leaveMessageCondition: action.leaveMessageCondition});
        case types.CARDMANAGE_SET_SELLCHANCE_CONDITION:
            return state.merge({sellchanceCardCondition: action.sellchanceCardCondition});
        case types.CARDMANAGE_SAVE_ORDER_FIELDS:
            return state.merge({orderFields: action.value,highSearchParamsAd:function(){
                let params = {};
                if(action.value){
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value;
                    }
                }
                return params
            }()});
        case types.CARDMANAGE_SET_CONTACTER_INFO:
            return state.merge({contacterCardData: action.contacterCardData});
        case types.CARDMANAGE_SET_SELLCHANCE_INFO:
            return state.merge({sellChanceCardData: action.sellChanceCardData});
        case   types.CARDMANAGE_SET_ADDCONTACTER_CONDITION:
            return state.merge({addContacterCondition: action.addContacterCondition,orderFields:{},highSearchParamsAd:{}});
        default:
            return state
    }
}