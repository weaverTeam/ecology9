
import crmcontactQuery from './contacterQuery'
import crmcontacterView from './contactView'
import crmcustomerContact from "./customerContact"
import crmcustomerView from './customerView'
import crmsearchResult from './search'
import crmsellChanceMain from './sellChanceMain'
import crmcustomerBirthday from './customerBirthday'
import addcustomer from './addCustomer'
import viewCustomerBase from './viewCustomerBase'
import mineCustomer from './mineCustomer'
import contact from './contact'
import crmCardManage from './cardManage'
import crmleaveMessage from './leaveMessage'
import crmoutResources from './outResources'
import crmcontractQuery from './contractQuery'

export default {
	crmcontactQuery,
    crmcontacterView,
    crmcustomerContact,
    crmcustomerView,
    crmsearchResult,
    crmsellChanceMain,
    crmcustomerBirthday,
    addcustomer,
    viewCustomerBase,
    mineCustomer,
    contact,
    crmCardManage,
    crmleaveMessage,
    crmoutResources,
    crmcontractQuery
}