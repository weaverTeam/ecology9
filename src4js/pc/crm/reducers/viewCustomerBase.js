import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'


let initState = {
    loading: false,
	nowRouterCmpath:"viewCustomerBase",
    conditioninfo:[],
	rcList:[],
	orderFields:{},      //表单对象
	canEdit:true,
	contacterId :1,
	customerId:1,
	contacterData:[],
	customerMermgeForm:[],
	showModal:false,
	modalInfo:{modalStyle:{}},
	businessInfoData:{},
	businessTab:[{ key: '1',title: '工商信息'},{key: '2', title: '日志'}],
	businessSelectKey:"1",
	businessLogDataKey:"",
	title:""
}

let initialState = Immutable.fromJS(initState);

export default function viewCustomerBase(state = initialState, action) {
    switch (action.type) {
		 case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
		case types.VIEWCUSTOMERBASE_INIT_CONDITION:
          	return state.merge({conditioninfo:action.conditioninfo,rcList:action.rcList,customerId:action.customerId,title:action.title});
	    case types.VIEWCUSTOMERBASE_SAVE_ORDER_FIELDS:
          	return state.merge({orderFields: action.value});
       case types.VIEWCUSTOMERBASE_INIT_TABLE:
            return state.merge({contacterData:action.contacterData});
		case types.VIEWCUSTOMERBASE_UPDATE_TABLE:
			return state.merge({contacterData: action.data});
		case types.VIEWCUSTOMERBASE_CUSTOMER_MERGE:
			return state.merge({customerMermgeForm: action.customerMermgeForm});
		case types.VIEWCUSTOMERBASE_SHOWDIALOG:
			return state.merge({showModal: action.showModal});
		case types.VIEWCUSTOMERBASE_INIT_DIALOG:
			return state.merge({modalInfo: action.modalInfo});
		case types.VIEWCUSTOMERBASE_SAVE_BUSINESSINFO:
			return state.merge({businessInfoData: action.businessInfoData});
		case types.VIEWCUSTOMERBASE_SET_BUSINESSINFO_TAB:
			return state.merge({businessSelectKey: action.key});
		case types.VIEWCUSTOMERBASE_SET_BUSINESSINFO_DATAKEY:
			return state.merge({businessLogDataKey: action.dataKey});
        default:
            return state
    }
}