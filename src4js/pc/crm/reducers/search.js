import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    loading: false,
    title: '查询客户',
    topButtons:[],
    conditioninfo:  [],
    dataKey:"",
    nowRouterCmpath:"search",
    showSearchAd:false,
    paramsBase:{
        selectType:"search"
    },
    searchParamsAd:{},
    orderFields:{},
    hasright:"-1",
    btnList:[],
    rcList:[],
    customerStr:"",
    customerIds:""
}

let initialState = Immutable.fromJS(initState);

export default function search(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
        case types.SEARCHRESULT_LOADING: 
      		return state.merge({loading: action.loading});
		case types.SEARCHRESULT_INIT_BASE:
          	return state.merge({conditioninfo:action.conditioninfo,paramsBase:action.paramsBase});
      	case types.SEARCHRESULT_INIT_TABLE: 
      		return state.merge({dataKey: action.dataKey,loading:action.loading,hasright:action.hasright});
        case types.SEARCHRESULT_SET_SHOW_SEARCHAD:
          	return state.merge({showSearchAd:action.value});
       case types.SEARCHRESULT_SAVE_ORDER_FIELDS:
          	return state.merge({orderFields: action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()});
        case types.SEARCHRESULT_INIT_TITLEINFO:
            return state.merge({title:action.title,rcList:action.rcList,searchParamsAd:{},orderFields:{}});  //
        case types.SEARCHRESULT_SAVE_ID:
            return state.merge({customerIds:action.customerIds}); 
        case types.SEARCHRESULT_INIT_CUSTOMERNAME:
            return state.merge({customerStr:action.customerStr,customerIds:action.customerIds}); 
        case types.SEARCHRESULT_CLEAR_CUSTOMER:
            return state.merge({searchParamsAd:{}}); 
        default:
            return state
    }
}