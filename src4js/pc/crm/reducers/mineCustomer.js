import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '我的客户',
    loading: false,
    dataKey:"",
    nowRouterCmpath:"mineCustomer",
    showSearchAd:false,
    searchParamsAd:{},
    searchBaseParams:{
        labelid:"my"
    },
    orderFields:{},
    tabDatas:[],
    detailTabKey:"my",
    sectorTreeData: [{"id": "0","isLeaf": true,"name": "全部行业","getLeafApi": ""}],
    statusOptions:[],
    hrmList:[],
    viewTypeList:[],
    isShowResource:false,
    istorefresh:1,
    rightTabData:[{key:1,title:"客户联系"},{key:2,title:"联系人"},{key:3,title:"客户留言"},{key:4,title:"销售机会"}],
    rightTabKey:1,
    contactLogs:[],
    cardloading:false,
    selectValue:"1",
    contacterCard:[],
    leaveMessage:[],
    sellChanceCard:[],
    tagDatas:[],
    rightInfos:{customerId:""},
    showRight:false,
    remindCount:0
}

let initialState = Immutable.fromJS(initState);

export default function mineCustomer(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
        case types.MINECUSTOMER_LOADING: 
      		return state.merge({loading: action.loading});
         case types.MINECUSTOMER_INIT_TABS: 
      		return state.merge({tabDatas: action.tabDatas});
      	case types.MINECUSTOMER_INIT_TABLE: 
      		return state.merge({dataKey: action.dataKey,loading:action.loading,searchBaseParams:action.searchParams,showRight:action.showRight});
        case types.MINECUSTOMER_SET_DETAIL_TABKEY:
            return state.merge({detailTabKey: action.key,istorefresh:action.istorefresh,rightTabKey:1,selectValue:"1"});
        case types.MINECUSTOMER_INIT_TAGS:
            return state.merge({sectorTreeData:action.sectorTreeData,statusOptions:action.statusOptions,hrmList:action.hrmList,viewTypeList:action.viewTypeList,isShowResource:action.isShowResource});
         case types.MINECUSTOMER_SAVE_ORDER_FIELDS:
                return state.merge({orderFields: action.value,searchParamsAd:function(){
                    let params = {};
                    if(action.value){
                        for (let key in action.value) {
                            params[action.value[key].name] = action.value[key].value;
                        }
                    }
                    return params
                }()});
        case types.MINECUSTOMER_SET_RIGHT_DETAIL_TABKEY:
            return state.merge({rightTabKey: action.key});
        case types.MINECUSTOMER_INIT_CONTACTLOGS:
            return state.merge({contactLogs:action.contactLogs,cardloading:action.cardloading})
        case types.MINECUSTOMER_CARDLOADING:
            return state.merge({cardloading:action.cardloading})
        case types.MINECUSTOMER_SAVE_SELECT_VALUE:
            return state.merge({selectValue:action.value})
        case types.MINECUSTOMER_INIT_CONTACTERCARD:
            return state.merge({contacterCard:action.contacterCard,cardloading:action.cardloading})
        case types.MINECUSTOMER_INIT_LEAVEMESSAGE:
            return state.merge({leaveMessage:action.leaveMessage,cardloading:action.cardloading})
        case types.MINECUSTOMER_INIT_SELLCHANCECARD:
            return state.merge({sellChanceCard:action.sellChanceCard,cardloading:action.cardloading})
        case types.MINECUSTOMER_INIT_TAGS_LIST:
             return state.merge({tagDatas:action.tagDatas})   
        case types.MINECUSTOMER_SET_RIGHT:
             return state.merge({rightInfos:action.rightInfos,loading:action.loading,showRight:action.showRight,rightTabKey:1}) 
        case types.MINECUSTOMER_SAVE_HRM_LIST:
            return state.merge({hrmList:action.value}) 
        case types.MINECUSTOMER_GET_REMINDCOUNT:
            return state.merge({remindCount:action.remindCount}) 
        case types.MINECUSTOMER_SAVE_INDUSTRY_LIST:
            return state.merge({sectorTreeData:action.value})
        case types.MINECUSTOMER_CLEAR_PAGE_STATUS:
            return state.merge({showSearchAd:false,
                searchParamsAd:{},
                searchBaseParams:{
                    labelid:"my"
                },
                orderFields:{},
                tabDatas:[],
                detailTabKey:"my",})
        default:
            return state
    }
}