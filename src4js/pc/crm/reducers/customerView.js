import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '客户信息',
    loading: false,
    nowRouterCmpath:"customerView",
    detailTabKey:"1",
    conditioninfo:[],
    tabDatas:[],
    dataKey:"",
    customerId:1,
    // addFormDatas:[],
    // showAddForm:false,
    // outResourceOrderFields:{},
    // outResourceParams:{},
    shareDataKey:"",
    shareOrderFields:{},
    viewlogDataKey:"",
    addressDataKey:"",
    isShowAddress:false,
    addressAddData:[],
    operate:"add",
    addressOrderFields:{},
    addressSearchParams:{},
    sellChanceDataKey:"",
    evalStandard:[],
    evalDataInfo:[],
    tabId:"",
    remindInfo:{},
    customereEvalRcList:[],
    hasright:"-1",
    rightLevel:0
}

let initialState = Immutable.fromJS(initState);

export default function customerView(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
        case types.CUSTOMERVIEW_LOADING:
            return state.merge({loading:action.loading})
        case types.CUSTOMERVIEW_INIT_TABDATAS:
            return state.merge({tabDatas:action.tabDatas,hasright:action.hasright,rightLevel:action.rightLevel,customerId:action.customerId,loading:action.loading,tabId:function(){
                let params = "";
                if(action.tabDatas){
                    for (let i=0;i< action.tabDatas.length;i++) {
                        if(action.tabDatas[i].default){
                            params = action.tabDatas[i].id;
                        }
                    }
                }
                return params
            }()});
		case types.CUSTOMERVIEW_SET_DETAIL_TABKEY:
            return state.merge({detailTabKey:action.detailTabKey,tabId:action.tabId});
        case types.INIT_SHARESETTING_TABLE:
            return state.merge({shareDataKey:action.shareDataKey,loading:action.loading,customerId:action.customerId})
        case types.SAVE_SHARE_FIELDS:
            return state.merge({shareOrderFields: action.value});
        case types.INIT_VIEWLOG_TABLE:
            return state.merge({viewlogDataKey:action.viewlogDataKey,loading:action.loading,customerId:action.customerId})
        case types.INIT_VIEWADDRESS_TABLE:
            return state.merge({addressDataKey:action.addressDataKey,loading:action.loading,customerId:action.customerId,addressOrderFields:{},addressSearchParams:{}})
        case types.INIT_VIEWADDRESS_ADDFORM:
            return state.merge({addressAddData:action.addressAddData,isShowAddress:action.isShowAddress,operate:action.operate,addressId:action.addressId||""})
        case types.SAVE_ADDRESS_FIELDS:
            return state.merge({addressOrderFields: action.value,addressSearchParams:function(){
                let params = {};
                if(action.value){
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value;
                    }
                }
                return params
            }()});
        case types.INIT_LISTSELLCHANCE_TABLE:
            return state.merge({sellChanceDataKey:action.sellChanceDataKey,customerId:action.customerId,loading:action.loading})
        case types.INIT_EVALUATION_STANDARD:
            return state.merge({evalStandard:action.evalStandard})
        case types.INIT_EVALUATION_TABLE:
            return state.merge({evalDataInfo:action.evalDataInfo,customerId:action.customerId})
        case types.CUSTOMERVIEW_SAVE_REMINDINFO:
            return state.merge({remindInfo:action.remindInfo})
        case types.CUSTOMEREVAL_SAVE_RIGHTMENU:
            return state.merge({customereEvalRcList:action.customereEvalRcList})
        default:
            return state
    }
}