import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

let initState = {
    title: '新建客户',
    loading: false,
    orderFields:{
    },
    selectKey:"1",
    nowRouterCmpath:"addCustomer",
    searchParamsAd:{
    },
    customerTypeList:[],
    defaultvalue:"",
    customerAddForm:[],
    tooManyRecord:false,
    noRecord:false,
    recordDatas:[
        
    ],
    columns :[{
            title: '客户名称',
            dataIndex: 'nameLink',
            key: 'nameLink',
            width:"20%",
            render: (text,record,index) => <div dangerouslySetInnerHTML={{__html:text}}></div>,
        }, {
            title: '客户类型',
            dataIndex: 'type',
            key: 'type',
            width:"20%",
        }, {
            title: '从',
            dataIndex: 'typebegin',
            key: 'typebegin',
            width:"20%",
        }, {
            title: '城市',
            dataIndex: 'city',
            key: 'address',
            width:"20%",
        }, {
            title: '客户经理',
            dataIndex: 'managerNameLink',
            key: 'managerNameLink',
            width:"20%",
            render:  (text,record,index) => <div dangerouslySetInnerHTML={{__html:text}}></div>,
        }],
    requiredAttr:[],
    importdatas:{}
}

let initialState = Immutable.fromJS(initState);

export default function addCustomer(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
        case types.ADDCUSTOMER_SET_DETAIL_TABKEY:
      		return state.merge({selectKey: action.selectKey, tooManyRecord:false,noRecord:false,orderFields:{},searchParamsAd:{}});
        case types.ADD_INIT_IMPORTDATAS:
            return state.merge({importdatas: action.importdatas,});
        case types.ADDCUSTOMER_LOADING: 
      		return state.merge({loading: action.loading});
        case types.SAVE_ADD_PARAMS:
            return state.merge({searchParamsAd: action.value});
        case types.ADDCUSTOMER_SAVE_ORDER_FIELDS:
          	return state.merge({orderFields: action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()});
        case types.ADDCUSTOMER_INIT_FORM:
      		return state.merge({customerAddForm: action.customerAddForm,noRecord:action.noRecord,loading:action.loading});
        case types.ADDCUSTOMER_INIT_TYPE:
      		return state.merge({customerTypeList: action.customerTypeList,defaultvalue:action.defaultvalue});
        case types.ADDCUSTOMER_SEARCH:
            return state.merge({recordDatas: action.recordDatas,tooManyRecord:action.tooManyRecord,noRecord:action.noRecord,loading:action.loading});
        case types.ADDCUSTOMER_SAVE_REQUIRED:
            return state.merge({requiredAttr: action.requiredAttr});
        case types.ADDCUSTOMER_UNMOUNT_CLEAR:
            return state.merge({selectKey: "1", tooManyRecord:false,noRecord:false,orderFields:{},searchParamsAd:{},customerAddForm:[],recordDatas:[],requiredAttr:[],importdatas:[],});
        default:
            return state
    }
}