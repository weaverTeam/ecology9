import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '客户生日',
    loading: false,
    dataKey:"",
    nowRouterCmpath:"customerBirthday",
    conditioninfo: [],
    showSearchAd:false,
    searchParamsAd:{},
    orderFields:{},
}

let initialState = Immutable.fromJS(initState);

export default function customerBirthday(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
		case types.CUSTOMERBIRTHDAY_INIT_CONDITION:
          	return state.merge({conditioninfo:action.condition});
		case types.CUSTOMERBIRTHDAY_LOADING: 
      		return state.merge({loading: action.loading});
      	case types.CUSTOMERBIRTHDAY_INIT_TABLE: 
      		return state.merge({dataKey: action.dataKey,loading:action.loading});
        case types.CUSTOMERBIRTHDAY_SET_SHOW_SEARCHAD:
              return state.merge({showSearchAd:action.value});
        case types.CUSTOMERBIRTHDAY_SAVE_ORDER_FIELDS:
            return state.merge({orderFields: action.value,searchParamsAd:function(){
                let params = {};
                if(action.value){
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value
                    }
                }
                return params
            }()});
        default:
            return state
    }
}