import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    loading: false,
    viewlogs:[],
    contacterInfo:[],
    sellChanceInfo:[],
    customerId:"",
    customerContactRcList:[],
    contactFields:{},
    rowHeight:1
}

let initialState = Immutable.fromJS(initState);

export default function customerContact(state = initialState, action) {
    switch (action.type) {
         case types.LOADING:
      		return state.merge({loading: action.loading});
         case types.INIT_VIEWLOGS:
              return state.merge({viewlogs: action.viewlogs,loading:action.loading,customerId:action.customerId});
        case types.CUSTOMERCONTACT_SET_CONTACTER_INFO:
            return state.merge({contacterInfo: action.contacterInfo});
        case types.CUSTOMERCONTACT_SET_SELLCHANCE_INFO:
            return state.merge({sellChanceInfo: action.sellChanceInfo}); 
        case types.CUSTOMERCONTACT_SAVE_RIGHTMENU:
            return state.merge({customerContactRcList:action.customerContactRcList})  
        case types.CUSTOMERCONTACT_SAVE_ORDER_FIELDS:
            return state.merge({contactFields:action.value,contactFieldsAd:function(){
                let params = {};
                if (action.value) {
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value
                    }
                }
                return params
            }() })
        case types.CUSTOMERCONTACT_CHANGE_ROWHEIGHT:
            return state.merge({rowHeight:action.value})
        default:
            return state
    }
}