import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    loading: false,
    viewMessages:[],
    messageFields:{},
    messageFieldsAd:{},
    rowHeight:1,
    customerId:""
}

let initialState = Immutable.fromJS(initState);

export default function leaveMessage(state = initialState, action) {
    switch (action.type) {
         case types.LOADING:
      		return state.merge({loading: action.loading});
         case types.INIT_VIEWMESSAGES:
              return state.merge({viewMessages: action.viewMessages,loading:action.loading,customerId:action.customerId});  
        case types.LEAVEMESSAGE_CHANGE_ROWHEIGHT:
            return state.merge({rowHeight:action.value}) 
        case types.LEAVEMESSAGE_SAVE_ORDER_FIELDS:
            return state.merge({messageFields:action.value,messageFieldsAd:function(){
                let params = {};
                if (action.value) {
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value
                    }
                }
                return params
            }() })  
        default:
            return state
    }
}