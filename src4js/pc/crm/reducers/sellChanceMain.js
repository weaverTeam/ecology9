import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '商机管理',
    loading: false,
    dataKey:"",
    nowRouterCmpath:"sellChanceMain",
    conditioninfo: [],
    showSearchAd:false,
    searchParamsAd:{},
    searchBaseParams:{
        labelid:"my"
    },
    fromType:{
        fromType:"sellchance",
    },
    orderFields:{},
    detailTabKey:"my",
    selectGroupInfo:{
    },
    sellChanceStatus:[],
    tabDatas:[],
    hrmList:[],
    mainTableInfo:[],
    subTableInfo:{columnData:[],columnDefine:[]},
    addOrderFields:{},
    addParamsAd:{},
    closeAddPage:false,
    subTableColumn:[
        {
            "title": "产品名称",
            "isLink":true,
            "linkKey":[
                "assetunitid",
                "salesprice"
            ],
            "linkUrl":'',
            "com": [{
                "formItemType": "BROWSER",
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "domkey": [
                    "productid"
                ],
                "browserType": 38,
                "fieldcol": 17,
                "label": "产品名称",
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "产品名称",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/lgc/search/LgcProductBrowser.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 3,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": true,
                    "type": 38
                }
            }],
            "width": "20%",
            "dataIndex": "productid",
            "key": "productid"
        },
        {
            "title": "计量单位",
            "com": [{
                "formItemType": "BROWSER",
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "domkey": [
                    "assetunitid"
                ],
                "browserType": 69,
                "fieldcol": 17,
                "label": "计量单位",
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "计量单位",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/lgc/maintenance/LgcAssetUnitBrowser.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 1,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": true,
                    "type": 69
                }
            }],
            "width": "10%",
            "dataIndex": "assetunitid",
            "key": "assetunitid"
        },
        {
            "title": "货币",
            "com": [{
                "formItemType": "BROWSER",
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "domkey": [
                    "currencyid"
                ],
                "browserType": 12,
                "fieldcol": 17,
                "label": "货币",
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "货币",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/fna/maintenance/CurrencyBrowser.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 3,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": true,
                    "type": 12
                }
            }],
            "width": "20%",
            "dataIndex": "currencyid",
            "key": "currencyid"
        },
        {
            "title": "单价",
            "isLink":true,
            "linkKey":[
                "salesnum",
                "totelprice"
            ],
            "com": [{
                "formItemType": "INPUT",
                "labelcol": 6,
                "colSpan": 2,
                "viewAttr": 2,
                "length": 10,
                "domkey": [
                    "salesprice"
                ],
                "fieldcol": 17,
                "label": "单价"
            }],
            "width": "15%",
            "dataIndex": "salesprice",
            "key": "salesprice"
        },
        {
            "title": "数量",
                "isLink":true,
            "linkKey":[
                "salesprice",
                "totelprice"
            ],
            "com": [{
                "formItemType": "INPUT",
                "labelcol": 6,
                "colSpan": 2,
                "viewAttr": 2,
                "length": 10,
                "value":1,
                "domkey": [
                    "salesnum"
                ],
                "fieldcol": 17,
                "label": "数量"
            }],
            "width": "10%",
            "dataIndex": "salesnum",
            "key": "salesnum"
        },
        {
            "title": "总价",
            "com": [{
                "formItemType": "INPUT",
                "labelcol": 6,
                "colSpan": 2,
                "viewAttr": 2,
                "length": 10,
                "domkey": [
                    "totelprice"
                ],
                "fieldcol": 17,
                "label": "总价"
            }],
            "width": "20%",
            "dataIndex": "totelprice",
            "key": "totelprice"
        }
    ],
    tagDatas:[],
    viewTypeList:[{name:"仅本人",id:0},{name:"含下属",id:1}],
    istorefresh:1,
    detailTabDatas:[{title:'客户联系',key:"1"},{title:'联系人',key:"2"}],
    rightTabKey:"1",
    selectValue:"1",
    contactLogs:[],
    cardloading:false,
    contacterCard:[],
    showRight:false,
    rightInfos:{},
    sellChanceId:"",
    remindCount:0,
    operate:"add",
    editSellChanceTab:[{ key: 'sellchance',title: '商机'},{key: 'exchange', title: '相关交流'}],
    editSelectKey:"sellchance",
    customerId:"",
    remindInfo:{},
    hasright:"-1"
}

let initialState = Immutable.fromJS(initState);

export default function sellChanceMain(state = initialState, action) {
    switch (action.type) {
        case types.SELLCHANCEMAIN_SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
		case types.SELLCHANCEMAIN_INIT_CONDITION:
          	return state.merge({conditioninfo:action.conditioninfo});
		case types.SELLCHANCEMAIN_LOADING: 
      		return state.merge({loading: action.loading});
        case types.INIT_SELLCHANCESTATUS: 
      		return state.merge({sellChanceStatus: action.sellChanceStatus,hrmList:action.hrmList});
        case types.SELLCHANCEMAIN_INIT_TABS: 
      		return state.merge({tabDatas: action.tabDatas});
      	case types.SELLCHANCEMAIN_INIT_TABLE: 
      		return state.merge({dataKey: action.dataKey,loading:action.loading,searchBaseParams:action.searchParams,showRight:action.showRight});
        case types.SELLCHANCEMAIN_SET_SHOW_SEARCHAD:
          	return state.merge({showSearchAd:action.value});
		case types.SELLCHANCEMAIN_SET_DETAIL_TABKEY:
            return state.merge({detailTabKey:action.key,istorefresh:action.istorefresh,rightTabKey:1,selectValue:"1"})
        case types.SELLCHANCEMAIN_SAVE_ORDER_FIELDS:
            return state.merge({orderFields: action.value,searchParamsAd:function(){
                let params = {};
                if(action.value){
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value;
                    }
                }
                return params
            }()});
        case types.SELLCHANCEMAIN_INIT_NEW_CREATE:
            return state.merge({mainTableInfo:action.mainTableInfo,subTableInfo:action.subTableInfo,loading:action.loading,sellChanceId:action.sellChanceId,operate:action.operate,customerId:action.customerId,hasright:action.hasright})
        case types.SELLCHANCE_SAVE_ADD_ORDER_FIELDS:
            return state.merge({addOrderFields: action.value,addParamsAd:function(){
                let params = {};
                if(action.value){
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value;
                    }
                }
                return params
            }()});
        case types.SELLCHANCE_CREATE_SUCCESS:
            return state.merge({addOrderFields:{},addParamsAd:{},closeAddPage:true})
        case types.SELLCHANCEMAIN_INIT_TAGS_LIST:
            return state.merge({tagDatas:action.tagDatas})
        case types.SELLCHANCEMAIN_SAVE_HRM_LIST:
            return state.merge({hrmList:action.value})
        case types.SELLCHANCEMAIN_SET_RIGHT_DETAIL_TABKEY:
            return state.merge({rightTabKey:action.key})
        case types.SELLCHANCEMAIN_SAVE_SELECT_VALUE:
            return state.merge({selectValue:action.value})
        case types.SELLCHANCEMAIN_INIT_CONTACTLOGS:
            return state.merge({contactLogs:action.contactLogs,cardloading:action.cardloading})
        case types.SELLCHANCEMAIN_CARDLOADING:
            return state.merge({cardloading:action.cardloading})
        case types.SELLCHANCEMAIN_INIT_CONTACTERCARD:
            return state.merge({contacterCard:action.contacterCard,cardloading:action.cardloading})
        case types.SELLCHANCEMAIN_SET_RIGHT:
            return state.merge({rightInfos:action.rightInfos,loading:action.loading,showRight:action.showRight})
        case types.SELLCHANCE_GET_REMINDCOUNT:
            return state.merge({remindCount:action.remindCount})
        case types.SELLCHANCEMAIN_SET_EDIT_TABKEY:
            return state.merge({editSelectKey:action.key})
        case types.SELLCHANCEMAIN_SAVE_REMINDINFO:
            return state.merge({remindInfo:action.remindInfo})
        default:
            return state
    }
}