import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    title: '查询联系人',
    loading: false,
    dataKey:"",
    nowRouterCmpath:"contactQuery",
    conditioninfo:[],
    showSearchAd:false,     //高级搜索展开
    searchParamsAd:{},    //高级搜索条件对象
    orderFields:{},      //表单对象
    isClearNowPageStatus:false,
    hasright:"-1"
}

let initialState = Immutable.fromJS(initState);

export default function contactQuery(state = initialState, action) {
    switch (action.type) {
        case types.SET_NOW_ROUTER_PATH:
      		return state.merge({nowRouterCmpath: action.path});
		case types.CONTACTQUERY_INIT_CONDITION:
          	return state.merge({conditioninfo:action.conditioninfo,hasright:action.hasright});
		case types.CONTACTQUERY_LOADING: 
      		return state.merge({loading: action.loading});
      	case types.CONTACTQUERY_INIT_TABLE: 
      		return state.merge({dataKey: action.dataKey,loading:action.loading});
        case types.CONTACTQUERY_SET_SHOW_SEARCHAD:
          	return state.merge({showSearchAd:action.value});
       case types.CONTACTQUERY_SAVE_ORDER_FIELDS:
          	return state.merge({orderFields: action.value,searchParamsAd:function(){
            	let params = {};
                if(action.value){
			    	for (let key in action.value) {
				    	params[action.value[key].name] = action.value[key].value
			    	}
		    	}
                return params
            }()});
       case types.CONTACTQUERY_CLEAR_PAGE_STATUS:
      		return state.merge({isClearNowPageStatus:action.isToReq});
       case types.CONTACTQUERY_UNMOUNT_CLEAR:
      		return state.merge({searchParamsAd:{},showSearchAd:false}).merge(
      			action.isToReq ? {} : {orderFields:{}});
        default:
            return state
    }
}