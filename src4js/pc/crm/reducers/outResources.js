import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

import {WeaTools} from 'ecCom'

const {ls} = WeaTools;

let initState = {
    dataKey:"",
    loading:false,
    title:"外部用户",
    customerId:1,
    addFormDatas:[],
    showAddForm:false,
    outResourceOrderFields:{},
    outResourceParams:{},
    viewDatas:[],
    edit:false,
    resourceid:1,
}

let initialState = Immutable.fromJS(initState);

export default function outResource(state = initialState, action) {
    switch (action.type) {
        case types.INIT_OUTRESOURCE_TABLE:
            return state.merge({dataKey:action.outResourceDataKey,loading:action.loading,customerId:action.customerId,showAddForm:action.showAddForm});
        case types.OUTRESOURCE_ADDFORM:
            return state.merge({addFormDatas:action.addFormDatas,showAddForm:action.showAddForm,outResourceOrderFields:{},outResourceParams:{}});
        case types.SAVE_OUTRESOURCE_FIELDS:
            return state.merge({outResourceOrderFields: action.value,outResourceParams:function(){
                let params = {};
                if(action.value){
                    for (let key in action.value) {
                        params[action.value[key].name] = action.value[key].value
                    }
                }
                return params
            }()});
        case types.OUTRESOURCE_VIEWFORM:
            return state.merge({viewDatas:action.viewDatas,edit:action.edit,loading:action.loading,resourceid:action.resourceid,
                outResourceOrderFields:{},outResourceParams:{}});
        case types.OUTRESOURCE_LOADING:
            return state.merge({loading:action.loading});
       
        default:
            return state
    }
}