import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'
import objectAssign from 'object-assign'
import cloneDeep from 'lodash/cloneDeep'

let initState = {
    title: '合同管理',
    loading: false,
    dataKey: "",
    selectedKey: "all",
    nowRouterCmpath: "contractQuery",
    showSearchAd: false,  //是否默认展开高级搜索
    searchParamsAd: {},    //高级搜索条件对象
    orderFields: {},      //表单对象
    isClearNowPageStatus: false,
    isShowDialog: false,
    dialogLoading:false,
    mainTableInfo:[],//合同主表
    subTable1Info:[],//产品明细
    subTable2Info:[],//付款方式
    subTable3Info:{},  //产品交货明细
    subTable4Info:{},   //付款交货明细
    rightMenuInfo:[],//右键菜单
    canEdit:false,//合同查看页面中是否显示编辑按钮
    dialogPageType:"",
    contractId:"",//当前模态窗口处理的合同ID
    tabDatas: [],
    conditionInfo:[],
    modleSelectTabKey:"1",
    shareList:[],
    hasTab:true,
    shareData:[],
    exchangeList:[],
    infoFields:{},
    infoFieldsAd:{},
    infoRequire:[],
    payMethodList:[],
    productList:[],
    fromType:{},
    
}

let initialState = Immutable.fromJS(initState);

export default function contractQuery(state = initialState, action) {
    switch (action.type) {
        case types.SET_DETAIL_TABKEY:
            return state.merge({ selectedKey: action.key });
        case types.CONTRACTQUERY_INIT_CONDITION:
            return state.merge({ conditionInfo: action.conditionInfo,tabDatas:action.tabDatas,fromType:action.fromType});
        case types.CONTRACTQUERY_INIT_TABLE:
            return state.merge({ dataKey: action.dataKey, loading: action.loading });
        case types.CONTRACTQUERY_SET_SHOW_SEARCHAD:
            return state.merge({ showSearchAd: action.value });
        case types.CONTRACTQUERY_LOADING:
            return state.merge({ loading: action.loading });
        case types.CONTRACTQUERY_SAVE_ORDER_FIELDS:
            return state.merge({
                orderFields: action.value, searchParamsAd: function () {
                    let params = {};
                    if (action.value) {
                        for (let key in action.value) {
                            params[action.value[key].name] = action.value[key].value
                        }
                    }
                    return params
                }()
            });
        case types.CONTRACTQUERY_CLEAR_PAGE_STATUS:
            return state.merge({ isClearNowPageStatus: action.isToReq });
        case types.CONTRACTQUERY_UNMOUNT_CLEAR:
            return state.merge({ searchParamsAd: {}, showSearchAd: false }).merge(action.isToReq ? {} : { orderFields: {} });
        case types.CONTRACTQUERY_SHOW_DIALOG:
            return state.merge({ isShowDialog: action.isShowDialog,dialogLoading:action.dialogLoading,modleSelectTabKey:action.modleSelectTabKey,hasTab:action.hasTab});
        case types.CONTRACTQUERY_SET_CONTRACTINFO:
            return state.merge({ mainTableInfo:action.mainTableInfo,subTable1Info:action.subTable1Info,subTable2Info:action.subTable2Info,
                rightMenuInfo:action.rightMenuInfo,canEdit:action.canEdit,contractId:action.contractId,dialogPageType:action.dialogPageType,status:action.status,
                subTable3Info:action.subTable3Info,subTable4Info:action.subTable4Info});
        case types.CONTRACTQUERY_CLOSE_DIALOG:
            return state.merge({ isShowDialog : action.isShowDialog,mainTableInfo:[],subTable1Info:[],subTable2Info:[],rightMenuInfo:[],canEdit:false,dialogPageType:"",contractId:-1});
        case types.CONTRACTQUERY_SET_MODAL_TAB:
            return state.merge({modleSelectTabKey:action.key})
        case types.CONTRACTQUERY_SET_SHARELIST:
            return state.merge({shareList:action.shareList,contractId:action.contractId,shareData:action.shareList,orderFields:{},})
        case types.CONTRACTQUERY_SAVE_SHAREDATA:
            return state.merge({shareData:action.shareData})
        case types.CONTRACTQUERY_SET_EXCHANGELIST:
            return state.merge({exchangeList:action.exchangeList,contractId:action.contractId})
        case types.CONTRACTQUERY_SAVE_INFO_FIELDS:
            return state.merge({
                infoFields: action.value, infoFieldsAd: function () {
                    let params = {};
                    if (action.value) {
                        for (let key in action.value) {
                            params[action.value[key].name] = action.value[key].value
                        }
                    }
                    return params
                }()
            });
        case types.CONTRACTQUERY_SAVE_INFO_REQUIRE:
            return state.merge({infoRequire:action.value})
        case types.CONTRACTQUERY_SAVE_INFO_PRODUCT:
            return state.merge({productList:action.value})
        case types.CONTRACTQUERY_SAVE_INFO_PAY:
            return state.merge({payMethodList:action.value})
        case types.CONTRACTQUERY_CLEAR_INFO:
            return state.merge({mainTableInfo:[],subTable1Info:[],subTable2Info:[],rightMenuInfo:[]})
        case types.CONTRACTQUERY_CLEAR_SAVE_INFO:
            return state.merge({infoFields:{},infoFieldsAd:{},infoRequire:[],payMethodList:[],productList:[]})
        default:
            return state;
    }
}