import {WeaTools} from 'ecCom'

export const getContractList = params => {    //合同列表
    return WeaTools.callApi('/api/crm/contract/list', 'GET', params);
}

export const getContractCondition = params => {    //高级搜素
    return WeaTools.callApi('/api/crm/contract/condition', 'GET', params);
}

export const getCmTabs = params => {    //合同tab页签
    return WeaTools.callApi('/api/crm/contract/tabs', 'GET', params);
}

export const getContractInfo = params => {        //获得合同新建、编辑、查看表单项目字段信息。
    return WeaTools.callApi('/api/crm/contract/form', 'GET', params);
}

export const approveInfo = params => {   // 提交审批
    return WeaTools.callApi('/api/crm/contract/approve', 'POST', params);
}

export const sign = params => {    //合同签单
    return WeaTools.callApi('/api/crm/contract/sign', 'POST', params);
}

export const complete = params => {    //执行完成
    return WeaTools.callApi('/api/crm/contract/complete', 'POST', params);
}

export const getContractShareList = params => {    //合同共享列表
    return WeaTools.callApi('/api/crm/contract/shareList', 'GET', params);
}

export const contractShareSave = params => {    //合同共享保存
    return WeaTools.callApi('/api/crm/contract/shareSave', 'POST', params);
}

export const getExchangeList = params => {    //合同相关交流  列表
    return WeaTools.callApi('/api/crm/contract/exchangeList', 'GET', params);
}

export const saveExchange = params => {    //合同相关交流  保存
    return WeaTools.callApi('/api/crm/contract/exchangeSave', 'POST', params);
}

export const statusBack = params => {    //合同 重新打开
    return WeaTools.callApi('/api/crm/contract/statusBack', 'POST', params);
}

export const saveInfo = params => {    //合同 编辑之后保存
    return WeaTools.callApi('/api/crm/contract/save', 'POST', params);
}

export const saveCreateInfo = params => {    //合同 新建保存
    return WeaTools.callApi('/api/crm/contract/create', 'POST', params);
}

export const payGoodsSave = params => {    //合同 ---付货保存接口
    return WeaTools.callApi('/api/crm/contract/payGoodsSave', 'POST', params);
}

export const payGoodsDelete = params => {    //合同 ---付货 删除
    return WeaTools.callApi('/api/crm/contract/payGoodsDelete', 'POST', params);
}

export const paySave = params => {    //合同 ---付款保存接口
    return WeaTools.callApi('/api/crm/contract/paySave', 'POST', params);
}

export const payDelete = params => {    //合同 ---付款 删除
    return WeaTools.callApi('/api/crm/contract/payDelete', 'POST', params);
}