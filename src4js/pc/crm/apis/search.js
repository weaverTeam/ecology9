import {WeaTools} from 'ecCom'

export const getCmCustomerList = params => {   //获取查询客户type=search，批量共享type=share，客户监控type=monitor，分配客户经理type=assign
    return WeaTools.callApi('/api/crm/customer/searchResult', 'GET', params);
}

export const getSearchCondition = params => {   //获取高级搜索
    return WeaTools.callApi('/api/crm/customer/condition', 'GET', params);
}

export const getSearchTitleInfo= params => {   //获  返回头信息以及按钮和右键中菜单
    return WeaTools.callApi('/api/crm/customer/seachResultRightMenu', 'GET', params);
}

export const saveCustomerAssign= params => {   //获保存客户经理分配
    return WeaTools.callApi('/api/crm/customer/assignSave', 'GET', params);
}

export const getCrmNamesById= params => {   //客户经理分配 根据id获取名称
    return WeaTools.callApi('/api/crm/customer/getCrmNamesById', 'GET', params);
}

export const saveChangeManager= params => {   //获保存  修改客户经理
    return WeaTools.callApi('/api/crm/customer/crmChangeManager', 'GET', params);
}

export const saveCustomerShare =params =>{    //批量共享  保存
    return WeaTools.callApi('/api/crm/customer/crmMutiShare', 'GET', params);
}