import {WeaTools} from 'ecCom'

export const getContacterForm = params => {
    return WeaTools.callApi('/api/crm/contacter/form', 'GET', params);
}

export const contacterEdit = params => {
    return WeaTools.callApi('/api/crm/contacter/edit', 'POST', params);
}

export const deleteContracter = params => {   //删除
    return WeaTools.callApi('/api/crm/contacter/delete', 'POST', params);
}

export const getTrailInfo = params => {   //获取工作轨迹值
    return WeaTools.callApi('/api/crm/contacter/trail', 'GET', params);
}