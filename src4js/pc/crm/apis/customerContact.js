import {WeaTools} from 'ecCom'

//获取客户  联系记录
export const getCusContactList = params => {
    return WeaTools.callApi('/api/crm/customer/contactLogs', 'GET', params);
}

//获取我的客户=-联系人卡片信息
export const getContacterCard = params => {  
    return WeaTools.callApi('/api/crm/customer/contacterCard', 'GET', params);
}

//获取我的客户=-销售机会
export const getSellchanceCard = params => {  
    return WeaTools.callApi('/api/crm/customer/sellchanceCard', 'GET', params);
}

//联系记录新建
export const saveCusContact = params => {
    return WeaTools.callApi('/api/crm/contactLog/contactCreate', 'GET', params);
}