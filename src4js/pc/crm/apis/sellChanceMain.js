import {WeaTools} from 'ecCom'

export const getSellChanceList = params => {    //获取商机列表
    return WeaTools.callApi('/api/crm/sellchance/list', 'GET', params);
}

export const getSellChanceCondition = params => {    //获取商机高级搜索
    return WeaTools.callApi('/api/crm/sellchance/condition', 'GET', params);
}

export const getSellChanceTab = params => {   //获取商机tab
    return WeaTools.callApi('/api/crm/sellchance/tabs', 'GET', params);
}

export const getSellChanceStatusAndHrm = params => {   //获取商机选择框
    return WeaTools.callApi('/api/crm/sellchance/conditionTop', 'GET', params);
}  

export const getShellChanceForm = params => {   //获取商机新建 -  编辑 --列表
    return WeaTools.callApi('/api/crm/sellchance/form', 'GET', params);
}  

export const saveSellChanceForm = params => {   //获取商机新建 -  编辑 --保存
    return WeaTools.callApi('/api/crm/sellchance/edit', 'POST', params);
}  

export const saveProduct = params => {   //获取商机新建 -  编辑 --保存商机产品接口。
    return WeaTools.callApi('/api/crm/sellchance/saveProduct', 'POST', params);
} 

export const getProductInfo = params => {   //产品，。，根据产品id获取产品信息。，明细表联动
    return WeaTools.callApi('/api/crm/product/productInfo', 'GET', params);
}  

export const createSellChance = params => {   //商机新建  保存整个新建页面
    return WeaTools.callApi('/api/crm/sellchance/create', 'POST', params);
}  

export const getSellChanceDetail = params => {   //商机名称 获取右侧详情
    return WeaTools.callApi('/api/crm/sellchance/rightInfo', 'GET', params);
}  

// export const getRemindInfo = params =>{  //商机  相关交流联系提醒
//     return WeaTools.callApi('/api/crm/contactLog/remindInfo', 'GET', params);
// }

// export const saveRemindSet =params =>{ //商机  相关交流联系提醒  保存
//     return WeaTools.callApi('/api/crm/contactLog/remindSetting', 'GET', params);
// }