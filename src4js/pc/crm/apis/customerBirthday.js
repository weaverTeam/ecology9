import {WeaTools} from 'ecCom'

export const getBirthdayList = params => {
    return WeaTools.callApi('/api/crm/customer/birthdayList', 'GET', params);
}

export const getBirthdayCondition = params => {
    return WeaTools.callApi('/api/crm/customer/birthdayCondition', 'GET', params);
}
