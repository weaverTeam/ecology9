import {WeaTools} from 'ecCom'

export const getContacterList = params => {    //查询联系人  列表
    return WeaTools.callApi('/api/crm/contacter/list', 'GET', params);
}

export const getCmCondition = params => {
    return WeaTools.callApi('/api/crm/contacter/condition', 'GET', params);
}  