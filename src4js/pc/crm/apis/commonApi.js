import {WeaTools} from 'ecCom'

export const getRightMenuList = params => {   //请求右键的
    return WeaTools.callApi('/api/crm/rightMenu/menuInfo', 'GET', params);
}