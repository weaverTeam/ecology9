import {WeaTools} from 'ecCom'

export const getContactList = params => {   //客户联系-联系情况-联系计划-列表
    return WeaTools.callApi('/api/crm/contactLog/contactLogRp', 'GET', params);
}

export const getContactCondition = params => {   //客户联系-联系情况-联系计划-列表
    return WeaTools.callApi('/api/crm/contactLog/condition', 'GET', params);
}

export const getContactLogRemindList = params => {   //客户联系-联系提醒-列表
    return WeaTools.callApi('/api/crm/contactLog/contactLogRemind', 'GET', params);
}

export const getFinishWorkPlan = params => {   //客户联系-联系提醒-完成
    return WeaTools.callApi('/api/crm/contactLog/finishWorkPlan', 'GET', params);
}

export const getRemindCount = params => {   //客户联系-联系提醒-数量统计
    return WeaTools.callApi('/api/crm/contactLog/remindCount', 'GET', params);
}

export const getRemindInfo = params => {   //客户提醒设置
    return WeaTools.callApi('/api/crm/contactLog/remindInfo', 'GET', params);
}

export const saveRemindSetting = params => {   //客户提醒设置  保存
    return WeaTools.callApi('/api/crm/contactLog/remindSetting', 'GET', params);
}