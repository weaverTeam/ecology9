import {WeaTools} from 'ecCom'

export const getCustomerAdd = params => {   //新建客户表单接口/
    return WeaTools.callApi('/api/crm/customer/customerAdd', 'GET', params);
}

export const  getCustomerType =params =>{   //客户类型接口/api/crm/customer/customerType
     return WeaTools.callApi('/api/crm/customer/customerType', 'GET', params);
}

export const  getCustomerSave =params =>{   //新建客户保存接口/api/crm/customer/customerSave
     return WeaTools.callApi('/api/crm/customer/customerSave', 'GET', params);
}

export const  getMyCustomerListForAdd =params =>{   //新建客户时，搜索客户接口/api/crm/customer/myCustomerListForAdd
     return WeaTools.callApi('/api/crm/customer/myCustomerListForAdd', 'GET', params);
}


export const  getCrmImportDatas =params =>{   //客户批量导入信息
     return WeaTools.callApi('/api/crm/customer/crmImportDatas', 'GET', params);
}

export const  crmImport =params =>{   //客户批量导入的接口
     return WeaTools.callApi('/api/crm/customer/crmImport', 'GET', params);
}

export const  generateExcel =params =>{ //api/crm/customer/generateExcel 格式化客户导入模板
    return WeaTools.callApi('/api/crm/customer/generateExcel', 'GET', params);
}
