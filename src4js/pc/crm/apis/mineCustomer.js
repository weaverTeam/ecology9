import {WeaTools} from 'ecCom'

//获取tag标签
export const getMyCustomerTags = params => {  
    return WeaTools.callApi('/api/crm/customer/myCustomerTags', 'GET', params);
}

//获取我的客户table列表
export const getMyCustomerList = params => {  
    return WeaTools.callApi('/api/crm/customer/myCustomerList', 'GET', params);
}

//获取我的客户tab详情
export const getConditionTop = params => {  
    return WeaTools.callApi('/api/crm/customer/conditionTop', 'GET', params);
}
//获取我的客户自定义标签列表
export const getMyCustomerTagList = params => {  
    return WeaTools.callApi('/api/crm/label/list', 'GET', params);
}

//我的客户 -- 标签-  标记为重要，取消
export const markImportant = params => {  
    return WeaTools.callApi('/api/crm/label/markImportant', 'GET', params);
}

//我的客户 -- 自定义标签-  应用，取消
export const markLabel = params => {  
    return WeaTools.callApi('/api/crm/label/markLabel', 'GET', params);
}

//获取我的客户=-客户联系
export const getContactLogs = params => {  
    return WeaTools.callApi('/api/crm/customer/contactLogs', 'GET', params);
}

//获取我的客户=-联系人卡片信息
export const getContacterCard = params => {  
    return WeaTools.callApi('/api/crm/customer/contacterCard', 'GET', params);
}

//获取我的客户=-客户留言信息
export const getLeaveMessage = params => {  
    return WeaTools.callApi('/api/crm/customer/leaveMessage', 'GET', params);
}

//获取我的客户=-销售机会
export const getSellchanceCard = params => {  
    return WeaTools.callApi('/api/crm/customer/sellchanceCard', 'GET', params);
}

//我的客户 ==   标签保存
export const crmTagEdit = params => {  
    return WeaTools.callApi('/api/crm/label/edit', 'GET', params);
}

//我的客户 ==   标签删除
export const crmTagDelete = params => {  
    return WeaTools.callApi('/api/crm/label/delete', 'GET', params);
}


//我的客户 ==   点击客户名称刷新方法
export const viewCustomerDetail = params => {  
    return WeaTools.callApi('/api/crm/customer/rightInfo', 'GET', params);
}