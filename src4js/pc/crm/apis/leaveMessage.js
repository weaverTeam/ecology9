import {WeaTools} from 'ecCom'

//获取留言信息
export const getLeaveMessage = params => {
    return WeaTools.callApi('/api/crm/customer/leaveMessage', 'GET', params);
}

//新建留言信息
export const saveLeaveMessage = params => {
    return WeaTools.callApi('/api/crm/customer/leaveMessageSave', 'GET', params);
}
