import {WeaTools} from 'ecCom'

export const addMailMenu = params => {  //新建邮件
    return WeaTools.callApi('/api/crm/customer/getEmail', 'GET', params);
}

export const contactLogExport = params => {  //导出老客户联系
    return WeaTools.callApi('/api/crm/customer/contactLogExport', 'POST', params);
}

export const getUniteForm= params => {   //客户合并 表单
    return WeaTools.callApi('/api/crm/customer/uniteForm', 'GET', params);
}

export const saveUniteForm= params => {   //客户合并 表单  保存
    return WeaTools.callApi('/api/crm/customer/unite', 'GET', params);
}

export const doApply= params => {   //门户申请  密码保存
    return WeaTools.callApi('/api/crm/customer/apply', 'GET', params);
}

export const deleteCustomer= params => {   //删除  客户
    return WeaTools.callApi('/api/crm/customer/crmDelete', 'GET', params);
}

export const getBusinessInfo= params => {   //工商信息
    return WeaTools.callApi('/api/crm/customer/businessInfo', 'POST', params);
}

export const cacheBusinessInfo= params => {   //工商信息  缓存数据
    return WeaTools.callApi('/api/crm/customer/cacheBusinessInfo', 'POST', params);
}

export const saveBusinessLog= params => {   //工商信息  保存日志
    return WeaTools.callApi('/api/crm/customer/saveBusinessLog', 'GET', params);
}

export const getBusinessLog= params => {   //工商信息  日志列表查询
    return WeaTools.callApi('/api/crm/customer/businessLog', 'GET', params);
}