import {WeaTools} from 'ecCom'

export const getCustomerTab = params => {  //获取客户的tab内容
    return WeaTools.callApi('/api/crm/customer/customerTab', 'GET', params);
}

export const getCustomerView = params => {   //获取客户基本信息
    return WeaTools.callApi('/api/crm/customer/customerView', 'GET', params);
}

export const editCustomerInfo = params => {    //编辑保存客户基本信息
    return WeaTools.callApi('/api/crm/customer/customerEdit', 'GET', params);
}

export const editContracterData = params => {    //编辑保存客户基本信息   联系人  即时编辑 -- 新建保存
   return WeaTools.callApi('/api/crm/customer/contacterEdit', 'GET', params);
}


export const getOutResourceInfo = params => {    //获取外部用户列表
    return WeaTools.callApi('/api/crm/customer/outResourceList', 'GET', params);
}

export const getOutResourceAdd = params => {    //获取外部用户列表  新建
    return WeaTools.callApi('/api/crm/customer/outResourceAddForm', 'GET', params);
}

export const saveOutResourceAdd = params => {    //获取外部用户列表  增删改操作
    return WeaTools.callApi('/api/crm/customer/outResourceOpertion', 'GET', params);
}

export const getOutResourceDetail = params => {    //获取外部用户列表  查看详情
    return WeaTools.callApi('/api/crm/customer/outResourceEditForm', 'GET', params);
}

export const getShareList = params => {    //获取共享设置列表  查看
    return WeaTools.callApi('/api/crm/customer/shareList', 'GET', params);
}

export const deleteShareList = params => {    //获取共享设置列表  删除
    return WeaTools.callApi('/api/crm/customer/deleteShare', 'GET', params);
}

export const saveShareList = params => {    //获取共享设置列表  新建
    return WeaTools.callApi('/api/crm/customer/addShare', 'GET', params);
}

export const getViewLogList = params => {    //获取修改记录列表
    return WeaTools.callApi('/api/crm/customer/viewLog', 'GET', params);
}

export const getViewAddressList = params => {    //获取地址管理列表
    return WeaTools.callApi('/api/crm/address/addresslist', 'GET', params);
}

export const viewAddressAddForm= params => {    //地址管理    新建
    return WeaTools.callApi('/api/crm/address/addForm', 'GET', params);
}

export const saveAddressAdd = params => {    //地址管理    新建  保存
    return WeaTools.callApi('/api/crm/address/addressAdd', 'GET', params);
}

export const deleteAddress = params => {    //地址管理列表   删除
    return WeaTools.callApi('/api/crm/address/addressDelete', 'GET', params);
}

export const addressEditForm = params => {    //地址管理列表   编辑
    return WeaTools.callApi('/api/crm/address/editForm', 'GET', params);
}

export const saveAddressEditForm = params => {    //地址管理列表   编辑  保存
    return WeaTools.callApi('/api/crm/address/addressEdit', 'GET', params);
}

export const getListSellChance= params => {    //客户卡片 - 商机管理列表   
    return WeaTools.callApi('/api/crm/sellchance/list', 'GET', params);
} 

export const deleteSellChance =params =>{     //客户卡片 - 商机管理    批量删除  
     return WeaTools.callApi('/api/crm/sellchance/delete', 'POST', params);
}

export const getEvaluationInfo =params =>{     //客户价值   列表信息
    return WeaTools.callApi('/api/crm/customer/crmEvaluation', 'GET', params);
}

export const getEvaluationStandard =params =>{     //客户价值   评估标准
    return WeaTools.callApi('/api/crm/customer/evaluation', 'GET', params);
}

export const evaluationUpdate =params =>{     //客户价值   更新
    return WeaTools.callApi('/api/crm/customer/crmEvaluationUpdate', 'GET', params);
}