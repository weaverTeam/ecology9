import {WeaTools} from 'ecCom'


//获取我的客户=-联系人卡片信息
export const getContacterCard = params => {  
    return WeaTools.callApi('/api/crm/customer/contacterCard', 'GET', params);
}

//获取我的客户=-客户留言信息
export const getLeaveMessage = params => {  
    return WeaTools.callApi('/api/crm/customer/leaveMessage', 'GET', params);
}

//获取我的客户=-销售机会
export const getSellchanceCard = params => {  
    return WeaTools.callApi('/api/crm/customer/sellchanceCard', 'GET', params);
}

//获取我的客户=-联系记录  高级搜索
export const contactLogCondition = params => {  
    return WeaTools.callApi('/api/crm/customer/contactLogCondition', 'GET', params);
}

//获取我的客户=-联系人卡片  高级搜索
export const contacterCardCondition = params => {  
    return WeaTools.callApi('/api/crm/customer/contacterCardCondition', 'GET', params);
}

//获取我的客户=-客户留言  高级搜索
export const leaveMessageCondition = params => {  
    return WeaTools.callApi('/api/crm/customer/leaveMessageCondition', 'GET', params);
}

//获取我的客户=-销售机会  高级搜索
export const sellchanceCardCondition = params => {  
    return WeaTools.callApi('/api/crm/customer/sellchanceCardCondition', 'GET', params);
}

//联系人   新建
export const getContacterAddForm = params => {  
    return WeaTools.callApi('/api/crm/contacter/form', 'GET', params);
}

//联系人   新建  -- 保存
export const saveContacterAdd = params => {  
    return WeaTools.callApi('/api/crm/contacter/create', 'POST', params);
}