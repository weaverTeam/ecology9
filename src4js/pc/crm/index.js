import Route from 'react-router/lib/Route'
import Home from './components/Home.js'
import MineCustomer from './components/MineCustomer.js'
import Contact from './components/Contact.js'
import ContractQuery from './components/ContractQuery.js'
import ContacterQuery from './components/ContacterQuery.js'
import ContacterView from './components/ContacterView.js'
import CustomerView from './components/CustomerView.js'
import SellChanceMain from './components/SellChanceMain.js'
import CustomerBirthday from './components/CustomerBirthday.js'
import EditSellChance from './components/sellchance/EditSellChance.js'
import AddSellChance from './components/sellchance/AddSellChance.js'
import Search from './components/Search.js'
import CustomerAdd from './components/customerAdd/index.js';
import CustomerContact from './components/CustomerContact.js';
import ViewCustomerBase from './components/ViewCustomerBase.js'
import LeaveMessage from './components/customerCard/LeaveMessage.js'
import OutResourceList from './components/customerCard/OutResourceList.js'
import OutResourceView from './components/customerCard/OutResourceView.js'
import ViewCustomerShare from './components/customerCard/ViewCustomerShare.js'
import ViewLog from './components/customerCard/ViewLog.js'
import ViewAddress from './components/customerCard/ViewAddress.js'
import ListSellChance from './components/customerCard/ListSellChance.js'
import CustomerEvaluation from './components/customerCard/CustomerEvaluation.js'

import BusinessInfo from './components/customerCard/BusinessInfo.js'
// import Demo  from './components/Demo'
import './css/index.css'
import './css/icon.css'
import './css/contactquery.css'
import './css/customerView.css'
import './css/customerContact.css'
import './css/sellchancemain.css'
import './css/contacterView.css'
import './css/customerAdd.css'
import './css/search.css'
import './css/contract.css'

import reducers from './reducers'
import * as CrmContactQueryAction from './actions/contacterQuery'
import * as CrmContactViewAction from './actions/contacterView'
import * as CrmCustomerContactAction from './actions/customerContact'
import * as CrmSearchAction from './actions/search'
import * as CrmSellChanceMainAction from './actions/sellChanceMain'
import * as CrmMineCustomerAction from './actions/mineCustomer'

require("./util/index");

import { comsReducer } from 'comsRedux'

let reducer = {...reducers,...comsReducer};


import { WeaTools } from 'ecCom';

const CrmRoute = (
	<Route path="crm" breadcrumbName="客户" component={Home}>
		<Route name="addCustomer"  path="addCustomer" component={CustomerAdd} />  
		<Route name="mineCustomer"  path="mineCustomer" component={MineCustomer} />
		<Route name="contact"  path="contact" component={Contact} />
		<Route name="contract" path="contract" component={ContractQuery} />
     	<Route name="sellChance" path="sellChance" component={SellChanceMain} />
		<Route name="customer" path="customer" component={Search} />
        <Route name="contacterSearch" path="contacterSearch" component={ContacterQuery} />  
		<Route name="birthday" path="birthday" component={CustomerBirthday} />

		
		<Route name="contacterView" path="contacterView" component={ContacterView} />
		<Route name="customerView" path="customerView" component={CustomerView} />
		<Route name="customerContact" path="customerContact" component={CustomerContact} />
		<Route name="viewCustomerBase" path="viewCustomerBase" component={ViewCustomerBase} />
		<Route name="leaveMessage" path="leaveMessage" component={LeaveMessage} />
		<Route name="outinterface" path="outinterface" component={OutResourceList} />
		<Route name="outinterfaceview" path="outinterfaceview" component={OutResourceView} />
		<Route name="viewCustomerShare" path="viewCustomerShare" component={ViewCustomerShare} />
		<Route name="viewlog" path="viewlog" component={ViewLog} />
		<Route name="viewAddress" path="viewAddress" component={ViewAddress} />
		<Route name="listSellChance" path="listSellChance" component={ListSellChance} />
		<Route name="customerEvaluation" path="customerEvaluation" component={CustomerEvaluation} />
	
		<Route name="editSellChance" path="editSellChance" component={EditSellChance} />
		<Route name="addSellChance" path="addSellChance" component={AddSellChance} />
		
		<Route name="businessInfo" path="businessInfo" component={BusinessInfo} />

		{ //<Route name="demo" path="demo" component={Demo} />
	}
    </Route>
)


export default {
	Route: CrmRoute,
	reducer,
	action: {
		CrmContactQueryAction,
		CrmContactViewAction,
		CrmCustomerContactAction,
		CrmSearchAction,
		CrmSellChanceMainAction,
		CrmMineCustomerAction,
	}
};