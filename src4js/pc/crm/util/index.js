//json数据转换
 const transData=(a, idStr, pidStr, chindrenStr)=> {  
        var r = [], hash = {}, id = idStr, pid = pidStr, children = chindrenStr, i = 0, j = 0, len = a.length;  
        for (; i < len; i++) {  
            hash[a[i][id]] = a[i];  
        }  
        for (; j < len; j++) {  
            var aVal = a[j], hashVP = hash[aVal[pid]];  
            if (hashVP) {  
                !hashVP[children] && (hashVP[children] = []);  
                hashVP[children].push(aVal);  
            } else {  
                r.push(aVal);  
            }  
        }  
        return r;  
} 
window.transData=transData;

//获取创建表单值，form默认插入
const crmGetFields=(data)=> {  
    let params = {};
    data && data.map(c =>{
        c.items.map(field => {
            if(field.formItemType =="BROWSER"){
                const replaceDatas = field.browserConditionParam.replaceDatas || [];
                let valueSpan = [];
                for(let i=0;i<replaceDatas.length;i++){
                    valueSpan.push(replaceDatas[i].name);
                }
                params[field.domkey[0]] ={
                    "name":field.domkey[0],
                    "value":field.value,
                    "valueSpan":`${valueSpan}`
                }
            }else{
                params[field.domkey[0]] ={
                    "name":field.domkey[0],
                    "value":field.value,
                    "valueSpan":`${field.value}`
                }
            }
            
        });
    });
    return params
} 
window.crmGetFields=crmGetFields;