import {message,Modal} from 'antd';
import {WeaTools,WeaDialog} from 'ecCom';
import * as SearchAction from '../actions/search'

const confirm = Modal.confirm;


export const fnJoinPara = (fn,para)=>{
	if(!fn || !para || typeof(fn) != "string") 
		return fn;
	let _st = fn.indexOf("(");
	let _ed = fn.lastIndexOf(")");
	if(_st == -1 || _ed == -1)
		return fn;
	let _stStr = fn.substring(0,_st);
	let _edStr = fn.substring(_ed);
	let _para = fn.substring(_st,_ed);
	_para += (_para == "(" ? "" : ",") + "'" + para + "'";
	return _stStr + _para + _edStr;
}

//查询客户   导出客户
export const crmExport = (module)=>{
    return WeaTools.callApi('/api/crm/customer/crmExport', 'GET', {}).then(data=>{
        window.location="/weaver/weaver.file.ExcelOut";          
    });
}

//发送邮件
export const sendMailBatch =(module)=>{
   return WeaTools.callApi('/api/crm/customer/getEmail', 'GET', {"customerids":module}).then(data=>{
       var url = "/email/new/MailInBox.jsp?opNewEmail=1&isInternal=0&to="+data.datas;
       openFullWindowHaveBar(url); 
    });
}

//发送邮件
export const shareBatch =(module)=>{
    return WeaTools.callApi('/api/crm/customer/getEmail', 'GET', {"customerids":module}).then(data=>{
        var url = "/email/new/MailInBox.jsp?opNewEmail=1&isInternal=0&to="+data.datas;
        openFullWindowHaveBar(url); 
     });
 }

 //批量删除
 export const deleteBatch = params =>{
    return WeaTools.callApi('/api/crm/customer/crmDelete', 'GET', {"customerIds":params}).then(data=>{
       if(data.status == "success"){
            //dispatch(doSearch());
            window.store_e9_element.dispatch(SearchAction.doSearch());
       }else{
           message.error("请求失败，"+data.msgcode+"!");
       }
     });
 }

 //升级
 export const doLevelUp =params =>{
    return WeaTools.callApi('/api/crm/customer/crmChangeStatus', 'GET', {"customerIds":params,operation:"up"}).then(data=>{
        if(data.status == "success"){
            return true;
        }else{
            message.error("请求失败，"+data.msgcode+"!");
        }
      });
 }
  //降级
  export const doLevelDown =params =>{
    return WeaTools.callApi('/api/crm/customer/crmChangeStatus', 'GET', {"customerIds":params,operation:"down"}).then(data=>{
        if(data.status == "success"){
            return true;
        }else{
            message.error("请求失败，"+data.msgcode+"!");
        }
      });
 }
 
 

const openWindow = (url)=>{
    let width = screen.availWidth-10 ;
    let height = screen.availHeight-50 ;
    let szFeatures = "top=0," ;
    szFeatures +="left=0," ;
    szFeatures +="width="+width+"," ;
    szFeatures +="height="+height+"," ;
    szFeatures +="directories=no," ;
    szFeatures +="status=yes,toolbar=no,location=no," ;
    szFeatures +="menubar=no," ;
    szFeatures +="scrollbars=yes," ;
    szFeatures +="resizable=yes" ; 
    window.open(url,"",szFeatures) ;
}

window.openFullWindowHaveBar = function(params){
  openFullWindowHaveBar(params);
}

//查看
const openFullWindowHaveBar = (params)=>{
  openWindow(params);
}
