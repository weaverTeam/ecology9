import React, {Component} from 'react'
import {Modal, Button,message,Form,Row,Col,Input,Checkbox,Icon,InputNumber,Popconfirm,Switch  } from 'antd'
const FormItem = Form.Item;
import equal from 'deep-equal'
import { is } from 'immutable'
import {
    WeaDialog,
    WeaRightMenu,
    WeaTop,
    WeaInput,
    WeaSearchGroup ,
    WeaTools,
} from 'ecCom';

class RemindSetModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contactVisible:false,
            showGroup:true,
            isremind:props.remindInfo.get('isRemind')|| "1",
            before:props.remindInfo.get('before') || 1
        }   
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !is(nextProps.customerId,this.props.customerId ) || 
        !is(nextProps.remindInfo,this.props.remindInfo ) || 
        !is(nextState.contactVisible,this.state.contactVisible )||
        !is(nextState.isremind,this.state.isremind )||
        !is(nextState.showGroup,this.state.showGroup )||
        !is(nextState.before,this.state.before )
    }
    componentWillReceiveProps(nextProps,nextState) {
		const { remindInfo={} } = this.props;
		if( !is(nextProps.remindInfo,this.props.remindInfo )){
            this.setState({
                isremind:nextProps.remindInfo.get('isRemind'),
                before:nextProps.remindInfo.get('before')
            })
        }
	}
    render() {
        const  {contactVisible,showGroup,isremind,before} = this.state;
        const {customerId,remindInfo} = this.props;
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 18 },
        };
        return (
            <div>
                <WeaDialog
                    style={{width: 450,height:250}}
                    visible={contactVisible}
                    title={'联系提醒设置'}
                    icon="icon-coms-crm"
                    iconBgcolor="#6d3cf7"
                    buttons={this.getRemindButtons()}
                    onCancel={() => this.setContactVisible(false)}
                >   
                    <div className="wea-crm-table-edit">
                    <Row className="wea-title">
                        <Col span="20">
                            <div>设置项</div>
                        </Col>
                        <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                            <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                        </Col>
                    </Row>
                    <Row className="wea-content" style={{display:showGroup ? "block":"none"} }>
                        <Col className="wea-form-cell" span={24}>
                            <Form horizontal style={{marginTop:"10px"}}>
                                <FormItem
                                    {...formItemLayout}
                                    label="启用"
                                >
                                     <Switch checked={isremind =="0" ? true:false} onChange={this.onSwitchChange} />
                                </FormItem>
                                <FormItem
                                    {...formItemLayout}
                                    label="超过"
                                >
                                    <InputNumber min={1} max={9999999} value={Number(before)} onChange={this.onOutChange} />
                                    <span>天未进行客户联系时自动提醒</span>
                                </FormItem>
                            </Form>
                        </Col>
                    </Row>
                    </div>
                </WeaDialog>
            </div>
        )
    }

    setContactVisible = (visible) => {
        this.setState({contactVisible: visible})
    }
    getButtons(){
        let btn = [];
        btn.push(<Button onClick={() => this.setVisible(false)}>关闭</Button>);

        return btn;
    }
   
    getRemindButtons(){
        let btn = [];
        btn.push(<Button onClick={this.saveRemindSetting.bind(this)}>保存</Button>);
        btn.push(<Button onClick={() => this.setContactVisible(false)}>关闭</Button>);

        return btn;
    }
    onSwitchChange=(checked)=>{
        this.setState({isremind:checked ? "0":"1"})
    }
    onOutChange=(value)=>{
        this.setState({before:value})
    }
    saveRemindSetting(){
        const {before,isremind}= this.state;
        const {actions,customerId} = this.props;
       this.props.saveRemindSetting({customerId:customerId,before:before,isremind:isremind});
       this.setContactVisible(false);
    }
}

export default RemindSetModal