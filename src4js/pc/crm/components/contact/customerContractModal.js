import React, {Component} from 'react'
import {Modal, Button,message,Form,Row,Col,Input,Checkbox,Icon,InputNumber,Popconfirm,Switch  } from 'antd'
const FormItem = Form.Item;
import equal from 'deep-equal'
import { is } from 'immutable'
import {
    WeaDialog,
    WeaRightMenu,
    WeaTop,
    WeaInput,
    WeaSearchGroup ,
    WeaTools,
} from 'ecCom';
import CustomerContact from "../CustomerContact"

class CustomerContractModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            contactVisible:false,
            showGroup:true,
            isremind:props.remindInfo.get('isRemind'),
            before:props.remindInfo.get('before')
        }   
    }
    set visible(value) {
        this.setState({visible: value})
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !is(nextProps.customerId,this.props.customerId ) || 
        !is(nextProps.remindInfo,this.props.remindInfo ) || 
        !is(nextState,this.state )

    }
    componentWillReceiveProps(nextProps,nextState) {
		const { remindInfo={} } = this.props;
		if( !is(nextProps.remindInfo,this.props.remindInfo )){
            this.setState({
                isremind:nextProps.remindInfo.get('isRemind'),
                before:nextProps.remindInfo.get('before')
            });

        }
	}
    render() {
        const  {visible,contactVisible,showGroup} = this.state;
        const {customerId,remindInfo} = this.props;
        const formItemLayout = {
            labelCol: { span: 4 },
            wrapperCol: { span: 18 },
        };
        return (
            <div>
                <WeaDialog
                    style={{width: 800,height:500}}
                    visible={visible}
                    title={'日志详情'}
                    icon="icon-coms-crm"
                    iconBgcolor="#6d3cf7"
                    buttons={this.getButtons()}
                    onCancel={() => this.setVisible(false)}
                >   
                    <div style={{width:"100%",height:500,overflow:"hidden"}} className="crm-contact-modal">
                        <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                            <WeaTop
                                title={"客户联系"}
                                loading={true}
                                icon={<i className='icon-portal-workflow' />}
                                iconBgcolor='#55D2D4'
                                buttons={this.getTopButtons()}
                                buttonSpace={10}
                                showDropIcon={true}
                                dropMenuDatas={this.getRightMenu()}
                                onDropMenuClick={this.onRightMenuClick.bind(this)}
                            >
                            <div style={{height:"447px",overflowX:"hidden",overflowY:"auto"}}>
                            <CustomerContact  ref="contactlogs" type={"customer"} params={{customerId:customerId,from:"all"}} />
                            </div>
                            </WeaTop>
                            
                        </WeaRightMenu>
                    </div>
                </WeaDialog>
                <WeaDialog
                    style={{width: 450,height:250}}
                    visible={contactVisible}
                    title={'联系提醒设置'}
                    icon="icon-coms-crm"
                    iconBgcolor="#6d3cf7"
                    buttons={this.getRemindButtons()}
                    onCancel={() => this.setContactVisible(false)}
                >   
                    <div className="wea-crm-table-edit">
                    <Row className="wea-title">
                        <Col span="20">
                            <div>设置项</div>
                        </Col>
                        <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                            <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                        </Col>
                    </Row>
                    <Row className="wea-content" style={{display:showGroup ? "block":"none"} }>
                        <Col className="wea-form-cell" span={24}>
                            <Form horizontal style={{marginTop:"10px"}}>
                                <FormItem
                                    {...formItemLayout}
                                    label="启用"
                                >
                                     <Switch checked={remindInfo.get('isRemind')=="0" ? true:false} onChange={this.onSwitchChange} />
                                </FormItem>
                                    <FormItem
                                    {...formItemLayout}
                                    label="超过"
                                >
                                    <InputNumber min={1} max={9999999} value={remindInfo.get('before')} onChange={this.onOutChange} />
                                    <span>天未进行客户联系时自动提醒</span>
                                </FormItem>
                            </Form>
                        </Col>
                    </Row>
                    </div>
                </WeaDialog>
            </div>
        )
    }

    setVisible = (visible) => {
        this.setState({visible: visible})
    }
    setContactVisible = (visible) => {
        this.setState({contactVisible: visible})
    }
    getButtons(){
        let btn = [];
        btn.push(<Button onClick={() => this.setVisible(false)}>关闭</Button>);

        return btn;
    }
    onRightMenuClick(key){
    	const { actions} = this.props;
    	if(key == '0'){
    		
    	}
    	if(key == '1'){
    		
    	}
    	if(key == '2'){
    	}
    }
    getRightMenu(){
    	let btns = [];
        btns.push({
    		icon: <i className='icon-coms-go-back'/>,
            content:'返回',
            key:0
    	}); 
    	btns.push({
    		icon: <i className='icon-coms-Collection '/>,
            content:'收藏',
            key:1
    	});
        btns.push({
            icon: <i className='icon-coms-help '/>,
            content:'帮助',
            key:2
        })
    	return btns
    }
    getTopButtons(){
        let btns = [];
        btns.push(<Button type="primary" onClick={()=>{this.setContactVisible(true)}}>联系提醒设置</Button>)
        return btns;
    }
    getRemindButtons(){
        let btn = [];
        btn.push(<Button onClick={this.saveRemindSetting.bind(this)}>保存</Button>);
        btn.push(<Button onClick={() => this.setContactVisible(false)}>关闭</Button>);

        return btn;
    }
    onSwitchChange=(checked)=>{
        this.setState({isremind:checked ? "0":"1"})
    }
    onOutChange=(value)=>{
        this.setState({before:value})
    }
    saveRemindSetting(){
        const {before,isremind}= this.state;
        const {actions,customerId} = this.props;
        actions.saveRemindSetting({customerId:customerId,before:before,isremind:isremind});
        this.setContactVisible(false);
    }
}

export default CustomerContractModal