import {Row,Col,Icon} from 'antd'
import cloneDeep from 'lodash/cloneDeep'
import equal from 'deep-equal'
import './style/index.css'

let sb = '';
class WeaRightLeftLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLeft:props.defaultShowLeft || false,
            isHover:false
        }
    }
    leftShowCo(e){
    	const {showLeft} = this.state;
    	this.setState({showLeft:!showLeft});
    	e.stopPropagation();
    	e.preventDefault();
    	e.nativeEvent.preventDefault();
	}
	componentWillReceiveProps(nextProps,nextState) {
		if(!equal(this.props.defaultShowLeft,nextProps.defaultShowLeft)){
			this.setState({showLeft:false})
		};
	}
    scrollheigth(){
//  	let top = jQuery(".wea-tree-search-layout .wea-right").offset() ? (jQuery(".wea-tree-search-layout .wea-right").offset().top ? jQuery(".wea-tree-search-layout .wea-right").offset().top : 0) : 0;
//      let scrollheigth = document.documentElement.clientHeight - top - 20;
//      jQuery(".wea-tree-search-layout .wea-right").height(scrollheigth);
    }
	componentDidMount(){
		this.scrollheigth();
	}
    render() {
        const {showLeft,height,isHover} = this.state;
        const {children,leftCom,leftWidth} = this.props;
        //判断IE8,禁用部分功能
	    let isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
	    let isIE9 = window.navigator.appVersion.indexOf("MSIE 9.0") >= 0;
	    const leftCol = showLeft ?  {
		    	xs: 24,
		    	sm: 24, 
		    	md: 24,
		    	lg: 24
	    	}:{
		    	xs: isIE8 ? 18 : 14,
		    	sm: 12,
		    	md: 12,
		    	lg: 14
	    	};
	    const rightCol = showLeft ?{
		    	xs: 0,
		    	sm: 0,
		    	md: 0,
		    	lg:0
	    	}: {
		    	xs: isIE8 ? 6 : 10,
                sm: 12,
		    	md: 12,
		    	lg: 10
	    	} ;
        return (
            <Row className="wea-right-left-layout">
                <Col {...leftCol} className="wea-left">
				  <div className='wea-right-left-layout-show-left'
	                    onClick={this.leftShowCo.bind(this)}
	                    onMouseEnter={()=>this.setState({isHover:true})}
	                    onMouseLeave={()=>this.setState({isHover:false})}
	                    style={{background: `url('/spa/crm/images/leftTree-${showLeft ? "hide" : "show"}${isHover ? "" : "-hover"}.png') no-repeat -2px 0`}}
                    ></div>
                    {leftCom}
                </Col>
                <Col {...rightCol} className="wea-right">
                    {children}
                </Col>
            </Row>
        )
    }
}
jQuery(window).resize(function() {
    sb && sb.scrollheigth && sb.scrollheigth()
});


export default WeaRightLeftLayout;
