import forEach from 'lodash/forEach'
import {WeaInput,WeaProjectInput,WeaDocInput,WeaCrmInput,WeaHrmInput,WeaDepInput,WeaWtInput,WeaWfInput,} from 'ecCom'
import {WeaComInput,WeaDateGroup,WeaBrowser,WeaCheckbox,WeaSelect,WeaTextarea,WeaDatePicker,WeaUpload } from 'ecCom'

const getSelectDefaultValue = (options) => {
    let value = '';
    forEach(options, (option) => {
        if(option.selected){
            value = option.key;
        }
    })
    return value;
}


const getComponent = (conditionType = 'input', browserConditionParam, domkey, props, field,callBackFn)=>{
         //console.log('=========================',;
        let ct = conditionType.toUpperCase();
        const {getFieldProps} = props.form;
        if(ct == 'INPUT'){
            return (<WeaInput 
                    {...getFieldProps(domkey[0])} 
                    defaultValue={field.value} 
                    length={field.length}
                    viewAttr={field.viewAttr} 
                    onBlur={(value)=>{callBackFn(field,value)}} 
                    hasBorder
                    type={field.type || "text"}
                />);
        }
        if(ct == 'SELECT'){
            return (
                <WeaSelect
                    {...getFieldProps(domkey[0], {
                        initialValue: getSelectDefaultValue(field.options)
                    })}
                    options= {field.options}
                    viewAttr={field.viewAttr} 
                    // value={field.value}
                    onChange={(value)=>{callBackFn(field,value)}}
                    hasBorder
                />
            );
        }
        if(ct == 'BROWSER'){
            const  showDropMenu = browserConditionParam.type=='4' || browserConditionParam.type=='164';//部门分部显示维度菜单
            // console.log('=========',showDropMenu);
            return (
                <WeaBrowser 
                    {...browserConditionParam} 
                    showDropMenu={showDropMenu}  
                    {...getFieldProps(domkey[0])} 
                    onChange={(ids, names, datas)=>{callBackFn(field,ids, names, datas,)}}
                    hasBorder
                />
            );
        }
        if(ct == 'DATE'){
            // console.log('DATE===field',field);{...getFieldProps(domkey[0],{initialValue: '0'})}
            return (
                <WeaDateGroup
                    {...getFieldProps(domkey[0],{initialValue: '0'})}
                    datas={[{value:'0',name:'全部'},
                            {value:'1',name:'今天'},
                            {value:'2',name:'本周'},
                            {value:'3',name:'本月'},
                            {value:'4',name:'本季'},
                            {value:'5',name:'本年'},
                            {value:'6',name:'指定日期范围'}]}
                    form={props.form}
                    domkey={domkey}
                />
            );
        }
        if(ct == 'CHECKBOX'){
            return (
                <WeaCheckbox
                    {...getFieldProps(domkey[0],{initialValue: field.value})}
                    viewAttr={field.viewAttr} 
                     onChange={(value)=>{callBackFn(field,value)}}
                />
            );
        }
        if(ct == 'HYPERLINK'){
            return (<a href={field.linkUrl} target="_blank">{field.value}</a>);
        }
         if(ct == 'TEXTAREA'){
            return (<WeaTextarea 
                {...getFieldProps(domkey[0],{initialValue: field.value})}
                    length={field.length}
                    minRows={field.minRows ? field.minRows: 2}
                    maxRows={field.maxRows ? field.maxRows: 4}
                  viewAttr={field.viewAttr} 
                  onBlur={(value)=>{callBackFn(field,value)}}
             />);
        }
        if(ct == 'DATEPICKER'){
            return (
                <WeaDatePicker
                    {...getFieldProps(domkey[0])}
                    formatPattern={field.formatPattern || 2}
                    value={field.value}
                    viewAttr={field.viewAttr}
                    onChange={(value)=>{callBackFn(field,value)}}
                />
            );
        }
        // if(ct == 'ATTACHEMENT'){
        //     return (
        //         <WeaUpload
        //             uploadId={domkey[0]}
        //             uploadUrl="/api/crm/common/fileUpload"
        //             category="0,0,0"
        //             autoUpload={true}
        //             showBatchLoad={false}
        //             showClearAll={false}
        //             multiSelection={true}
        //            // datas={field.value}
        //            // onChange={(value)=>{callBackFn(field,value)}}
        //         />
        //     );
        // }
       
};

// const 
export default {getComponent};