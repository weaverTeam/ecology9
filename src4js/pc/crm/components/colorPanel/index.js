import {  Icon,Button ,Input,Row,Col   } from 'antd';
import './style/index.css';

class ColorPanpel extends React.Component {
    constructor(props) {
		super(props);
        this.state={
            bgcolor:this.props.bgcolor|| "#2db7f5",
            valuecolor:this.props.valuecolor || "#fff",
        }
	}
	render() {
        const {data,style} =this.props;
        let {bgcolor,valuecolor}=this.state;
		return (
             <div style={style}>
                <div className="crm-color-panel" >
                    <div className="crm-color-panel-header"><div>调色板</div><div onClick={this.onCancelColor.bind(this)}><Icon type="cross" /></div></div>
                    <Row gutter={16} className="crm-color-panel-row">
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#dee5f2",color:"#5a6986"}} colorValue="#dee5f2" textColor="#5a6986" onClick={()=>this.chooseColor("#dee5f2","#5a6986")}  className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#e0ecff",color:"#206cff"}} colorValue="#e0ecff" textColor="#206cff" onClick={()=>this.chooseColor("#e0ecff","#206cff")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                        <div style={{backgroundColor:"#dfe2ff",color:"#0000cc"}} colorValue="#dfe2ff" textColor="#0000cc" onClick={()=>this.chooseColor("#dfe2ff","#0000cc")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#e0d5f9",color:"#5229a3"}} colorValue="#e0d5f9" textColor="#5229a3" onClick={()=>this.chooseColor("#e0d5f9","#5229a3")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#fde9f4",color:"#854f61"}} colorValue="#fde9f4" textColor="#854f61" onClick={()=>this.chooseColor("#fde9f4","#854f61")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#ffe3e3",color:"#cc0000"}} colorValue="#ffe3e3" textColor="#cc0000" onClick={()=>this.chooseColor("#ffe3e3","#cc0000")} className="color-panel-box">a</div>
                        </Col>
                    </Row>
                <Row gutter={16} className="crm-color-panel-row">
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#5a6986",color:"#dee5f2"}} colorValue="#5a6986" textColor="#dee5f2" onClick={()=>this.chooseColor("#5a6986","#dee5f2")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#206cff",color:"#e0ecff"}} colorValue="#206cff" textColor="#e0ecff" onClick={()=>this.chooseColor("#206cff","#e0ecff")}  className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                        <div style={{backgroundColor:"#0000cc",color:"#dfe2ff"}} colorValue="#0000cc" textColor="#dfe2ff" onClick={()=>this.chooseColor("#0000cc","#dfe2ff")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#5229a3",color:"#e0d5f9"}} colorValue="#5229a3" textColor="#e0d5f9" onClick={()=>this.chooseColor("#5229a3","#e0d5f9")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#854f61",color:"#fde9f4"}} colorValue="#854f61" textColor="#fde9f4" onClick={()=>this.chooseColor("#854f61","#fde9f4")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#cc0000",color:"#ffe3e3"}} colorValue="#cc0000" textColor="#ffe3e3" onClick={()=>this.chooseColor("#cc0000","#ffe3e3")} className="color-panel-box">a</div>
                        </Col>
                    </Row>
                    <Row gutter={16} className="crm-color-panel-row">
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#fff0e1",color:"#ec7000"}} colorValue="#fff0e1" textColor="#ec7000" onClick={()=>this.chooseColor("#fff0e1","#ec7000")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#fadcb3",color:"#b36d00"}} colorValue="#fadcb3" textColor="#b36d00" onClick={()=>this.chooseColor("#fadcb3","#b36d00")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                        <div style={{backgroundColor:"#f3e7b3",color:"#ab8b00"}} colorValue="#f3e7b3" textColor="#ab8b00" onClick={()=>this.chooseColor("#f3e7b3","#ab8b00")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#ffffd4",color:"#636330"}} colorValue="#ffffd4" textColor="#636330" onClick={()=>this.chooseColor("#ffffd4","#636330")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#f9ffef",color:"#64992c"}} colorValue="#f9ffef" textColor="#64992c" onClick={()=>this.chooseColor("#f9ffef","#64992c")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#f1f5ec",color:"#006633"}} colorValue="#f1f5ec" textColor="#006633" onClick={()=>this.chooseColor("#f1f5ec","#006633")} className="color-panel-box">a</div>
                        </Col>
                    </Row>
                    <Row gutter={16} className="crm-color-panel-row">
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#ec7000",color:"#fff0e1"}} colorValue="#ec7000" textColor="#ec7000" onClick={()=>this.chooseColor("#ec7000","#fff0e1")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#b36d00",color:"#fadcb3"}} colorValue="#b36d00" textColor="#fadcb3" onClick={()=>this.chooseColor("#b36d00","#fadcb3")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                        <div style={{backgroundColor:"#ab8b00",color:"#f3e7b3"}} colorValue="#ab8b00" textColor="#f3e7b3" onClick={()=>this.chooseColor("#ab8b00","#f3e7b3")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#636330",color:"#ffffd4"}} colorValue="#636330" textColor="#ffffd4" onClick={()=>this.chooseColor("#636330","#ffffd4")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#64992c",color:"#f9ffef"}} colorValue="#64992c" textColor="#f9ffef" onClick={()=>this.chooseColor("#64992c","#f9ffef")} className="color-panel-box">a</div>
                        </Col>
                        <Col className="crm-color-panel-col" span={4}>
                            <div style={{backgroundColor:"#006633",color:"#f1f5ec"}} colorValue="#006633" textColor="#f1f5ec" onClick={()=>this.chooseColor("#006633","#f1f5ec")} className="color-panel-box">a</div>
                        </Col>
                    </Row>
                    <div ><Input style={{color:valuecolor,background:bgcolor}} value={data} /></div>
                    <div className="crm-color-panel-footer">
                        <Button type="primary" size="small" onClick={this.onSureColor.bind(this)}>确定</Button>
                        <Button type="ghost" size="small" onClick={this.onCancelColor.bind(this)}>取消</Button>
                    </div>
                 </div>
             </div>
        )
	}
    onSureColor(){
        const bgcolor= this.state.bgcolor;
        const valuecolor = this.state.valuecolor;
        this.props.sureColor(bgcolor,valuecolor);
    }
    onCancelColor(){
        this.props.cancelColor();
    }
    chooseColor=(bgcolor,valuecolor)=>{
        this.setState({bgcolor:bgcolor,valuecolor:valuecolor})
    }
}

export default ColorPanpel;