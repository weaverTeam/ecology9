import React, {Component} from 'react'
import {Modal, Button,message,Form,Table,Row,Col,Input,Checkbox,Icon,InputNumber,Popconfirm } from 'antd';
const InputGroup = Input.Group;
const FormItem = Form.Item;
import equal from 'deep-equal'
import {WeaDialog, WeaRightMenu,WeaSelect,WeaBrowser,WeaSelectGroup,WeaInput } from 'ecCom';
import WeaCustomerShareTableEdit from '../WeaCrmTableEdit/customerShare.js'


class AddCustomerShare extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            required:[],
            showGroup:true,
            selectsharetype:"2",
            shareleveltype:"1",
            roleleveltype:"0",
            datas:[]
        }
    }
    set visible(value) {
        this.setState({visible: value})
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !equal(nextProps.data,this.props.data ) || 
        !equal(nextState,this.state )||
        !equal(this.props.all,nextProps.all)

    }
    componentWillUnmount() {
		//组件卸载时一般清理一些状态
	}
    render() {
        const  {visible,showGroup,selectsharetype,shareleveltype,roleleveltype,datas} = this.state;
        const {all} = this.props;
        const {getFieldProps,getFieldsValue} = all.form;
        let type = [{key: '1', selected: true, showname: '人力资源'},
            {key: '2', selected: false, showname: '部门'},
            {key: '5', selected: false, showname: '分部'},
            {key: '3', selected: false, showname: '角色'},
            {key: '4', selected: false, showname: '所有人'},
            {key: '6', selected: false, showname: '岗位级别'}
        ];
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        let roletype =[
            {
                "key": "0",
                "selected": true,
                "showname": "部门"
            }, {
                "key": "1",
                "selected": false,
                "showname": "分部"
            }, {
                "key": "2",
                "selected": false,
                "showname": "总部"
            }
        ];
        let shareOptions =[
            {
                "key": "0",
                "selected": true,
                "showname": "全部"
            }, {
                "key": "1",
                "selected": false,
                "showname": "指定部门"
            }, {
                "key": "2",
                "selected": false,
                "showname": "指定分部"
            }
        ];
        const selectLinkageDatas ={
            "0":{
                "conditionType":"KONG",
                "domkey": [
                    ""
                ],
            },
            "1":{
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "conditionType": "BROWSER",
                "domkey": [
                    "jobtitledepartment"
                ],
                "browserType": "57",
                "fieldcol": 17,
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "多部门",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/hrm/company/HrmDepartmentDsp.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 3,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": false,
                    "type": "57",
                    "tabs": [
                        {
                            "selected": false,
                            "dataParams": {
                                "list": "1"
                            },
                            "name": "按列表",
                            "key": "1"
                        }, {
                            "selected": false,
                            "name": "按组织结构",
                            "key": "2"
                        }
                    ]
                }
            },
            "2":{
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "conditionType": "BROWSER",
                "domkey": [
                    "jobtitlesubcompany"
                ],
                "browserType": "164",
                "fieldcol": 17,
                "label": "多分部",
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "多分部",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/hrm/company/SubcompanyBrowser.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 3,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": false,
                    "type": "164",
                    "tabs": [
                        {
                            "selected": false,
                            "dataParams": {
                                "list": "1"
                            },
                            "name": "按列表",
                            "key": "1"
                        }, {
                            "selected": false,
                            "name": "按组织结构",
                            "key": "2"
                        }
                    ]
                }
            }
        };
        const columns=[
            {
                "title": "对象类型",
                "com": [ { label: '', type: 'INPUT' , "viewAttr": 1, key: 'sharetypespan', disabled: false, },],
                "width": "20%",
                "dataIndex": "sharetypespan",
                "key": "sharetypespan"
            },
            {
                "title": "对象",
                "com": [ { label: '', type: 'INPUT' , "viewAttr": 1, key: 'shareidspan', disabled: false, },],
                "width": "30%",
                "dataIndex": "shareidspan",
                "key": "shareidspan"
            },
            {
                "title": "安全级别",
                "com": [ { label: '', type: 'INPUT' , "viewAttr": 1, key: 'seclevelspan', disabled: false, },],
                "width": "30%",
                "dataIndex": "seclevelspan",
                "key": "seclevelspan"
            },
            {
                "title": "共享级别",
                "com": [ { label: '', type: 'INPUT' , "viewAttr": 1, key: 'sharelevelspan', disabled: false, },],
                "width": "20%",
                "dataIndex": "sharelevelspan",
                "key": "sharelevelspan"
            },
        ];
        return (
            <WeaDialog
                style={{width: 800,height:450}}
                visible={visible}
                title={'添加共享'}
                icon="icon-coms-crm"
                iconBgcolor="#96358a"
                buttons={this.getButtons()}
                onCancel={() => this.setVisible(false)}
            >   
                <div style={{width:"100%",height:"450px",overflow:"auto"}}>
                    <div className="wea-crm-table-edit">
                        <Row className="wea-title">
                            <Col span="20">
                                <div>基本信息</div>
                            </Col>
                            <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                                <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                            </Col>
                        </Row>
                        <Row className="wea-content" style={{display:showGroup ? "block":"none"} }>
                            <Col className="wea-form-cell" span={24}>
                                <Form horizontal style={{marginTop:"10px"}}>
                                    <FormItem
                                        {...formItemLayout}
                                        label="对象类型："
                                    >
                                        <WeaSelect  onChange={this.changeSelect.bind(this)} viewAttr={2} value={`${selectsharetype}`} options={type} />
                                    </FormItem>
                                    {selectsharetype && selectsharetype!='4'?
                                    <FormItem
                                        {...formItemLayout}
                                        label="对象："
                                    >
                                    {selectsharetype==1?
                                        <WeaBrowser {...getFieldProps('shareid')}  type={17} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==1?'block':'none',width:'100%'}} title="多人力资源"  onChange={this.hrmOnchange.bind(this)}/>
                                    :selectsharetype==2?
                                        <WeaBrowser {...getFieldProps('shareid')} type={57} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==2?'block':'none',width:'100%'}} linkUrl="/hrm/company/HrmDepartmentDsp.jsp?id=" title="多部门" tabs={[{key:'1',name:'按列表',selected:false,dataParams:{list:1}},{key:'2',name:'按组织架构',selected:false}]} isMultCheckbox={false} hasAdvanceSerach={true} hasAddBtn={false}/>
                                    :selectsharetype==5?
                                        <WeaBrowser {...getFieldProps('shareid')}  type={194} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==5?'block':'none',width:'100%'}} linkUrl="/hrm/company/HrmSubCompanyDsp.jsp?id=" title="多分部" tabs={[{key:'1',name:'按列表',selected:false,dataParams:{list:1}},{key:'2',name:'按组织架构',selected:false}]} isMultCheckbox={false} hasAdvanceSerach={true} hasAddBtn={false}/>
                                    :selectsharetype==3?
                                        <WeaBrowser {...getFieldProps('shareid')}  type={65} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==3?'block':'none',width:'100%'}} linkUrl="/hrm/roles/HrmRolesEdit.jsp?id=" title="多角色" />
                                    :selectsharetype==6?
                                        <WeaBrowser {...getFieldProps('shareid')} type={278} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==6?'block':'none',width:'100%'}} title="多岗位" />
                                    :''}
                                    </FormItem>
                                    :''}
                                    {selectsharetype && selectsharetype==3 ?
                                        <FormItem
                                        {...formItemLayout}
                                        label="级别："
                                        >
                                        <WeaSelect  onChange={this.rolelevelSelect.bind(this)} viewAttr={2} value={`${roleleveltype}`} options={roletype} />
                                        </FormItem>
                                        :''
                                    }
                                    {selectsharetype && selectsharetype!='1' && selectsharetype!='6'?
                                    <FormItem {...formItemLayout} label="安全级别：" style={{ marginTop: 24 }}>
                                        <InputGroup>
                                            <Col span="6">
                                                <InputNumber min={0} max={100} {...getFieldProps('seclevel', { initialValue: '10' })} />
                                            </Col>
                                            <Col span="2">
                                                  <p className="ant-form-split">-</p>
                                            </Col>
                                            <Col span="6">
                                                <InputNumber min={0} max={100} {...getFieldProps('seclevelMax', { initialValue: '100' })} />
                                            </Col>
                                        </InputGroup>
                                    </FormItem>
                                    :''}
                                    {selectsharetype && selectsharetype==6 ?
                                        <FormItem
                                        {...formItemLayout}
                                        label="级别："
                                        >
                                        <WeaSelectGroup
                                            {...getFieldProps("jobtitlelevel")}
                                            selectWidth={"100px"}
                                            options={shareOptions}
                                            selectLinkageDatas={selectLinkageDatas}
                                            form={all.form}
                                            viewAttr={2}
                                        />
                                        </FormItem>
                                        :''
                                    }
                                    <FormItem
                                    {...formItemLayout}
                                    label="共享级别："
                                >
                                    <WeaSelect  
                                        onChange={this.sharelevelSelect.bind(this)} 
                                        viewAttr={2} value={`${shareleveltype}`} 
                                        options={[{"key": "1","selected": true,"showname": "查看"}, {"key": "2","selected": false,"showname": "编辑"}]} />
                                </FormItem>
                                </Form>
                            </Col>
                        </Row>
                    </div>
                    <WeaCustomerShareTableEdit 
                        title={"共享信息"} 
                        showGroup={true} 
                        needAdd={true} 
                        needCopy={false}
                        columns={columns} 
                        datas={datas}
                        canAdd={true}
                        addColumns={this.addColumns.bind(this)}
                       onChange={this.editTableDatas.bind(this)}
                />
                </div>
            </WeaDialog>
        )
    }

    setVisible = (visible) => {
        this.setState({visible: visible})
    }
    hrmOnchange(ids,names,arr){
        const {actions} = this.props;
        const valuespan = (function(arr){
            let namespan= "";
            for(let i=0;i<arr.length;i++){
                namespan +=arr[i].name +",";
            }
            return namespan.slice(0,-1);
        })(arr);
        actions.saveOrderFields({shareid:{name:'shareid',value:ids,valueSpan:valuespan}})
    }
    changeSelect(value){
        const {actions} = this.props;
        actions.saveOrderFields({shareid:{name:'shareid',value:'',valueSpan:""}})
        this.setState({
            selectsharetype:value,
            roleleveltype:"0"
        })
    }
    sharelevelSelect(value){
        this.setState({
            shareleveltype:value
        })
    }
    rolelevelSelect(value){
        this.setState({
            roleleveltype:value
        })
    }
    getButtons(){
        const {operate} = this.props;
        let btn = [];
        btn.push( <Button type="primary"  onClick={this.doSave.bind(this)} >保存</Button>);
        btn.push(<Button onClick={() => this.setVisible(false)}>关闭</Button>);
        return btn;
    }
    addColumns(){
        const {selectsharetype,shareleveltype,roleleveltype,datas} =this.state;
        const {all,actions,customerIds} = this.props;
        const {validateFields,getFieldsValue} = all.form;
        const orderFields1 = all.orderFields.toJS();
        let params = {};
        let paramsAttr = [];
        const result = function(){
        if(selectsharetype != "4"){
            if(orderFields1.shareid&&orderFields1.shareid.value){
                if(selectsharetype == "1"){
                    if(orderFields1.shareid.value.indexOf(',')>=0){
                        let valueArr =  orderFields1.shareid.value.split(',');
                        let valueArrSpan =  orderFields1.shareid.valueSpan.split(',');
                        for(let i=0;i<valueArr.length;i++){
                             paramsAttr.push({
                                 sharetype:selectsharetype,
                                 sharetypespan:"人力资源",
                                 sharelevel:shareleveltype,
                                 sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                                 shareid:valueArr[i],
                                 shareidspan:valueArrSpan[i],
                                 seclevel:"10",
                                 seclevelMax:"100",
                                 seclevelspan:"",
                             });
                        }
                     }else{
                         paramsAttr.push({
                             sharetype:selectsharetype,
                             sharetypespan:"人力资源",
                             sharelevel:shareleveltype,
                             sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                             shareid:orderFields1.shareid.value,
                             shareidspan:orderFields1.shareid.valueSpan,
                             seclevel:"10",
                             seclevelMax:"100",
                             seclevelspan:"",
                         });
                     }
                }
                if(selectsharetype == "2" || selectsharetype == "5" ||selectsharetype == "3"){
                    if(orderFields1.shareid.value.indexOf(',')>=0){
                        let valueArr =  orderFields1.shareid.value.split(',');
                        let valueArrSpan =  orderFields1.shareid.valueSpan.split(',');
                        for(let i=0;i<valueArr.length;i++){
                             paramsAttr.push({
                                 sharetype:selectsharetype,
                                 sharetypespan:selectsharetype == "2" ?"部门":(selectsharetype == "3"?"角色":"分部"),
                                 sharelevel:shareleveltype,
                                 sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                                 shareid:valueArr[i],
                                 shareidspan:valueArrSpan[i],
                                 seclevel:orderFields1.seclevel&&orderFields1.seclevel.value+""||"10",
                                 seclevelMax:orderFields1.seclevelMax&&orderFields1.seclevelMax.value+""||"100",
                                 seclevelspan:(orderFields1.seclevel&&orderFields1.seclevel.value||10)+"-"+(orderFields1.seclevelMax&&orderFields1.seclevelMax.value||100),
                                 rolelevel:selectsharetype == "3"? roleleveltype:"0"
                             });
                        }
                     }else{
                         paramsAttr.push({
                            sharetype:selectsharetype,
                            sharetypespan:selectsharetype == "2" ?"部门":(selectsharetype == "3"?"角色":"分部"),
                            sharelevel:shareleveltype,
                            sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                            shareid:orderFields1.shareid.value,
                            shareidspan:orderFields1.shareid.valueSpan,
                            seclevel:orderFields1.seclevel&&orderFields1.seclevel.value+""||"10",
                            seclevelMax:orderFields1.seclevelMax&&orderFields1.seclevelMax.value+""||"100",
                            seclevelspan:(orderFields1.seclevel&&orderFields1.seclevel.value||10)+"-"+(orderFields1.seclevelMax&&orderFields1.seclevelMax.value||100),
                            rolelevel:selectsharetype == "3"? roleleveltype:"0"
                         });
                     }
                }
                if(selectsharetype == "6"){
                    if(orderFields1.shareid.value.indexOf(',')>=0){
                        let valueArr =  orderFields1.shareid.value.split(',');
                        let valueArrSpan =  orderFields1.shareid.valueSpan.split(',');
                        for(let i=0;i<valueArr.length;i++){
                            let obj={
                                sharetype:selectsharetype,
                                sharetypespan:"岗位级别",
                                sharelevel:shareleveltype,
                                sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                                shareid:valueArr[i],
                                shareidspan:valueArrSpan[i],
                                seclevel:'10',
                                seclevelMax:'100',
                                //seclevelspan:seclevel+"-"+seclevelMax,
                                jobtitlelevel:orderFields1.jobtitlelevel&&orderFields1.jobtitlelevel.value || "0",

                            }
                            if(orderFields1.jobtitlelevel.value=="1"){
                                obj.jobtitledepartment= orderFields1.jobtitlelevel.valueSpan[0] ;
                                obj.seclevelspan="指定部门("+orderFields1.jobtitlelevel.valueSpan[1]+")";
                            }
                            if(orderFields1.jobtitlelevel.value=="2"){
                                obj.jobtitlesubcompany= orderFields1.jobtitlelevel.valueSpan[0];
                                obj.seclevelspan="指定分部("+orderFields1.jobtitlelevel.valueSpan[1]+")";
                            }
                             paramsAttr.push(obj);
                        }
                     }else{
                        let obj={
                            sharetype:selectsharetype,
                            sharetypespan:"岗位级别",
                            sharelevel:shareleveltype,
                            sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                            shareid:orderFields1.shareid.value,
                            shareidspan:orderFields1.shareid.valueSpan,
                            seclevel:'10',
                            seclevelMax:'100',
                            jobtitlelevel:orderFields1.jobtitlelevel&&orderFields1.jobtitlelevel.value || "0",

                        }
                        if(orderFields1.jobtitlelevel.value=="1"){
                            obj.jobtitledepartment= orderFields1.jobtitlelevel.valueSpan[0] ;
                            obj.seclevelspan="指定部门("+orderFields1.jobtitlelevel.valueSpan[1]+")";
                        }
                        if(orderFields1.jobtitlelevel.value=="2"){
                            obj.jobtitlesubcompany= orderFields1.jobtitlelevel.valueSpan[0];
                            obj.seclevelspan="指定分部("+orderFields1.jobtitlelevel.valueSpan[1]+")";
                        }
                         paramsAttr.push(obj);
                     }
                } 
            }else{
                Modal.warning({
                    title: '系统提示',
                    content: '必填信息不完整!',
                });
            }
        }
        if(selectsharetype == "4"){
            paramsAttr.push({
                sharetype:selectsharetype,
                sharetypespan:"所有人",
                sharelevel:shareleveltype,
                shareid:"",
                shareidspan:"安全级别",
                sharelevelspan:shareleveltype == "1" ? "查看":"编辑",
                seclevel:orderFields1.seclevel&&orderFields1.seclevel.value||10,
                seclevelMax:orderFields1.seclevelMax&&orderFields1.seclevelMax.value||100,
                seclevelspan:(orderFields1.seclevel&&orderFields1.seclevel.value||10)+"-"+(orderFields1.seclevelMax&&orderFields1.seclevelMax.value||100),
            })
        }
        return paramsAttr
       }();
       
       this.setState({datas:[...datas,...result]})
    }
    editTableDatas(data){
        this.setState({datas:data})
    }
    doSave(){
        const {actions,customerIds} = this.props;
        const {datas} = this.state;
        actions.saveCustomerShare({customerIds:customerIds,info:JSON.stringify(datas)});
        this.setState({datas:[],visible:false})
    }
 
}

export default AddCustomerShare