import {Input,Icon,Modal,Button,Form,Row,Col,InputNumber} from 'antd';
const InputGroup = Input.Group;
// const confirm = Modal.confirm;
const FormItem = Form.Item;
import objectAssign from 'object-assign'
import {WeaDialog, WeaRightMenu,WeaSelect,WeaBrowser,WeaSelectGroup} from 'ecCom';

let _this = null;


class ShareSetting extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            visible: false,
            selectsharetype:"2",
            shareleveltype:"1"
        }
    }
    set visible(value) {
        _this.setState({visible: value})
    }
    componentWillReceiveProps(nextProps) {

    }

    render(){
        const {all} = this.props;
        const  {visible,selectsharetype,shareleveltype} = this.state;
        const {getFieldProps,getFieldsValue} = all.form;
        let type = [{key: '1', selected: true, showname: '人力资源'},
            {key: '2', selected: false, showname: '部门'},
            {key: '5', selected: false, showname: '分部'},
            {key: '3', selected: false, showname: '角色'},
            {key: '4', selected: false, showname: '所有人'},
            {key: '6', selected: false, showname: '岗位级别'}
        ];
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        let shareOptions =[
            {
                "key": "0",
                "selected": true,
                "showname": "全部"
            }, {
                "key": "1",
                "selected": false,
                "showname": "指定部门"
            }, {
                "key": "2",
                "selected": false,
                "showname": "指定分部"
            }
        ]
        const selectLinkageDatas ={
            "0":{
                "conditionType":"KONG",
                "domkey": [
                    ""
                ],
            },
            "1":{
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "conditionType": "BROWSER",
                "domkey": [
                    "jobtitledepartment"
                ],
                "browserType": "57",
                "fieldcol": 17,
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "多部门",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/hrm/company/HrmDepartmentDsp.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 3,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": false,
                    "type": "57",
                    "tabs": [
                        {
                            "selected": false,
                            "dataParams": {
                                "list": "1"
                            },
                            "name": "按列表",
                            "key": "1"
                        }, {
                            "selected": false,
                            "name": "按组织结构",
                            "key": "2"
                        }
                    ]
                }
            },
            "2":{
                "labelcol": 6,
                "colSpan": 2,
                "value": "",
                "conditionType": "BROWSER",
                "domkey": [
                    "jobtitlesubcompany"
                ],
                "browserType": "164",
                "fieldcol": 17,
                "label": "多分部",
                "browserConditionParam": {
                    "isAutoComplete": 1,
                    "isDetail": 0,
                    "title": "多分部",
                    "linkUrl": "/systeminfo/BrowserMain.jsp?url=/hrm/company/SubcompanyBrowser.jsp",
                    "isMultCheckbox": false,
                    "hasAdd": false,
                    "viewAttr": 3,
                    "dataParams": {},
                    "hasAdvanceSerach": true,
                    "isSingle": false,
                    "type": "164",
                    "tabs": [
                        {
                            "selected": false,
                            "dataParams": {
                                "list": "1"
                            },
                            "name": "按列表",
                            "key": "1"
                        }, {
                            "selected": false,
                            "name": "按组织结构",
                            "key": "2"
                        }
                    ]
                }
            }
        }
        return (
            <WeaDialog
                className="wea-crm-dialog" 
                style={{width: 500,height:250}}
                visible={visible}
                title={'添加共享'}
                icon="icon-coms-crm"
                iconBgcolor="#96358a"
                buttons={[
                    <Button type="primary" onClick={this.save.bind(this)} >保存</Button>
                ]}
                onCancel={() =>{ _this.setState({visible:false})}}
            >   
                <div style={{width:"100%",height:"250px",marginTop:'20px',overflow:"auto"}}>
                    <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                    <Row className="" style={{marginBottom:'10px'}}>
                    <Form horizontal>
                        <FormItem
                          {...formItemLayout}
                          label="对象类型："
                        >
                          <WeaSelect  onChange={this.changeSelect.bind(this)} viewAttr={2} value={`${selectsharetype}`} options={type} />
                        </FormItem>
                        {selectsharetype && selectsharetype!='4'?
                        <FormItem
                          {...formItemLayout}
                          label="对象："
                        >
                        {selectsharetype==1?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })}  type={17} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==1?'block':'none',width:'100%'}} title="多人力资源" />
                        :selectsharetype==2?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })} type={57} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==2?'block':'none',width:'100%'}} linkUrl="/hrm/company/HrmDepartmentDsp.jsp?id=" title="多部门" tabs={[{key:'1',name:'按列表',selected:false,dataParams:{list:1}},{key:'2',name:'按组织架构',selected:false}]} isMultCheckbox={false} hasAdvanceSerach={true} hasAddBtn={false}/>
                        :selectsharetype==5?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })}  type={194} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==5?'block':'none',width:'100%'}} linkUrl="/hrm/company/HrmSubCompanyDsp.jsp?id=" title="多分部" tabs={[{key:'1',name:'按列表',selected:false,dataParams:{list:1}},{key:'2',name:'按组织架构',selected:false}]} isMultCheckbox={false} hasAdvanceSerach={true} hasAddBtn={false}/>
                        :selectsharetype==3?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })}  type={65} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==3?'block':'none',width:'100%'}} linkUrl="/hrm/roles/HrmRolesEdit.jsp?id=" title="多角色" />
                        :selectsharetype==6?
                             <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })} type={278} isSingle={false} viewAttr={3} inputStyle = {{display:selectsharetype==6?'block':'none',width:'100%'}} title="多岗位" />
                        :''}
                        </FormItem>
                        :''}
                        {selectsharetype && selectsharetype!='1' && selectsharetype!='6'?
                        <FormItem {...formItemLayout} label="安全级别：" style={{ marginTop: 24 }}>
                            <InputGroup>
                                <Col span="7">
                                  <InputNumber min={0} max={100}  {...getFieldProps('seclevel', { initialValue: '10' })} />
                                </Col>
                                <Col span="2">
                                  <p className="ant-form-split">-</p>
                                </Col>
                                <Col span="7">
                                  <InputNumber min={0} max={100} {...getFieldProps('seclevelMax', { initialValue: '100' })} />
                                </Col>
                            </InputGroup>
                        </FormItem>
                        :''}
                        {selectsharetype && selectsharetype==6?
                            <FormItem
                              {...formItemLayout}
                              label="级别："
                            >
                            <WeaSelectGroup
                                {...getFieldProps("jobtitlelevel")}
                                selectWidth={"100px"}
                                options={shareOptions}
                                selectLinkageDatas={selectLinkageDatas}
                                form={all.form}
                                viewAttr={2}
                            />
                            </FormItem>
                            :''
                        }
                        <FormItem
                        {...formItemLayout}
                        label="共享级别："
                      >
                        <WeaSelect  
                            onChange={this.sharelevelSelect.bind(this)} 
                            viewAttr={2} value={`${shareleveltype}`} 
                            options={[{"key": "1","selected": true,"showname": "查看"}, {"key": "2","selected": false,"showname": "编辑"}]} />
                      </FormItem>
                    </Form>
                </Row>
                    </WeaRightMenu>
                </div>
            
            </WeaDialog>
        )
    }
    onRightMenuClick(key){
        const {actions,} = this.props;
       if(key == '0'){
           this.save();
       }
       if(key == '1'){
       }
       if(key == '2'){
       }
   }
   getRightMenu(){
       // const {comsWeaTable,sharearg,actions} = this.props;
       let btns = [];
       btns.push({
           icon: <i className='icon-coms-Preservation'/>,
           content:'保存',
           key:0
       });
       btns.push({
           icon: <i className='icon-coms-Collection2'/>,
           content:'收藏',
           key:1
       });
       btns.push({
           icon: <i className='icon-coms-help'/>,
           content:'帮助',
           key:2
       });
       return btns
   }
   changeSelect(value){
        const {actions} = this.props;
        actions.saveShareOrderFields({relatedshareid:{name:'relatedshareid',value:''}})
        this.setState({
            selectsharetype:value
        })
    }
    sharelevelSelect(value){
        this.setState({
            shareleveltype:value
        })
    }
    save(){
        const {selectsharetype,shareleveltype,} =this.state;
        const {all,actions,customerId} = this.props;
        const {validateFields,getFieldsValue} = all.form;
       const shareOrderFields1 = all.shareOrderFields.toJS();
       let params = {};
       const bool = function(){
            if(selectsharetype == "6"){
                params = {
                    customerId:customerId,
                    sharetype:selectsharetype,
                    relatedshareid:shareOrderFields1.relatedshareid&&shareOrderFields1.relatedshareid.value,
                    sharelevel:shareleveltype,
                    jobtitlelevel:shareOrderFields1.jobtitlelevel&&shareOrderFields1.jobtitlelevel.value || "0",
                }
                if(!shareOrderFields1.relatedshareid.value){
                    Modal.warning({
                        title: '系统提示',
                        content: '必填信息不完整!',
                    });
                    return false
                }
                if(params.jobtitlelevel!="0"){
                    if(shareOrderFields1.jobtitlelevel.valueSpan[0]){
                        if(shareOrderFields1.jobtitlelevel.value=="1"){
                            params.jobtitledepartment= shareOrderFields1.jobtitlelevel.valueSpan[0] 
                        }
                        if(shareOrderFields1.jobtitlelevel.value=="2"){
                            params.jobtitlesubcompany= shareOrderFields1.jobtitlelevel.valueSpan[0]
                        }
                    }else{
                        Modal.warning({
                            title: '系统提示',
                            content: '必填信息不完整!',
                        });
                        return false
                    }
                }
                return true;
            }else if(selectsharetype == "4"){
                params ={
                    customerId:customerId,
                    sharetype:selectsharetype,
                    sharelevel:shareleveltype,
                    seclevel:shareOrderFields1.seclevel&&shareOrderFields1.seclevel.value||10,
                    seclevelMax:shareOrderFields1.seclevelMax&&shareOrderFields1.seclevelMax.value||100,
                }
                return true;
            }else if(selectsharetype == "1"){
                params ={
                    customerId:customerId,
                    sharetype:selectsharetype,
                    sharelevel:shareleveltype,
                    relatedshareid:shareOrderFields1.relatedshareid&&shareOrderFields1.relatedshareid.value,
                }
                if(!shareOrderFields1.relatedshareid.value){
                    Modal.warning({
                        title: '系统提示',
                        content: '必填信息不完整!',
                    });
                    return false
                }
                return true;
            }else{
                params ={
                    customerId:customerId,
                    sharetype:selectsharetype,
                    relatedshareid:shareOrderFields1.relatedshareid&&shareOrderFields1.relatedshareid.value,
                    sharelevel:shareleveltype,
                    seclevel:shareOrderFields1.seclevel&&shareOrderFields1.seclevel.value||10,
                    seclevelMax:shareOrderFields1.seclevelMax&&shareOrderFields1.seclevelMax.value||100,
                }
                if (!params.relatedshareid) {
                    Modal.warning({
                        title: '系统提示',
                        content: '必填信息不完整!',
                    });
                    return false
                }
                return true;
            }
       }();
       if(bool){
           actions.saveShareSetting(params);
            this.setState({visible:false})
            all.form.resetFields();
       }
    }
}

export default ShareSetting;