import React, {Component} from 'react'
import {Modal, Button,message,Form,Table,Input,Checkbox,Icon,InputNumber,Popconfirm } from 'antd'
const FormItem = Form.Item;
import equal from 'deep-equal'
import CrmTools from '../crmTools'
import {
    WeaDialog,
    WeaRightMenu,
    WeaTop,
    WeaInput,
    WeaSearchGroup ,
    WeaTools,
} from 'ecCom';


class AddressManageModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            required:[]
        }
       
    }
    set visible(value) {
        this.setState({visible: value})
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !equal(nextProps.data,this.props.data ) || 
        !equal(nextState,this.state )||
        !equal(this.props.operate,nextProps.operate)


    }
    componentWillUnmount() {
		//组件卸载时一般清理一些状态
		this.setState({required:[]});
	}
    render() {
        const  {visible,} = this.state;
        const {operate} = this.props;
        return (
            <WeaDialog
                style={{width: 800,height:450}}
                visible={visible}
                title={`${operate==="add" ? '添加地址信息':'编辑地址信息'}`}
                icon="icon-coms-crm"
                iconBgcolor="#6d3cf7"
                buttons={this.getButtons()}
                onCancel={() => this.setVisible(false)}
            >   
                <div style={{width:"100%",height:"450px",overflow:"auto"}}>
                    <div >
                        <Form horizontal>
                            {operate == "add" && this.getConditions()}
                            {operate == "edit" && this.getEditForms()}
                       </Form>
                    </div>
                </div>
            
            </WeaDialog>
        )
    }

    setVisible = (visible) => {
        this.setState({visible: visible})
    }
    getConditions(){
        const {data,all} = this.props;
        let group = [];
        let viewObj = [];
		data && data.map(c =>{
			let items = [];
			c.items.map(field => {
                let fit = field.formItemType.toUpperCase();
                if(fit != "BROWSER" && field.viewAttr == "3"){
                    viewObj.push(field.domkey[0]);
                }else if(fit == "BROWSER"){
                    const bcp = field.browserConditionParam;
                    if(bcp.viewAttr == "3"){
                       viewObj.push(field.domkey[0]);
                    }  
                }
                items.push({
                    com:(<FormItem
                        label={`${field.label}`}
                        labelCol={{span: `${field.labelcol}`}}
                        wrapperCol={{span: `${field.fieldcol}`}}>
                            { WeaTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey, all, field)}
                        </FormItem>),
                    colSpan:2
                })
            });
			group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items}/>)
        });
        this.setState({required:viewObj})
		return group;
    }
    getEditForms(){
        const {data,all} = this.props;
        let group = [];
		data && data.map(c =>{
			let items = [];
			c.items.map(field => {
                items.push({
                    com:(<FormItem
                        label={`${field.label}`}
                        labelCol={{span: `${field.labelcol}`}}
                        wrapperCol={{span: `${field.fieldcol}`}}>
                            { CrmTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey, all, field,this.callBackFn)}
                        </FormItem>),
                    colSpan:2
                })
            });
			group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items}/>)
        });
		return group;
    }
    callBackFn=(field,value,id,name)=>{
       const {actions,addressId,all} = this.props;
       let fit = field.formItemType.toUpperCase();
       const fieldName=field.domkey[0];
       let obj = {};
       if(( fit === "INPUT" ||fit === "TEXTAREA") && value !== field.value){
            if( field.must && value == ""){
                return false;
            }else{
                const params ={
                    customerId:all.customerId,
                    addressId:addressId,
                    fieldname:field.domkey[0] , 
                    oldvalue:field.value,   
                    newvalue:value, 
                    fieldtype:fit
                };
                actions.saveEditAddress(params);
            }  
       }
       if(fit === "SELECT" || fit === "CHECKBOX" || fit === "BROWSER"){
             const params ={
                customerId:all.customerId,
                addressId:addressId,
                fieldname:field.domkey[0] , 
                oldvalue:field.value,   
                newvalue:value,
                fieldtype:fit  
            };
                actions.saveEditAddress(params);
       }
    }
    getButtons(){
        const {operate} = this.props;
        let btn = [];
        {
            operate=="add" && btn.push( <Button type="primary"  onClick={this.doSave.bind(this)} >保存</Button>);
        }
        btn.push(<Button onClick={() => this.setVisible(false)}>关闭</Button>);

        return btn;
    }
    doSave(){
        const {actions,all} = this.props;
        const {resetFields}=all.form;
        const {required} = this.state;
        const addressSearchParams = all.addressSearchParams.toJS();
        const require = (function(){
			for(let i=0;i<required.length;i++){
				if(!addressSearchParams[required[i]]){
					return false
				}
			 }
			return true;
        })();
        if(require){
            actions.saveAddressAddForm(all.customerId);
            this.setState({visible:false,required:[]});
            resetFields(); 
        }else{
			Modal.warning({
				title: '系统提示',
				content: '必填信息不完整!',
			});
		}
    }
}

export default AddressManageModal