import { Card,Pagination,Row,Col  } from 'antd';
import {WeaPopoverHrm} from 'ecCom'
import {is} from 'immutable'

class ViewLeaveMessage extends React.Component{
    constructor(props) {
		super(props);
        this.state={
            page:1 || props.page
        }
	}
    shouldComponentUpdate(nextProps,nextState) {
        if(!is(this.props.data, nextProps.data)){
            this.setState({page:1})
        }
        return  !is(this.props.data, nextProps.data)||
            !is(this.state.page,nextState.page);
    }
    render(){
        const {page} = this.state;
        const {data} = this.props;
        const cardDatas = data.toJS();
        return (
            <div className="crm-view-contacter">
                <WeaPopoverHrm />
                {this.getCards(page)}
                {
                    cardDatas.length>0 ?
                    <div ><Pagination defaultCurrent={1}  total={cardDatas.length} onChange={this.changePageSize}/></div>
                    :<div className="crm-no-datas-show">没有可显示的数据</div>
                }
            </div>
        )
    }
    getCards(page){
        const {data} = this.props;
        const cardDatas = data.toJS();
        let cards = [];
        cardDatas.map((item,index)=>{
            if(index>=(10*Number(page)-10) && index<= (10*Number(page)-1)){
                 cards.push( 
                <Card title={<Row>
                        <Col span={12} style={{overflow:"hidden",textOverflow:"ellipsis"}}>
                            <div className="card-right-hrm">
                                <span className="card-call" dangerouslySetInnerHTML={{__html:item.createNameLink}}></span>
                                <span className="card-call">{item.createdate} {item.createtime}</span>
                            </div>	
                        </Col>
                    </Row>} 
                    className="view-contacter-card">
                    <Row>
                       <div className="card-right">
                        <div className="feedbackrelate">
                            <div>{item.remark}</div>
                            {item.docids && <div className="relatetitle">相关文档:&nbsp;<span dangerouslySetInnerHTML={{__html:item.docNames}}></span></div>}
                        </div>
                    </div>
                    </Row>
                    <div className="clear"></div>
                </Card>)
            }
        });
        return cards;
    }
    changePageSize=(page)=>{
        this.setState({page:page})
    }
}


export default ViewLeaveMessage;