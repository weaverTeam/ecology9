
import { Card,Pagination ,Row,Col } from 'antd';
import {is} from 'immutable'

class ViewContacter extends React.Component{
    constructor(props) {
		super(props);
        this.state={
            page:1
        }
    }
    shouldComponentUpdate(nextProps,nextState) {
        if(!is(this.props.data, nextProps.data)){
            this.setState({page:1})
        }
        return  !is(this.props.data, nextProps.data)||
        !is(this.state.page,nextState.page);;
    }
    render(){
        const {page} = this.state;
        const {data} = this.props;
        const cardDatas = data.toJS();
        return (
            <div className="crm-view-contacter">
                {this.getCards(page)}
                {
                    cardDatas.length>0 ?
                    <div ><Pagination defaultCurrent={1}  total={cardDatas.length} onChange={this.changePageSize}/></div>
                    :<div className="crm-no-datas-show">没有可显示的数据</div>
                }
            </div>
        )
    }
    getCards(page){
        const {data} = this.props;
        const cardDatas = data.toJS();
        let cards = [];
        cardDatas.map((item,index)=>{
            if(index>=(10*Number(page)-10) && index<= (10*Number(page)-1)){
                 cards.push( 
                    <Card title={<Row>
                        <Col span={12} style={{overflow:"hidden",textOverflow:"ellipsis"}} ><span className="card-name" title={item.firstname} dangerouslySetInnerHTML={{__html:item.firstnameLink}}></span><span className="card-call">{item.titleName}</span></Col>
                        <Col span={12}><i className="icon-coms-crm" /><span className="card-call">{item.jobtitle}</span></Col>
                    </Row>} 
                     extra={<a href="#">More</a>} className="view-contacter-card">
                    <Row>
                        <Col span={12}>移动电话:{item.mobilephone}</Col>
                        <Col span={12}>电子邮箱:{item.email}</Col>
                    </Row>
                    <Row>
                        <Col span={12}>办公电话:{item.phoneoffice}</Col>
                        <Col span={12}>IM号码:{item.imcode}</Col>
                    </Row>
                    <div className="clear"></div>
                </Card>)
            }
        });
        return cards;
    }
    changePageSize=(page)=>{
        this.setState({page:page})
    }
}

export default ViewContacter;