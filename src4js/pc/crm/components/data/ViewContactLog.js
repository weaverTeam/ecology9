import { Card,Pagination  } from 'antd';
import {WeaPopoverHrm} from 'ecCom'
import {is} from 'immutable'

class ViewContactLog extends React.Component{
    constructor(props) {
		super(props);
        this.state={
            page:props.page || 1
        }
	}
    shouldComponentUpdate(nextProps,nextState) {
        if(!is(this.props.data, nextProps.data)){
            this.setState({page:1})
        }
        return  !is(this.props.data, nextProps.data)||
        !is(this.state.page,nextState.page);
    }
    render(){
        const {page} = this.state;
        const {data} = this.props;
        const cardDatas = data.toJS();
        return (
            <div className="crm-view-contactlog">
                <WeaPopoverHrm />
                {this.getCards(page)}
                {
                    cardDatas.length>0 ?
                    <div ><Pagination defaultCurrent={1}  total={cardDatas.length} onChange={this.changePageSize}/></div>
                    :<div className="crm-no-datas-show">没有可显示的数据</div>
                }
            </div>
        )
    }
    getCards(page){
        const {data} = this.props;
        const cardDatas = data.toJS();
        let cards = [];
        cardDatas.map((item,index)=>{
            if(index>=(10*Number(page)-10) && index<= (10*Number(page)-1)){
                 cards.push( <Card className="view-contactlog-card" key={index}>
                    <div className="card-left"><img src={item.createrImg} /></div>
                    <div className="card-right">
                        <div className="card-right-hrm"><span dangerouslySetInnerHTML={{__html:item.createrNameLink}}></span>&nbsp;&nbsp;{item.createdate} {item.createtime}</div>	
                        <div className="feedbackrelate">
                            <div>{item.description}</div>
                            {item.docid && <div className="relatetitle">相关文档:&nbsp;<span dangerouslySetInnerHTML={{__html:item.docName}}></span></div>}
                            {item.requestid && <div className="relatetitle">相关流程:&nbsp;<span dangerouslySetInnerHTML={{__html:item.requestName}}></span></div>}
                            {item.projectid && <div className="relatetitle">相关项目:&nbsp;<span dangerouslySetInnerHTML={{__html:item.projectName}}></span></div>}
                            {item.fileid && <div className="relatetitle">相关附件:&nbsp;<span dangerouslySetInnerHTML={{__html:item.fileName}}></span></div>}
                            {item.tempcontacterid && <div className="relatetitle">相关联系人:&nbsp;<span dangerouslySetInnerHTML={{__html:item.contacterName}}></span></div>}
                            {item.sellchanceid && <div className="relatetitle">相关商机:&nbsp;<span dangerouslySetInnerHTML={{__html:item.sellchancename}}></span></div>}
                        </div>
                    </div>
                    <div className="clear"></div>
                </Card>)
            }
        });
        return cards;
    }
    changePageSize=(page)=>{
        this.setState({page:page})
    }
}

window.openFullWindowHaveBar = function(routeUrl) {
    window.open(routeUrl);
}
export default ViewContactLog;