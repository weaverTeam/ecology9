import React, {Component} from 'react'
import {Modal, Button,message,Form,Table,Input,Checkbox,Icon,InputNumber,Popconfirm } from 'antd'
const FormItem = Form.Item;
import equal from 'deep-equal'
import {WeaDialog,
    WeaRightMenu,
    WeaTop,
    WeaInput,
    WeaSearchGroup ,
    WeaTools,
} from 'ecCom';


class AddContacterModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            required:[]
        }
       
    }
    set visible(value) {
        this.setState({visible: value})
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !equal(nextProps.addCondition,this.props.addCondition ) || !equal(nextState,this.state )

    }
    componentWillUnmount() {
		//组件卸载时一般清理一些状态
		this.setState({required:[]});
	}
    render() {
        const  {visible,} = this.state;
        return (
            <WeaDialog
                style={{width: 800,height:400}}
                visible={visible}
                title={'新建联系人'}
                icon="icon-coms-crm"
                iconBgcolor="#6d3cf7"
                buttons={[
                    <Button type="primary"  onClick={this.doSave} >保存</Button>,
                    <Button onClick={() => this.setVisible(false)}>关闭</Button>
                ]}
                onCancel={() => this.setVisible(false)}
            >   
                <div style={{width:"100%",height:"400px",overflow:"auto"}}>
                    <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                        <Form horizontal>
                             {this.getConditions()}
                       </Form>
                    </WeaRightMenu>
                </div>
            
            </WeaDialog>
        )
    }

    setVisible = (visible) => {
        this.setState({visible: visible})
    }

     onRightMenuClick(key){
    	 const {actions,} = this.props;
    	if(key == '0'){
            this.doSave();
    	}
    	if(key == '1'){
    	}
    	if(key == '2'){
    	}
    }
    getRightMenu(){
    	// const {comsWeaTable,sharearg,actions} = this.props;
    	let btns = [];
    	btns.push({
    		icon: <i className='icon-coms-Preservation'/>,
            content:'保存',
            key:1
        });
        btns.push({
    		icon: <i className='icon-coms-Preservation'/>,
            content:'还原',
            key:2
        });
        btns.push({
    		icon: <i className='icon-coms-Preservation'/>,
            content:'取消',
            key:3
    	});
        btns.push({
    		icon: <i className='icon-coms-Collection2'/>,
            content:'收藏',
            key:4
    	});
        btns.push({
            icon: <i className='icon-coms-help'/>,
            content:'帮助',
            key:5
        });
    	return btns
    }
    getConditions(){
        const {addCondition,all} = this.props;
        let group = [];
        let viewObj = [];
		addCondition.toJS().length>0 && addCondition.toJS().map(c =>{
			let items = [];
			c.items.map(field => {
                let fit = field.formItemType.toUpperCase();
                if(fit != "BROWSER" && field.viewAttr == "3"){
                    viewObj.push(field.domkey[0]);
                }else if(fit == "BROWSER"){
                    const bcp = field.browserConditionParam;
                    if(bcp.viewAttr == "3"){
                       viewObj.push(field.domkey[0]);
                    }  
                }
                items.push({
                    com:(<FormItem
                        label={`${field.label}`}
                        labelCol={{span: `${field.labelcol}`}}
                        wrapperCol={{span: `${field.fieldcol}`}}>
                            { WeaTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey, all, field)}
                        </FormItem>),
                    colSpan:2
                })
            });
			group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items}/>)
        });
        this.setState({required:viewObj})
		return group;
    }
    doSave=()=>{
        const {actions,all,customerId} = this.props;
        const {required}  = this.state;
        const _params = all.highSearchParamsAd.toJS();
        const require = (function(){
            for(let i=0;i<required.length;i++){
               if(!_params[required[i]]){
                   return false
               }
            }
           return true;
       })(); 
       if(require){
           actions.saveContacter({customerId:customerId,main:0,..._params});
           this.setState({visible:false,required:[]});
        }else{
            Modal.warning({
                title: '系统提示',
                content: '必填信息不完整!',
            });
        }
    }
}

export default AddContacterModal