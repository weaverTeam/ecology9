
import { Card,Pagination ,Row,Col ,Icon} from 'antd';
import {is} from 'immutable'

class ViewSellChance extends React.Component{
    constructor(props) {
		super(props);
        this.state={
            page:1
        }
    }
    shouldComponentUpdate(nextProps,nextState) {
        if(!is(this.props.data, nextProps.data)){
            this.setState({page:1})
        }
        return  !is(this.props.data, nextProps.data)||
        !is(this.state.page,nextState.page);;
    }
    render(){
        const {page} = this.state;
        const {data} = this.props;
        const cardDatas = data.toJS();
        return (
            <div className="crm-view-contacter">
                {this.getCards(page)}
                {
                    cardDatas.length>0 ?
                    <div ><Pagination defaultCurrent={1}  total={cardDatas.length} onChange={this.changePageSize}/></div>
                    :<div className="crm-no-datas-show">没有可显示的数据</div>
                }
            </div>
        )
    }
    getCards(page){
        const {data} = this.props;
        const cardDatas = data.toJS();
        let cards = [];
        cardDatas.map((item,index)=>{
            if(index>=(10*Number(page)-10) && index<= (10*Number(page)-1)){
                 cards.push( 
                    <Card title={<Row>
                        <Col span={12} style={{overflow:"hidden",textOverflow:"ellipsis"}} title={item.subject}><span className="card-name" dangerouslySetInnerHTML={{__html:item.subjectNameLink}}></span></Col>
                        <Col span={12} style={{textAlign:"right"}}><Icon type="pay-circle-o" /><span className="card-green-sign card-call">{item.preyield}</span><Icon type="clock-circle-o" /><span className="card-call">{item.createdate}</span></Col>
                    </Row>} 
                    className="view-contacter-card">
                    <Row>
                        <Col span={8}>销售预期:<span className="card-green-sign card-call">{item.predate}</span></Col>
                        <Col span={8}>可能性:<span className="card-green-sign card-call">{item.probability}</span></Col>
                        <Col span={8}>归档状态:<span className="card-green-sign card-call">{item.endtatusid}</span></Col>
                    </Row>
                    <Row>
                        <Col span={8}>销售状态:{item.sellstatus}</Col>
                        <Col span={8}>成功关键因素:{item.sufactor}</Col>
                        <Col span={8}>失败关键因素:{item.defactor}</Col>
                    </Row>
                    <div className="clear"></div>
                </Card>)
            }
        });
        return cards;
    }
    changePageSize=(page)=>{
        this.setState({page:page})
    }
}
export default ViewSellChance;