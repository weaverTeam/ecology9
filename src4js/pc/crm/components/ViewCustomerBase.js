 import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as ViewCustomerBaseAction from '../actions/viewCustomerBase'
import * as CustomerCardRightMenuAction from '../actions/rightMenu' 
import forEach from 'lodash/forEach'
import isEmpty from 'lodash/isEmpty'


import WeaRightLeftLayout from './weaCrmRightLeft'
import CustomerContact from './CustomerContact'  
import CrmTools from './crmTools';
import WeaCrmContacterTableEdit from './WeaCrmTableEdit/contacterEditTable.js'
import BusinessInfo from './customerCard/BusinessInfo'
import {WeaNewScroll  ,WeaSearchGroup,WeaRightMenu, WeaDialog,WeaBrowser,WeaReqTop,WeaUpload} from 'ecCom'

import {WeaErrorPage,WeaTools} from 'ecCom'
import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action

import {Button,Form,Modal,message,Icon,Row,Col,Input} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;

import cloneDeep from 'lodash/cloneDeep'
import objectAssign from 'object-assign'

import PropTypes from 'react-router/lib/PropTypes'
import Immutable from 'immutable'
const is = Immutable.is;

let _this = null;

class ViewCustomerBase extends React.Component {
    constructor(props) {
		super(props);
        _this = this;
        this.state={
            canAdd:true,
            showright:true,
            showGroup:true
        }
    }
    componentDidMount() {
        const {actions,location:{query}} = this.props;
        actions.setNowRouterCmpath('viewCustomerBase');
        actions.initDatas(query);
        actions.getContacterInfo({from:"all",...query});
    }
    componentWillReceiveProps(nextProps) {
         //  if(window.location.pathname.indexOf('/spa/crm/index') >= 0 && nextProps.title && document.title !== nextProps.title)
    		document.title = nextProps.title

    }
    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.conditioninfo,nextProps.conditioninfo)||
        !is(this.props.loading,nextProps.loading)||
        !is(this.props.rcList,nextProps.rcList)||
        !is(this.props.orderFields,nextProps.orderFields)||
        !is(this.props.contacterId,nextProps.contacterId)||
        !is(this.props.customerId,nextProps.customerId)||
        !is(this.props.contacterData,nextProps.contacterData)||
        !is(this.props.customerMermgeForm,nextProps.customerMermgeForm)||
        !is(this.props.showModal,nextProps.showModal)||
        !is(this.props.modalInfo,nextProps.modalInfo)||
        !is(this.state.canAdd,nextState.canAdd)||
        !is(this.state.showright,nextState.showright)||
        !is(this.state.showGroup,nextState.showGroup)||
        !is(this.props.businessInfoData,nextProps.businessInfoData)||
        !is(this.props.businessSelectKey,nextProps.businessSelectKey)||
        !is(this.props.businessTab,nextProps.businessTab)||
        !is(this.props.businessLogDataKey,nextProps.businessLogDataKey)||
        !is(this.props.comsWeaTable,nextProps.comsWeaTable)||
        !is(this.props.title,nextProps.title)
        
    }
    componentWillUnmount(){

    }
    render() {
        let that = this;
        const {loading,actions,title,conditioninfo,detailTabKey,customerId,contacterData,customerMermgeForm,showModal,modalInfo,businessInfoData,businessSelectKey,businessTab,
            businessLogDataKey,location,rightLevel} = this.props;
        const {canAdd,showright,} =this.state;
        const columns = [
            {
                "title": "姓名",
                "hasadd":true,
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr":rightLevel == 1 ? 1:3,
                    "length": 10,
                    "domkey": [
                        "firstname"
                    ],
                    "fieldcol": 17,
                    "label": "姓名"
                }],
                "width": "10%",
                "dataIndex": "firstname",
                "key": "firstname"
            },
            {
                "title": "称呼",
                "com": [{
                    "formItemType": "BROWSER",
                    "labelcol": 6,
                    "colSpan": 2,
                    "value": "",
                    "domkey": [
                        "title"
                    ],
                    "browserType": 38,
                    "fieldcol": 17,
                    "label": "称呼",
                    "browserConditionParam": {
                        "isAutoComplete": 1,
                        "isDetail": 0,
                        "title": "称呼",
                        "linkUrl": "/systeminfo/BrowserMain.jsp?url=/lgc/search/LgcProductBrowser.jsp",
                        "isMultCheckbox": false,
                        "hasAdd": false,
                        "viewAttr":rightLevel == 1 ? 1:3,
                        "dataParams": {},
                        "hasAdvanceSerach": true,
                        "isSingle": true,
                        "type": 59
                    }
                }],
                "width": "10%",
                "dataIndex": "callid",
                "key": "callid"
            },
            {
                "title": "岗位",
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel == 1 ? 1:3,
                    "length": 10,
                    "domkey": [
                        "jobtitle"
                    ],
                    "fieldcol": 17,
                    "label": "岗位"
                }],
                "width": "10%",
                "dataIndex": "jobtitle",
                "key": "jobtitle"
            },
            {
                "title": "办公电话",
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel,
                    "length": 10,
                    "domkey": [
                        "phoneoffice"
                    ],
                    "fieldcol": 17,
                    "label": "办公电话"
                }],
                "width": "10%",
                "dataIndex": "phoneoffice",
                "key": "phoneoffice"
            },
            {
                "title": "移动电话",
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel,
                    "length": 10,
                    "value":1,
                    "domkey": [
                        "mobilephone"
                    ],
                    "fieldcol": 17,
                    "label": "移动电话"
                }],
                "width": "10%",
                "dataIndex": "mobilephone",
                "key": "mobilephone"
            },
            {
                "title": "邮箱",
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel,
                    "length": 10,
                    "domkey": [
                        "email"
                    ],
                    "fieldcol": 17,
                    "label": "邮箱"
                }],
                "width": "10%",
                "dataIndex": "email",
                "key": "email"
            },
            {
                "title": "IM号码",
                "hasIcon":true,
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel,
                    "length": 10,
                    "domkey": [
                        "imcode"
                    ],
                    "fieldcol": 15,
                    "label": "IM号码"
                }],
                "width": "10%",
                "dataIndex": "imcode",
                "key": "imcode"
            },
            {
                "title": "意向判断",
                "com": [{
                    "formItemType": "SELECT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel,
                    "length": 10,
                    "domkey": [
                        "attitude"
                    ],
                    "fieldcol": 17,
                    "label": "意向判断",
                    "options": [
						{
							"key": "",
							"selected": true,
							"showname": ""
						}, {
							"key": "支持我方",
							"selected": false,
							"showname": "支持我方"
						}, {
							"key": "未表态",
							"selected": false,
							"showname": "未表态"
						}, {
							"key": "未反对",
							"selected": false,
							"showname": "未反对"
						}, {
							"key": "反对",
							"selected": false,
							"showname": "反对"
						}
					]
                }],
                "width": "10%",
                "dataIndex": "attitude",
                "key": "attitude"
            },
            {
                "title": "关键点",
                "com": [{
                    "formItemType": "INPUT",
                    "labelcol": 6,
                    "colSpan": 2,
                    "viewAttr": rightLevel,
                    "length": 10,
                    "domkey": [
                        "attention"
                    ],
                    "fieldcol": 17,
                    "label": "关键点"
                }],
                "width": "15%",
                "dataIndex": "attention",
                "key": "attention"
            },
            {
                "title": "",
                "com": [
                    <Button type="primary" title='保存' size="small" onClick={this.doSaveContracter}><Icon type="save" /></Button>,
                    <Button type="primary" title='删除' size="small"  onClick={this.doDelete}><Icon type="minus" /></Button>
                ],
                "width": "5%",
                "dataIndex": "opertion",
                "key": "opertion",
                className:"table-edit-opertion"
            }
        ];
    
        return (
            <div className="height100">
                <WeaRightLeftLayout defaultShowLeft={showright} leftCom={
                    <WeaNewScroll height={"100%"} children={<WeaRightMenu width={"auto"} datas={this.getRightMenuBase()} onClick={this.onRightMenuClickBase.bind(this)}>
                        <Form horizontal>{this.getSearchs()}</Form>
                        <div>
                            <WeaCrmContacterTableEdit 
                                title={"联系人"} 
                                showGroup={true} 
                                needAdd={rightLevel ==1 ? false :true} 
                                needCopy={false}
                                columns={columns} 
                                datas={contacterData.toJS()}
                                canAdd={canAdd}
                                onChange={this.onChange.bind(this)}
                                onAddChange={this.onAddChange.bind(this)}
                                onEditChange={this.onEditChange.bind(this)}
                                showRightContactlog={this.showRightContactlog.bind(this)}
                            />
                        </div>
                    </WeaRightMenu>}  />
                } >
                    <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                        <div className="customer-contact-title">客户联系</div>
                        <div className="customer-contact-bottomline"></div>
                        <WeaNewScroll height={"100%"} children={<CustomerContact key={new Date().getTime()} ref="contactlogs" params={{from:"base",customerId:customerId}} location={location}/> } />
                    </WeaRightMenu>
                </WeaRightLeftLayout>
                <WeaDialog 
                    title={modalInfo.get('modalTitle')} icon="icon-coms-crm" iconBgcolor="#6d3cf7" 
                    visible={showModal} style={modalInfo.get('modalStyle').toJS()} 
                    buttons={ modalInfo.get('modalType') !== "business" && [
                        <Button type="primary"  onClick={()=>{this.doSave(modalInfo.get('modalType'))}} >保存</Button>,
                        <Button onClick={() => {actions.showDilog(false)}}>关闭</Button>
                    ]} 
                    onCancel={() => {actions.showDilog(false)}}>	
                    {
                        modalInfo.get('modalType') == "customerMerge" && 
                        <Form horizontal>
                           {this.getCustomerMerge()} 
                        </Form>
                    }
                    {
                       ( modalInfo.get('modalType') == "applyPortal" || modalInfo.get('modalType') == "setPasswd")&& 
                        <div>
                           {this.getApplyPortal()} 
                        </div>
                    }
                    {
                        modalInfo.get('modalType') == "business" && 
                      <div>  
                        <WeaReqTop 
                            title={"工商信息"}
                            loading={true}
                            icon={<i className='icon-coms-crm' />}
                            iconBgcolor='#55D2D4'
                            buttons={[]}
                            buttonSpace={10}
                            showDropIcon={false}
                            tabDatas={businessTab.toJS()}
                            selectedKey={businessSelectKey}
                            onChange={this.businessChangeData.bind(this)}
                        >
                        </WeaReqTop>
                        <div style={{height:670,overflow:"auto"}}>
                            {
                                businessSelectKey == "1"&&
                                <BusinessInfo  data={businessInfoData.toJS()} />
                            }
                            {
                                businessSelectKey == "2" &&
                                <WeaTable
                                    sessionkey={businessLogDataKey}
                                    hasOrder={true}
                                    needScroll={false}
                                />
                            }
                        </div>
                    </div>
                    }
                </WeaDialog>
            </div>
        )
    }
    getRightMenuBase(){
    	 const {rcList,actions} = this.props;
    	let btns = [];
        rcList.toJS().map(item=>{
            let _icon="icon-coms-Need-feedback"
            if(item.key == "2"){   //导出客户联系
                _icon="icon-coms-export "
            }else if(item.key == "3"){  //新建邮件
                _icon="icon-coms-Send-emails"
            }else if(item.key == "39"){  //新建工作流
                _icon="icon-coms-Workflow "
            }else if(item.key == "40"){  //新建协作事件
                _icon="icon-coms-synergism  "
            }else if(item.key == "41"){  //新建计划
                _icon="icon-coms-Planning-tasks  "
            }else if(item.key == "42"){  //客户合并
                _icon="icon-coms-HumanResources"
            }
            return  btns.push({
                icon: <i className={_icon}/>,
                content:`${item.content}`,
                key:`${item.key}`,
                onClick:key =>{eval("actions." + `${item.fn}`);}
            });
        })
       
    	return btns
    }  
    onRightMenuClickBase(key){
        const {actions,customerId} = this.props;
        if(key == '2'){  //导出客户联系
           actions.contactLogExport(customerId);
       }
       if(key == '3'){  //新建邮件
           actions.addMailMenu(customerId);
       }
       if(key == '39'){  //新建工作流
         //actions.addMailMenu(customerId);
       }
       if(key == '40'){  //新建协作事件
        //actions.addMailMenu(customerId);
      }
      if(key == '41'){  //新建计划
        //actions.addMailMenu(customerId);
      }
      if(key == '44'){  //客户合并
        const modalInfo={
            modalTitle:"客户合并",
            modalStyle:{width: 446, height: 180},
            modalType:"customerMerge"
        };
        actions.getUniteForm(customerId);
        actions.setModalBaseInfo(modalInfo);
        actions.showDilog(true);
      }
      if(key == '45'){  //修改密码
        const modalInfo={
            modalTitle:"设置密码",
            modalStyle:{width: 446, height: 240},
            modalType:"setPasswd"
        };
        actions.setModalBaseInfo(modalInfo);
        actions.showDilog(true);
      }
      if(key == '4'){  //删除
        actions.deleteCustomer(customerId);
      }
      if(key == '46'){  //工商信息
        actions.getBusinessInfo(customerId);
       
      }
   }
    onRightMenuClick(key){
    	 const {actions,comsWeaTable,dataKey} = this.props;
    	
    }
    getRightMenu(){
    	let btns = [];
        btns.push({
            icon: <i className='icon-Right-menu-batch'/>,
            content:'收藏',
        });
    	btns.push({
            icon: <i className='icon-Right-menu-batch'/>,
            content:'帮助',
        });
    	return btns
    }  
    getSearchs() {
    	const { conditioninfo,rightLevel } = this.props;
		let group = [];
		conditioninfo.toJS().map(c =>{
			let items = [];
			c.items.map(field => {
                let fit = field.formItemType.toUpperCase();
                if(fit== "ATTACHEMENT"){
                    items.push({
                        com:(<FormItem
                            label={`${field.label}`}
                            labelCol={{span: `${field.labelcol}`}}
                            wrapperCol={{span: `${field.fieldcol}`}}>
                            <WeaUpload
                                uploadId={field.domkey[0]}
                                uploadUrl="/api/crm/common/fileUpload"
                                category="0,0,0"
                                autoUpload={true}
                                showBatchLoad={false}
                                showClearAll={false}
                                multiSelection={true}
                                viewAttr={rightLevel}
                                datas={field.datas}
                                onChange={(ids,lists) => {this.onUploadChange(ids,lists,field)}}
                                onUploading={this.uploadState}
                            />
                            </FormItem>),
                        colSpan:1
                    })
                }else{
                    items.push({
                        com:(<FormItem
                            label={`${field.label}`}
                            labelCol={{span: `${field.labelcol}`}}
                            wrapperCol={{span: `${field.fieldcol}`}}>
                                {CrmTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey, this.props, field,this.callback)}
                            </FormItem>),
                        colSpan:1
                    })
                }
			});
			group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items}/>)
		});
		return group
    }
    callback=(field,value,id,name)=>{
       const {customerId,actions } = this.props;
       let fit = field.formItemType.toUpperCase();
       const fieldName=field.domkey[0];
       let obj = {};
       if(( fit === "INPUT" ||fit === "TEXTAREA") && value !== field.value){
            if( field.viewAttr== 3 && value == ""){
                return false;
            }else{
                const params ={
                    customerId:customerId,
                    fieldName:field.domkey[0] , 
                    oldValue:field.value,   
                    newValue:value,    
                    fieldType:fit
                };
               actions.customerEdit(params);
            }  
       }
       if(fit === "SELECT" || fit === "CHECKBOX" || fit === "BROWSER" || fit === "DATEPICKER"){
             const params ={
                customerId:customerId,
                fieldName:field.domkey[0] , 
                oldValue:field.value,   
                newValue:value,    
                fieldType:fit
            };
                actions.customerEdit(params);
       }
    }
    onUploadChange(ids,lists,field){
        const {actions,customerId}  = this.props;
        const params ={
            customerId:customerId,
            fieldName:field.domkey[0] , 
            oldValue:"",   
            newValue: `${ids}`,    
            fieldType:field.formItemType
        };
           actions.customerEdit(params);
    }
    uploadState(state){
        // if(state=="uploaded")
        //     message.error("操作成功！");
    }
    onChange(data){
        const {actions} = this.props;
       actions.updateContacterData(data);
    }
    doDelete=()=>{
        const {dataSources,canAdd} = this.state;
        const {actions,contacterData} = this.props;
        this.setState({canAdd:true})
        actions.updateContacterData(contacterData.toJS().slice(0,-1));
    }
    onAddChange=()=>{
        this.setState({canAdd:false})
    }
    onEditChange=(field,oldvalue,key,value,index)=>{
        let {actions,customerId,contacterData} = this.props;
        let params={
            id:field.contacterid,
            customerId:customerId
        }
        params[key] = value;
        if(key == "firstname" && value == ""){
            return false
        }
        if(key == "title" && value == ""){
            return false
        }
        if(key == "jobtitle" && value == ""){
            return false
        }
        if(value !== oldvalue){
            actions.editContracterData(params);
        }
    }
    doSaveContracter=()=>{
        const {actions,contacterData,customerId} = this.props;
        let params = contacterData.toJS().slice(-1)[0];
        params.customerId=customerId
        if(params.firstname && params.title &&params.jobtitle){
            actions.editContracterData(params);
            this.setState({canAdd:true});
        }else{
            Modal.warning({
				title: '系统提示',
				content: '必填信息不完整!',
			});
        }
         
    }
    showRightContactlog(bool,obj){
        //console.log(bool,obj.contacterid)
        this.setState({showright:bool})
    }
    getCustomerMerge(){
        const {customerMermgeForm,actions} = this.props;
        let group = [];
       
		customerMermgeForm.toJS().map(c =>{
			let items = [];
			c.items.map(field => {
                    items.push({
                        com:(<FormItem
                            label={`${field.label}`}
                            labelCol={{span: `${field.labelcol}`}}
                            wrapperCol={{span: `${field.fieldcol}`}}>
                                { WeaTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey, this.props, field)}
                            </FormItem>),
                        colSpan:1
                    })
                });
			group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items} col={1} />)
        });
       
		return group
    }
    getApplyPortal(){
        const {getFieldProps} = this.props.form;
        const {orderFields} = this.props;
        const {showGroup} = this.state
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        const passwdProps = getFieldProps('passwd', {
            rules: [
              { required: true, whitespace: true, min: 6,max:20, message: '用户名为 6-20 个数字或者字母'  },
              { validator: this.checkPasswd },
            ],
          });
        const rePasswdProps = getFieldProps('rePasswd', {
            rules: [{
              required: true,
              whitespace: true,
              message: '请再次输入密码',
            }, {
              validator: this.checkRePasswd,
            }],
          });
        return (
        <div className="wea-crm-table-edit">
            <Row className="wea-title">
                <Col span="20">
                    <div>基本信息</div>
                </Col>
                <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                    <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
                </Col>
            </Row>
            <Row className="wea-content" style={{display:showGroup ? "block":"none"} }>
                <Col className="wea-form-cell" span={24}>
                    <Form horizontal style={{marginTop:"10px"}}>
                        <FormItem
                            {...formItemLayout}
                            label="密码"
                            hasFeedback
                        >
                        <Input {...passwdProps} type="password" autoComplete="off"
                        />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="确认密码"
                            hasFeedback
                        >
                            <Input {...rePasswdProps} type="password" autoComplete="off" placeholder="两次输入密码保持一致"
                        />
                        </FormItem>
                    </Form>
                </Col>
                <Col span="24">
                    <div style={{paddingLeft:"20px"}}><span style={{color:"#f50",marginRight:"2px"}}>*</span>注：1、密码只能输入数字或字母，不能输入其它符号或文字;</div>
                    <div style={{paddingLeft:"52px"}}>2、密码最少输入6位，最多20位。</div>
                </Col>
            </Row>
        </div>
        )
    }
    noop=()=>{
        return true
    }
    checkPasswd=(rule, value, callback)=> {
        const { validateFields } = this.props.form;
        var reName = new RegExp(/^[A-Za-z0-9]*$/g);
        if (!reName.test(value)) {
           callback("只能为 6-20 个数字或者字母");
        }
        callback();
      }
    
    checkRePasswd=(rule, value, callback)=> {
        const { getFieldValue } = this.props.form;
        if (value && value !== getFieldValue('passwd')) {
          callback('两次输入密码不一致！');
        } else {
          callback();
        }
      }
    doSave=(type)=>{
        const {actions,customerId} = this.props;
        if(type=="customerMerge"){
            actions.customerMergeSave();
        }
        if(type=="applyPortal"){
            this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  return;
                }
                actions.applyPortalSave({customerId:customerId,method:"updatePassword",passwordnew:values.passwd});
                this.props.form.resetFields();
              });
        }
        if(type=="setPasswd"){
            this.props.form.validateFields((errors, values) => {
                if (!!errors) {
                  return;
                }
                actions.setPasswdSave({customerId:customerId,method:"updatePassword",passwordnew:values.passwd});
                this.props.form.resetFields();
              });
        }
    }
    businessChangeData(key){   //查看工商信息的信息
        const {actions,customerId} = this.props;
        actions.businessChangeTab(key,customerId);
    }
}

class MyErrorHandler extends React.Component {
    render(){
        const hasErrorMsg = this.props.error && this.props.error!=="";
        return (
            <WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
        );
    }
}

ViewCustomerBase = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(ViewCustomerBase);

ViewCustomerBase = createForm({   //当页面是编辑的时候，不绑定redux
	onFieldsChange(props, fields) {
        props.actions.saveOrderFields({...props.orderFields.toJS(), ...fields});
    },
	mapPropsToFields(props) {
		return props.orderFields.toJS();
  	}
})(ViewCustomerBase);

function mapStateToProps(state) {
    // return {...state.crmContactQuery}
	const {viewCustomerBase,comsWeaTable} = state;
    return {
    	loading: viewCustomerBase.get('loading'),
        conditioninfo: viewCustomerBase.get('conditioninfo'),
        rcList: viewCustomerBase.get('rcList'),
        orderFields:viewCustomerBase.get('orderFields'),
        contacterId :viewCustomerBase.get('contacterId'),
        customerId:viewCustomerBase.get('customerId'),
        contacterData:viewCustomerBase.get('contacterData'),
        customerMermgeForm:viewCustomerBase.get('customerMermgeForm'),
        showModal:viewCustomerBase.get('showModal'),
        modalInfo:viewCustomerBase.get('modalInfo'),
        businessInfoData:viewCustomerBase.get('businessInfoData'),
        businessSelectKey:viewCustomerBase.get('businessSelectKey'),
        businessTab:viewCustomerBase.get('businessTab'),
        businessLogDataKey:viewCustomerBase.get('businessLogDataKey'),
        title:viewCustomerBase.get('title'),
        comsWeaTable: comsWeaTable, //绑定整个table
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...ViewCustomerBaseAction,...CustomerCardRightMenuAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewCustomerBase);
