import { Row, Col,Spin} from 'antd';
import {WeaUpload,WeaBrowser,} from 'ecCom';
import {is} from "immutable"

export default class AddBatch extends React.Component {
    shouldComponentUpdate(nextProps) {
        return !is(this.props.initData,nextProps.initData);
    }
    render() {
        const {initData,actions,params} = this.props;
        const {getFieldProps} = params.form;
        const initDataInfo = initData.toJS();
        const tipInfoList = initDataInfo.tipInfoList;
        let lis = [];
        if(tipInfoList){
            tipInfoList.map((info, index) => {
                lis.push((<li style={{lineHeight:2}}>{info}</li>));
            });
        }
        if(!initDataInfo.tipInfoList){
            return <div className="align-center top40">
            <Spin size="large"></Spin>
        </div>
        }else{
            return (
                <div>
                    <iframe id="downFile" name="downFile" style={{display:"none"}}/>
                    <Row className="crm-add-row">
                        <Col span="24" className="crm-add-batch-title">客户导入</Col>
                    </Row>
                    <Row className="crm-add-row">
                        <Col span="2">Excel文件</Col>
                        <Col span="22" className="crm-add-input">
                        { <WeaUpload 
                            uploadId="file"
                                uploadUrl="/api/crm/common/fileUpload"
                                category="0,0,0"
                                autoUpload={false}
                                showBatchLoad={false}
                                showClearAll={false}
                                multiSelection={false}
                                datas={[]}
                                viewAttr={3}
                                onChange={this.fileUploadBack}
                                onUploading={(state)=>this.props.hsaUploadState(state)}
                        />
                        }
                        </Col>
                    </Row>
                    <Row className="crm-add-row">
                        <Col span="2">客户经理</Col>
                        <Col span="22" className="crm-add-input">
                            <WeaBrowser type={1} title={"人员"} {...getFieldProps(initDataInfo["manager"].domkey[0])}  {...initDataInfo["manager"].browserConditionParam} />
                        </Col>
                    </Row>
                    <Row className="crm-add-row">
                        <Col span="2">客户状态</Col>
                        <Col span="22" className="crm-add-input">
                            <WeaBrowser type={264} title={"客户状态"} {...getFieldProps(initDataInfo["customerStatus"].domkey[0])} {...initDataInfo["customerStatus"].browserConditionParam} />
                        </Col>
                    </Row>
            
                    <Row className="crm-add-row">
                        <Col span="24" className="crm-add-batch-title">说明</Col>
                    </Row>
                    <Row className="crm-add-row">
                        <Col span="24" className="crm-add-batch-title">
                            <div style={{lineHeight:2}}><strong>导入步骤：</strong></div>
                            <div style={{paddingLeft:43}}>
                                <ul>
                                    <li style={{lineHeight:2}}>第一步，请先<a href="/CRM/ExcelToDB.xls"  style={{color:"blue !important",textDecoration:"underline !important"}}>下载EXCEL文档模版</a>，<span style={{color:"red"}}>调整模板请到"后端应用中心-客户-基本信息字段设置"调整导入字段后保存</span>。</li>
                                    <li style={{lineHeight:2}}>第二步，下载后，填写内容，注意，要填写的内容在下边的说明中有详细的说明，请一定要确定你的Excel文档的格式是模板中的格式，而没有被修改掉。</li>
                                    <li style={{lineHeight:2}}>第三步，选择客户经理，默认情况下，客户经理是客户导入者本人。</li>
                                    <li style={{lineHeight:2}}>第四步，可以选择被导入的客户的状态，如果不进行选择，默认情况下导入的客户是无效客户。</li>
                                    <li style={{lineHeight:2}}>第五步，点击右键的提交，进入客户的导入。</li>
                                    <li style={{lineHeight:2}}>第六步，如果以上步骤和Excel文件正确的话，则会被正确的导入，也会出现提示。如果有问题，则根据提示下载Excel并对错误之处进行修改。</li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                    <Row className="crm-add-row">
                        <Col span="24" className="crm-add-batch-title">
                            <div style={{lineHeight:2}}><strong>确认具有完全控制权限者将不能共享此目录下的文档吗？:</strong></div>
                            <div style={{paddingLeft:43}}>
                                <ul>
                                    <li style={{lineHeight:2}}>客户导入不会判断客户是否重复，如果数据库中有相同名字的客户，则不会给出任何提示。</li>
                                    <li style={{lineHeight:2}}>如果导入成功客户以上的客户(包括成功客户),请必须填写客户类型。</li>
                                    <li style={{lineHeight:2}}>建议一次性导入的客户不要太多，控制在100以内。</li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                    <Row className="crm-add-row">
                        <div style={{lineHeight:2}}><strong>按照EXCEL文档中各字段的顺序填写数据，其中红色的部分为必填字段，不能为空！</strong></div>
                    </Row>
                    <Row className="crm-add-row">
                        <Col span="12">
                            <div style={{color:"#FF0000"}}>
                                <div style={{lineHeight:2}}><strong>必填</strong></div>
                                <div style={{paddingLeft:43}}>
                                    <ul style={{listStyleType:"disc"}}>
                                        {lis}
                                    </ul>
                                </div>
                            </div>
                        </Col>
                        <Col span="12" className="crm-add-input" >
                            <div style={{lineHeight:2}}><strong>错误类型</strong></div>
                            <div style={{paddingLeft:43}}>
                                <ul style={{listStyleType:"disc"}}>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"red",display:"inline",color:"red"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>数据不允许为为空</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"#33CCCC",display:"inline",color:"#33CCCC"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>浏览框输入的信息不存在</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"#FF9900",display:"inline",color:"#FF9900"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>数据必须为整数</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"#99CC00",display:"inline",color:"#99CC00"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>数据必须为小数或者整数</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"#CC99FF",display:"inline",color:"#CC99FF"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>输入的文字过长</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"yellow",display:"inline",color:"yellow"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>时间输入有误</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"#a1a3a6",display:"inline",color:"#a1a3a6"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>下拉框值有误</span>
                                    </li>
                                    <li style={{lineHeight:2}}>
                                        <div style={{backgroundColor:"#FF8080",display:"inline",color:"#FF8080"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        <span style={{paddingLeft:5}}>判断值必须填【是】或【否】</span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </div>
            )
        }
    }
    fileUploadBack=(ids)=>{
        // console.log(ids)
        const {actions} = this.props;
       actions.startImportFile({fileid:`${ids}`});
    }
    
}