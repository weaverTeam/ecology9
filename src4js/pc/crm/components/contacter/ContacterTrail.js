import {Timeline,Icon} from "antd"
import {is } from 'immutable'
import {WeaPopoverHrm } from "ecCom"

class ContacterTrail extends React.Component{

    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.data,nextProps.data)
    }
    render(){
        const {data} = this.props;
        console.log(data.toJS())
        return (
            <div className="contacter-trail-container">
                <WeaPopoverHrm />
                <Timeline>
                    {this.getTimeLine()}
                </Timeline>
            </div>
        )
    }
    getTimeLine(){
        const {data} = this.props;
        const datas = data.toJS();
        let items = [];
        datas && datas.map(line=>{
            items.push(
                <Timeline.Item dot={<Icon type="clock-circle-o" className="contacter-trail-dot" style={{color:"#028102"}}/>}>
                    <div className="contacter-trail-customer" ><span style={{color:"#1cbeec"}} dangerouslySetInnerHTML={{__html:line.hrmLink}}></span> <span style={{color:"#939393",margin:"0 5px"}}>于</span><span style={{color:"#1cbeec"}}>{line.contactTime}</span> <span style={{color:"#939393",margin:"0 5px"}}>最后联系</span></div>
                </Timeline.Item>
            )
        })
        return  items
    }
}

export default ContacterTrail;