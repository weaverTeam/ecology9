import {Table,Icon,Row,Col,Button,Spin} from 'antd'
import {WeaTools,WeaInput,WeaSelect,WeaBrowser} from 'ecCom'
import equal from 'deep-equal'

const { getComponent } = WeaTools;

class CustomerShare extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			columns: [],
			datas: [],
			selectedRowKeys: [],
			current: 1,
			showGroup: props.showGroup ? props.showGroup : false
		}
		this.doDelete = this.doDelete.bind(this);
		this.doAdd = this.doAdd.bind(this);
	}
	componentDidMount() {
		const { datas = [], columns = [] } = this.props;
		columns.length > 0 && this.setState(datas.length > 0 ? {datas: this.addKeytoDatas(datas),columns} : {columns})
	}
	componentWillReceiveProps(nextProps,nextState) {
		const { columns = [], datas = [], selectedRowKeys = [] } = this.props;
		const _columns = nextProps.columns || [];
		const _datas = nextProps.datas || [];
		const _selectedRowKeys = nextProps.selectedRowKeys || [];
		!equal(columns,_columns) && this.setState({columns});
		!equal(datas,_datas) && this.setState({datas: this.addKeytoDatas(_datas)});
		!equal(selectedRowKeys,_selectedRowKeys) && this.setState({selectedRowKeys})
	}
	addKeytoDatas(datas){
		let _datas = datas.map((data, i) => {
			let _data = {...data};
			_data.key = i
			return _data
		})
		return _datas
	}
	render() {
		const { datas,showGroup,columns, selectedRowKeys } = this.state;
		const {title,needAdd,needCopy} = this.props;
		return(
			<div className="wea-crm-table-edit" >
				<Row className="wea-title">
                    <Col span="20">
                        <div>{title}</div>
                    </Col>
                    {needAdd &&
						<Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
							<Button type="primary" title='新增' size="small" onClick={this.doAdd}><Icon type="plus" /></Button>
							<Button type="primary" disabled={!`${selectedRowKeys}`} title='删除' size="small" onClick={this.doDelete} ><Icon type="minus" /></Button>
	                        <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
	                    </Col>
                    }
                </Row>
				<Row className="wea-content" style={{display:showGroup ? "block":"none"} }>
					<Col className="wea-form-cell" span={24}>
						<Table 
							columns={this.getColumns()}
							dataSource={datas}
							pagination={false}
							rowSelection={this.getRowSelection()}
						/>
					 </Col>
                </Row>
               
            </div>
		)
	}
	getColumns(){
		const { columns } = this.state;
		let _columns = [].concat(columns);
		_columns = _columns.map(col => {
			let _col = { ...col };
			_col.render = ( text, record, index ) => {
				return this.getColRender( _col, text, record, index );
			}
			return _col
		});
		return _columns
	}
	getColRender( _col, text, record, index ){
		const { com,isLink = false,linkKey =[],linkUrl=""} = _col;
		let _com = [];
		com.map(c => {
			if(typeof c.props === 'object'){
				_com.push(c);
			}else{
				const { domkey,viewAttr =2,key, label, formItemType = 'INPUT', options = [], browserConditionParam = {}, width ,value } = c;
				const _type = formItemType.toUpperCase();
				_com.push(
					<span>
						{label && <span style={{marginLeft: 5}}>{label}</span>}
						{ _type === 'INPUT' && !isLink && 
							<WeaInput 
								defaultValue={record[key]} 
								value={record[key]} 
								style={{width, display: 'inline-block'}}
								viewAttr={viewAttr} /> 
						}
					</span>
				)
			}
		});
		return (
			<div>
				{_com}
			</div>
		)
	}
	getRowSelection(){
		const { columns, selectedRowKeys } = this.state;
		if(!`${columns}`) return null
		const rowSelection = {
			selectedRowKeys,
			onChange: (sRowKeys, selectedRows) => {
				this.setState({selectedRowKeys : sRowKeys});
			}
		}
		return rowSelection
	}

	doAdd(){
		typeof this.props.addColumns === 'function' && this.props.addColumns();
	}
	doDelete(){
		const { datas, current, selectedRowKeys } = this.state;
		let _datas = [].concat(datas);
		selectedRowKeys.map(key => {
			_datas = _datas.filter(data => data.key !== key);
		});
		_datas = _datas.map((data, i) => {
			let _data = {...data};
			_data.key = i;
			return _data
		})
		this.setState({datas: _datas,selectedRowKeys: []});
		this.onChange(_datas);
		
	}
	onChange(datas){
		const { columns } = this.state;
		let _datas = datas.map((data, i) => {
			let _data = {...data}
			delete _data.key
			return _data
		});
		typeof this.props.onChange === 'function' && this.props.onChange(_datas, columns);
	}
}

export default CustomerShare;

