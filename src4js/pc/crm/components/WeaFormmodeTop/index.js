import {Row,Col,Button,Dropdown,Menu,Progress,Tabs} from 'antd'

import {WeaInputSearch} from 'ecCom'
import cloneDeep from 'lodash/cloneDeep'
import './style/index.css'
const Item = Menu.Item

let sb = '';
class WeaFormmodeTop extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        	showDrop:false,
            percent:85,
            end:true,
            height:0
        }
		sb = this;
    }
	scrollheigth(){
		const {heightSpace} =  this.props;
		let heightOther = heightSpace || 0;
    	let top = jQuery(".wea-new-top-req-content").offset() ? (jQuery(".wea-new-top-req-content").offset().top ? jQuery(".wea-new-top-req-content").offset().top : 0) : 0;
        let scrollheigth = document.documentElement.clientHeight - top - heightOther;
        this.setState({height:scrollheigth});
    }
	componentDidMount(){
		this.scrollheigth();
	}
	componentWillReceiveProps(nextProps){
		const {loading} = this.props;
		if(!loading && nextProps.loading){
			this.timer = setInterval(()=>{
				let percent = this.state.percent;
				let newP = percent < 96 ? (percent + 1) : 96;
				this.setState({end:false, percent:newP})
			},10);
		}else if(loading && !nextProps.loading){
			this.setState({percent: 100})
			this.timer && clearInterval(this.timer);
			setTimeout(()=>{this.setState({percent:85,end:true})},300);
		}
    }
    render() {
    	const {showDrop,percent,height,end} = this.state;
        const {children, icon, iconBgcolor,title,buttons,buttonSpace,loading,selectedKey,tabDatas,showDropIcon,dropMenuDatas=[]} = this.props;//isFixed:wea-new-top-req是否固定
		const menu = dropMenuDatas ?
        <Menu mode='vertical' onClick={o => {if(typeof this.props.onDropMenuClick == 'function') this.props.onDropMenuClick(o.key)}}>
    		{
    			dropMenuDatas.map((d, i)=> {
        			return <Item key={d.key || i} disabled={d.disabled}>
        				<span className='wea-right-menu-icon'>{d.icon}</span>
        				{d.content}
        			 </Item>
    		})}
    	</Menu> : '';
		let isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
	    let isIE9 = window.navigator.appVersion.indexOf("MSIE 9.0") >= 0;
//		console.log(percent);
         const {countParam,showSearchAd,buttonsAd,searchsAd,
            searchType,searchsBaseValue,hasDropMenu,dropIcon,buttonsDrop,showSearchDrop,searchsDrop} = this.props;

        const searchIcon =  `${searchType}`.indexOf('icon') >= 0;
        const searchBase =  `${searchType}`.indexOf('base') >= 0;
        const searchAdvanced =  `${searchType}`.indexOf('advanced') >= 0;
        const searchDrop =  `${searchType}`.indexOf('drop') >= 0;

        return (
        	<div className="wea-new-top-req-wapper crm-wea-new-top-req-wapper">
			<div className="wea-tab-frommode">
	            <Row className="wea-new-top-req" style={{backgroundColor:"#f9f9f9"}}>
	                {!end ? <Progress percent={percent} showInfo={false} strokeWidth={2} status="active"/> : ''}
	                <Col className="wea-new-top-req-main" span={24} >
	                	<div className='wea-new-top-req-icon'>
		                	{iconBgcolor ?
		                		<div className="icon-circle-base" style={{background:iconBgcolor ? iconBgcolor : ""}}>
					   				{icon}
								</div>
								:
								<span style={{verticalAlign:'middle',marginRight:10}}>
						   			{icon}
								</span>
		                	}
	                	</div>
						<div className="wea-new-top-req-title-text">{title}</div>
						<Row className="wea-new-top-req-title" style={{height:"35px"}}>
		                    <Col xs={isIE8 ? 14 : 12} sm={16} md={16} lg={16}>
								<Tabs defaultActiveKey={selectedKey} activeKey={selectedKey}
			                    onChange={this.onChange.bind(this)} >
									{
			                            tabDatas && tabDatas.map(data=>{
			                                return <Tabs.TabPane tab={data.title} key={data.key} />
			                            })
			                        }
			                    </Tabs>
			                </Col>
		                    <Col xs={isIE8 ? 10 : 12} sm={8} md={8} lg={8} style={{textAlign:"right"}}>
				                {
				                    buttons.map((data,index)=>{
				                        return (
				                            <span key={index} style={{display:'inline-block',lineHeight:'28px',verticalAlign:'middle',marginRight:!!buttonSpace ? buttonSpace : 10}}>{data}</span>
				                        )
				                    })
				                }
                                {searchBase && <WeaInputSearch value={searchsBaseValue} onSearch={this.onSearch.bind(this)} onSearchChange={this.onSearchChange.bind(this)}/>}
                                {searchAdvanced && <Button type='ghost' className="wea-advanced-search" onClick={this.setShowSearchAd.bind(this,true)}>高级搜索</Button>}
                                {searchDrop && <span style={{marginLeft:15}}>{dropIcon}</span>}

				                <span className='wea-new-top-req-drop-btn' onClick={()=>this.setState({showDrop:true})}>
				                	<i className="icon-button icon-New-Flow-menu" />
								</span>
								{
									dropMenuDatas.length>0 ?
									<div className='wea-new-top-req-drop-menu wea-right-menu' onMouseLeave={()=>this.setState({showDrop:false})} style={{display:showDrop ? 'block' : 'none'}}>
										<span className='wea-new-top-req-drop-btn' onClick={()=>this.setState({showDrop:false})}>
											<i className="icon-button icon-New-Flow-menu" />
										</span>
										<div className='wea-right-menu-icon-background'></div>
										{ menu}
									</div>:""
								}
	                		</Col>
		                </Row>
	                </Col>
	            </Row>
                 <div className="wea-search-container" style={{display:showSearchAd ? 'block' : 'none'}}>
                        <Button type='ghost' className="wea-advanced-search" onClick={this.setShowSearchAd.bind(this,false)}>高级搜索</Button>
                        <div className='wea-advanced-searchsAd' >
                            {searchsAd}
                        </div>
                        <div className="wea-search-buttons">
                            <div style={{"textAlign":"center"}}>
                            {
                                buttonsAd && buttonsAd.map((data,index)=>{
                                    return (
                                        <span key={index} style={{marginLeft:15}}>{data}</span>
                                    )
                                })
                            }
                            </div>
                        </div>
                </div>
                <div className="mask-dark" style={{display:showSearchAd ? 'block' : 'none'}}></div>
                <div className="mask-wrapper" style={{display:showSearchAd ? 'block' : 'none'}} onClick={()=> this.props.hideSearchAd && this.props.hideSearchAd()}> </div>
                <div className="wea-search-container" style={{display:showSearchDrop ? 'block' : 'none'}}>
                    <span className="wea-Drop-search" onClick={this.setShowSearchDrop.bind(this,false)}>{dropIcon}</span>
                    <div className='wea-advanced-searchsAd' >
                        {searchsDrop}
                    </div>
                </div>
				</div>
	            {
	            	children && <div className='wea-new-top-req-content' style={{height:height}}>
		            	{children}
		            </div>
	            }
            </div>
        )
    }
     onSearch(v){
        if (typeof this.props.onSearch == 'function') {
            this.props.onSearch(v);
        }
    }
    onSearchChange(v){
        if (typeof this.props.onSearchChange == 'function') {
            this.props.onSearchChange(v);
        }
    }
    setShowSearchAd(bool){
        if(typeof this.props.setShowSearchAd == 'function'){
            this.props.setShowSearchAd(bool)
        }
    }
    setShowSearchDrop(bool){
        if(typeof this.props.setShowSearchDrop == 'function'){
            this.props.setShowSearchDrop(bool)
        }
    }
    onChange(key){
    	if (typeof (this.props.onChange) == "function") {
            this.props.onChange(key);
        }
    }
}
jQuery(window).resize(function() {
    sb && sb.scrollheigth && sb.scrollheigth()
});


export default WeaFormmodeTop;


// WEBPACK FOOTER //
// ./WEAVER_CloudStore_ec/src4js/components/ecology9/wea-req-top/index.js