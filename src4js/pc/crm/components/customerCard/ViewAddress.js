import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as ViewAddressAction from '../../actions/customerView'

import AddressManageModal from '../data/AddressManageModal.js'
import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action;

import {WeaTab,WeaLeftRightLayout,WeaSearchGroup, WeaRightMenu, WeaPopoverHrm} from 'ecCom'

import {WeaErrorPage,WeaTools} from 'ecCom'
const { getComponent } = WeaTools;

import {Button,Form,Modal,message} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;

let _this = null;

class ViewAddress extends React.Component {
    constructor(props) {
		super(props);
        _this = this;
        this.state={
            required:[]
        }
    }
    componentDidMount() {
    	const {actions,location:{query}} = this.props;
        actions.getViewAddressList(query);
    }
    componentWillReceiveProps(nextProps) {

    }
    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.addressDataKey,nextProps.addressDataKey)||
        !is(this.props.comsWeaTable,nextProps.comsWeaTable)||
        !is(this.props.customerId,nextProps.customerId)||
        !is(this.props.isShowAddress,nextProps.isShowAddress)||
        !is(this.props.addressAddData,nextProps.addressAddData)||
        !is(this.props.operate,nextProps.operate)||
        !is(this.props.addressId,nextProps.addressId)||
        !is(this.props.addressOrderFields,nextProps.addressOrderFields)
    }
    componentWillUnmount(){

    }
    render() {
        const {addressDataKey,comsWeaTable,actions,customerId,isShowAddress,addressAddData,operate,addressId} = this.props;
        return (
            <div style={{height:"100%"}}>
                <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                    <WeaTable 
                        sessionkey={addressDataKey}
                        hasOrder={true}
                        needScroll={true}
                        onOperatesClick={this.onOperatesClick.bind(this)}
                    />
                </WeaRightMenu>
                {
                    <AddressManageModal ref="addressmodal" data={addressAddData.toJS()} all={this.props} operate={operate} addressId={addressId} actions={actions}  />
                }
            </div>
        )
    }
    getRightMenu(){
        const {comsWeaTable,addressDataKey} =this.props;
        const tablekey = addressDataKey ? addressDataKey.split('_')[0] : 'init';
		const tableNow = comsWeaTable.get(tablekey);
    	const selectedRowKeys = tableNow.get('selectedRowKeys');
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-New-Flow'/>,
            content:'新建',
            key:0
        });
        btns.push({
            icon: <i className='icon-coms-edit'/>,
            content:'编辑',
            key:1,
            disabled: !selectedRowKeys || !`${selectedRowKeys.toJS()}`
        });
        btns.push({
            icon: <i className='icon-coms-delete'/>,
            content:'删除',
            key:2,
            disabled: !selectedRowKeys || !`${selectedRowKeys.toJS()}`
        });
        btns.push({
            icon: <i className='icon-coms-content-o'/>,
            content:'显示定制列',
            key:3
        });
        btns.push({
            icon: <i className='icon-coms-Collection'/>,
            content:'收藏',
            key:4
        });
    	btns.push({
            icon: <i className='icon-coms-help'/>,
            content:'帮助',
            key:5
        });
    	return btns
    }
    onRightMenuClick(key){
        const {actions,comsWeaTable,addressDataKey,customerId} = this.props;
        const tablekey = addressDataKey ? addressDataKey.split('_')[0] : 'init';
		const tableNow = comsWeaTable.get(tablekey);
    	const selectedRowKeys = tableNow.get('selectedRowKeys');
        if(key == '0'){
            actions.addressAddForm({customerId:customerId});
            this.refs.addressmodal.visible= true;
         }
         if(key == '1'){
            actions.addressEditForm({customerId:customerId,addressId:`${selectedRowKeys.toJS()}`});
            this.refs.addressmodal.visible= true;
         }
       if(key == '2'){
          actions.deleteAddress({ids:`${selectedRowKeys.toJS()}`},customerId);
       }
       if(key == '3'){
            actions.setColSetVisible(addressDataKey,true);
            actions.tableColSet(addressDataKey,true)
       }
       if(key == '4'){
           // actions.setColSetVisible(dataKey,true);
           // actions.tableColSet(true)
       }
   }
   onOperatesClick=(record,index,operate,flag)=>{
        // console.log(record,index,operate,flag)
        const {actions,customerId} = this.props;
        if(flag=="0"){  //编辑
            actions.addressEditForm({customerId:customerId,addressId:record.randomFieldId});
            this.refs.addressmodal.visible= true;
        }
        if(flag=="1"){  //删除
            actions.deleteAddress({ids:record.randomFieldId},customerId);
        }
   } 
}

class MyErrorHandler extends React.Component {
    render(){
        const hasErrorMsg = this.props.error && this.props.error!=="";
        return (
            <WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
        );
    }
}

ViewAddress = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(ViewAddress);

ViewAddress = createForm({
	onFieldsChange(props, fields) {
        props.actions.saveAddressOrderFields({...props.addressOrderFields.toJS(), ...fields});
    },
	mapPropsToFields(props) {
		return props.addressOrderFields.toJS();
  	}
})(ViewAddress);

function mapStateToProps(state) {
    const {crmcustomerView,comsWeaTable} = state;
    return  {
        addressDataKey:crmcustomerView.get('addressDataKey'),
        customerId:crmcustomerView.get('customerId'),
        isShowAddress:crmcustomerView.get('isShowAddress'),
        addressAddData:crmcustomerView.get('addressAddData'),
        operate:crmcustomerView.get('operate'),
        addressOrderFields:crmcustomerView.get('addressOrderFields'),
        addressId:crmcustomerView.get('addressId'),
        addressSearchParams:crmcustomerView.get('addressSearchParams'),
        comsWeaTable: comsWeaTable, //绑定整个table
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...ViewAddressAction,...WeaTableAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewAddress);
