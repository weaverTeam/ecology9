import { WeaTools,WeaSearchGroup,WeaInput } from 'ecCom'
import {Button,Form,Modal,Table,Icon,Row,Col} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;
import equal from "deep-equal"
import Immutable from 'immutable'
const is = Immutable.is;
import WeaCrmShowGroup from '../weaCrmShowGroup'

class BusinessInfo extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            data:props.data||{}
        }
    }
    shouldComponentUpdate(nextProps,nextState) {
        return !equal(this.state.data,nextState.data)
    }
    componentWillReceiveProps(nextProps,nextState){
        if(!equal(this.props.data,nextProps.data)){
            this.setState({
                data:nextProps.data
            })
        }
    }

    render() {
        const {data} = this.state;
        const formItemLayout ={
            labelCol: { span:6},
            wrapperCol: { span: 18 },
        };
       const employeesColumns =[
        {
            title: '姓名',
            dataIndex: 'name',
            key: 'name',
          }, {
            title: '职务',
            dataIndex: 'job_title',
            key: 'job_title',
          },
       ];
        const branchesColumns =[
            {
                title: '名称',
                dataIndex: 'name',
                key: 'name',
            }
        ];
        const changerecordsColumns =[
            {
                title: '变更项目',
                dataIndex: 'change_item',
                key: 'change_item',
            }, {
                title: '变更日期',
                dataIndex: 'change_date',
                key: 'change_date',
                width:"120px"
            }, {
                title: '变更前内容',
                dataIndex: 'before_content',
                key: 'before_content',
            }, {
                title: '变更后内容',
                dataIndex: 'after_content',
                key: 'after_content',
            }
        ];
        const partnersColumns =[
            {
                title: '姓名',
                dataIndex: 'name',
                key: 'name',
              }, {
                title: '类型',
                dataIndex: 'stock_type',
                key: 'stock_type',
              },
        ];
        const abnormal_itemsColumns=[
            {
                title: '经营异常列入原因',
                dataIndex: 'in_reason',
                key: 'in_reason',
            }, {
                title: '列入日期',
                dataIndex: 'in_date',
                key: 'in_date',
            },{
                title: '移出原因',
                dataIndex: 'out_reason',
                key: 'out_reason',
            }, {
                title: '移出时间',
                dataIndex: 'out_date',
                key: 'out_date',
            },
        ]
        return (
            <div>
               <WeaCrmShowGroup title={"基本信息"}   needTigger={true} children={
                    <Form horizontal style={{marginTop:"10px"}} className="crm-business-form">
                        <FormItem
                            {...formItemLayout}
                            label="公司名称: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.name}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="公司类型: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.econ_kind}  />
                        </FormItem>
                            <FormItem
                            {...formItemLayout}
                            label="注册资本: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.regist_capi}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="地址: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.address}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="企业注册号: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.reg_no}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="经营范围: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.scope}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="营业开始日期: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.term_start}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="营业结束日期: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.term_end}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="法人: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.oper_name}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="成立日期: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.start_date}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="注销日期: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.end_date}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="核准日期: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.check_date}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="在业: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.status}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="组织机构号: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.org_no}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="统一社会信用代码: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.credit_no}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="省份缩写: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.province}  />
                        </FormItem>
                        <FormItem
                            {...formItemLayout}
                            label="企业编号: "
                        >
                            <WeaInput viewAttr={1} hasBorder value={data.id}  />
                        </FormItem>
                    </Form>
                }/>
                <WeaCrmShowGroup title={"主要成员"}  needTigger={true} children={
                    <Table  columns={employeesColumns} dataSource={data.employees} pagination={false} />
                }/>
                <WeaCrmShowGroup title={"分支机构"}  needTigger={true} children={
                    <Table  columns={branchesColumns} dataSource={data.branches} pagination={false} />
                }/>
                <WeaCrmShowGroup title={"变更信息"}  needTigger={true} children={
                    <Table  columns={changerecordsColumns} dataSource={data.changerecords} pagination={false} />
                }/>
                <WeaCrmShowGroup title={"股东信息"}  needTigger={true} children={
                    <Table  columns={partnersColumns} dataSource={data.partners} pagination={false} />
                }/>
                <WeaCrmShowGroup title={"经营信息"}  needTigger={true} children={
                    <Table  columns={abnormal_itemsColumns} dataSource={data.abnormal_items} pagination={false} />
                }/>
            </div>
        )
    }
   
}




export default BusinessInfo;
