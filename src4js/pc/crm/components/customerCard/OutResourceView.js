import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as OutResourceViewAction from '../../actions/outResource'

import CrmTools from '../crmTools'

import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import {
    WeaTab,
    WeaTop,
    WeaLeftRightLayout,
    WeaSearchGroup,
    WeaRightMenu,
    WeaPopoverHrm
} from 'ecCom'

import {WeaErrorPage,WeaTools} from 'ecCom'
const { getComponent } = WeaTools;

import {Button,Form,Modal,message} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;
import equal from 'deep-equal'
let _this = null;

class OutResourceView extends React.Component {
    constructor(props) {
		super(props);
        _this = this;
        this.state={
        }
    }
    componentDidMount() {
    	 const {actions,location:{query}} = this.props;
         actions.getOutResourceDetail(query,false);
    }
    componentWillReceiveProps(nextProps) {
        // if(!is(this.props.viewDatas,nextProps.viewDatas)){
        //     const {actions} = this.props;
        //     actions.unmountClear();
        //     actions.getOutResourceDetail({id:6771},true);
        // }
    }
    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.outResourceOrderFields,nextProps.outResourceOrderFields)||
            !is(this.props.outResourceParams,nextProps.outResourceParams)||
            !is(this.props.title,nextProps.title)||
            !is(this.props.loading,nextProps.loading)||
            !is(this.props.edit,nextProps.edit)||
            !is(this.props.viewDatas,nextProps.viewDatas)||
            !is(this.props.resourceid,nextProps.resourceid)
    }
    componentWillUnmount(){
        
    }
    render() {
        const {title,loading} = this.props;

        return (
            <div>
                <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                <WeaTop
                    title={title}
                    loading={loading}
                    icon={<i className='icon-portal-workflow' />}
                    iconBgcolor='#55D2D4'
                    buttons={this.getButtons()}
                    buttonSpace={10}
                    showDropIcon={true}
                    dropMenuDatas={this.getRightMenu()}
                    onDropMenuClick={this.onRightMenuClick.bind(this)}
                >
                    <div>
                        <Form horizontal>{this.getForm()}</Form>
                    </div>
                    </WeaTop>
                </WeaRightMenu>
            </div>
        )
    }
    getRightMenu(){
        const {edit} =this.props;
        let btns = [];
        {
            !edit && 
            btns.push({
                icon: <i className='icon-coms-New-Flow'/>,
                content:'编辑',
                key:0
            });
        }
        {
            edit &&
            btns.push({
                icon: <i className='icon-coms-Preservation '/>,
                content:'保存',
                key:1
            });
        }
        btns.push({
            icon: <i className='icon-coms-Collection'/>,
            content:'收藏',
            key:4
        });
    	btns.push({
            icon: <i className='icon-coms-help'/>,
            content:'帮助',
            key:5
        });
    	return btns
    }
    onRightMenuClick(key){
        const {actions,resourceid} = this.props;
        const {required} = this.state;
       if(key == '0'){
          actions.getOutResourceDetail({id:resourceid},true);
       }
       if(key == '1'){
            // console.log(this.props.form.getFieldsValue()) ;
             let formvalue = this.props.form.getFieldsValue();
            actions.saveOutResourceEdit({...formvalue,operation:"edit",id:resourceid})
        }
       if(key == '4'){
       }
       if(key == '5'){
           // actions.setColSetVisible(dataKey,true);
           // actions.tableColSet(true)
       }
   }
   getForm() {
    const { viewDatas,edit } = this.props;
   // const {actions,location:{query}} = this.props;
   // actions.getOutResourceDetail(query,false);
    let group = [];
    viewDatas.toJS().map(c =>{
            let items = [];
            c.items.map(field => {
                    items.push({
                        com:(<FormItem
                            label={`${field.label}`}
                            labelCol={{span: `${field.labelcol}`}}
                            wrapperCol={{span: `${field.fieldcol}`}}>
                                { WeaTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey, this.props, field,)}
                            </FormItem>),
                        colSpan:2
                    })
                });
            group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items}/>)
        });
        return group
    }
    getButtons() {
    	const { actions,resourceid,edit} = this.props;
    	let btns = [];
        { !edit && btns.push(<Button type="primary" onClick={()=>{actions.getOutResourceDetail({id:resourceid},true)}}>编辑</Button>)}
        { edit && btns.push(<Button type="primary" onClick={()=>{actions.saveOutResourceEdit({operation:"edit",id:resourceid})}}>保存</Button>)}
        return btns;
    }

}

class MyErrorHandler extends React.Component {
    render(){
        const hasErrorMsg = this.props.error && this.props.error!=="";
        return (
            <WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
        );
    }
}

OutResourceView = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(OutResourceView);

OutResourceView = createForm({
	// onFieldsChange(props, fields) {
    //     props.actions.saveOutResourceOrderFields({...props.outResourceOrderFields.toJS(), ...fields});
    // },
	// mapPropsToFields(props) {
	// 	return props.outResourceOrderFields.toJS();
  	// }
})(OutResourceView);

function mapStateToProps(state) {
    const {crmoutResources,comsWeaTable} = state;
    return  {
        outResourceOrderFields:crmoutResources.get('outResourceOrderFields'),
        outResourceParams:crmoutResources.get('outResourceParams'),
        viewDatas:crmoutResources.get('viewDatas'),
        edit:crmoutResources.get('edit'),
        title:crmoutResources.get('title'),
        loading:crmoutResources.get('loading'),
        resourceid:crmoutResources.get('resourceid'),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...OutResourceViewAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OutResourceView);
