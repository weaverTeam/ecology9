import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as ViewLogAction from '../../actions/customerView'

import AddShareModal from '../setting/addShare.js'
import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import {
    WeaTab,
    WeaLeftRightLayout,
    WeaSearchGroup,
    WeaRightMenu,
    WeaPopoverHrm
} from 'ecCom'

import {WeaErrorPage,WeaTools} from 'ecCom'
const { getComponent } = WeaTools;

import {Button,Form,Modal,message} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;

let _this = null;

class ViewLog extends React.Component {
    constructor(props) {
		super(props);
        _this = this;
        this.state={
            required:[]
        }
    }
    componentDidMount() {
    	const {actions,location:{query}} = this.props;
        actions.getViewLogList(query);
    }
    componentWillReceiveProps(nextProps) {

    }
    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.viewlogDataKey,nextProps.viewlogDataKey)||
        !is(this.props.comsWeaTable,nextProps.comsWeaTable)||
        !is(this.props.customerId,nextProps.customerId)
    }
    componentWillUnmount(){

    }
    render() {
        const {viewlogDataKey,comsWeaTable,actions,customerId} = this.props;
        return (
            <div>
                <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                    <WeaTable 
                        sessionkey={viewlogDataKey}
                        hasOrder={true}
                        needScroll={true}
                    />
                </WeaRightMenu>
            </div>
        )
    }
    getRightMenu(){
        const {comsWeaTable} =this.props;
        let btns = [];

        btns.push({
            icon: <i className='icon-coms-content-o'/>,
            content:'显示定制列',
            key:2
        });
        btns.push({
            icon: <i className='icon-coms-Collection'/>,
            content:'收藏',
            key:3
        });
    	btns.push({
            icon: <i className='icon-coms-help'/>,
            content:'帮助',
            key:4
        });
    	return btns
    }
    onRightMenuClick(key){
        const {actions,comsWeaTable,viewlogDataKey,customerId} = this.props;
       if(key == '2'){
           actions.setColSetVisible(viewlogDataKey,true);
           actions.tableColSet(viewlogDataKey,true)
       }
       if(key == '3'){
          // actions.saveOutResourceForm({operation:"add",customerId:customerId},required)
       }
       if(key == '4'){
           // actions.setColSetVisible(dataKey,true);
           // actions.tableColSet(true)
       }
   }
   
}

class MyErrorHandler extends React.Component {
    render(){
        const hasErrorMsg = this.props.error && this.props.error!=="";
        return (
            <WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
        );
    }
}

ViewLog = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(ViewLog);

ViewLog = createForm({
	// onFieldsChange(props, fields) {
    //     props.actions.saveShareOrderFields({...props.shareOrderFields.toJS(), ...fields});
    // },
	// mapPropsToFields(props) {
	// 	return props.shareOrderFields.toJS();
  	// }
})(ViewLog);

function mapStateToProps(state) {
    const {crmcustomerView,comsWeaTable} = state;
    return  {
        viewlogDataKey:crmcustomerView.get('viewlogDataKey'),
        customerId:crmcustomerView.get('customerId'),
        //shareOrderFields:crmcustomerView.get('shareOrderFields'),
        comsWeaTable: comsWeaTable, //绑定整个table
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...ViewLogAction,...WeaTableAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewLog);
