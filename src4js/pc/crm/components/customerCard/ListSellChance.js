import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as ListSellChanceAction from '../../actions/customerView'

import AddSellChance from '../sellchance/AddSellChance.js'
import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import {
    WeaTab,
    WeaLeftRightLayout,
    WeaSearchGroup,
    WeaRightMenu,
    WeaPopoverHrm,
    WeaDialog
} from 'ecCom'

import {WeaErrorPage,WeaTools} from 'ecCom'
const { getComponent } = WeaTools;

import {Button,Form,Modal,message} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;

let _this = null;

class ListSellChance extends React.Component {
    constructor(props) {
		super(props);
        _this = this;
        this.state={
            showModal:false
        }
    }
    componentDidMount() {
    	const {actions,location:{query}} = this.props;
       actions.getListSellChance({...query,fromType:1});
    }
    componentWillReceiveProps(nextProps) {

    }
    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.sellChanceDataKey,nextProps.sellChanceDataKey)||
        !is(this.props.comsWeaTable,nextProps.comsWeaTable)||
        !is(this.props.customerId,nextProps.customerId)||
        !is(this.state.showModal,nextState.showModal)
    }
    componentWillUnmount(){

    }
    render() {
        const {sellChanceDataKey,comsWeaTable,actions,customerId} = this.props;
        const {showModal} = this.state;
        return (
            <div style={{height:"100%"}}>
                <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                    <WeaTable 
                        sessionkey={sellChanceDataKey}
                        hasOrder={true}
                        needScroll={true}
                    />
                </WeaRightMenu>
                <WeaDialog title={"新建商机"} icon="icon-coms-crm" iconBgcolor="#6d3cf7" visible={showModal} style={{width: 950, height: 560}} 
                    buttons={[
                        <Button onClick={() => this.setState({showModal:false})}>关闭</Button>
                    ]} 
                    onCancel={() => this.setState({showModal:false})}>
                    <div style={{width:"100%",height:"560px",overflow:"auto"}}>	
                        {
                            showModal &&  <AddSellChance createCallBack={this.createCallBack.bind(this)} key={new Date().getTime()}  />
                        }
                    </div>
                </WeaDialog>
            </div>
        )
    }
    getRightMenu(){
        const {comsWeaTable,sellChanceDataKey} =this.props;
        const tablekey = sellChanceDataKey ? sellChanceDataKey.split('_')[0] : 'init';
		const tableNow = comsWeaTable.get(tablekey);
    	const selectedRowKeys = tableNow.get('selectedRowKeys');
        let btns = [];
        btns.push({
            icon: <i className='icon-coms-New-Flow'/>,
            content:'新建',
            key:0
        });
        btns.push({
            icon: <i className='icon-coms-delete'/>,
            content:'删除',
            key:1,
            disabled: !selectedRowKeys || !`${selectedRowKeys.toJS()}`
        });
        btns.push({
            icon: <i className='icon-coms-content-o'/>,
            content:'显示定制列',
            key:2
        });
        btns.push({
            icon: <i className='icon-coms-Collection'/>,
            content:'收藏',
            key:3
        });
    	btns.push({
            icon: <i className='icon-coms-help'/>,
            content:'帮助',
            key:4
        });
    	return btns;
    }
    onRightMenuClick(key){
        const {actions,comsWeaTable,sellChanceDataKey,customerId} = this.props;
        const tablekey = sellChanceDataKey ? sellChanceDataKey.split('_')[0] : 'init';
		const tableNow = comsWeaTable.get(tablekey);
    	const selectedRowKeys = tableNow.get('selectedRowKeys');
        if(key == '0'){
            this.setState({showModal:true})
         }
         if(key == '1'){
            actions.deleteSellChance({customerId:customerId,sellChanceIds:`${selectedRowKeys.toJS()}`});
         }
       if(key == '2'){
            actions.setColSetVisible(sellChanceDataKey,true);
            actions.tableColSet(sellChanceDataKey,true)
       }
       if(key == '4'){
           // actions.setColSetVisible(dataKey,true);
           // actions.tableColSet(true)
       }
   }
   createCallBack(){
     this.setState({showModal:false})
     const {actions,location:{query}} = this.props;
     actions.getListSellChance({...query,fromType:1});
   }
}

class MyErrorHandler extends React.Component {
    render(){
        const hasErrorMsg = this.props.error && this.props.error!=="";
        return (
            <WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
        );
    }
}

ListSellChance = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(ListSellChance);

ListSellChance = createForm({
	// onFieldsChange(props, fields) {
    //     props.actions.saveAddressOrderFields({...props.addressOrderFields.toJS(), ...fields});
    // },
	// mapPropsToFields(props) {
	// 	return props.addressOrderFields.toJS();
  	// }
})(ListSellChance);

function mapStateToProps(state) {
    const {crmcustomerView,comsWeaTable} = state;
    return  {
        sellChanceDataKey:crmcustomerView.get('sellChanceDataKey'),
        customerId:crmcustomerView.get('customerId'),
        
        comsWeaTable: comsWeaTable, //绑定整个table
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...ListSellChanceAction,...WeaTableAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListSellChance);
