import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as LeaveMessageAction from '../../actions/leaveMessage'

import { WeaErrorPage, WeaTools,WeaTextarea ,WeaBrowser,WeaSelect,WeaNewScroll ,WeaPopoverHrm } from 'ecCom'
import {Button,Select,Icon,Form, Input,Modal,Spin,Card,Pagination,Row,Col} from "antd";
const Option = Select.Option;
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;
import equal from 'deep-equal'

class RelateCommunication extends React.Component {
	constructor(props) {
		super(props);
        this.state={
            direction:"down",
            value:"请填写留言信息",
            docIds:"",
            isrefresh:0
        }
	}
	componentWillMount() {
        // const { actions,params,location } = this.props;
        // let fromUrl = {};
        // if(location){fromUrl = location.query;}
        // const newBaseParams = {...params,...fromUrl};
        // actions.initDatas(newBaseParams);
	}
	componentWillReceiveProps(nextProps) {
        const oldParams = this.props.params;
        const newParams = nextProps.params;
        if(!equal(oldParams,newParams)){
            const { actions,params } =nextProps;
            actions.initDatas(params);
        }
	}
	shouldComponentUpdate(nextProps, nextState) {
		//组件渲染控制
        return  !is(this.state.docIds,nextState.docIds)||
        !is(this.state.value,nextState.value)||
        !is(this.state.isrefresh,nextState.isrefresh)||
        !is(this.props.datas,nextProps.datas);
	}
    componentDidMount() {
        let me= this;
		jQuery("#relateExchange").focus(function(){
           if(jQuery(this).val()==="请填写留言信息"){
                jQuery(this).css({height:"100px",color:"#000"});
                jQuery(this).val("");
                jQuery(".customer-contact-group").show();
           }
        });
        jQuery(".customer-contact-content").click(function(){
           me.closeContainer();
        });
        jQuery(".customer-contact-group-addition").toggle(
            function(){
                jQuery(".customer-contact-group-container").show();
                me.setState({direction:"up"});
            },function(){
                jQuery(".customer-contact-group-container").hide();
                me.setState({direction:"down"});
        });
       
    }

	render() {
		const { datas,all } = this.props;
        const {direction,value,isrefresh} = this.state;
        const {getFieldProps,getFieldsValue} = all.form;
        const formItemLayout = {
            labelCol: { span: 3 },
            wrapperCol: { span: 14 },
        };
		return (
			<div className="customer-contact">
				<div className="customer-contact-header">
                    <WeaTextarea  fieldName="relateExchange" minRows={1} value={value} onChange={this.onTextChange}/>
                    <div className="customer-contact-group">
                        <Button type="primary" size="small" onClick={this.submitMessage}>提交</Button>
                        <div className="customer-contact-group-addition">附加信息<Icon type={direction} /></div>
                        <div className="customer-contact-group-container">
                            <Form horizontal >
                                <FormItem
                                label={"相关文档"}    //{...getFieldProps('docIds', { initialValue: '' })}
                                {...formItemLayout}>
                                    <WeaBrowser key={isrefresh} {...getFieldProps('docIds', { initialValue: '' })} type={9} title="文档" hasAdvanceSerach={true} isSingle={false} onChange={this.docOnChange}/>
                                </FormItem>
                            </Form>
                        </div>
                    </div>
                    <div className="customer-contact-bottomline"></div>
                </div>
                <div className="customer-contact-content" style={{padding:"5px 10px",height:"auto"}}> 
                    <div id="relateExchange" style={{overflow: "auto"}}>
                         <WeaNewScroll height={"100%"} children={<ViewExchange data={datas}/>} />
                    </div>
                </div>
            </div>
		)
	}
    onTextChange=(value)=>{
        this.setState({value:value});
    }
    docOnChange=(ids, names, datas)=>{
        this.setState({docIds:ids});
    }
   
    submitMessage=()=>{
        const {all,actions}  =this.props;
        const {value,docIds,isrefresh} = this.state;
        if(value){
            const content= {
                contractId:all.contractId,
                remark:value,
                docIds:docIds,
            };
            actions.saveExchange(content);
            all.form.resetFields();
            this.setState({
                 docIds:"",
                 value:"请填写留言信息",
                 isrefresh:isrefresh+1
            });
             jQuery("#relateExchange").css({height:"28px",color:"#c7c7c7"});
             jQuery(".customer-contact-group").hide();
        }else{
             Modal.info({
                title: '系统提示',
                content: '请填写留言信息',
            });
        }
       
    }

    closeContainer=()=>{
         if(!jQuery("#relateExchange").val()){
            jQuery("#relateExchange").css({height:"28px",color:"#c7c7c7"});
            jQuery("#relateExchange").val("请填写留言信息"); 
            jQuery(".customer-contact-group").hide();
        }
    }
}

class ViewExchange extends React.Component{
    constructor(props) {
		super(props);
        this.state={
            page:1 || props.page
        }
	}
    shouldComponentUpdate(nextProps,nextState) {
        if(!is(this.props.data, nextProps.data)){
            this.setState({page:1})
        }
        return  !is(this.props.data, nextProps.data)||
            !is(this.state.page,nextState.page);
    }
    render(){
        const {page} = this.state;
        const {data} = this.props;
        const cardDatas = data.toJS();
        return (
            <div className="crm-view-contacter">
                <WeaPopoverHrm />
                {this.getCards(page)}
                {
                    cardDatas.length>0 ?
                    <div ><Pagination defaultCurrent={1}  total={cardDatas.length} onChange={this.changePageSize}/></div>
                    :<div className="crm-no-datas-show">没有可显示的数据</div>
                }
            </div>
        )
    }
    getCards(page){
        const {data} = this.props;
        const cardDatas = data.toJS();
        let cards = [];
        cardDatas.map((item,index)=>{
            if(index>=(10*Number(page)-10) && index<= (10*Number(page)-1)){
                 cards.push( 
                    <div className="crm-contract-exchange"> 
                        <Row>
                            <Col span={12} style={{overflow:"hidden",textOverflow:"ellipsis"}}>
                                <div className="card-right-hrm">
                                    <span className="card-call" dangerouslySetInnerHTML={{__html:item.createrNameLink}}></span>
                                    <span className="card-call">{item.createDate} {item.createTime}</span>
                                </div>	
                            </Col>
                        </Row>
                    
                        <Row>
                        <div className="card-right">
                            <div className="feedbackrelate">
                                <div style={{height:"24px",lineHeight:"24px",margin:"0 10px"}}>{item.remark}</div>
                                {item.docIds && <div className="relatetitle" style={{height:"24px",lineHeight:"24px",margin:"0 10px"}}>相关文档:&nbsp;<span dangerouslySetInnerHTML={{__html:item.docNames}}></span></div>}
                            </div>
                        </div>
                        </Row>
                        <div className="clear"></div>
                    </div>)
            }
        });
        return cards;
    }
    changePageSize=(page)=>{
        this.setState({page:page})
    }
}


export default RelateCommunication;