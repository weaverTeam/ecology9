import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as ViewCustomerShareAction from '../../actions/customerView'

import AddShareModal from '../setting/addShare.js'
import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import {
    WeaTab,
    WeaLeftTree,
    WeaLeftRightLayout,
    WeaSearchGroup,
    WeaRightMenu,
    WeaPopoverHrm
} from 'ecCom'

import {WeaErrorPage,WeaTools} from 'ecCom'
const { getComponent } = WeaTools;

import {Button,Form,Modal,message} from 'antd' 
const createForm = Form.create;
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;
import equal from 'deep-equal'

let _this = null;

class ViewCustomerShare extends React.Component {
    constructor(props) {
		super(props);
        _this = this;
        this.state={
            required:[]
        }
    }
    componentDidMount() {
    	const {actions,location:{query}} = this.props;
        actions.getShareSettingList(query);
    }
    componentWillReceiveProps(nextProps) {

    }
    shouldComponentUpdate(nextProps,nextState) {
        return !is(this.props.shareDataKey,nextProps.shareDataKey)||
        !is(this.props.comsWeaTable,nextProps.comsWeaTable)||
        !is(this.props.customerId,nextProps.customerId)||
        !is(this.props.shareOrderFields,nextProps.shareOrderFields)
    }
    componentWillUnmount(){

    }
    render() {
        const {shareDataKey,comsWeaTable,actions,customerId} = this.props;
        return (
            <div className="height100">
                <WeaRightMenu datas={this.getRightMenu()} onClick={this.onRightMenuClick.bind(this)}>
                    <WeaTable 
                        sessionkey={shareDataKey}
                        hasOrder={true}
                        needScroll={true}
                        onOperatesClick={this.onOperatesClick.bind(this)}
                    />
                    {
                        <AddShareModal ref="addShareModal" all ={this.props} customerId={customerId} actions={actions} />
                    }
                </WeaRightMenu>
            </div>
        )
    }
    getRightMenu(){
        const {shareDataKey,comsWeaTable} =this.props;
        const tablekey = shareDataKey ? shareDataKey.split('_')[0] : 'init';
		const tableNow = comsWeaTable.get(tablekey);
    	const selectedRowKeys = tableNow.get('selectedRowKeys');
        let btns = [];

        btns.push({
            icon: <i className='icon-coms-New-Flow'/>,
            content:'添加',
            key:0
        });

        btns.push({
            icon: <i className='icon-coms-delete'/>,
            content:'删除',
            key:1,
            disabled: !selectedRowKeys || !`${selectedRowKeys.toJS()}`
        });

        btns.push({
            icon: <i className='icon-coms-content-o'/>,
            content:'显示定制列',
            key:2
        });
        btns.push({
            icon: <i className='icon-coms-Collection'/>,
            content:'收藏',
            key:3
        });
    	btns.push({
            icon: <i className='icon-coms-help'/>,
            content:'帮助',
            key:4
        });
    	return btns
    }
    onRightMenuClick(key){
        const {actions,comsWeaTable,shareDataKey,customerId} = this.props;
        const {required} = this.state;
        const tablekey = shareDataKey ? shareDataKey.split('_')[0] : 'init';
		const tableNow = comsWeaTable.get(tablekey);
    	const selectedRowKeys = tableNow.get('selectedRowKeys');
       if(key == '0'){
           this.refs.addShareModal.visible = true
       }
       if(key == '1'){
            actions.shareSettingDelete({id:`${selectedRowKeys.toJS()}`},customerId);
        }
       if(key == '2'){
           actions.setColSetVisible(shareDataKey,true);
           actions.tableColSet(shareDataKey,true)
       }
       if(key == '3'){
        //    actions.saveOutResourceForm({operation:"add",customerId:customerId},required)
       }
       if(key == '4'){
           // actions.setColSetVisible(dataKey,true);
           // actions.tableColSet(true)
       }
   }
    onOperatesClick(record,index,operate,flag){
        const {actions,customerId} = this.props;
        if(flag=="0"){  //共享 删除
            actions.shareSettingDelete({id:record.randomFieldId},customerId);
        }
    }
   
}

class MyErrorHandler extends React.Component {
    render(){
        const hasErrorMsg = this.props.error && this.props.error!=="";
        return (
            <WeaErrorPage msg={hasErrorMsg?this.props.error:"对不起，该页面异常，请联系管理员！"} />
        );
    }
}

ViewCustomerShare = WeaTools.tryCatch(React, MyErrorHandler, {error: ""})(ViewCustomerShare);

ViewCustomerShare = createForm({
	onFieldsChange(props, fields) {
        props.actions.saveShareOrderFields({...props.shareOrderFields.toJS(), ...fields});
    },
	mapPropsToFields(props) {
		return props.shareOrderFields.toJS();
  	}
})(ViewCustomerShare);

function mapStateToProps(state) {
    const {crmcustomerView,comsWeaTable} = state;
    return  {
        shareDataKey:crmcustomerView.get('shareDataKey'),
        customerId:crmcustomerView.get('customerId'),
        shareOrderFields:crmcustomerView.get('shareOrderFields'),
        comsWeaTable: comsWeaTable, //绑定整个table
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({...ViewCustomerShareAction,...WeaTableAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewCustomerShare);
