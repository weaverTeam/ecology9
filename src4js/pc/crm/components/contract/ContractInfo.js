import { Form ,} from 'antd';
import {WeaTools,WeaSearchGroup} from 'ecCom'
import {is} from 'immutable'
import equal from 'deep-equal'
const createForm = Form.create;
const FormItem = Form.Item;
const { getComponent } = WeaTools;
import WeaCrmTableEdit from '../WeaCrmTableEdit';
import WeaCrmContractPayTable from '../WeaCrmTableEdit/contractPayTable.js'

class ContractInfo extends React.Component{
    constructor(props) {
		super(props);
	}
    shouldComponentUpdate(nextProps,nextState) {
       return !is(this.props.subTable1,nextProps.subTable1)||
       !is(this.props.subTable2,nextProps.subTable2)||
       !is(this.props.subTable3,nextProps.subTable3)||
       !is(this.props.subTable4,nextProps.subTable4)||
       !is(this.props.mainTableInfo,nextProps.mainTableInfo)||
       !is(this.props.infoField,nextProps.infoField)||
       !is(this.props.edit,nextProps.edit)
    }
    render(){
        const {subTable1,subTable2,subTable3,subTable4,edit,all:{status}} = this.props;
        const productTable = <WeaCrmTableEdit
            title={"产品"}
            showGroup={true}
            needAdd={edit=="edit"? true:(edit=="add"? true:false)}
            needCopy={false}
            columns={subTable1.columnDefine}
            datas={subTable1.columnData}
            onChange={this.productTableDatas.bind(this)}
        />
        const payMethodTable = <WeaCrmTableEdit
            title={"付款方式"}
            showGroup={true}
            needAdd={edit=="edit"? true:(edit=="add"? true:false)}
            needCopy={false}
            columns={subTable2.columnDefine}
            datas={subTable2.columnData}
            onChange={this.payTableDatas.bind(this)}
        />
        let detailComs = [];
            detailComs.push(productTable);
            detailComs.push(payMethodTable);
        if(status == "2"){
            for(let i=0;i<subTable1.columnData.length;i++){
                subTable1.columnData[i].child = subTable3[subTable1.columnData[i].dataId];
            }
            for(let i=0;i<subTable2.columnData.length;i++){
               subTable2.columnData[i].child = subTable4[subTable2.columnData[i].dataId];
            }
        };
        if(status == "2"){
            subTable1.columnDefine.push(
                {
                    "title": "",
                    "com": [
                        <span></span>
                    ],
                    "width": "5%",
                    "dataIndex": "handle",
                    "key": "handle"
                }
            )
            subTable2.columnDefine.push(
                {
                    "title": "",
                    "com": [
                        <span></span>
                    ],
                    "width": "5%",
                    "dataIndex": "handle",
                    "key": "handle"
                }
            )
        }
        
        const productChildColumns= [
            {
                "title": "",
                "com": [
                ],
                "width": "55%",
                "dataIndex": "1",
                "key": "1"
            },
            {
                "title": "单据号",
                "com": [
                    {
                        "formItemType": "INPUT",
                        "labelcol": 6,
                        "colSpan": 2,
                        "viewAttr": 2,
                        "length": 10,
                        "domkey": [
                            "formNum"
                        ],
                        "fieldcol": 17,
                        "label": "单据号"
                    }
                ],
                "width": "10%",
                "dataIndex": "formNum",
                "key": "formNum"
            },{
                "title": "实际交货数量",
                "com": [
                    {
                        "formItemType": "INPUT",
                        "labelcol": 6,
                        "colSpan": 2,
                        "viewAttr": 2,
                        "length": 10,
                        "domkey": [
                            "factNum"
                        ],
                        "fieldcol": 17,
                        "label": "实际交货数量",
                        "type":"number"
                    }
                ],
                "width": "10%",
                "dataIndex": "factNum",
                "key": "factNum"
            },{
                "title": "实际交货日期",
                "com": [
                    {
                        "formItemType": "DATEPICKER",
                        "labelcol": 6,
                        "colSpan": 2,
                        "formatPattern": 2,
                        "viewAttr": 2,
                        "domkey": [
                            "factDate"
                        ],
                        "fieldcol": 17,
                        "label": "实际交货日期"
                    }
                ],
                "width": "10%",
                "dataIndex": "factDate",
                "key": "factDate"
            },{
                title: '操作',
                dataIndex: 'operation',
                com:[ <span></span>],
                key: 'operation',
                width:"15%"
                },
            ];
        const payChildColumns = [
            {
				"title": "",
				"com": [
				],
				"width": "55%",
				"dataIndex": "1",
				"key": "1"
			},
			{
				"title": "单据号",
				"com": [
					{
						"formItemType": "INPUT",
						"labelcol": 6,
						"colSpan": 2,
						"viewAttr": 2,
						"length": 10,
						"domkey": [
							"formNum"
						],
						"fieldcol": 17,
						"label": "单据号"
					}
				],
				"width": "10%",
				"dataIndex": "formNum",
				"key": "formNum"
			},{
				"title": "实际付款金额",
				"com": [
					{
						"formItemType": "INPUT",
						"labelcol": 6,
						"colSpan": 2,
						"viewAttr": 2,
						"length": 10,
						"domkey": [
							"factPrice"
						],
						"fieldcol": 17,
                        "label": "实际付款金额",
                        "type":"number"
					}
				],
				"width": "10%",
				"dataIndex": "factPrice",
				"key": "factPrice"
			},{
				"title": "实际付款日期",
				"com": [
					{
						"formItemType": "DATEPICKER",
						"labelcol": 6,
						"colSpan": 2,
						"formatPattern": 2,
						"viewAttr": 2,
						"domkey": [
							"factDate"
						],
						"fieldcol": 17,
						"label": "实际付款日期"
					}
				],
				"width": "10%",
				"dataIndex": "factDate",
				"key": "factDate"
			},{
				title: '操作',
				dataIndex: 'operation',
				com:[ <span></span>],
                key: 'operation',
                width:"15%"
			  },

		];

        return (
            <div >
                <Form horizontal>
                    {this.getFormItemGroups()}
                </Form>
                {
                    status == "2" ?
                    <div>
                        <WeaCrmContractPayTable 
                            title={"产品"}
                            showGroup={true} 
                            columns={subTable1.columnDefine}
                            datas={subTable1.columnData}
                            childColumns ={productChildColumns}
                            type={"product"}
                            payRowSave={this.productRowsave.bind(this)}
                            payDetailHandle={this.productDetailHandle.bind(this)}
                          //  onChange={this.editChange.bind(this)}
                        />
                        <WeaCrmContractPayTable 
                            title={"付款方式"}
                            showGroup={true} 
                            columns={subTable2.columnDefine}
                            datas={subTable2.columnData}
                            childColumns ={payChildColumns}
                            type={"pay"}
                            payRowSave={this.payRowsave.bind(this)}
                            payDetailHandle={this.payDetailHandle.bind(this)}
                        //  onChange={this.editChange.bind(this)}
                        />
                    </div>
                    :detailComs
                }
            </div>
        )
    }
    getFormItemGroups() {
        const {all,actions,mainTableInfo} = this.props;
        let group = [];
        let viewObj=[];
		mainTableInfo.map(c =>{
			let items = [];
			c.items.map(field => {
                let fit = field.formItemType.toUpperCase();
                if(fit != "BROWSER" && field.viewAttr == "3"){
                    viewObj.push(field.domkey[0]);
                }else if(fit == "BROWSER"){
                    const bcp = field.browserConditionParam;
                    if(bcp.viewAttr == "3"){
                       viewObj.push(field.domkey[0]);
                    }  
                }

                items.push({
                    com:(<FormItem
                        label={`${field.label}`}
                        labelCol={{span: `${field.labelcol}`}}
                        wrapperCol={{span: `${field.fieldcol}`}}>
                            { WeaTools.getComponent(field.formItemType, field.browserConditionParam, field.domkey,this.props, field)}
                        </FormItem>),
                    colSpan:1
                })
            });
			group.push(<WeaSearchGroup needTigger={true} title={c.title} showGroup={c.defaultshow} items={items}/>)
        });
        actions.saveContractInfoRequire(viewObj);
		return group
    }
    productTableDatas(datas){
        const {actions} = this.props;
        actions.saveContractProduct(datas);
    }
    payTableDatas(datas){
        const {actions} = this.props;
        actions.saveContractPay(datas);
    }
    productRowsave(record,index){
        //console.log(record, index );
        const {actions,all} = this.props;
        actions.payGoodsSave({contractId:all.contractId,
            payGoodsList:JSON.stringify([{productId:record.productId,formNum:record.formNum,factNum:record.factNum,factDate:record.factDate,isFinish:record.isFinish,isRemind:record.isRemind}])});
    }
    productDetailHandle(record,index,type){
        //console.log(record, index ,type);
        const {actions,all} = this.props;
        if(type=="save"){
            actions.payGoodsSave({contractId:all.contractId,payGoodsList:JSON.stringify([{...record}])});
        }
        if(type=="delet"){
            actions.payGoodsDelete({contractId:all.contractId,dataId:record.dataId,productId:record.productId});
        }
    }
    payRowsave(record,index){  //
        //console.log(record, index );
        const {actions,all} = this.props;
       actions.paySave({contractId:all.contractId,
        payList:JSON.stringify([{paymethodId:record.dataId,formNum:record.formNum,factPrice:record.factPrice,factDate:record.factDate,isFinish:record.isFinish,isRemind:record.isRemind}])});
    }
    payDetailHandle(record,index,type){
       // console.log(record, index ,type);
        const {actions,all} = this.props;
        if(type=="save"){
            actions.paySave({contractId:all.contractId,payList:JSON.stringify([{ ...record}])});
        }
        if(type=="delet"){
            actions.payDelete({contractId:all.contractId,dataId:record.dataId,paymethodId:record.paymethodId});
        }
    }
}

ContractInfo = createForm({
    onFieldsChange(props, fields) { 
       props.actions.saveInfoFields({ ...props.infoField.toJS(), ...fields });
    },
    mapPropsToFields(props) {
        return props.infoField.toJS()
    }
})(ContractInfo);


export default ContractInfo;