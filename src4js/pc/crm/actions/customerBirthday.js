import * as types from '../constants/ActionTypes'
import * as API_CUSTOMERBIRTHDAY from '../apis/customerBirthday'
import * as API_TABLE from '../apis/table'

import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import { Modal } from 'antd'
import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'

//记录当前流程页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}

//初始化查询联系人
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerBirthday.get('nowRouterCmpath');

		API_CUSTOMERBIRTHDAY.getBirthdayCondition(params).then((data) => {
			dispatch({
				type: `${viewScope}_` + types.INIT_CONDITION,
				condition: data.datas,
			});
		});
	}
}

//搜索
export const doSearch = (params ) => {
	//console.log("搜索");
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerBirthday.get('nowRouterCmpath');
		 //  alert('crm' + viewScope);
		const searchParamsAd = getState()['crm' + viewScope].get('searchParamsAd').toJS();
		const newparams = {...searchParamsAd,...params};
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CUSTOMERBIRTHDAY.getBirthdayList(newparams).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
			dispatch({
				type:`${viewScope}_` + types.INIT_TABLE,
				dataKey: data.sessionkey,
				loading:false
			});
		});
	}
}



//高级搜索受控
export const setShowSearchAd = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SET_SHOW_SEARCHAD, value: bool })
	}
}

//高级搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}