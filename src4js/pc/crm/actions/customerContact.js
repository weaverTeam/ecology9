import * as types from '../constants/ActionTypes'
import * as API_CUSTOMERCONTACT from '../apis/customerContact'
import * as API_COMMON from '../apis/commonApi'
import { Modal,message } from 'antd'
import objectAssign from 'object-assign'

import Immutable from 'immutable'

//初始化联系记录
export const initDatas = params => {
	return(dispatch, getState) => {
			dispatch({ type:types.LOADING, loading: true });
			API_CUSTOMERCONTACT.getCusContactList(params).then((data) => {
				if(data.status=="success"){
                    dispatch({
                        type:types.INIT_VIEWLOGS,
						viewlogs: data.datas,
						customerId:params.customerId,
                        loading:false,
                    });
                }else{
                     Modal.error({
                        title: '系统提示',
                        content: '查询失败,'+data.msgcode,
                    });
                }
				
			});
		}
}

// 联系人信息获取
export const getContacter = params => {
	return(dispatch, getState) => {
		API_CUSTOMERCONTACT.getContacterCard(params).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CUSTOMERCONTACT_SET_CONTACTER_INFO, 
					contacterInfo: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

// 销售机会信息获取
export const getSellChance = params => {
	return(dispatch, getState) => {

		API_CUSTOMERCONTACT.getSellchanceCard(params).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CUSTOMERCONTACT_SET_SELLCHANCE_INFO, 
					sellChanceInfo: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

//提交联系记录
export const saveContacts = (content,params) => {
	return(dispatch, getState) => {
		API_CUSTOMERCONTACT.saveCusContact(content).then((data) => {
			if(data.status=="success"){
				dispatch(initDatas(params));
				dispatch(saveContactFields());
				dispatch(changeTextHeight(1));
			}else{
					Modal.error({
					title: '系统提示',
					content: '操作失败,'+data.msgcode,
				});
			}
			
		});
	}
}

//获取客户联系的右键
export const getRightMenu = params =>{
	return(dispatch, getState) => {
		API_COMMON.getRightMenuList(params).then((data) => {
			if(data.status=="success"){
				dispatch({
					type:types.CUSTOMERCONTACT_SAVE_RIGHTMENU,
					customerContactRcList:data.datas
				});
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

//绑定form 更新内容
export const saveContactFields =(value={})=>{
	return (dispatch,getState)=>{
		dispatch({
			type:types.CUSTOMERCONTACT_SAVE_ORDER_FIELDS,
			value:value
		})
	}

}
//改变行高
export const changeTextHeight = h =>{
	return (dispatch,getState)=>{
		dispatch({
			type:types.CUSTOMERCONTACT_CHANGE_ROWHEIGHT,
			value:h
		})
	}
}