import * as types from '../constants/ActionTypes'
import * as API_CONTACTQUERY from '../apis/contacterQuery'


import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import { Modal } from 'antd'
import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'

//记录当前流程页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}

//初始化查询联系人
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterCmpath');
	
		API_CONTACTQUERY.getCmCondition().then((data) => {
			dispatch({
				type: `${viewScope}_` + types.INIT_CONDITION,
				conditioninfo: data.datas,
				hasright:data.hasright
			});
		});
	}
}

//搜索
export const doSearch = (params ) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterCmpath');
		 //  alert('crm' + viewScope);
		const searchParamsAd = getState()['crm' + viewScope].get('searchParamsAd').toJS();
		const newParams = {...searchParamsAd, ...params};
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CONTACTQUERY.getContacterList(newParams).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
			dispatch({
				type:`${viewScope}_` + types.INIT_TABLE,
				dataKey: data.sessionkey,
				loading:false
			});
		});
	}
}

//组件卸载时是否需要清除的数据
export const isClearNowPageStatus = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterWfpath');
		dispatch({ type: viewScope + '_' + types.CLEAR_PAGE_STATUS, isToReq: bool })
	}
}
//组件卸载时需要清除的数据
export const unmountClear = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterWfpath');
		dispatch({ type: viewScope + '_' + types.UNMOUNT_CLEAR, isToReq: bool })
	}
}

//高级搜索受控
export const setShowSearchAd = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterCmpath');
		//console.log(viewScope)
		dispatch({ type: viewScope + '_' + types.SET_SHOW_SEARCHAD, value: bool })
	}
}

//高级搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontactQuery.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}
//获取联系人id
