import * as types from '../constants/ActionTypes'
import * as API_CONTACTERVIEW from '../apis/contacterView'


import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'
import { message } from 'antd';
//记录当前页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}
//设置表单tabkey   
export const setDetailTabKey = key => {
	return (dispatch,getState)=>{
		const contacterId = getState().crmcontacterView.get('contacterId');
		dispatch({type:types.SET_DETAIL_TABKEY,detailTabKey:key});
		if(key == "2"){
			API_CONTACTERVIEW.getTrailInfo({contacterId:contacterId}).then((data) => {
				if(data.status == "success"){
					dispatch({
						type: types.CONTACTERVIEW_INIT_TRAILINFO,
						trailinfo: data.datas
					});
				}else{
					 message.error('请求失败,'+data.msgcode+"!");
				}
			}).catch(err=>message.error(err));
		}
	}
}

//初始化查询联系人
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontacterView.get('nowRouterCmpath');
		
		API_CONTACTERVIEW.getContacterForm(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: `${viewScope}_` + types.INIT_CONDITION,
					conditioninfo: data.datas,
					title:data.title,
					customerId:data.customerId,
					contacterId:params.contacterId,
					canEdit:data.canEdit,
					hasright:data.hasright,
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
			
		}).catch(err=>message.error(err));
	}
}

//搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcontacterView.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}


//实时编辑
export const contacterEdit =(params)=>{
	return(dispatch, getState) => {
		// const viewScope = getState().crmcontacterView.get('nowRouterCmpath');
		// dispatch({ type: viewScope + '_' + types.SAVE_ID, value: id });
		API_CONTACTERVIEW.contacterEdit(params).then((data) => {
			if(data.status=="success"){
				message.success('操作成功！');
			}else{
				 message.error('操作失败,'+data.msgcode+"!");
			}
		});
	}
}

//删除联系人
export const deleteContracter = (id) =>{
	return (dispatch,getState)=>{
		API_CONTACTERVIEW.deleteContracter({contacterId:id}).then((data) => {
			if(data.status=="success"){
				window.close();
			}else{
				 message.error('操作失败,'+data.msgcode+"!");
			}
		});
	}
}