import * as types from '../constants/ActionTypes'
import * as API_SEARCH from '../apis/search'
import * as API_TABLE from '../apis/table'

import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import { Modal ,message} from 'antd'
import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'

//记录当前流程页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}


//初始化查询客户 - 批量共享 - -客户监控 =客户经理 的基本信息
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsearchResult.get('nowRouterCmpath');
		const paramsBase =  getState()['crm' + viewScope].get('paramsBase').toJS();

		const newParams = {...paramsBase,...params};

		API_SEARCH.getSearchCondition(newParams).then((data) => {
			dispatch({
				type: `${viewScope}_` + types.INIT_BASE,
				conditioninfo:data.datas,
				paramsBase:{...paramsBase,...params}
			});
		});
	}
}

//初始化查询客户 - 批量共享 - -客户监控 =客户经理 的title   右键  顶部操作信息
export const getTitleInfo = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsearchResult.get('nowRouterCmpath');
		const paramsBase =  getState()['crm' + viewScope].get('paramsBase').toJS();
		const newParams = {...paramsBase,...params};

		API_SEARCH.getSearchTitleInfo(newParams).then((data) => {
			dispatch({
				type: `${viewScope}_` + types.INIT_TITLEINFO,
				title:data.datas.title,
				rcList:data.datas.rightMenus,
			});
		});
	}
}

//搜索
export const doSearch = (params ) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsearchResult.get('nowRouterCmpath');
		const paramsBase =  getState()['crm' + viewScope].get('paramsBase').toJS();
		const searchParamsAd = getState()['crm' + viewScope].get('searchParamsAd').toJS();
		const newParams = {...paramsBase,...searchParamsAd, ...params};
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });
		API_SEARCH.getCmCustomerList(newParams).then((data) => {
			{data.hasright && dispatch(WeaTableAction.getDatas(data.sessionkey,  1));}
			dispatch({
				type:`${viewScope}_` + types.INIT_TABLE,
				dataKey: data.sessionkey,
				hasright:data.hasright,
				loading:false
			});
			
		});
	}
}



//高级搜索受控
export const setShowSearchAd = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsearchResult.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SET_SHOW_SEARCHAD, value: bool })
	}
}

//高级搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsearchResult.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}

//根据客户id  获取名称  
export const getCrmNamesById = params =>{
	return(dispatch, getState) => {
		API_SEARCH.getCrmNamesById(params).then((data) => {
			dispatch({
				type:types.SEARCHRESULT_INIT_CUSTOMERNAME,
				customerStr:data.datas,
				customerIds:params.customerIds
			});
		});
	}
}

//保存 客户经理分配
export const saveCustomerAssign =(params,query)=>{
	return(dispatch, getState) => {
		API_SEARCH.saveCustomerAssign(params).then((data) => {
			if(data.status == "success"){
				dispatch({type:types.SEARCHRESULT_CLEAR_CUSTOMER,customerIds:""})
				dispatch(doSearch());
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//保存客户id
export const  getCustomerIds =value =>{
	return (dispatch,getState)=>{
		dispatch({type:types.SEARCHRESULT_SAVE_ID,customerIds:value})
	}
}

//保存  修改客户经理
export const saveChangeManager=(params,query)=>{
	return(dispatch, getState) => {
		API_SEARCH.saveChangeManager(params).then((data) => {
			if(data.status == "success"){
				return true;
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//批量共享  保存
export const saveCustomerShare =params =>{
	return(dispatch, getState) => {
		API_SEARCH.saveCustomerShare(params).then((data) => {
			if(data.status == "success"){
				message.success('分享成功!');
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
