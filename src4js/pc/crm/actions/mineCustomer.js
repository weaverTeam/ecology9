import * as types from '../constants/ActionTypes'
import * as API_MINECUSTOMER from '../apis/mineCustomer'
import * as API_CONTACT from '../apis/contact'

import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action;

import { Modal,message } from 'antd'
import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'

//记录当前流程页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}
//tabs页签读取
export const getMyCustomerTags = params => {
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');

		API_MINECUSTOMER.getMyCustomerTags(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:`${viewScope}_` + types.INIT_TABS,
					tabDatas: data.datas,
				});
			}else{
				 message.error('tab标签请求失败,'+data.msgcode+"!");
			};
		});
	}
}

//自定义标签数据获取
export const getMyCustomerTagsList = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');

		API_MINECUSTOMER.getMyCustomerTagList(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:`${viewScope}_` + types.INIT_TAGS_LIST,
					tagDatas: data.datas,
				});
			}else{
				 message.error('tab标签请求失败,'+data.msgcode+"!");
			};
		});
	}
}
//tabs页签受控
export const setMineCustomerTabKey = (key,num) => {
	return(dispatch, getState) => {
		
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SET_DETAIL_TABKEY, key: key,istorefresh:num+1 });
		dispatch(conditionTop({labelid:key}));
		dispatch(saveOrderFields());
		dispatch(doSearch({labelid:key}));
		dispatch(getContactLogs({labelid:key}))
	}
}

//搜索
export const doSearch = (params ) => {
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		const searchBaseParams = getState()[viewScope].get('searchBaseParams').toJS();
		const searchParamsAd = getState()[viewScope].get('searchParamsAd').toJS();

		const newParams = {...searchBaseParams,...searchParamsAd, ...params};
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_MINECUSTOMER.getMyCustomerList(newParams).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type:`${viewScope}_` + types.INIT_TABLE,
					dataKey: data.sessionkey,
					loading:false,
					searchParams: {...searchBaseParams, ...params },
					showRight:false
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			};
		});
	}
}

//状态，标签tab页信息展示
export const conditionTop = params => {
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');

		const searchBaseParams = getState()[viewScope].get('searchBaseParams').toJS();
		const newParams = {...searchBaseParams, ...params};

		// let sectorTreeData1 = getState()[viewScope].get('sectorTreeData').toJS();

		API_MINECUSTOMER.getConditionTop(newParams).then((data) => {
			//data.sectorTreeData.unshift({"id": "0","isLeaf": true,"name": "全部行业","getLeafApi": ""});
			dispatch({
				type:types.MINECUSTOMER_INIT_TAGS,
				sectorTreeData:data.sectorTreeData,
				statusOptions:data.statusOptions,
				hrmList:data.hrmList,
				viewTypeList:data.viewTypeList,
				isShowResource:data.isShowResource
			});
			
		});
	}
}

//保存新的人员列表
export const saveHrmList = value =>{
	return (dispatch, getState) => {
		dispatch({ type: types.MINECUSTOMER_SAVE_HRM_LIST, value: value })
	}
}
//保存更新行业列表
export const saveIndustry = value =>{
	return (dispatch, getState) => {
		dispatch({ type: types.MINECUSTOMER_SAVE_INDUSTRY_LIST, value: value })
	}
}

//高级搜索表单内容
export const saveOrderFields = (value = {}) => {
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}

//我的客户--右侧信息tab受控
export const setRightTabKey = key =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		const selectValue = getState()[viewScope].get('selectValue');
		dispatch({ type: viewScope + '_' + types.SET_RIGHT_DETAIL_TABKEY, key: key });
		if(key == 1){
			dispatch(getContactLogs({datatype:selectValue}));
		}
		if(key == 2){
			dispatch(getContacterCard({datatype:selectValue}));
		}
		if(key == 3){
			dispatch(getLeaveMessage({datatype:selectValue}));
		}
		if(key == 4){
			dispatch(getSellchanceCard({datatype:selectValue}));
        }
	}
}
//右侧上方时间日期value保存
export const saveSelectValue = val =>{
	return(dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_SELECT_VALUE, value: val })
	}
}
//我的客户--客户联系信息
export const getContactLogs = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		const searchParamsAd = getState()[viewScope].get('searchParamsAd').toJS();
		const searchBaseParams = getState()[viewScope].get('searchBaseParams').toJS();
		const newParams = {...searchBaseParams, ...searchParamsAd,...params};

		dispatch({ type: viewScope + '_' + types.CARDLOADING, cardloading: true });
		API_MINECUSTOMER.getContactLogs(newParams).then((data) => {
			dispatch({
				type:types.MINECUSTOMER_INIT_CONTACTLOGS,
				contactLogs:data.datas,
				cardloading:false,
			});
			
		});
	}
}
//我的客户--联系人
export const getContacterCard = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		const searchParamsAd = getState()[viewScope].get('searchParamsAd').toJS();
		const searchBaseParams = getState()[viewScope].get('searchBaseParams').toJS();
		const newParams = {...searchBaseParams, ...searchParamsAd,...params};

		dispatch({ type: viewScope + '_' + types.CARDLOADING, cardloading: true });
		API_MINECUSTOMER.getContacterCard(newParams).then((data) => {
			dispatch({
				type:types.MINECUSTOMER_INIT_CONTACTERCARD,
				contacterCard:data.datas,
				cardloading:false,
			});
			
		});
	}
}

//我的客户--客户留言
export const getLeaveMessage = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		const searchParamsAd = getState()[viewScope].get('searchParamsAd').toJS();
		const searchBaseParams = getState()[viewScope].get('searchBaseParams').toJS();
		const newParams = {...searchBaseParams, ...searchParamsAd,...params};

		dispatch({ type: viewScope + '_' + types.CARDLOADING, cardloading: true });
		API_MINECUSTOMER.getLeaveMessage(newParams).then((data) => {
			dispatch({
				type:types.MINECUSTOMER_INIT_LEAVEMESSAGE,
				leaveMessage:data.datas,
				cardloading:false,
			});
			
		});
	}
}

//我的客户--销售机会
export const getSellchanceCard = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		const searchParamsAd = getState()[viewScope].get('searchParamsAd').toJS();
		const searchBaseParams = getState()[viewScope].get('searchBaseParams').toJS();
		const newParams = {...searchBaseParams, ...searchParamsAd,...params};

		dispatch({ type: viewScope + '_' + types.CARDLOADING, cardloading: true });
		API_MINECUSTOMER.getSellchanceCard(newParams).then((data) => {
			dispatch({
				type:types.MINECUSTOMER_INIT_SELLCHANCECARD,
				sellChanceCard:data.datas,
				cardloading:false,
			});
			
		});
	}
}

//我的客户 -- 是否标记重要
export const isMarkToImportant = value =>{
	return (dispatch, getState) => {

		API_MINECUSTOMER.markImportant(value).then((data) => {
			if(data.status == "success"){
				//message.success("操作成功！");
				return true
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//我的客户 -- 设置自定义标签
export const markToCustomTagType =value =>{
	return (dispatch, getState) => {

		API_MINECUSTOMER.markLabel(value).then((data) => {
			if(data.status == "success"){
				message.success("操作成功！");
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//我的客户 -- 标签保存
export const crmTagEdit =value =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');

		API_MINECUSTOMER.crmTagEdit(value).then((data) => {
			if(data.status == "success"){
				dispatch(getMyCustomerTagsList({type:"crm"}));
				dispatch(getMyCustomerTags());
			}else{
				message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//我的客户 -- 标签s删除
export const crmTagDelete =value =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');

		API_MINECUSTOMER.crmTagDelete(value).then((data) => {
			if(data.status == "success"){
				message.success("操作成功！");
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//我的客户 -- 点击列表 -- 客户名称
export const viewCustomerDetail = (value) =>{
	return (dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });
		API_MINECUSTOMER.viewCustomerDetail({customerId:value}).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:types.MINECUSTOMER_SET_RIGHT,
					rightInfos:data.datas,
					loading:false,
					showRight:true
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//获取联系提醒数量
export const getRemindCount =params=>{
	return (dispatch, getState) => {
		API_CONTACT.getRemindCount(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:types.MINECUSTOMER_GET_REMINDCOUNT,
					remindCount:data.datas,
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//组件卸载时是否需要清除的数据
export const isClearNowPageStatus = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().mineCustomer.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.CLEAR_PAGE_STATUS, bool: bool })
	}
}