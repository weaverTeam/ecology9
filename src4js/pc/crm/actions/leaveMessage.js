import * as types from '../constants/ActionTypes'
import * as API_LEAVEMESSAGE from '../apis/leaveMessage'

import { Modal } from 'antd'
import objectAssign from 'object-assign'

import Immutable from 'immutable'

//初始化联系记录
export const initDatas = params => {
	return(dispatch, getState) => {
			dispatch({ type:types.LOADING, loading: true });
			API_LEAVEMESSAGE.getLeaveMessage(params).then((data) => {
				if(data.status=="success"){
                    dispatch({
                        type:types.INIT_VIEWMESSAGES,
                        viewMessages: data.datas,
                        loading:false,
                        customerId:params.customerId
                    });
                }else{
                     Modal.error({
                        title: '系统提示',
                        content: '查询失败,'+data.msgcode,
                    });
                }
				
			});
		}
}

//提交新的留言信息
export const saveLeaveMessage = params => {
    console.log(params)
	return (dispatch, getState) => {
			API_LEAVEMESSAGE.saveLeaveMessage(params).then((data) => {
				if(data.status=="success"){
                    dispatch(initDatas({from:"mine",customerId:params.customerId}));
                    dispatch(changeTextHeight(1));
                    dispatch(saveMessageFields());
                }else{
                     Modal.error({
                        title: '系统提示',
                        content: '操作失败,'+data.msgcode,
                    });
                }
				
			});
		}
}

//改变行高
export const changeTextHeight = h =>{
	return (dispatch,getState)=>{
		dispatch({
			type:types.LEAVEMESSAGE_CHANGE_ROWHEIGHT,
			value:h
		})
	}
}

//绑定form 更新内容
export const saveMessageFields =(value={})=>{
	return (dispatch,getState)=>{
		dispatch({
			type:types.LEAVEMESSAGE_SAVE_ORDER_FIELDS,
			value:value
		})
	}

}