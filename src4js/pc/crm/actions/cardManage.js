import * as types from '../constants/ActionTypes'
import * as API_CARDMANAGE from '../apis/cardManage'

import { Modal } from 'antd'
import objectAssign from 'object-assign'

import Immutable from 'immutable'

//切换tab
export const setTabKey = key => {
	return(dispatch, getState) => {
		dispatch({  type: types.CARDMANAGE_SET_DETAIL_TABKEY, key: key })	
	}
}

export const initTabKey =key =>{
    return(dispatch, getState) => {
		dispatch({  type: types.CARDMANAGE_INIT_TABKEY, key: key })	
	}
}

//高级搜索受控
export const setShowSearchAd = bool => {
	return(dispatch, getState) => {
		dispatch({ type: types.CARDMANAGE_SET_SHOW_SEARCHAD, value: bool })
	}
}

// 联系人信息获取
export const getContacterInfo = params => {
	return(dispatch, getState) => {
		const baseParams = getState()['crmCardManage'].get('baseParams').toJS();
		API_CARDMANAGE.getContacterCard(baseParams).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_CONTACTER_INFO, 
					contacterCardData: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

// 销售机会信息获取
export const getSellChanceInfo = params => {
	return(dispatch, getState) => {
		const baseParams = getState()['crmCardManage'].get('baseParams').toJS();

		API_CARDMANAGE.getSellchanceCard(baseParams).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_SELLCHANCE_INFO, 
					sellChanceCardData: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

// 客户联系  -- 高级搜索
export const contactLogCondition = params => {
	return(dispatch, getState) => {
		API_CARDMANAGE.contactLogCondition(params).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_CONTACTLOG_CONDITION, 
					contactLogCondition: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}
// 联系人  -- 高级搜索
export const contacterCardCondition = params => {
	return(dispatch, getState) => {
		API_CARDMANAGE.contacterCardCondition(params).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_CONTACTERCARD_CONDITION, 
					contacterCardCondition: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

// 客户留言  -- 高级搜索
export const leaveMessageCondition = params => {
	return(dispatch, getState) => {
		API_CARDMANAGE.leaveMessageCondition(params).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_LEAVEMESSAGE_CONDITION, 
					leaveMessageCondition: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}
// 销售机会  -- 高级搜索
export const sellchanceCardCondition = params => {
	return(dispatch, getState) => {
		API_CARDMANAGE.sellchanceCardCondition(params).then((data) => {
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_SELLCHANCE_CONDITION, 
					sellchanceCardCondition: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		});
	}
}

// 高级搜索  表单数据绑定
export const saveOrderFields = value => {
	return(dispatch, getState) => {
		dispatch({ type:types.CARDMANAGE_SAVE_ORDER_FIELDS, value: value })
	}
}

//保存传递过来的参数
export const setParamsFromLeft = params =>{
	return(dispatch, getState) => {
		dispatch({ type:types.CARDMANAGE_SAVE_LEFT_PARAMS, value: params })
	}
}

//单一改变searchParamsAd ，刷新数据重新请求
export const changeSearchParamsAd = value =>{
	return(dispatch, getState) => {
		const selectTabKey = getState()['crmCardManage'].get('selectTabKey');
		const highSearchParamsAd = getState()['crmCardManage'].get('highSearchParamsAd').toJS();
		const baseParams = getState()['crmCardManage'].get('baseParams').toJS();
		const newParams = {...baseParams,...highSearchParamsAd};
		dispatch({ type:types.CARDMANAGE_SAVE_LEFT_PARAMS, value: newParams })
		 
		if(selectTabKey == "2"){
			dispatch(getContacterInfo());
		}
		if(selectTabKey == "4"){
			dispatch(getSellChanceInfo());
		}
		
	}
}

//联系人新建  页面form获取
export const addContacterForm =params =>{
	return (dispatch,getState)=>{
		API_CARDMANAGE.getContacterAddForm(params).then(data =>{
			if(data.status == "success"){
				dispatch({ 
					type: types.CARDMANAGE_SET_ADDCONTACTER_CONDITION, 
					addContacterCondition: data.datas
				})
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		})
	}
}

//联系人新建  保存
export const saveContacter = params =>{
	return (dispatch,getState)=>{
		API_CARDMANAGE.saveContacterAdd(params).then(data =>{
			if(data.status == "success"){
				dispatch(getContacterInfo())
			}else{
				message.error("请求失败:"+data.msgcode);
			}
		})
	}
}

