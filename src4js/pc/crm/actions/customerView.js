import * as types from '../constants/ActionTypes'
import * as API_CUSTOMERVIEW from '../apis/customerView'
import * as API_CONTACT from '../apis/contact'
import * as API_COMMON from '../apis/commonApi'
import {Modal} from 'antd'
import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action;

import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import {message} from 'antd'
import Immutable from 'immutable'


//记录当前页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}

//初始化TAB页
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerView.get('nowRouterCmpath');

		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CUSTOMERVIEW.getCustomerTab(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: `${viewScope}_` + types.INIT_TABDATAS,
					tabDatas: data.datas,
					loading:false,
					customerId:params.customerId,
					hasright:data.hasright,
					rightLevel:data.rightLevel
				});
				
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}


//设置表单tabkey   
export const setDetailTabKey = (key) => {
	return (dispatch,getState)=>{
		dispatch({type:types.CUSTOMERVIEW_SET_DETAIL_TABKEY,detailTabKey:key,tabId:key})
	}
}


//共享设置  获取列表
export  const getShareSettingList =params =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerView.get('nowRouterCmpath');
		
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CUSTOMERVIEW.getShareList(params).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type: types.INIT_SHARESETTING_TABLE,
					shareDataKey: data.sessionkey,
					loading:false,
					customerId:params.customerId
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//共享设置  删除
export const shareSettingDelete = (params,cusid) =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.deleteShareList(params).then((data) => {
			if(data.status == "success"){
				dispatch(getShareSettingList({customerId:cusid}));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//共享设置 新建   表单受控
export const saveShareOrderFields =value =>{
	return(dispatch, getState) => {
		dispatch({  type:types.SAVE_SHARE_FIELDS, value: value })
	}
}
//共享设置  新建保存  
export const saveShareSetting =params =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.saveShareList(params).then((data) => {
			if(data.status == "success"){
				dispatch(getShareSettingList({customerId:params.customerId}));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//修改记录   查询列表
export const getViewLogList = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerView.get('nowRouterCmpath');
		
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CUSTOMERVIEW.getViewLogList(params).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type: types.INIT_VIEWLOG_TABLE,
					viewlogDataKey: data.sessionkey,
					loading:false,
					customerId:params.customerId
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//地址管理  获取列表
export const getViewAddressList = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerView.get('nowRouterCmpath');
		
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CUSTOMERVIEW.getViewAddressList(params).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type: types.INIT_VIEWADDRESS_TABLE,
					addressDataKey: data.sessionkey,
					loading:false,
					customerId:params.customerId
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//地址管理 新建
export const addressAddForm = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerView.get('nowRouterCmpath');

		API_CUSTOMERVIEW.viewAddressAddForm(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.INIT_VIEWADDRESS_ADDFORM,
					addressAddData: data.datas,
					isShowAddress:true,
					operate:"add"
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//地址管理  新建 保存

export const saveAddressAddForm =(cusid)=>{
	return(dispatch, getState) => {
		const addressSearchParams = getState().crmcustomerView.get('addressSearchParams').toJS();
		const params = {...addressSearchParams,customerId:cusid};

		API_CUSTOMERVIEW.saveAddressAdd(params).then((data) => {
			if(data.status == "success"){
				dispatch(getViewAddressList({customerId:cusid}));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
		
	}
}
//地址管理删除 
export const deleteAddress =(params,cusid) =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.deleteAddress(params).then((data) => {
			if(data.status == "success"){
				dispatch(getViewAddressList({customerId:cusid}));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//地址管理 新建表单受控
export const saveAddressOrderFields =value =>{
	return (dispatch,getState) =>{
		dispatch({  type:types.SAVE_ADDRESS_FIELDS, value: value })
	}
}

//地址管理  编辑 
export const addressEditForm = params =>{
	return(dispatch, getState) => {

		API_CUSTOMERVIEW.addressEditForm(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.INIT_VIEWADDRESS_ADDFORM,
					addressAddData: data.datas,
					isShowAddress:true,
					operate:"edit",
					addressId:params.addressId
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//地址管理  编辑 保存 
export const saveEditAddress =params =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.saveAddressEditForm(params).then((data) => {
			if(data.status == "success"){
				message.success('操作成功!');
				dispatch(getViewAddressList({customerId:params.customerId}))
			}else{
				message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//商机管理 获取列表
export const getListSellChance = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmcustomerView.get('nowRouterCmpath');
		
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CUSTOMERVIEW.getListSellChance(params).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type: types.INIT_LISTSELLCHANCE_TABLE,
					sellChanceDataKey: data.sessionkey,
					loading:false,
					customerId:params.customerId
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//客户卡片 商机管理  批量删除
export const deleteSellChance =params=>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.deleteSellChance(params).then((data) => {
			if(data.status == "success"){
				dispatch(getListSellChance({fromType:1,customerId:params.customerId}));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//客户价值   评估标准
export const getEvaluationStandard =params =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.getEvaluationStandard(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.INIT_EVALUATION_STANDARD,
					evalStandard: data.datas,
				});
			}else{
				message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//客户价值   列表信息
export const getEvaluationInfo =params =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.getEvaluationInfo(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.INIT_EVALUATION_TABLE,
					evalDataInfo: data.datas,
					customerId:params.customerId
				});
			}else{
				message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//客户价值  更新
export const evaluationUpdate =params =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.evaluationUpdate(params).then((data) => {
			if(data.status == "success"){
				dispatch(getEvaluationInfo({customerId:params.customerId}));
			}else{
				message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//获取消息提醒设置
export const getRemindInfo =id =>{
    return (dispatch, getState) => {
		API_CONTACT.getRemindInfo({customerId:id}).then((data) => {
			if(data.status == "success"){
				dispatch({ type: types.CUSTOMERVIEW_SAVE_REMINDINFO, remindInfo: data.datas })
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//保存消息提醒设置
export const saveRemindSet = params =>{
	return (dispatch, getState) => {
		API_CONTACT.saveRemindSetting(params).then((data) => {
			if(data.status == "success"){
				return true
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//获取客户价值的右键
export const getEvalRightMenu =params =>{
	return (dispatch, getState) => {
		API_COMMON.getRightMenuList(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:types.CUSTOMEREVAL_SAVE_RIGHTMENU,
					customereEvalRcList:data.datas
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}