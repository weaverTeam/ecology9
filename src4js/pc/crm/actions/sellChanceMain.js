import * as types from '../constants/ActionTypes'
import * as API_SELLCHANCEMAIN from '../apis/sellChanceMain'
import * as API_MINECUSTOMER from '../apis/mineCustomer'
import * as API_TABLE from '../apis/table'
import * as API_CONTACT from '../apis/contact'

import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import { Modal,message } from 'antd'
import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'

//记录当前流程页
export const setNowRouterCmpath = value => {
	return { type: types.SELLCHANCEMAIN_SET_NOW_ROUTER_PATH, path: value }
}

//初始化 高级搜索
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
	
		API_SELLCHANCEMAIN.getSellChanceCondition(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: `${viewScope}_` + types.INIT_CONDITION,
					conditioninfo: data.datas,
				});
			}else{
				 message.error('初始化信息失败,'+data.msgcode+"!");
			}
		});
	}
}

//搜索
export const doSearch = (params ) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		const searchBaseParams = getState()['crm' + viewScope].get('searchBaseParams').toJS();
		const searchParamsAd = getState()['crm' + viewScope].get('searchParamsAd').toJS();

		const fromType = getState()['crm' + viewScope].get('fromType').toJS();

		const newParams = {...searchBaseParams,...searchParamsAd, ...params};
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_SELLCHANCEMAIN.getSellChanceList(newParams).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type:`${viewScope}_` + types.INIT_TABLE,
					dataKey: data.sessionkey,
					loading:false,
					searchParams: {...searchBaseParams, ...params },
					showRight:false,
					fromType:{...fromType,crmids:data.crmIds}
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			};
		});
	}
}

//tabs页签读取
export const sellChanceTabs = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');

		API_SELLCHANCEMAIN.getSellChanceTab(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:`${viewScope}_` + types.INIT_TABS,
					tabDatas: data.datas,
				});
			}else{
				 message.error('tab标签请求失败,'+data.msgcode+"!");
			};
		});
	}
}
//tabs页签受控
export const setSellChanceTabKey = (key,num) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SET_DETAIL_TABKEY, key: key,istorefresh:num+1 });
		dispatch(saveOrderFields({}));
		dispatch(doSearch({labelid:key}));
		dispatch(getContactLogs({labelid:key}));

	}
}
//商机状态受控
export const sellChanceStatusAndHrm = params => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');

		API_SELLCHANCEMAIN.getSellChanceStatusAndHrm(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.INIT_SELLCHANCESTATUS,
					sellChanceStatus: data.datas.statusList,
					hrmList:data.datas.hrmList || [],
				});
			}else{
				 message.error('商机状态请求失败,'+data.msgcode+"!");
			};
		});
	}
}
//切换联系人tab
export const changeTab=key=>{
    return { type: types.SET_DETAIL_TABKEY, key: key }
} 

//保存新的人员列表
export const saveHrmList = value =>{
	return (dispatch, getState) => {
		dispatch({ type: types.SELLCHANCEMAIN_SAVE_HRM_LIST, value: value })
	}
}

//高级搜索受控
export const setShowSearchAd = bool => {
	return (dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SET_SHOW_SEARCHAD, value: bool })
	}
}

//高级搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}

//商机新建  获取 编辑表单
export const sellChanceForm = (params) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });
		dispatch(saveAddOrderFields());
		API_SELLCHANCEMAIN.getShellChanceForm(params).then(({datas,status,hasright}) => {
			if(status == "success"){
				if(hasright){
					dispatch({
						type: viewScope + '_' +types.INIT_NEW_CREATE,
						mainTableInfo: datas.mainTable,
						subTableInfo:datas.subTable,
						hasright:hasright,
						loading:false,
						sellChanceId:params.sellChanceId,
						customerId:datas.customerId,
						operate:params.operation
					});
					let fieldsObj =crmGetFields(datas.mainTable);
					dispatch(saveAddOrderFields(fieldsObj));
				}else{
					dispatch({
						type: viewScope + '_' +types.INIT_NEW_CREATE,
						mainTableInfo: [],
						subTableInfo:[],
						hasright:hasright,
						loading:false,
						sellChanceId:"",
						customerId:"",
						operate:params.operation
					});
				}
			}else{
				 message.error(datas.msgcode+"!");
			};
		});
	}
}
//商机新建  主表实时编辑保存
export const sellChanceEdit = (params) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		API_SELLCHANCEMAIN.saveSellChanceForm(params).then(({datas,status}) => {
			if(status == "success"){
				message.success("操作成功!");
			}else{
				 message.error(datas.msgcode+"!");
			};
		});
	}
}

//商机新建  明细表  产品保存
export const saveProduct = (params) => {
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');

		API_SELLCHANCEMAIN.saveProduct(params).then((datas) => {
			if(datas.status == "success"){
				message.success("操作成功!");
			}else{
				 message.error(datas.msgcode+"!");
			};
		});
	}
}

//新建商机  ==表单绑定列表
export const saveAddOrderFields =(value={})=>{
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: types.SELLCHANCE_SAVE_ADD_ORDER_FIELDS, value: value })
	}
}

//商机新建  保存新建页面
export const createSellChance = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');

		API_SELLCHANCEMAIN.createSellChance(params).then((data) => {
			if(data.status == "success"){
				dispatch({ type: types.SELLCHANCE_CREATE_SUCCESS, value: {} })
			}else{
				 message.error(data.msgcode+"!");
			};
		});
	}
}

//获取商机管理  自定义标签列表
export const getSellChanceTagsList = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');

		API_MINECUSTOMER.getMyCustomerTagList(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:`${viewScope}_` + types.INIT_TAGS_LIST,
					tagDatas: data.datas,
				});
			}else{
				 message.error('tab标签请求失败,'+data.msgcode+"!");
			};
		});
	}
}

//商机管理 -- 是否标记重要
export const isMarkToImportant = value =>{
	return (dispatch, getState) => {

		API_MINECUSTOMER.markImportant(value).then((data) => {
			if(data.status == "success"){
				//message.success("操作成功！");
				return true
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//商机管理 -- 设置自定义标签
export const markToCustomTagType =value =>{
	return (dispatch, getState) => {

		API_MINECUSTOMER.markLabel(value).then((data) => {
			if(data.status == "success"){
				message.success("操作成功！");
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//商机管理 -- 标签s删除
export const crmTagDelete =value =>{
	return (dispatch, getState) => {
		API_MINECUSTOMER.crmTagDelete(value).then((data) => {
			if(data.status == "success"){
				message.success("操作成功！");
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//商机管理 -- 标签保存
export const crmTagEdit =value =>{
	return (dispatch, getState) => {

		API_MINECUSTOMER.crmTagEdit(value).then((data) => {
			if(data.status == "success"){
				dispatch(sellChanceTabs());
				dispatch(getSellChanceTagsList({type:"sellchance"}));
			}else{
				message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//商机管理  右边tab切换
export const setRightTabKey = key =>{
	return (dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		const selectValue = getState()['crm' + viewScope].get('selectValue');
		dispatch({ type: viewScope + '_' + types.SET_RIGHT_DETAIL_TABKEY, key: key });
		if(key == 1){
			dispatch(getContactLogs({datatype:selectValue}));
		}
		if(key == 2){
			dispatch(getContacterCard({datatype:selectValue}));
		}
	}
}

//右侧上方时间日期value保存
export const saveSelectValue = val =>{
	return(dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_SELECT_VALUE, value: val })
	}
}

//商机管理--客户联系信息
export const getContactLogs = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		const searchParamsAd = getState()['crm' + viewScope].get('searchParamsAd').toJS();
		const searchBaseParams = getState()['crm' + viewScope].get('searchBaseParams').toJS();
		const fromType = getState()['crm' + viewScope].get('fromType').toJS();
		const newParams = {...searchBaseParams, ...searchParamsAd,...fromType,...params};

		dispatch({ type: viewScope + '_' + types.CARDLOADING, cardloading: true });
		API_MINECUSTOMER.getContactLogs(newParams).then((data) => {
			dispatch({
				type:types.SELLCHANCEMAIN_INIT_CONTACTLOGS,
				contactLogs:data.datas,
				cardloading:false,
			});
			
		});
	}
}
//商机管理--联系人
export const getContacterCard = params =>{
	return (dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		const searchParamsAd = getState()['crm' + viewScope].get('searchParamsAd').toJS();
		const searchBaseParams = getState()['crm' + viewScope].get('searchBaseParams').toJS();
		const fromType = getState()['crm' + viewScope].get('fromType').toJS();
		const newParams = {...searchBaseParams, ...searchParamsAd,...fromType,...params};

		dispatch({ type: viewScope + '_' + types.CARDLOADING, cardloading: true });
		API_MINECUSTOMER.getContacterCard(newParams).then((data) => {
			dispatch({
				type:types.SELLCHANCEMAIN_INIT_CONTACTERCARD,
				contacterCard:data.datas,
				cardloading:false,
			});
			
		});
	}
}

//商机管理  请求右侧详细信息
export const getSellChanceDetail = id =>{
	return (dispatch, getState) => {
		const viewScope = getState().crmsellChanceMain.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });
		API_SELLCHANCEMAIN.getSellChanceDetail({sellchanceId:id}).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:types.SELLCHANCEMAIN_SET_RIGHT,
					rightInfos:data.datas,
					loading:false,
					showRight:true
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//获取联系提醒数量
export const getRemindCount =params=>{
	return (dispatch, getState) => {
		API_CONTACT.getRemindCount(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type:types.SELLCHANCE_GET_REMINDCOUNT,
					remindCount:data.datas,
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//商机编辑tab切换
export const editSellChanceChangeTab =key =>{
	return (dispatch,getState)=>{
		dispatch({type:types.SELLCHANCEMAIN_SET_EDIT_TABKEY,key:key})
	}
}

//客户联系提醒数据
export const getRemindInfo =id=>{
	return (dispatch, getState) => {
		API_CONTACT.getRemindInfo({customerId:id}).then((data) => {
			if(data.status == "success"){
				dispatch({ type: types.SELLCHANCEMAIN_SAVE_REMINDINFO, remindInfo: data.datas })
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//客户联系提醒数据  保存
export const saveRemindSet =params =>{
	return (dispatch, getState) => {
		API_CONTACT.saveRemindSetting(params).then((data) => {
			if(data.status == "success"){
				return true
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}