import * as types from '../constants/ActionTypes'
import * as API_CONTACT from '../apis/contact'

import { WeaTable } from 'comsRedux'

const WeaTableAction = WeaTable.action;

import { Modal,message } from 'antd'
import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'

//记录当前流程页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}

//初始化高级搜索内容
export const initConditions = params => {
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');
		// const paramsBase = getState()['workflow' + viewScope].get('paramsBase').toJS();
		// const searchParams = getState()['workflow' + viewScope].get('searchParams').toJS();
		// const newParams = { ...paramsBase, ...searchParams, ...params };
		API_CONTACT.getContactCondition(params).then((data) => {
			dispatch({
				type: `${viewScope}_` + types.INIT_CONDITION,
				conditioninfo: data.datas,
			});
		});
	}
}

//搜索
export const doSearch = (params ) => {
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');
		const paramsBase = getState()[viewScope].get('paramsBase').toJS();
		const searchParams = getState()[viewScope].get('searchParams').toJS();
		const searchParamsAd = getState()[viewScope].get('searchParamsAd').toJS();

		const newParams = {...paramsBase,...searchParams,...searchParamsAd, ...params};
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CONTACT.getContactList(newParams).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
			dispatch({
				type:`${viewScope}_` + types.INIT_TABLE,
				dataKey: data.sessionkey,
				loading:false,
				paramsBase: { ...paramsBase,...params },
			});
		});
	}
}
//联系提醒列表
export const getRemindList =params =>{
	return (dispatch,getState) => {
		const viewScope = getState().contact.get("nowRouterCmpath");
		dispatch({ type: viewScope + '_' + types.LOADING, loading: true });

		API_CONTACT.getContactLogRemindList(params).then((data) => {
			dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
			dispatch({
				type:types.CONTACT_INIT_REMIND_TABLE,
				dataKey: data.sessionkey,
				loading:false
			});
		});
	}
}

//切换tab
export const setTabKey = key =>{
	return (dispatch,getState) => {
		const viewScope = getState().contact.get("nowRouterCmpath");
		dispatch({  type: viewScope + '_' + types.SET_DETAIL_TABKEY, key: key })
	}
}

//高级搜索受控
export const setShowSearchAd = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SET_SHOW_SEARCHAD, value: bool })
	}
}

//高级搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}
//联系情况--联系计划--完成
export const finishWorkPlan = params =>{
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');

		API_CONTACT.getFinishWorkPlan(params).then((data) => {
			if(data.status == "success"){
				dispatch(doSearch());
			}else{
				message.error("提交失败:"+data.msgcode);
			}
		});
	}
}
//组件卸载时是否需要清除的数据
export const isClearNowPageStatus = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.CLEAR_PAGE_STATUS, isToReq: bool })
	}
}
//组件卸载时需要清除的数据
export const unmountClear = bool => {
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.UNMOUNT_CLEAR, isToReq: bool })
	}
}

//获取联系提醒记录
export const getRemindCount =params=>{
	return(dispatch, getState) => {
		const viewScope = getState().contact.get('nowRouterCmpath');

		API_CONTACT.getRemindCount(params).then((data) => {
			if(data.status == "success"){
				dispatch({ type: types.CONTACT_SAVE_COUNT, remindCount: data.datas })
			}else{
				message.error("提交失败:"+data.msgcode);
			}
		});
	}
}
//客户提醒设置信息
export const getRemindInfo =params=>{
	return(dispatch, getState) => {
		API_CONTACT.getRemindInfo(params).then((data) => {
			if(data.status == "success"){
				dispatch({ type: types.CONTACT_SAVE_REMINDINFO, remindInfo: data.datas,customerId:params.customerId })
			}else{
				message.error("提交失败:"+data.msgcode);
			}
		});
	}
}
//保存客户提醒设置
export const saveRemindSetting =params =>{
	return(dispatch, getState) => {
		API_CONTACT.saveRemindSetting(params).then((data) => {
			if(data.status == "success"){
				return true
			}else{
				message.error("提交失败:"+data.msgcode);
			}
		});
	}
} 