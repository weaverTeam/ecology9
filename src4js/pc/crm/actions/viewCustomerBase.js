import * as types from '../constants/ActionTypes'
import * as API_CUSTOMERVIEW from '../apis/customerView'
import * as API_MINECUSTOMER from '../apis/mineCustomer'

import objectAssign from 'object-assign'

import cloneDeep from 'lodash/cloneDeep'

import { WeaTools } from 'ecCom'
import Immutable from 'immutable'
import { message } from 'antd';
//记录当前页
export const setNowRouterCmpath = value => {
	return { type: types.SET_NOW_ROUTER_PATH, path: value }
}

//初始化查
export const initDatas = params => {
	return(dispatch, getState) => {
		const viewScope = getState().viewCustomerBase.get('nowRouterCmpath');
		API_CUSTOMERVIEW.getCustomerView(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: `${viewScope}_` + types.INIT_CONDITION,
					conditioninfo: data.datas,
					rcList:data.rcList,
					title:data.customerName,
					customerId:params.customerId
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
			
		});
	}
}

//实时编辑
export const customerEdit =(params)=>{
	return(dispatch, getState) => {

		API_CUSTOMERVIEW.editCustomerInfo(params).then((data) => {
			if(data.status=="success"){
				message.success('操作成功！');
			}else{
				 message.error('操作失败,'+data.msgcode+"!");
			}
		});
	}
}
//搜索表单内容
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		const viewScope = getState().viewCustomerBase.get('nowRouterCmpath');
		dispatch({ type: viewScope + '_' + types.SAVE_ORDER_FIELDS, value: value })
	}
}

//获取联系人
export const getContacterInfo = params =>{
	return (dispatch,getState)=>{
		const viewScope = getState().viewCustomerBase.get('nowRouterCmpath');
		API_MINECUSTOMER.getContacterCard(params).then((data) => {
			if(data.status=="success"){
				dispatch({
					type:`${viewScope}_` + types.INIT_TABLE,
					contacterData:data.datas
				})
			}else{
				 message.error('操作失败,'+data.msgcode+"!");
			}
		});
	}
}
//更新基本信息   联系人明细表
export const updateContacterData =(data)=>{
	return(dispatch, getState) => {
		dispatch({type:types.VIEWCUSTOMERBASE_UPDATE_TABLE, data: data});
	}
}
//更新基本信息   联系人明细表   即时编辑 --- 新建保存
export const editContracterData= params =>{
	return(dispatch, getState) => {
        const para = {info:JSON.stringify(params)}
		API_CUSTOMERVIEW.editContracterData(para).then((data) => {
			if(data.status=="success"){
				if(params.id){
					message.success('操作成功!');
				}else{
					dispatch(getContacterInfo({from:"all",customerId:params.customerId}));
				}	
			}else{
				message.error('操作失败,'+data.msgcode+"!");
			}
		});
	}
}


