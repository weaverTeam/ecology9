import * as types from '../constants/ActionTypes'
import * as API_CUSTOMERVIEW from '../apis/customerView'

import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action;


import { WeaTools } from 'ecCom'
import {message,Modal} from 'antd'
import Immutable from 'immutable'


//获取外部用户列表
export const getOutResourceList =params =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.getOutResourceInfo(params).then((data) => {
			if(data.status == "success"){
				dispatch(WeaTableAction.getDatas(data.sessionkey,  1));
				dispatch({
					type: types.INIT_OUTRESOURCE_TABLE,
					outResourceDataKey: data.sessionkey,
					loading:false,
					customerId:params.customerId,
					showAddForm:false,
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//外部用户  新建
export const outResourceAdd = params =>{
	return(dispatch, getState) => {

		API_CUSTOMERVIEW.getOutResourceAdd(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.OUTRESOURCE_ADDFORM,
					addFormDatas: data.datas,
					showAddForm:true
				});
			}else{
				 message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//外部用户  新建 受控
export const saveOutResourceOrderFields= value =>{
	return(dispatch, getState) => {
		dispatch({  type:types.SAVE_OUTRESOURCE_FIELDS, value: value })
	}
}

//外部用户  新建 保存   
export const saveOutResourceForm = (params,required)=>{
	return(dispatch, getState) => {
		const outResourceParams = getState()["crmoutResources"].get('outResourceParams').toJS();

		const newParams = {...outResourceParams,...params};
		const require = (function(){
			for(let i=0;i<required.length;i++){
			   if(!outResourceParams[required[i]]){
				   return false
			   }
			}
		   return true;
	   })(); 
	   //console.log(outResourceParams,outResourceParams["seclevel"])
	   if(outResourceParams["seclevel"] && outResourceParams["seclevel"]<0){
			if(require){
				API_CUSTOMERVIEW.saveOutResourceAdd(newParams).then((data) => {
					if(data.status == "success"){
						dispatch(getOutResourceList({customerId:params.customerId}));
					}else{
							message.error('请求失败,'+data.msgcode+"!");
					}
				});
			}else{
				Modal.warning({
					title: '系统提示',
					content: '必填信息不完整!',
				});
			}
	   }else{
			Modal.warning({
				title: '系统提示',
				content: '外部用户安全级别只能为负数!',
			});
	   }	
	}
}

//外部用户 删除
export const outResourceDelete =(params,customerId) =>{
	return(dispatch, getState) => {
		API_CUSTOMERVIEW.saveOutResourceAdd(params).then((data) => {
			if(data.status == "success"){
				dispatch(getOutResourceList({customerId:customerId}));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
//外部用户   查看详情
export const getOutResourceDetail =(params,bool) =>{
	return(dispatch, getState) => {
		dispatch({ type:types.OUTRESOURCE_LOADING, loading: true });

		API_CUSTOMERVIEW.getOutResourceDetail(params).then((data) => {
			if(data.status == "success"){
				dispatch({
					type: types.OUTRESOURCE_VIEWFORM,
					viewDatas:data.datas,
					edit:bool,
					loading:false,
					resourceid:params.id
				});
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}

//外部用户  编辑  保存
export const saveOutResourceEdit = params =>{
	return(dispatch, getState) => {
		//const outResourceParams = getState()["crmoutResources"].get('outResourceParams').toJS();
		//const newParams = {...outResourceParams,...params};

		API_CUSTOMERVIEW.saveOutResourceAdd(params).then((data) => {
			if(data.status == "success"){
				dispatch(getOutResourceDetail({id:params.id,operate:'view'},false));
			}else{
					message.error('请求失败,'+data.msgcode+"!");
			}
		});
	}
}
