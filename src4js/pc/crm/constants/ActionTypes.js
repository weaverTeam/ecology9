/**
 * 列表公用
 */
export const SET_NOW_ROUTER_PATH = "set_now_router_path"    //获得查询key
export const SET_SHOW_SEARCHAD = 'set_show_searchad' //高级搜索受控
export const SAVE_ORDER_FIELDS = 'save_order_fields' //高级搜所表单
export const INIT_BASE = 'init_base' //加载基本数据
export const LOADING = "loading"  //正在加载
export const CARDLOADING = "cardloading"  //卡片正在加载
export const INIT_TABLE = "init_table" //初始化列表
export const SET_DETAIL_TABKEY = "set_detail_tabkey" //详情tab页切换
export const INIT_CONDITION= 'init_condition'   //高级搜索初始化
export const UNMOUNT_CLEAR = 'unmount_clear' //组件卸载需要清除的数据
export const CLEAR_PAGE_STATUS = 'clear_page_status' //是否需要清除页面状态
export const SAVE_DETAIL = 'save_detail' //实时保存详情
export const SAVE_ID =  "save_id"  //获取ID
export const INIT_TABDATAS =  "init_tabdatas"  //初始化tabye
export const INIT_TABS = 'init_tabs' //加载tab
export const INIT_FORM = 'init_form' //初始化form
export const INIT_TYPE = 'init_type' //初始化type
export const SAVE_REQUIRED = "save_required"  //保存必填字段
export const SET_RIGHT_DETAIL_TABKEY = "set_right_detail_tabkey" //右侧卡片tab切换
export const SAVE_SELECT_VALUE = "save_select_value" //select默认值
export const INIT_TAGS = 'init_tags'  //标签初始化
export const INIT_TAGS_LIST = 'init_tags_list'  //自定义标签初始化
export const INIT_NEW_CREATE = 'init_new_create'  //新建初始化
export const INIT_TITLEINFO = 'init_titleInfo'
/**
 * 新建客户
 */
export const ADDCUSTOMER_SET_DETAIL_TABKEY = 'addCustomer_set_detail_tabkey'   //切换tab
export const ADDCUSTOMER_SAVE_ORDER_FIELDS = 'addCustomer_save_order_fields' //高级搜所表单
export const ADDCUSTOMER_LOADING = 'addCustomer_loading'   //加载状态 
export const ADDCUSTOMER_INIT_FORM ='addCustomer_init_form'   //新建客户列表
export const ADDCUSTOMER_INIT_TYPE ='addCustomer_init_type'   //客户类型查询
export const ADDCUSTOMER_SEARCH ='addCustomer_search'   //客户类型查询
export const ADDCUSTOMER_SAVE_REQUIRED ='addCustomer_save_required'   //保存必填字段
export const SAVE_ADD_PARAMS = "save_add_params"   //保存新建的信息
export const ADD_INIT_IMPORTDATAS = "add_init_importdatas"  //批量导入接口
export const ADDCUSTOMER_UNMOUNT_CLEAR = 'addCustomer_unmount_clear'  //卸载组件状态

/**
 * 我的客户
 */
export const MINECUSTOMER_LOADING = 'mineCustomer_loading'   //加载状态 
export const MINECUSTOMER_INIT_TABS = 'mineCustomer_init_tabs' //初始化tab
export const MINECUSTOMER_INIT_TABLE = "mineCustomer_init_table"  //初始化列表
export const MINECUSTOMER_SET_DETAIL_TABKEY = 'mineCustomer_set_detail_tabkey' //改变tab
export const MINECUSTOMER_INIT_TAGS = 'mineCustomer_init_tags' //初始化列表上方数据
export const MINECUSTOMER_SAVE_ORDER_FIELDS = 'mineCustomer_save_order_fields' //高级搜所表单
export const MINECUSTOMER_SET_RIGHT_DETAIL_TABKEY = "mineCustomer_set_right_detail_tabkey" //右侧卡片tab切换
export const MINECUSTOMER_INIT_CONTACTLOGS = "mineCustomer_init_contactlogs" //客户联系列表
export const MINECUSTOMER_INIT_CONTACTERCARD = "mineCustomer_init_contacterCard"   //联系人
export const MINECUSTOMER_INIT_LEAVEMESSAGE = "mineCustomer_init_leaveMessage"   //客户留言
export const MINECUSTOMER_INIT_SELLCHANCECARD = "mineCustomer_init_sellchanceCard"   //销售机会
export const MINECUSTOMER_CARDLOADING = "mineCustomer_cardloading"  //卡片正在加载
export const MINECUSTOMER_SAVE_SELECT_VALUE = "mineCustomer_save_select_value" //select默认值
export const MINECUSTOMER_INIT_TAGS_LIST = 'mineCustomer_init_tags_list'  //自定义标签初始化
export const MINECUSTOMER_SET_RIGHT = "mineCustomer_set_right"    //点击客户名称，初始化右边列表
export const MINECUSTOMER_SAVE_HRM_LIST = "mineCustomer_save_hrm_list"  //更新人员列表
export const MINECUSTOMER_GET_REMINDCOUNT = "mineCustomer_get_remindCount"  
export const MINECUSTOMER_SAVE_INDUSTRY_LIST = "mineCustomer_save_industry_list"  //更新行业列表树
export const MINECUSTOMER_CLEAR_PAGE_STATUS = "mineCustomer_clear_page_status"  //清楚数据

export const CARDMANAGE_SET_DETAIL_TABKEY = "cardManage_set_detail_tabKey"  //卡片  tab切换
export const CARDMANAGE_SET_SHOW_SEARCHAD = "cardManage_set_show_searchad"    ///卡片高级搜索受控
export const CARDMANAGE_SET_CONTACTLOG_CONDITION = "cardManage_set_contactlog_condition" //联系记录高级搜
export const CARDMANAGE_SET_CONTACTERCARD_CONDITION = "cardManage_set_contactercard_condition" //联系人高级搜
export const CARDMANAGE_SET_LEAVEMESSAGE_CONDITION = "cardManage_set_leavemessage_condition" //客户留言高级搜
export const CARDMANAGE_SET_SELLCHANCE_CONDITION = "cardManage_set_sellchance_condition" //销售机会高级搜
export const CARDMANAGE_SAVE_ORDER_FIELDS = "cardManage_set_order_fields" //高级搜索数据绑定
export const CARDMANAGE_SAVE_LEFT_PARAMS = 'cardManage_save_left_params' //保存左边点击过来参数
export const CARDMANAGE_SET_CONTACTER_INFO = 'cardManage_set_contacter_info' //联系人信息
export const CARDMANAGE_SET_SELLCHANCE_INFO =  'cardManage_set_sellchance_info'   //销售机会信息
export const CARDMANAGE_SET_ADDCONTACTER_CONDITION = 'cardManage_set_addContacter_condition'  //联系人新建
/** 
 * 客户联系
 */
export const CONTACT_LOADING = 'contact_loading'   //加载状态 
export const CONTACT_INIT_TABS = 'contact_init_tabs' //初始化tab
export const CONTACT_INIT_TABLE = "contact_init_table"  //初始化列表
export const CONTACT_SET_DETAIL_TABKEY = 'contact_set_detail_tabkey' //改变tab
export const CONTACT_INIT_REMIND_TABLE = 'contact_init_remind_table'  //初始化联系提醒列表
export const CONTACT_INIT_CONDITION= 'contact_init_condition'   //高级搜索初始化
export const CONTACT_SET_SHOW_SEARCHAD = "contact_set_show_searchad"   //高级搜索显隐
export const CONTACT_SAVE_ORDER_FIELDS = 'contact_save_order_fields' //高级搜所表单
export const CONTACT_UNMOUNT_CLEAR = 'contact_unmount_clear' //组件卸载需要清除的数据
export const CONTACT_CLEAR_PAGE_STATUS = 'contact_clear_page_status' //是否需要清除页面状态
export const CONTACT_SAVE_COUNT = 'contact_save_count' //数量统计
export const CONTACT_SAVE_REMINDINFO = "contact_save_remindInfo"

/**
 * 查询联系人
 */
export const CONTACTQUERY_LOADING = 'contactQuery_lo`ading'   //加载状态 
export const CONTACTQUERY_SET_SHOW_SEARCHAD = "contactQuery_set_show_searchad"   //高级搜索显隐
export const CONTACTQUERY_SAVE_ORDER_FIELDS = 'contactQuery_save_order_fields' //高级搜所表单
export const CONTACTQUERY_INIT_CONDITION= 'contactQuery_init_condition'   //高级搜索初始化
export const CONTACTQUERY_INIT_TABLE = "contactQuery_init_table"  //初始化列表


//联系人详情
export const CONTACTERVIEW_SAVE_ORDER_FIELDS = 'contacterView_save_order_fields' //联系人详情搜所表单
export const CONTACTERVIEW_DETAIL = 'contacterView_detail' //实时保存详情
export const CONTACTERVIEW_SAVE_ID =  "contacterView_save_id"  //获取联系人ID
export const CONTACTERVIEW_INIT_CONDITION= 'contacterView_init_condition'   //初始化信息
export const CONTACTERVIEW_INIT_TRAILINFO = "contacterView_init_trailInfo"  //联系人轨迹信息

export const INIT_VIEWLOGS  = 'init_viewlogs'   //联系人记录信息
export const CUSTOMERCONTACT_SET_CONTACTER_INFO = "customerContact_set_contacter_info"  //联系人下拉框
export const CUSTOMERCONTACT_SET_SELLCHANCE_INFO = "customerContact_set_sellchance_info"  //商机下拉框
export const CUSTOMERCONTACT_SAVE_RIGHTMENU = "customerContact_save_rightmenu" // 联系记录右键
export const CUSTOMERCONTACT_TO_REFRESH = "customerContact_to_refresh"  //刷新页面
export const CUSTOMERCONTACT_SAVE_ORDER_FIELDS = "customerContact_save_order_fields"  //保存
export const CUSTOMERCONTACT_CHANGE_ROWHEIGHT = "customerContact_change_rowHeight"
/**
 * 客户留言
 */
export const INIT_VIEWMESSAGES = 'init_viewMessages' //客户留言信息
export const LEAVEMESSAGE_CHANGE_ROWHEIGHT = "leaveMessage_change_rowHeight"
export const LEAVEMESSAGE_SAVE_ORDER_FIELDS = "leaveMessage_save_order_fields"

//客户信息详情
export const CUSTOMERVIEW_INIT_TABDATAS= 'customerView_init_tabdatas'   //初始化信息
export const CUSTOMERVIEW_LOADING = 'customerView_loading'   //加载状态 
export const CUSTOMERVIEW_SET_DETAIL_TABKEY = "customerView_set_detail_tabkey"
export const CUSTOMERVIEW_SAVE_REMINDINFO = "customerView_save_remindInfo"   //客户联系消息提醒

export const VIEWCUSTOMERBASE_INIT_CONDITION= 'viewCustomerBase_init_condition'   //高级搜索初始化
// export const VIEWCUSTOMERBASE_SAVE_ID =  "viewCustomerBase_save_id"  //客户id
export const VIEWCUSTOMERBASE_SAVE_ORDER_FIELDS = 'viewCustomerBase_save_order_fields' //客户详情搜所表单
export const VIEWCUSTOMERBASE_INIT_TABLE = 'viewCustomerBase_init_table' //客户详情   联系人列表
export const VIEWCUSTOMERBASE_UPDATE_TABLE = 'viewCustomerBase_update_table' //客户详情   联系人列表  更新
export const VIEWCUSTOMERBASE_CUSTOMER_MERGE = 'viewCustomerBase_customer_merge' //客户详情  右键  客户合并
export const VIEWCUSTOMERBASE_SHOWDIALOG = 'viewCustomerBase_showdialog' //模态框
export const VIEWCUSTOMERBASE_INIT_DIALOG = "viewCustomerBase_init_dialog"
export const VIEWCUSTOMERBASE_SAVE_BUSINESSINFO = "viewCustomerBase_save_businessInfo"   //获取工商信息
export const VIEWCUSTOMERBASE_SET_BUSINESSINFO_TAB = "viewCustomerBase_set_businessInfo_tab"   //工商信息 tab切换
export const VIEWCUSTOMERBASE_SET_BUSINESSINFO_DATAKEY = "viewCustomerBase_set_businessInfo_datakey" //工商信息 datakey
//共享设置
export const INIT_SHARESETTING_TABLE = 'init_shareSetting_table'  //初始化共享设置table
export const SAVE_SHARE_FIELDS = "save_share_fields" //

//修改记录
export const INIT_VIEWLOG_TABLE = "init_viewlog_table"

//地址管理
export const INIT_VIEWADDRESS_TABLE ="init_viewaddress_table" //列表
export const INIT_VIEWADDRESS_ADDFORM = 'init_viewaddress_addform'  //新建表单
export const SAVE_ADDRESS_FIELDS = "save_address_fields"  //
//客户卡片 商机管理
export const INIT_LISTSELLCHANCE_TABLE = 'init_listSellChance_table'  //table列表

//客户价值
export const INIT_EVALUATION_STANDARD = 'init_evaluation_standard'
export const INIT_EVALUATION_TABLE = 'init_evaluation_table'
export const CUSTOMEREVAL_SAVE_RIGHTMENU = "customerEval_save_rightMenu"
/**
 * 查询客户
 */
export const SEARCHRESULT_LOADING = 'searchResult_loading'   //加载状态 
export const SEARCHRESULT_SET_SHOW_SEARCHAD = "searchResult_set_show_searchad"   //高级搜索显隐
export const SEARCHRESULT_SAVE_ORDER_FIELDS = 'searchResult_save_order_fields' //高级搜所表单
export const SEARCHRESULT_INIT_BASE= 'searchResult_init_base'   //基本数据初始化
export const SEARCHRESULT_INIT_TABLE = "searchResult_init_table"  //初始化列表
export const SEARCHRESULT_INIT_TITLEINFO = 'searchResult_init_titleInfo'  //头部右键等信息
export const SEARCHRESULT_SAVE_ID =  "searchResult_save_id"  //获取ID
export const SEARCHRESULT_INIT_CUSTOMERNAME = 'searchResult_init_customername'  //客户名称
export const SEARCHRESULT_CLEAR_CUSTOMER = 'searchResult_clear_customer'
/**
 * 商机管理
 */
export const SELLCHANCEMAIN_LOADING = 'sellChanceMain_loading'   //加载状态 
export const SELLCHANCEMAIN_SET_SHOW_SEARCHAD = "sellChanceMain_set_show_searchad"   //高级搜索显隐
export const SELLCHANCEMAIN_SAVE_ORDER_FIELDS = 'sellChanceMain_save_order_fields' //高级搜所表单
export const SELLCHANCEMAIN_INIT_CONDITION= 'sellChanceMain_init_condition'   //高级搜索初始化
export const SELLCHANCEMAIN_INIT_TABLE = "sellChanceMain_init_table"  //初始化列表
export const CONTROL_MODLE = 'control_modle' //控制modle
export const INIT_SELLCHANCESTATUS = 'init_sellChanceStatus' //初始化商机状态
export const SELLCHANCEMAIN_INIT_TABS = 'sellChanceMain_init_tabs' //初始化tab
export const SELLCHANCEMAIN_SET_DETAIL_TABKEY = 'sellChanceMain_set_detail_tabkey' //改变tab
export const SELLCHANCEMAIN_INIT_NEW_CREATE = 'sellChanceMain_init_new_create'  //新建商机
export const SELLCHANCE_SAVE_ADD_ORDER_FIELDS = "sellChance_save_add_order_fields"  //新建商机保存填写数据
export const SELLCHANCE_CREATE_SUCCESS = "sellChance_create_success" //新建成功
export const SELLCHANCEMAIN_INIT_TAGS_LIST = 'sellChanceMain_init_tags_list'  //自定义标签初始化
export const SELLCHANCEMAIN_SAVE_HRM_LIST = "sellChanceMain_save_hrm_list"  //更新人员列表
export const SELLCHANCEMAIN_SET_RIGHT_DETAIL_TABKEY = "sellChanceMain_set_right_detail_tabkey" //右侧卡片tab切换
export const SELLCHANCEMAIN_SAVE_SELECT_VALUE = "sellChanceMain_save_select_value" //select默认值
export const SELLCHANCEMAIN_INIT_CONTACTLOGS = "sellChanceMain_init_contactlogs" //客户联系列表
export const SELLCHANCEMAIN_INIT_CONTACTERCARD = "sellChanceMain_init_contacterCard"   //联系人
export const SELLCHANCEMAIN_CARDLOADING = "sellChanceMain_cardloading"  //卡片正在加载
export const SELLCHANCEMAIN_SET_RIGHT = "sellChanceMain_set_right" //右侧信息获取
export const SELLCHANCE_GET_REMINDCOUNT ="sellChance_get_remindCount" //商机里面联系提醒统计
export const SELLCHANCEMAIN_SET_EDIT_TABKEY = 'sellChanceMain_set_edit_tabkey' //改变商机编辑的tab
export const SELLCHANCEMAIN_SAVE_REMINDINFO = "sellChanceMain_save_remindinfo" //联系提星设置数据
export const SELLCHANCEMAIN_SET_NOW_ROUTER_PATH = "sellChanceMain_set_now_router_path"
/**
 * 客户生日
 */
export const CUSTOMERBIRTHDAY_LOADING = 'customerBirthday_loading'   //加载状态 
export const CUSTOMERBIRTHDAY_SET_SHOW_SEARCHAD = "customerBirthday_set_show_searchad"   //高级搜索显隐
export const CUSTOMERBIRTHDAY_SAVE_ORDER_FIELDS = 'customerBirthday_save_order_fields' //高级搜所表单
export const CUSTOMERBIRTHDAY_INIT_CONDITION= 'customerBirthday_init_condition'   //高级搜索初始化
export const CUSTOMERBIRTHDAY_INIT_TABLE = "customerBirthday_init_table"  //初始化列表

/**
 * 外部用户
 */
export const INIT_OUTRESOURCE_TABLE = 'init_outResource_table'   //初始化外部用户列表
export const OUTRESOURCE_ADDFORM = "outResource_addForm"  //外部用户新建表单
export const SAVE_OUTRESOURCE_FIELDS = "save_outResource_fields"  //外部用户  新建表单受控
export const OUTRESOURCE_VIEWFORM = "outResource_viewForm"  //查看
export const OUTRESOURCE_LOADING= "outResource_loading"  

/**
 * 查询合同
 */
export const CONTRACTQUERY_LOADING = 'contractQuery_loading'   //加载状态 
export const CONTRACTQUERY_SET_SHOW_SEARCHAD = "contractQuery_set_show_searchad"   //高级搜索显隐
export const CONTRACTQUERY_SAVE_ORDER_FIELDS = 'contractQuery_save_order_fields' //高级搜所表单
export const CONTRACTQUERY_INIT_CONDITION= 'contractQuery_init_condition'   //高级搜索初始化
export const CONTRACTQUERY_INIT_TABLE = "contractQuery_init_table"  //初始化列表
export const CONTRACTQUERY_SHOW_DIALOG = "contractQuery_show_dialog"  //显示合同卡片模态窗口
export const CONTRACTQUERY_SHOW_DIALOG_DATA = "contractQuery_show_dialog_data"  //显示合同卡片模态窗口数据
export const CONTRACTQUERY_CLOSE_DIALOG = "contractQuery_close_dialog"  //关闭合同卡片模态窗口
export const CONTRACTQUERY_SET_MODAL_TAB = "contractQuery_set_modal_tab"  //模态框tab切换
export const CONTRACTQUERY_SET_SHARELIST = "contractQuery_set_sharelist"  //共享列表
export const CONTRACTQUERY_SAVE_SHAREDATA = 'contractQuery_save_sharedata'
export const CONTRACTQUERY_SET_EXCHANGELIST = "contractQuery_set_exchangelist" //相关交流列表
export const CONTRACTQUERY_SET_CONTRACTINFO = "contractQuery_set_contractinfo" //合同基本信息
export const CONTRACTQUERY_SAVE_INFO_FIELDS = "contractQuery_save_info_fields"
export const CONTRACTQUERY_SAVE_INFO_REQUIRE= "contractQuery_save_info_require"
export const CONTRACTQUERY_SAVE_INFO_PRODUCT= "contractQuery_save_info_product"
export const CONTRACTQUERY_SAVE_INFO_PAY= "contractQuery_save_info_pay"
export const CONTRACTQUERY_CLEAR_INFO = "contractQuery_clear_info"
export const CONTRACTQUERY_CLEAR_SAVE_INFO = "contractQuery_clear_save_info"   //清楚保存的暂存信息