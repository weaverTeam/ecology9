// 定义模块路由与模块菜单的映射
export const MODULE_ROUTE_MAP = {
    'portal':   {id: 0, name: '我的门户'},
    'workflow': {id: 1, name: '我的流程'},
    'document': {id: 2, name: '我的知识'},
    'hrm': {id: 5, name: '我的人事'},
    'meeting': {id: 6, name: '我的会议'},
    'report': {id: 110, name: '我的报表'},
    'workplan': {id: 140, name: '我的日程'},
    'blog': {id: 392, name: '我的微博'},
    'inte': {id: -2774, name: '集成测试'},
    'crm': {id: 3, name: '我的客户'},
};