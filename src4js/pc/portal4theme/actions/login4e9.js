import {WeaTools} from 'ecCom';
import * as THEME_API from '../apis/theme';
import {LOGIN4E9} from '../constants/ActionTypes';
import ECLocalStorage from '../util/ecLocalStorage';
import { message } from 'antd';
/**
 * 加载登录页图片
 *
 * @returns {function(*, *)}
 */
export function loadLoginImages() {
    return (dispatch, getState) => {
        THEME_API.getLoginImages().then((result) => {
            let settings = result.settings;
            let loginBgImages = result.bgsrc;
            let loginLogoImage = settings.logoImage;
            ECLocalStorage.set('login', 'loginLogoImage', loginLogoImage, false);
            let loginBgImage = getState().login4e9.get('loginBgImage');
            if (!loginBgImage) {
                loginBgImage = settings.bgImage;
                ECLocalStorage.set('login', 'loginBgImage', loginBgImage, false); 
            }  
            let initialSlide = 0;
            for (var i = 0; i < loginBgImages.length; i++) {
                if(loginBgImage === loginBgImages[i]) {
                    initialSlide = i;
                    break;
                }
            }
            dispatch({
                type: LOGIN4E9.LOGIN4E9_IMAGES,
                value: {
                    loginBgImage,
                    loginLogoImage,
                    loginBgImages,
                    settings,
                    initialSlide,
                    autoCarousel: settings.autoCarousel,
                    carouselTime: settings.carouselTime,
                }
            });
        });
    }
}

/**
 * 切换登录页背景图片
 *
 * @param loginBgImage     登录页背景图片
 *
 * @returns {function(*, *)}
 */
export function changeLoginBgImage(loginBgImage, initialSlide) {
    ECLocalStorage.set('login', 'loginBgImage', loginBgImage, false);
    return (dispatch) => {
        dispatch({
            type: LOGIN4E9.LOGIN4E9_IMAGES,
            value: {
                loginBgImage,
                initialSlide,
            }
        });
    }
}

/**
 * 加载登录页背景图片库
 *
 * @returns {function(*, *)}
 */
export function loadLoginBgImages() {
    return (dispatch) => {
        THEME_API.getLoginImages().then((result) => {
            let loginBgImages = result.bgsrc;
            dispatch({
                type: LOGIN4E9.LOGIN4E9_IMAGES,
                value: {
                    loginBgImages: loginBgImages
                }
            });
        });
    }
}

/**
 * 显示隐藏登录页背景图片库
 *
 * @param visible     是否显示
 *
 * @returns {function(*, *)}
 */
export function changeLoginBgImagesVisible(visible) {
    return (dispatch) => {
        dispatch({
            type: LOGIN4E9.LOGIN4E9_IMAGES,
            value: {
                loginBgImagesVisible: visible
            }
        });
    }
}


/**
 * 更新最新轮播下标
 *
 * @param initialSlide     轮播下标
 *
 * @returns {function(*, *)}
 */
export function changeInitialSlide(initialSlide) {
    return (dispatch) => {
        dispatch({
            type: LOGIN4E9.LOGIN4E9_IMAGES,
            value: {
                initialSlide
            }
        });
    }
}

/**
 * 是否自动轮播
 *
 * @param autoCarousel     是否轮播
 *
 * @returns {function(*, *)}
 */
export function changeAutoCarousel(autoCarousel) {
    return (dispatch) => {
        dispatch({
            type: LOGIN4E9.LOGIN4E9_IMAGES,
            value: {
                autoCarousel
            }
        });
    }
}

/**
 * 轮播时间
 *
 * @param carouselTime     轮播时间
 *
 * @returns {function(*, *)}
 */
export function changeCarouselTime(carouselTime) {
    return (dispatch) => {
        dispatch({
            type: LOGIN4E9.LOGIN4E9_IMAGES,
            value: {
                carouselTime
            }
        });
    }
}

/**
 * 保存轮播信息
 *
 * @returns {function(*, *)}
 */
export function saveLoginInfo(){
    return (dispatch, getState) => {
        const login4e9 = getState().login4e9.toJSON();
        const params = {
            loginTemplateId: login4e9.settings.id,
            autoCarousel: login4e9.autoCarousel,
            carouselTime: login4e9.carouselTime,
        };
        THEME_API.saveLoginInfo(params).then((result) => {
            if(result.status === 'failed'){
                message.error(result.errormsg,3);
            }else{
                message.success(result.successmsg);
            }
        });
    }
}

