import React from 'react';
import { Carousel } from 'antd';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as loginActions from '../../../actions/login';
import * as login4e9Actions from '../../../actions/login4e9';

import ECLocalStorage from '../../../util/ecLocalStorage';

import E9MultiLang from './E9MultiLang';
import E9Form from './E9Form';
import E9QRCode from './E9QRCode';
import E9BgImages from './E9BgImages';
class E9Login extends React.Component {
    componentWillMount() {
        const {loginActions, login4e9Actions} = this.props;
        loginActions.loadLoginFormSetting();
        loginActions.loadLoginQRCode();
        login4e9Actions.loadLoginImages();
    }
    onChangeLoginType() {
        const {loginActions, loginType} = this.props;
        loginActions.changeLoginType(loginType);
    }
    afterChange(current){
        const {login4e9Actions} = this.props;
        login4e9Actions.changeInitialSlide(current);
    }
    render() {
        const {loginBgImage, loginLogoImage, loginBgImages, loginType, settings, initialSlide, autoCarousel, carouselTime } = this.props;
        const { id, isLock, showFooter, footerContent, footerBgColor, footerColor, changeBgImage, logoPosition, areaPosition, areaOpacity,
        logoBoxVisible, areaBgColor, showQrcode, qrcodeMiddle, formOpacity, footerOpacity, bgImages } = settings;
        if(!id) return <div/>
        const clientWidth = top.document.body.clientWidth;
        const clientHeight = top.document.body.clientHeight;
        const areaWidth = 406;
        const areaHeight = 544 - (logoBoxVisible ? 0 : 190);
        let areaStyle = {
            top: areaPosition.y * (clientHeight - areaHeight),
            left: areaPosition.x * (clientWidth - areaWidth),
        }
        let logoStyle = {
            top: logoPosition.y * (clientHeight - 136),
            left: logoPosition.x * (clientWidth - 178),
        }
        let isForm = loginType == 'form';
        let bgImageHtml = <img style={{height: clientHeight, width:'100%'}} src={loginBgImage} alt="图片加载失败" />
        if(autoCarousel){
            let bgHtml = loginBgImages.map((imgSrc, index) => {
                return  <img style={{height: clientHeight}} src={imgSrc} alt="图片加载失败" />;
            });
            bgImageHtml = <Carousel autoplay={true} dots={false} slickGoTo={initialSlide} initialSlide={initialSlide} autoplaySpeed={carouselTime} afterChange={this.afterChange.bind(this)}>
                      {bgHtml}
                    </Carousel>
        }
        return (<div className="e9login-container-box">
            <div className="e9login-container" style={{height:'100%'}}>
                {isLock ? '' : <div className="e9login-logo-setting" style={logoStyle}>
                    <img className="e9login-logo-img" src={loginLogoImage} alt="" />
                </div>}
                <div className="e9login-area" style={areaStyle}>
                    <div className="e9login-area-shadow" style={{background:areaBgColor, opacity: areaOpacity || 0.4}}></div>
                    {showQrcode && !qrcodeMiddle ? <div className="e9login-toggle" onClick={this.onChangeLoginType.bind(this)}>
                            {isForm ? <i className="wevicon wevicon-e9login-scan" /> : <i className="wevicon wevicon-e9login-back" />}
                    </div> : '' }
                    <div className="e9login-area-content">
                        {logoBoxVisible ? <div className="e9login-logo">{isLock ? <img className="e9login-logo-img" src={loginLogoImage} alt="" /> : ''}</div> : ''}
                        <div className="e9login-form-box" style={{opacity: formOpacity || 1}}>
                          <E9MultiLang onChangeLoginType={this.onChangeLoginType.bind(this)}/> {isForm ? <E9Form /> : <E9QRCode/>}
                        </div>
                    </div>
                    <div style={{height:'26px',width:'100%'}}/>
                </div>
            {changeBgImage ? <E9BgImages /> : ''}
            {bgImageHtml}
            {showFooter ? <div className="e9login-footer" style={{backgroundColor:footerBgColor,color:footerColor,opacity:footerOpacity}}>
                         <input type="text" value={footerContent}  disabled={true}></input>
                   </div> : ''}
            </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const {login, login4e9} = state;
    return {
        loginType: login.get('loginType'),
        loginBgImage: login4e9.get('loginBgImage'),
        initialSlide: login4e9.get('initialSlide'),
        autoCarousel: login4e9.get('autoCarousel'),
        carouselTime: login4e9.get('carouselTime'),
        loginLogoImage: login4e9.get('loginLogoImage'),
        settings: login4e9.get("settings").toJSON(),
        loginBgImages: login4e9.get('loginBgImages'),
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginActions: bindActionCreators(loginActions, dispatch),
        login4e9Actions: bindActionCreators(login4e9Actions, dispatch),
        dispatch: dispatch
    }
};

module.exports = connect(mapStateToProps, mapDispatchToProps)(E9Login);