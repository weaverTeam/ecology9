import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as login4e9Actions from '../../../actions/login4e9';
import { InputNumber, Button } from 'antd';
import { WeaCheckbox } from 'ecCom';
class E9BgImages extends React.Component {
    render() {
        const { loginBgImages, loginBgImagesVisible, loginBgImage, autoCarousel, carouselTime, actions } = this.props;
        let images = loginBgImages.toJSON().map((item, index) => {
            let class4selected = loginBgImage == item ? 'e9login-bg-images-image-selected' : '';
            return (
                <img key={index}
                     className={`e9login-bg-images-image ${class4selected}`}
                     src={item}
                     alt=""
                     onClick={actions.changeLoginBgImage.bind(this, item, index)}
                />
            )
        });
        return (
            <div className="e9login-bg-images">
                <div className="e9login-bg-images-btn" onClick={actions.changeLoginBgImagesVisible.bind(this,!loginBgImagesVisible)}>
                    <div className="e9login-bg-images-shadow e9login-bg-images-shadow-icon"></div>
                    <div className="e9login-bg-images-btn-icon">
                        <i className="wevicon wevicon-e9login-palette" />
                    </div>
                </div>
                {
                    loginBgImagesVisible ? (
                            <div className="e9login-bg-images-container" onMouseLeave={actions.changeLoginBgImagesVisible.bind(this,!loginBgImagesVisible)}> 
                                <div className="e9login-bg-images-toolbar">
                                    <div className="e9login-setting-label">单击切换背景</div>
                                    <WeaCheckbox value={autoCarousel} onChange={actions.changeAutoCarousel.bind(this,!autoCarousel)}/>
                                    <div className="e9login-setting-label">自动轮播</div>
                                    {autoCarousel ? <div className="e9login-setting-label">轮播速度:</div> : ''}
                                    {autoCarousel ? <InputNumber defaultValue={carouselTime} onChange={actions.changeCarouselTime.bind(this)} min={1000} style={{width:50,marginTop:'-4px'}}/> : ''}
                                    <Button size="small" type="primary" style={{position:'absolute',right:5,top:10}} onClick={actions.saveLoginInfo.bind(this)}>保存</Button>
                                </div>
                                <div className="e9login-bg-images-shadow  e9login-bg-images-shadow-picture"></div>
                                <div className="e9login-bg-images-images">
                                    {images}
                                </div>
                            </div>
                        ) : ''
                }
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    const {login4e9}=state;
    return {
        loginBgImages: login4e9.get('loginBgImages'),
        loginBgImagesVisible: login4e9.get('loginBgImagesVisible'),
        loginBgImage: login4e9.get('loginBgImage'),
        autoCarousel: login4e9.get('autoCarousel'),
        carouselTime: login4e9.get('carouselTime'),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(login4e9Actions, dispatch)
    }
};

module.exports = connect(mapStateToProps, mapDispatchToProps)(E9BgImages);