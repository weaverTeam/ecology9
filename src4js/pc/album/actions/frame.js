import * as types from '../constants/ActionTypes'

export const doLoading = (loading = false) => {
	return {
		type: types.FRAME_LOADING,
		loading
	}
}