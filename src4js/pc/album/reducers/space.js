import * as types from '../constants/ActionTypes'

let initialState = {
	title: "相册空间",
	loading: false,
};

export default function space(state = initialState, action) {
	switch(action.type) {
		case types.SPACE_LOADING:
			return {...state, loading: action.loading};
		default:
			return state
	}
}