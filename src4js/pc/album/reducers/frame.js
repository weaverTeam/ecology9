import * as types from '../constants/ActionTypes'

let initialState = {
	title: "相册主页",
	loading: false,
};

export default function frame(state = initialState, action) {
	switch(action.type) {
		case types.FRAME_LOADING:
			return {...state, loading: action.loading};
		default:
			return state
	}
}