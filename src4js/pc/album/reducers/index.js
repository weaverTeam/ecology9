import albumFrame from "./frame"
import albumSpace from "./space"

export default {
    albumFrame,
    albumSpace
}