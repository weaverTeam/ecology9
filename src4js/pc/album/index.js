import Route from "react-router/lib/Route"

import Home from "./components/Home"
import Frame from "./components/Frame"
import Space from "./components/Space"

import "./css/icon.css"

import reducer from "./reducers/"
import * as FrameAction from "./actions/frame"
import * as SpaceAction from "./actions/space"

const AlbumRoute = (
	<Route path="album" component={ Home }>
	    <Route name="frame" path="frame" component={ Frame } />
	    <Route name="space" path="space" component={ Space } />
  	</Route>
)

module.exports = {
	Route: AlbumRoute,
	reducer,
	action: {
		FrameAction,
		SpaceAction
	}
}