const Home = props => {
	return(
		<div style={{height:"100%"}}>
        	{props.children}
        </div>
	)
}

export default Home