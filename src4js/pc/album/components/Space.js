import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as SpaceAction from '../actions/space'

//import Immutable from 'immutable'
//const is = Immutable.is;

import {
	WeaErrorPage,
	WeaTools,
} from 'ecCom'


//import { WeaTable } from 'comsRedux'

class Space extends React.Component {
	constructor(props) {
		super(props);
	}
	componentDidMount() {
		//一些初始化请求
		let { actions } = this.props;
	}
	componentWillReceiveProps(nextProps) {
		const keyOld = this.props.location.key;
		const keyNew = nextProps.location.key;
		//点击菜单路由刷新组件
		if(keyOld !== keyNew) {

		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		//组件渲染控制
		return true
	}
	componentWillUnmount() {
		//组件卸载时一般清理一些状态

	}
	render() {
		let { loading, title } = this.props;
		return (
			<div className='wea-album-frame'>
				{ loading ? '' : title }
            </div>
		)
	}
}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

Space = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(Space);

//form 表单与 redux 双向绑定
//Space = createForm({
//	onFieldsChange(props, fields) {
//		props.actions.saveFields({ ...props.fields, ...fields });
//	},
//	mapPropsToFields(props) {
//		return props.fields;
//	}
//})(Space);


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	return { ...state.albumSpace }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(SpaceAction, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Space);