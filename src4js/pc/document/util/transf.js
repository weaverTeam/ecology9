export const transfStr = (name = '', ids = '', list = [], type = '') => {
  let str = '';
  const mirror = {
    37: "doc",
    152: "workflow",
  }
  list.map(item => {
    if(name === 'Upload' && type === 'image') {
      str += `<img style='width: 100px;height: 100px;' src='${item.loadlink || item.filelink}' />`;
    }
    if(name === 'Upload' && type === 'file') {
      str += `<a href='${item.filelink}' unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>${item.filename}</a>${item.showLoad ? "&nbsp;<a href='javascript:void(0)' unselectable='off' contenteditable='false' href='${item.loadlink}' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>下载(" + item.filesize + ")</a>" : ""}<br>`;
    }
    if(name === 'Browser') {
      str += `<a onclick='ecCom.WeaRichText.openAppLink(this,${item.id},"${type}")' href='javascript:void(0);' unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>${item.name || item.showname}</a>&nbsp;&nbsp;&nbsp;`;
    }
  })
  return str
}

export const transfTime = (day, time) => {
  let oldDate = new Date(`${day} ${time}`),
    nowDate = new Date(),
    duration = nowDate.getTime() - oldDate,
    oneDay = 24 * 3600 * 1000,
    oneHour = 3600 * 1000,
    oneMinute = 60000,
    tenSecond = 10000,
    hour = 0,
    minute = 0,
  	second = 0;
  if(duration > 0) {
    if(duration < tenSecond) {
      return '刚刚'
    }
    if(duration < oneMinute) {
      second = Math.floor(duration / tenSecond);
      return `${second}秒前`
    }
    if(duration < oneHour) {
      minute = Math.floor(duration / oneMinute);
      return `${minute}分钟前`
    }
    if(duration < oneDay) {
      hour = Math.floor(duration / oneHour);
      minute = Math.floor((duration % oneHour) / oneHour);
      return `${hour}小时前`
    }
  }
  return `${day} ${time}`
}