import * as types from '../constants/ActionTypes'

let initialState = {
  loading: false,
  basicInfo: {},
  rightMenus: [],
};

import Immutable from 'immutable'
let initState = Immutable.fromJS(initialState);

export default function fileView(state = initState, action) {
  switch(action.type) {
    case types.FILEVIEW_LOADING:
      return state.merge({loading: action.loading});
    case types.FILEVIEW_SETBADIC:
      return state.merge({basicInfo: action.basicInfo, rightMenus: action.rightMenus, loading: false});
    default:
      return state
  }
}
