import * as types from '../constants/ActionTypes'

let initialState = {
  loading: false,
  basicInfo: {},
  readCount: '',
  praiseInfo: {},
  rightMenus: [],
  tabInfo: [],
  tabKey: 'content',
  dataKeys: {},
  attrDatas: [],
  scoreDatas: [],
  replyList: [],
  markInfo: {},
  replyCount: 0,
};

import Immutable from 'immutable'
let initState = Immutable.fromJS(initialState);

export default function detail(state = initState, action) {
  switch(action.type) {
    case types.DETAIL_LOADING:
      return state.merge({loading: action.loading});
    case types.DETAIL_SETTABKEY:
      return state.merge({tabKey: action.tabKey});
    case types.DETAIL_SETDATAKEY:
      return state.merge({dataKeys: state.get('dataKeys').merge({[action.params.type]: action.dataKey})});
    case types.DETAIL_SETBADIC:
      return state.merge({basicInfo: action.basicInfo, rightMenus: action.rightMenus, loading: false});
    case types.DETAIL_SETREADCOUNT:
      return state.merge({readCount: action.readCount});
    case types.DETAIL_SETTABINFO:
      return state.merge({tabInfo: action.tabInfo});
    case types.DETAIL_SETPRAISECOUNT:
      return state.merge({praiseInfo: action.praiseInfo, loading: false});
    case types.DETAIL_SETATTR:
      return state.merge({attrDatas: action.attrDatas, loading: false});
    case types.DETAIL_SETSCORE:
      return state.merge({scoreDatas: action.scoreDatas, markInfo: action.markInfo, loading: false});
    case types.DETAIL_SETREPLYLIST:
      return state.merge({replyList: action.replyList, loading: false});
    case types.DETAIL_SETREPLYCOUNT:
      return state.merge({replyCount: action.replyCount});
    default:
      return state
  }
}
