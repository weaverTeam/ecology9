import documentMyDoc from "./myDoc"
import documentSearch from "./search"
import documentDirectory from "./directory"
import documentDummy from "./dummy"
import documentLatest from "./latest"
import documentRank from "./rank"
import documentSubscription from "./subscription"
import documentBatchSharing from "./batchSharing"
import documentMonitor from "./monitor"
import documentDocLog from "./docLog"
import documentDetail from "./detail"
import documentFileView from "./fileView"
import documentAdd from "./add"

export default {
	documentMyDoc,
	documentSearch,
	documentDirectory,
	documentDummy,
	documentLatest,
	documentRank,
	documentSubscription,
	documentBatchSharing,
	documentMonitor,
	documentDocLog,
	documentDetail,
	documentFileView,
	documentAdd
}