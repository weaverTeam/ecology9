import * as types from '../constants/ActionTypes'

let initialState = {
  loading: false,
  tabKey: '0',
};

import Immutable from 'immutable'
let initState = Immutable.fromJS(initialState);

export default function add(state = initState, action) {
  switch(action.type) {
    case types.ADD_LOADING:
      return state.merge({loading: action.loading});
    case types.ADD_SETTABKEY:
      return state.merge({tabKey: action.tabKey});
    default:
      return state
  }
}
