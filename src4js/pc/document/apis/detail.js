import { WeaTools } from 'ecCom'

// 基本信息
export const getBasic = params => {
	return WeaTools.callApi('/api/doc/detail/basicInfo', 'GET', params);
}
// tab
export const getTabInfo = params => {
	return WeaTools.callApi('/api/doc/detail/tabInfo', 'GET', params);
}

// 回复数量
export const getReplyCount = params => {
	return WeaTools.callApi('/api/doc/reply/replyCount', 'GET', params);
}

//  /api/doc/read/addReadLog 增加阅读日志
// 阅读数量
export const getReadCount = params => {
	return WeaTools.callApi('/api/doc/read/readCount', 'GET', params);
}

// 属性
export const getAttr = params => {
	return WeaTools.callApi('/api/doc/detail/docParamInfo', 'GET', params);
}

// 打分
export const getScore = params => {
	return WeaTools.callApi('/api/doc/score/docScore', 'GET', params);
}

export const doMark = params => {
	return WeaTools.callApi('/api/doc/score/doMarkDoc', 'GET', params);
}
// 点赞
export const getPraise = params => {
	return WeaTools.callApi('/api/doc/praise/praiseInfo', 'GET', params);
}

export const setPraise = (params, i = false) => {
	return WeaTools.callApi(`/api/doc/praise/${ i ? 'doPraise' : 'unPraise'}`, 'GET', params);
}

// 回复
export const getReplyList = params => {
	return WeaTools.callApi('/api/doc/reply/replyList', 'GET', params);
}

export const saveReply = params => {
	return WeaTools.callApi('/api/doc/reply/saveReply', 'POST', params);
}

export const getMoreReplyMain = params => {
	return WeaTools.callApi('/api/doc/reply/moreReply', 'GET', params);
}

export const getMoreReplyChild = params => {
	return WeaTools.callApi('/api/doc/reply/residueReply', 'GET', params);
}

export const deleteReply = params => {
	return WeaTools.callApi('/api/doc/reply/deleteReply', 'GET', params);
}
