import * as types from '../constants/ActionTypes'
import * as Detail from '../apis/Detail'
import Immutable from 'immutable'
import { message } from 'antd'

export const doLoading = ( loading = false ) => {
  return {
    type: types.DETAIL_LOADING,
    loading
  }
}

export const setTabKey = ( tabKey = 0 ) => {
  return {
    type: types.DETAIL_SETTABKEY,
    tabKey
  }
}

//文档详情 基本信息
export const getBasic = ( params = {} ) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    Detail.getBasic(params).then(data => {
      dispatch({
        type: types.DETAIL_SETBADIC,
        basicInfo: data.data,
        rightMenus: data.rightMenus
      });
    })
    Detail.getReadCount(params).then(data => {
      dispatch({
        type: types.DETAIL_SETREADCOUNT,
        readCount: data.readCount,
      });
    })
    Detail.getPraise(params).then(data => {
      dispatch({
        type: types.DETAIL_SETPRAISECOUNT,
        praiseInfo: data.praiseInfo,
      });
    })
    Detail.getTabInfo(params).then(data => {
      dispatch({
        type: types.DETAIL_SETTABINFO,
        tabInfo: data.tabInfo,
      });
    })
    Detail.getReplyCount(params).then(data => {
      dispatch({
        type: types.DETAIL_SETREPLYCOUNT,
        replyCount: data.replyCount,
      });
    })
  }
}



//文档详情 文档属性
export const getAttr = ( params = {} ) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    Detail.getAttr(params).then(data => {
      dispatch({
        type: types.DETAIL_SETATTR,
        attrDatas: data.data
      });
    })
  }
}

//文档详情 文档打分
export const getScore = ( params = {} ) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    Detail.getScore(params).then(data => {
      dispatch({
        type: types.DETAIL_SETSCORE,
        scoreDatas: data.docMark,
        markInfo: data.markInfo,
      });
    })
  }
}

//文档详情 文档打分
export const doMark = ( params = {} ) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    Detail.doMark(params).then(data => {
      dispatch({
        type: types.DETAIL_SETSCORE,
        scoreDatas: data.docMark,
        markInfo: data.markInfo,
      });
    })
  }
}

//文档详情 文档点赞
export const setPraise = (params = {}, i = false) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    let praiseInfo = getState().documentDetail.get('praiseInfo');
    Detail.setPraise(params, i).then(data => {
      if(data.status === 1 && !params.type)
        dispatch({
          type: types.DETAIL_SETPRAISECOUNT,
          praiseInfo: praiseInfo.merge({isPraised: i ? '1' : '0'}),
        })
      if(data.status === 1 && params.type){
        dispatch(getReplyList({docid: params.docid}))
      }
    })
  }
}

//文档详情 文档点赞星
export const setStar = (i, remark = '') => {
  return (dispatch, getState) => {
    let markInfo = getState().documentDetail.get('markInfo'),
      scoreDatas = getState().documentDetail.get('scoreDatas');
    dispatch({
      type: types.DETAIL_SETSCORE,
      scoreDatas,
      markInfo: markInfo.merge({mark: i.toString(), remark}),
    })
  }
}

//文档详情 获取回复列表
export const getReplyList = (params = {}) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    Detail.getReplyList(params).then(data => {
      dispatch({
        type: types.DETAIL_SETREPLYLIST,
        replyList: data.replyList,
      })
    })
  }
}

//文档详情 回复提交
export const saveReply = (params = {}, hasMore = false) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    let replyList = getState().documentDetail.get('replyList');
    Detail.saveReply(params).then(data => {
      if(hasMore){
        let { docid } = params;
        dispatch(getMoreReplyChild({ docid, ...hasMore }));
      }else{
        dispatch({
          type: types.DETAIL_SETREPLYLIST,
          replyList: params.replyid ? replyList.push(Immutable.fromJS(data.reply)) : replyList.unshift(Immutable.fromJS(data.reply)),
        })
      }
    })
  }
}

//文档详情 获取更多
export const getMoreReplyMain = (params = {}) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    let replyList = getState().documentDetail.get('replyList');
    Detail.getMoreReplyMain(params).then(data => {
      dispatch({
        type: types.DETAIL_SETREPLYLIST,
        replyList: replyList.concat(Immutable.fromJS(data.replyList)),
      })
    })
  }
}

export const getMoreReplyChild = (params = {}) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    let replyList = getState().documentDetail.get('replyList');
    Detail.getMoreReplyChild(params).then(data => {
      dispatch({
        type: types.DETAIL_SETREPLYLIST,
        replyList: replyList.concat(Immutable.fromJS(data.replyList)),
      })
    })
  }
}

export const deleteReply = (params = {}) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true))
    let replyList = getState().documentDetail.get('replyList');
    Detail.deleteReply(params).then(data => {
      data.status !== 1 && message.error(data.msg);
      dispatch({
        type: types.DETAIL_SETREPLYLIST,
        replyList: data.status === 1 ? replyList.filter(item => item.get('replyid') !== params.replyid) : replyList,
      })
    })
  }
}

