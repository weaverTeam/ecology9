import * as types from '../constants/ActionTypes';
import * as FileView from '../apis/fileView';

export const doLoading = ( loading = false ) => {
  return {
    type: types.FILEVIEW_LOADING,
    loading,
  };
};

//查看附件 基本信息
export const getBasic = ( params = '' ) => {
  return (dispatch, getState) => {
    dispatch(doLoading(true));
    FileView.getBasic(params).then(data => {
      dispatch({
        type: types.FILEVIEW_SETBADIC,
        basicInfo: data.data,
        rightMenus: data.rightMenus
      });
    });
  };
};
