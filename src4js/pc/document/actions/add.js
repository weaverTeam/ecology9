import * as types from '../constants/ActionTypes'
import Immutable from 'immutable'

export const doLoading = ( loading = false ) => {
  return {
    type: types.ADD_LOADING,
    loading
  }
}

export const setTabKey = ( tabKey = 0 ) => {
  return {
    type: types.ADD_SETTABKEY,
    tabKey
  }
}

//文档详情 回复提交
//export const saveReply = (params = {}, hasMore = false) => {
//return (dispatch, getState) => {
//  dispatch(doLoading(true))
//  let replyList = getState().documentDetail.get('replyList');
//  Detail.saveReply(params).then(data => {
//    if(hasMore){
//      let { docid } = params;
//      dispatch(getMoreReplyChild({ docid, ...hasMore }));
//    }else{
//      dispatch({
//        type: types.ADD_SETREPLYLIST,
//        replyList: params.replyid ? replyList.push(Immutable.fromJS(data.reply)) : replyList.unshift(Immutable.fromJS(data.reply)),
//      })
//    }
//  })
//}
//}
