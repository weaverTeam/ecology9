import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as ComsAction from '../actions/coms'
import * as DetailAction from '../actions/detail'

import { Row, Col, Tabs, Form, Button, Input, Modal, Icon, Spin, Popover, message } from 'antd'
const FormItem = Form.Item;

import Immutable from 'immutable'
const is = Immutable.is;

import {
  WeaReqTop,
  WeaRightMenu,
  WeaErrorPage,
  WeaTools,
  WeaTableEdit,
  WeaNewScroll,
  WeaRichText,
  WeaPopoverHrm,
  WeaInputSearch,
} from 'ecCom'


import { WeaTable } from 'comsRedux'
import ReplyItem from './Detail/ReplyItem'

import DocLog from './DocLog'

import '../css/detail.less'
import * as Util from '../util/transf'

class Detail extends React.Component {
  static contextTypes = {
    router: PropTypes.routerShape
  }
  constructor(props) {
    super(props);
    this.isIE = !!window.ActiveXObject || "ActiveXObject" in window;
    this.state = {
      docid: window.location.search.split('=')[1] || props.location.query.id || '',
      richVisible: false,
      richValue: '',
      status: '',
      score: -1,
      remark: props.markInfo.size > 0 ? Number(props.markInfo.get('remark')) : '',
    }
    this.onTabsChange = this.onTabsChange.bind(this);
    this.onPraiseClick = this.onPraiseClick.bind(this);
    this.onToolsChange = this.onToolsChange.bind(this);
    this.doSubmit = this.doSubmit.bind(this);
    this.extentsConfig = [
      '-',
      {
        name: 'Upload',  //上传类型组件
        show: <Icon type="paper-clip" title='上传附件' />,
        uploadUrl: `/api/doc/upload/uploadFile?model=reply&docid=${this.state.docid}`, //上传地址
        category: '/api/doc/upload/uploadFile?model=reply',//文档目录
      },
      {
        name: 'Browser', //浏览按钮组件，浏览按钮类型及其他属性按照浏览按钮说明
        show: <i className='icon-coms-Journal' title='文档' />,
        type: '37', //浏览按钮类型
        title: '文档', //浏览按钮标题
      },
      {
        name: 'Browser', //浏览按钮组件，浏览按钮类型及其他属性按照浏览按钮说明
        show: <i className='icon-coms-Workflow-o' title='流程' />,
        type: '152', //浏览按钮类型
        title: '流程', //浏览按钮标题
      },
    ]
  }
  componentDidMount() {
    //一些初始化请求
    let { actions } = this.props,
      { docid } = this.state;
    actions.getBasic({ docid });
    actions.doSearch('Detail', { type: 'file', docid });
    
    //this.isIE && loadOffice && window.Load && window.Load();
  }
  componentWillReceiveProps(nextProps) {
    const keyOld = this.props.location.key;
    const keyNew = nextProps.location.key;
    //点击菜单路由刷新组件
    if(keyOld !== keyNew) {

    }
    let { markInfo } = this.props,
      _markInfo = nextProps.markInfo;
    if(!is(markInfo, _markInfo) && _markInfo.size > 0){
      this.setState({remark: _markInfo.get('remark')})
    }
//  if(!is(this.props.praiseInfo, nextProps.praiseInfo)){
//    console.log('-----');
//    WeaTools.tooltip('.icon-coms-Good');
//    WeaTools.tooltip('.wea-doc-detail-top-reply-count');
//  }
    //设置页标题
    if(window.location.pathname.indexOf('/spa/document/') >= 0 && !is(this.props.basicInfo, nextProps.basicInfo) && nextProps.basicInfo.get('docSubject') && document.title !== nextProps.basicInfo.get('docSubject'))
      document.title = nextProps.basicInfo.get('docSubject');
  }
  shouldComponentUpdate(nextProps, nextState) {
    //组件渲染控制
    return true
  }
  componentWillUnmount() {
    //组件卸载时一般清理一些状态

  }
  render() {
    let { loading, basicInfo, tabKey, dataKeys, praiseInfo, actions, tabInfo, showSearchAd } = this.props,
      { docid } = this.state,
      title = basicInfo.get('docSubject') || '',
      fileTableKey = dataKeys.get('file') || '',
      shareTableKey = dataKeys.get('share') || '',
      versionTableKey = dataKeys.get('version') || '',
      isPraised = praiseInfo.get('isPraised') === '1',
      users = praiseInfo.get('users') || Immutable.fromJS([]),
      usersShow = praiseInfo.get('username') && isPraised ? users.unshift(praiseInfo) : users,
      pre_title = $('.wea-new-top-req-title-text').html() || '',
      pre_content = $('.wea-doc-detail-content-main').html() || '',
      markStyle = showSearchAd ? { height: '100%',overflow: 'hidden' } : null;
    return (
      <div className='wea-doc-detail'>
        <WeaRightMenu datas={this.getRightMenu()}>
          <WeaPopoverHrm>
            <WeaReqTop
              title={
                title ? 
                <span style={{paddingLeft: 8}}>
                  { title }
                  <span 
                    style={{fontSize: 16, marginLeft: 13, marginTop: -3, color: isPraised ? '#ff9f01' : '#b2b2b2'}}
                  >
                    <i 
                      title={isPraised ? '取消赞' : '赞'}
                      className='icon-coms-Good'
                      style={{ cursor: 'pointer', marginRight: 2 }}
                      onClick={()=> actions.setPraise({docid, praiseid: docid, type: 0}, !isPraised)}
                    />
                    <Popover overlayClassName="wea-doc-detail-top-reply-count-wrap" placement="bottomLeft" content={
                      usersShow.size > 0 ?
                        <span>
                          {
                            usersShow.map((user, index) => {
                              return (
                                <span><span dangerouslySetInnerHTML={{ __html: `<a href='javaScript:openhrm(${user.get('userid')});' onclick='pointerXY(event);' style='color: #fff;'>${user.get('username')}</a>` }} />{usersShow.size < index ? ', ' : ''}</span>
                              )
                            })
                          }
                        </span>
                      : '还没有人点赞哦 ~'
                    } 
                    trigger="hover">
                      <span className='wea-doc-detail-top-reply-count' style={{ cursor: 'pointer', verticalAlign: 'top', fontSize: 12,  color: isPraised ? '#ff9f01' : '#333' }}> ({isPraised ? praiseInfo.get('users').size + (isPraised ? 1 : 0) : 0})</span>
                    </Popover>
                  </span>
                </span>
                : <span dangerouslySetInnerHTML={{ __html: pre_title }} />
              }
              loading={loading}
              icon={<i className='icon-coms-doc' />}
              iconBgcolor='#df583a'
              replaceTab={
                tabInfo.size === 0 ?
                <span style={{display: 'block', height: 37}} />
                :
                <Tabs 
                  activeKey={tabKey}
                  onChange={this.onTabsChange} 
                >
                  { this.getTabPane() }
                </Tabs>
              }
              buttons={this.getButtons()}
              showDropIcon={true}
              dropMenuDatas={this.getRightMenu()}
              onDropMenuClick={this.onRightMenuClick.bind(this)}
            >
              <div className='wea-doc-detail-content'>
                <div className='wea-doc-detail-content-inner'>
                  <div style={{ display: tabKey === 'content' ? 'block' : 'none', height: '100%', overflow: 'auto' }}>
                    <div className='wea-doc-detail-content-main' dangerouslySetInnerHTML={{ __html: pre_content }} />
                  </div>
                  <div className='wea-doc-detail-content-inner-tab-content'  style={{ display: tabKey === 'content' ? 'none' : 'block' }}>
                    <WeaNewScroll scrollId='wea_doc_detail_content' height='100%'>
                    { 
                      (tabKey === 'acc' || tabKey === 'version' || tabKey === 'ref' || tabKey === 'children' || tabKey === 'share') && this.getTitle()
                    }
                    <div style={{display: tabKey === 'acc' ? 'block' : 'none'}}>
                      <WeaTable
                        sessionkey={fileTableKey}
                      />
                    </div>
                    { 
                      tabKey === 'param' && this.getAttr()
                    }
                    <div style={{display: tabKey === 'share' ? 'block' : 'none'}}>
                      <WeaTable
                        sessionkey={shareTableKey}
                      />
                    </div>
                    <div style={{display: tabKey === 'version' ? 'block' : 'none'}}>
                      <WeaTable
                        sessionkey={versionTableKey}
                      />
                    </div>
                    <div style={{display: tabKey === 'log' ? 'block' : 'none', ...markStyle }}>
                      <DocLog docid={docid} searchType={['advanced']} needScroll={false}/>
                    </div>
                    { 
                      tabKey === 'score' && this.getRank()
                    }
                    { 
                      tabKey === 'reply' && this.getReply()
                    }
                    </WeaNewScroll>
                  </div>
                </div>
                { tabKey === 'content' && this.getBottom() }
              </div>
            </WeaReqTop>
          </WeaPopoverHrm>
        </WeaRightMenu>
      </div>
    )
  }
  // bottom
  getBottom(){
    let { basicInfo, readCount, actions } = this.props,
      { docid } = this.state; 
    if(basicInfo.size === 0 || readCount === '' ) return null
    return (
      <span className='wea-doc-detail-content-text-sub'>
        <span>最后由</span>
        <span>{ basicInfo.get('doclastmoduser') }</span>
        <span>编辑于{ basicInfo.get('doclastmoddatetime') }</span>
        <span>阅读 ({ readCount })</span>
      </span>
    )
  }
  // tab
  getTabPane(){
    let { comsWeaTable, dataKeys, tabInfo } = this.props,
      tablekey = dataKeys.get('file') ? dataKeys.get('file').split('_')[0] : 'init',
      tableNow = comsWeaTable.get(tablekey) || comsWeaTable.get('init');
    return tabInfo.map(data => {
      return <Tabs.TabPane tab={data.get('value')} key={data.get('key')} />
    })
  }
  onTabsChange(key){
    let { actions, attrDatas, scoreDatas } = this.props,
      { docid } = this.state;
    actions.setTabKey(key);
    key === 'param' && attrDatas.size === 0 && actions.getAttr({ docid })
    key === 'share' && actions.doSearch('Detail', { type: 'share', id: docid })
    key === 'version' && actions.doSearch('Detail', { type: 'version', docid })
    key === 'score' && scoreDatas.size === 0 && actions.getScore({ docid })
    key === 'reply' ? actions.getReplyList({ docid }) : this.setState({ richValue: '', richVisible: false })
  }
  // 右键菜单
  getButtons(){
    let { rightMenus } = this.props,
      btnArr = [];
    rightMenus = rightMenus.filter(menu => menu.get('isTop') === '1');
    rightMenus.map(btn => {
      btnArr.push(<Button type="primary" onClick={()=> this.onRightMenuClick(btn.get('type'))}>{btn.get('menuName')}</Button>);
    });
    return btnArr
  }
  getRightMenu(){
    let { rightMenus } = this.props,
      btnArr = [];
    rightMenus.map(btn => {
      btnArr.push({
        key: btn.get('type'),
          icon: <i className={btn.get('menuIcon')}/>,
          content: btn.get('menuName'),
          onClick: key => this.onRightMenuClick(key)
        });
    });
    return btnArr
  }
  onRightMenuClick(key){
    let { rightMenus, basicInfo } = this.props,
      { docid } = this.state;
    // console.log('key: ', key)
    if(key === 'BTN_SHARE' || key === 'BTN_REPLY' || key === 'BTN_LOG')
      this.onTabsChange(key.split('_')[1].toLocaleLowerCase());
    
    key === 'BTN_DOWNLOAD' && 
      rightMenus.map(btn => {
        if(btn.get('type') === key) {
          open(`/weaver/weaver.file.FileDownload?${btn.get('params')}`)
        }
      });
    key === 'BTN_CREATE_WORKFLOW' &&
      rightMenus.map(btn => {
        if(btn.get('type') === key) {
          open(btn.get('params') ? `/workflow/request/AddRequest.jsp?docid=${docid}&${btn.get('params')}` : `/workflow/request/RequestType.jsp?docid=${docid}`)
        }
      });
    key === 'BTN_EDIT' && open(`/docs/docs/DocEdit.jsp?id=${docid}`)
    key === 'BTN_RELATE_WORKFLOW' && open(`/workflow/search/WFSearchTemp.jsp?docids=${docid}`)
    key === 'BTN_CREATE_PLAN' && open(`/workplan/data/WorkPlan.jsp?docid=${docid}&add=1`)
    key === 'BTN_PRINT' && open(`/docs/docs/DocPrint.jsp?id=${docid}`)
    key === 'BTN_STORE' && open('/systeminfo/BrowserMain.jsp?url=/favourite/FavouriteBrowser.jsp&fav_pagename=' + escape(`文档:${basicInfo.get('docSubject')}`) + 
      '&fav_uri=/docs/docs/DocDsp.jsp&fav_querystring=' + escape(`id=${docid}`) + '&mouldID=doc')
    key === 'BTN_HELP' && open('/formmode/apps/ktree/ktreeHelp.jsp?pathKey=docs/docs/DocDsp.jsp')
    // key === 'BTN_DELETE' && 
  }
  // tab 内容的 title
  getTitle(){
    let { tabKey, comsWeaTable, dataKeys, actions } = this.props,
      { docid } = this.state,
      tablekey = dataKeys.get('file') ? dataKeys.get('file').split('_')[0] : 'init',
      tableNow = comsWeaTable.get(tablekey) || comsWeaTable.get('init'),
      contentLeft = null,
      contentRight = null;
    if(tabKey === 'acc') {
      contentLeft = '附件列表';
      let disabled = tableNow.get('selectedRowKeys').size === 0;
      contentRight = (
        <span style={{marginRight: 20}}>
          <WeaInputSearch style={{width: 180, verticalAlign: 'middle'}} placeholder='请输入附件名称' onSearch={v => actions.doSearch('Detail', { type: 'file', docid, imageFileName: v })}/>
          <Button type='primary'
            style={{ marginLeft: 10}}
            disabled={disabled}
            onClick={() => !disabled && open(`/weaver/weaver.file.FileDownload?onlydownloadfj=1&download=1&fieldvalue=${tableNow.get('selectedRowKeys').toJS()}`)}
          >
            批量下载
          </Button>
        </span>
      );
    }
    if(tabKey === 'version') {
      contentLeft = '版本信息';
      contentRight = null;
    }
    if(tabKey === 'ref') {
      contentLeft = '相关资源信息';
      contentRight = <Button>文档订阅</Button>;
    }
    if(tabKey === 'children') {
      contentLeft = '子文档列表';
      contentRight = null;
    }
    if(tabKey === 'share') {
      contentLeft = '共享信息';
      contentRight = null;
    }
    return (
      <Row className='wea-doc-detail-content-inner-title'>
        <Col span={12} style={{paddingLeft: 20}}>
          { contentLeft }
        </Col>
        <Col span={12} style={{textAlign: 'right'}}>
          { contentRight }
        </Col>
      </Row>
    )
  }
  
  //文档属性
  getAttr(){
    let { attrDatas } = this.props,
      items = [];
    attrDatas.map((item, i) => {
      let col = item.get('column') || 1;
      items.push(
        <Col span={12 * col}>
          <FormItem 
            label={item.get('label')}
            labelCol={{span: 6 / col }}
            wrapperCol={{span: 24 - 10 / col}}
          >
            <div className='wea-doc-detail-content-inner-attr-value' dangerouslySetInnerHTML={{__html: item.get('value')}}/>
          </FormItem>
        </Col>
      )
    })
    return (
      <Row className='wea-doc-detail-content-inner-attr'>
        {items}
      </Row>
    )
  }
  // 文档打分
  getRank(){
    let { scoreDatas, actions, markInfo } = this.props,
      { docid, score, remark } = this.state,
      _score = Number(markInfo.get('mark') || 0),
      scoreShow = score > -1 ? score : _score,
      stars = [0,0,0,0,0],
      hasPH = !remark,
      props = hasPH ? { placeholder: '请输入评论内容 ~' } : { value: remark };
    return (
      <div className='wea-doc-detail-content-inner-tab-content-rank'>
        <p style={{ fontSize: 14 }}>我的评分</p>
        <div className='wea-doc-detail-content-inner-tab-content-rank-star'>
          <span>评价文档</span>
          {
            stars.map((s,index) => {
              return <i key={index} 
                className={`icon-coms-Collection${index < scoreShow ? '2' : ''}`} 
                onClick={e => {
                  this.setState({score: -1})
                  actions.setStar(index + 1 === _score ? 0 : index + 1, remark);
                }}
                onMouseEnter={e => this.setState({score: index + 1})}
                onMouseLeave={e => this.setState({score: -1})}
              />
            })
          }
        </div>
        <Input type="textarea" {...props} autosize={{ minRows: 6, maxRows: 6 }} onChange={e => this.setState({remark: e.target.value})} />
        <div style={{textAlign: 'right',padding: '15px 0'}}>
          <Button type="ghost" size='large' style={{width: 93, borderRadius: 3}} onClick={()=> {
            if(scoreShow || remark) {
              actions.doMark({docid, score: scoreShow, remark})
            }else{
              message.warning('请输入评论内容！')
            }
          }}>提交</Button>
        </div>
        <div className='wea-doc-detail-content-inner-tab-content-rank-list'>
          <div>总评</div>
          {
            scoreDatas.map((item, i) => {
              return (
                <FormItem 
                  label={item.get('label')}
                  labelCol={{span: 8}}
                  wrapperCol={{span: 16}}
                >
                  <span style={{color: i === 2 ? '#000' : '#ffa741', fontSize: 14}}>{item.get('value')}</span>
                </FormItem>
              )
            })
          }
        </div>
      </div>
    )
  }
  // 回复
//{ name: 'tools', items: [ 'Maximize' ] },
  getReply(){
    let { actions } = this.props,
      { docid, richVisible, richValue, status } = this.state;
    return (
      <div className='wea-doc-detail-reply'>
        { this.getReplyList() }
        <div className='wea-doc-detail-reply-rich' style={{padding: richVisible ? 0 : 10 }}>
          <div style={{ display: richVisible ? 'none' : 'block' }} onClick={() => { this.setState({richVisible: true}); this.refs.reply_main.getEditor().focus()}}>
            <i className='icon-coms-edit' /> 写回复
          </div>
          <div style={{ display: richVisible ? 'block' : 'none' }}>
            <WeaRichText
              ref='reply_main'
              value={richValue}
              ckConfig={{
                toolbar:[
                  { name: 'document', items: [ 'Source'] },
                  { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
                  { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
                  { name: 'colors', items: [ 'TextColor' ] },
                  { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
                  { name: 'links', items: [ 'Link', 'Unlink' ] },
                  { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] },
                  { name: 'clipboard', items: [ 'Undo', 'Redo' ] },
                ],
                height:100,
                uploadUrl: `/api/doc/upload/uploadFile?model=reply&docid=${docid}`,
              }}
              extentsConfig={this.extentsConfig}
              onChange={v=>this.setState({richValue: v})}
              onStatusChange={s => this.setState({status: s})}
              onToolsChange={this.onToolsChange}
            />
            {
              status === 'ready' &&
              <div className='wea-doc-detail-reply-rich-btm'>
                <Button type='primary' onClick={() => {
                	if(richValue) {
                  	let { Upload_image, Upload_file, Browser_152, Browser_37 } = this.state;
                  	this.doSubmit({ docid, content: richValue, replyid: '' }, 0, Upload_image, Upload_file, Browser_152, Browser_37)
                    this.setState({richValue: '', richVisible: false})
                  }else{
                    message.warning('回复内容不能为空！')
                  }
                }}>提交</Button>
                <Button style={{marginLeft: 8}} type='ghost' onClick={()=>this.setState({richValue: '',richVisible: false})}>取消</Button>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }
  doSubmit(params, hasMore, Upload_image, Upload_file, Browser_152, Browser_37){
    let { actions } = this.props,
      { content } = params;
    if(Upload_image){
      let { ids, names } = this.extentsfilter(content, Upload_image);
      params.imgFileids = ids;
      params.imgFilenames = names;
    }
    if(Upload_file){
      let { ids, names } = this.extentsfilter(content, Upload_file);
      params['field-annexupload'] = ids;
      params['field-annexupload-name'] = names;
    }
    if(Browser_152){
      let { ids, names } = this.extentsfilter(content, Browser_152);
      params.signworkflowids = ids;
      params.signworkflownames = names;
    }
    if(Browser_37){
      let { ids, names } = this.extentsfilter(content, Browser_37);
      params.signdocids = ids;
      params.signdocnames = names;
    }
    actions.saveReply(params, hasMore);
  }
  extentsfilter(content, list){
    let ids = [],
      names = [],
      _list = list.filter(item => content.indexOf(item.string) >= 0);
    _list.map(item => {
      ids.push(item.fileid || item.id)
      names.push(item.filename || item.name)
    })
    return { ids: ids.join(','), names: names.join('////~~weaversplit~~////') }
  }
  onToolsChange(name = '', ids = '', list = [], type = ''){
    let stateList = this.state[`${name}_${type}`],
      mirror = {
        37: "doc",
        152: "workflow",
      },
      newList = list.map(item => {
        let _item = { ...item };
        if(name === 'Upload' && type === 'image') {
          _item.string = `=${_item.fileid}`;
        }else if(name === 'Upload' && type === 'file') {
          _item.string = `=${_item.fileid}`;
        }else if(name === 'Browser') {
          _item.string = `,${item.id},`;
        }
        return _item
      });
    this.setState({ [`${name}_${type}`] : (stateList ? stateList.concat(newList) : newList) })
    return Util.transfStr(name, ids, list, type);
  }
  // 回复列表
  getReplyList(){
    let { replyList, loading, actions } = this.props,
      { docid, richVisible } = this.state;
    if(replyList.size === 0 && !loading) return (
      <div className='wea-doc-detail-reply-list' style={{ textAlign: 'center', marginTop: 150, color: '#999' }}>
        <p><i className='icon-document-No-response' style={{fontSize: 50}}/></p>
        <p style={{fontSize: 20, marginTop: 20}}>暂无数据</p>
      </div>
    )
    let _replyList = this.resetReplyList(replyList);
    return (
      <Spin spinning={loading}>
        <div className='wea-doc-detail-reply-list' style={ richVisible ? { marginBottom: 208 } : {}}>
          { 
            _replyList.map((item, i)=> {
              let isLast = _replyList.size === i + 1;
              return (
                <ReplyItem 
                  data={item}
                  isLast={isLast}
                  extentsConfig={this.extentsConfig}
                  onPraiseClick={this.onPraiseClick}
                  onRichSubmit={(params, hasMore, Upload_image, Upload_file, Browser_152, Browser_37) => this.doSubmit({ docid, ...params }, hasMore, Upload_image, Upload_file, Browser_152, Browser_37 )}
                  getMoreReplyMain={params => actions.getMoreReplyMain({ docid, ...params })}
                  getMoreReplyChild={params => actions.getMoreReplyChild({ docid, ...params })}
                  deleteReply={params => actions.deleteReply({ docid, ...params })}
                />
              )
            }) 
          }
        </div>
      </Spin>
    )
  }
  // 回复列表重组
  resetReplyList(replyList){
    let _replyList = replyList.filter(item => item.get('replyid') === item.get('replymainid'));
    _replyList = _replyList.map(_item => {
      let __item = _item.merge({children: []});
      replyList.map(item => {
        if(item.get('replymainid') === _item.get('replyid') && item.get('replyid') !== item.get('replymainid')){
          __item = __item.merge({children: __item.get('children').push(item)}) 
        }
        return item
      })
      return __item
    });
    return _replyList
  }
  // 回复列表点赞
  onPraiseClick(data, isPraise){
    let { actions } = this.props;
    actions.setPraise({type: 1,docid: data.get('docid'),praiseid: data.get('replyid')},isPraise);
  }
}

//组件检错机制
class MyErrorHandler extends React.Component {
  render() {
    const hasErrorMsg = this.props.error && this.props.error !== "";
    return(
      <WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
    );
  }
}

Detail = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(Detail);

//form 表单与 redux 双向绑定
//Detail = createForm({
//  onFieldsChange(props, fields) {
//    props.actions.saveFields({ ...props.fields, ...fields });
//  },
//  mapPropsToFields(props) {
//    return props.fields;
//  }
//})(Detail);


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
  const { documentDocLog, documentDetail, comsWeaTable } = state;
  return { 
    loading: documentDetail.get('loading'),
    basicInfo: documentDetail.get('basicInfo'),  
    readCount: documentDetail.get('readCount'),  
    praiseInfo: documentDetail.get('praiseInfo'),
    rightMenus: documentDetail.get('rightMenus'),
    dataKeys: documentDetail.get('dataKeys'),
    tabKey: documentDetail.get('tabKey'),
    tabInfo: documentDetail.get('tabInfo'),
    replyCount: documentDetail.get('replyCount'),
    comsWeaTable,
    attrDatas: documentDetail.get('attrDatas'),
    scoreDatas: documentDetail.get('scoreDatas'),
    markInfo: documentDetail.get('markInfo'),
    replyList: documentDetail.get('replyList'),
    showSearchAd: documentDocLog.get('showSearchAd'),
  }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({...DetailAction, ...ComsAction}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail);