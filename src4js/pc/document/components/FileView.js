import PropTypes from 'react-router/lib/PropTypes';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as FileViewAction from '../actions/fileView';

import { Button } from 'antd';

import Immutable from 'immutable';
const is = Immutable.is;

import {
  WeaReqTop,
  WeaRightMenu,
  WeaErrorPage,
  WeaTools,
} from 'ecCom'

import '../css/detail.less'

class FileView extends React.Component {
  static contextTypes = {
    router: PropTypes.routerShape
  }
  constructor(props) {
    super(props);
    this.isIE = !!window.ActiveXObject || "ActiveXObject" in window;
    this.state = {
      search: window.location.search || '',
    }
  }
  componentDidMount() {
    //一些初始化请求
    let { actions } = this.props,
      { search } = this.state;
    actions.getBasic(search);
    //this.isIE && loadOffice && window.Load && window.Load();
  }
  componentWillReceiveProps(nextProps) {
    const keyOld = this.props.location.key;
    const keyNew = nextProps.location.key;
    //点击菜单路由刷新组件
    if(keyOld !== keyNew) {

    }
    //设置页标题
    if(window.location.pathname.indexOf('/spa/document/') >= 0 && !is(this.props.basicInfo, nextProps.basicInfo) && nextProps.basicInfo.get('imagefilename') && document.title !== nextProps.basicInfo.get('imagefilename'))
      document.title = nextProps.basicInfo.get('imagefilename');
  }
  shouldComponentUpdate(nextProps, nextState) {
    //组件渲染控制
    return true
  }
  componentWillUnmount() {
    //组件卸载时一般清理一些状态

  }
  render() {
    let { loading, basicInfo, tabKey, dataKeys } = this.props,
      { id } = this.state,
      prevname = basicInfo.get('prevname') || '',
      title = basicInfo.get('imagefilename') || '',
      pre_title = $('.wea-new-top-req-title-text').html() || '',
      pre_content = $('.wea-doc-detail-content-main').html() || '';
    //if(basicInfo.size === 0) return (<div className='wea-new-top-req-title-text' dangerouslySetInnerHTML={{__html: window.pre_title}} />);
    return (
      <div className='wea-doc-detail'>
        <WeaRightMenu datas={this.getRightMenu()}>
          <WeaReqTop
            title={basicInfo.size === 0 ? <span style={{ marginLeft: 8 }} dangerouslySetInnerHTML={{__html: pre_title}} /> : <span style={{ marginLeft: 8 }}><span>{prevname} </span>{title}</span>}
            loading={loading}
            icon={<i className='icon-coms-doc' />}
            iconBgcolor='#df583a'
            replaceTab={<span style={{display: 'block', height: 37}} />}
            buttons={this.getButtons()}
            showDropIcon={true}
            dropMenuDatas={this.getRightMenu()}
            onDropMenuClick={this.onRightMenuClick.bind(this)}
          >
            <div className='wea-doc-detail-content'>
              <div className='wea-doc-detail-content-inner'>
                <div className='wea-doc-detail-content-main' style={{height: '100%'}} dangerouslySetInnerHTML={{__html: pre_content}} />
              </div>
            </div>
          </WeaReqTop>
        </WeaRightMenu>
      </div>
    )
  }
  //title
  getButtons(){
    let { rightMenus } = this.props,
      btnArr = [];
    rightMenus = rightMenus.filter(menu => menu.get('isTop') === '1');
    rightMenus.map(btn => {
      btnArr.push(<Button type="primary" onClick={()=> this.onRightMenuClick(btn.get('type'))}>{btn.get('menuName')}</Button>);
    });
    return btnArr
  }
  getRightMenu(){
    let { rightMenus } = this.props,
      btnArr = [];
    rightMenus.map(btn => {
      btnArr.push({
        key: btn.get('type'),
          icon: <i className={btn.get('menuIcon')}/>,
          content: btn.get('menuName'),
          onClick: key => {}
        });
    });
    return btnArr
  }
  onRightMenuClick(key){
    let { rightMenus } = this.props;
    //console.log('key: ', key)
    if(key === 'BTN_DOWNLOAD'){
      rightMenus.map(btn => {
        if(btn.get('type') === key) {
          open(`/weaver/weaver.file.FileViewDownload?${btn.get('params')}`)
        }
      });
    }
  }
}

//组件检错机制
class MyErrorHandler extends React.Component {
  render() {
    const hasErrorMsg = this.props.error && this.props.error !== "";
    return(
      <WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
    );
  }
}

FileView = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(FileView);

// 把 state map 到组件的 props 上
const mapStateToProps = state => {
  const { documentFileView } = state;
  return { 
    loading: documentFileView.get('loading'),
    basicInfo: documentFileView.get('basicInfo'),  
    rightMenus: documentFileView.get('rightMenus'),
  }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(FileViewAction, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FileView);