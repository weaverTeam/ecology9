/**
 * 我的文档
 */
export const MYDOC_LOADING = 'mydoc_loading'
export const MYDOC_SETDATAKEY = 'mydoc_setdatakey' //分页key
export const MYDOC_SAVEFIELDS = 'mydoc_savefields' //表单同步
export const MYDOC_SETCONDITIONINFO = 'mydoc_setconditioninfo' //高级搜索
export const MYDOC_SETTREEDATAS = 'mydoc_settreedatas' //左侧树
export const MYDOC_SETSHOWSEARCHAD= 'mydoc_setshowsearchad' //高级查询显隐
export const MYDOC_SET_SELECTED_TREEKEYS= 'mydoc_setselectedtreekeys'; //树点击存key
// export const MYDOC_SEARCH_RESULT= 'mydoc_searchresult'; //树点击存key
export const MYDOC_GET_RIGHT_MENU= 'mydoc_get_right_menu'; //右键菜单
export const MYDOC_LOG = 'mydoc_log'; //日志
/**
 * 最新文档
 */
export const LATEST_LOADING = 'latest_loading'
export const LATEST_SETDATAKEY = 'latest_setdatakey' //分页key
export const LATEST_SAVEFIELDS = 'latest_savefields' //表单同步
export const LATEST_SETCONDITIONINFO = 'latest_setconditioninfo' //高级搜索
export const LATEST_SETTREEDATAS = 'latest_settreedatas' //左侧树
export const LATEST_SETSHOWSEARCHAD= 'latest_setshowsearchad' //高级查询显隐
export const LATEST_SET_SELECTED_TREEKEYS= 'latest_setselectedtreekeys'; //树点击存key
// export const LATEST_SEARCH_RESULT= 'latest_searchresult'; //树点击存key
export const LATEST_GET_TOP_TAB= 'latest_get_top_tab'; //TOPmenu
export const LATEST_GET_RIGHT_MENU= 'latest_get_right_menu'; //右键菜单
export const LATEST_LOG = 'latest_log';//日志

/**
 * 查询文档
 */
export const SEARCH_LOADING = 'search_loading' //loading
export const SEARCH_SAVEFIELDS = 'search_savefields' //表单同步
export const SEARCH_SETCONDITIONINFO = 'search_setconditioninfo' //高级搜索
export const SEARCH_SETTREEDATAS = 'search_settreedatas' //左侧树
export const SEARCH_SETSHOWTABLE = 'search_setshowtable' //查询 列表显示切换
export const SEARCH_SETSHOWSEARCHAD= 'search_setshowsearchad' //高级查询显隐
export const SEARCH_SET_SELECTED_TREEKEYS= 'search_set_selected_treekeys';//树点击存key
export const SEARCH_GET_RIGHT_MENU='search_get_right_menu'//右键菜单
export const SEARCH_SETDATAKEY='search_setdatakey'//sessionkey
export const SEARCH_LOG = 'search_log';//日志
export const SEARCH_IMPORT = "search_import";  //导入虚拟目录
/**
 * 文档目录
 */
export const DIRECTORY_LOADING = 'directory_loading'
export const DIRECTORY_SETDATAKEY = 'directory_setdatakey' //分页key
export const DIRECTORY_SAVEFIELDS = 'directory_savefields' //表单同步
export const DIRECTORY_SETCONDITIONINFO = 'directory_setconditioninfo' //高级搜索
export const DIRECTORY_SETTREEDATAS = 'directory_settreedatas' //左侧树
export const DIRECTORY_SETSHOWSEARCHAD= 'directory_setshowsearchad' //高级查询显隐
export const DIRECTORY_SET_SELECTED_TREEKEYS= 'directory_set_selected_treekeys'; //树点击存key
// export const MYDOC_SEARCH_RESULT= 'mydoc_searchresult'; //树点击存key
export const DIRECTORY_GET_RIGHT_MENU= 'directory_get_right_menu'; //右键菜单
export const DIRECTORY_SEARCH_INIT_TREE = 'directory_search_init_tree'; //初始化树
export const DIRECTORY_LOG = 'directory_log';//日志
export const DIRECTORY_IMPORT = "directory_import";  //导入虚拟目录
/**
 * 虚拟目录
 */
export const DUMMY_LOADING = 'dummy_loading'
export const DUMMY_SETDATAKEY = 'dummy_setdatakey' //分页key
export const DUMMY_SAVEFIELDS = 'dummy_savefields' //表单同步
export const DUMMY_SETCONDITIONINFO = 'dummy_setconditioninfo' //高级搜索
export const DUMMY_SETTREEDATAS = 'dummy_settreedatas' //左侧树
export const DUMMY_SETSHOWSEARCHAD= 'dummy_setshowsearchad' //高级查询显隐
export const DUMMY_SET_SELECTED_TREEKEYS= 'dummy_set_selected_treekeys'; //树点击存key
export const DUMMY_GET_RIGHT_MENU= 'dummy_get_right_menu'; //右键菜单


/**
 * 知识排名
 */
export const RANK_LOADING = 'rank_loading'
export const RANK_SETDATAKEY = 'rank_setdatakey' //分页key
export const RANK_SAVEFIELDS = 'rank_savefields' //表单同步
export const RANK_SETCONDITIONINFO = 'rank_setconditioninfo' //高级搜索
export const RANK_SETSHOWSEARCHAD= 'rank_setshowsearchad' //高级查询显隐
export const RANK_GET_RIGHT_MENU= 'rank_get_right_menu'; //右键菜单
export const RANK_GET_TOP_TAB= 'rank_get_top_tab'; //TOPmenu
export const RANK_LOG = 'rank_log';//日志

/**
 * 文档订阅
 */

export const SUBSCRIPTION_LOADING = 'subscription_loading'
export const SUBSCRIPTION_GET_RIGHT_MENU= 'subscription_get_right_menu'; //右键菜单


/**
 * 批量共享
 */
export const BATCHSHARING_LOADING = 'batchsharing_loading'
export const BATCHSHARING_GET_RIGHT_MENU= 'batchsharing_get_right_menu'; //右键菜单


/**
 * 文档监控
 */
export const MONITOR_LOADING = 'monitor_loading'
export const MONITOR_GET_RIGHT_MENU= 'monitor_get_right_menu'; //右键菜单


/**
*文档日志
*/
export const DOCLOG_LOADING = 'doclog_loading'
export const DOCLOG_SETDATAKEY = 'doclog_setdatakey' //分页key
export const DOCLOG_SAVEFIELDS = 'doclog_savefields' //表单同步
export const DOCLOG_SETCONDITIONINFO = 'doclog_setconditioninfo' //高级搜索
export const DOCLOG_SETSHOWSEARCHAD= 'doclog_setshowsearchad' //高级查询显隐
export const DOCLOG_GET_RIGHT_MENU= 'doclog_get_right_menu'; //右键菜单
export const DOCLOG_GET_TOP_TAB= 'doclog_get_top_tab'; //TOPmenu
export const DOCLOG_DOCID='doclog_docid'


/**
*文档详情
*/
export const DETAIL_LOADING = 'detail_loading'
export const DETAIL_SETTABKEY = 'detail_settabkey'
export const DETAIL_SETDATAKEY = 'detail_setdatakey'
export const DETAIL_SETBADIC = 'detail_setbadic'
export const DETAIL_SETATTR = 'detail_setattr'
export const DETAIL_SETSCORE = 'detail_setscore'
export const DETAIL_SETREADCOUNT = 'detail_setreadcount'
export const DETAIL_SETPRAISECOUNT = 'detail_setpraisecount'
export const DETAIL_SETREPLYLIST = 'detail_setreplylist'
export const DETAIL_SETTABINFO = 'detail_settabinfo'
export const DETAIL_SETREPLYCOUNT = 'detail_setreplycount'

/**
*查看附件
*/
export const FILEVIEW_LOADING = 'fileview_loading'
export const FILEVIEW_SETBADIC = 'fileview_setbadic'

/**
*新建文档
*/
export const ADD_LOADING = 'add_loading'
export const ADD_SETTABKEY = 'add_settabkey'

