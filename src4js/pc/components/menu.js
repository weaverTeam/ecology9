export default {
  comList_1 : [
    { key: 'WeaTop', title: '顶部' , abc: 'db'},
    { key: 'WeaLeftTree', title: '左侧树', abc: 'zcs' },
    { key: 'WeaLeftRightLayout', title: '左右布局', abc: 'zybj' },
    { key: 'WeaRightMenu', title: '右键菜单', abc: 'yjcd' },
    { key: 'WeaMenu', title: '门户菜单', abc: 'mhcd' },
    { key: 'WeaNewScroll', title: '新滚动条', abc: 'gdt' },
    { key: 'WeaScroll', title: '老滚动条', abc: 'gdt' },
    { key: 'WeaErrorPage', title: '错误提示', abc: 'cwts' },
    { key: 'WeaInputSearch', title: '输入框（带放大镜）', abc: 'srk' },
    { key: 'WeaReqTop', title: '顶部（表单）', abc: 'db' },
    { key: 'WeaSearchGroup', title: '高级查询面板', abc: 'gjcx' },
    { key: 'WeaTab', title: '页签', abc: 'yq' },
    { key: 'WeaAlertPage', title: '空白页', abc: 'kb' },
    { key: 'WeaEchart', title: '图表', abc: 'tb' },
    { key: 'WeaMap', title: '地图', abc: 'dt' },
    { key: 'WeaQRCode', title: '二维码', abc: 'ewm' },
      {key: 'WeaDragChoose', title: '拖动选择',abc: 'ddd'},
      {key: 'WeaNewScrollPagination', title: '滚动翻页', abc: 'eee'}
  ],
  comList_2 : [
    { key: 'WeaTable', title: '分页列表', abc: 'fy' },
    { key: 'WeaTableRedux', title: 'Redux版分页列表', abc: 'reduxfy' },
    { key: 'WeaTableMobx', title: 'Mobx版分页列表', abc: 'mobxfy' },
    { key: 'WeaTableEdit', title: '可编辑分页列表', abc: 'kbjfy' },
    { key: 'WeaPopoverHrm', title: '人员卡片', abc: 'rykp' },
    { key: 'WeaBrowser', title: '浏览按钮', abc: 'llan' },
    { key: 'WeaUpload', title: '上传组件', abc: 'sc' },
    { key: 'WeaRichText', title: '富文本编辑器', abc: 'fwbbjq' },
  ],
  comList_3 : [
    { key: 'WeaInput', title: '单行文本', abc: 'dhwb' },
    { key: 'WeaTextarea', title: '多行文本', abc: 'dhwb' },
    { key: 'WeaSelectCtrl', title: '下拉框', abc: 'xlk' },
    { key: 'WeaCheckbox', title: '选择框', abc: 'xzk' },
    { key: 'WeaTimePicker', title: '时间', abc: 'sj' },
    { key: 'WeaDatePicker', title: '日期', abc: 'rq' },
    { key: 'WeaDateGroup', title: '日期组合', abc: 'rqzh' },
    { key: 'WeaScope', title: '数值范围', abc: 'sjfw' },
    { key: 'WeaOrgTree', title: '组织树', abc: 'zzs' },
    { key: 'WeaDialog', title: '弹框', abc: 'tk' },
    { key: 'WeaDraggable', title: '拖拽', abc: 'tz' },
  ],
  comList_4 : [
    { key: 'WeaTools', title: '工具库', children: [
      { key: 'WeaTools/storage', title: '本地缓存', abc: 'bdcc' },
      { key: 'WeaTools/fetch', title: '异步请求', abc: 'ybqq' },
      { key: 'WeaTools/error', title: '异常处理', abc: 'yccl' },
      { key: 'WeaTools/getBrower', title: '按钮选择', abc: 'anxz' },
    ]},
  ],
  test: [
    { key: 'test_cjm', title: '陈佳敏', abc: 'cjm' },
    { key: 'test_jbb', title: '蒋贝贝', abc: 'jbb' },
    { key: 'test_lx', title: '林鑫', abc: 'lx' },
    { key: 'test_py', title: '彭友', abc: 'py' },
    { key: 'test_zrj', title: '张仁健', abc: 'zrj' },
    { key: 'test_zy', title: '张尧', abc: 'zy' },
    { key: 'test_zz', title: '朱钊', abc: 'zz' },
  ]
};
