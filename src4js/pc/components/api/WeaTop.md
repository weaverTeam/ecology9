## 参数说明如下：

参数 | 说明 | 类型 | 可选 | 默认
---|---|---|---|---
id | 富文本实例标识id，启用本地存储的话，此项必填 | string |  | ''
ls | 是否开启本地存储，开启后会自动加载本地存储上次关闭编辑器的缓存内容，提交时需自己根据id清除本地存储数据,localStorage.remove('wea_rich_text_ls_' + id) | bool |  | false
value | 富文本内容值受控 | string | | ''
ckConfig | CKEditor 默认配置 | object | 必填 | 详见下方
extentsConfig | 添加在上部工具栏的自定义react 组件 | array | | [], 配置详见下方
bottomBarConfig | 添加在底部工具栏的自定义react 组件 | array |  | [], 配置详见下方
bootomBarStyle | 底部工具栏样式 | object |  | {}
onStatusChange | 富文本编辑器加载状态，加载完成会抛出'ready' | function | 'ready' | fucntion(status){}
onChange | 富文本值变化时触发 | function | function(value){} |
onToolsChange | 获取默认内置扩展react 组件（upload 、browser等）状态变化，自定义内容拼接 | function(name, ids, list, type){} | | 详见下方

**以微博为例：**

```

<WeaRichText
    id={this.CKE_ID}
    ref={this.CKE_ID}
    ls={true}
    value={!hasSubmited && defaultTemplate.tempContent && !localStorage[`wea_rich_text_ls_${this.CKE_ID}`] ? defaultTemplate.tempContent : richValue}
    ckConfig={{
        toolbar:[
            { name: 'document', items: [ 'Source'] },
            { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
            { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] }
        ],
        extraPlugins: 'autogrow',
        height:150,
        autoGrow_minHeight: 150,
        autoGrow_maxHeight: 600,
        removePlugins: 'resize',
        uploadUrl: '/api/blog/fileupload/uploadimage',
    }}
    extentsConfig
    bottomBarConfig={ bottomBarConfig }
    bootomBarStyle={{}}
    onChange={v=>this.setState({richValue: v})}
    onStatusChange={s => this.setState({status: s})}
    onToolsChange={Util.transfStr}
/>

```

## ckConfig 详细配置

[点击查看CK官方文档](https://docs.ckeditor.com/#!/api/CKEDITOR.config)

```
组件默认：

{
    toolbar: 'Basic', //ck基础默认
    toolbarStartupExpanded: true, //默认头部工具栏展开
    toolbarCanCollapse: false, //工具栏不可收缩
    allowedContent: true, // 不启用内容过滤
    startupFocus: true,
    font_names: '宋体/SimSun;新宋体/NSimSun;仿宋/FangSong;楷体/KaiTi;仿宋_GB2312/FangSong_GB2312;'+
        '楷体_GB2312/KaiTi_GB2312;黑体/SimHei;华文细黑/STXihei;华文楷体/STKaiti;华文宋体/STSong;华文中宋/STZhongsong;'+
        '华文仿宋/STFangsong;华文彩云/STCaiyun;华文琥珀/STHupo;华文隶书/STLiti;华文行楷/STXingkai;华文新魏/STXinwei;'+
        '方正舒体/FZShuTi;方正姚体/FZYaoti;细明体/MingLiU;新细明体/PMingLiU;微软雅黑/Microsoft YaHei;微软正黑/Microsoft JhengHei;'+
        'Arial/Arial, Helvetica, sans-serif;' +
        'Comic Sans MS/Comic Sans MS, cursive;' +
        'Courier New/Courier New, Courier, monospace;' +
        'Georgia/Georgia, serif;' +
        'Lucida Sans Unicode/Lucida Sans Unicode, Lucida Grande, sans-serif;' +
        'Tahoma/Tahoma, Geneva, sans-serif;' +
        'Times New Roman/Times New Roman, Times, serif;' +
        'Trebuchet MS/Trebuchet MS, Helvetica, sans-serif;' +
        'Verdana/Verdana, Geneva, sans-serif',
    ...ckConfig,
    extraPlugins: `${ckConfig.uploadUrl ? 'imagepaste,uploadimage,' : ''}${ckConfig.extraPlugins || ''}`, //uploadUrl 存在则启用粘贴、拖拽上传图片
});

微博：
{
    toolbar:[
        { name: 'document', items: [ 'Source'] }, //查看源码
        { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },//居左、居中、居右、列表
        { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] }, //格式、字体、大小
        { name: 'colors', items: [ 'TextColor' ] }, //文本颜色
        { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },//粗体、斜体、下划线、删除线
        { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] } //上传图片、表格、表情
    ],
    extraPlugins: 'autogrow', // 内容自适应撑开
    height:150, //初始化默认高度
    autoGrow_minHeight: 150, //最小高度
    autoGrow_maxHeight: 600, //最大高度
    removePlugins: 'resize',
    uploadUrl: '/api/blog/fileupload/uploadimage', //本地图片上传接口，严格按照上传组件接口格式
}


```

## extentsConfig、bottomBarConfig 详细配置

*目前内部默认支持 上传组件 和 浏览按钮*

```
extentsConfig、bottomBarConfig 数据形式相同，只是添加在上部ck工具栏中，还是添加在底部工具栏中。

[
    {
        name: 'Browser',  //Component: 自定义组件， Browser： 浏览按钮， Upload： 上传组件
        show：'自定义图标或组件'，
        ...props //其他组件属性
    }，
]

以微博为例：

let bottomBarConfig = [];
weiboCKEditorConfig.map(cg => {
    cg.name === 'selects' && bottomBarConfig.push({
        name: 'Component', //自定义组件标识，富文本组件将直接放入组件展示，功能外部自定义
        show: <Mood onSelect={mood => this.setState({mood})}/> //自定义心情组件
    })
    cg.name === 'dialogs' && cg.items.map(item => {
        let obj = {
            name: 'Browser', //浏览按钮组件，浏览按钮类型及其他属性按照浏览按钮说明
            show: <div className="wea-cb-item">
                <span className={`wea-cbi-icon ${mirror[item].icon}`}/>
                <span className="wea-cbi-text">{mirror[item].name}</span>
            </div>,
            type: mirror[item].type, //浏览按钮类型
            title: mirror[item].name, //浏览按钮标题
            isSingle: item === 'Templet', //单选、多选
        };
        if(item === 'Templet') obj.onToolsChange = this.onTempletChange // 自定义浏览按钮事件，如微博模板需要判断是否模板覆盖内容确认框
        bottomBarConfig.push(obj);
    })
    cg.name === 'uploads' && cg.items.map(item => {
        bottomBarConfig.push({
            name: 'Upload',  //上传类型组件
            show: <div className="wea-cb-item">
                <span className='wea-cbi-icon icon-blog-Enclosure'/>
                <span className="wea-cbi-text">附件</span>
            </div>,
            uploadId: 'webo_edit', //上传实例id
            uploadUrl: `${item.Attachment.uploadUrl}?category=${item.Attachment.position}`, //上传地址
            category: item.Attachment.position,//文档目录
            maxUploadSize: item.Attachment.maxSize,//最大限制
            style: {display: "inline-block", padding: 0}
        })
    })
})

onTempletChange(name, ids, list, type){
    let { richValue } = this.state;
    Util.templetChange(richValue,v => this.setState({richValue: v}), name, ids, list, type)
}

```

## onToolsChange

```
此必须为纯函数，根据输入返回字符串输出

以微博 Util 为例：

export const transfStr = (name = '', ids = '', list = [], type = '') => {
    //获取浏览按钮上传组件的结果，不处理默认会走组件内部的方法
    let str = '';
    const mirror = {
        37: "doc",
        task: "task",
        18: "crm",
        152: "workflow",
        135: "project",
        workplan: "workplan",
        blogTemplate: "blogTemplate"
    }
    list.map(item => {
        if(name === 'Upload' && type === 'image'){
            str += '<img class="formImgPlay" src="' + item.filelink + '" data-imgsrc="' + (item.loadlink || item.filelink) + '" />'
        }
        if(name === 'Upload' && type === 'file'){
            str += (item.filelink + '<br>')
        }
        if(name === 'Browser'){
            str += (type === 'blogTemplate' ? item.name : `<a href='javascript:void(0)'  linkid='${item.id}' linkType='${mirror[type]}' onclick='try{return openAppLink(this,${item.id});}catch(e){}' ondblclick='return false;'  unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>${item.name || item.showname}</a>`)
        }
    })
    //返回拼接好的富文本内容
    return str
}

//模板组件单独处理，会有callback 执行内容处理，获取callback 的返回值
export const templetChange = (valueNow = '' ,callback,  name = '', ids = '', list = [], type = '') => {
    if(valueNow){
        Modal.confirm({
            title: '替换当前内容？',
            onOk: () => {
                callback(transfStr(name, ids, list, type))
            },
            onCancel: () => {

            }
        });
    }else{
        callback(transfStr(name, ids, list, type))
    }
}

```