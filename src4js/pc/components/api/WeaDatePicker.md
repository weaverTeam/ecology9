## 参数说明如下：

参数 | 说明 | 类型 | 可选 | 默认
---|---|---|---|---
fieldName | 浏览按钮隐藏域的name | string | |
viewAttr | 编辑权限 | number |` 1：只读，2：可编辑， 3：必填` | 2
value | 默认的数据 | string | |
onChange | 选择数据时的调用，参数为选中数据的value | function(value) | |
formatPattern |　数据展示的格式|number | | 2
startValue | 可选择的日期开始时间 | '2014-01-12' | | null
endValue |可选择的日期结束时间 | '2014-01-12' | | null
noInput | 流程表单特殊展示，没有input框，只有日期icon图标  | Boolean | | false
style | 样式 | ｛｝ | | null

formatPattern 说明:
  ```
  1：yyyy/MM/dd
  2：yyyy-MM-dd
  3：yyyy年MM月dd日
  4：yyyy年MM月
  5：MM月dd日
  6：EEEE
  7：日期大写
  8：yyyy/MM/dd hh:mm a
  9：yyyy/MM/dd HH:mm
  ```