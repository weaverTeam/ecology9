import WeaTableRedux from './WeaTableRedux.md';
import WeaRichText from './WeaRichText.md';
import WeaDatePicker from './WeaDatePicker.md';

export default {
	WeaTableRedux,
	WeaRichText,
  WeaDatePicker,
}
