## 后端说明
和E8分页的区别：

1、tableString和E8一致

2、多了pageUID参数，这个UUID是自行生成并写死的常量

3、col中的column必须唯一，如果特殊的无法唯一请补充_key

4、把结果生成随机数放入Util_TableMap并返回给前端

```
String pageUid = PageUidFactory.getBrowserUID("doclist");
String tableString = "<table instanceid='BrowseTable' tabletype='none' pageUid =\"" + pageUid + "\">" + 
                            "<sql backfields=\"" + backfields + "\" sqlform=\"" + Util.toHtmlForSplitPage(fromSql) + "\" sqlwhere=\"" + sqlwhere + "\"  sqlorderby=\"" + orderby + "\"  sqlprimarykey=\"t1.id\" sqlsortway=\"Desc\"/>" + 
                            "<head>"+ 
                            "	<col width=\"0%\" hide=\"true\" text=\"\" column=\"id\"/>" + 
                            "   <col width=\"30%\"  text=\"" + SystemEnv.getHtmlLabelName(229, user.getLanguage())+ "\" orderkey=\"docsubject\" column=\"docsubject\"/>" + 
                            "   <col width=\"30%\"  text=\"" + SystemEnv.getHtmlLabelName(2094, user.getLanguage()) + "\" display=\"true\" orderkey=\"ownerid\" column=\"ownerid\" transmethod=\"weaver.hrm.resource.ResourceComInfo.getResourcename\"/>" + 
                            "   <col width=\"35%\"  text=\"" + SystemEnv.getHtmlLabelName(723, user.getLanguage())+ "\" display=\"true\" orderkey=\"doclastmoddate\" column=\"doclastmoddate\" transmethod=\"weaver.splitepage.transform.SptmForDoc.getModifydate\" otherpara=\"column:doclastmodtime\"/>" + 
                            "</head>" + 
                      "</table>";
String sessionkey = Util.getEncrypt(Util.getRandom());
Util_TableMap.setVal(sessionkey, tableString);
apidatas.put("result", sessionkey);
return apidatas;
```


## 前端说明

```
import { WeaTable } from 'comsRudex'

WeaTable.action //table的一些action
```

组件参数说明如下：

参数 | 说明 | 类型 | 默认
---|---|---|---
sessionkey | 列表的数据和组件标识，一半通过搜索条件接口返回 | string |
hasOrder | 是否启用排序 | bool | false
needScroll | 是否启用table内部列表滚动，将自适应到父级高度 | bool | false
onOperatesClick | 自定义操作按钮点击方法，不设置此项将自动读取table统一接口中的全局方法 | function | function(record,index,operate,flag){ 当前行数据，当前行号，被点击的操作按钮数据，被点击的操作按钮的key }


## WeaTable.action


**1、WeaTable.action.getDatas(sessionkey,current,pageSize)**

每次搜索返回最新sessionkey时候触发table更新

参数 | 说明 | 类型 | 默认
---|---|---|---
sessionkey | 列表的数据和组件标识，一般通过搜索条件接口返回 | string |
current | 指定新的当前页码，一般在reload情况下如需显示当前页可传入 | number | 1
pageSize | 指定新的分页大小 | number |  默认从后台配置读取,初始10

**2、WeaTable.action.setColSetVisible(sessionkey,bool)**

自定义列显隐


**3、WeaTable.action.tableColSet(sessionkey,isInit)**

自定义列接口触发

参数 | 说明 | 类型 | 默认
---|---|---|---
sessionkey | 列表的数据和组件标识，一般通过搜索条件接口返回 | string |
isInit | 获取数据，保存数据标识, 默认保存由内部控制，显示定制列时获取数据，传入true | bool | true


## redux 部分常用状态值说明

参数 | 说明 | 类型 
---|---|---
loading | 列表内部接口请求触发的加载状态 | bool 
selectedRowKeys | 列表被选择的行id，例如判断是否选择为空 | array
current | 当前页码 | number
pageSize | 当前分页大小 | number
