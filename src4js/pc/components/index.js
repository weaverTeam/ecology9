// react
import React from 'react';
import { render } from 'react-dom';
// redux
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk/lib/index';
// react-router
import { createHistory, useBasename, createHashHistory } from 'history';
import useRouterHistory from 'react-router/lib/useRouterHistory';
import IndexRedirect from 'react-router/lib/IndexRedirect';
import Router from 'react-router/lib/Router';
import Route from 'react-router/lib/Route';
// react-router-redux
import { routerReducer } from 'react-router-redux/lib/reducer';
import syncHistoryWithStore from 'react-router-redux/lib/sync';
// redux components
// import { comsReducer } from 'comsRedux';
// components
import Home from './router/Home';
import Antd from './router/Antd';
import Instruction from './router/Instruction';
import Log from './router/Log';
import Coms from './router/Coms';
import Test from './router/Test';

import './css/index.less';
import './css/md.less';

let debug = true,
  devTool = window.__REDUX_DEVTOOLS_EXTENSION__,
  // reducers
  reducers = combineReducers({
    // ...comsReducer,
    routing: routerReducer
  }),
  // thunk
  thrunk = applyMiddleware(thunkMiddleware),
  // use devtool
  initialState = debug && devTool ? compose(thrunk, window.__REDUX_DEVTOOLS_EXTENSION__()) : thrunk,
  // createStore
  store = createStore(reducers, initialState);

//module.hot && module.hot.accept('../index.js', () => {
//store.replaceReducer(reducers)
//})

store.subscribe(() => {
  // console.log(store.getState())
});

let browserHistory = useRouterHistory(createHashHistory)({
    queryKey: '_key',
    basename: '/'
  }),
  history = syncHistoryWithStore(browserHistory, store);

const Root = () => (
  <Provider store={store}>
    <Router history={history}>
      <Route key='/' path="/" component={ Home }>
	      <IndexRedirect to="instruction" />
	      <Route key='instruction' path="instruction" component={ Instruction } />
	      <Route key='log' path="log" component={ Log } />
	      <Route key='antd' path="antd" component={ Antd } />
	      <Route key='coms' path="coms" component={ Coms } />
	      <Route key='test' path="test" component={ Test } />
      </Route>
    </Router>
  </Provider>
 );

try {
  render(<Root />, document.getElementById('container'), () => {});
} catch(e) {
  window.console ? console.log('出错了： ', e) : alert('出错了： ' + e);
}