import React from 'react'
import RepoList from './Pengyou/repoList'
import RepoListBtn from './Pengyou/repoListBtn'
import $ from 'jquery'

export default class AddRepoList extends React.Component {
  //当组件AddRepoList被调用时，React会调用组件的构造函数。
  //当RepoList组件需要获取url属性时，就会在this.state初始化一个包含url的对象（当React调用组件的render方法时）
  constructor(props) {
    super(props);
    this.state = {
    	url : '',
    	data : ''
    };
    this.handleClick = this.handleClick.bind(this);
  }
  //处理按钮的click事件。当按钮监听到click操作后，会更新组件的state值，同时AddRepoList会重新render RepoListBtn组件.
  handleClick(arg){
  	if(arg == 'YES'){
  		this.setState({url: 'https://api.github.com/search/repositories?q=javascript&sort=stars'});
  	}
  }


  //React调用AddRepoList组件的render()方法，此时React会更新DOM去匹配组件的render输出
  //组件AddRepoList向子组件RepoListBtn和RepoList传输数据，将属性onBtnClick和promise分别挂载到RepoListBtn和RepoList组件
  //的props上。
  render() {
  	const url = this.state.url;
    return (
    	<div>
    		<RepoListBtn onBtnClick = {this.handleClick}/><br />
      		<RepoList promise = {$.getJSON(url)}/>
    	</div>
    );
  }
}


