import React from 'react'
// import LinCom from './Linxin/components/lin_component';
import Basket from './Linxin/components/demo_component';
// import Father from './Linxin/components/Father_com';
import { Provider } from 'react-redux';
import { reducer } from './Linxin/redux/reducer';
import { createStore } from 'redux';
const store = createStore(reducer);

const Main = () => (
  <div>
    {/* <LinCom name="mycom"/> */}
    {/* <a href="https://github.com/ylinwind/gg">昨天的项目地址 暂时没有弄到这个位置来。。。</a>*/}
    
    {/* <Father/> */}
    <Provider store={store}>
      <Basket/> 
    </Provider>
  </div>
)

export default Main