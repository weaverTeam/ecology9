import React from 'react';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { change } from '../redux/action';
import '../style/style.css';


//用react-redux
class Basket extends React.Component{
    constructor(props){
        super(props);
        this.state={

        }
    }
    render(){
        console.log(this.props.state,'ggg')
        return(
            <article className="lin_box">
                <div className="lin_screen">
                    {this.props.state.num}
                </div>
                <div className="lin_btn">
                    <button onClick={this.add}>add</button>
                    <button onClick={this.jian}>jian</button>
                </div>
            </article>
        )
    }
    add=()=>{
        console.log(1)
        this.props.action({type:'ADD',num:this.props.state.num})
    }
    jian=()=>{
        console.log(2);
        this.props.action({type:'JIAN',num:this.props.state.num})
    }
}
function mapStateToProps(state){
    return{
        state
    }
}
function mapDispatchToProps(dispatch){
    return{
        action:bindActionCreators(change,dispatch)
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Basket);