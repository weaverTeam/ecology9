import React from 'react';
import Child from './Child_com';

class Father extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            Father_num : 0
        }
    }
    render(){
        return(
            <div>
                <input type="text" onChange={this.dataChange} value={this.state.Father_num}/>
                <hr/>
                <Child 
                num={this.state.Father_num}
                onClickAction={(num)=>{
                    this.setState({
                        Father_num:num
                    })
                }}
                />
            </div>
        )
    }
    dataChange = (e) => {
        console.log(this,e.target.value,'---')
        this.setState({
            Father_num:e.target.value
        })
    }
    // shouldComponentUpdate(nextProps,nextState) { //是否允许更新，用来做组件优化
    //     console.log(nextProps,nextState,'fu_shouldComponentUpdate')
    //     return true;
    // }
    componentWillMount() {
        console.log('componentWillMount')
    }
    //  //Updating
    // componentWillReceiveProps() { //当父组件变化时，子组件需要受控，要在这里加判断然后setState
        
    // }
    componentDidMount(){

    }
}
export default Father;