import React from 'react';

class Child extends React.Component{
    constructor(props){
        super(props);
        this.state={
            child_num : 0
        }
    }
    shouldComponentUpdate(nextProps,nextState) { //是否允许更新，用来做组件优化
        console.log(nextProps,nextState,'zi_shouldComponentUpdate');
        if(this.state!==nextState||this.props!==nextProps){
            console.log(999)
            return true;
        }else{
            return false;
        }
    }
     //Updating
    componentWillReceiveProps(nextProps) { //当父组件变化时，子组件需要受控，要在这里加判断然后setState
        console.log('zi_',nextProps)
    }
    render(){
        return(
            <div>
                child-num:
                <span>father_num:{this.props.num}</span><br/>
                <span>state_num  :{this.state.child_num}</span>
                <br/>
                <button onClick={this.add}>子组件加1</button>
                <br/>
                <button
                    onClick={
                        ()=>{
                            if(typeof this.props.onClickAction ==='function'){
                                this.props.onClickAction(0);
                            }
                        }}
                >清空父级数据</button>
                
            </div>
        )
    }
    add = () =>{
        this.setState({
            child_num:this.state.child_num+1
        })
    }
 }
export default Child;