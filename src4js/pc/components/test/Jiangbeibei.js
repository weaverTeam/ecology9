require("!style-loader!css-loader!./Jiangbeibei/css/index.css")
import React from 'react'
import ParentCom from './Jiangbeibei/components/ParentCom'
import WeaRichTextCom from './Jiangbeibei/components/WeaRichTextCom'

const Main = () => (
  <div>
  	<h2 className="title">组件传值+生命周期</h2>
  	<ParentCom/>
  	<h2 className="title">富文本</h2>
	<WeaRichTextCom/>
	
  </div>
)

export default Main
