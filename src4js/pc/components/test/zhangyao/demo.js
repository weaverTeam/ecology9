import React from 'react';
import { WeaRichText } from 'ecCom';
export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      value: '',
    }
  }
  render() {
    const { status, value } = this.state;
    return (
      <div style={{ margin: '20px 0'}}>
        {/*title*/}
        <h1 className="api">引用组件库：富文本组件</h1>
        {/*显示富文本框输入内容*/}
        <div style={{ marginBottom: 20, minHeight: 100, border: '1px solid #ddd' }} dangerouslySetInnerHTML={{__html: value}} />
        <WeaRichText 
          id='test'
          value={value}
          ckConfig
          bottomBarConfig
          bootomBarStyle
          onChange={value => this.setState({ value })}
          onStatusChange={status => this.setState({ status })}
        />
      </div>
    )
  }
};
