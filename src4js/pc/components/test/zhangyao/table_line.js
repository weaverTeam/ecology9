import React from 'react'
export default class Tabelline extends React.Component{
	constructor(props){
		super(props)
		//子组件state.value，用于向父组件传递数据
		this.state = {
			value : 2
		}
	}
	render() {
		return (
				//点击子组件时，执行回调函数，将子组件的state.value传入父组件
				//随后子组件state.value++
				<tr onClick={()=>{
					if(typeof this.props.handleChange === 'function'){
						//调用传入的父组件方法，改变父组件state.value
						this.props.handleChange(this.state.value);
						this.setState({value : this.state.value+1});
					}
				}}>
					{/*将从父组件获取的数据填充到子组件*/}
					<td>{this.props.parameter}</td>
					<td>{this.props.explain}</td>
					<td>{this.props.type}</td>
					<td>{this.props.moren}</td>
				</tr>
			)
	}
}
