import React from 'react';
import Tabelline from './table_line'
export default class Main extends React.Component{
constructor(props) {
    super(props);
    this.state = {
    	//父组件向子组件传递的数据
        myDatas : [
			{"parameter":'shape','explain':'指定头像的形状','type':'number','moren':'circle'},
			{"parameter":'size','explain':'设置头像的大小','type':'number','moren':'default'},
			{"parameter":'src','explain':'图片类头像的资源地址','type':'string','moren':'-'},
			{"parameter":'icon','explain':'设置头像的图标类型,参考Icon组件','type':'string','moren':'-'}
		],
		//父组件state.value
		value : 1
    }
  }
//父组件将向子组件传递的props
handleChange = (val)=>{
	this.setState({
		value : val
	})
} 
//监听父组件state.value的编号
componentDidUpdate() {
	alert('父组件state.value: '+this.state.value)
}
render() {
    const myDatas = this.state.myDatas;
    //调用map函数遍历state.myDates向子组件传数据
    const myData = myDatas.map((pro)=>{
    	//通过子组件的props接收来自于父组件的数据
    	return <Tabelline handleChange={this.handleChange} parameter={pro.parameter} explain={pro.explain} type={pro.type} moren={pro.moren}/>
    })
    //父组件渲染
    return (
    	//在父组件生成一个table-title
     	<table className="wea-table">
			<thead>
				<tr>
				    <th>参数</th>
				    <th>说明</th>
				    <th>类型</th>
				    <th>默认值</th>
			    </tr>
			</thead>
			{/*载入子组件*/}
		    <tbody>
		    	{myData}
		    </tbody>
		</table>
    )
  }
}