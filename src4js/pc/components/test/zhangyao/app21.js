import React from 'react'
//调用E9组件库组件wea-button
import {WeaButton} from 'ecCom'
class App21 extends React.Component{
	constructor(props){
		super(props);
		//子组件状态,初始化state
		this.state = {
			num : 0
		}
	}
	render(){
		//在此块级作用域有效的num
		let num =  this.state.num;
		return (
			<div>
				{/*调用E9组件库组件Wea-buttom*/}
				<WeaButton onClick={()=>{
				//点击按钮修改state.num值
					this.setState({
						num : num+1
					})
				}} text={'改变子组件状态Num'}/>
				
				<WeaButton onClick={()=>{
				/*
					此处实现了子组件向父组件传递数据：
					验证父组件传入属性的类型是否为function
					在子组件中调用父组件传入的onChange方法
					传入子组件的state.num，执行回调函数，调用父组件的setState，修改父组件state.fatherNum
				*/
					if(typeof (this.props.onChange)==='function'){
						this.props.onChange(num)
					}
				}} text={'回调修改父级状态fatherNum'}/>

				{/*查看子组件state.num值*/}
				<div className="wea-indent">子组件状态Num：{this.state.num}</div>
				{/*查看父组件state.fatherNum*/}
				<div className="wea-indent">父组件状态fatherNum：{this.props.fatherNum}</div>
			</div>
			)
	}
}
export default class App21Father extends React.Component{
	constructor(props){
		super(props);
		//父组件初始化state
		this.state = {
			fatherNum : 0
		}
		//修正this指针的指向，绑定得到当前类
		this.getTime = this.getTime.bind(this)
	}
	//自定义方法，查看组件生命周期的触发时间点
	getTime(){
		 const mydate = new Date(); 
		 const time = mydate.toLocaleString();
		 const sec = mydate.getTime().toString();
		 return time+' second : '+sec;
	}
	//在刷新页面后首先触发componentWillMount
	componentWillMount() {
		console.log('componentWillMount at:'+this.getTime())

    }
    //组件比较小时，componentWillMount和componentDidMount几乎同步触发
    componentDidMount() {
    //1、用来发起请求
    //2、这时候真实dom已有，这里用来集成传统组件或者做dom操作
		 console.log('componentDidMount at:'+this.getTime())
    }
    //当组件接收来自父组件的props触发componentWillReceiveProps
    //Updating
    componentWillReceiveProps() { 
    //当父组件变化时，子组件需要受控，要在这里加判断然后setState
		console.log('componentWillReceiveProps at:'+this.getTime())
    }
    //当组件状态将要发生改变时，触发shouldComponentUpdate
    shouldComponentUpdate() { 
    //是否允许更新，用来做组件优化
		console.log('shouldComponentUpdate at:'+this.getTime())
        return true;
    }
    //组件将要更新，触发componentWillUpdate
    componentWillUpdate() {
    //next will render
		console.log('componentWillUpdate at:'+this.getTime())

    }
    //组件更新完毕，数据更新后页面发生变化
    componentDidUpdate() { 
    //每次组件setState都会触发，通常用来动态调整一些样式
		console.log('componentDidUpdate at:'+this.getTime())
    }
    //组件将要被移出时触发componentWillUnmount
    //Unmounting
    componentWillUnmount() {
		console.log('componentWillUnmount at:'+this.getTime())
    }
	render(){
		return(
		<div className="wea-Margin-top">	
			{/*通过props将父组件setState传入子组件，在子组件中改变父组件状态，反馈给父组件*/}
			<App21 fatherNum={this.state.fatherNum} onChange={(v)=>{
				this.setState({
					fatherNum : v
				})
			}}/>
			{/*调用E9组件库组件Wea-buttom
			点击按钮修改父组件state。fatherNum*/}
			<WeaButton onClick={()=>{
				this.setState({
					fatherNum : this.state.fatherNum+1
				})
			}} text={'父级btn点击改变fatherNum'}/>
		</div>
		)
	}
}