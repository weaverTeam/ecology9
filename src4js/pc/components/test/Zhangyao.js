import React from 'react';
import './zhangyao/index.less';
import Demo from './zhangyao/demo';
import App21Father from './zhangyao/app21';
import Table_demo from './zhangyao/table_demo';
const Main = () => (
	<div>
		{/*titel*/}
		<h1 className="wea-api">API</h1>
		{/*表格组件*/}
		<Table_demo/>
		{/*父子组件通信和生命周期使用*/}
		<App21Father/>
		{/*富文本组件*/}
		<Demo/>
	</div>
)

export default Main