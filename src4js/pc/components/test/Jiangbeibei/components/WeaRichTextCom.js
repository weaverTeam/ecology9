import React from 'react'
import {WeaRichText} from 'ecCom'
export default class WeaRichTextCom extends React.Component{
	constructor(props){
		super(props);
		this.state={
			status:'',
			value:''
		}
	}
	render(){
		const { status ,value}=this.props;
		return (
			<div style={{ margin: '20px 0'}}>
                <div style={{ marginBottom: 20, minHeight: 100, border: '1px solid #ddd' }} dangerouslySetInnerHTML={{__html: value}} />
                <WeaRichText 
                    id='test'
                    value={value}
                      ckConfig={{
				        toolbar:[
				            { name: 'document', items: [ 'Source'] },
				            { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
				            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
				            { name: 'colors', items: [ 'TextColor' ] },
				            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
				            { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] }
				        ],
				        extraPlugins: 'autogrow',
				        height:150,
				        autoGrow_minHeight: 150,
				        autoGrow_maxHeight: 600,
				        removePlugins: 'resize',
				        uploadUrl: '/api/blog/fileupload/uploadimage',
				    }}
                    bottomBarConfig
                    bootomBarStyle
                    onChange={value => this.setState({ value })}
                    onStatusChange={status => this.setState({ status })}
               
                />
          	  </div>

			)
	}
}