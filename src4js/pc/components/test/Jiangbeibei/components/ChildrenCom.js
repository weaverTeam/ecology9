import React from 'react'
class ChildrenCom extends React.Component{
	constructor(props){
		super(props)
		this.state={
			valueChange:'',
			opacity:1.0
		}
		this.ChangeValueOne=this.ChangeValueOne.bind(this)
		this.ChangeValueTwo=this.ChangeValueTwo.bind(this)
	}
	ChangeValueOne(e){
		this.props.onChangeValue1(e.target.value)
	}
	ChangeValueTwo(e){
		this.props.onChangeValue2(e.target.value)
	}
	componentWillMount(){}
	componentWillReceiveProps(nextProps){
		const valuel=this.props.value2;
		//alert(valuel);
		this.setState({valueChange:valuel});
	}

	componentDidMount(){
		this.timer=setInterval(()=>{
		    let opacity=this.state.opacity;
		    opacity-=0.05
		    if(opacity<0.1){
		    	opacity=1.0;
		    }
		    this.setState({opacity:opacity});
		},100);
	}
	shouldComponentUpdate(){
		return true;
	}
	componentWillUpdate(){
		console.log("componentWillUpdate")
	}
	componentDidUpdate(){
		console.log("ComponentDidUpdate")
	}
	componentWillUnmount(){
		clearInterval(this.timer);
	}
	render(){
		const {value1,value2}=this.props;
		let valueChange=this.state.valueChange;
		let opacity=this.state.opacity;
		return (
			<div>
				<p>子组件</p>
				<input type="text" placeholder="please input value1" value={value1} onChange={this.ChangeValueOne}/>
				<input type="text" placeholder="please input value2" value={value2} onChange={this.ChangeValueTwo}/>
				<div>componentWillReceiveProps:<span>{valueChange}</span></div>
				<div style={{opacity:opacity}}>componentDidMount</div>
			</div>
			)
	}
}
export default ChildrenCom
