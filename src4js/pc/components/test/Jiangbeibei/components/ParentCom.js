import React from 'react'
import ChildrenCom from './ChildrenCom'
class ParentCom extends React.Component{
	constructor(props){
		super(props)
		this.state={
			value1:'',
			value2:''
		}
		this.onChangeOne=this.onChangeOne.bind(this)
		 this.onChangeTwo=this.onChangeTwo.bind(this)	
		 this.onChangeValue1=this.onChangeValue1.bind(this)
		 this.onChangeValue2=this.onChangeValue2.bind(this)
	}
	onChangeValue1(value){
		this.setState({value1:value});
	}
	onChangeValue2(value){
		this.setState({value2:value});
	}
	onChangeOne(e){

		this.setState({value1:e.target.value});
	}
	onChangeTwo(e){
		this.setState({value2:e.target.value});
	}
	render(){
		var value1=this.state.value1;
		var value2=this.state.value2;
		return (
			<div>
				<p>父组件</p>
				<input type="text" placeholder="please input value1" value={value1} onChange={this.onChangeOne} />
				<input type="text" placeholder="please input value2" value={value2} onChange={this.onChangeTwo}/>
				<ChildrenCom value1={value1} value2={value2}  onChangeValue1={this.onChangeValue1} onChangeValue2={this.onChangeValue2}/>
			</div>
			)
	}
}
export default ParentCom