import React from 'react';

class Cjm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        };
    }

    handleClick = (e) => {//解决this指向：（1）使用箭头函数（2）下面用onClick={this.handleClick.bind(this)也ok}
        const value = e.target.value;
        this.setState({
            value: parseInt(value)+1
        });
    } 

    render() {
        return (
            <div>
                <button onClick={this.handleClick} value={this.state.value}>CLICK ME</button>
                <p>{this.state.value}</p>
            </div>
        )
    }
}
export default Cjm