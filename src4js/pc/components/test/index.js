import Chenjiamin from './Chenjiamin';
import Jiangbeibei from './Jiangbeibei';
import Linxin from './Linxin';
import Pengyou from './Pengyou';
import Zhangrenjian from './Zhangrenjian';
import Zhangyao from './Zhangyao';
import Zhuzhao from './Zhuzhao';

export default {
  cjm: Chenjiamin,
  jbb: Jiangbeibei,
  lx: Linxin,
  py: Pengyou,
  zrj: Zhangrenjian,
  zy: Zhangyao,
  zz: Zhuzhao,
}
