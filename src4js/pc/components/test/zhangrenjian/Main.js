import React from 'react';
import InputData from './CommonComponent/InputData';

class Main extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            text: '',
            data: []
        };

        this.addData = this.addData.bind(this);
    }

    componentDidUpdate() {
        this.timer = setTimeout(() => {
            console.log(1);
        });
    }

    componentWillUpdate() {
        clearTimeout(this.timer);
    }

    //使input输入框的value属性受控
    onChangeHandle(e) {
        const value = e.target.value;
        this.setState({
            text: value
        });
    }

    addData() {
        const text = this.state.text;
        this.setState({
            data: this.state.data.concat(text), //点提交时往data里加入一条新text
            text: '' // 点提交后清空输入框
        });
    }



    render() {

        return (
            <div className="centerWrapper">
                <InputData
                    text={this.state.text}
                    onChangeHandle={this.onChangeHandle.bind(this)}
                    onSubmit={this.addData}
                />
                <DataList data={this.state.data}></DataList>
            </div>
        )

    }
}


//渲染data数组里所有数组元素的组件
class DataList extends React.Component{
    render() {
        return (
            <ul>
                {
                    this.props.data.map((elem) => (<li>{elem}</li>) )
                }
            </ul>
        )

    }
}

export default Main;