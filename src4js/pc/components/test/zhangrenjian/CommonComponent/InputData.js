import React from 'react';
import './index.less';

class InputData extends React.Component{

    static propTypes = {
        onSubmit: React.PropTypes.func.isRequired,
        onChange: React.PropTypes.func.isRequired,
        text: React.PropTypes.string,
        clickButtonText: React.PropTypes.string,
        placeholder: React.PropTypes.string
    };

    render() {
        return (
            <div>
                <input
                    type="text"
                    placeholder={this.props.placeholder ? this.props.placeholder : "请输入文本"}
                    value={this.props.text}
                    onChange={this.props.onChangeHandle}
                    className="textbox"
                />
                <input
                    className="button"
                    type="button"
                    value={this.props.clickButtonText ? this.props.clickButtonText : "提交"}
                    onClick={this.props.onSubmit}
                />
            </div>
        )

    }
}

export default InputData;