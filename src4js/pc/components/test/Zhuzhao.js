import React from 'react';
import {Router,Route,hashHistory,Link,IndexRoute} from 'react-router'
import Style from './Zhuzhao/index.css';

const FToC = require("./Zhuzhao/fToc.js");
const CToF = require("./Zhuzhao/cTof.js");

const Nav = React.createClass({
    render : function () {
        return (
            <div>
                <nav>
                    <ul>
                        <li><Link to="/test/fToc">父向子传值</Link></li>
                        <li><Link to="/test/cTof">子向父传值</Link></li>
                    </ul>
                </nav>
                <div>
                    {this.props.children}
                </div>
            </div>
        )
    }
});


const routes = <Route path="/test" component={Nav}>
    <Route path="/test/fToc" component={FToC}/>
    <Route path="/test/cTof" component={CToF}/>
</Route>;

const Main = () => (
    <div>
        <Router routes={routes} history={hashHistory} />
    </div>
);

export default Main