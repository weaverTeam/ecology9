/**
 * Created by Administrator on 2017/9/20
 */
import React from "react";

const InputCom = React.createClass({
    getInitialState : function () {
        return {
            value : ""
        }
    },
    textChange : function (event) {
        this.setState({value:event.target.value});
    },
    render : function () {
        return (
            <div>
                <h1>父组件向子组件传值练习</h1>
                <p>这是父组件，输入</p>
                <input value={this.state.value} type="text" onChange={this.textChange}/>
                <p>这是子组件</p>
                <Pcom value={this.state.value} />
            </div>
        )
    }
});

const Pcom = React.createClass({
    render : function () {
        return (
            <p>{this.props.value}</p>
        )
    }
});


module.exports = InputCom;
