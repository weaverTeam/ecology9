import React from 'react';
import {Router,Route,hashHistory,Link,IndexRoute} from 'react-router'

const routes = <Route path="/test" component={Nav}>
        <Route path="/test/fToc" component={}/>
    </Route>;
<Router/>

const Nav = React.createClass({
    render : function () {
        return (
            <nav>
                <ul>
                    <li><Link to="/test/fToc">父向子传值</Link></li>
                    <li><Link to="/test/*">home</Link></li>
                    <li><Link to="/cTof">子向父传值</Link></li>
                    <li><Link to="/siblings">兄弟组件传值</Link></li>
                    {this.props.children}
                </ul>
            </nav>
        )
    }
});

module.exports = Nav;