/**
 * Created by Administrator on 2017/9/20
 */
import React from 'react';


const Ccom = React.createClass({
    getInitialState : function () {
        return {
            value : ""
        }
    },
    textChange : function (event) {
        this.setState({value : event.target.value});
        this.props.changeValue(this.state.value);
    },
    render : function () {
        return (
            <input type="text" onChange={this.textChange}/>
        )
    }
});

const Fcom = React.createClass({
   getInitialState : function () {
       return {
           value : ""
       }
   },
    change : function (newValue) {
       this.setState({value:newValue})
    },
    render : function () {
        return(
            <div>
                <h1>子组件向父组件传值练习</h1>
                <p>这是父组件，只读</p>
                <input value={this.state.value} type="text" readOnly/><br/>
                <p>这是子组件</p>
                <Ccom changeValue={this.change}/>
            </div>
        )
    }
});
module.exports = Fcom;

