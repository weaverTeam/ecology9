```
import React from 'react';
import { WeaRichText } from 'ecCom';

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      value: '',
    }
  }
  render() {
    const { status, value } = this.state;
    return (
      <div>
        <div style={{ marginBottom: 20, minHeight: 100, border: '1px solid #ddd' }} dangerouslySetInnerHTML={{__html: value}} />
        <WeaRichText 
          id='test'
          value={value}
          ckConfig = {
            {
              font_defaultLabel: '华文彩云',
              font_defaultFamily: '华文彩云/STCaiyun',
              fontSize_defaultLabel: '16',
              fontSize_defaultSize: '16/16px',
            }
          }
          bottomBarConfig
          bootomBarStyle
          onChange={value => this.setState({ value })}
          onStatusChange={status => this.setState({ status })}
        />
      </div>
    )
  }
};
```

