```
import React from 'react';
import { WeaDatePicker } from 'ecCom';

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      value: '2011-11-11',
    }
  }
  render() {
    const { status, value } = this.state;
    return (
      <div style={{ margin: '20px 0'}}>
        <div style={{ marginBottom: 20, minHeight: 100, border: '1px solid #ddd' }} dangerouslySetInnerHTML={{__html: value}} />
        <WeaDatePicker
          value={value}
          onChange={(v)=> {this.setState({value: v})}}
        />
      </div>
    )
  }
};

```