import React from 'react';
import { WeaDatePicker } from 'ecCom';

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      value: '',
      startValue: new Date().setDate(1),
      endValue: new Date(),
    }
  }
  render() {
    const { status, value, startValue, endValue } = this.state;
    return (
      <div style={{ margin: '20px 0'}}>
        <div style={{ marginBottom: 20, minHeight: 100, border: '1px solid #ddd' }} dangerouslySetInnerHTML={{__html: value}} />
        <WeaDatePicker
          value={value}
          startValue={startValue}
          endValue={endValue}
          onChange={value => this.setState({ value })}
        />
      </div>
    )
  }
};
