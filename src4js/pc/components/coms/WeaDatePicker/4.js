import React from 'react';
import { WeaDatePicker } from 'ecCom';

export default class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      value: '2017-09-10',
    }
  }
  render() {
    const { status, value} = this.state;
    return (
      <div style={{ margin: '20px 0'}}>
        <div style={{ marginBottom: 20, minHeight: 100, border: '1px solid #ddd' }} dangerouslySetInnerHTML={{__html: value}} />
        <WeaDatePicker
          style={{marginRight: 10}}
          value={value}
          formatPattern="7"
          onChange={value => this.setState({ value })}
        />
        <WeaDatePicker
          style={{marginRight: 10}}
          value={value}
          formatPattern="8"
          onChange={value => this.setState({ value })}
        />
        <WeaDatePicker
          style={{marginRight: 10}}
          formatPattern="3"
          value={value}
          onChange={value => this.setState({ value })}
        />
        <br/><br/>
        <WeaDatePicker
          style={{marginRight: 10}}
          noInput
          value={value}
          formatPattern="7"
          onChange={value => this.setState({ value })}
        />
        <WeaDatePicker
          style={{marginRight: 10}}
          noInput
          value={value}
          formatPattern="8"
          onChange={value => this.setState({ value })}
        />
        <WeaDatePicker
          style={{marginRight: 10}}
          noInput
          formatPattern="3"
          value={value}
          onChange={value => this.setState({ value })}
        />
      </div>
    )
  }
};