import React from 'react';
import WeaTableRedux_0 from './WeaTableRedux_0.js';
import WeaTableRedux_0_md from './WeaTableRedux_0.md';
import WeaRichText_0 from './WeaRichText_0.js';
import WeaRichText_0_md from './WeaRichText_0.md';
import WeaRightMenu_0 from './WeaRightMenu_0.js';
import WeaRightMenu_0_md from './WeaRightMenu_0.md';
import DragChooseDemo from './DragChooseDemo/index.js';
import DragChooseDemo_md from './DragChooseDemo/index.md';
import WeaDatePicker_1 from './WeaDatePicker/1.js';
import WeaDatePicker_1_md from './WeaDatePicker/1.md';
import WeaDatePicker_2 from './WeaDatePicker/2.js';
import WeaDatePicker_2_md from './WeaDatePicker/2.md';
import WeaDatePicker_3 from './WeaDatePicker/3.js';
import WeaDatePicker_3_md from './WeaDatePicker/3.md';
import WeaDatePicker_4 from './WeaDatePicker/4.js';
import WeaDatePicker_4_md from './WeaDatePicker/4.md';

//let paths = require.context('./', true, /^\.\/Wea\w+\/Wea\w+\.js$/);
//
//paths.keys().forEach(path => {
//const com = req(path);
//const md = req(path.replace('.js','.md'));
//});

export default {
  WeaTableRedux: [
    {
      title: '流程待办',
      com: WeaTableRedux_0,
      md: WeaTableRedux_0_md
    },
  ],
  WeaRichText: [
    {
      title: '默认编辑器',
      com: WeaRichText_0,
      md: WeaRichText_0_md
    },
  ],
  WeaRightMenu: [
    {
      title: '右键菜单',
      com: WeaRightMenu_0,
      md: WeaRightMenu_0_md
    },
  ],
  WeaDragChoose: [
    {
      title: '拖拽选择',
      com:DragChooseDemo,
      md: DragChooseDemo_md
    }
  ],
  WeaDatePicker: [
    {
      title: '基本使用',
      com: WeaDatePicker_1,
      md: WeaDatePicker_1_md
    },
    {
      title: '限制选择',
      com: WeaDatePicker_2,
      md: WeaDatePicker_2_md
    },
    {
      title: '不显示input框',
      com: WeaDatePicker_3,
      md: WeaDatePicker_3_md
    },
    {
      title: '自定义展示',
      com: WeaDatePicker_4,
      md: WeaDatePicker_4_md
    },

  ],
};

