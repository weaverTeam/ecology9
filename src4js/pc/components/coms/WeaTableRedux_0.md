```
import Immutable from 'immutable' //E9 Redux 版 table 使用不可变数据 进行存储
import { WeaTools } from 'ecCom'
import { WeaTable } from 'comsRedux'
const WeaTableAction = WeaTable.action;

let dataKey = '';
//--------------------------- action 中这样写：

const doSearch = ( params = {} ) => {
    return (dispatch, getState) => {
    	// 这里未绑定redux 暂存在 变量里
        //dispatch({
        //    type: types.LOADING,
        //    loading: true
        //});
        WeaTools.callApi('/api/workflow/reqlist/list', 'GET', params).then(data => {
            // 触发 table 更新
            dispatch(WeaTableAction.getDatas(data.sessionkey, params.current || 1));
            // 保存 sessionkey 以便组件取用
            //dispatch({
            //	type: types.SETDATAKEY,
            //	dataKey: data.sessionkey,
            //	loading: false
            //})
            // 这里未绑定redux 暂存在 变量里
            dataKey = data.sessionkey;
        })
    }
}

//--------------------------- Component 中这样写：

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Button } from 'antd'

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.showCustom = this.showCustom.bind(this)
    }
    componentDidMount() {
    	//一些初始化请求
        const { actions } = this.props;
        //借用流程待办的数据接口
        const params = {
        	viewScope: 'doing',
			offical: '',
			officalType: -1,
			method: 'all',
			complete: 0,
			viewcondition: 0,
			actiontype: 'splitpage',
        }
        actions.doSearch(params); //MainAction 中的 doSeach 会请求到 sessionkey 详见 action 说明
    }
    showCustom(){
    	//一些初始化请求
        const { actions } = this.props;
        actions.setColSetVisible(dataKey,true);
        actions.tableColSet(dataKey,true); //定制列数据接口触发
    }
    render(){
        const { comsWeaTable } = this.props;
        return (
        	<div>
	        	<div style={{ lineHeight: '48px'}}>
	        		<Button type="primary" onClick={this.showCustom} disabled={!dataKey} >显示定制列</Button>
	        		<span style={{ marginLeft: 10, color: '#f00' }}>注: 借用流程待办接口，部分操作可能会修改数据库，例如定制列、转发功能</span>
        		</div>
                <WeaTable 
                    sessionkey={dataKey}
                    hasOrder={true}
                    needScroll={true}
                />
            </div>
        )
    }
}

// 把 state map 到组件的 props 上
const mapStateToProps = state => {
    const { comsWeaTable } = state;
    return { 
    	comsWeaTable //从 redux 中获取可能有用的 table 的状态：
    }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
    return {
        //...析构 WeaTableAction 会覆盖 MainAction 的同名方法，请不要重名
    	actions: bindActionCreators({doSearch, ...WeaTableAction}, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
```

