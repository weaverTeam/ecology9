import {WeaDragChoose} from 'ecCom';
import "./index.less";

class DragChooseDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectKeys: [],
            startRow: -1,
            startColumn: -1,
            endRow: -1,
            endColumn: -1
        };
    }
    
    render() {
        let trs = [];
        let tds = [];
        for (let i=0;i<9;i++) {
            let trIdx = Math.floor(i/3), tdIdx = i%3;
            tds.push(
                <td key={i} itemPosition={[trIdx, tdIdx]}></td>
            );
            if (i%3 == 2) {
                trs.push(<tr key={trIdx}>{tds}</tr>);
                tds = [];
            }
        }
        return (
            <div className="wea-demo-choose-drag">
                <WeaDragChoose onSelect={this.select} onSelectEnd={this.selectEnd} activeClass="active">
                    <table>
                        <tbody>
                            {trs}
                        </tbody>
                    </table>
                </WeaDragChoose>
            </div>
        );
    }
    
    select =  (dragState) => {
        const {startRow, startColumn, endRow, endColumn} = dragState;
    }
    
    selectEnd = (dragState) => {
        console.log("selectEnd", dragState);
    }
    
}

export default DragChooseDemo;