```
import React from 'react';
import { WeaRightMenu } from 'ecCom';

const menu = [
  {
    key: '1',
    icon: <i className='icon-coms-search' />,
    content: '按钮 1',
    onClick: key => console.log(`按钮 ${key}`),
  },
  {
    key: '2',
    icon: <i className='icon-coms-search' />,
    content: '按钮 2',
    onClick: key => console.log(`按钮 ${key}`),
  }
];

export default props => (
  <WeaRightMenu datas={menu} >
    <div style={{ backgroundColor: '#dadada', textAlign: 'center', height: 300 }} >
      <span style={{ lineHeight: 2 }} >点击右键查看菜单</span>
    </div>
  </WeaRightMenu>
);
```

