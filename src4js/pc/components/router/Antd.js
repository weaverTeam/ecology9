import React from 'react';
import Datas from '../menu';

let { comList_1, comList_2, comList_3, comList_4 } = Datas,
	list = comList_1.concat(comList_2).concat(comList_3).concat(comList_4);

const Main = () => (
  <iframe className='wea-components-doc-antd' style={{ height: (list.length + 11) * 42 + 10 }} src='http://1x.ant.design/docs/react/introduce' />
);

export default Main;
