import React from 'react';
import TestPerson from '../test';

const Main = props => {
  const Demo = TestPerson[props.location.query.id];
  return (
    <div>
      <Demo />
    </div>
  )
};

export default Main