import React from 'react';
import { Row, Col, Menu, Icon, Input, Select, } from 'antd';
import Datas from '../menu';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const MenuItem = Menu.Item;
const Option = Select.Option;
const OptGroup = Select.OptGroup;

let { comList_1, comList_2, comList_3, comList_4, test } = Datas,
  list = test.concat(comList_1).concat(comList_2).concat(comList_3).concat(comList_4)

const toLocale = str => {
  return str.toLocaleLowerCase() + str.toLocaleUpperCase()
}

const getOptions = items => {
  return items.map(item => {
    const { key, title, abc } = item;
    return (<Option key={key} value={key + '{$_$}' +  toLocale(key) + toLocale(abc) + title}>{key + ' ' + title}</Option>)
  })
}

const getMenuItem = items => {
  return items.map(item => {
    const { key, title } = item;
    return (
      <MenuItem key={key} >
        <span style={{ fontSize: 13, fontWeight: 400 }}>{key}</span>
        <span style={{ color: '#999', marginLeft: 5 }}>{title}</span>
      </MenuItem>
    )
  })
}

const getTestItem = items => {
  return items.map(item => {
    const { key, title } = item;
    return (
      <MenuItem key={key} >
        <span style={{ fontSize: 13, fontWeight: 400 }}>{title}</span>
      </MenuItem>
    )
  })
}

export default class Home extends React.Component {
  static contextTypes = {
    router: React.PropTypes.object
  }
  constructor(props) {
    super(props);
    this.state = { height: (list.length + 11) * 42 + 10 }
    this.onMenuClick = this.onMenuClick.bind(this);
  }
  componentDidMount() {
// const height = $('.wea-components-doc .wea-components-doc-body-menu').height();
// this.setState({ height });
  }
  componentDidUpdate(preProps, preState) {
// this.componentDidMount()
  }
  onMenuClick(item) {
    let { key } = item,
      { router } =  this.context,
      pathname = key.indexOf('test_') >= 0 ? '/test' : (key === 'instruction' || key === 'log' ? key : '/coms');
    router.push({
      pathname,
      query: pathname === `/test` ? { id: key.split('test_')[1] } : (pathname === `/coms` ? { id: key } : {}),
    });
    $('body','html').scrollTop(0);
  }
  render() {
    let { height } = this.state,
      { location } = this.props,
      { router } =  this.context,
      { pathname, query } = location,
      { id = '' } = query,
      defaultSelectedKeys = id ? [id] : [pathname.split('/')[1]];
    return (
      <div className='wea-components-doc'>
        <Row className='wea-components-doc-header'>
          <Col span={24} style={{ paddingLeft: 280 }}>
            <div style={{ fontSize: 20, color: '#57c5f7', fontWeight: 600, paddingLeft: 24, position: 'absolute', left: 0 }}>E9 组件库文档</div>
            <div style={{ borderLeft: '1px solid #e9e9e9', paddingLeft: 20, height: 33 }}>
              <Select
                size='large'
                placeholder='搜索组件...'
                showSearch
                  style={{ width: 300 }}
                  onSelect={v => router.push({
                  pathname: '/coms',
                  query: { id: v.split('{$_$}')[0] }
                })}
              >
                <OptGroup key='type_0' label="非业务组件">
                  {
                    getOptions(Datas.comList_1)
                  }
                </OptGroup>
                <OptGroup key='type_1' label="业务组件">
                  {
                    getOptions(Datas.comList_2)
                  }
                </OptGroup>
                <OptGroup key='type_2' label="表单组件">
                  {
                    getOptions(Datas.comList_3)
                  }
                </OptGroup>
                <OptGroup key='type_3' label="工具库">
                  {
                    getOptions(Datas.comList_4[0].children)
                  }
                </OptGroup>
              </Select>
            </div>
          </Col>
        </Row>
        <Row style={{ padding: '20px 40px' }}>
          <Col span={24} >
            <Row className='wea-components-doc-body'>
              <Col style={{ paddingLeft: 279 }} >
                <div className='wea-components-doc-body-menu' >
                  <Menu
                    mode="inline"
                    defaultOpenKeys={['test','components','WeaTools']}
                    defaultSelectedKeys={defaultSelectedKeys}
                    style={{ width: 280}}
                    onClick={ this.onMenuClick }
                  >
                    <MenuItem key="instruction">
                      <span style={{ fontSize: 14, fontWeight: 500 }}>相关说明</span>
                    </MenuItem>
                    <MenuItem key="log">
                      <span style={{ fontSize: 14, fontWeight: 500 }}>更新日志</span>
                    </MenuItem>
                    <SubMenu key="test" title="人员测试页">
                      {
                        getTestItem(Datas.test)
                      }
                    </SubMenu>
                    <SubMenu key="components" 
                      title={<span style={{ fontSize: 14, fontWeight: 500 }}>Components</span>} 
                    >
                      <MenuItemGroup title="非业务组件">
                        {
                          getMenuItem(Datas.comList_1)
                        }
                      </MenuItemGroup>
	                    <MenuItemGroup title="业务组件">
                        {
                          getMenuItem(Datas.comList_2)
                        }
	                    </MenuItemGroup>
		                  <MenuItemGroup title="表单组件">
	                      {
	                        getMenuItem(Datas.comList_3)
	                      }
		                  </MenuItemGroup>
		                  <MenuItemGroup title="其他">
	                      {
	                        Datas.comList_4.map(item => {
	                          const { key, title, children } = item;
	                          if(children){
	                            return (
	                              <SubMenu key={key} title={
	                                <span>
	                                  <span style={{ fontSize: 13, fontWeight: 400 }}>{key}</span>
	                                  <span style={{ color: '#999', marginLeft: 5 }}>{title}</span>
	                                </span>
	                              } >
	                                {
	                                  children.map(c => {
	                                    return <MenuItem key={c.key} >{c.title}</MenuItem>
	                                  })
	                                }
	                              </SubMenu>
	                            )
	                          }
	                          return (
	                            <MenuItem key={key} >
	                              <span style={{ fontSize: 13, fontWeight: 400 }}>{key}</span>
	                              <span style={{ color: '#999', marginLeft: 5 }}>{title}</span>
	                            </MenuItem>
	                          )
	                        })
	                      }
		                  </MenuItemGroup>
                  	</SubMenu>
                  </Menu>
                </div>
                <div className='wea-components-doc-body-content' style={{minHeight: height}}>
                  {this.props.children}
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    )
  }
};
