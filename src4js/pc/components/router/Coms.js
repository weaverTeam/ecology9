import React from 'react';
import { Row, Col, Icon, Card, Collapse, } from 'antd';
import Datas from '../menu';
import Coms from '../coms';
import Api from '../api';

const Panel = Collapse.Panel;

let { comList_1, comList_2, comList_3, comList_4 } = Datas,
  list = comList_1.concat(comList_2).concat(comList_3).concat(comList_4);

const getTitle = id =>{
  let title = null;
  list.map(item => {
    if(item.children){
      item.children.map(c => {
        if(c.key === id){
          title = (
            <h1 key={id}>
              <span>{id.split('/')[0]}</span>
              <span style={{marginLeft: 20, fontSize:'0.8em' }}>{c.title}</span>
            </h1>
          )
        }
      })
    }else{
      if(item.key === id){
        title = (
          <h1 key={id}>
            <span>{id}</span>
            <span style={{marginLeft: 20, fontSize:'0.8em' }}>{item.title}</span>
          </h1>
        )
      }
    }
  });
  return title
};

const Main = props => {
  let { id } = props.location.query;
  return(
    <div>
      <div className='wea-components-doc-md markdown-body'>
        { getTitle(id) }
        <h2>
          代码演示<Icon style={{marginLeft: 15}} type="appstore" />
        </h2>
      </div>
      {
        Coms[id] && Coms[id].map((item, i) => {
          return (
            <Card key={i} title={ <item.com /> }>
              <Collapse defaultActiveKey={[]}>
                <Panel header={<span style={{ fontWeight: 600 }}><span className='wea-components-doc-md-card-title'>{item.title}</span> 点击查看源码 <Icon type="edit" /></span>} key='1'>
                  <div className='wea-components-doc-md markdown-body markdown-body-bg-fff'>
                    <article dangerouslySetInnerHTML={{__html: item.md}}></article>
                  </div>
                </Panel>
              </Collapse>
            </Card>
          )
        })
      }
      <div className='wea-components-doc-md markdown-body'>
        <article dangerouslySetInnerHTML={{__html: Api[id]}}></article>
      </div>
    </div>
  )
};

export default Main;
