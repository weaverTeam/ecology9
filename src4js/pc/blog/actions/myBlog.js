import * as types from '../constants/ActionTypes'

import {message, Modal} from 'antd'

import Apis from '../apis/myBlog'

import {batchActions} from 'redux-batched-actions';
import WeaUtils from '../components/Component/wea-utils'
const {GetRegex,DateFormat} = WeaUtils
export const setKeywords = v => {
    return (dispatch, getState) => {
        dispatch({
            type: types.MYBLOG_SET_KEYWORDS,
            data: {value: v, status: 1},
        })
    }
}


export const doLoading = loading => {
    return (dispatch, getState) => {
        dispatch({
            type: types.MYBLOG_LOADING,
            data: {status: 1},
            loading
        })
    }
}

export const getWeiboCKEditorConfig = (params) => {
    return (dispatch, getState) => {
        Apis.getWeiboCKEditorConfig(params)
            .then(
                result => {
                    dispatch({
                        type: types.WEIBO_CKEDITOR_CONFIG,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getUserInfo = params => {
    return (dispatch, getState) => {
        Apis.getUserInfo(params)
            .then(
                result => {
                    dispatch({
                        type: types.USER_INFO,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getDefaultTemplate = params => {
    return (dispatch, getState) => {
        Apis.getDefaultTemplate(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_DEFAULT_TEMPLATE,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getWeiboGroup = params => {
    return (dispatch, getState) => {
        Apis.getWeiboGroup(params)
            .then(
                result => {
                    dispatch({
                        type: types.WEIBO_GROUP,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getIndexInfo = params => {
    return (dispatch, getState) => {
        Apis.getIndexInfo(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_INDEX_INFO,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getBasicInfo = params => {
    return (dispatch, getState) => {
        Apis.getBasicInfo(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_BASIC_INFO,
                        data: result
                    })
                }
            )
            .catch(
                error => {
                    message.error(error)
                })
    }
}

export const getReceiverList = params => {
    return (dispatch, getState) => {
        Apis.getReceiverList(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_RECEIVER_LIST,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getVisitorList = params => {
    return (dispatch, getState) => {
        Apis.getVisitorList(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_VISITOR_LIST,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getWeiboList = (others, params) => {
    return (dispatch, getState) => {
        let currentParams = {...params,_func: 'getWeiboList', _otherType: others.type};
        dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: true, currentParams: currentParams})
        if (others.type == 22) {
            dispatch({type: types.MYBLOG_CLEAR_COMMENT_COUNT,  data: {status: 1}});
        }
        let response = null
        switch (others.type * 1) {
            case 11:
                response = Apis.getWeiboMainPageList(params)
                break;
            case 12:
                response = Apis.getWeiboMainPageNewList(params)
                break;
            case 21:
                response = Apis.getWeiboList(params)
                break;
            default:
                response = Apis.getWeiboEveryTypeList(params)
                break;
        }
        response.then(
            result => {
                dispatch({
                    type: types.MYBLOG_WEIBO_LIST,
                    data: result,
                    others: others,
                    currentParams: currentParams
                })

            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const searchWeiboList = (params) => {
    return (dispatch, getState) => {
        let currentParams = {...params,_func: 'searchWeiboList'};
        dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: true, currentParams: currentParams})
    
        Apis.searchWeiboList(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_SEARCH_WEIBO_LIST,
                        data: result,
                        currentParams: currentParams
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const searchWeiboMainPageList = (params) => {
    return (dispatch, getState) => {
        let currentParams = {...params,_func: 'searchWeiboMainPageList'};
        dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: true, currentParams: currentParams})
    
        Apis.searchWeiboMainPageList(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_SEARCH_WEIBO_MAIN_PAGE_LIST,
                        data: result,
                        currentParams: currentParams
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getSystemWorkLog = (params, others) => {
    return (dispatch, getState) => {
        Apis.getSystemWorkLog(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_SYSTEM_WORK_LOG,
                        data: result,
                        others: others
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const editWeibo = (params, others, callback) => {
    return (dispatch, getState) => {
        if (params.content) {
            params.content = params.content.replace(GetRegex('wrap'), "")
            Apis.editWeibo(params)
                .then(
                    result => {
                        dispatch({
                            type: types.MYBLOG_EDIT_WEIBO,
                            data: result,
                            others: others
                        })
                        if (callback) callback(result)
                    }
                ).catch(
                error => {
                    message.error(error)
                }
            )
        }
        else {
            Modal.warning({title: '警告', content: "请填写内容"})
            Apis.getNull()
        }
    }
}

export const editComment = (params, others) => {
    return (dispatch, getState) => {
        if (params.content) {
            Apis.editComment(params)
                .then(
                    result => {
                        dispatch({
                            type: types.MYBLOG_EDIT_COMMENT,
                            data: result,
                            others: others
                        })
                    }
                ).catch(
                error => {
                    message.warning(error)
                }
            )
        }
        else {
            Modal.warning({title: '警告', content: "请填写内容"})
            Apis.getNull()
        }
    }
}

export const deleteComment = (params, others) => {
    return (dispatch, getState) => {
        Apis.deleteComment(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_DELETE_COMMENT,
                        data: result,
                        others: others
                    })
                }
            ).catch(
            error => {
                message.warning(error)
            }
        )
    }
}

export const editAgree = (params, others) => {
    return (dispatch, getState) => {
        Apis.editAgree(params)
            .then(
                result => {
                    dispatch({
                        type: types.MYBLOG_EDIT_AGREE,
                        data: result,
                        others: others
                    })
                }
            ).catch(
            error => {
                message.warning(error)
            }
        )
    }
}

export const getFanList = (params) => {
    return (dispatch, getState) => {
        dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: true})

        Apis.getFanList(params).then(
            result => {
                dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: false})
                dispatch({
                    type: types.MYBLOG_FAN_LIST,
                    data: result
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getAttentionList = (params, callback) => {
    return (dispatch, getState) => {
        dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: true})

        Apis.getAttentionList(params).then(
            result => {
                dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: false})
                dispatch({
                    type: types.MYBLOG_ATTENTION_LIST,
                    data: result
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const createGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.createGroup(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_CREATE_GROUP,
                    data: result
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const editGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.editGroup(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_EDIT_GROUP,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const deleteGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.deleteGroup(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_DELETE_GROUP,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const copyGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.copyGroup(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_COPY_GROUP,
                //     data: result
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const moveGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.moveGroup(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_MOVE_GROUP,
                //     data: result
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const resetGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.resetGroup(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_RESET_GROUP,
                //     data: result
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const removeGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.removeGroup(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_REMOVE_GROUP,
                //     data: result
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}


export const createAndMoveGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.createAndMoveGroup(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_CREATE_AND_MOVE_GROUP,
                //     data: result
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const createAndCopyGroup = (params, callback) => {
    return (dispatch, getState) => {
        Apis.createAndCopyGroup(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_CREATE_AND_COPY_GROUP,
                //     data: result
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const editFan = (params, callback) => {
    return (dispatch, getState) => {
        Apis.editFan(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_EDIT_FAN,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const editAttition = (params, callback) => {
    return (dispatch, getState) => {
        Apis.editAttition(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_EDIT_ATTENTION,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const editAttitions = (params, callback) => {
    return (dispatch, getState) => {
        Apis.editAttitions(params).then(
            result => {
                // dispatch({
                //     type: types.MYBLOG_EDIT_ATTENTIONS,
                //     data: result,
                //     others: params
                // })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getMessageList = (params) => {
    return (dispatch, getState) => {
        Apis.getMessageList(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_MESSAGE_LIST,
                    data: result
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const ignoreMessage = (params, callback) => {
    return (dispatch, getState) => {
        Apis.ignoreMessage(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_IGNORE_MESSAGE,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const ignoreMessages = (callback) => {
    return (dispatch, getState) => {
        Apis.ignoreMessages().then(
            result => {
                dispatch({
                    type: types.MYBLOG_IGNORE_MESSAGES,
                    data: result
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const approveMessage = (params, callback) => {
    return (dispatch, getState) => {
        Apis.approveMessage(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_APPROVE_MESSAGE,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getNote = (params) => {
    return (dispatch, getState) => {
        Apis.getNote(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_NOTE,
                    data: result
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const saveNote = (params, callback) => {
    return (dispatch, getState) => {
        if (params.content) {
            params.content = params.content.replace(GetRegex('wrap'), "")
            Apis.saveNote(params)
                .then(
                    result => {
                        // dispatch({
                        //     type: types.MYBLOG_SAVE_NOTE,
                        //     data: result
                        // })
                        if (callback) callback(result)
                    }
                ).catch(
                error => {
                    message.error(error)
                }
            )
        }
        else {
            Modal.warning({title: '警告', content: "请填写内容"})
            Apis.getNull()
        }
    }
}

export const setScore = (params) => {
    return (dispatch, getState) => {
        Apis.setScore(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_SET_SCORE,
                    data: result,
                    others: params
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const setNotice = (params) => {
    return (dispatch, getState) => {
        Apis.setNotice(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_SET_NOTICE,
                    data: result,
                    others: params
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const setReaded = (params, callback) => {
    return (dispatch, getState) => {
        Apis.setReaded(params).then(
            result => {
                dispatch({
                    type: types.MYBLOG_SET_READED,
                    data: result,
                    others: params
                })
                if (callback) callback(result)
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const init = () => {
	return (dispatch, state) => {
        let others = {type: 21, status: true}, params = {endDate: DateFormat(new Date(), "yyyy-MM-dd")};
        let currentParams = {...params,_func: 'getWeiboList', _otherType: others.type};
        dispatch({type: types.MYBLOG_LOADING, data: {status: 1}, loading: true, currentParams: currentParams});
        
	    let initArray = [
	        {promise: Apis.getUserInfo(), type: types.USER_INFO},
            {promise: Apis.getWeiboGroup(), type: types.WEIBO_GROUP},
            {promise: Apis.getMessageList(), type: types.MYBLOG_MESSAGE_LIST},
            {promise: Apis.getDefaultTemplate(), type: types.MYBLOG_DEFAULT_TEMPLATE},
            {promise: Apis.getNote(), type: types.MYBLOG_NOTE},
            {promise: Apis.getWeiboCKEditorConfig(), type: types.WEIBO_CKEDITOR_CONFIG},
            {promise: Apis.getWeiboList(params), type:types.MYBLOG_WEIBO_LIST, typeOthers: {others: others, currentParams: currentParams}}
        ];
        let allPro = Promise.all(initArray.map((obj) => obj.promise));
        allPro.then((results) => {
            let actions = initArray.map((obj, i) => {
                return {
                    type: obj.type,
                    data: results[i],
                    ...obj.typeOthers
                }
            });
            dispatch(batchActions(actions, "MYBLOG_INIT"));
        });
	}
}