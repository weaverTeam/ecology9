import * as types from '../constants/ActionTypes';
import Apis from '../apis/myAttention';
import BlogApis from '../apis/myBlog';
import {message} from 'antd';

export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.MYATTENTION_LOADING,
			loading
		});
	}
};

export const getMyAttentions = (params) => {
	return (dispatch, getState) => {
	    dispatch({
            type: types.MYATTENTION_LIST_LOADING
        });
		Apis.getMyBlogAttentions(params)
			.then(result => {
				dispatch({
					type: types.MYATTENTION_LIST,
					data: result,
                    params: params
				})
			})
			.catch(error => message.error(error));
	}
};

export const getMyAttentionShare = (params) => {
    return (dispatch, getState) => {
        dispatch({
            type: types.MYATTENTION_LIST_LOADING
        });
        Apis.getMyAttentionShare(params)
            .then(result => {
                dispatch({
                    type: types.MYATTENTION_LIST,
                    data: result,
                    params: params
                })
            })
            .catch(error => message.error(error));
    }
};
export const editAttention = (params, callback) => {
	return (dispatch, getState) => {
		Apis.editAttention(params).then(
			result => {
			    callback && callback(result.attnInfo);
            }
		).catch(
			error => {
				message.error(error)
			}
		)
	}
};

export const getAttentionCountInfos = (params) => {
	return (dispatch, getState) => {
	    dispatch({type: types.MYATTENTION_COUNTLIST_LOADING});
		Apis.getAttentionCountInfos(params).then(
			result => {
				dispatch({
					type: types.MYATTENTION_COUNTLIST,
					data:result
				})
			}
		).catch(
			error => {
				message.error(error)
			}
		)
	};
};

export const getAttentionWorkDateList = (params, index) => {
	return (dispatch, getState) => {
		Apis.getAttentionWorkDateList(params).then(
			result => {
				dispatch({
					type: types.MYATTENTIONWORK_BY_DATE,
					data: result,
					index: index
				})
			}
		).catch(
			error => {
				message.error(error)
			}
		)
	};
};

export const chooseUser = (userId) => {
	return {
		type: types.MYATTENTION_CHOOSEUSER,
		userId: userId
	};
};
export const getCurrentUser = () => {
	return (dispatch, getState) => {
        BlogApis.getUserInfo().then(
            result => {
                dispatch({
                    type: types.CURRENT_USER,
                    data: result
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    };
};
export const searchWeiboList = (params) => {
    return (dispatch, getState) => {
        dispatch({type: types.MYATTENTION_COUNTLIST_LOADING});
        BlogApis.searchWeiboMainPageList(params).then(
            result => {
                dispatch({
                    type: types.MYATTENTION_SEARCHWEIBO,
                    data: result
                })
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    };
};

export const sendRemindUnsubmitAll = (params) => {
    return (dispatch, getState) => {
        Apis.sendRemindUnsubmitAll(params).then(
            result => {
                message.success("已全部提醒！");
            }
        ).catch(
            error => {
                message.error(error)
            }
        )
    };
};

export const updateUserAttentionStatus = attnInfo => {
    return {
        type: types.MYATTENTION_UPDATE_USER_ATTENTIONSTATUS,
        attnInfo: attnInfo
    };
};

export const resetRight = () => {
	return {type: types.MYATTENTION_RESETRIGHT};
};

export const clearLeft = () => {
    return {type: types.MYATTENTION_CLEARLEFT};
};
export const clearRightList = () => {
	return {type: types.MYATTENTION_CLEARRIGHTLIST};
};