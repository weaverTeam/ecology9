import * as types from '../constants/ActionTypes'
import * as Apis from '../apis/report'

import {message} from 'antd'

const startLoad = (dispatch, loading) => {
    dispatch({
        type: types.REPORT_LOADING,
        data: {status: 1},
        loading: loading
    })
}

export const doLoading = loading => {
    return (dispatch, getState) => {
        dispatch({
            type: types.REPORT_LOADING,
            data: {status: 1},
            loading
        })
    }
}

export const getUserInfo = params => {
    return (dispatch, getState) => {
        Apis.getUserInfo(params)
            .then(
                result => {
                    dispatch({
                        type: types.USER_INFO,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getReportList = params => {
    return (dispatch, getState) => {
        Apis.getReportList(params)
            .then(
                result => {
                    dispatch({
                        type: types.REPORT_LIST,
                        data: result
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const addReport = (params, callback) => {
    return (dispatch, getState) => {
        startLoad(dispatch, true)
        Apis.addReport(params)
            .then(
                result => {
                    startLoad(dispatch, false)
                    dispatch({
                        type: types.REPORT_ADD,
                        data: result,
                        others: params
                    })
                    if (callback) callback(result)
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const deleteReport = (params, callback) => {
    return (dispatch, getState) => {
        startLoad(dispatch, true)
        Apis.deleteReport(params)
            .then(
                result => {
                    startLoad(dispatch, false)
                    dispatch({
                        type: types.REPORT_DELETE,
                        data: result,
                        others: params
                    })
                    if (callback) callback(result)
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

const startLoadList = (dispatch, loading, currentParams) => {
    dispatch({
        type: types.REPORT_LOADING_LIST,
        data: {status: 1},
        loading: loading,
        currentParams: currentParams
    })
}
export const getReportTable = params => {
    return (dispatch, getState) => {
        let currentParams = {...params, _func:'getReportTable'}
        startLoadList(dispatch, true, currentParams)
        Apis.getReportTable(params)
            .then(
                result => {
                    dispatch({
                        type: types.REPORT_TABLE,
                        data: result,
                        currentParams: currentParams
                    })
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
}

export const getAttentionReport = (params) => {
	return (dispatch, state) => {
        let currentParams = {...params, _func:'getAttentionReport'}
        startLoadList(dispatch, true, currentParams)
        Apis.getAttentionReport(params).then((result) => {
            dispatch({
                type: types.REPORT_TABLE,
                data: result,
                currentParams: currentParams
            })
        });
    }
}

export const chooseOwner = owner=> {
    return  (dispatch, state) => {
        dispatch({
            type: types.REPORT_CHOOSE_OWNER,
            owner: owner
        });
    };
}

export const getTempReportTableInfo = (params) => {
    return (dispatch, state) => {
        let currentParams = {...params, _func:'getTempReportTableInfo'}
        startLoadList(dispatch, true, currentParams)
        Apis.getTempReportTableInfo(params).then((result) => {
            dispatch({
                type: types.REPORT_TABLE,
                data: result,
                currentParams: currentParams
            })
        });
    }
}

export const deleteTempCondition = (params, callback) => {
	return (dispatch) => {
		Apis.deleteTempCondition(params).then((result) => {
		    callback && callback();
        }).catch((e) => {
        	message.error(e);
        })
	}
};

export const getBlogReportConfig = (params) => {
	return (dispatch) => {
	    Apis.getBlogReportConfig(params).then(result => {
	        dispatch({
	            type: types.BLOG_REPORT_CONFIG,
	            data:result
            });
        })
    };
};

export const getTempReportExpandRows = params => {
    return (dispatch) => {
        dispatch({
            type: types.BLOG_REPORT_SUBLIST_LOADING,
            parent: params
        });
        Apis.getTempReportExpandRows(params).then(result => {
            dispatch({
                type: types.BLOG_REPORT_SUBLIST,
                data: result,
                parent: params
            });
        })
    };
};
export const toggleConditionExpand = conditionId => {
    return {
        type: types.BLOG_REPORT_TOGGLE_CONDITION_EXPAND,
        conditionId: conditionId
    }
};

export const editCustomizeReport = (params, callback) => {
    return (dispatch, getState) => {
        startLoad(dispatch, true)
        Apis.editCustomizeReport(params)
            .then(
                result => {
                    startLoad(dispatch, false)
                    dispatch({
                        type: types.REPORT_EDIT,
                        data: result,
                        others: params
                    })
                    if (callback) callback(result)
                }
            ).catch(
            error => {
                message.error(error)
            }
        )
    }
};