import * as types from '../constants/ActionTypes'
import * as API_LIST from '../apis/setting'
import { WeaTable } from 'comsRedux'
import objectAssign from 'object-assign'

const WeaTableAction = WeaTable.action;

export const doLoading = loading => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_LOADING,
			loading
		});
	}
}

//切换tab
export const changeTab = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_SET_TABKEY,data:value
		});
	}
}

//切换switch状态
export const changeChecked = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_CHANGE_CHECKED,data:value
		});
	}
}

//微博基本设置-获取

export const getUserBlogBaseSetting = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getUserBlogBaseSettingP(value).then(data =>{
			dispatch({type:types.SETTING_GET_SETTING, data: data.userBlogBaseSetting.isReceive});
		});
	}
}

//微博基本设置-保存
export const saveBlogUserSetting = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({ type:types.SETTING_LOADING, loading: true });
		API_LIST.saveBlogUserSettingP(value).then(data =>{
			dispatch({ type:types.SETTING_LOADING, loading: false });
		});
	}
}

//高级搜索是否显示
export const setShowSearchAd = bool => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_SHOW_SEARCHAD,data:bool
		});
	}
}

//获取上级和指定分享信息
export const getFixedSharingInfos = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getFixedSharingInfosP(value).then(data =>{
			dispatch({type:types.SETTING_GET_FIXED_SHARINGINFOS, data: data});
		});
	}
}


//获取我的分享设置表格
export const getBlogShareTable = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({ type:types.SETTING_LOADING, loading: true });
		API_LIST.getBlogShareTableP(value).then(data =>{
			dispatch(WeaTableAction.getDatas(data.sessionkey, 1));
			dispatch({type:types.SETTING_SHARE_SAVE_SESSIONKEY, data: data.sessionkey});
			dispatch({ type: types.SETTING_LOADING, loading: false });
		});
	}
}


//新增或修改个人分享信息
export const saveBlogShareList = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.saveBlogShareListP(value).then(data =>{
			dispatch(getBlogShareTable({}));
		});
	}
}

//个人模版设置--高级查询条件
export const getUserBlogTemplateShareCondition = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getUserBlogTemplateShareConditionP(value).then(data =>{
			dispatch({type:types.SETTING_GET_USER_CONDITION, data: data.conditioninfo[0].items});
		});
	}
}

//删除个人微博共享信息
export const deleteBlogUserShare = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.deleteBlogUserShareP(value).then(data =>{
			dispatch(getBlogShareTable({}));
		});
	}
}

//分享设置--根据id获取共享信息
export const getBlogUserShareById = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getBlogUserShareByIdP({shareId:value.shareId}).then(data =>{
			dispatch({type:types.SETTING_GET_USER_SHAREBYID, data: data});
            let valuea = {};
            valuea = objectAssign({},{
            	relatedshareid:{name:'relatedshareid',value:data.info.content?data.info.content:0},
            	seclevel:{name:'seclevel',value:data.info.seclevel},
            	seclevelMax:{name:'seclevelMax',value:data.info.seclevelMax},
            	canViewMinTime:{name:'canViewMinTime',value:data.info.canViewMinTime}})
			dispatch({type:types.SETTING_SAVE_ORDER_FIELDS, data: valuea })
		}).then(()=>value.this.setState({showModal:true,editid:true,shareId:value.shareId}));
	}
}


//表单内容存储到store
export const saveOrderFields = (value = {}) => {
	return(dispatch, getState) => {
		dispatch({type:types.SETTING_SAVE_ORDER_FIELDS, data: value })
	}
}


//获取模版设置--列表
export const getUserBlogTemplateTable = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({ type:types.SETTING_LOADING, loading: true });
		API_LIST.getUserBlogTemplateTableP(value).then(data =>{
			dispatch(WeaTableAction.getDatas(data.sessionkey, 1));
			dispatch({type:types.SETTING_MOUDLE_SAVE_SESSIONKEY, data: data.sessionkey});
			dispatch({type:types.SETTING_LOADING, loading: false });
		});
	}
}

//模版设置--根据id删除模版
export const deleteBlogTemplateByIds = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.deleteBlogTemplateByIdsP(value).then(data =>{
			dispatch(getUserBlogTemplateTable({}));
		});
	}
}

//保存模板
export const setUserBlogTemplateToDefault = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.setUserBlogTemplateToDefaultP(value).then(data =>{
			dispatch(getUserBlogTemplateTable({}));
		});
	}
}

//保存默认模板id
export const saveUserBlogMoudleDefaultId = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_SAVE_USER_BLOG_MOUDLE_DEFAULTID,data:value
		});
	}
}

//新建模板
export const saveOrUpdateBlogTemplate = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.saveOrUpdateBlogTemplateP(value).then(data =>{
			dispatch(getUserBlogTemplateTable({}));
			dispatch(saveckvalue());
		});
	}
}

//模版设置--根据blog_template主键id，获得模版对象信息
export const getBlogTempleteInfoById = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getBlogTempleteInfoByIdP(value).then(data =>{
			dispatch({type:types.SETTING_GET_BLOG_TEMPLETEINFO_BYID, data: data});
			dispatch({type:types.SETTING_SAVE_CKVALUE, data: data.BlogTemplateBean.tempContent});
		})
		// .then(()=>{
		// 	if (value.this.state.showModal&&value.this.refs[value.this.CKE_ID]) {
  //               let {CreateEditor} = value.this.refs[value.this.CKE_ID];
  //               const {moudleinfo} = value.this.props;
  //               CreateEditor(moudleinfo.BlogTemplateBean&&moudleinfo.BlogTemplateBean.tempContent||'');
  //           }else {
  //           	value.this.refs[value.this.CKE_ID]&&value.this.refs[value.this.CKE_ID].RemoveEditor()
  //           }
		// })
	}
}

//保存富文本内容
export const saveckvalue = value => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_SAVE_CKVALUE,data:value
		});
	}
}

// export const resetmoudleinfo = value => {
// 	return (dispatch, getState) => {
// 		dispatch({
// 			type: types.SETTING_RESET_MOUDLEINFO,data:value
// 		});
// 	}
// }

//模版设置--预览
export const setPreview = (value={}) => {
	return (dispatch, getState) => {
		API_LIST.setPreviewP(value).then(data =>{
			dispatch({type:types.SETTING_SET_PREVIEW, data: data});
		}).then(()=>dispatch({type:types.SETTING_SET_PREVIEWSHOW, data:true}));
	}
}
//设置预览modal是否展示
export const setPreviewShow = (value={}) => {
	return (dispatch, getState) => {
		dispatch({
			type: types.SETTING_SET_PREVIEWSHOW,data:value
		});
	}
}

//模版设置--个人模版设置--模版分享设置  表格
export const getUserBlogTemplateShareTable = (value = {}) => {
	return (dispatch, getState) => {
		API_LIST.getUserBlogTemplateShareTableP(value).then(data =>{
			dispatch({type:types.SETTING_SPINNING, spinning: true });
			dispatch(WeaTableAction.getDatas(data.sessionkey, 1));
			dispatch({type:types.SETTING_MOUDLE_SHARE_SESSIONKEY, data: data.sessionkey});
			dispatch({type:types.SETTING_SPINNING, spinning: false });
		});
	}
}

//模版设置--分享设置--保存分享条件
export const addUserTempShare = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({ type:types.SETTING_SPINNING, spinning: true });
		API_LIST.addUserTempShareP(value).then(data =>{
			dispatch({type:types.SETTING_SPINNING, spinning: false });
			dispatch(getUserBlogTemplateShareTable({tempid:value.tempid}));
		});
	}
}

//删除个人微博共享信息
export const deleteUserTempShare = (value = {}) => {
	return (dispatch, getState) => {
		dispatch({type:types.SETTING_SPINNING, spinning: true });
		API_LIST.deleteUserTempShareP(value).then(data =>{
			dispatch(getUserBlogTemplateShareTable({tempid:value.tempida}));
			dispatch({type:types.SETTING_SPINNING, spinning: false });
		});
	}
}