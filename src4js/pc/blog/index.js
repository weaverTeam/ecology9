import Route from "react-router/lib/Route"
import {IndexRedirect} from 'react-router';
import Home from "./components/Home.js"
import MyBlog from "./components/MyBlog/"
import MyAttention from "./components/MyAttention/"
import AttentionWeiboList from "./components/MyAttention/AttentionWeiboList"
import RightWeibo from "./components/MyAttention/RightWeibo"
import Report from "./components/Report/"
import Setting from "./components/Setting.js"
//anruo : import test
import Test from "./components/Test/"

import "./css/icon.css"

import reducer from "./reducers/"
import * as MyBlogAction from "./actions/myBlog"
import * as MyAttentionAction from "./actions/myAttention"
import * as ReportAction from "./actions/report"
import * as SettingAction from "./actions/Setting"
import WeaBlogDict from './components/Component/wea-blog-dict';

const blogRoute = (
    <Route path="blog" component={ Home }>
        <Route name="myBlog" path="myBlog" component={ MyBlog }/>
        <Route name="myAttention" path="myAttention" component={ MyAttention }>
            <IndexRedirect to="list"/>
            <Route path="list" component={AttentionWeiboList}/>
            <Route path="user/:userId" component={RightWeibo}/>
        </Route>
        <Route name="Report" path="Report" component={ Report }/>
        <Route name="Setting" path="Setting" component={ Setting }/>
        //anruo : test Route
        <Route path="test" component={Test}/>
    </Route>
)

WeaBlogDict.addWindowFunc();

module.exports = {
    Route: blogRoute,
    reducer,
    action: {
        MyBlogAction,
        MyAttentionAction,
        ReportAction,
        SettingAction,
    }
}