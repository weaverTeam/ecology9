export const USER_INFO = 'user_info'
export const WEIBO_GROUP = 'weibo_group'
export const WEIBO_CKEDITOR_CONFIG = 'weibo_ckeditor_config'

/**
 * 我的微博
 */
export const MYBLOG_LOADING = 'myblog_loading'
export const MYBLOG_DEFAULT_TEMPLATE = 'myblog_default_template'
export const MYBLOG_CLEAR_COMMENT_COUNT = 'MYBLOG_CLEAR_COMMENT_COUNT';

export const MYBLOG_INDEX_INFO = 'myblog_index_info'
export const MYBLOG_BASIC_INFO = 'myblog_basic_info'
export const MYBLOG_RECEIVER_LIST = 'myblog_receiver_list'
export const MYBLOG_VISITOR_LIST = 'myblog_visitor_list'

export const MYBLOG_SYSTEM_WORK_LOG = 'myblog_system_work_log'

export const MYBLOG_SEARCH_WEIBO_LIST = 'myblog_search_weibo_list'
export const MYBLOG_WEIBO_LIST = 'myblog_weibo_list'
export const MYBLOG_EDIT_WEIBO = 'myblog_edit_weibo'
export const MYBLOG_SEARCH_WEIBO_MAIN_PAGE_LIST = 'myblog_search_weibo_main_page_list'

export const MYBLOG_EDIT_COMMENT = 'myblog_edit_comment'
export const MYBLOG_DELETE_COMMENT = 'myblog_delete_comment'

export const MYBLOG_EDIT_AGREE = 'myblog_edit_agree'

export const MYBLOG_FAN_LIST = 'myblog_fan_list'
export const MYBLOG_ATTENTION_LIST = 'myblog_attention_list'

export const MYBLOG_CREATE_GROUP = 'myblog_create_group'
export const MYBLOG_EDIT_GROUP = 'myblog_edit_group'
export const MYBLOG_DELETE_GROUP = 'myblog_delete_group'
export const MYBLOG_COPY_GROUP = 'myblog_copy_group'
export const MYBLOG_MOVE_GROUP = 'myblog_move_group'
export const MYBLOG_RESET_GROUP = 'myblog_reset_group'
export const MYBLOG_REMOVE_GROUP = 'myblog_remove_group'
export const MYBLOG_CREATE_AND_MOVE_GROUP = 'myblog_create_and_move_group'
export const MYBLOG_CREATE_AND_COPY_GROUP = 'myblog_create_and_copy_group'

export const MYBLOG_EDIT_FAN = 'myblog_edit_fan'
export const MYBLOG_EDIT_ATTENTION = 'myblog_edit_attention'
export const MYBLOG_EDIT_ATTENTIONS = 'myblog_edit_attentions'

export const MYBLOG_MESSAGE_LIST = 'myblog_message_list'
export const MYBLOG_IGNORE_MESSAGE = 'myblog_ignore_message'
export const MYBLOG_IGNORE_MESSAGES = 'myblog_ignore_messages'
export const MYBLOG_APPROVE_MESSAGE = 'myblog_approve_message'

export const MYBLOG_NOTE = 'myblog_note'
export const MYBLOG_SAVE_NOTE = 'myblog_save_note'

export const MYBLOG_SET_SCORE = 'myblog_set_score'
export const MYBLOG_SET_NOTICE = 'myblog_set_notice'
export const MYBLOG_SET_READED = 'myblog_set_readed'

export const MYBLOG_SET_KEYWORDS = 'myblog_set_keywords'

/**
 * 我的关注
 */
export const MYATTENTION_LOADING = 'myattention_loading'
export const MYATTENTION_LIST = 'MYATTENTION_LIST';
export const MYATTENTION_LIST_LOADING = 'MYATTENTION_LIST_LOADING';
export const MYATTENTION_COUNTLIST = 'MYATTENTION_COUNTLIST';
export const MYATTENTIONWORK_BY_DATE = 'MYATTENTIONWORK_BY_DATE';
export const MYATTENTION_CHOOSEUSER = 'MYATTENTION_CHOOSEUSER';
export const CURRENT_USER = 'CURRENT_USER';
export const MYATTENTION_SEARCHWEIBO = 'MYATTENTION_SEARCHWEIBO';
export const MYATTENTION_COUNTLIST_LOADING = 'MYATTENTION_COUNTLIST_LOADING';
export const MYATTENTION_UPDATE_USER_ATTENTIONSTATUS = "MYATTENTION_UPDATE_USER_ATTENTIONSTATUS";
export const MYATTENTION_RESETRIGHT = 'MYATTENTION_RESETRIGHT';
export const MYATTENTION_CLEARLEFT = 'MYATTENTION_CLEARLEFT';
export const MYATTENTION_CLEARRIGHTLIST = 'MYATTENTION_CLEARRIGHTLIST';

/**
 * 微博报表
 */
export const REPORT_LOADING = 'report_loading'
export const REPORT_LOADING_LIST = 'REPORT_LOADING_LIST';
export const REPORT_LIST = 'report_list'
export const REPORT_ADD = 'report_add'
export const REPORT_EDIT = 'REPORT_EDIT';
export const REPORT_DELETE = 'report_delete'
export const REPORT_TABLE = 'report_table'
export const REPORT_CHOOSE_OWNER = 'REPORT_CHOOSE_OWNER';
export const BLOG_REPORT_CONFIG = 'BLOG_REPORT_CONFIG';
export const BLOG_REPORT_SUBLIST = 'BLOG_REPORT_SUBLIST';
export const BLOG_REPORT_SUBLIST_LOADING = 'BLOG_REPORT_SUBLIST_LOADING';
export const BLOG_REPORT_TOGGLE_CONDITION_EXPAND = 'BLOG_REPORT_TOGGLE_CONDITION_EXPAND';

/**
 * 微博设置
 */
export const SETTING_LOADING = 'setting_loading'
export const SETTING_SPINNING = 'setting_spinning'
export const SETTING_SET_TABKEY = 'setting_set_tabkay' //tab切换
export const SETTING_CHANGE_CHECKED = 'setting_change_checked' //tab切换
export const SETTING_GET_SETTING = 'setting_get_setting' //微博基本设置-获取
export const SETTING_SHOW_SEARCHAD = 'setting_show_searchad' //微博基本设置-获取
export const SETTING_GET_FIXED_SHARINGINFOS = 'setting_get_fixed_sharinginfos' //获取上级和指定分享信息
export const SETTING_SHARE_SAVE_SESSIONKEY = 'setting_share_save_sessionkey' //获取我的分享设置表格
export const SETTING_MOUDLE_SAVE_SESSIONKEY = 'setting_moudle_save_sessionkey' //获取我的分享设置表格
export const SETTING_GET_USER_CONDITION = 'setting_get_user_condition' //个人模版设置--高级查询条件
export const SETTING_GET_USER_SHAREBYID = 'setting_get_user_sharebyid' //分享设置--根据id获取共享信息
export const SETTING_SAVE_ORDER_FIELDS = 'setting_save_ordr_fields' //高级搜索值绑定到redux
export const SETTING_SAVE_USER_BLOG_MOUDLE_DEFAULTID = 'setting_save_user_blog_moudle_defaultid' //保存默认模板的ID
export const SETTING_SET_MOUDLE_MODAL = 'setting_set_moudle_modal' //模板设置modal受控
export const SETTING_GET_BLOG_TEMPLETEINFO_BYID = 'setting_get_blog_templeteinfo_byid' //模版设置--根据blog_template主键id，获得模版对象信息
export const SETTING_RESET_MOUDLEINFO = 'setting_reset_moudleinfo' //模板设置modal受控
export const SETTING_SAVE_CKVALUE = 'setting_save_ckvalue' //模板设置ck富文本内容
export const SETTING_SET_PREVIEW = 'setting_set_preview' //模板设置预览
export const SETTING_MOUDLE_SHARE_SESSIONKEY = 'setting_moudle_share_sessionkey' //模版设置--个人模版设置--模版分享设置  表格
export const SETTING_SET_PREVIEWSHOW = 'setting_set_previewshow' //模板设置预览是否展示
