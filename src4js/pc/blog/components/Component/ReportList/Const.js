export const ICONS = {
    blog_submit_ok:{icon:"icon-blog-AlreadyConcern", text:"已提交", color:"#2bb028"},
    blog_submit_no:{icon:"icon-blog-Uncommitted-o", text:"未提交", color:"#505050"},
    mood_happy:{icon:"icon-blog-Emoji", text:"高兴", color:"#2fbbff"},
    mood_unhappy:{icon:"icon-blog-Unhappy", text:"不高兴", color:"#ff2a2a"},
    sign_absent:{icon:"icon-blog-Absenteeism", text:"旷工", color:"#7326ff"},
    sing_late:{icon:"icon-blog-Late", text:"迟到", color:"#ff7800"},
    sign_ok:{icon:"icon-blog-normal", text:"正常", color:"#2cc7a4"}
};
export const REPORT_TYPE = {
    attention: 'attentionReport',
    my: 'myReport'
};