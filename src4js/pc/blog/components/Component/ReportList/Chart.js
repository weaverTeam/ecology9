import _get from 'lodash/get';
class Chart extends React.Component {
	constructor(props) {
		super(props);
        this.chart = undefined;
	}
	render() {
	    let minWidth = _get(this, 'props.categories.length', 0) * 100 + 50;
	    let style = {height: '100%', minWidth: minWidth};
		return (
			<div ref="chartDiv" style={style}></div>
		);
	}
	clear() {
	    this.chart && this.chart.clear();
    }
    paint() {
        let {chartName, categories, series} = this.props;
        this.chart || (this.chart = window.echarts.init(ReactDOM.findDOMNode(this.refs.chartDiv)));
    
        // 指定图表的配置项和数据
        let option = {
            noDataLoadingOption: {
                text: '暂无数据',
                effect: 'bubble',
                effectOption: {
                    effect: {
                        n:0
                    }
                }
            },
            title: {text: chartName},
            xAxis: {data: categories,
                axisLine:{lineStyle:{color:'#c5c5c5'}},
                splitLine: {
                    show:true,
                    lineStyle: {color:['#c5c5c5']}
                },
                axisTick:{lineStyle:{color: '#c5c5c5'}},
                axisLabel: {
                    interval: 0,
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            yAxis: {
                max: 5,
                axisLine:{lineStyle:{color:'#c5c5c5', width: 1, shadowBlur: 0, opacity: 0}},
                axisLabel: {
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            grid: {borderColor: '#c5c5c5'},
            color: ['#4bb1fd'],
            series: [{type: 'bar', data: series}],
            tooltip: {
                formatter: '<strong>' + chartName + '</strong><br/>{b0}: {c0}'
            }
        };
        // 使用刚指定的配置项和数据显示图表。
        this.chart.setOption(option);
    }
}

Chart.propTypes = {
    chartName: React.PropTypes.string,//表格标题
    categories: React.PropTypes.array,//x轴
    series: React.PropTypes.array,//柱状图数据
}

export default Chart;