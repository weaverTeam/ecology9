import {Form, Select,message, Button} from 'antd';
import * as Apis from '../../../apis/report';
import {WeaDialog, WeaSelect, WeaBrowser} from 'ecCom';
const FormItem = Form.Item;
class ConditionDialog extends React.Component {
    constructor(props) {
        super(props);
        this.show = this.show.bind(this);
        this.close = this.close.bind(this);
        this.save = this.save.bind(this);
        this.changeType = this.changeType.bind(this);
        this.changeBrowser = this.changeBrowser.bind(this);
        this.humanBrowserProps = {
            title:"多人力资源",
            onChange: this.changeBrowser,
            layout: document.body,
            "hasAdvanceSerach": true,
            "idSeparator": ",",
            "isAutoComplete": 1,
            "isDetail": 0,
            "isMultCheckbox": false,
            "isSingle": false,
            "linkUrl": "/hrm/resource/HrmResource.jsp?id=",
            "quickSearchName": "",
            "type": "17",
            "viewAttr": 2
        };
        this.companyBrowserProps = {
            onChange: this.changeBrowser,
            "hasAdvanceSerach": true,
            "idSeparator": ",",
            "isAutoComplete": 1,
            "isDetail": 0,
            "isMultCheckbox": false,
            "isSingle": false,
            "title": "多分部",
            "linkUrl": "/hrm/company/HrmSubCompanyDsp.jsp?id=",
            "quickSearchName": "",
            "tabs": [
                {
                    "dataParams": {
                        "list": "1"
                    },
                    "isSearch": true,
                    "key": "1",
                    "name": "按列表",
                    "selected": false
                },
                {
                    "isSearch": false,
                    "key": "2",
                    "name": "按组织架构",
                    "selected": false
                }
            ],
            "type": "194",
            "viewAttr": 2
        };
        this.departBrowserProps = {
            "title": "多部门",
            onChange: this.changeBrowser,
            "hasAdvanceSerach": true,
            "idSeparator": ",",
            "isAutoComplete": 1,
            "isDetail": 0,
            "isMultCheckbox": false,
            "isSingle": false,
            "linkUrl": "/hrm/company/HrmDepartmentDsp.jsp?id=",
            "quickSearchName": "",
            "tabs": [
                {
                    "dataParams": {
                        "list": "1"
                    },
                    "isSearch": true,
                    "key": "1",
                    "name": "按列表",
                    "selected": false
                },
                {
                    "isSearch": false,
                    "key": "2",
                    "name": "按组织架构",
                    "selected": false
                }
            ],
            type: 57,
            "viewAttr": 2
        };
        this.state = {
            visible: false,
            obj: {
                type: '1',
                id: props.reportId
            },//保存用
            browserDatas: []
        };
    }
    
    componentWillReceiveProps(nextProps) {
        if (nextProps.reportId != this.props.reportId) {
            let obj = this.state.obj;
            obj.id = nextProps.reportId;
            this.setState({obj: obj});
        }
    }
    show() {
        this.setState({
            visible: true
        });
    }
    save() {
        let {obj} = this.state;
        if (!obj.content) {
            message.error("内容不能为空");
            return;
        }
        Apis.addTempCondition(obj).then(result=>{
            if (result.status == 1) {
                if (result.api_status == 1) {
                    this.close();
                    this.props.saveCallback && this.props.saveCallback();
                } else if (result.api_status == 2) {
                    let typeName = obj.type == 1 ? '人' : obj.type == 3 ? '部门' : '分部';
                    message.error(`该${typeName}已存在`);
                } else if (result.api_status == 3) {
                    message.error(`该${obj.type == 3 ? '部门' : '分部'}无可看人员`);
                } else {
                    message.error("保存失败");
                }
            } else {
                message.error("保存出错");
            }
        }).catch(e => {
            message.error(e);
        });
    }
    
    close() {
        let obj = this.state.obj;
        obj.type = '1';
        obj.content = '';
        this.setState({
            obj: obj,
            browserDatas: []
        }, () => this.setState({visible: false}));
    }
    changeType(value) {
        let obj = this.state.obj || {};
        obj.type = value, obj.content = "";
        this.setState({
            obj: obj,
            browserDatas: []
        });
    }
    
    changeBrowser(value, o, replaceDatas) {
        let obj = this.state.obj;
        obj.content = value;
        this.setState({
            obj: obj,
            browserDatas:replaceDatas
        });
    }
    
    render() {
        let {obj, browserDatas} = this.state;
        return (
            <WeaDialog visible={this.state.visible} title="报表条件" style={{height: 160, width: 520}}
                       icon="icon-coms-blog" iconBgcolor="#6d3cf7"
                       buttons={[<Button type="primary" onClick={this.save}>保存</Button>,<Button type="ghost" onClick={this.close}>取消</Button>]}>
                <div className="wea-condition-dialog" ref="conditionDialogDiv">
                    <Form horizontal>
                        <FormItem label="类型：" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            <WeaSelect fieldName="type" onChange={this.changeType} value={obj.type}
                                       options={[
                                           {key: '1', selected: true, showname: '人员'},
                                           {key: '3', selected: false, showname: '部门'},
                                           {key: '2', selected: false, showname: '分部'}
                                       ]} />
                        </FormItem>
                        <FormItem label="条件内容：" labelCol={{ span: 6 }} wrapperCol={{ span: 14 }}>
                            {obj.type == '1' ?
                                <WeaBrowser {...this.humanBrowserProps} key="user" ref="user" replaceDatas={browserDatas}/> :
                                obj.type == '3' ?
                                    <WeaBrowser {...this.departBrowserProps} key="department" ref="department" replaceDatas={browserDatas}/> :
                                    <WeaBrowser {...this.companyBrowserProps} key="subcompany" ref="subcompany" replaceDatas={browserDatas}/>
                            }
                        </FormItem>
                        <span className="tip">说明：无权查看的对象无法添加至报表显示</span>
                    </Form>
                </div>
            </WeaDialog>
        );
    }
}

export default ConditionDialog;