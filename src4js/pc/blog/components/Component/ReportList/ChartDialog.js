import {Tabs, Button, Spin} from 'antd';
import {WeaDialog,WeaEchart} from 'ecCom';
import * as reportApis from '../../../apis/report';
const TabPane = Tabs.TabPane
class ChartDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            loading: true,
            title: '指数统计',
            result: {}
        };
        this.back = this.back.bind(this);
        this.changeYear = this.changeYear.bind(this);
        this.queryChart = this.queryChart.bind(this);
    }
    back() {
        this.setState({
            visible: false
        });
    }
    show() {
        this.setState({
            visible: true,
            loading: true
        }, this.queryChart);
    }
    changeYear(year) {
        this.queryChart(year);
    }
    
    queryChart(year = this.props.year) {
        this.setState({
            loading: true,
            currentYear: year
        });
        let {chartType, type, value} = this.props;
        reportApis.getBlogChartInfos({
            year: year,
            chartType: chartType,
            type: type,
            value: value
        }).then((result) => {
            this.setState({
                loading: false,
                title: result.title,
                enableYear: result.enableYear,
                result: result
            });
        })
    }
	render() {
	    let {loading, title, enableYear, currentYear, result={}} = this.state;
        let lastYear = new Date().getFullYear();
        let yearArr = [];
        for (let y = enableYear; y<=lastYear;y++) {
            yearArr.push(y);
        }
		return (
			<WeaDialog visible={this.state.visible} title={title} style={{width: 800, height: 500}}
                       icon="icon-coms-blog" iconBgcolor="#6d3cf7"
                       buttons={[<Button type="primary" onClick={this.back}>返回</Button>]}>
                <div className="chartBody">
                    <Tabs activeKey={`${currentYear}`} type="card" onChange={this.changeYear}>
                        {yearArr.map((year) => <TabPane tab={`${year}年`} key={`${year}`}/>)}
                    </Tabs>
                    <div className="chartContainer">
                        
                        {loading ? <div className="align-center top40"><Spin size="large"/></div> :
                            <WeaEchart chartName={result.blogSeriesName} categories={result.categoriesList} series={result.blogSeriesList}/>
                        }
                    </div>
                </div>
            </WeaDialog>
		);
	}
}

export default ChartDialog;