import {Button, Spin, message} from 'antd';
import {WeaDialog,WeaEchart} from 'ecCom';
import WeaDateSwitch from "../wea-date-switch/"
import * as reportApis from '../../../apis/report';
import {REPORT_TYPE} from './Const';

class CompareDialog extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            visible: false,
            loading: true,
            result: {},
            currentDate: props.currentDate
        };
        this.back = this.back.bind(this);
        this.changeDate = this.changeDate.bind(this);
	}
	
	componentWillReceiveProps(nextProps) {
	    this.setState({
	        currentDate: nextProps.currentDate
        });
    }
	back() {
	    this.setState({
	        visible: false
        });
    }
    show() {
        this.setState({
            visible: true,
            loading: true
        }, () => {
            let {tableBody, cols, type, reportId} = this.props;
            let {currentDate} = this.state;
            let isAttention = reportId == REPORT_TYPE.attention ? 1 : 0;
            let ids = tableBody.filter((row, i) => cols[i]).map((row) => row[0] && (isAttention ? row[0].oId: row[0].conditionId));
            if (ids.length == 0) {
                message.info("请选择对比条目");
                return;
            }
            let that = this;
            reportApis.getBlogComparedChartInfos({
                isAttention: isAttention,
                year: currentDate.getFullYear(),
                month: currentDate.getMonth() + 1,
                conditionIds: ids.join(','),
                chartType: type
            }).then(result=> {
                this.setState({
                    loading: false,
                    result: result
                });
            });
        });
    }
    changeDate(value) {
        this.setState({
            currentDate: value
        }, this.show);
    }
	render() {
	    let {title} = this.props;
	    let {visible,currentDate, loading, result={}} = this.state;
		return (
			<WeaDialog visible={visible} title={title} style={{width: 800, height: 500}}
                       icon="icon-coms-blog" iconBgcolor="#6d3cf7"
                       buttons={[<Button type="primary" onClick={this.back}>返回</Button>]}>
                <div className="chartBody">
                    <WeaDateSwitch ref="dateSwitch" defaultValue={currentDate} onChange={this.changeDate}
                                   style={{fontSize: "14px"}}/>
                    <div className="chartContainer" style={{top: 23}}>
                        {loading ? <div className="align-center top40"><Spin size="large"/></div> :
                            <WeaEchart chartName={result.chartName} categories={result.categories} series={result.series}/>
                        }
                    </div>
                </div>
            </WeaDialog>
		);
	}
}
export default CompareDialog;