import React from 'react'
import objectAssign from 'object-assign'

import {Tooltip, Rate} from 'antd'

import ChartDialog from '../ChartDialog';

class TotalCell extends React.Component {
	constructor(props) {
		super(props);
        this.showChart = this.showChart.bind(this);
	}
	showChart() {
	    this.refs.chartDialog.show();
    }
	render() {
        let {value, title, isShowChart, oId, oType, currentDate, oIndexChartType} = this.props;
        let year = currentDate.getFullYear();
		return (
            <span className="wea-index-cell">
                <Tooltip title={title}>
                    <span className="wea-ic-star">
                        <Rate allowHalf disabled defaultValue={value} value={value} style={{fontSize: "14px"}}/>
                        <span className="wea-ics-text">{value}</span>
                    </span>
                </Tooltip>
                {isShowChart == '1' ?
                    <span className="wea-ic-chart icon-blog-ReportForm" onClick={this.showChart}/> :
                    <span className="wea-ic-chart icon-blog-ReportForm disabled"/>
                }
                <ChartDialog ref="chartDialog" type={oType} value={oId} year={year} chartType={oIndexChartType}/>
            </span>
		);
	}
}
export default TotalCell;

