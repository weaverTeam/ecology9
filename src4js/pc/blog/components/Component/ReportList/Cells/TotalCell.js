import React from 'react'
import objectAssign from 'object-assign'

import {Tooltip} from 'antd'

export default function TotalCell(props) {
    let {value, title} = props
    return (
            <span>
                <Tooltip title={title[0]}>
                    <span style={{color: "red"}}>{value[0]}</span>
                </Tooltip>
                <Tooltip title={title[1]}>
                    <span style={{display: 'inline-block'}}>/{value[1]}</span>
                </Tooltip>
            </span>
    )
}