import React from 'react'
import objectAssign from 'object-assign'

import {ICONS} from '../Const'

import {Tooltip} from 'antd'

export default function ReportCell(props) {
    let {value, title, hasText, style} = props
    if (hasText) {
        return (
            <div style={objectAssign({}, {display: "inline-block", verticalAlign: "middle"}, style)}>
                <span
                    className={ICONS[value].icon}
                    style={{fontSize: "14px", verticalAlign: "middle", color: ICONS[value].color}}
                />
                <span style={{verticalAlign: "middle"}}>&nbsp;&nbsp;{ICONS[value].text}</span>
            </div>
        )
    } else {
        return (
            <Tooltip title={title}>
                <span className={ICONS[value].icon} style={{fontSize: "14px", color: ICONS[value].color}}/>
            </Tooltip>
        )
    }
}