import HeaderCell from "./HeaderCell"
import IndexCell from "./IndexCell"
import NormalCell from "./NormalCell"
import ReportCell from "./ReportCell"
import TotalCell from "./TotalCell"

import "./index.less"

export default {
    header: HeaderCell,
    index: IndexCell,
    normal: NormalCell,
    report: ReportCell,
    total: TotalCell
}