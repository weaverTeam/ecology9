import React from 'react'
import objectAssign from 'object-assign'

import {Tooltip} from 'antd'

export default function NormalCell(props) {
    let {value, title} = props
    return (
        <Tooltip title={title}>
            <span>{value}</span>
        </Tooltip>
    )
}