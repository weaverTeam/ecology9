import React from 'react'
import objectAssign from 'object-assign'
import WeaBlogDict from '../../wea-blog-dict';
const innerHref = WeaBlogDict.href;

import {Tooltip, Rate} from 'antd'

class HeaderCell extends React.Component {
	constructor(props) {
		super(props);
        this.toggleConditionExpand = this.toggleConditionExpand.bind(this);
        this.state = {
            open: false
        };
	}
	
	componentWillReceiveProps(nextProps) {
	    if (nextProps.open != this.state.open) {
	        this.setState({
	            open: nextProps.open
            });
        }
    }
    toggleConditionExpand(oId) {
        // 第一次请求花的时间比较多，所以还是用state做处理。
	    let open = !this.state.open;
	    this.setState({
	        open: open
        });
	    this.props.toggleConditionExpand(this.props);
    }
	render() {
        let {oType, oName, value, title, oId} = this.props
        if (!oType) {
            return (
                <span title={title}>
                    {value}
                </span>
            )
        }
        let iconClass = this.state.open ? 'icon-coms-form-delete' : 'icon-coms-Add-to';
        return (
            <span className="wea-header-cell">
                {oType != 1 ?
                    [<span className={iconClass} onClick={this.toggleConditionExpand}/>,
                        <a className='ml20' onClick={this.toggleConditionExpand}>{oName}</a>] :
                    <a className="ml32" href={innerHref.blog(oId)} target="_blank">{oName}</a>
                }
            </span>
        );
	}
}

export default HeaderCell;