import React from 'react'

import './index.less'

export default function WeaSplitLine(props) {
    const {children, ...rest} = props
    return (
        <div className='wea-split-line' {...rest}>
            <div className='wea-sl-item wea-sl-line'/>
            <div className='wea-sl-item wea-sl-wrap '>
                <div className='wea-sl-text'>
                    {children}
                </div>
            </div>
        </div>
    )
}