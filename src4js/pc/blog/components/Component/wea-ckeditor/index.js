import React from 'react';

import CKEAppend from "./CKEAppend"
import CKEReplace from "./CKEReplace"

export default class WeaCKEditor extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {ck, ...rest} = props
        return ck == "replace" ? <CKEReplace ref="ck" {...rest}/> : <CKEAppend ref="ck" {...rest}/>
    }
}