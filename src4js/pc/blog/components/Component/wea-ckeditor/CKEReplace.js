import React, {Component} from 'react';
import T from 'prop-types'

import CONFIG from "./config"

import Toolbar from "./CKEBar/index"

export default class WeaCKEditorReplace extends Component {
    constructor(props) {
        super(props)
        this.state = {
            editing: false,
            isLoaded: false
        }
    }

    componentWillUnmount() {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        editor.focusManager.hasFocus = false
    }

    componentWillReceiveProps(props) {
        if (!this.state.isLoaded && props.content) {
            this.SetContents(props.content)
            this.setState({isLoaded: true})
        }
    }

    componentDidMount() {
        const {
            id, type, config,
            onReady, onFocus, onBlur, onChange
        } = this.props

        CKEDITOR.on('instanceReady', (event) => {
            if (onReady) {
                onReady()
            } else {
            }
            this.setState({editing: true})
            //document.getElementById(id + '_toolbar').style.display = 'block';
        })

        const editor = CKEDITOR.replace(id, CONFIG[type](config))

        editor.focusManager.hasFocus = false

        editor.on('focus', function (event) {
            if (onFocus) {
                onFocus()
            } else {
            }
        })

        editor.on('blur', function (event) {
            if (onBlur) {
                onBlur()
            } else {
            }
        })

        editor.on('change', function (event) {
            if (onChange) {
                onChange()
            } else {
            }
        })
    }

    get instance() {
        return CKEDITOR.instances[this.props.id]
    }

    get instances() {
        return CKEDITOR.instances
    }

    render() {
        const {id, content, configBar, typeBar, onClickBar} = this.props,
            {editing} = this.state

        return (
            <div id={id + "_container"} style={{display: (editing ? "" : "none")}}>
                <textarea name={id} id={id} value={content}/>
                <Toolbar id={id} config={configBar} type={typeBar} ref="toolbar" insertHTML={this.InsertHTML} onClick={onClickBar}/>
            </div>
        )
    }

    GetContents = () => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        let toolbar = this.refs.toolbar.value

        return editor.getData()
    }

    SetContents = (contents) => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        editor.setData(contents)
    }

    InsertHTML = (html, isClear) => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg') {

            // Insert as html.
            if(isClear) editor.setData(html)
            else editor.insertHtml(html);
        } else {
            alert('You must be in WYSIWYG mode!');
        }
    }

    InsertText = (text) => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg') {

            // Insert as plain text.
            editor.insertText(text);
        } else {
            alert('You must be in WYSIWYG mode!');
        }
    }

    ExecuteCommand = (commandName) => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        // Check the active editing mode.
        if (editor.mode == 'wysiwyg') {

            // Execute the command.
            editor.execCommand(commandName);
        }
        else
            alert('You must be in WYSIWYG mode!');
    }

    CheckDirty = () => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        // Checks whether the current editor content contains changes when compared to the content loaded into the editor at startup.
        return editor.checkDirty()
    }

    ResetDirty = () => {
        const editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        // Resets the "dirty state" of the editor.
        editor.resetDirty()
    }

    CleanEditor = (hasToolbar) => {
        let editor = CKEDITOR.instances[this.props.id]
        if (!editor) return

        editor.focusManager.hasFocus = false

        let toolbar = this.refs.toolbar

        // Retrieve the editor content. In an Ajax application this data would be sent to the server or used in any other way.
        let html = editor.getData()
        editor.setData("")
        editor.resetDirty()

        toolbar.clear = true
        if(hasToolbar){
            return {
                content: html,
                toolbar: toolbar.value
            }
        }
        return html
        //return html
    }
}

WeaCKEditorReplace.propTypes = {
    id: T.string,//id或name
    type: T.string,//类型
    config: T.object,//自定义配置
    content: T.string,//初始化内容

    onReady: T.function,//加载前函数
    onFocus: T.function,//聚焦函数
    onBlur: T.function,//失焦函数
    onChange: T.function,//文本内容变更函数
}

WeaCKEditorReplace.defaultProps = {
    id: "weackeditor",
    type: "default"
}

WeaCKEditorReplace.ref = {
    GetContents: T.function,//return: <string> html
    SetContents: T.function,//params: <string> contents
    InsertHTML: T.function,//params: <string> html
    InsertText: T.function,//params: <string> text
    ExecuteCommand: T.function,//params: <string> commond

    CleanEditor: T.function,//return: <string> html

    CheckDirty: T.function,//return: <boolean>
    ResetDirty: T.function,

    instance: T.object,
    instances: T.array
}