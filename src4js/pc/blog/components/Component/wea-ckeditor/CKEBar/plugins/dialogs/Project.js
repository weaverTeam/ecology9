import React, {Component} from 'react';
import T from 'prop-types'

import {WeaBrowser} from 'ecCom';

export default class Project extends Component {
    constructor(props) {
        super(props)
        this.state = {value: ""}
    }

    get value() {
        return {project: this.state.value}
    }

    set value(value) {
        this.setState({value: value})
    }

    render() {
        return (
            <WeaBrowser
                type={135}
                title="项目"
                customized={true}
                hasAdvanceSerach={true}
                isSingle={false}
                onChange={this.onChange}
            >
                <div className="wea-cb-item">
                    <span className="wea-cbi-icon icon-blog-Project"/>
                    <span className="wea-cbi-text">项目</span>
                </div>
            </WeaBrowser>
        )
    }

    onChange = (a, b, value) => {
        let anruo = [],
            project = value.map(
                item => {
                    anruo.push("<a href='javascript:void(0)'  linkid=" + item.id + " linkType='project' onclick='try{return openAppLink(this," + item.id + ");}catch(e){}' ondblclick='return false;'  unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>" + item.name + "</a>")
                    return {id: item.id, name: item.name, type: "project"}
                }
            )
        this.props.insertHTML(anruo.join("&nbsp;&nbsp;&nbsp;"))

        this.setState({value: project})
    }
}

Project.propTypes = {}

Project.defaultProps = {}

Project.ref = {}