import React, {Component} from 'react';
import T from 'prop-types'

import {WeaBrowser} from 'ecCom';

export default class Templet extends Component {
    constructor(props) {
        super(props)
        this.state = {value: ""}
    }

    get value() {
        return {template: this.state.value}
    }

    set value(value) {
        this.setState({value: value})
    }

    render() {
        return (
            <WeaBrowser
                customized={true}
                type={"blogTemplate"}
                title="模板"
                hasAdvanceSerach={true}
                isSingle={true}
                onChange={this.onChange}
            >
                <div className="wea-cb-item">
                    <span className="wea-cbi-icon icon-blog-Task"/>
                    <span className="wea-cbi-text">模板</span>
                </div>
            </WeaBrowser>
        )
    }

    onChange = (a, b, value) => {
        let template = value.map(
            item => {
                return {id: item.id, name: item.name, type: "template"}
            }
        )
        this.props.insertHTML(b, true)

        this.setState({value: template})
    }
}

Templet.propTypes = {}

Templet.defaultProps = {}

Templet.ref = {}