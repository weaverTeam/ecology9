import React, {Component} from 'react';
import T from 'prop-types'

import {WeaBrowser} from 'ecCom';

export default class Customer extends Component {
    constructor(props) {
        super(props)
        this.state = {value: ""}
    }

    get value() {
        return {customer: this.state.value}
    }

    set value(value) {
        this.setState({value: value})
    }

    render() {
        return (
            <WeaBrowser
                customized={true}
                type={18}
                title="客户"
                hasAdvanceSerach={true}
                isSingle={false}
                onChange={this.onChange}
            >
                <div className="wea-cb-item">
                    <span className="wea-cbi-icon icon-blog-Personnel"/>
                    <span className="wea-cbi-text">客户</span>
                </div>
            </WeaBrowser>
        )
    }

    onChange = (a, b, value) => {
        let anruo = [],
            customer = value.map(
                item => {
                    anruo.push("<a href='javascript:void(0)'  linkid=" + item.id + " linkType='crm' onclick='try{return openAppLink(this," + item.id + ");}catch(e){}' ondblclick='return false;'  unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>" + item.name + "</a>")
                    return {id: item.id, name: item.name, type: "crm"}
                }
            )
        this.props.insertHTML(anruo.join("&nbsp;&nbsp;&nbsp;"))

        this.setState({value: customer})
    }
}

Customer.propTypes = {}

Customer.defaultProps = {}

Customer.ref = {}