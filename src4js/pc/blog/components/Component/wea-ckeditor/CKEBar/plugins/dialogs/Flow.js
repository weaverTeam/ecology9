import React, {Component} from 'react';
import T from 'prop-types'

import {WeaBrowser} from 'ecCom';

export default class Flow extends Component {
    constructor(props) {
        super(props)
        this.state = {value: ""}
    }

    get value() {
        return {flow: this.state.value}
    }

    set value(value) {
        this.setState({value: value})
    }

    render() {
        return (
            <WeaBrowser
                customized={true}
                type={152}
                title="流程"
                hasAdvanceSerach={true}
                isSingle={false}
                onChange={this.onChange}
            >
                <div className="wea-cb-item">
                    <span className="wea-cbi-icon icon-blog-Process"/>
                    <span className="wea-cbi-text" title="流程">流程</span>
                </div>
            </WeaBrowser>
        )
    }

    onChange = (a, b, value) => {
        let anruo = [],
            flow = value.map(
                item => {
                    anruo.push("<a href='javascript:void(0)'  linkid=" + item.id + " linkType='workflow' onclick='try{return openAppLink(this," + item.id + ");}catch(e){}' ondblclick='return false;'  unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>" + item.name + "</a>")
                    return {id: item.id, name: item.name, type: "workflow"}
                }
            )
        this.props.insertHTML(anruo.join("&nbsp;&nbsp;&nbsp;"))

        this.setState({value: flow})
    }
}

Flow.propTypes = {}

Flow.defaultProps = {}

Flow.ref = {}