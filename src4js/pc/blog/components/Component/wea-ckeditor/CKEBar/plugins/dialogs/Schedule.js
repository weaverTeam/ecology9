import React, {Component} from 'react';
import T from 'prop-types'

import {WeaBrowser} from 'ecCom';

export default class Schedule extends Component {
    constructor(props) {
        super(props)
        this.state = {value: ""}
    }

    get value() {
        return {schedule: this.state.value}
    }

    set value(value) {
        this.setState({value: value})
    }

    render() {
        return (
            <WeaBrowser
                type={"workplan"}
                title="日程"
                customized={true}
                hasAdvanceSerach={true}
                isSingle={false}
                onChange={this.onChange}
            >
                <div className="wea-cb-item">
                    <span className="wea-cbi-icon icon-blog-Schedule"/>
                    <span className="wea-cbi-text">日程</span>
                </div>
            </WeaBrowser>
        )
    }

    onChange = (a, b, value) => {
        let anruo = [],
            schedule = value.map(
                item => {
                    anruo.push("<a href='javascript:void(0)'  linkid=" + item.id + " linkType='workplan' onclick='try{return openAppLink(this," + item.id + ");}catch(e){}' ondblclick='return false;'  unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>" + item.name + "</a>")
                    return {id: item.id, name: item.name, type: "workplan"}
                }
            )
        this.props.insertHTML(anruo.join("&nbsp;&nbsp;&nbsp;"))

        this.setState({value: schedule})
    }
}

Schedule.propTypes = {}

Schedule.defaultProps = {}

Schedule.ref = {}