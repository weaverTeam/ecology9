import React, {Component} from 'react';
import T from 'prop-types'

import {WeaBrowser} from 'ecCom';

export default class Task extends Component {
    constructor(props) {
        super(props)
        this.state = {value: ""}
    }

    componentDidMount() {

    }

    render() {
        return (
            <div className="wea-cb-item">
                <span className="wea-cbi-icon icon-blog-Task"/>
                <span className="wea-cbi-text">任务</span>
            </div>
        )
    }
}

Task.propTypes = {}

Task.defaultProps = {}

Task.ref = {}