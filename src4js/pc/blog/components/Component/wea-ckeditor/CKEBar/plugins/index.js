import dialogs from "./dialogs/"
import selects from "./selects/"
import uploads from "./uploads/"

export default {
    dialogs,
    selects,
    uploads
}