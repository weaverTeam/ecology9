import React, {Component} from 'react';
import T from 'prop-types'
import {WeaUpload} from 'ecCom';

import {Progress} from 'antd';

window.anruo = {
    ckeditor: {
        attachments: []
    }
}

export default class Attachment extends Component {
    constructor(props) {
        super(props)
        this.state = {
            value: [],
            status: "",
            percent: 0,
            speed: 500
        }
    }

    get value() {
        return {attachment: this.state.value}
    }

    set value(value) {
        //this.setState({value: value})
    }


    componentDidMount() {
        //<WeaUpload ref={fieldName} uploadId={fieldName} {...objectAssign({},baseProps,fileattr)} onChange={this.fileChangeEvent.bind(this)} onUploading={this.onUploading.bind(this)}>
        //</WeaUpload>
    }

    render() {
        let {params, id} = this.props,
            {uploadUrl = "", position = "", maxSize = ""} = params,
            {percent} = this.state
        return (
            uploadUrl ? (
                <WeaUpload
                    maxUploadSize={maxSize}
                    btnSize="small"
                    uploadUrl={`${uploadUrl}?category=${position}`}
                    category={uploadUrl}
                    onUploading={this.onFileUploading}
                    onChange={this.onFileChange}
                    ref={'attachment'}
                    uploadId={id + "_attachment"}
                    style={{display: "inline-block", padding: "0"}}
                >
                    <div className="wea-cb-item" onClick={this.onClick}>
                        <span className="wea-cbi-icon icon-blog-Enclosure"/>
                        <span className="wea-cbi-text">附件({maxSize}M)</span>
                        {
                            percent == 0 || percent == 100 ? "" : (
                                <div style={{display: "inline-block"}}>
                                    <span style={{verticalAlign: "middle"}}>
                                        &nbsp;(&nbsp;
                                    </span>
                                            <Progress
                                                type="circle"
                                                status="active"
                                                percent={percent}
                                                width="16"
                                                format={percent => ''}
                                            />
                                            <span style={{verticalAlign: "middle"}}>
                                        &nbsp;{`${percent}%`}&nbsp;)
                                    </span>
                                </div>
                            )
                        }
                    </div>
                </WeaUpload>
            ) : (
                <div className="wea-cb-item" onClick={this.onClick}>
                    <span className="wea-cbi-icon icon-blog-Enclosure"/>
                    <span className="wea-cbi-text">附件</span>
                </div>
            )
        )
    }

    onClick = () => {
        let {onClick} = this.props
        if (onClick) onClick()
        this.setState({percent: 0, speed: 500})
    }

    onFileChange = (a, afterData) => {
        let beforeData = window.anruo.ckeditor.attachments,
            //beforeData = this.state.value,
            data = afterData.slice(beforeData.length),
            anruo = ""

        data.forEach(
            item => {
                anruo += item.filelink
            }
        )

        this.props.insertHTML(anruo)

        window.anruo.ckeditor.attachments = afterData
        //this.setState({value: afterData})
    }

    onFileUploading = (status) => {
        this.setState({status: status})

        if (status == "uploading") {
            let timeCount = () => {
                let {percent} = this.state
                if (percent == 80) {
                    this.setState({percent: percent + 1, speed: 10000})
                } else if (percent > 80) {
                    if (percent >= 98) {
                        clearInterval(this.interval)
                        this.setState({percent: 98, speed: 500})
                    } else {
                        this.setState({percent: percent + 1})
                    }
                } else {
                    this.setState({percent: this.state.percent + 10})
                }
            }
            this.interval = setInterval(timeCount, this.state.speed)
            timeCount()
        }
        if (status == "uploaded") {
            clearInterval(this.interval)
            this.setState({percent: 100, speed: 500})
        }
    }
}


Attachment.propTypes = {}

Attachment.defaultProps = {}

Attachment.ref = {}