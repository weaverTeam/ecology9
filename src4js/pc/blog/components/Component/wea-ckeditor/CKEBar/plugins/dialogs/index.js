import Document from "./Document"
import Schedule from "./Schedule"
import Flow from "./Flow"
import Customer from "./Customer"
import Project from "./Project"
import Task from "./Task"
import Templet from "./Templet"

export default {
    Document,
    Schedule,
    Flow,
    Customer,
    Project,
    Task,
    Templet
}