import React, {Component} from 'react';
import T from 'prop-types'

import CONFIG from "./config"
import Plugins from "./plugins"
import objectAssign from 'object-assign'

export default class WeaCKEditorBar extends Component {
    constructor(props) {
        super(props)
    }

    get value() {
        let result = {}
        for (let ref in this.refs) {
            result = objectAssign({}, result, this.refs[ref].value)
        }
        return result
    }

    set clear(value) {
        if (value) {
            for (let ref in this.refs) {
                this.refs[ref].value = ""
            }
        }
    }

    render() {
        let {insertHTML, type = "default", config = [], onClick, id} = this.props,
            Bars = [],
            Params = []
        CONFIG[type](config).forEach(
            g => {
                if (Object.prototype.toString.call(g.items) == "[object Array]" && g.items.length > 0)
                    return g.items.forEach(
                        i => {
                            if (typeof i == "string") {
                                Params.push("")
                                Bars.push(Plugins[g.name][i])
                            } else {
                                let name = Object.keys(i)[0]
                                if (name) {
                                    Params.push(i[name])
                                    Bars.push(Plugins[g.name][name])
                                }
                            }
                        }
                    )
            }
        )
        return (
            (config.length == 0 ) && type == "default" ? <div/> : (
                <div id={id} className="wea-ckeditor-bar">
                    {
                        Bars.map(
                            (Tool, key) => {
                                return <Tool ref={`bar${key}`} id={id} insertHTML={insertHTML}
                                             params={Params[key]} onClick={onClick}/>
                            }
                        )
                    }
                </div>
            )
        )
    }
}

WeaCKEditorBar.propTypes = {}

WeaCKEditorBar.defaultProps = {}

WeaCKEditorBar.ref = {}