import "./index.less"
import objectAssign from 'object-assign'

const CONFIG = []

const basic = []

//all edition
CONFIG.default = function (config) {
    return objectAssign([], basic, config)
}

//blog edition
CONFIG.blog = function (config = []) {
    return objectAssign([], [
        {name: 'selects', items: ['Mood']},
        {name: 'dialogs', items: ['Document', 'Schedule', 'Flow', 'Customer', 'Project', 'Task', 'Templet']},
        {name: 'uploads', items: ['Attachment']}
    ], basic, config)
}

export default CONFIG