import "./index.less"
import objectAssign from 'object-assign'

const CONFIG = {}

const basic = {
    uiColor: '#f8f8f8',
    width: 'auto',
    height: 80,
    language: "zh-cn" || "en",
    toolbarCanCollapse: false,
    allowedContent: true,
    startupFocus:true
}

//all edition
CONFIG.default = function (config) {
    return objectAssign({}, {
        toolbar:[
            { name: 'markdown', items: [ 'Markdown' ] },
            { name: 'document', items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
            { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            { name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
            { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
            '/',
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
            { name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
            { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
            { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        ],
        extraPlugins: 'markdown'
    }, basic, config)
}

//blog edition
CONFIG.blog = function (config = {}) {
    return objectAssign({}, {
        toolbar:[
            { name: 'markdown', items: [ 'Markdown' ] },
            { name: 'document', items: [ 'Source'] },
            { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
            { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] }
        ],
        extraPlugins: 'markdown'
    }, basic, config)
}

export default CONFIG