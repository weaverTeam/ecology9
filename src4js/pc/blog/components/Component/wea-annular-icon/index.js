import React from 'react'

export default function WeaAnnularIcon(props) {
    const {diameter = 22, icon = "", color = "#fff", className, style} = props,
        iconStyle = {
            position: "relative",
            width: diameter + "px",
            height: diameter + "px",
            display: "inline-block"
        },
        itemStyle = {
            position: "absolute",
        },
        outerStyle = {
            width: diameter + "px",
            height: diameter + "px",
            borderRadius: diameter / 2 + "px",
            border: "1px solid #c5c5c5"
        },
        innerStyle = {
            left: diameter / 6 + "px",
            top: diameter / 6 + "px",
            fontSize: diameter / 3 * 2,
            color: color
        }
    return (
        <div style={{...iconStyle, ...style}} className={className}>
            <span style={{...itemStyle, ...outerStyle}}/>
            <span className={icon} style={{...itemStyle, ...innerStyle}}/>
        </div>
    )
}
