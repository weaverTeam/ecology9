import React, {Component, Children} from 'react'
import T from 'react-router/lib/PropTypes'
import classNames from 'classnames'

import {Icon, Tooltip} from 'antd'

import './dotCom.less'

import ComVar from '../ComVar'
const {Type, PrefixCls, Format} = ComVar
const P = PrefixCls + '-item-dot'

import WeaAnnularIcon from "../../wea-annular-icon"

let DotCom = function(type, text, tip, format) {
    let dotComCls = classNames(
        {
            [`${P}`]: true
        }
    ),
        dotComTextCls = classNames(
            {
                [`${P}-text`]: true,
                [`${P}-text-${format}`]: !!format
            }
        ),
        typeIcon = {
            current: {
                name: "current",
                icon: "icon-blog-solid",
                color: "#32a8ff",
                tip: "编辑中 "
            },
            normal: {
                name: "normal",
                icon: "icon-blog-Submit",
                color: "#75bf26",
                tip: "提交于 "
            },
            expired: {
                name: "expired",
                icon: "icon-blog-delay",
                color: "#f5b400",
                tip: "补交于 "
            },
            white: {
                name: "white",
                icon: "icon-blog-Uncommitted",
                color: "#ff3044",
                tip: "未提交 "
            }
        }
    return (
        <Tooltip title={tip} placement="right">
            <div className={dotComCls}>
                <WeaAnnularIcon {...typeIcon[type]}/>
                <div className={dotComTextCls}>
                    <span>{text}</span>
                </div>
            </div>
        </Tooltip>
    )
};
/*class DotCom extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {type, text, tip, format} = this.props,
            dotComCls = classNames(
                {
                    [`${P}`]: true
                }
            ),
            dotComIconCls = classNames(
                {
                    [`${P}-icon`]: true,
                    [`${P}-icon-${type}`]: !!type
                }
            ),
            dotComCircleCls = classNames(
                {
                    [`${P}-circle`]: true
                }
            ),
            dotComTextCls = classNames(
                {
                    [`${P}-text`]: true,
                    [`${P}-text-${format}`]: !!format
                }
            ),
            typeIcon = {
                current: {
                    name: "current",
                    icon: "icon-blog-solid",
                    color: "#32a8ff",
                    tip: "编辑中 "
                },
                normal: {
                    name: "normal",
                    icon: "icon-blog-Submit",
                    color: "#75bf26",
                    tip: "提交于 "
                },
                expired: {
                    name: "expired",
                    icon: "icon-blog-delay",
                    color: "#f5b400",
                    tip: "补交于 "
                },
                white: {
                    name: "white",
                    icon: "icon-blog-Uncommitted",
                    color: "#ff3044",
                    tip: "未提交 "
                }
            }
        return (
            <Tooltip title={tip} placement="right">
                <div className={dotComCls}>
                    <WeaAnnularIcon {...typeIcon[type]}/>
                    <div className={dotComTextCls}>
                        <span>{text}</span>
                    </div>
                </div>
            </Tooltip>
        )
    }
}

DotCom.propTypes = {
    type: T.string,
    text: T.string,
    tip: T.string,
    format: T.string
}

DotCom.defaultProps = {
    type: Type.normal.name,
    text: Type.normal.text,
    tip: Type.normal.tip,
    format: Format.year,
}*/

export default DotCom