import React, {Component} from 'react'
import T from 'prop-types'
import {Timeline} from 'antd'

import './timelineItem.less'

import Utils from '../wea-utils'

import DotCom from './DotCom/index'
import ComVar from './ComVar'
const {Offset, Week, PrefixCls, Format, Type} = ComVar
const P = PrefixCls + '-item'

class WeaTimelineItem extends Component {
    constructor(props) {
        super(props)
    }

    getActualDate = (dateStr) => {
        let dateArr = dateStr.split('-'),
            date = new Date(dateArr[0], --dateArr[1], dateArr[2])
        return date
    }

    getDateDif = (start, end) => {
        const date = (start - end) / 86400000
        if (date > 0) {
            return Math.abs(Math.floor(date))
        }
        return Math.abs(Math.ceil(date))
    }

    getDateText = (date) => {
        const now = new Date(),
            days = this.getDateDif(now, date),
            yearNow = now.getFullYear(),
            yearDate = date.getFullYear()

        let text = '', format = ''

        if (yearNow >= yearDate) {
            if (days == 0) {
                text += '今天'
                format = Format.current
            } else if (days <= Offset) {
                text += Week[date.getDay()]
                format = Format.week
            } else {
                text += (date.getMonth() + 1) + '月' + date.getDate() + '日'
                format = Format.month
                // if (yearNow !== yearDate) {
                //     text = (yearDate + '年') + text
                //     format = Format.year
                // }
            }
        } else {
            text = '非法日期'
            format = Format.invalid
        }

        return [format, text]
    }

    render() {
        const {date, type, tipDate, children} = this.props
        let format = Format.current, text = "今天", tip = Type.current.tip
        if (!!date) {
            const dateActual = this.getActualDate(date)
            format = this.getDateText(dateActual)
            text = this.getDateText(dateActual)[1]
            tip = Type[type].tip + ((type == Type.normal.name || type == Type.expired.name) ? (' ' + Utils.DateFormat(tipDate, "yyyy年MM月dd日 HH:mm")) : "")
        }

        return (
            <div>
                <Timeline.Item className={P} dot={DotCom(type, text, tip, format)}>
                    {children}
                </Timeline.Item>
            </div>
        )
    }
}

WeaTimelineItem.propTypes = {
    type: T.string.isRequired,
    date: T.string.isRequired,//时间轴上日期，格式："yyyy-MM-dd"
    tipDate: T.string.isRequired//提示的具体时间，格式："yyyy-MM-dd hh:mm:ss"
}

WeaTimelineItem.defaultProps = {
    type: Type.normal.name
}

export default WeaTimelineItem