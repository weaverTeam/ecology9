import React from 'react'
import objectAssign from 'object-assign'
export default function WeaCircleImage(props){
    const {diameter = 0, name, src, style} = props,
        imgStyle = {
            width: diameter,
            height: diameter,
            borderRadius: diameter / 2,
            display: "inline-block",
            verticalAlign: "middle"
        },
        spanStyle = {
            width: diameter,
            height: diameter,
            borderRadius: diameter / 2,
            display: "inline-block",
            verticalAlign: "middle",

            fontSize: diameter * 0.25,
            background: "#32a8ff",
            color: "white",
            textAlign: "center",
            lineHeight: diameter + "px"
        }
    return (
        !!src ? (
            <img src={src} style={objectAssign({}, imgStyle, {...style})}/>
        ) : (
            <span style={objectAssign({}, spanStyle, {...style})}>{name}</span>
        )
    )
}