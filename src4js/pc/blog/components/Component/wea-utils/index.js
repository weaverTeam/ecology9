const Utils = {}

Utils.GB2312UnicodeConverter = {
    ToUnicode: function (str) {
        return escape(str).toLocaleLowerCase().replace(/%u/gi, '\\u');
    }
    , ToGB2312: function (str) {
        return unescape(str.replace(/\\u/gi, '%u'));
    }
};

// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Utils.DateFormat = (date, fmt) => {
    if (date && fmt) {
        let _this = date
        const type = Object.prototype.toString.call(date)
        if (type == "[object String]") {
            let dateArray = date.split(" "),
                d1 = dateArray[0].split("-"),
                d2 = dateArray[1] ? dateArray[1].split(":") : [];
            _this = new Date((d1[0] || 0) * 1, (d1[1] || 0) * 1 - 1, (d1[2] || 0) * 1, (d2[0] || 0 ) * 1, (d2[1] || 0) * 1, (d2[2] || 0) * 1)
        } else if (type == "[object Date]") {

        } else {
            return ""
        }
        let o = {
            "M+": _this.getMonth() + 1, //月份
            "d+": _this.getDate(), //日
            "h+": _this.getHours() % 12 == 0 ? 12 : _this.getHours() % 12, //小时
            "H+": _this.getHours(), //小时
            "m+": _this.getMinutes(), //分
            "s+": _this.getSeconds(), //秒
            "q+": Math.floor((_this.getMonth() + 3) / 3), //季度
            "S": _this.getMilliseconds() //毫秒
        };
        let week = {
            "0": "\\u65e5",
            "1": "\\u4e00",
            "2": "\\u4e8c",
            "3": "\\u4e09",
            "4": "\\u56db",
            "5": "\\u4e94",
            "6": "\\u516d"
        };
        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (_this.getFullYear() + "").substr(4 - RegExp.$1.length));
        }
        if (/(E+)/.test(fmt)) {
            let weekStr = ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\\u661f\\u671f" : "\\u5468") : "") + week[_this.getDay() + ""]
            fmt = fmt.replace(RegExp.$1, Utils.GB2312UnicodeConverter.ToGB2312(weekStr));
        }
        for (let k in o) {
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    } else {
        return ""
    }
}

Utils.DateAddition = (date, number, unit) => {
    switch (unit) {
        case "year ": {
            date.setFullYear(date.getFullYear() + number);
            break;
        }
        case "season": {
            date.setMonth(date.getMonth() + number * 3);
            break;
        }
        case "month": {
            date.setMonth(date.getMonth() + number);
            break;
        }
        case "week": {
            date.setDate(date.getDate() + number * 7);
            break;
        }
        case "day": {
            date.setDate(date.getDate() + number);
            break;
        }
        case "hour": {
            date.setHours(date.getHours() + number);
            break;
        }
        case "minute": {
            date.setMinutes(date.getMinutes() + number);
            break;
        }
        case "second": {
            date.setSeconds(date.getSeconds() + number);
            break;
        }
        default: {
            date.setDate(date.getDate() + number);
            break;
        }
    }
    return date;
}

Utils.GetRegex = (name) => {
    let regex = {
        "wrap": /[\f\n\r\t\v]/g
    }
    if (name)return regex[name]
    return //
}

Utils.MapPropsToFields = (data) => {
    var result = {}
    for (var key in data) {
        result[key] = {
            value: data[key]
        }
    }
    return result
}
Utils.imgZoom = (dom, parentSelector) => {
    let imgList = jQuery(parentSelector + " img",dom);
    imgList.each((index,img) => {
        let $img = $(img);
        if ($img.attr("resize")) {
            return;
        }
        if ($img.parents(".ui-coomixPic").length > 0) {
            return;
        }
        $img.css({"max-width": "400px", "max-height" : "400px"}).attr("resize", 1).coomixPic({
            path:'/blog/js/weaverImgZoom/images',
            preload:true,
            blur: true,
            left: '向左转',
            right: '向右转',
            source: '查看原图',
            hide: '收起'
        });
    });
};

export default Utils