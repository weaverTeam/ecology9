export default {
	href: {
		user: (userId) => `/hrm/HrmTab.jsp?_fromURL=HrmResource&id=${userId}`,//人事信息页面
		blog: (blogId) => `/spa/blog/index.html#/user/${blogId}`,//微博信息页面
        // blog: (blogId) => `/blog/viewBlog.jsp?blogid=${blogId}`,//微博信息页面
        dept: (deptId) => `/hrm/HrmTab.jsp?_fromURL=HrmDepartmentDsp&id=${deptId}&hasTree=`,//部门页面
        viewAttentionUser: (userId) => `#/main/blog/myAttention/user/${userId}`,
        viewAttentionList: () => `#/main/blog/myAttention/list`
	},
    addWindowFunc: () => {
        let openFullWindowHaveBar = (url) => {
            var redirectUrl = url;
            var width = screen.availWidth - 10;
            var height = screen.availHeight - 50;
            var szFeatures = "top=0,";
            szFeatures += "left=0,";
            szFeatures += "width=" + width + ",";
            szFeatures += "height=" + height + ",";
            szFeatures += "directories=no,";
            szFeatures += "status=yes,toolbar=no,location=no,";
            szFeatures += "menubar=no,";
            szFeatures += "scrollbars=yes,";
            szFeatures += "resizable=yes"; //channelmode
            window.open(redirectUrl, "", szFeatures);
        }
    
        //打开应用连接
        window.openAppLink = (obj, linkid) => {
            var linkType = jQuery(obj).attr("linkType");
            var discussid = jQuery(obj).parents(".reportItem").attr("id");
            if (linkType == "doc")
                window.open("/docs/docs/DocDsp.jsp?moduleid=blog&id=" + linkid + "&discussid=" + discussid + "&linkType=" + linkType + "&linkid=" + linkid);
            else if (linkType == "task")
                window.open("/proj/process/ViewTask.jsp?moduleid=blog&taskrecordid=" + linkid + "&discussid=" + discussid + "&linkType=" + linkType + "&linkid=" + linkid);
            else if (linkType == "crm")
                window.open("/CRM/data/ViewCustomer.jsp?moduleid=blog&CustomerID=" + linkid + "&discussid=" + discussid + "&linkType=" + linkType + "&linkid=" + linkid);
            else if (linkType == "workflow")
                window.open("/workflow/request/ViewRequest.jsp?moduleid=blog&requestid=" + linkid + "&discussid=" + discussid + "&linkType=" + linkType + "&linkid=" + linkid);
            else if (linkType == "project")
                window.open("/proj/data/ViewProject.jsp?moduleid=blog&ProjID=" + linkid + "&discussid=" + discussid + "&linkType=" + linkType + "&linkid=" + linkid);
            else if (linkType == "workplan")
                window.open("/workplan/data/WorkPlanDetail.jsp?moduleid=blog&workid=" + linkid + "&discussid=" + discussid + "&linkType=" + linkType + "&linkid=" + linkid);
            return false;
        }
        //打开附件
        window.opendoc = (showid, versionid, docImagefileid, obj) => {
            var discussid = jQuery(obj).parents(".reportItem").attr("id");
            openFullWindowHaveBar("/docs/docs/DocDspExt.jsp?id=" + showid + "&imagefileId=" + docImagefileid + "&blogDiscussid=" + discussid + "&isFromAccessory=true&isfromcoworkdoc=1");
        }
        //打开附件
        window.opendoc1 = (showid, obj) => {
            var discussid = jQuery(obj).parents(".reportItem").attr("id");
            openFullWindowHaveBar("/docs/docs/DocDsp.jsp?id=" + showid + "&blogDiscussid=" + discussid + "&pstate=sub");
        }
        //下载附件
        window.downloads = (files, obj) => {
            var discussid = jQuery(obj).parents(".reportItem").attr("id");
            //jQuery("#downloadFrame").attr("src", "/weaver/weaver.file.FileDownload?fileid=" + files + "&download=1&blogDiscussid=" + discussid);
            window.open("/weaver/weaver.file.FileDownload?fileid=" + files + "&download=1&blogDiscussid=" + discussid)
        }

        window.openBlog = (blogid)=>{
            var url="/blog/viewBlog.jsp?blogid="+blogid;
            window.open(url);
         }
    },
    bottomBarMirror: {
        Document: {type: 37, name: '文档', icon: 'icon-blog-Document'},
        Schedule: {type: 'workplan', name: '日程', icon: 'icon-blog-Schedule'},
        Flow: {type: 152, name: '流程', icon: 'icon-blog-Process'},
        Customer: {type: 18, name: '客户', icon: 'icon-blog-Personnel'},
        Project: {type: 135, name: '项目', icon: 'icon-blog-Project'},
        Task: {type: 'prjtsk', name: '任务', icon: 'icon-blog-Task'},
        Templet: {type: 'blogTemplate', name: '模板', icon: 'icon-blog-Task'},
    }
    
};