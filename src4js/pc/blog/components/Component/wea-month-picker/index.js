import React, {Component} from 'react'
import PropTypes from 'react-router/lib/PropTypes'

import {Dropdown} from 'antd';

import './index.less'

class WeaMonthPicker extends Component {
    constructor(props) {
        super(props)

        let {value} = this.props
        this.state = {
            visible: false,

            value: value,
            year: value.getFullYear(),
            month: value.getMonth() + 1
        }
    }

    componentWillReceiveProps(props) {
        let {year, month} = this.state,
            _year = props.value.getFullYear(),
            _month = props.value.getMonth() + 1
        this.setState({
            value: props.value,
            year: _year,
            month: _month
        })
    }

    render() {
        let {style, disabledDate} = this.props,
            {value, year, month, visible} = this.state,
            dateStr = year + "-" + (month < 10 ? '0' + month : month)
        return (
            <div className="wea-month-picker" style={style}>
                <Dropdown
                    overlay={
                        <div className="wea-month-picker-modal">
                            <div className="wea-mpm-head">
                                <span className="wea-mpmh-pre icon-blog-left" onClick={() => this.preYearClick(year)}/>
                                <span className="wea-mpmh-text">{year}</span>
                                {
                                    (disabledDate && disabledDate(value)) ? "" : (
                                        <span
                                            className="wea-mpmh-next icon-blog-right"
                                            onClick={() => this.nextYearClick(year)}
                                        />
                                    )
                                }
                            </div>
                            <table className="wea-mpm-body">
                                <tr>
                                    {[1,2,3].map(i=>this.getMonthTd(i, month))}
                                </tr>
                                <tr>
                                    {[4,5,6].map(i=>this.getMonthTd(i, month))}
                                </tr>
                                <tr>
                                    {[7,8,9].map(i=>this.getMonthTd(i, month))}
                                </tr>
                                <tr>
                                    {[10,11,12].map(i=>this.getMonthTd(i, month))}
                                </tr>
                            </table>
                            <div className="wea-mpm-foot" onClick={this.setToday}>
                                本月
                            </div>
                        </div>
                    }
                    visible={visible}
                    onVisibleChange={this.handleVisibleChange}
                    trigger={['click']}
                >
                    <span className="wea-mp-text">
                        {dateStr}
                    </span>
                </Dropdown>
            </div>
        )
    }
    
    getMonthTd(i, month) {
        let value = new Date();
        value.setFullYear(this.state.year);
        value.setMonth(i - 1);
        if (this.props.disabledDate && this.props.disabledDate(value))
            return undefined;
        let cnMonth = ['一', '二', '三', '四', '五', '六', '七', '八', '九', '十','十一' ,'十二']
        return (
            <td value={i}  onClick={this.setMonth.bind(this, i)}><span className={"wea-mpmb-item " + (month == i ? "active" : "")}>{cnMonth[i-1]}月</span></td>
        );
    }

    preYearClick = (year) => {
        let {value} = this.state
        value.setFullYear(year - 1)
        this.setState({
            value: value,
            year: year - 1
        })
    }

    nextYearClick = (year) => {
        let {value} = this.state
        value.setFullYear(year + 1)
        this.setState({
            value: value,
            year: year + 1
        })
    }

    setToday = () => {
        let value = new Date()
        this.setState({
            value: value,
            year: value.getFullYear(),
            month: value.getMonth() + 1
        })
    }

    setMonth = (month) => {
        let {value} = this.state;
        let {disabledDate} = this.props;
        value.setMonth(month - 1);
        if (disabledDate(value))
            return;
        this.setState({
            value: value,
            month: month,
            visible: false
        });
        this.props.onChange && this.props.onChange(value)
    }
    handleVisibleChange = (visible) => {
        this.setState({visible: visible});
    }
}

export default WeaMonthPicker