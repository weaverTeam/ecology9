import React from 'react'

import "./index.less"

export default WhitePage = function (props) {
    const {height = 0, type = "default", text = "没有可显示的数据"} = props,
        Type = {
            default: "icon-blog-blank",
            message: "icon-blog-NoData"
        }
    return (
        <div className="wea-white-page" style={{paddingTop: `${height / 2 - 50}px`}}>
            <span className={"wea-wp-item wea-wp-icon " + Type[type]}/>
            <span className="wea-wp-item wea-wp-data">{text}</span>
        </div>
    )
}