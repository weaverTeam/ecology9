import React from 'react';
import HisWeiboContainer from './MyAttention/HisWeiboContainer';
class HisWeiboSingle extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
        let {userId} = this.props.params;
		return (
			<HisWeiboContainer userId={userId} changeTitle={true}/>
		);
	}
}
export default HisWeiboSingle;