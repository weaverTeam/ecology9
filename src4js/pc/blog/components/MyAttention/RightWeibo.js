import React from 'react';
import HisWeiboContainer from './HisWeiboContainer';
class RightWeibo extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {userId} = this.props.params;
        return (
            <HisWeiboContainer userId={userId} changeTitle={false}/>
        );
    }
}
export default RightWeibo;