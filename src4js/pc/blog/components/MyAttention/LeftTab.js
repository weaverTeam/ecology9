import { connect } from 'react-redux';
import * as MyAttentionAction from '../../actions/myAttention';
import { bindActionCreators } from 'redux';
import { Tabs, Spin} from 'antd';
import {WeaInputSearch,WeaNewScroll,WeaOrgTree } from 'ecCom';
import {WeaNewScrollPagination} from 'ecCom';
import WeaCircleImg from '../Component/wea-circle-image';
import AttentionTool from './AttentionTool';
import WeaBlogDict from '../Component/wea-blog-dict';
import WeaWhitePage from "../Component/wea-white-page"

const TabPane = Tabs.TabPane;
const innerHref = WeaBlogDict.href;
class LeftTab extends React.Component {
	constructor(props) {
		super(props);
		this.search = this.search.bind(this);
		this.chooseUser = this.chooseUser.bind(this);
        this.changeTab = this.changeTab.bind(this);
        this.clickTree = this.clickTree.bind(this);
        this.queryNext = this.queryNext.bind(this);
        this.query = this.query.bind(this);
        this.updateUserAttentionStatus = this.updateUserAttentionStatus.bind(this);
		this.params = {
            searchUserName: "",
            currentPage: 1,
            pageSize: 30
        };
        this.state = {
            tab: "1",
            endTip: false
        };
	}
	componentDidMount() {
		this.search();
	}
	componentWillReceiveProps(nextProps) {
	    // changeTab会触发一次onScroll事件，而且是在changeTab执行完之后触发的，非常诡异。
        // 这里由于endTip是一个与props无交互的状态，暂时就这么处理了。
        if (nextProps.myAttentions != this.props.myAttentions) {
            this.setState({
                endTip: false
            });
        }
    }
	search(userName) {
        this.setState({
            endTip: false
        });
	    this.props.actions.clearLeft();
		this.params = {
            currentPage: 1,
            searchUserName: userName || "",
            pageSize:30
        };
        this.query();
	}
	queryNext() {
	    if (this.props.myAttLasted) {
            this.state.endTip || this.setState({
	            endTip: true
            });
            return;
        }
	    this.params.currentPage++;
        this.query();
    }
    query() {
        if (this.state.tab == '1')
            this.props.actions.getMyAttentions(this.params);
        else if (this.state.tab == '3')
            this.props.actions.getMyAttentionShare(this.params);
    }
	updateUserAttentionStatus(attnInfo) {
	    this.props.actions.updateUserAttentionStatus(attnInfo);
    }
	chooseUser(userId) {
	    this.props.actions.chooseUser(userId);
	    window.location.href = innerHref.viewAttentionUser(userId);
	}
	changeTab(key) {
	    this.setState({tab: key, endTip: false}, this.search);
        this.refs.search && this.refs.search.setState({
            value: ""
        });
    }
    clickTree(t) {
        if (t && t.node && t.node.props && t.node.props.type == 3) {
            this.chooseUser(t.node.props.id);
        }
    }
	render() {
		let list = this.props.myAttentions || [];
        let isOrgTree = (this.state.tab != "1" && this.state.tab != "3");
		return (
			<div className="attentionTab" style={{width:'100%',height:'100%'}}>
				<Tabs defaultActiveKey="1" size="small" onChange={this.changeTab} animated={false}>
					<TabPane tab="我的关注" key="1"/>
                    <TabPane tab="组织结构" key="2"/>
					<TabPane tab="分享给我" key="3"/>
				</Tabs>
                {isOrgTree ||
                <div className="search">
                    <WeaInputSearch ref="search" style={{width: '100%'}} onSearch={this.search}/>
                </div>
                }
                {isOrgTree ||
                    <WeaNewScrollPagination ref="scroll" onScrollEnd={this.queryNext}>
                        <ul className='attentionlist'>
                            {
                                list.map( l => {
                                    let className = l.userId == this.props.userId ? "cur" : "";
                                    return (
                                        <li key={l.userId} className={className}>
                                            <div className="line cur-pointer" onClick={this.chooseUser.bind(this, l.userId)}>
                                                <AttentionTool attentionStatus={l.attentionStatus} userId={l.userId} islower={l.islower}
                                                               callback={this.updateUserAttentionStatus}/>
                                                <span className="fl">{WeaCircleImg({src:l.imageUrl,name:l.lastname, diameter:40})}</span>
                                                <div className="content">
                                                    <div className="dis-block">
                                                        <span className="lastName">{l.lastname}</span>
                                                        {(l.isnew == "1") && (<span className="new">•</span>)}
                                                    </div>
                                                    <span className="dept text-elli">{l.departmentName + ' ' + l.jobtitle}</span>
                                                </div>
                                            </div>
                                        </li>
                                    )
                                })
                            }
                        </ul>
                        {this.props.myAttLoaded || <div className="pt20 align-center"><Spin /></div>}
                        {list.length > 0 && this.state.endTip && <div className="endTip"><i className="icon-blog-ArrowheadUp" />没有其他人了</div>}
                        {this.props.myAttLoaded && list.length == 0 && <div className="pt20 align-center"><WeaWhitePage /></div>}
                    </WeaNewScrollPagination>
                }
                <div className="weiboOrgTree" style={{display: isOrgTree ? '' : 'none'}}>
                    <WeaOrgTree  isLoadUser={true}
                                 treeNodeClick={this.clickTree}
                    />
                </div>
			</div>
		);
	}
}


const mapStateToProps = state => {
	const { myAttentions,myAttLoaded,userId,myAttLasted } = state.blogMyAttention;
	return { myAttentions,myAttLoaded,userId,myAttLasted }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(MyAttentionAction, dispatch)
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(LeftTab);