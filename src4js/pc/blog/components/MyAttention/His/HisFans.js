import AttentionApi from '../../../apis/myAttention';
import { message } from 'antd';
import Attention from '../../MyBlog/RightContainer/Attention/Attention';

class HisFans extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			fanList: []
		};
	}
	componentDidMount() {
		AttentionApi.getAttentionMesUsers({userId: this.props.userId}).then((result) => {
			this.setState({
				fanList: result.attnList || []
			});
		}).catch(error => message.error(error));
	}
	render() {
		let list = this.state.fanList;
		return (
			<div className="hisAttention">
				{list.map((item) => {
					item.attentionStatus = undefined;
					item.attentionMeStatus = undefined;
					return (
						<Attention
							key={item.userId}
							type="fan"
							data={item}
						/>
					);
				})}
			</div>
		);
	}
}

export default HisFans;