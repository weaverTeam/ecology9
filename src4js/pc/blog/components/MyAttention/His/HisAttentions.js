import AttentionApi from '../../../apis/myAttention';
import { message } from 'antd';
import Attention from '../../MyBlog/RightContainer/Attention/Attention';

class HisAttentions extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			attentionList: []
		};
	}
	componentDidMount() {
		AttentionApi.getMyBlogAttention({blogId: this.props.userId, groupid: 'all'}).then((result) => {
			this.setState({
				attentionList: result.attentionList || []
			});
		}).catch(error => message.error(error));
	}
	render() {
		let list = this.state.attentionList;
		return (
			<div className="hisAttention">
				{list.map((item) => {
					item.attentionStatus = undefined;
					item.attentionMeStatus = undefined;
					return (
						<Attention
							key={item.userId}
							type="attention"
							data={item}
						/>
					);
				})}
			</div>
		);
	}
}

export default HisAttentions;