import AttentionApi from '../../../apis/myAttention';
import { message } from 'antd';
import WeiboListNR from '../../Component/WeiboListNoRedux';
import {WeaNewScrollPagination} from 'ecCom';
import WeaUtils from "../../Component/wea-utils"
const {DateFormat, DateAddition} = WeaUtils
import { Spin} from 'antd';

class HisWeibo extends React.Component {
	constructor(props) {
		super(props);
        this.getList = this.getList.bind(this);
        this.search = this.search.bind(this);
        this.getWeiboList = this.getWeiboList.bind(this);
        this.queryNext = this.queryNext.bind(this);
        this.setWeiboList = this.setWeiboList.bind(this);
        this.state = {
            records: {
                discussList: []
            },
            loading: true,
            ended: false
        };
        this.params = {searching: false};//搜索参数
        this.reset = false;//是否重新搜索
	}
	componentDidMount() {
	    this.search();
	}
	getList(params, callback) {
	    this.setState({
	        loading: true
        }, (obj) => {
        });
	    if (params.searching) {
            AttentionApi.getBlogViewBySearch(params).then((result) => {
                this.setState({
                    loading: false
                });
                callback(result);
            }).catch(error => message.error(error));
        } else {
            AttentionApi.getViewBlogList(params).then((result) => {
                // 避免切换过快时下方数据与右侧选中不一致
                if (params.userId != this.params.userId)
                    return;
                this.setState({
                    loading: false
                });
                callback(result);
            }).catch(error => message.error(error));
        }
    }
    search(params = {}) {
        this.refs.scroll && this.refs.scroll.scrollTop();
        if (params.searching)
            params.blogId = this.props.userId;
        else
            params.userId = this.props.userId;
        this.getWeiboList(params);
    }
    /**
     * 1. 参数中，固定部分如下：
     * {
     *  searching:搜索状态
     *  endDate:下一页的开始时间，searching为false时启用，翻页时自动拼接
     *  currentPage:下一页的页码，searching为true时启用，翻页时自动拼接
     *  ...
     * }
     * 其它传参(包括content)为外部调用时的传参。外部查询时需要直接调用getWeiboList方法，形如this.refs.weibos.getWeiboList(params)。
     * 调用时不传endDate与currentPage即可从第一页开始。
     * 2. 返回数据格式固定，数据格式为：
     * {
	 *  "isMood": "1",    --是否启用心情，
     *  "isManagerScore": "1"    --是否启用上机评分
     *  discussList: [
     *      {
     *          agrees:[],
     *          appItems:[],
     *          blogReplyList:[],
     *          content: 微博内容
     *          ...
     *          workdate:'2017-07-06'
     *      }
     *  ]
	 * }
     */
    getWeiboList(params = {}, reset = true) {
        this.reset = reset;
        this.params = params;
        this.getList(params, this.setWeiboList);
    }
    
    /**
     * 翻页方法
     * @param params
     */
    queryNext(params = this.params) {
        if (this.state.loading || this.state.ended) {
            return;
        }
        if (params.searching) {
            params.currentPage = (params.currentPage || 1) + 1;
        } else {
            let discussList = this.state.records.discussList || [];
            let lastDis = discussList.length > 0 ? discussList[discussList.length - 1]: {};
            params.endDate = DateFormat(DateAddition(new Date(lastDis.workdate), -1, "day"), "yyyy-MM-dd");
        }
        this.getWeiboList(params, false);
    }
    // 绑定this,处理数据拼接。
    setWeiboList(result) {
        result.__hasData = result.discussList ? !!result.discussList.length : null;
        if (!this.reset) {
            let discussList = this.state.records.discussList;
            discussList && (result.discussList = discussList.concat(result.discussList));
        } else
            this.reset = false;
        this.setState({
            records: result,
            ended: !result.__hasData
        });
    }
	render() {
	    let {records, loading, ended} = this.state;
	    let hasData = records && records.discussList && records.discussList.length > 0;
        let showContent = !this.state.loading || hasData;//避免刚进入时显示无数据页面。
		return (
            <WeaNewScrollPagination onScrollEnd={this.queryNext} ref="scroll">
                <div className="hisWeibo">
                    {showContent && <WeiboListNR
                        ref="weiboList"
                        userId={this.props.userId}
                        currentUserId={this.props.currentUserId}
                        hasImage={false}
                        records={records}
                    />}
                    {loading && <div className="pt20 align-center"><Spin /></div>}
                    {hasData && ended && <div className="endTip"><i className="icon-blog-ArrowheadUp" />没有微博了</div>}
                </div>
            </WeaNewScrollPagination>
		);
	}
}
HisWeibo.propTypes = {
    userId: React.PropTypes.string,//访问用户id
    currentUserId: React.PropTypes.string//登录用户id
}
export default HisWeibo;