import ReportList from '../../Component/ReportList/';
import * as ReportApi from '../../../apis/report';
import {REPORT_TYPE} from '../../Component/ReportList/Const';

class HisReport extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            loading: true,
            data: {}
        };
	}
    
    getReportTable(params = {}) {
        this.setState({
            loading: true
        });
        params.blogId = this.props.userId;
        ReportApi.getReportTable(params).then(result => {
            this.setState({
                loading: false,
                data: result.tableInfo
            });
        });
    }
	
	render() {
        let {loading, data} = this.state;
        let {title} = this.props;
		return (
		    <div className="hisReport">
                <ReportList
                    owner={{key:REPORT_TYPE.my, title: title}}
                    loading={loading}
                    data={data}
                    actions={this}
                />
            </div>
		);
	}
}

export default HisReport;