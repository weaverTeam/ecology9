import WeaCircleImg from '../Component/wea-circle-image';
import BlogUtils from '../MyBlog/LeftContainer/Utils';
import AttentionTool from './AttentionTool';
import { Tabs,message, Spin } from 'antd';
import TopSearchPanel from '../MyBlog/RightContainer/Weibo/TopSearchPanel';
import AttentionApi from '../../apis/myAttention';
import weaBlogDict from '../Component/wea-blog-dict';
import HisAttentions from './His/HisAttentions';
import HisFans from './His/HisFans';
import HisWeibo from './His/HisWeibo';
import HisReport from './His/HisReport';
import WeaUtils from '../Component/wea-utils/';
import {WeaNewScroll, WeaAlertPage } from 'ecCom';

const RangeStar = BlogUtils.RangeStar;
const TabPane = Tabs.TabPane;
const innerHref = weaBlogDict.href;

class HisWeiboContainer extends React.Component {
	constructor(props) {
		super(props);
		let funcs = ['search', 'switchTab', 'getBlogInfo'];
		funcs.forEach((func) => this[func] = this[func].bind(this));
		this.state = {
			showSearch: true,
			blogInfo: undefined,
			index: undefined
		};
	}
	componentDidMount() {
		this.getBlogInfo();
	}
	componentWillReceiveProps(nextProps) {
		this.setState({
			blogInfo:undefined,
			index: undefined
		}, () => this.getBlogInfoByUserId(nextProps.userId));
	}
	getBlogInfo() {
		this.getBlogInfoByUserId(this.props.userId);
	}
	getBlogInfoByUserId(userId) {
		if (!userId) return;
		AttentionApi.getBlogPersonalInfo({blogId: userId}).then((result) => {
			this.setState({blogInfo:result});
            if (this.props.changeTitle) {
                document.title = result.username + "的微博";
            }
            // 有权限展示时才显示指数
            if (result.viewStatus > 0) {
                AttentionApi.getBlogWorkIndex({blogId: userId}).then((result) => {
                    this.setState({index: result})
                }).catch(error => message.error(error));
            }
		}).catch(error => message.error(error));
	}
	search (params){
	    params.startDate = WeaUtils.DateFormat(params.startDate, 'yyyy-MM-dd');
        params.endDate = WeaUtils.DateFormat(params.endDate, 'yyyy-MM-dd');
        params.searching = true;
        this.refs.weibo.search(params);
	}
	
	switchTab(key) {
	    let showSearch = key === '1';
		this.setState({
			showSearch: showSearch
		});
        if (showSearch) {
            this.refs.weibo.search({searching: false});
        }
	}
	render() {
		let blogInfo = this.state.blogInfo;
		if (!blogInfo)
			return (
				<div className="align-center top40">
					<Spin size="large"></Spin>
				</div>);
		else if (blogInfo.viewStatus == -1)
			return (
            <WeaAlertPage>
				<div className="color-black">
					你当前不具有对
					<a href={innerHref.user(blogInfo.userId)} target="_blank">{blogInfo.username}</a>
					工作微博的查看权限，请联系对方将微博分享给你！
				</div>
            </WeaAlertPage>
			);
		else if (blogInfo.viewStatus == 0)
			return (
                <WeaAlertPage>
                    <div className="color-black">
                        你当前不具有对
                        <a href={innerHref.user(blogInfo.userId)} target="_blank">{blogInfo.username}</a>
                        工作微博的查看权限，请联系对方将微博分享给你，或向他发关注申请！
                    </div>
                    <div className="msgTool">
                        <AttentionTool attentionStatus={blogInfo.attentionStatus} islower={blogInfo.islower} userId={blogInfo.userId} callback={this.getBlogInfo}/>
                    </div>
                </WeaAlertPage>
			);
        
		let {mood, schedule, work} = this.state.index || {};
		let he = blogInfo.sex === '0' ? '他' : '她';
        let isMe = blogInfo.currentUserId == this.props.userId;
        if (isMe)
            he = '我';
		return (
			<div className="hisWeiboContainer">
				<div>
					<div className="panel align-center">
						<div className="verLine" />
						<div className="user">
							<div className="userImg">
								<a href={innerHref.user(blogInfo.userId)} target="_blank">
								{WeaCircleImg({src:blogInfo.imageUrl,name:blogInfo.username, diameter:72})}
								</a>
							</div>
							<div className="userInfo text-elli">
								<div><a href={innerHref.user(blogInfo.userId)} target="_blank">{blogInfo.username}</a></div>
								<div className="deptName"><a href={innerHref.dept(blogInfo.deptId)} target="_blank">{blogInfo.deptName}</a></div>
                                {isMe||<AttentionTool attentionStatus={blogInfo.attentionStatus} islower={blogInfo.islower} userId={blogInfo.userId} callback={this.getBlogInfo}/>}
							</div>
						</div>
					</div>
					<div className="panel align-center">
						<div className="verLine" />
						{this.state.index ?
							<div className="index">
								{work && <div><RangeStar name={work.name} tip={work.workIndexTitle} value={work.workIndex}/></div>}
								{mood && <div><RangeStar name={mood.name} tip={mood.moodIndexTitle} value={mood.moodIndex}/></div>}
								{schedule && <div><RangeStar name={schedule.name} tip={schedule.scheduleTitle} value={schedule.scheduleIndex}/></div>}
							</div> :
							<div className="index" style={{textAlign: 'center'}}><Spin size="large" /></div>
						}
					</div>
					<div className="panel align-center">
						<div className="contact">
							<div>上级：<a className="info" href={innerHref.user(blogInfo.managerId)} target="_blank">{blogInfo.managerName}</a></div>
							<div>电话：<span className="info">{blogInfo.telephone}</span></div>
							<div>手机：<span className="info">{blogInfo.mobilePhone}</span></div>
						</div>
					</div>
					<div className="clear-both" />
				</div>
				<div className="downTab">
					<Tabs animated={false} defaultActiveKey="1" onChange={this.switchTab} size="small"
						  tabBarExtraContent={this.state.showSearch && <TopSearchPanel requestData={this.search}/>}
					>
						<TabPane tab={`${he}的微博(${blogInfo.blogCount})`} key="1">
                            <HisWeibo ref="weibo" userId={blogInfo.userId} currentUserId={blogInfo.currentUserId} key={blogInfo.userId}/>
						</TabPane>
						<TabPane tab={`${he}的报表`} key="2">
                            <HisReport userId={blogInfo.userId} title={`${he}的报表`}/>
						</TabPane>
						<TabPane tab={`${he}的关注(${blogInfo.myAttentionCount})`} key="3">
                            <WeaNewScroll>
                                <HisAttentions userId={blogInfo.userId}/>
                            </WeaNewScroll>
						</TabPane>
						<TabPane tab={`${he}的粉丝(${blogInfo.attentionMeCount})`} key="4">
                            <WeaNewScroll>
							    <HisFans userId={blogInfo.userId}/>
                            </WeaNewScroll>
						</TabPane>
					</Tabs>
				</div>
			</div>
		);
	}
};

export default HisWeiboContainer;