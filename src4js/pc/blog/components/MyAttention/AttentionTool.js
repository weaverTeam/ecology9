import * as MyAttentionAction from '../../actions/myAttention';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

class AttentionTool extends React.Component {
	constructor(props) {
		super(props);
		this.apply = this.apply.bind(this);
	}
	
	apply(event) {
		event.stopPropagation();
		let {attentionStatus, islower, userId} = this.props;
		this.props.actions.editAttention({attentionStatus, islower, userId}, this.props.callback);
	}
	
	render() {
		/**
		 *
		 1表示单向关注，按钮显示 取消关注。
		 2表示双向关注，按钮显示 取消关注。
		 3表示没有关注关系，且不是上下级关系。 按钮显示 申请关注。
		 4表示没有关注关系，且不是上下级关系，且已发送申请。按钮显示 已发送申请。
		 5表示没有关注关系，且是上下级关系。按钮显示 添加关注。
		 */
		let text="取消关注";
		let func = this.apply;
		let className = "icon-blog-CancelConcern";
		switch(this.props.attentionStatus) {
			case "3": text="申请关注";className = "icon-blog-AddTo";break;
			case "4": text="已申请";func = undefined;className = "icon-blog-AlreadyConcern";break;
			case "5": text="添加关注";className = "icon-blog-AddTo";break;
			default: break;
		}
		return (
			<a className="tool" onClick={func}><span className={className} />{text}</a>
		);
	}
}

AttentionTool.propTypes = {
	attentionStatus: React.PropTypes.string,//当前关注状态
	userId: React.PropTypes.string,//人员id
	islower: React.PropTypes.string,//后台需要的参数
	callback: React.PropTypes.func//用户点击按钮、执行成功后的回调。
};

const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(MyAttentionAction, dispatch)
	}
}
export default connect(() => new Object(), mapDispatchToProps)(AttentionTool);