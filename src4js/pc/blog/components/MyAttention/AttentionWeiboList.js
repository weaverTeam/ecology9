import TopSearchPanel from '../MyBlog/RightContainer/Weibo/TopSearchPanel';
import {Collapse, Spin} from 'antd';
import WeaUtils from '../Component/wea-utils/';
import * as MyAttentionAction from '../../actions/myAttention';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {WeaNewScroll } from 'ecCom';
import {WeaNewScrollPagination} from 'ecCom';
import WeiboListNR from '../Component/WeiboListNoRedux';

const Panel = Collapse.Panel;
window.WeaUtils = WeaUtils;

class AttentionWeiboList extends React.Component {
    constructor(props) {
        super(props);
        this.getWeiboList = this.getWeiboList.bind(this);
        this.search = this.search.bind(this);
        this.queryNext = this.queryNext.bind(this);
        this.params = {};
        this.state = {
            reading: true
        };
    }
    componentDidMount() {
        this.props.actions.getAttentionCountInfos();
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.myCountList.length > 0)
            this.getWeiboList([0], nextProps.myCountList);
    }
    search(params) {
        this.props.actions.clearRightList();
        params.startDate = WeaUtils.DateFormat(params.startDate, 'yyyy-MM-dd');
        params.endDate = WeaUtils.DateFormat(params.endDate, 'yyyy-MM-dd');
        if (!params.content) {
            params.searching = false;
            this.params = params;
            this.props.actions.getAttentionCountInfos(params);
        }
        else {
            params.groupId="all";
            params.currentPage=1;
            params.searching = true;
            this.params = params;
            this.props.actions.searchWeiboList(params);
        }
    }
    queryNext() {
        let ended = !this.props.searchWeiboList.__hasData;
        if (!this.props.countLoaded || ended) return;
        this.params.currentPage++;
        this.props.actions.searchWeiboList(this.params);
    }
    
    
    getWeiboList(args, myCountList = this.props.myCountList) {
        if (!args || !myCountList)
            return;
        args.forEach((arg) => {
            let cdata = myCountList[arg];
            if (cdata && !cdata.loaded) {
                this.props.actions.getAttentionWorkDateList({workDate:cdata.workdate}, arg);
            }
        });
    }
    
    remind(workdate) {
        this.props.actions.sendRemindUnsubmitAll({workdate: workdate});
    }
    
    getSearchWeiboRender() {
        let currentUserId = this.props.userInfo.id;
        let list = this.props.searchWeiboList;
        let ended = !list.__hasData;
        let hasData = list.discussList && list.discussList.length > 0;
        return (
            <div className="weiboDate">
                <WeaNewScrollPagination onScrollEnd={this.queryNext}>
                    {(this.props.countLoaded || hasData) && <WeiboListNR
                        currentUserId={currentUserId}
                        records={this.props.searchWeiboList}
                        hasImage={false}
                        hasScroll={false}
                    />}
                    {this.props.countLoaded || <div className="pt20 align-center"><Spin /></div>}
                    {hasData && ended && <div className="endTip"><i className="icon-blog-ArrowheadUp" />没有微博了</div>}
                </WeaNewScrollPagination>
            </div>
        );
    }
    
    render() {
        let currentUserId = this.props.userInfo.id;
        return (
            <div className="h100">
                <div className="weiboDateTop">
                    <TopSearchPanel ref="TopSearchPanel" requestData={this.search}/>
                </div>
                {this.params.searching ? this.getSearchWeiboRender() :
                    (this.props.countLoaded ? <div className="weiboDate panelWeibo">
                        <WeaNewScroll>
                            <Collapse defaultActiveKey={['0']} onChange={this.getWeiboList}>
                                {!!this.props.myCountList && this.props.myCountList.map((item,i) => {
                                    let remind = this.remind.bind(this, item.workdate);
                                    return (
                                        <Panel header={getPanelHeader(item, remind)} key={i}>
                                            {(!item.loaded) ? <div className="ml20 mt10 mb10"><Spin size="large" /></div>:
                                                <WeiboListNR
                                                    currentUserId={currentUserId}
                                                    records={item.data}
                                                    hasImage={false}
                                                    hasScroll={false}
                                                    blogType={i + 1}
                                                />
                                            }
                                        </Panel>
                                    );
                                })}
                            </Collapse>
                        </WeaNewScroll>
                    </div> :
                    <div className="weiboDate">
                        <div className="spin">
                            <Spin size="large"/>
                        </div>
                    </div>
                    )
                }
            </div>
        );
    }
}

let getPanelHeader = function(item, remind) {
    let {workdate, unsubmit, isCanRemind} = item;
    let clickFunc = (event) => {
        event.stopPropagation();
        remind();
    };
    return (
        <span className="panelHeader">
            <span className="curdate">{(WeaUtils.DateFormat(new Date(), 'yyyy-MM-dd') == workdate) && '今天'}</span>
            {WeaUtils.DateFormat(workdate,'M月dd日')}
            <span className="ml5">{WeaUtils.DateFormat(workdate,'周E')}</span>
            {unsubmit > 0 && (<span className="orange pos-rel"><span className="dot">•</span><span className="dot-ml">{`${unsubmit}人未提交`}</span></span>)}
            <span className="icons">
                    {(isCanRemind == 1)&&<span className='icon-blog-remind' onClick={clickFunc}/>}
                <span className='icon-blog-down'/>
			</span>
        </span>
    );
};

const mapStateToProps = state => {
    const { myCountList,userInfo,searchWeiboList,countLoaded } = state.blogMyAttention;
    return { myCountList,userInfo,searchWeiboList,countLoaded };
};

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(MyAttentionAction, dispatch)
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(AttentionWeiboList);