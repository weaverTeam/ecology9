import PropTypes from 'react-router/lib/PropTypes'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Button, Tabs } from 'antd'
import * as MyAttentionAction from '../../actions/myAttention'
import { WeaErrorPage, WeaTools } from 'ecCom'
import {
    WeaTop,
    WeaTab,
    WeaLeftRightLayout,
} from 'ecCom'
import LeftTab from './LeftTab';
import AttentionWeiboList from './AttentionWeiboList';
import * as AttCon from '../../constants/AttentionConstant';
import HisWeiboContainer from './HisWeiboContainer';
//import { } from '../../coms/index'

import WeaBlogDict from '../Component/wea-blog-dict';

import '../../css/myAttention.less';


const TabPane = Tabs.TabPane;
const innerHref = WeaBlogDict.href;
class MyAttention extends React.Component {
	static contextTypes = {
		router: PropTypes.routerShape
	}
	constructor(props) {
		super(props);
	}
	componentDidMount() {
	    // 保证刷新时进入列表，而不是人员页。
		if (this.props.rightType == AttCon.RIGHT_TYPE_WEIBO && this.props.params && this.props.params.userId) {
		    window.location.href = innerHref.viewAttentionList();
        }
	    //一些初始化请求
		const { actions } = this.props;
        actions.getCurrentUser();
	}
    componentWillUnmount() {
        this.props.actions.resetRight();
    }
	render() {
		const { loading, title } = this.props;
		return (
			<div className='wea-blog-my-attention'>
				<WeaTop
	              	title={title}
	              	loading={loading}
	              	icon={<span className='icon-coms-blog'/>}
                    iconBgcolor='#6d3cf7'
	              	buttons={[]}
	              	>
					<WeaLeftRightLayout defaultShowLeft={true} leftCom={<LeftTab/>}>
						<div className="wea-right-container">
                            {this.props.children}
						</div>
	              	</WeaLeftRightLayout>
              	</WeaTop>
            </div>
		)
	}

}

//组件检错机制
class MyErrorHandler extends React.Component {
	render() {
		const hasErrorMsg = this.props.error && this.props.error !== "";
		return(
			<WeaErrorPage msg={ hasErrorMsg ? this.props.error : "对不起，该页面异常，请联系管理员！" } />
		);
	}
}

MyAttention = WeaTools.tryCatch( React, MyErrorHandler, { error: "" })(MyAttention);

//form 表单与 redux 双向绑定
//MyAttention = createForm({
//	onFieldsChange(props, fields) {
//		props.actions.saveFields({ ...props.fields, ...fields });
//	},
//	mapPropsToFields(props) {
//		return props.fields;
//	}
//})(MyAttention);


// 把 state map 到组件的 props 上
const mapStateToProps = state => {
	const { loading, title, rightType, userId } = state.blogMyAttention;
	return { loading, title, rightType, userId }
}

// 把 dispatch map 到组件的 props 上
const mapDispatchToProps = dispatch => {
	return {
		actions: bindActionCreators(MyAttentionAction, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAttention);