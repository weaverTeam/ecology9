import {Row,Col,Switch,Button,Modal,Input,Form,InputNumber,Spin} from 'antd';
import { WeaBrowser,WeaInput,WeaDatePicker,WeaSelect } from 'ecCom';
import {WeaTable} from 'comsRedux'
//import WeaCkEditor from "../../Component/wea-ckeditor/CKEAppend"
const InputGroup = Input.Group;
const FormItem = Form.Item;
const confirm = Modal.confirm;
let __this = null;
export default class MouldSetting extends React.Component {
    constructor(props) {
        super(props);
        __this = this;
        this.state = {
            checked: props.checked ? props.checked : true,
            showModal:props.showModal ? props.showModal :false,
            showtotalmodal:props.showtotalmodal ? props.showtotalmodal :false,
            showDrop:true,
            color:false,
            smallModal:false,
        }
        this.CKE_ID = "wea_ckeditor_weibo_setting"
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.showModal !== nextProps.showModal) {
            this.setState({showModal: nextProps.showModal});
        }
        if(this.props.checked !== nextProps.checked){
             this.setState({checked: nextProps.checked});
        }
        if(this.props.showtotalmodal !== nextProps.showtotalmodal){
             this.setState({showtotalmodal: nextProps.showtotalmodal});
        }
    }

    render(){
        const {style,sessionkey,data,moudleShareSessionkey,all,selectedRowKeys,loading,spinning,realId} = this.props;
        const doc = data?data.BlogTemplateBean.tempContent:'';
        const isSystem = data?data.BlogTemplateBean.isSystem:'';
        const {checked,showModal,smallModal,showtotalmodal,showDrop} = this.state;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        const {getFieldProps,getFieldsValue} = all.form;
        const footer=
            <Button key="back" type="ghost" size="large" onClick={()=>{this.props.changeState(false);}}>关 闭</Button>
        const footert=
            <Button key="back" type="ghost" size="large" onClick={()=>{this.props.changeStatet(false)}}>返 回</Button>    
        const footerr = [
            <Button key="save" type="primary" size="large" onClick={()=>{this.addShare();this.setState({smallModal:false});all.form.resetFields()}}>保 存</Button>
            ];
        let show = "";
        let bk = "";
        if(selectedRowKeys.length!==0){
            if(this.state.color){
                 show = '#5cb4ff';
            }else{
                 show = '#34a2ff';
            }
        }else{
             show = '#d9d9d9'
        }
        return(
            
            <Row className="" style={{display:style?style.display:'block'}}>
                <WeaTable
                loading={loading}
                hasOrder={true}
                needScroll={true}
                sessionkey={sessionkey}
                onOperatesClick={this.props.onOperatesClickt.bind(this)}
                />
                <Modal
                    title={'查看模板'}
                    wrapClassName="vertical-center-modal blogsettingmodal-vertical-center"
                    visible={showModal}
                    footer={footer}
                    className={`blogsettingmodal preview`}
                    onCancel={()=>{this.props.changeState(false)}}
                >
                <div className="icon-circle-base blogsettingmodal-modal" style={{background:'#6d3cf7'}}>
                    <span className='icon-coms-blog'/>
                </div>

                <Row className="" style={{marginBottom:'10px'}}>
                    
                    <Col span="7" style={{textAlign:"right",height:'28px',marginBottom:'24px'}}>
                        <div style={{lineHeight:'28px',paddingRight:'25px'}} >模版名称：</div>
                    </Col>
                    <Col span="11" style={{height:'28px',backgroundColor:'rgb(248,248,248)',marginBottom:'24px',border:'1px solid #e0e0e0'}}>
                         <span className="text" style={{lineHeight:'28px',paddingLeft:'10px'}}>{data?data.BlogTemplateBean.tempName:''}</span>
                    </Col>
                    <Col span="7" style={{textAlign:"right",height:'28px',marginBottom:'24px'}}>
                        <div style={{lineHeight:'28px',paddingRight:'25px'}}>模板描述：</div>
                    </Col>
                    <Col span="11" style={{height:'28px',backgroundColor:'rgb(248,248,248)',marginBottom:'24px',border:'1px solid #e0e0e0'}}>
                        <span className="text" style={{lineHeight:'28px',paddingLeft:'10px'}}>{data?data.BlogTemplateBean.tempDesc:''}</span>
                    </Col>
                    <Col span="7" style={{textAlign:"right",height:'28px',marginBottom:'24px'}}>
                        <div style={{lineHeight:'28px',paddingRight:'25px'}}>模板类型：</div>
                    </Col>
                    <Col span="11" style={{height:'28px',backgroundColor:'rgb(248,248,248)',marginBottom:'24px',border:'1px solid #e0e0e0'}}>
                        <span className="text" style={{lineHeight:'28px',paddingLeft:'10px'}}>{isSystem=='0'?'个人模板':'系统模板'}</span>
                    </Col>
                    <Col span="7" style={{textAlign:"right",height:'28px',marginBottom:'24px'}}>
                        <div style={{lineHeight:'28px',paddingRight:'25px'}}>状态：</div>
                    </Col>
                    <Col span="11" style={{textAlign:"left",height:'28px',marginBottom:'24px'}}>
                        <Switch style={{marginTop:'5px'}} checked={data?data.BlogTemplateBean.isUsed:''} onChange={this.changeSwitch.bind(this)} />
                    </Col>
                    <Col span="7" style={{textAlign:"right",height:'28px'}}>
                        <div style={{lineHeight:'28px',paddingRight:'25px'}}>模版内容：</div>
                    </Col>
                    <Col span="14" style={{backgroundColor:'rgb(248,248,248)',height:'300px',overflow:'auto',border:'1px solid #e0e0e0'}}>
                        <div dangerouslySetInnerHTML={{__html: data?data.BlogTemplateBean.tempContent:''}}>
                    
                        </div>
                    </Col>
                </Row>

                </Modal>

                    <Modal
                        title={'添加共享设置'}
                        wrapClassName="vertical-center-modal"
                        visible={showtotalmodal}
                        footer={footert}
                        className={`blogModal shareModal`}
                        style={{height:'600px'}}
                        onCancel={()=>{this.props.changeStatet(false)}}
                    >

                    
                    <div className="icon-circle-base blogsettingmodal-modal" style={{background:'#6d3cf7'}}>
                        <span className='icon-coms-blog' />
                    </div>
                    <Row className="" style={{borderBottom:'1px solid #e9e9e9'}}>
                        <Col span="20" style={{height:'32px'}}>
                            <div>共享条件</div>
                        </Col>
                        <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                            <i className='icon-coms-Add-to-hot' style={{fontSize:'20px',marginRight:'10px',color:'#34a2ff',cursor:'pointer'}} onClick={()=>{this.setState({smallModal:true})}} />
                            <i className='icon-coms-form-delete-hot' style={{fontSize:'20px',marginRight:'10px',color:show,cursor:'pointer'}} onMouseEnter={()=>{this.setState({color:true})}} onMouseOut={()=>{this.setState({color:false})}} onClick={()=>{this.showConfirm()}} />
                            <i className={showDrop ? 'icon-blog-up2' : 'icon-blog-down2'} style={{cursor:'pointer',fontSize:'12px',position:'absolute',top:'4px'}} onClick={()=>this.setState({showDrop:!showDrop})}/>
                        </Col>
                    </Row>
                <Spin spinning={spinning}>
                    <Row className="" style={showDrop?{marginBottom:'10px'}:{height:0,overflow:'hidden'}}>
                        <WeaTable
                        loading={loading}
                        hasOrder={true}
                        needScroll={true}
                        sessionkey={moudleShareSessionkey}
                        />
                    </Row>
                </Spin>
                    </Modal>
                <Modal
                    title={'添加分享'}
                    wrapClassName="vertical-center-modal blogsettingmodal-vertical-center"
                    visible={smallModal}
                    footer={footerr}
                    className={`blogsettingsmallmodal`}
                    onCancel={()=>{this.setState({smallModal:false});all.form.resetFields()}}
                >
                <div className="icon-circle-base blogsettingmodal-modal" style={{background:'#6d3cf7'}}>
                    <i className='icon-coms-blog' />
                </div>
                               
                <Row className="" style={{marginBottom:'10px'}}>
                    <Form horizontal>
                        <FormItem
                          {...formItemLayout}
                          label="条件类型："
                        >
                          <WeaSelect disabled={true} viewAttr={2} options={[{key: '1', selected:true, showname: '人员'}]} />
                        </FormItem>
                        <FormItem
                          {...formItemLayout}
                          label="条件内容："
                        >
                            <WeaBrowser {...getFieldProps('relatedshareid3')} type={17} isSingle={false} viewAttr={2} inputStyle = {{display:'block',width:'100%'}} />
                        </FormItem>
                        
                       
                    </Form>
                </Row>    

                </Modal>
                
             </Row>
        )
    }
    addShare(){
        const {actions,all,realId} = this.props;
        const {getFieldsValue} = all.form;
        const relatedshareid = all.orderFields.relatedshareid3?all.orderFields.relatedshareid3.value:''
        actions.addUserTempShare({relatedshareid:relatedshareid,tempid:realId,shareid:0,sharetype:1,});
    }
    changeSwitch(checked){
        const {actions} = this.props;
        this.setState({checked:checked});
    }
    showConfirm(e) {
        const {selectedRowKeys,actions,realId} = this.props;
        confirm({
            title: '您是否确认要删除这项内容',
            content: '',
            onOk() {
                actions.deleteUserTempShare({shareIds:selectedRowKeys,tempida:realId})
            },
            onCancel() {
                
            },
        });
    }
   

}
window.detectRadioStatus = function(a){
    const { actions } = __this.props;
        __this.props.changeStater(a.value);
    }
