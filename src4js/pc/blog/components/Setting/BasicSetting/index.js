import {Row,Col,Switch} from 'antd';

export default class BasicSetting extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            checked: props.checked ? props.checked : false,

        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.checked !== nextProps.checked) {
            this.setState({checked: nextProps.checked});
        }
    }

	render(){
		const {style} = this.props;
		const {checked} = this.state;
		return(
			<Row className="" style={{borderBottom:'1px solid #e9e9e9',display:style?style.display:'block'}}>
                    <Col span="5" style={{textAlign:"center",height:'32px'}}>
                        <div style={{lineHeight:'32px'}}>接收关注申请</div>
                    </Col>
                    <Col span="19" style={{textAlign:"left",backgroundColor:'rgb(248,248,248)',height:'32px'}}>
	                    <Switch style={{margin:'5px 0 0 10px'}} checked={checked} onChange={this.props.onChange} />
	                </Col>
            </Row>
				
		)
	}

}

