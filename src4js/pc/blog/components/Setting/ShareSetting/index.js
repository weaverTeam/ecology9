import {Row,Col,Switch,Input,Icon,Modal,Button,Form,InputNumber} from 'antd';
const InputGroup = Input.Group;
const confirm = Modal.confirm;
const FormItem = Form.Item;
import objectAssign from 'object-assign'
import { WeaBrowser,WeaInput,WeaDatePicker,WeaSelect,WeaScope} from 'ecCom';
import {WeaTable} from 'comsRedux'
let __this = null;


class ShareSetting extends React.Component {
    constructor(props) {
        super(props);
        __this = this;
        this.state = {
            showDefaultDrop:true,
            showDrop:true,
            showModal:this.props.states?this.props.states.showModals:false,
            selectsharetype:this.props.shareinfo.info?this.props.shareinfo.info.type:1,
            editid:this.props.states?this.props.states.editid:false,  
            shareId:this.props.states?this.props.states.shareId:'',
            color:false,
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.shareinfo !== nextProps.shareinfo) {
            this.setState({selectsharetype: nextProps.shareinfo.info.type});
        }
        if (this.props.states !== nextProps.states) {

            this.setState({showModal: nextProps.states.showModals,editid: nextProps.states.editid,shareId: nextProps.states.shareId,selectsharetype:nextProps.states.selectsharetype});
        }
    }

    render(){
        const {style,needTigger,sharinginfos,sessionkey,actions,shareinfo,all,selectedRowKeys,loading} = this.props;
        const {showDrop,showDefaultDrop,showModal,selectsharetype,editid,shareId} = this.state;
        const {getFieldProps,getFieldsValue} = all.form;
        let values = getFieldsValue();
        values = objectAssign({},{
            sharetype:selectsharetype,
            shareid:shareId||0,
            relatedshareid:all.orderFields.relatedshareid&&all.orderFields.relatedshareid.value||'',
            seclevel:all.orderFields.seclevel&&all.orderFields.seclevel.value||10,
            seclevelMax:all.orderFields.seclevelMax&&all.orderFields.seclevelMax.value||100,
            canViewMinTime:all.orderFields.canViewMinTime&&all.orderFields.canViewMinTime.value||''
        });
        let managers = sharinginfos.managers ? sharinginfos.managers.contentName : [];
        let specify = sharinginfos.specify ? sharinginfos.specify.contentName : [];
        const footer = [
            <Button key="save" type="primary" size="large" onClick={()=>{__this.handleSubmit();}}>保 存</Button>
            ];
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };


        let type = [{key: '1', selected: true, showname: '人员'},
                    {key: '2', selected: false, showname: '分部'},
                    {key: '3', selected: false, showname: '部门'},
                    {key: '4', selected: false, showname: '角色'},
                    {key: '5', selected: false, showname: '所有人'},
                    {key: '7', selected: false, showname: '申请人'}
                   ];
        const items = false;
        let show = "";
        let bk = "";
        if(selectedRowKeys.length!==0){
            if(this.state.color){
                 show = '#5cb4ff';
            }else{
                 show = '#34a2ff';
            }
        }else{
             show = '#d9d9d9'
        }
        return(
            <Row style={{margin:'20px',display:style?style.display:'block'}}>
                <Row className="" style={{borderBottom:'1px solid #e9e9e9',marginBottom:'20'}}>
                    <Col span="20" style={{height:'32px'}}>
                        默认分享
                    </Col>
                    <Col span="4" style={{textAlign:"right",paddingRight:"1px",fontSize:12}}>
                        <i className={showDefaultDrop ? 'icon-blog-up2' : 'icon-blog-down2'} style={{cursor:'pointer'}} onClick={()=>this.setState({showDefaultDrop:!showDefaultDrop})}/>
                    </Col>
                </Row>
                
                <Row className="defaultShare" style={showDefaultDrop?{marginBottom:'10px'}:{height:0,overflow:'hidden'}}>
                    {managers.length!=0?
                        <Col span="8" style={{height:'32px'}}>
                            <p style={{textAlign:'right',lineHeight:'32px',paddingRight:'20px'}}>上级：</p>
                        </Col>
                        :''}
                    {managers.length!=0?
                        <Col span="10" style={{height:'32px'}}>
                            <WeaBrowser type={17} isSingle={false} replaceDatas={managers} hasBorder viewAttr={1}  />
                        </Col>
                        :''}
                    {specify.length!=0?
                        <Col span="8" style={{height:'32px',marginTop:'20px'}}>
                            <p style={{textAlign:'right',lineHeight:'32px',paddingRight:'20px'}}>指定共享人：</p>
                        </Col>
                        :''}
                    {specify.length!=0?
                        <Col span="10" style={{height:'32px',marginTop:'20px'}}>
                            <WeaBrowser type={17} isSingle={false} replaceDatas={specify} hasBorder viewAttr={1}  />
                        </Col>
                        :''}
                </Row>
                  
                <Row className="" style={{borderBottom:'1px solid #e9e9e9'}}>
                    <Col span="20" style={{height:'32px'}}>
                        <div>我的分享设置</div>
                    </Col>
                    <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
                        <i className='icon-coms-Add-to-hot' style={{fontSize:'20px',marginRight:'10px',color:'#34a2ff',cursor:'pointer'}} onClick={()=>{this.setState({showModal:true,editid:false,shareId:0,selectsharetype:1})}} />
                        <i className='icon-coms-form-delete-hot' style={{fontSize:'20px',marginRight:'11px',color:show,cursor:'pointer'}} onMouseEnter={()=>{this.setState({color:true})}} onMouseOut={()=>{this.setState({color:false})}} onClick={this.showConfirm.bind(this)} />
                        <i className={showDrop ? 'icon-blog-up2' : 'icon-blog-down2'} style={{cursor:'pointer',fontSize:'12px',position:'absolute',top:'4px',right:'1px'}} onClick={()=>this.setState({showDrop:!showDrop})}/>
                    </Col>
                </Row>
                <Row className="" style={showDrop?{marginBottom:'10px'}:{height:0,overflow:'hidden'}}>
                    <WeaTable 
                    hasOrder={true}
                    needScroll={true}
                    loading={loading}
                    sessionkey={sessionkey}
                    onOperatesClick={this.onOperatesClick2.bind(this)}
                    />
                </Row>
                <Modal
                    title={`${editid?'编辑分享':'添加分享'}`}
                    wrapClassName="vertical-center-modal blogsettingmodal-vertical-center"
                    visible={showModal}
                    maskClosable={false}
                    footer={footer}
                    className={`blogsettingmodal`}
                    onCancel={()=>this.setState({showModal:false,editid:false,selectsharetype:1,shareId:'0'})}
                >
                <div className="icon-circle-base blogsettingmodal-modal"  style={{backgroundColor:'#6d3cf7'}}>
                    <span className='icon-coms-blog' />
                </div>
                               
                <Row className="" style={{marginBottom:'10px'}}>
                    <Form horizontal>
                        <FormItem
                          {...formItemLayout}
                          label="条件类型："
                        >
                          <WeaSelect disabled={editid} onChange={this.changeSelect.bind(this)} viewAttr={2} value={`${selectsharetype}`} options={type} />
                        </FormItem>
                        {selectsharetype&&selectsharetype!='5'?
                        <FormItem
                          {...formItemLayout}
                          label="条件内容："
                        >
                        {selectsharetype==1?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })} replaceDatas={editid?shareinfo.browserInfos:[]} type={17} isSingle={false} viewAttr={2} inputStyle = {{display:selectsharetype==1?'block':'none',width:'100%'}} title="多人力资源" />
                        :selectsharetype==2?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })} replaceDatas={editid?shareinfo.browserInfos:[]} type={194} isSingle={false} viewAttr={2} inputStyle = {{display:selectsharetype==2?'block':'none',width:'100%'}} linkUrl="/hrm/company/HrmSubCompanyDsp.jsp?id=" title="多分部" tabs={[{key:'1',name:'按列表',selected:false,dataParams:{list:1}},{key:'2',name:'按组织架构',selected:false}]} isMultCheckbox={false} hasAdvanceSerach={true} hasAddBtn={false}/>
                        :selectsharetype==3?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })} replaceDatas={editid?shareinfo.browserInfos:[]} type={57} isSingle={false} viewAttr={2} inputStyle = {{display:selectsharetype==3?'block':'none',width:'100%'}} linkUrl="/hrm/company/HrmDepartmentDsp.jsp?id=" title="多部门" tabs={[{key:'1',name:'按列表',selected:false,dataParams:{list:1}},{key:'2',name:'按组织架构',selected:false}]} isMultCheckbox={false} hasAdvanceSerach={true} hasAddBtn={false}/>
                        :selectsharetype==4?
                            <WeaBrowser {...getFieldProps('relatedshareid', { initialValue: '' })} replaceDatas={editid?shareinfo.browserInfos:[]} type={65} isSingle={false} viewAttr={2} inputStyle = {{display:selectsharetype==4?'block':'none',width:'100%'}} linkUrl="/hrm/roles/HrmRolesEdit.jsp?id=" title="多角色" />
                        :selectsharetype==7?
                             <WeaBrowser {...getFieldProps('relatedshareid', { rules: [
                                                        { validator: __this.checkPass },
                                                      ], })}
                                         replaceDatas={editid?shareinfo.browserInfos:[]} type={17} isSingle={false} viewAttr={2} inputStyle = {{display:selectsharetype==7?'block':'none',width:'100%'}} title="多人力资源" />
                        :''}
                        </FormItem>
                        :''}
                        {selectsharetype && selectsharetype!='1' && selectsharetype!='7'?
                        <FormItem {...formItemLayout} label="安全级别：" style={{ marginTop: 24 }}>
                            <InputGroup>
                                <Col span="6">
                                  <InputNumber  {...getFieldProps('seclevel', { initialValue: editid?(shareinfo.info&&shareinfo.info.seclevel):'10' })} />
                                </Col>
                                <Col span="2">
                                  <p className="ant-form-split">-</p>
                                </Col>
                                <Col span="6">
                                  <InputNumber  {...getFieldProps('seclevelMax', { initialValue: editid?(shareinfo.info&&shareinfo.info.seclevelMax):'100' })} />
                                </Col>

                            </InputGroup>
                        </FormItem>
                        :''}
                        {selectsharetype && selectsharetype!=7?
                            <FormItem
                              {...formItemLayout}
                              label="起始可查看时间："
                            >
                                <WeaDatePicker {...getFieldProps('canViewMinTime',{initialValue:editid?(shareinfo.info&&shareinfo.info.canViewMinTime):''})}  viewAttr={2} />
                            </FormItem>
                            :''
                        }
                        
                    </Form>
                </Row>    

                </Modal>
            
            </Row>
        )
    }
    showConfirm(e,type=1) {
        const {selectedRowKeys,actions,shareinfo} = this.props;
        const {selectsharetype} = this.state;
        confirm({
            title: '您是否确认要删除这项内容',
            content: type==7?'删除申请人将清除这些人对您的关注':'',
            onOk() {
                if(typeof(e)=="string"){
                    actions.deleteBlogUserShare({shareIds:e});
                }else{
                    actions.deleteBlogUserShare({shareIds:selectedRowKeys})
                }
            },
            onCancel() {},
        });
    }
    checkPass(rule, value, callback) {
        const {all,shareinfo} = __this.props;
        let oldparam =  shareinfo.info.content.split(',');
        let isexist = value.split(',');
        let bool = isexist.some((key) => oldparam.indexOf(key) == -1);
        bool? callback('不允许添加人员，只允许删除人员'): callback();
    }
    handleSubmit() {

        const {all,actions} = this.props;
        const {validateFields,getFieldsValue,getFieldError} = all.form;
        const {selectsharetype,shareId} = this.state;
        let value = getFieldsValue();
        value = objectAssign({},{
            sharetype:selectsharetype,
            shareid:shareId||0,
            relatedshareid:all.orderFields.relatedshareid&&all.orderFields.relatedshareid.value||'',
            seclevel:all.orderFields.seclevel&&all.orderFields.seclevel.value||10,
            seclevelMax:all.orderFields.seclevelMax&&all.orderFields.seclevelMax.value||100,
            canViewMinTime:all.orderFields.canViewMinTime&&all.orderFields.canViewMinTime.value||''
        });
        validateFields((errors, values) => {
            if (!!errors && errors.relatedshareid) {
                return;
            }else{
                actions.saveBlogShareList(value);
                this.setState({showModal:false,shareId:0});
                all.form.resetFields()
            }
        });
    }

    onOperatesClick2(record,index,operate,flag){
        const {actions} = this.props;
        if(flag == 0){
            actions.getBlogUserShareById({shareId:record.realId,this:this});
        }
        if(flag == 1){
            this.showConfirm(record.realId,record.type);
        }
    }
    changeSelect(value){
        const {actions} = this.props;
        actions.saveOrderFields({relatedshareid:{name:'relatedshareid',value:''}})
        this.setState({
            selectsharetype:value
        })
    }

}
//编辑
// window.editUserShare = function(id){
//     const { actions } = __this.props;
//     __this.setState({showModal:true,editid:true});
//     actions.getBlogUserShareById({shareId:id});
// }
//删除个人微博共享信息
// window.delUserShare= function(value){
//     const { actions } = ___this.props;
//     actions.deleteBlogUserShare({shareIds:value});
// }

// BasicSetting = Form.create()(BasicSetting);
export default ShareSetting;