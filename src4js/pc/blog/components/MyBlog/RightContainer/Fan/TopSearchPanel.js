import React, {Component} from 'react'

import {WeaInputSearch} from 'ecCom'

class TopSearchPanel extends Component {
    constructor(props) {
        super(props)
        this.state = this.initData()
    }

    initData = () => {
        return {
            content: '',
        }
    }

    get data() {
        return this.state;
    }

    set clear(value) {
        this.setState(this.initData())
    }

    render() {
        let {content} = this.state
        return (
            <div className="wea-fans-top-search-panel">
                <div className="wea-ftsp-item wea-ftsp-search">
                    <WeaInputSearch
                        placeholder=""
                        value={content}
                        onSearchChange={this.onChange.bind(null, 'content')}
                        onSearch={this.onSearch}
                        style={{width: '125px'}}
                    />
                </div>
            </div>
        )
    }

    onChange = (field, value) => {
        if (!value) value = ""
        this.setState({[field]: (value.target ? value.target.value : value)})
    }

    onSearch = () => {
        const {content} = this.state
        this.requestData({searchKey: content})
    }

    requestData = (params) => {
        this.props.requestData(params)
    }

}

export default TopSearchPanel