import React, {Component} from 'react'

import TopSearchPanel from "./TopSearchPanel";

import WeaWhitePage from "../../../Component/wea-white-page"
import {Tabs} from 'antd'
const TabPane = Tabs.TabPane
import {WeaNewScroll} from 'ecCom';
import Fan from "./Fan"

class Fans extends Component {
    render() {
        let {height, data = [], actions} = this.props
        return (
            <Tabs activeKey="fans" size="small"
                  tabBarExtraContent={<TopSearchPanel requestData={this.getList}/>}
            >
                <TabPane tab="我的粉丝" key="fans">
                
                        <WeaNewScroll height={height - 61}>
                            <div className="wea-myBlog-fan">
                                {
                                    data.length ? (
                                        <div style={{padding: "0 8px"}}>
                                            {
                                                data.map(
                                                    item => {
                                                        return <Fan data={item} type="fan" actions={actions}/>
                                                    }
                                                )
                                            }
                                        </div>
                                    ) : <WeaWhitePage height={height}/>
                                }
                            </div>
                        </WeaNewScroll>
                    </TabPane>
                </Tabs>
        )
    }

    getList = (params) => {
        this.props.actions.getFanList(params)
    }
}

export default Fans