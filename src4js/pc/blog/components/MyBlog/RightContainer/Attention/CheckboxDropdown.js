import React, {Component} from 'react'

import {Dropdown, Button, Input, Checkbox, Tooltip, message} from 'antd'
import objectAssign from 'object-assign'
import {WeaInput} from 'ecCom';

import "../../../../css/myBlog/rightContainer/checkboxDropDown.less"

class CheckboxDropdown extends Component {
    constructor(props) {
        super(props)
        this.state = this.initData(props)
    }

    initData = (props) => {
        const {options, values} = props
        return {
            visible: false,

            name: "",
            creating: false,
            isInner: true,

            options: this.markOptions(options, values)
        }
    }

    markOptions = (options, values) => {
        if (options && (options.length > 0)) {
            let _options = objectAssign([], options)
            if (values && values.length > 0) {
                for (let i = 0; i < _options.length; i++) {
                    for (let j = 0; j < values.length; j++) {
                        if (_options[i].value == values[j]) {
                            _options[i].checked = true
                        }
                    }
                }
            }
            return _options
        } else {
            return []
        }
    }

    get value() {
        let {options} = this.state,
            values = []
        options.forEach(
            item => {
                if (item.checked) {
                    values.push(item.value)
                }
            }
        )
        return values
    }

    set visible(value) {
        this.setState({...this.initData(this.props), visible: value})
    }

    componentWillReceiveProps(props) {
        if(this.state.isInner){
            const {options, values} = props
            this.setState({options: this.markOptions(options, values)})
        }
    }

    render() {
        const {title, children, footer, style} = this.props,
            {visible, creating, options, name} = this.state
        return (
            <Dropdown
                trigger={['click']}
                visible={visible}
                onVisibleChange={this.changeVisible}
                overlay={
                    <ul className="wea-checkbox-dropdown" style={style || {}}>
                        <li className="wea-cd-item wea-cd-head">
                            {title}
                        </li>
                        <li className="wea-cd-item wea-cd-body">
                            <ul className="wea-cdb-checkboxes">
                                {
                                    options.map(
                                        item => {
                                            const {name, value, checked, isInconsistent} = item
                                            return (
                                                <li
                                                    className="wea-cdbc-option"
                                                    style={{width: (isInconsistent ? "100%" : "50%")}}
                                                >
                                                    <Checkbox
                                                        checked={checked}
                                                        onChange={this.changeOption.bind(this, item)}
                                                    >
                                                        <Tooltip
                                                            placement="bottomLeft"
                                                            title={name}
                                                            overlayStyle={{maxWidth: "200px"}}
                                                        >
                                                            <span>{name}</span>
                                                        </Tooltip>
                                                    </Checkbox>
                                                </li>
                                            )
                                        }
                                    )
                                }
                            </ul>
                            <div className="wea-cdb-create">
                                <div className="wea-cdbc-btn" style={{display: (creating ? "none" : "inline-block")}}
                                     onClick={() => this.changeCreateOption(true)}>
                                    <span className="wea-cdbcb-icon icon-blog-NewPacket"/>
                                    <span className="wea-cdbcb-text">新建分组</span>
                                </div>
                                <div className="wea-cdbc-bar" style={{display: (creating ? "inline-block" : "none")}}>
                                    <WeaInput
                                        style={{width: "164px", verticalAlign: "middle"}}
                                        value={name}
                                        onChange={this.changeName}
                                    />
                                    <Button
                                        style={{margin: "0 12px"}}
                                        type="primary"
                                        onClick={() => this.createOption(false)}
                                    >确定</Button>
                                    <Button onClick={() => this.changeCreateOption(false)}>取消</Button>
                                </div>
                            </div>
                        </li>
                        <li className="wea-cd-item wea-cd-footer">
                            {footer}
                        </li>
                    </ul>
                }
            >
                {children}
            </Dropdown>
        )
    }

    changeOption = (obj, e) => {
        const {options} = this.state,
            checked = e.target.checked
        if (obj.isInconsistent && checked) {
            this.setState({
                options: options.map(
                    item => {
                        if (item.isInconsistent) {
                            return {...item, checked: true}
                        } else {
                            return {...item, checked: false}
                        }
                    }
                )
            })
        } else {
            this.setState({
                options: options.map(
                    item => {
                        if (item.isInconsistent) {
                            return {...item, checked: false}
                        } else if (item.value == obj.value) {
                            return {...item, checked: checked}
                        } else {
                            return item
                        }
                    }
                )
            })
        }
    }

    createOption = () => {
        const {options, name} = this.state,
            {actions} = this.props
        if (!name) {
            message.error("分组名不能为空");
            return;
        }
        actions.createGroup(
            {groupName: name},
            result => {
                let {status, groupInfo} = result,
                    {groupId, groupName} = groupInfo
                if (status == "1") {
                    if (groupId) {
                        options.push({name: groupName, value: groupId})
                    }
                    this.setState({
                        isInner: false,
                        options: options.map(
                            item => {
                                if (item.isInconsistent) {
                                    return {...item, checked: false}
                                } else if (item.name == name) {
                                    return {...item, checked: true}
                                } else {
                                    return item
                                }
                            }
                        )
                    })
                    actions.getWeiboGroup()
                }
            }
        )
        this.changeCreateOption(false)
    }

    changeName = (value) => {
        this.setState({name: value})
    }

    changeVisible = (value) => {
        this.setState({visible: value})
    }

    changeCreateOption = (value) => {
        if (!value) this.setState({name: ""})
        this.setState({creating: value})
    }
}

export default CheckboxDropdown