import React, {Component} from 'react'

import {Select, Modal, Button, Tooltip} from 'antd'
const Option = Select.Option

import {WeaInputSearch} from 'ecCom'

import WeaSplitLine from "../../../Component/wea-split-line"

import CreateGroupModal from "./CreateGroupModal"

class TopSearchPanel extends Component {
    constructor(props) {
        super(props)
        this.state = this.initData(props)
    }

    initData = (props) => {
        return {
            groupId: props.defaultGroup.groupId || 'all',
            groupName: props.defaultGroup.groupName || '全部关注',
            content: '',
        }
    }

    get data() {
        return this.state;
    }

    render() {
        let {content, groupId, groupName} = this.state,
            {groupsBefore, groupsAfter} = this.props
        return (
            <div className="wea-fans-top-search-panel">
                <div className="wea-ftsp-item wea-ftsp-select">
                    <Select
                        style={{width: 120}}
                        dropdownMatchSelectWidth={false}
                        value={groupId + "_" + groupName}
                        onSelect={this.onSelect}
                    >
                        {
                            groupsBefore.map(
                                item => {
                                    const {groupId, groupName, groupCount} = item
                                    return (
                                        <Option
                                            className="wea-select-option"
                                            value={groupId + "_" + groupName}
                                        >{`${groupName} ( ${groupCount} )`}</Option>
                                    )
                                }
                            )
                        }
                        <Option
                            value="label"
                            disabled={true}
                            style={{
                                color: "#8a8a8a",
                                padding: 0,
                                minWidth: "200px",
                                cursor: "default"
                            }}
                        ><WeaSplitLine>我的分组</WeaSplitLine></Option>
                        {
                            groupsAfter.map(
                                item => {
                                    const {groupId, groupName, groupCount} = item
                                    return (
                                        <Option
                                            className="wea-select-option"
                                            value={groupId + "_" + groupName}
                                        >{`${groupName} ( ${groupCount} )`}
                                        </Option>
                                    )
                                }
                            )
                        }
                        <Option
                            value="new"
                            style={{
                                background: "#e9e9e9",
                                textAlign: "center",
                                padding: "8px 0",
                                minWidth: "250px"
                            }}
                        ><span className="icon-blog-NewPacket"/>&nbsp;&nbsp;&nbsp;新建分组</Option>
                    </Select>
                    {
                        groupId != "all" && groupId != "nogroup" ? (
                            <span>
                                <span
                                    onClick={() => {
                                        this.refs.editGroupModal.visible = true
                                    }}
                                    className="wea-ftsps-icon icon-blog-Pay"
                                />
                                <span onClick={this.deleteGroup} className="wea-ftsps-icon icon-blog-delete"/>
                            </span>
                        ) : ""
                    }
                    <CreateGroupModal ref="createGroupModal" requestData={this.createGroup} title="新建分组"/>
                    <CreateGroupModal
                        ref="editGroupModal"
                        requestData={this.editGroup}
                        title="编辑分组"
                        name={groupName}
                        value={groupId}
                    />
                </div>
                <div className="wea-ftsp-item wea-ftsp-search">
                    <WeaInputSearch
                        placeholder=""
                        value={content}
                        onSearchChange={this.onSearchChange}
                        onSearch={this.onSearch}
                        style={{width: '125px'}}
                    />
                </div>
            </div>
        )
    }

    createGroup = (params) => {
        const {actions} = this.props
        actions.createGroup(
            {groupName: params},
            result => {
                let {status, groupInfo} = result,
                    {groupId, groupName} = groupInfo
                if (status == "1" && groupId) {
                    actions.getWeiboGroup()
                    actions.getAttentionList(
                        {groupId: groupId},
                        () => {
                            this.setState({groupId: groupId, groupName: groupName})
                        }
                    )
                    this.props.changeGroup({groupId: groupId, groupName: groupName})
                }
            }
        )
        this.refs.createGroupModal.visible = false
    }

    editGroup = (name, value) => {
        let {actions} = this.props
        actions.editGroup(
            {groupId: value, groupName: name},
            result => {
                let {status, groupInfo} = result,
                    {groupId, groupName} = groupInfo
                if (status == "1" && groupId) {
                    actions.getAttentionList(
                        {groupId: groupId},
                        this.setState({groupId: groupId, groupName: groupName})
                    )
                    this.props.changeGroup({groupId: groupId, groupName: groupName})
                }
            }
        )
        this.refs.editGroupModal.visible = false
    }

    deleteGroup = () => {
        let {actions} = this.props,
            {groupId, groupName} = this.state
        const modal = Modal.confirm({
            title: '确定要删除分组吗？',
            content: <span>删除后，分组下所以人员将从此分组列表中移除</span>,
            onOk: () => {
                actions.deleteGroup(
                    {groupId: groupId, groupName: groupName},
                    result => {
                        if (result.status == "1") {
                            actions.getAttentionList(
                                {groupId: "nogroup"},
                                this.setState({groupId: "nogroup", groupName: "未分组"})
                            )
                            this.props.changeGroup({groupId: "nogroup", groupName: "未分组"})
                        }
                    }
                )
                modal.destroy()
            },
            onCancel: () => {
            }
        })
    }

    onSelect = (value) => {
        if (value == "new") {
            this.refs.createGroupModal.visible = true
        } else {
            const values = value.split("_")
            this.setState({groupId: values[0], groupName: values[1], content: ""})
            this.props.actions.getAttentionList({groupId: values[0]})
            this.props.changeGroup({groupId: values[0], groupName: values[1]})
        }
    }

    onSearchChange = (value) => {
        this.setState({content: value})
    }
    onSearch = () => {
        this.setState({groupId: "all", groupName: "全部关注"})
        this.props.actions.getAttentionList({searchKey: this.state.content})
        this.props.changeGroup({groupId: "all", groupName: "全部关注"})
    }
}

export default TopSearchPanel