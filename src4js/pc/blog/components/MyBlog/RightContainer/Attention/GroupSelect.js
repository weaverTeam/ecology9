import React, {Component} from 'react'

import {Menu, Dropdown} from 'antd'

import CreateGroupModal from "./CreateGroupModal"

class GroupSelect extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false
        }
    }

    render() {
        const {groups, children, title, disabledGroup} = this.props,
            {visible} = this.state
        return (
            <div>
                <Dropdown
                    overlay={
                        <Menu onClick={this.createGroup}>
                            {
                                groups.map(
                                    item => {
                                        const {groupId, groupName, groupCount} = item
                                        return (
                                            <Menu.Item
                                                key={`${groupId}_${groupName}`}
                                                style={(
                                                    disabledGroup == groupId ? {
                                                        backgroundColor: "#e7f4fd",
                                                        color: '#1691e9',
                                                        cursor:"default",
                                                        maxWidth: "200px",
                                                        overflow:"hidden",
                                                        textOverflow: "ellipsis",
                                                        whiteSpace: "nowrap",
                                                    } : {
                                                        maxWidth: "200px",
                                                        overflow:"hidden",
                                                        textOverflow: "ellipsis",
                                                        whiteSpace: "nowrap",
                                                    }
                                                )}
                                            >{`${groupName} ( ${groupCount} )`}</Menu.Item>
                                        )
                                    }
                                )
                            }
                            <Menu.Item
                                key="new"
                                style={{
                                    background: "#e9e9e9",
                                    textAlign: "center",
                                    padding: "8px",
                                    maxWidth: "200px",
                                    overflow:"hidden",
                                    textOverflow: "ellipsis",
                                    whiteSpace: "nowrap",
                                }}
                            ><span className="icon-blog-NewPacket"/>&nbsp;&nbsp;&nbsp;{title || "新建分组"}</Menu.Item>
                        </Menu>
                    }
                    trigger={['click']}
                    visible={visible}
                    onVisibleChange={this.changeVisible}
                >
                    {children}
                </Dropdown>
                <CreateGroupModal ref="createGroupModal" requestData={this.requestData} title={title}/>
            </div>
        )
    }

    changeVisible = (value) => {
        this.setState({visible: value})

        const {onVisible} = this.props
        if (onVisible) onVisible(value)
    }

    createGroup = (value) => {
        if (value.key == "new") {
            this.setState({visible: false})
            this.refs.createGroupModal.visible = true
        } else {
            const values = value.key.split("_")
            this.setState({visible: false})
    
            const {onChange, onVisible, disabledGroup} = this.props
            if ((disabledGroup != values[0]) && onChange) onChange({value: values[0], name: values[1]})
            if (onVisible) onVisible(false)
        }
    }

    requestData = (name, value) => {
        const {onVisible, createGroup} = this.props
        createGroup(name, value)
        if (onVisible) onVisible(false)
        this.refs.createGroupModal.visible = false
    }
}

export default GroupSelect