import React, {Component} from 'react'
import T from 'prop-types'

import {Checkbox} from "antd"

import TopSearchPanel from "./TopSearchPanel"
import {WeaNewScroll} from 'ecCom';
import WeaWhitePage from "../../../Component/wea-white-page"

import Attention from "./Attention"

class Attentions extends Component {
    static contextTypes = {
        router: T.routerShape
    }

    constructor(props) {
        super(props)
        this.state = this.initData()
    }

    initData = () => {
        return {
            checksCount: 0,
            checksValue: false,
            indeterminateValue: false,

            selectValue: "",
            selectName: "",

            currentGroup: {
                groupId: "all",
                groupName: "全部关注"
            },

            data: []
        }
    }

    componentWillReceiveProps(props) {
        const {data} = props
        this.setState({data: data})
    }

    get userIdsString() {
        return this.getCheckedValue("userId")
    }

    get isLowersString() {
        return this.getCheckedValue("islower")
    }

    getCheckedValue = (name) => {
        if (name) {
            let result = []
            this.state.data.forEach(
                item => {
                    if (item.checked) {
                        result.push(item[name])
                    }
                }
            )
            return result.join(",")
        } else {
            return ""
        }
    }

    set clearData(value) {
        if (value) this.setState(this.initData())
    }

    render() {
        let {checksValue, checksCount, indeterminateValue, data, currentGroup} = this.state,
            {weiboGroup, height, actions, changeGroup} = this.props,
            groups = weiboGroup.groupList || [],
            groupsBefore = [],
            groupsAfter = [],
            groupsAttention = []
        groups.forEach(
            item => {
                if ((item.groupId == "all") || (item.groupId == "nogroup")) {
                    groupsBefore.push(item)
                    if (item.groupId == "nogroup") {
                        groupsAttention.push(item)
                    }
                } else {
                    groupsAfter.push(item)
                    groupsAttention.push(item)
                }
            }
        )
        return (
            <div className="wea-myBlog-fan">
                <TopSearchPanel
                    groupsBefore={groupsBefore}
                    groupsAfter={groupsAfter}
                    status={indeterminateValue}
                    changeGroup={this.changeGroup}
                    actions={actions}
                    defaultGroup={currentGroup}
                />
                <WeaNewScroll height={height - 45}>
                <div style={{padding: "16px 16px 8px 16px"}}>
                    <Checkbox
                        className={"wea-fft-check " + (indeterminateValue ? "wea-checkbox-indeterminate" : "")}
                        checked={checksValue}
                        onChange={this.onChecks}
                        indeterminate={indeterminateValue}
                    >全选</Checkbox>
                    <span>
                        &nbsp;&nbsp;&nbsp;已选择&nbsp;
                        <span style={{color: '#32a8ff'}}>{checksCount}</span>
                        &nbsp;个对象
                    </span>
                </div>
                
                {
                    data.length ? (
                        <div style={{padding: "0 8px"}}>
                            {
                                data.map(
                                    item => {
                                        return (
                                            <Attention
                                                type="attention"
                                                groups={groupsAttention}
                                                data={item}
                                                onCheck={this.onCheck}
                                                actions={actions}
                                                group={currentGroup}
                                            />
                                        )
                                    }
                                )
                            }
                        </div>
                    ) : <WeaWhitePage height={height}/>
                }
                </WeaNewScroll>
            </div>
        )
    }

    getList = (params) => {
        this.props.actions.getAttentionList(params)
    }

    changeGroup = (value) => {
        this.setState({data: [], checksValue: false, checksCount: 0, indeterminateValue: false, currentGroup: value})
        this.props.onCheck(false)
        this.props.changeGroup(value)
    }

    onChecks = (e) => {
        let {data = []} = this.state,
            checksValue = e.target.checked,
            _data = data.map(
                item => {
                    item.checked = checksValue
                    return item
                }
            ),
            _checksCount = checksValue ? data.length : 0
        this.setState({data: _data, checksValue: checksValue, checksCount: _checksCount, indeterminateValue: false})
        this.props.onCheck(!!_checksCount)
    }

    onCheck = (userId, e) => {
        let {data = [], checksValue, checksCount, indeterminateValue} = this.state,
            checkValue = e.target.checked,
            _checksValue = checksValue,
            _checksCount = checksCount,
            _indeterminateValue = indeterminateValue,
            _data = data.map(
                item => {
                    if (item.userId == userId) {
                        item.checked = checkValue
                        if (checkValue) {
                            _checksCount++
                        } else {
                            _checksCount--
                            if (checksValue) {
                                _checksValue = false
                            }
                        }
                    }
                    return item
                }
            )
        if (_checksCount == data.length) _checksValue = true
        if (_checksCount > 0 && _checksCount < data.length) _indeterminateValue = true
        else _indeterminateValue = false
        this.setState({
            data: _data,
            checksValue: _checksValue,
            checksCount: _checksCount,
            indeterminateValue: _indeterminateValue
        })
        this.props.onCheck(!!_checksCount)
    }
}

export default Attentions