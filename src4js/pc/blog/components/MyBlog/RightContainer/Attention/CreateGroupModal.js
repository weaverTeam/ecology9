import React, {Component} from 'react'

import {Select, Input, Button} from 'antd'
import {WeaDialog} from 'ecCom'
const Option = Select.Option

import {WeaInputSearch, WeaInput} from 'ecCom'

class CreateGroupModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            value: props.value || "",
            name: props.name || ""
        }
    }

    componentWillReceiveProps(props) {
        const {value, name} = props
        this.setState({
            value: value,
            name: name
        })
    }

    get data() {
        return this.state;
    }

    set visible(value) {
        this.setState({visible: value})
    }

    render() {
        let {visible, name} = this.state
        return (
            <WeaDialog
                style={{height: 80}}
                visible={visible}
                title={this.props.title || ''}
                icon="icon-coms-blog"
                iconBgcolor="#6d3cf7"
                buttons={[
                    <Button
                        type="primary"
                        onClick={this.onSave}
                    >保存</Button>,
                    <Button
                        onClick={() => {
                            this.setState({visible: false})
                        }}
                    >取消</Button>
                ]}
                onCancel={() => {
                    this.setState({visible: false})
                }}
            >
                <div style={{margin: "20px 0"}}>
                    <span style={{lineHeight: "32px", position: "absolute", marginLeft: "64px"}}>分组名称</span>
                    <WeaInput
                        style={{width: "60%", marginLeft: "30%"}}
                        value={name}
                        onChange={this.onChange}
                    />
                    <span style={{fontSize: "16px", marginLeft: "16px", color: "red"}}>!</span>
                </div>
            </WeaDialog>
        )
    }

    onSave = () => {
        const {name, value} = this.state
        if (name) {
            this.props.requestData(name, value)
        } else {
            const modal = Modal.warning({
                title: '警告',
                content: '请填写分组名称！',
            })
            setTimeout(() => {
                modal.destroy()
            }, 1000);
        }

    }

    onChange = (value) => {
        this.setState({name: value})
    }

    clearState() {
        this.setState({
            visible: false,
            value: "",
            name: ""
        })
    }
}

export default CreateGroupModal