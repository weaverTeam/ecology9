import React, {Component} from 'react'

import {Checkbox, Tooltip, Button} from 'antd'

import WeaCircleImage from "../../../Component/wea-circle-image"

import CheckboxDropdown from "./CheckboxDropdown"
import weaBlogDict from '../../../Component/wea-blog-dict';
const innerHref = weaBlogDict.href;

class Attention extends Component {
    constructor(props) {
        super(props)
        const {groupId, groupName} = props.data
        this.state = {
            selectName: groupName || "",
            selectValue: groupId || "",
            selectVisible: false,
            checkValue: false,
        }
    }

    componentWillReceiveProps(props) {
        const {checked, groupId, groupName} = props.data
        this.setState({
            checkValue: checked,
            selectValue: groupId,
            selectName: groupName
        })
    }

    render() {
        const {data, onCheck, actions} = this.props,
            {checkValue, selectVisible} = this.state,
            {departmentName, imageUrl, lastname, attentionStatus, attentionMeStatus, userId, islower} = data,
            groupList = this.props.groups

        let attentionOperation = null,
            attentionDesc = null

        let groupsArray = []
        if (data.groups) {
            data.groups.forEach(
                item => {
                    groupsArray.push(item.groupName)
                }
            )
        }

        if (attentionMeStatus) {
            switch (attentionMeStatus * 1) {
                case 1:
                    attentionOperation = {icon: "icon-blog-checkbox", text: "添加关注"};
                    break;
                case 2:
                    attentionOperation = {icon: "icon-blog-AlreadyConcern", text: "申请关注"};
                    break;
                case 3:
                    attentionOperation = {icon: "icon-blog-AlreadyConcern", text: "已发送申请", cancelEvent: true};
                    break;
                case 4:
                    attentionOperation = {icon: "icon-blog-CancelConcern", text: "取消关注"};
                    attentionDesc = {icon: "icon-blog-MutualConcern", text: "相互关注"};
                    break;
                default:
                    break;
            }
        }

        if (attentionStatus) {
            switch (attentionStatus * 1) {
                case 1:
                    attentionOperation = {icon: "icon-blog-CancelConcern", text: "取消关注"};
                    attentionDesc = {icon: "icon-blog-follow", text: "已关注"};
                    break;
                case 2:
                    attentionOperation = {icon: "icon-blog-CancelConcern", text: "取消关注"};
                    attentionDesc = {icon: "icon-blog-MutualConcern", text: "相互关注"};
                    break;
                case 3:
                    attentionOperation = {icon: "icon-blog-AlreadyConcern", text: "申请关注"};
                    break;
                case 4:
                    attentionOperation = {icon: "icon-blog-AlreadyConcern", text: "已发送申请", cancelEvent: true};
                    break;
                case 5:
                    attentionOperation = {icon: "icon-blog-checkbox", text: "添加关注"};
                    break;
                default:
                    break;
            }
        }

        return (
            <div className="wea-fans-fan">
                <div className="wea-ff-top">
                    {
                        attentionDesc ? (
                            <Tooltip class="wea-tool-tip" placement="right" title={attentionDesc.text}>
                                <span className={"wea-fft-icon " + attentionDesc.icon}/>
                            </Tooltip>
                        ) : ""
                    }
                    {
                        onCheck ? (
                            <Checkbox
                                className="wea-fft-check"
                                checked={checkValue}
                                onChange={onCheck.bind(null, userId)}
                            />
                        ) : ""
                    }
                </div>
                <ul className="wea-ff-content">
                    <li className="wea-ffc-item wea-ffc-icon">
                        <a href={innerHref.blog(userId)} target="_blank">
                            <WeaCircleImage diameter={70} name={lastname} src={imageUrl}/>
                        </a>
                    </li>
                    <li className="wea-ffc-item wea-ffc-name"><a href={innerHref.blog(userId)} target="_blank">{lastname}</a></li>
                    <li className="wea-ffc-item wea-ffc-department">{departmentName}</li>
                    {
                        groupList ? (
                            <li className="wea-ffc-item wea-ffc-group">
                                <CheckboxDropdown
                                    title="请选择分组："
                                    ref="checkboxDropdown"
                                    footer={
                                        <Button type="primary" onClick={this.resetGroup}>保存</Button>
                                    }
                                    values={
                                        data.groups.map(
                                            item => {
                                                return item.groupId
                                            }
                                        )
                                    }
                                    options={
                                        groupList.map(
                                            item => {
                                                if (item.groupId == "nogroup") {
                                                    return {
                                                        name: item.groupName,
                                                        value: item.groupId,
                                                        isInconsistent: true
                                                    }
                                                } else {
                                                    return {name: item.groupName, value: item.groupId}
                                                }
                                            }
                                        )
                                    }
                                    style={{width: "332px"}}
                                    actions={actions}
                                >
                                    <Tooltip placement="top" title={groupsArray.join(", ")}
                                             overlayStyle={{maxWidth: "200px"}}>
                                        <div className="wea-ffcg-container">
                                            <span className="wea-ffcgc-text">
                                                {groupsArray.join(", ")}&nbsp;&nbsp;&nbsp;
                                            </span>
                                            <span
                                                className={"wea-ffcgc-icon icon-coms-organization-down"}
                                            />
                                        </div>
                                    </Tooltip>
                                </CheckboxDropdown>
                            </li>
                        ) : ""
                    }
                    {
                        attentionOperation ? (
                            <li className={"wea-ffc-item wea-ffc-operation " + (attentionOperation.cancelEvent ? "wea-ffc-operation-disabled" : "")}
                                onClick={
                                    attentionOperation.cancelEvent ?
                                        () => "" :
                                        () => this.changeConcern(userId, attentionStatus || attentionMeStatus, islower)
                                }
                            >
                                <span className={"wea-ffco-icon " + attentionOperation.icon}/>
                                <span className="wea-ffco-text">&nbsp;&nbsp;&nbsp;&nbsp;{attentionOperation.text}</span>
                            </li>
                        ) : ""
                    }
                </ul>
            </div>
        )
    }

    changeConcern = (userId, attentionStatus, islower) => {
        const {type, actions} = this.props
        switch (type) {
            case "fan":
                actions.editFan(
                    {
                        userId: userId,
                        attentionMeStatus: attentionStatus,
                        islower: islower
                    },
                    result => {
                        if (result.status == "1") {
                            actions.getBasicInfo()
                        }
                    }
                )
                break;
            case "attention":
                actions.editAttition(
                    {
                        userId: userId,
                        attentionStatus: attentionStatus,
                        islower: islower
                    },
                    result => {
                        if (result.status == "1") {
                            actions.getBasicInfo()
                        }
                    }
                )
                break;
            default:
                break;
        }
    }

    resetGroup = () => {
        let ref = this.refs.checkboxDropdown,
            {actions, data, group} = this.props,
            destGroup = ref.value
        if (destGroup.length == 0) destGroup = ["nogroup"]
        actions.resetGroup(
            {userId: data.userId, destGroup: destGroup.join(",")},
            result => {
                if (result.status == "1") {
                    actions.getWeiboGroup()
                    actions.getAttentionList({groupId: group.groupId})
                }
            }
        )
        ref.visible = false
    }

    createAndMoveGroup = (groupName) => {
        let {actions, data, group} = this.props
        actions.createAndMoveGroup(
            {idSet: data.userId, sourceGroup: group.groupId, groupName: groupName},
            result => {
                if (result.status == "1") {
                    actions.getWeiboGroup()
                    actions.getAttentionList({groupId: group.groupId})
                }
            }
        )
    }
}

export default Attention