const ComVar = {
    WeiboType: {
        current: "current",
        none: "none",
        normal: "normal",
        expired: "expired",
        white: "white"
    },
    WeiboStatus: {
        edit: "edit",
        view: "view"
    },
    PublicRangeType:{
        public: "0",//公开
        private: "1",//私密
        many: "2"//部分公开
    },
    PublicRangeLevelType: {
        department: "0",//部门
        subDepartment: "1",//分部
        all: "2",//所有人
        hr: "3",//人力资源
        role: "4",//角色
        job: "5"//岗位
    },
    PublicRangeRoleLevel: {
        head: "0",//总部
        department: "1",//部门
        subDepartment: "2"//分部
    },
    PublicRangeJobLevel: {
        head: "0",//总部
        department: "1",//指定部门
        subDepartment: "2"//指定分部
    },
    TaskType:{
        workflow: 2,
        cooperation: 5,
        customer: 9
    },
    AppType:{
        mood: "mood"
    },
    Moods:{
        happy:"26917",
        sad:"26918"
    },
    ComeFrom:{
        pc:0,
        mobileWeb:1,
        android:2,
        iphone:3,
        pad:4
    },
    commentMinHeight : 100
}

export default ComVar