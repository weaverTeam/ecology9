import React, {Component} from 'react';
import T from 'prop-types'

import { Menu, Dropdown } from 'antd';

export default class Mood extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            value:""
        }
    }
    render() {
        let {visible, value} = this.state
        return (
            <Dropdown trigger={['click']} onVisibleChange={this.onVisibleChange} overlay={
                <Menu onSelect={this.onMoodSelect}>
                    <Menu.Item key="1">
                        <span className="icon-blog-Emoji">&nbsp;高兴</span>
                    </Menu.Item>
                    <Menu.Item key="2">
                        <span className="icon-blog-Unhappy">&nbsp;不高兴</span>
                    </Menu.Item>
                </Menu>
            }>
                <div className="wea-cb-item">
                    <span className={"wea-cbi-icon "+(value === "2" ? "icon-blog-Unhappy": (value === "1" ? "icon-blog-Emoji" : "icon-blog-Mood"))}/>
                    <span className="wea-cbi-text">心情</span>
                </div>
            </Dropdown>
        )
    }
	
	getValue(){
		return this.state.value
	}

    onVisibleChange = (value) =>{
        this.setState({visible: value})
    }

    onMoodSelect = (item) => {
    	const { onSelect } = this.props;
        this.setState({value: item.key});
        typeof onSelect === 'function' && onSelect(item.key)
    }
}
