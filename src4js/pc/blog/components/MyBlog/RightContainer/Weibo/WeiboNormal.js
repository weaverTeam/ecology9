import React from 'react'

const richfilter = (data, keyWords, color) => {
	let newData = data,
		bgColor = color || "#ff9632",
		newKeyWords = "<span style='background-color: " + bgColor + ";'>" + keyWords + "</span>",
		nodeNum = -1,
		charNum = -1,
		keys = new RegExp(keyWords, "g"),
		nodes = new RegExp("\<.*?\>", "ig"), //匹配html元素
		chars = new RegExp("\&.*?\;", "ig"), //匹配转义字符
		oldNodes = newData.match(nodes), //存放html元素的数组
		oldChars = newData.match(chars); //存放转义字符的数组
		
	newData = newData.replace(nodes, '{~}'); //替换html标签
	newData = newData.replace(chars, '{~~}'); //替换转义字符
	newData = newData.replace(keys, newKeyWords); //替换key
	newData = newData.replace(/{~}/g, () => { //恢复html标签
		nodeNum++;
		return oldNodes[nodeNum];
	});
	newData = newData.replace(/{~~}/g, () => { //恢复html标签
		charNum++;
		return oldChars[charNum];
	});
	return newData
}

const WeiboNormal = (props) => {
	const { data, id, keyWords } = props;
	let dataFilter = keyWords ? richfilter(data, keyWords, '#ff9632') : data;
	//keyWords && eval(`dataFilter = data.replace(/` + keyWords + `/g,'<span style="background-color: #ff9632">${keyWords}</span>')`);
	return <div className={'wea-mbwc-normal'} dangerouslySetInnerHTML={{__html: `<div class="wea-ckeditor-content reportItem" id="${id}">${dataFilter}</div>`}}/>
}

export default WeiboNormal