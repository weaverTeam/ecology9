import React, {Component} from 'react'

import {DatePicker} from 'antd'

import Utils from "../../../Component/wea-utils"
const {DateAddition} = Utils

import {WeaInputSearch,WeaDatePicker} from 'ecCom'

class TopSearchPanel extends Component {
    constructor(props) {
        super(props)
        this.state = this.initData()
    }

    initData = () => {
        return {
            startDate: DateAddition(new Date(), -1, "month"),
            endDate: new Date(),
            content: ''
        }
    }

    get data() {
        return this.state;
    }

    render() {
        return (
            <div className="wea-weibo-top-search-panel">
                <div className="wea-wtsp-item" style={{width: 120}}>
                    <WeaDatePicker 
                     value={this.state.startDate}
                     onChange={this.onChange.bind(null, 'startDate')}
                     placeholder="开始日期"
                     disabledDate={this.disabledStartDate}
                     viewAttr={2}
                     style={{width: '125px'}}
                     />
                </div>
                &nbsp;至&nbsp;&nbsp;&nbsp;&nbsp;
                <div className="wea-wtsp-item" style={{width: 120}}>
                    <WeaDatePicker 
                     value={this.state.endDate}
                     onChange={this.onChange.bind(null, 'endDate')}
                     placeholder="结束日期"
                     disabledDate={this.disabledEndDate}
                     viewAttr={2}
                     style={{width: '125px'}}
                     />
                </div>
                <div className="wea-wtsp-item">
                    <WeaInputSearch
                        placeholder=""
                        value={this.state.content}
                        onSearchChange={this.onChange.bind(null, 'content')}
                        onSearch={this.onSearch}
                        style={{width: '125px'}}
                    />
                </div>
            </div>
        )
    }

    disabledStartDate  = (startDate) => {
        if (!startDate || !this.state.endDate) {
            return false
        }
        return startDate.getTime() > new Date(this.state.endDate).getTime()
    }
    disabledEndDate = (endDate) => {
        if (!endDate || !this.state.startDate) {
            return false
        }
        return endDate.getTime() <  new Date(this.state.startDate).getTime()
    }
    onChange = (field, value) => {
        this.setState({[field]: value,})
    }
    onSearch = () => {
    	const { onChange } = this.props;
        this.requestData({...this.state})
        if(typeof onChange === 'function'){
        	onChange(this.state.content);
        }
    }
    requestData = (params) => {
        this.props.requestData(params)
    }

}

export default TopSearchPanel