import React, {Component} from 'react'
import T from 'prop-types'

import {WeaTimeline} from '../../../Component/'
const {WeaItem} = WeaTimeline

import ComVar from '../ComVar'
const {AppType, WeiboType} = ComVar

import WeaUtils from '../../../Component/wea-utils'
const {DateFormat} = WeaUtils

import '../../../../css/myBlog/rightContainer/weibo.less'

import WeaSplitLine from "../../../Component/wea-split-line"
import WeaWhitePage from "../../../Component/wea-white-page"

import WeiboContainer from './WeiboContainer'
import WeiboEdit from './WeiboEdit'
import WeiboNormal from './WeiboNormal'
import WeiboExpired from './WeiboExpired'
import WeiboWhite from './WeiboWhite'

class Weibos extends Component {
    static contextTypes = {
        router: T.routerShape
    }

    constructor(props) {
        super(props)
    }

    render() {
        const {records, userInfo, actions, weiboCKEditorConfig, blogType, hasScroll, hasEditor, hasImage, defaultTemplate, height, keyWords} = this.props,
            {currentUser = {}} = userInfo,
            {
                discussList = [],
                isManagerScore = 0,
                isMood = 0
            } = records

        let hasSubmited = true
        discussList.forEach(
            item => {
                if (item.workdate == DateFormat(new Date(), "yyyy-MM-dd"))
                    if (item.type == WeiboType.white) {
                        hasSubmited = false
                        return
                    }
            }
        )

        let weibos = []
        if (hasEditor) weibos.push({type: WeiboType.current})
        if (discussList[0]) {
            const {workdate, type} = discussList[0]
            if (workdate == DateFormat(new Date(), "yyyy-MM-dd") && (type != WeiboType.normal) && hasEditor) {
                weibos = weibos.concat(discussList.slice(1))
            } else {
                weibos = weibos.concat(discussList)
            }
        }

        const content = this.renderWeiboDom(actions, weibos, currentUser, isManagerScore, isMood, blogType, hasScroll, hasEditor, hasImage, hasSubmited, defaultTemplate, weiboCKEditorConfig, keyWords)

        return (
            hasEditor ? (
                <div className="wea-myBlog-weibo" style={{backgroundColor: "#f4f4f4"}}>
                    {
                        hasScroll ? (
                            <WeaTimeline>{content}</WeaTimeline>
                        ) : (
                            <div className="weiboNoTime">
                                {
                                    (weibos.length > 1) ? content : <WeaWhitePage height={height}/>
                                }
                            </div>
                        )
                    }
                </div>
            ) : (
                weibos.length > 0 ? (
                    <div className="wea-myBlog-weibo" style={{backgroundColor: "#f4f4f4"}}>
                        {
                            hasScroll ? (
                                <WeaTimeline>{content}</WeaTimeline>
                            ) : (
                                <div className="weiboNoTime">{content}</div>
                            )
                        }
                    </div>
                ) : <WeaWhitePage height={height}/>
            )
        )
    }

    renderWeiboDom = (actions, weibos, currentUser, isManagerScore, isMood, blogType, hasScroll, hasEditor, hasImage, hasSubmited, defaultTemplate, weiboCKEditorConfig, keyWords) => {
        let preWorkData = ""
        return weibos.map(
            (item, key) => {
                let {
                        type, workdate = "", createdate = "", createtime = "", content, username, userid, imageurl, score,
                        isCanEdit, isCanReplenish, isUnSumitRemind, isShowLog, isCanZan, isMeCanScored, isShowReadOrUnread, isReaded,replenishTitle,isHasLocation,
                        blogZanBean, blogReplyList = [], appItems = [], sysWorkLog = [],
                    } = item,
                    user = {name: username, id: userid, src: imageurl},
                    blogId = item.id,
                    date = createdate && createtime ? (createdate + " " + createtime) : "",
                    isLast = key == weibos.length - 1 ? true : false,
                    moods = [],
                    agrees = blogZanBean ? (blogZanBean.blogZanPoList ? blogZanBean.blogZanPoList : []) : [],
                    isAgree = false

                appItems.forEach(
                    item => {
                        if (item && item.type == AppType.mood) {
                            moods.push(item);
                        }
                    }
                )

                agrees.forEach(
                    item => {
                        if (item.zanHrmid == currentUser.id) isAgree = true
                    }
                )

                const renderContent = COM => {
                    return (
                        <WeiboContainer
                            _key={key}
                            actions={actions}
                            blogId={blogId}
                            blogType={blogType}
                            isAgree={isAgree}
                            agrees={agrees}
                            content={content}
                            currentUser={currentUser}
                            user={user}
                            date={date}
                            mood={moods[0]}
                            type={type}
                            createdate={createdate}
                            workdate={workdate}
                            score={score * 1}
                            defaultTemplate={defaultTemplate}
                            isManagerScore={isManagerScore * 1}
                            isCanZan={isCanZan * 1}
                            isLast={isLast * 1}
                            isCanEdit={isCanEdit * 1}
                            isShowLog={isShowLog * 1}
                            isCanReplenish={isCanReplenish * 1}
                            isUnSumitRemind={isUnSumitRemind * 1}
                            isMeCanScored={isMeCanScored * 1}
                            isShowReadOrUnread={isShowReadOrUnread * 1}
                            isReaded={isReaded * 1}
                            comments={blogReplyList || []}
                            systemWorkLog={sysWorkLog || {}}
                            hasEditor={hasEditor}
                            hasImage={hasImage}
                            hasScroll={hasScroll}
                            weiboCKEditorConfig={weiboCKEditorConfig}
                            replenishTitle={replenishTitle}
                            isHasLocation={isHasLocation}
                        >
                            <COM data={content} id={blogId} keyWords={keyWords}/>
                        </WeiboContainer>
                    )
                }

                if (hasScroll) {
                    return (
                        <WeaItem
                            date={workdate}
                            type={type}
                            tipDate={createdate + " " + createtime}
                        >
                            {type == WeiboType.current && hasEditor ?
                                <WeiboEdit
                                    actions={actions}
                                    blogType={blogType}
                                    hasSubmited={hasSubmited}
                                    defaultTemplate={defaultTemplate}
                                    weiboCKEditorConfig={weiboCKEditorConfig}
                                /> : null}
                            {type == WeiboType.normal ? renderContent(WeiboNormal) : null}
                            {type == WeiboType.expired ? renderContent(WeiboExpired) : null}
                            {type == WeiboType.white ? renderContent(WeiboWhite) : null}
                        </WeaItem>
                    )
                } else {
                    const renderContents = () => {
                        if (type == WeiboType.current && hasEditor)
                            return (
                                <WeiboEdit
                                    actions={actions}
                                    blogType={blogType}
                                    hasSubmited={hasSubmited}
                                    defaultTemplate={defaultTemplate}
                                    weiboCKEditorConfig={weiboCKEditorConfig}
                                />
                            )
                        else if (type == WeiboType.normal)
                            return renderContent(WeiboNormal)
                        else if (type == WeiboType.expired)
                            return renderContent(WeiboExpired)
                        else if (type == WeiboType.white)
                            return renderContent(WeiboWhite)
                        else return <div/>
                    }

                    let timeHr = ""
                    if (preWorkData !== createdate) {
                        preWorkData = createdate
                        timeHr = createdate
                    }
                    return (
                        <div>
                            {timeHr ? (
                                <WeaSplitLine
                                    style={(
                                        key === 0 ? {
                                            borderTop: "none",
                                            marginBottom: "16px"
                                        } : {borderTop: "1px solid #eaeaea", padding: "8px", height: "48px"}
                                    )}
                                >
                                    {DateFormat(new Date(createdate), "以下是M月dd日 EE 提交内容")}
                                </WeaSplitLine>
                            ) : ""}
                            {renderContents()}
                        </div>
                    )
                }
            }
        )
    }
}

Weibos.propTypes = {
    actions: React.PropTypes.any,//需要使用以下方法：setReaded，setScore，editComment，editAgree，editWeibo，getSystemWorkLog，getBasicInfo，deleteComment，editWeibo
    userInfo: React.PropTypes.object,//使用currentUser.id属性
    height: React.PropTypes.number,//声明定高
    //isReset: React.PropTypes.bool,//将其置为true，滚动条会置顶，用于切换card时。
    defaultTemplate: React.PropTypes.object,//使用tempContent属性，用于保存之前的编辑内容
    records: React.PropTypes.object,//显示的数据，其中的discussList会和之前的数据拼起来,isManagerScore为是否有打分权限，isMood与__hasData现在无用
    blogType: React.PropTypes.any,//用于生成CKEditor的唯一标识
    hasScroll: React.PropTypes.bool,//是否显示时间轴
    hasEditor: React.PropTypes.bool,//微博是否可编辑？
    hasImage: React.PropTypes.bool//是否显示头像
};
export default Weibos