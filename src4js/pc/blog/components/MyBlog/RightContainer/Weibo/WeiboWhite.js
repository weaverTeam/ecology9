import React  from 'react'

const WeiboWhite = () => {
  return (
    <div className={'wea-mbwc-white'}>
      <span className="icon-blog-Uncommitted" style={{color:"#c5c5c5", fontSize:"18px", verticalAlign: "middle"}}/>
      <span style={{verticalAlign: "middle"}}>&nbsp;&nbsp;&nbsp;未提交微博</span>
    </div>
  )
}

export default WeiboWhite