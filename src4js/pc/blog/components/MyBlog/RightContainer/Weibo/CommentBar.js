import React from 'react'

import {Checkbox, Button} from 'antd'

export default function CommentBar(props) {
    const {setPrivate, submitComment, cancelComment, className = "", ...rest} = props
    return (
        <div className={"wea-weibo-comment-bar " + className} {...rest}>
            <Button onClick={cancelComment} type="ghost" className="wea-wcb-cancel">取消</Button>
            <Button onClick={submitComment} type="primary" className="wea-wcb-submit">评论</Button>
            <Checkbox onChange={setPrivate} className="wea-wcb-private">私密</Checkbox>
        </div>
    )
}