import {WeaDialog,WeaMap} from 'ecCom';
import {message} from 'antd';
import Apis from '../../../../apis/myBlog';
class WeiboMapDialog extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            show: false,
            title: '',
            position: undefined
        }
        this.map = undefined;
	}
       
	render() {
	    let {show,title,position} = this.state;
		return (
			<WeaDialog icon="icon-coms-blog" visible={show} title="地图查看" style={{width: 600, height: 500}} onCancel={this.close}>
                <WeaMap position={position} title={title}/>
            </WeaDialog>
		);
	}
	
	open = () => {
		this.setState({show: true}, this.getLocation);
	}
	
	close = () => {
		this.setState({show: false});
	}
	
	getLocation = () => {
        Apis.getBlogLocations({discussId: this.props.discussId}).then((result) => {
            if (result.status != '1') {
                message.error("读取地理位置失败");
                return;
            }
            let loc = result.blogLocationList[0];
            loc.location = loc.location.split(',');
            let lnglat = new AMap.LngLat(loc.location[1], loc.location[0]);
            lnglat.offset(new AMap.Pixel(-12,-36));
            this.setState({
                position: lnglat,
                title: loc.locationName
            });
        }).catch((e) => {
            console && console.error("读取地理位置出错", e);
        	message.error("读取地理位置出错");
        });
	}
}

export default WeiboMapDialog;