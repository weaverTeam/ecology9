import React from 'react'

import {Button} from 'antd'

import AuthoritySelect from "./AuthoritySelect"

export default function WeiboBar(props) {
    const {onAuthType, onAuthRange, authType, authRange, saveClick, cancelClick, publishClick, className = "", ...rest} = props
    return (
        <div className={"wea-weibo-weibo-bar " + className} {...rest}>
            {/*<AuthoritySelect*/}
                {/*onType={onAuthType}*/}
                {/*onRange={onAuthRange}*/}
                {/*defaultType={authType}*/}
                {/*defaultRange={authRange}*/}
                {/*className="wea-wwb-authority"*/}
            {/*/>*/}
            {
                cancelClick ? (
                    <Button onClick={cancelClick} type="ghost" className="wea-wwb-cancel">取消</Button>
                ) : ""
            }
            {
                publishClick ? (
                    <Button onClick={publishClick} type="primary" className="wea-wwb-publish">发布</Button>
                ) : ""
            }
            {
                saveClick ? (
                    <Button onClick={saveClick} type="primary" className="wea-wwb-save">保存</Button>
                ) : ""
            }
        </div>
    )
}