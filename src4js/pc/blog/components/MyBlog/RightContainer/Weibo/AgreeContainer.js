import React, {Component, Children} from 'react'
import T from 'prop-types';
import WeaBlogDict from '../../../Component/wea-blog-dict';
const innerHref = WeaBlogDict.href;
import '../../../../css/myBlog/rightContainer/agreeContainer.less'

class AgreeContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            open: false
        }
    }

    checkStatus = (data, count) => {
        if (data && data.length > 0) {
            if (data.length > count) {
                return 'more'
            } else {
                return 'less'
            }
        } else {
            return 'none'
        }
    }
    loadDom = (data, models, me) => {
        let result = [false, [], []]
        if (me) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].zanHrmid == me) {
                    result[0] = true
                } else {
                    let dom = (
                        <span
                            className="wea-acmc-name"
                            onClick={() => window.open(innerHref.blog(data[i].zanHrmid))}
                        >{data[i].zanHrmname}</span>
                    )
                    if (i < 10) {
                        result[1].push(dom)
                    }
                    result[2].push(dom)
                }
            }
        } else {
            for (let i = 0; i < data.length; i++) {
                let dom = (
                    <span
                        className="wea-acmc-name"
                        onClick={() => window.open(innerHref.blog(data[i].zanHrmid))}
                    >{data[i].zanHrmname}</span>
                )
                if (i < 10) {
                    result[1].push(dom)
                }
                result[2].push(dom)
            }
        }
        return result
    }
    onAgreeClick = () => {
        const {open} = this.state
        this.setState({open: !open})
    }

    render() {
        const {data = [], models = 10, me = '', ...rest} = this.props,
            {open} = this.state,
            status = this.checkStatus(data, models),
            _data = this.loadDom(data, models, me),
            dataLess = _data[1],
            dataMore = _data[2],
            showMe = _data[0]
        return (
            status == 'none' ? null : (
                <div className='wea-agree-container' {...rest}>
                    <span className="wea-ac-item wea-ac-left icon-blog-Good">&nbsp;</span>
                    <div className="wea-ac-item wea-ac-middle">
                        {
                            status == 'less' ? (
                                <div className="wea-acm-container">
                                    {showMe ? (data.length == 1 ? <span>我</span> :
                                        <span>我和&nbsp;&nbsp;</span>) : null}
                                    {dataLess}
                                    <span>&nbsp;觉得很赞</span>
                                </div>
                            ) : null
                        }
                        {
                            status == 'more' ? (
                                <div className={"wea-acm-container " + (open ? "" : "wea-acm-container-less")}>
                                    {showMe ? <span>我和&nbsp;&nbsp;</span> : null}
                                    {open ? dataMore : dataLess}
                                    <span>&nbsp;等{data.length}人觉得很赞&nbsp;&nbsp;</span>
                                    {
                                        !open ? (
                                            <span
                                                className={'wea-ac-icon icon-blog-' + (open ? 'up' : 'down')}
                                                onClick={this.onAgreeClick}
                                            >&nbsp;</span>
                                        ) : null
                                    }
                                </div>
                            ) : null
                        }
                    </div>
                    {
                        status == 'more' && open ? (
                            <span
                                className={'wea-ac-item wea-ac-right icon-blog-' + (open ? 'up' : 'down')}
                                onClick={this.onAgreeClick}
                            >&nbsp;</span>
                        ) : null
                    }
                </div>
            )
        )
    }
}

AgreeContainer.propTypes = {
    data: T.array.isRequired,
    models: T.number,
    me: T.string
}

export default AgreeContainer