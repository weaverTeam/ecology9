import React, {Component} from 'react'
import T from 'prop-types'

import {Select, Popover, Modal, Button, Table, Form, Input, Row, Col, InputNumber, Dropdown, Menu,message} from 'antd'
const Option = Select.Option
const createForm = Form.create
const FormItem = Form.Item
const InputGroup = Input.Group

import '../../../../css/myBlog/rightContainer/authoritySelect.less'
import ComVar from "../ComVar"
const {PublicRangeLevelType, PublicRangeRoleLevel, PublicRangeJobLevel, PublicRangeType} = ComVar

import {WeaDepInput, WeaComInput, WeaHrmInput, WeaRoleInput, WeaBrowser} from 'ecCom'

// anruo test select

class AddAuthorityForm extends Component {
    render() {
        const {getFieldProps, getFieldValue} = this.props.form,
            formItemLayout = {
                labelCol: {span: 7},
                wrapperCol: {span: 12},
            },
            typeProps = getFieldProps('type', {
                rules: [
                    {required: true, message: '请选择类型'},
                ],
                initialValue: PublicRangeLevelType.department
            }),
            typeValue = getFieldValue('type'),
            safetyStartProps = getFieldProps('safetyStart', {
                rules: [
                    {
                        required: (typeValue === PublicRangeLevelType.hr || typeValue === PublicRangeLevelType.job ? false : true),
                        message: '请输入安全级别'
                    },
                ],
                initialValue: "0"
            }),
            safetyEndProps = getFieldProps('safetyEnd', {
                rules: [
                    {
                        required: (typeValue === PublicRangeLevelType.hr || typeValue === PublicRangeLevelType.job ? false : true),
                        message: '请输入安全级别'
                    },
                ],
                initialValue: "100"
            }),
            roleLevelProps = getFieldProps('roleLevel', {
                rules: [
                    {required: (typeValue === PublicRangeLevelType.role ? true : false), message: '请选择级别'},
                ],
                initialValue: PublicRangeRoleLevel.head
            }),
            jobTypeProps = getFieldProps('jobType', {
                rules: [
                    {required: (typeValue === PublicRangeLevelType.job ? true : false), message: '请选择类型'},
                ],
                initialValue: PublicRangeRoleLevel.head
            }),
            jobTypeValue = getFieldValue('jobType')
        return (
            <div style={{height: "250px"}}>
                <Form horizontal>
                    <FormItem
                        {...formItemLayout}
                        label="类型"
                    >
                        <Select
                            {...typeProps}
                            placeholder="请选择类型"
                            style={{width: '100%'}}
                        >
                            <Option value={PublicRangeLevelType.department}>部门</Option>
                            <Option value={PublicRangeLevelType.subDepartment}>分部</Option>
                            <Option value={PublicRangeLevelType.all}>所有人</Option>
                            <Option value={PublicRangeLevelType.hr}>人力资源</Option>
                            <Option value={PublicRangeLevelType.role}>角色</Option>
                            <Option value={PublicRangeLevelType.job}>岗位</Option>
                        </Select>
                    </FormItem>
                    {
                        typeValue === PublicRangeLevelType.department ? (
                            <FormItem
                                {...formItemLayout}
                                label="条件内容"
                                required
                            >
                                <WeaDepInput
                                    {...getFieldProps(
                                        `content_${PublicRangeLevelType.department}`, {
                                            rules: [
                                                {
                                                    required: (typeValue === PublicRangeLevelType.department ? true : false),
                                                    message: '请选择部门'
                                                },
                                            ]
                                        })
                                    }
                                />
                            </FormItem>
                        ) : null
                    }
                    {
                        typeValue === PublicRangeLevelType.subDepartment ? (
                            <FormItem
                                {...formItemLayout}
                                label="条件内容"
                                required
                            >
                                <WeaComInput
                                    {...getFieldProps(
                                        `content_${PublicRangeLevelType.subDepartment}`, {
                                            rules: [
                                                {
                                                    required: (typeValue === PublicRangeLevelType.subDepartment ? true : false),
                                                    message: '请选择分部'
                                                },
                                            ]
                                        })
                                    }
                                />
                            </FormItem>
                        ) : null
                    }
                    {
                        typeValue === PublicRangeLevelType.hr ? (
                            <FormItem
                                {...formItemLayout}
                                label="条件内容"
                                required
                            >
                                <WeaHrmInput
                                    {...getFieldProps(
                                        `content_${PublicRangeLevelType.hr}`, {
                                            rules: [
                                                {
                                                    required: (typeValue === PublicRangeLevelType.hr ? true : false),
                                                    message: '请选择人力资源'
                                                },
                                            ]
                                        })
                                    }
                                />
                            </FormItem>
                        ) : null
                    }
                    {
                        typeValue === PublicRangeLevelType.role ? (
                            <FormItem
                                {...formItemLayout}
                                label="条件内容"
                                required
                            >
                                <Col span="11">
                                    <FormItem>
                                        <WeaHrmInput
                                            {...getFieldProps(
                                                `content_${PublicRangeLevelType.role}`, {
                                                    rules: [
                                                        {
                                                            required: (typeValue === PublicRangeLevelType.role ? true : false),
                                                            message: '请选择角色'
                                                        },
                                                    ]
                                                })
                                            }
                                        />
                                    </FormItem>
                                </Col>
                                <Col span="13" style={{lineHeight: "20px"}}>
                                    <FormItem
                                        labelCol={{span: "8"}}
                                        wrapperCol={{span: "16"}}
                                        label="级别"
                                        required
                                    >
                                        <Select
                                            {...roleLevelProps}
                                            placeholder="请选择级别"
                                            style={{width: '100%'}}
                                        >
                                            <Option value={PublicRangeRoleLevel.head}>总部</Option>
                                            <Option value={PublicRangeRoleLevel.department}>部门</Option>
                                            <Option value={PublicRangeRoleLevel.subDepartment}>分部</Option>
                                        </Select>
                                    </FormItem>
                                </Col>
                            </FormItem>
                        ) : null
                    }
                    {
                        typeValue === PublicRangeLevelType.job ? (
                            <FormItem
                                {...formItemLayout}
                                label="条件内容"
                                required
                            >
                                <WeaHrmInput
                                    {...getFieldProps(
                                        `content_${PublicRangeLevelType.job}`, {
                                            rules: [
                                                {
                                                    required: (typeValue === PublicRangeLevelType.job ? true : false),
                                                    message: '请选择岗位'
                                                },
                                            ]
                                        })
                                    }
                                />
                            </FormItem>
                        ) : null
                    }
                    {
                        typeValue === PublicRangeLevelType.job ? (
                            <FormItem
                                {...formItemLayout}
                                label="岗位级别"
                                required
                            >
                                <Col span={jobTypeValue === PublicRangeJobLevel.head ? "24" : "11"}>
                                    <FormItem>
                                        <Select
                                            {...jobTypeProps}
                                            placeholder="请选择岗位级别"
                                            style={{width: '100%'}}
                                        >
                                            <Option value={PublicRangeJobLevel.head}>总部</Option>
                                            <Option value={PublicRangeJobLevel.department}>指定部门</Option>
                                            <Option value={PublicRangeJobLevel.subDepartment}>指定分部</Option>
                                        </Select>
                                    </FormItem>
                                </Col>
                                {
                                    jobTypeValue === PublicRangeJobLevel.department ? (
                                        <Col span="11" offset="2">
                                            <FormItem>
                                                <WeaDepInput
                                                    {...getFieldProps(
                                                        `jobLevel_${PublicRangeJobLevel.department}`, {
                                                            rules: [
                                                                {
                                                                    required: true,
                                                                    message: '请选择部门'
                                                                },
                                                            ]
                                                        })
                                                    }
                                                />
                                            </FormItem>
                                        </Col>
                                    ) : null
                                }
                                {
                                    jobTypeValue === PublicRangeJobLevel.subDepartment ? (
                                        <Col span="11" offset="2">
                                            <FormItem>
                                                <WeaComInput
                                                    {...getFieldProps(
                                                        `jobLevel_${PublicRangeJobLevel.subDepartment}`, {
                                                            rules: [
                                                                {
                                                                    required: true,
                                                                    message: '请选择分部'
                                                                },
                                                            ]
                                                        })
                                                    }
                                                />
                                            </FormItem>
                                        </Col>
                                    ) : null
                                }
                            </FormItem>
                        ) : null
                    }
                    {
                        typeValue !== PublicRangeLevelType.hr && typeValue !== PublicRangeLevelType.job ? (
                            <FormItem
                                {...formItemLayout}
                                required
                                label="安全级别"
                            >
                                <InputGroup className="wea-input-group">
                                    <Col span="11">
                                        <FormItem>
                                            <InputNumber {...safetyStartProps} style={{width: "100%"}} min={0} max={100}
                                                         step={1}/>
                                        </FormItem>
                                    </Col>
                                    <Col span="2" style={{textAlign: "center"}}>
                                        ~
                                    </Col>
                                    <Col span="11">
                                        <FormItem>
                                            <InputNumber {...safetyEndProps} style={{width: "100%"}} min={0} max={100}
                                                         step={1}/>
                                        </FormItem>
                                    </Col>
                                </InputGroup>
                            </FormItem>
                        ) : null
                    }
                </Form>
            </div>
        )
    }
}

AddAuthorityForm = createForm()(AddAuthorityForm)

const SelectOption = (props) => {
    const {name, tip, isSelected, isLast} = props
    return (
        <div className={"wea-as-option " + (isLast ? 'wea-as-option-last' : '')}>
            <span className={"wea-aso-item wea-aso-icon " + (isSelected ? 'icon-blog-Submit' : '')}/>
            <span className="wea-aso-item wea-aso-name">{name}</span>
            <span className="wea-aso-item wea-aso-tip">{tip}</span>
        </div>
    )
}

class AuthoritySelect extends Component {
    constructor(props) {
        super(props)
        this.state = {
            type: props.defaultType || PublicRangeType.public,
            range: props.defaultRange || [],
            modalSetVisible: false,
            selectedRowKeys: [],
            modalAddVisible: false
        }
    }

    render() {
        const {className, ...rest} = this.props,
            {type, modalSetVisible, selectedRowKeys, modalAddVisible} = this.state,
            types = [
                {name: '公开', value: PublicRangeType.public, tip: '所有人可见'},
                {name: '私密', value: PublicRangeType.private, tip: '仅自己可见'},
                {name: '部分公开', value: PublicRangeType.many, tip: '选中范围的人可见'}
            ]

        //anruo test table
        const authorities = []
        for (let i = 0; i < 100; i++) {
            authorities.push({
                key: i,
                type: `部门${i}`,
                content: `需求分析${i}`,
                level: `${i} - 100`
            })
        }

        return (
            <div className={'wea-authority-select ' + className} {...rest}>
                <span className="wea-as-name">权限：&nbsp;&nbsp;&nbsp;</span>
                <Select
                    //size="large"
                    dropdownMatchSelectWidth={false}
                    style={{width: 120, verticalAlign: 'top'}}
                    value={type}
                    onChange={this.changeType}
                >
                    {
                        types.map(
                            (item, key) => {
                                const {value, ...rest} = item,
                                    isSelected = value == type ? true : false,
                                    isLast = key == types.length - 1 ? true : false
                                return (
                                    <Option value={value} key={value}>
                                        <SelectOption {...rest} isSelected={isSelected} isLast={isLast}/>
                                    </Option>
                                )
                            }
                        )
                    }
                </Select>
                <Popover
                    placement="bottomLeft"
                    title="提示"
                    content="&nbsp;&nbsp;&nbsp;微博没有真正意义上的“私密”，上级和后端指定共享人为固定共享，不可修改，“私密”和“部分公开”也是建立在这个基础上的共享。"
                    trigger="hover"
                    overlayStyle={{width: '250px'}}
                >
                    <span className="wea-as-icon icon-blog-Explain"/>
                </Popover>
                {
                    type == PublicRangeType.many ?
                        <span className="wea-as-text" onClick={() => this.setModalSetVisible(true)}>公开范围设置</span> : null
                }
                {
                    type == PublicRangeType.many ? (
                        <Modal
                            title={
                                <div>
                                    <span className="icon-blog-Note"
                                          style={{fontSize: '28px', color: '#ff7725', backgroundColor: '#f4f4f4'}}/>
                                    <span style={{
                                        lineHeight: '28px',
                                        fontSize: '16px',
                                        verticalAlign: 'top',
                                        marginLeft: '4px',
                                        fontWeight: 'normal'
                                    }}>&nbsp;公开范围设置</span>
                                </div>
                            }
                            footer={[
                                <Button
                                    key="save" type="primary" size="large"
                                    onClick={() => this.setModalAddVisible(true)}
                                >添加</Button>,
                                <Button
                                    key="publish" type="primary" size="large"
                                    onClick={this.deleteAuthority}
                                >批量删除</Button>,
                                <Button
                                    key="back" type="ghost" size="large"
                                    onClick={() => this.setModalSetVisible(false)}
                                >取消</Button>
                            ]}
                            wrapClassName="wea-modal wea-vertical-center-modal wea-nopadding-modal"
                            width="720"
                            visible={modalSetVisible}
                            onOk={() => this.setModalSetVisible(false)}
                            onCancel={() => this.setModalSetVisible(false)}
                        >
                            <Table
                                className="wea-table wea-table-pagination-margin"
                                style={{height: '520px'}}
                                scroll={{x: true, y: 420}}
                                dataSource={authorities}
                                pagination={{
                                    total: authorities.length,
                                    showSizeChanger: true,
                                    showQuickJumper: true,
                                    showTotal: this.showTotalChange,
                                    onShowSizeChange: this.showSizeChange,
                                    onChange: this.showCurrentChange
                                }}
                                columns={[
                                    {
                                        title: '类型',
                                        dataIndex: 'type',
                                        key: 'type',
                                        width: 180,
                                        className: "wea-table-column-padding"
                                    },
                                    {title: '条件内容', dataIndex: 'content', key: 'content', width: 320},
                                    {title: '安全级别', dataIndex: 'level', key: 'level', width: 120},
                                    {title: '', dataIndex: '', key: '', render: this.renderOperation, width: 100},
                                ]}
                                rowSelection={{
                                    selectedRowKeys,
                                    onChange: this.selectAuthority,
                                }}
                            />
                            <Modal
                                title={
                                    <div>
                                    <span className="icon-blog-Note"
                                          style={{fontSize: '28px', color: '#ff7725', backgroundColor: '#f4f4f4'}}/>
                                        <span style={{
                                            lineHeight: '28px',
                                            fontSize: '16px',
                                            verticalAlign: 'top',
                                            marginLeft: '4px',
                                            fontWeight: 'normal'
                                        }}>&nbsp;添加公开范围</span>
                                    </div>
                                }
                                footer={[
                                    <Button
                                        key="save" type="primary" size="large"
                                        onClick={this.saveAuthorityClick}
                                    >保存</Button>,
                                    <Button
                                        key="back" type="ghost" size="large"
                                        onClick={() => this.setModalAddVisible(false)}
                                    >取消</Button>
                                ]}
                                wrapClassName="wea-modal wea-vertical-center-modal"
                                width="600"
                                visible={modalAddVisible}
                                onOk={() => this.setModalAddVisible(false)}
                                onCancel={() => this.setModalAddVisible(false)}
                            >
                                <AddAuthorityForm
                                    ref={
                                        (ref) => {
                                            this.form = ref ? ref.getForm() : null
                                        }
                                    }
                                />
                            </Modal>
                        </Modal>
                    ) : null
                }
            </div>
        )
    }

    changeType = (value) => {
        let {onType} = this.props
        if (onType) onType(value)
        this.setState({type: value})
    }

    setModalSetVisible = (modalSetVisible) => {
        this.setState({modalSetVisible: modalSetVisible})
    }

    deleteAuthority = () => {
    }

    showSizeChange = (current, pageSize) => {
    }
    showCurrentChange = (current) => {
    }

    showTotalChange = (total) => {
        return `共 ${total} 条`
    }

    selectAuthority = (selectedRowKeys) => {
        let {onRange} = this.props
        if(onRange) onRange(selectedRowKeys)
        this.setState({selectedRowKeys})
    }

    renderOperation = (text, record, index) => {
        return (
            <Dropdown overlay={
                <Menu>
                    <Menu.Item>
                        <span onClick={(text, record, index) => this.deleteAuthority()}>删除</span>
                    </Menu.Item>
                </Menu>
            }>
                <div>操作&nbsp;&nbsp;&nbsp;<span className="icon-blog-down"/></div>
            </Dropdown>
        )
    }

    //anruo submit authority
    saveAuthorityClick = () => {
        this.form.validateFieldsAndScroll({force: true}, (errors, values) => {
            if (!!errors) {
                message.error("填写有误");
                return
            }
            const {type, safetyStart, safetyEnd, roleLevel, jobType} = values,
                result = {}
            result.type = type
            result.content = type === PublicRangeLevelType.hr ? "" : values[`content_${type}`]
            result.safetyStart = PublicRangeLevelType.hr || PublicRangeLevelType.job ? "" : safetyStart
            result.safetyEnd = PublicRangeLevelType.hr || PublicRangeLevelType.job ? "" : safetyEnd
            result.roleLevel = PublicRangeLevelType.role ? roleLevel : ""
            result.jobType = PublicRangeLevelType.job ? jobType : ""
            result.jobLevel = PublicRangeLevelType.job && jobType !== PublicRangeJobLevel.head ? values[`jobLevel_${jobType}`] : ""
        })
    }

    setModalAddVisible = (modalAddVisible) => {
        this.setState({modalAddVisible: modalAddVisible})
    }
}

export default AuthoritySelect