import React, {Component, Children} from 'react'
import T from 'prop-types'

import {Tooltip, Tabs, Modal, Button, Table,message} from 'antd'
const TabPane = Tabs.TabPane
const Confirm = Modal.confirm;

import WeaCircleImage from "../../../Component/wea-circle-image"
import WeaRate from "../../../Component/wea-rate"

import { WeaRichText } from 'ecCom'

//import WeaCkEditor from "../../../Component/wea-ckeditor/CKEAppend"
import CommentList from "./CommentList"
import WorkRecordList from "./WorkRecordList"
import CommentBar from "./CommentBar"
import WeiboBar from "./WeiboBar"

import AgreeContainer from './AgreeContainer'

import ComVar from "../ComVar"
const {TaskType, Moods, PublicRangeType, ComeFrom, WeiboType} = ComVar

import WeaUtils from "../../../Component/wea-utils"
const {DateFormat} = WeaUtils

import WeaAnnularIcon from "../../../Component/wea-annular-icon"
import WeaBlogDict from '../../../Component/wea-blog-dict';
const innerHref = WeaBlogDict.href;

import Mood from "./Mood"

import * as Util from '../../../../util/index'
import WeiboMapDialog from './WeiboMapDialog';
import Dict from '../../../Component/wea-blog-dict';

const mirror = Dict.bottomBarMirror;
const Authority = {
    one: 'one',
    many: 'many',
    all: 'all'
}

class WeiboContainer extends Component {
    static contextTypes = {
        router: T.routerShape
    }

    constructor(props) {
        super(props)
        this.state = {
            showWorkRecord: false,
            showScore: false,

            modalPublicViewVisible: false,

            commenting: false,

            authType: PublicRangeType.public,
            authRange: [],
            editing: false,

            showEdit: false,
            showComment: false,
            isRemind: false
        }
        this.onTempletChange = this.onTempletChange.bind(this);
    }

    componentDidMount() {
        let dom = ReactDOM.findDOMNode(this);
        WeaUtils.imgZoom(dom, ".wea-mbwc-normal");
    }

    componentDidUpdate() {
        let dom = ReactDOM.findDOMNode(this);
        WeaUtils.imgZoom(dom, ".wea-mbwc-normal");
    }
    render() {
        let {showScore, showWorkRecord, commenting, editing, authType, authRange, showEdit, showComment} = this.state,
            {
                _key, actions,
                children, blogId, blogType, isLast, currentUser = {}, date = "", user, type, workdate,
                mood, authority, isAgree, score, content, defaultTemplate, agrees,replenishTitle,
                systemWorkLog = {}, comments = [],
                isCanReplenish, isShowLog, isCanEdit, isUnSumitRemind, isCanZan, isMeCanScored, 
                isHasLocation, isManagerScore, isShowReadOrUnread, isReaded, weiboCKEditorConfig,
                hasEditor, hasImage, hasScroll
            } = this.props
        let customers = [],
            workflows = [],
            cooperations = [],
            sysWorkLog = systemWorkLog && systemWorkLog.length > 0 ? systemWorkLog : [];
        content || (content = defaultTemplate.tempContent);
        sysWorkLog.forEach(
            item => {
                switch (item.tasktype * 1) {
                    case TaskType.workflow:
                        item.url = "/workflow/request/ViewRequest.jsp?isovertime=0&requestid=" + item.taskid
                        workflows.push(item);
                        break;
                    case TaskType.cooperation:
                        item.url = "/cowork/ViewCoWork.jsp?id=" + item.taskid
                        cooperations.push(item);
                        break;
                    case TaskType.customer:
                        item.url = "/CRM/data/ViewCustomer.jsp?CustomerID=" + item.taskid
                        customers.push(item);
                        break;
                }
            }
        )
        const workRecords = [
            {name: '客户', data: customers, count: customers.length},
            {name: '流程', data: workflows, count: workflows.length},
            {name: '协作', data: cooperations, count: cooperations.length}
        ]

        const typeIcon = {
            current: {
                name: "current",
                icon: "icon-blog-solid",
                color: "#32a8ff",
                tip: "编辑中 "
            },
            normal: {
                name: "normal",
                icon: "icon-blog-Submit",
                color: "#75bf26",
                tip: "提交于 "
            },
            expired: {
                name: "expired",
                icon: "icon-blog-delay",
                color: "#f5b400",
                tip: "补交于 "
            },
            white: {
                name: "white",
                icon: "icon-blog-Uncommitted",
                color: "#ff3044",
                tip: "未提交 "
            }
        }

        //anruo test table
        const publics = []
        for (let i = 0; i < 100; i++) {
            publics.push({
                type: `部门${i}`,
                content: `需求分析${i}`,
                level: `${i} - 100`
            })
        }

		this.ckeId = '';
        let ckeId = ""
        if (blogType && workdate && user.id) {
            if (blogId) {
                ckeId = `wea_ckeditor_weibo_${blogType}_${workdate}_${user.id}_${blogId || ""}`
            } else {
                ckeId = `wea_ckeditor_weibo_${blogType}_${workdate}_${user.id}_anruo_${_key}`
            }
            this.ckeId = ckeId;
        }
        
        //暂时接入过来 zxt
        let bottomBarConfig = [];
        
        weiboCKEditorConfig && weiboCKEditorConfig.map(cg => {
        	cg.name === 'selects' && bottomBarConfig.push({
        		name: 'Component', 
        		show: <Mood ref={ckeId + '_mood'} onSelect={mood => {}}/>
        	})
        	cg.name === 'dialogs' && cg.items.map(item => {
	        	let obj = { 
	        		isBlog: true,
	        		name: 'Browser', 
	        		show: <div className="wea-cb-item">
	                    <span className={`wea-cbi-icon ${mirror[item].icon}`}/>
	                    <span className="wea-cbi-text">{mirror[item].name}</span>
	                </div>,
	        		type: mirror[item].type, 
	        		title: mirror[item].name,
	        		isSingle: item === 'Templet',
	        	};
	        	if(item === 'Templet') obj.onToolsChange = this.onTempletChange
	        	bottomBarConfig.push(obj);
	        })
        	cg.name === 'uploads' && cg.items.map(item => {
	        	bottomBarConfig.push({ 
	        		isBlog: true,
	        		name: 'Upload', 
	        		show: <div className="wea-cb-item">
	        	        <span className='wea-cbi-icon icon-blog-Enclosure'/>
	        	        <span className="wea-cbi-text">附件</span>
	        	    </div>,
	        		uploadId: 'webo_edit', 
	        		uploadUrl: `${item.Attachment.uploadUrl}?category=${item.Attachment.position}`, 
	        		category: item.Attachment.position, 
	        		maxUploadSize: item.Attachment.maxSize,
	        		style: {display: "inline-block", padding: 0}
	        	})
	        })
        })
        //暂时接入过来 zxt
    
        return (
            <div>
                <div
                    style={{display: (editing ? "none" : "")}}
                    className={'wea-mbw-container wea-mbw-container-' + (isLast ? 'last' : 'other')}
                    onMouseEnter={this.onMouseEnter}
                    onMouseLeave={this.onMouseLeave}
                >
                    {
                        isManagerScore ? (
                            score ? (
                                isMeCanScored ? (
                                    <WeaRate
                                        className="wea-mbwc-score"
                                        value={score}
                                        onChange={this.onScoreChange}
                                    />
                                ) : (
                                    <WeaRate
                                        className="wea-mbwc-score"
                                        value={score}
                                        disabled={true}
                                    />
                                )
                            ) : (
                                showScore ? (
                                    isMeCanScored ? (
                                        <WeaRate
                                            className="wea-mbwc-score"
                                            value={score}
                                            onChange={this.onScoreChange}
                                        />
                                    ) : (
                                        <WeaRate
                                            className="wea-mbwc-score"
                                            value={score}
                                            disabled={true}
                                        />
                                    )
                                ) : null
                            )
                        ) : null
                    }
                    {
                        !hasScroll ? <Tooltip title={replenishTitle} placement="leftBottom"><div className="wea-mbwc-type"><WeaAnnularIcon {...typeIcon[type]}/></div></Tooltip> : null
                    }
                    <WeaCircleImage
                        name={user.name}
                        diameter={40}
                        src={user.src}
                        style={{
                            margin: "0 6px 0 10px",
                            position: "absolute",
                            display: (hasImage ? "inline-block" : "none")
                        }}
                    />

                    <div style={{marginLeft: (hasImage ? "56px" : "0")}}>
                        <div className="wea-mbwc-top" style={{marginTop: (!hasImage ? "-10px" : "0")}}>
                            <span
                                className="wea-mbwct-item wea-mbwct-name"
                                onClick={() => window.open(innerHref.blog(user.id))}
                            >
                                {user.name || ""}
                               </span>
                            <span
                                className={'wea-mbwct-item wea-mbwct-mood ' + (mood ? (mood.itemName == Moods.happy ? "icon-blog-Emoji" : (mood.itemName == Moods.sad ? "icon-blog-Unhappy" : "")) : "")}
                            />
                            {
                                isShowReadOrUnread ? (
                                    !isReaded ? (
                                        <span className="wea-mbwct-item wea-mbwct-readed icon-blog-NewMessage"/>
                                    ) : ""
                                ) : ""
                            }
                            {
                                authority == Authority.one ? (
                                    <div className="wea-mbwct-item">
                                        <span className="authority-item authority-one">私密</span>
                                    </div>
                                ) : (
                                    authority == Authority.many ? (
                                        <div className="wea-mbwct-item">
                                            <span
                                                className="authority-item authority-many icon-blog-VisiblePart"
                                                onClick={() => this.setModalPublicViewVisible(true)}
                                            >&nbsp;部分公开</span>
                                        </div>
                                    ) : ''
                                )
                            }
                        </div>
                        <div style={{padding: (!hasImage ? "0 12px 0 12px" : "0")}}>
    
                            {Children.only(children)}
                            {isHasLocation == '1' ?
                                <div style={{margin: "15px 0 0 10px", fontSize: "14px"}}>
                                    <a onClick={this.openMap}><span className="icon-coms-position" style={{marginRight: "4px"}}/>查看地图</a>
                                </div> : undefined
                            }
                            <div className="wea-mbwc-bottom">
                            <span
                                className="wea-mbwcb-item wea-mbwcb-left">{DateFormat(date, "yyyy年MM月dd日 HH:mm")}</span>
                                <div className="wea-mbwcb-item wea-mbwcb-right">
                                    {
                                        isUnSumitRemind ? (
                                            <span
                                                className="wea-mbwcbr-item icon-blog-remind"
                                                onClick={this.onRemindClick}
                                            ><span
                                                style={{fontSize: "12px"}}>{this.state.isRemind ? "已提醒" : " 提醒"}</span></span>
                                        ) : ""
                                    }
                                    {
                                        isCanReplenish ? (
                                            <span
                                                className="wea-mbwcbr-item icon-blog-Pay"
                                                onClick={() => this.onEditClick(ckeId + "_edit", defaultTemplate.tempContent)}
                                            ><span style={{fontSize: "12px"}}>&nbsp;补交</span></span>
                                        ) : ""
                                    }
                                    {
                                        isCanEdit ? (
                                            <span
                                                className="wea-mbwcbr-item icon-blog-Pay"
                                                onClick={() => this.onEditClick(ckeId + "_edit", content)}
                                            ><span style={{fontSize: "12px"}}>&nbsp;编辑</span></span>
                                        ) : ""
                                    }
                                    <span
                                        className="wea-mbwcbr-item icon-blog-Comment"
                                        onClick={this.onCommentClick.bind(this, ckeId + "_comment")}
                                        style={(commenting ? {color: "#32a8ff", cursor: "default"} : {
                                            color: "#484848",
                                            cursor: "pointer"
                                        })}
                                    ><span style={{fontSize: "12px"}}>&nbsp;评论</span></span>
                                    {
                                        isCanZan ? (
                                            <span
                                                className="wea-mbwcbr-item icon-blog-Good"
                                                style={ {color: isAgree ? "#32a8ff" : "#484848"}}
                                                onClick={this.onAgreeClick}
                                            ><span style={{fontSize: "12px"}}>&nbsp;点赞</span></span>
                                        ) : ''
                                    }
                                    {
                                        isShowLog ? (
                                            <span
                                                className={'wea-mbwcbr-item icon-blog-' + (showWorkRecord ? 'up' : 'down')}
                                                onClick={this.onWorkRecordClick}
                                            ><span style={{fontSize: "12px"}}>&nbsp;系统工作记录</span></span>
                                        ) : ""
                                    }
                                </div>
                            </div>
                            <AgreeContainer
                                data={agrees}
                                me={currentUser.id || ""}
                                style={{margin: '10px 10px 0 10px'}}
                            />
                            <CommentList
                                data={comments || []}
                                blogId={blogId}
                                actions={actions}
                                user={user}
                                blogType={blogType}
                                hasEditor={hasEditor}
                                weiboCKEditorConfig={weiboCKEditorConfig}
                            />
                            {
                                showWorkRecord ? (
                                    <Tabs type="card" className="wea-nbwc-workRecord" animated={false}>
                                        {
                                            workRecords.map(
                                                (item, key) => {
                                                    return (
                                                        <TabPane
                                                            tab={item.name + ' ' + (item.count ? '(' + item.count + ')' : '')}
                                                            key={key + 1}>
                                                            <WorkRecordList data={item.data || []}/>
                                                        </TabPane>
                                                    )
                                                }
                                            )
                                        }
                                    </Tabs>
                                ) : null
                            }
                            {
                                authority == Authority.many ? (
                                    <Modal
                                        title={
                                            <div>
                                                <div className="icon-circle-base" style={{backgroundColor:'#6d3cf7'}}><span className='icon-coms-blog'/></div>
                                                <span
                                                    style={{
                                                        lineHeight: '28px',
                                                        fontSize: '16px',
                                                        verticalAlign: 'top',
                                                        marginLeft: '4px',
                                                        fontWeight: 'normal'
                                                    }}
                                                >&nbsp;
                                                    查看公开范围
                                        </span>
                                            </div>
                                        }
                                        footer={
                                            <Button key="back" type="ghost" size="large"
                                                    onClick={() => this.setModalPublicViewVisible(false)}
                                            >取消</Button>
                                        }
                                        wrapClassName="wea-modal wea-vertical-center-modal wea-nopadding-modal"
                                        width="720"
                                        visible={this.state.modalPublicViewVisible}
                                        onOk={() => this.setModalPublicViewVisible(false)}
                                        onCancel={() => this.setModalPublicViewVisible(false)}
                                    >
                                        <Table
                                            className="wea-table wea-table-pagination-margin"
                                            style={{height: '520px'}}
                                            scroll={{x: true, y: 420}}
                                            dataSource={publics}
                                            pagination={{
                                                total: publics.length,
                                                showSizeChanger: true,
                                                showQuickJumper: true,
                                                showTotal: this.onPublicViewShowTotalChange,
                                                onShowSizeChange: this.onPublicViewShowSizeChange,
                                                onChange: this.onPublicViewChange
                                            }}
                                            columns={[
                                                {
                                                    title: '类型',
                                                    dataIndex: 'type',
                                                    key: 'type',
                                                    width: 180,
                                                    className: "wea-table-column-padding"
                                                },
                                                {title: '条件内容', dataIndex: 'content', key: 'content', width: 420},
                                                {title: '安全级别', dataIndex: 'level', key: 'level', width: 120}
                                            ]}
                                        />
                                    </Modal>
                                ) : ''
                            }
                            {
                                ckeId && showComment ? (
                                    <div>
                                    	<div className='wea-ckeditor-import-ecCom' style={{padding: "12px 10px 0 10px", minHeight: ComVar.commentMinHeight+64}}>
	                                    	<WeaRichText 
							            		id={ckeId + "_comment"}
							                    ref={ckeId + "_comment"}
							                    value={""}
							                    ckConfig={{
							                    	toolbar:[
											            { name: 'document', items: [ 'Source'] },
											            { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
											            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
											            { name: 'colors', items: [ 'TextColor' ] },
											            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
											            { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] }
											        ],
											        extraPlugins: 'autogrow',
							                        height: ComVar.commentMinHeight,
							                        autoGrow_minHeight:ComVar.commentMinHeight,
							                        autoGrow_maxHeight: 600,
							                        removePlugins: 'resize',
							                        uploadUrl: '/api/blog/fileupload/uploadimage',
							                    }}
							                    extentsConfig
							                    bottomBarConfig
							                    onChange={v=>{}}
							                    onStatusChange={s => {}}
							                    onToolsChange={Util.transfStr}
							            	/>
						            	</div>
                                        <CommentBar
                                            setPrivate={this.setPrivate}
                                            submitComment={this.submitComment.bind(this, ckeId + "_comment")}
                                            cancelComment={this.cancelComment.bind(this, ckeId + "_comment")}
                                            style={{margin: "12px 12px 10px 12px", display: (commenting ? "" : "none")}}
                                        />
                                    </div>
                                ) : <div/>
                            }
                        </div>
                    </div>
                </div>
                {
                    ckeId && showEdit ? (
                        <div className='wea-ckeditor-import-ecCom'>
                            <div style={{minHeight: 251}}>
                                <WeaRichText
                                    id={ckeId + "_edit"}
                                    ref={ckeId + "_edit"}
                                    value={content}
                                    ckConfig={{
                                        toolbar:[
                                            { name: 'document', items: [ 'Source'] },
                                            { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
                                            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
                                            { name: 'colors', items: [ 'TextColor' ] },
                                            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
                                            { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] }
                                        ],
                                        extraPlugins: 'autogrow',
                                        height:150,
                                        autoGrow_minHeight: 150,
                                        autoGrow_maxHeight: 600,
                                        removePlugins: 'resize',
                                        uploadUrl: '/api/blog/fileupload/uploadimage',
                                    }}
                                    extentsConfig
                                    bottomBarConfig={ bottomBarConfig }
                                    onChange={v=>{}}
                                    onStatusChange={s => {}}
                                    onToolsChange={Util.transfStr}
                                />
                            </div>
                            <WeiboBar
                                authType={authType}
                                authRange={authRange}
                                onAuthType={this.onAuthType}
                                onAuthRange={this.onAuthRange}
                                cancelClick={this.cancelWeibo.bind(this, ckeId + "_edit")}
                                publishClick={this.publishWeibo.bind(this, ckeId)}
                                style={{
                                    display: (editing ? "" : "none"),
                                    borderTop: "none",
                                    borderBottom: "none",
                                    padding: "10px 16px 8px 16px"
                                }}
                            />
                        </div>
                    ) : <div/>
                }
                <WeiboMapDialog ref="map" discussId={blogId}/>
            </div>
        )
    }
    onTempletChange(name, ids, list, type){
    	Util.templetChange(this.refs[this.ckeId + "_edit"].getData(),v =>this.refs[this.ckeId + "_edit"].setData(v), name, ids, list, type)
    }

    onMouseEnter = () => {
        const {isMeCanScored, isShowReadOrUnread, isReaded} = this.props
        if (isMeCanScored) this.setState({showScore: true})
        if (isShowReadOrUnread && !isReaded) {
            const {actions, blogId, user, workdate} = this.props
            actions.setReaded(
                {
                    blogId: user.id,
                    discussId: blogId,
                    workdate: workdate,
                    userId: user.id
                },
                result => {
                    if (result.status == "1") {
                        actions.getBasicInfo()
                    }
                }
            )
        }
    }

    onMouseLeave = () => {
        const {isMeCanScored} = this.props
        if (isMeCanScored) this.setState({showScore: false})

    }

    onScoreChange = (value) => {
        let {actions, blogId, workdate, user} = this.props
        actions.setScore({
            discussId: blogId,
            score: value,
            workdate: workdate,
            userId: user.id
        })
    }

    onAuthType = (authType) => {
        this.setState({authType: authType})
    }

    onAuthRange = (authRange) => {
        this.setState({authRange: authRange})
    }

    onCommentClick = (ckeId) => {
        if (!this.state.commenting) {
            this.setState(
                {showComment: true, commenting: true}
//              () => {

//                  let { createEditor } = this.refs[ckeId],
//                      succeedExec = () => {
//                          createEditor()
//                          this.setState({commenting: true})
//                      },
//                      failExec = () => {
//                          this.setState({commenting: false})
//                      }
//                  succeedExec()
//                  // if (CountEditor() == (this.props.hasEditor ? 1 : 0)) {
//                  //     succeedExec()
//                  // } else {
//                  //     Confirm({
//                  //         title: '你确定继续评论！',
//                  //         content: '你有未保存的操作信息，请先关闭再继续评论！',
//                  //         onOk: () => {
//                  //             succeedExec()
//                  //         },
//                  //         onCancel: () => {
//                  //             failExec()
//                  //         }
//                  //     })
//                  // }
//              }
            )
        }
    }

    setPrivate = (event) => {
        this.setState({isPrivate: event.target.checked})
    }

    submitComment = (ckeId) => {
        let ckref = this.refs[ckeId];
        if (!ckref.checkMode()) {
            message.error("不能以源码模式或markdown模式保存，请将编辑器切换到可视化模式");
            return;
        }
        let {actions, blogId, blogType, user, workdate} = this.props
        this.setState({commenting: false})
    
        let params = {
            content: this.refs[ckeId].getData() || '',
            bediscussantId: user.id,
            workdate: workdate,
            commentType: this.state.isPrivate ? 1 : 0,
            comefrom: ComeFrom.pc
        }
        if (blogId) params.discussId = blogId
        actions.editComment(
            params,
            {
                type: blogType,
                workdate: workdate,
                discussId: blogId,
                userId: user.id
            }
        )
        this.refs[ckeId].removeEditor();
        this.setState({showComment: false})
    }

    cancelComment = (ckeId) => {
        this.refs[ckeId].removeEditor()
        this.setState({commenting: false, showComment: false})
    }

    onAgreeClick = () => {
        let {actions, blogId, blogType, currentUser, workdate, isAgree, user} = this.props

        let params = {
            workdate: workdate,
            userId: currentUser.id,
            optType: isAgree ? 0 : 1
        }
        if (blogId) params.discussId = blogId
        actions.editAgree(
            params,
            {
                type: blogType,
                workdate: workdate,
                discussId: blogId,
                userId: user.id
            }
        )
    }

    onEditClick = (ckeId, content) => {
        if (!this.state.editing) {
            this.setState(
                {showEdit: true, editing: true}
            )
        }
    }

    cancelWeibo = (ckeId) => {
        this.refs[ckeId].removeEditor()
        this.setState({editing: false, showEdit: false})
    }

    publishWeibo = (ckeId) => {
        let {actions, blogId, blogType, user} = this.props,
            {authType, authRange} = this.state,
            workdate = DateFormat(this.props.workdate, "yyyy-MM-dd")
        let ckref = this.refs[ckeId + '_edit'];
        if (!ckref.checkMode()) {
            message.error("不能以源码模式或markdown模式保存，请将编辑器切换到可视化模式");
            return;
        }
        this.setState({editing: false})

        let params = {
            appItemId: this.refs[ckeId + '_mood'].getValue() || "1",
            comefrom: ComeFrom.pc,
            content: this.refs[ckeId + '_edit'].getData(),
            workdate: workdate
        }
        if (blogId) params.discussId = blogId
        actions.editWeibo(
            params,
            {
                type: blogType,
                workdate: workdate,
                discussId: blogId,
                userId: user.id
            },
            result => {
                if (result.status == "1" && !blogId) {
                    actions.getBasicInfo()
                }
            }
        )

        this.setState({showEdit: false})
    }

    onRemindClick = () => {
        let {actions, user, workdate} = this.props;
        let params = {workdate: workdate, remindId: user.id};
        actions.remind(params, (result) => {
            this.setState({
                isRemind: true
            });
        });
    }

    onWorkRecordClick = () => {
        const {showWorkRecord} = this.state;
        this.setState({showWorkRecord: !showWorkRecord})

        let {actions, blogId, blogType, workdate, user} = this.props

        if (!showWorkRecord) {
            actions.getSystemWorkLog(
                {
                    workdate: workdate,
                    blogId: user.id
                },
                {
                    type: blogType,
                    workdate: workdate,
                    discussId: blogId,
                    userId: user.id,
                }
            )
        }
    }

    setModalPublicViewVisible = (modalPublicViewVisible) => {
        this.setState({modalPublicViewVisible: modalPublicViewVisible})
    }

    onPublicViewShowSizeChange = (current, pageSize) => {
    }
    onPublicViewChange = (current) => {

    }

    onPublicViewShowTotalChange = (total) => {
        return `共 ${total} 条`
    }
    openMap = () => {
        this.refs.map.open();
    }
}

export default WeiboContainer

//<WeaCkEditor
//  id={ckeId + "_comment"}
//  ref={ckeId + "_comment"}
//  type="blog"
//  style={{padding: "12px 10px 0 10px"}}
//  config={{
//      extraPlugins: 'autogrow',
//      height: 200,
//      autoGrow_minHeight: 200,
//      autoGrow_maxHeight: 600,
//      //autoGrow_bottomSpace: 50,
//      removePlugins: 'resize'
//  }}
//  // typeBar="blog"
//  // configBar={{}}
//  //onBlur={this.cancelComment.bind(this, ckeId+"_comment")}
///>

//<WeaCkEditor
//  id={ckeId + "_edit"}
//  ref={ckeId + "_edit"}
//  type="blog"
//  config={{
//      extraPlugins: 'autogrow',
//      height: 200,
//      autoGrow_minHeight: 200,
//      autoGrow_maxHeight: 600,
//      //autoGrow_bottomSpace: 50,
//      removePlugins: 'resize'
//  }}
//  configBar={weiboCKEditorConfig}
//  //onBlur={this.cancelWeibo.bind(this, ckeId+"_edit")}
///>