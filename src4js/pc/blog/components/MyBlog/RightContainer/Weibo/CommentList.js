import React, {Component} from 'react'

import {Tabs, Modal, Button, Table, message} from 'antd'
const Confirm = Modal.confirm;

import { WeaRichText } from 'ecCom'

//import WeaCkEditor from "../../../Component/wea-ckeditor/CKEAppend"

import CommentBar from "./CommentBar"

import ComVar from "../ComVar"
const {ComeFrom} = ComVar

import WeaUtils from '../../../Component/wea-utils'
const {DateFormat} = WeaUtils
import WeaBlogDict from '../../../Component/wea-blog-dict';
const innerHref = WeaBlogDict.href;

import * as Util from '../../../../util/index'

class CommentItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isPrivate: false,
            commenting: false
        }
    }

    componentDidMount() {
        let dom = ReactDOM.findDOMNode(this);
        WeaUtils.imgZoom(dom, ".wea-cl-content");
    }

    componentDidUpdate() {
        let dom = ReactDOM.findDOMNode(this);
        WeaUtils.imgZoom(dom, ".wea-cl-content");
    }
    render() {
        const {commenting, isPrivate} = this.state,
            {data, blogId, blogType, user, _key, weiboCKEditorConfig} = this.props,
            {id, userid, username, createdate = "", createtime = "", content, comefrom, comefromStr, commentType, isCanDelete} = data

        let ckeId = ""
        if (blogType && data.workdate && user.id) {
            if (data.id) {
                ckeId = `wea_ckeditor_weibo_${blogId || ""}_${blogType}_${data.workdate}_${user.id}_comment_list_${data.id}`
            } else {
                ckeId = `wea_ckeditor_weibo_${blogId || ""}_${blogType}_${data.workdate}_${user.id}_comment_list_anruo_${_key}`
            }
        }
        
        return (
            <li>
                <div className="wea-cl-title">
                    <a className="wea-clt-item wea-clt-name"
                       href={innerHref.blog(userid)}>{username || ""}</a>
                    <span className="wea-clt-item wea-clt-date">
                        {DateFormat(createdate + " " + createtime, "yyyy年MM月dd日 HH:mm")}
                    </span>
                    {
                        commentType * 1 ? (
                            <span className="wea-clt-item wea-clt-private">(私评)</span>
                        ) : ""
                    }
                    {
                        comefrom * 1 ? (
                            <span
                                className="wea-clt-item wea-clt-from">{comefromStr}</span>
                        ) : ""
                    }
                    {
                        isCanDelete * 1 ? (
                            <span
                                className="wea-clt-item wea-clt-delete icon-blog-delete"
                                title="删除"
                                onClick={this.deleteComment.bind(this, ckeId)}
                            />
                        ) : ""
                    }
                    <span
                        className="wea-clt-item wea-clt-add icon-blog-Comment"
                        onClick={this.addComment.bind(this, ckeId)}
                        title="评论"
                        style={(commenting ? {color: "#32a8ff", cursor: "default"} : {
                            color: "#2e4b7c",
                            cursor: "pointer"
                        })}
                    />
                </div>
                <div
                    className="wea-cl-content"
                    dangerouslySetInnerHTML={{__html: `<div class="wea-ckeditor-content">${content}</div>`}}
                />
                {
                    ckeId ? (
                        <div>
                        	{ commenting && <div className='wea-ckeditor-import-ecCom' style={{padding: "12px 12px 0 12px", minHeight: ComVar.commentMinHeight + 64}}>
		                        	<WeaRichText
					            		id={ckeId}
					                    ref={ckeId}
					                    value={`<a href="${innerHref.blog(userid)}" target="_blank">@${username}</a>&nbsp;`}
					                    ckConfig={{
					                    	toolbar:[
									            { name: 'document', items: [ 'Source'] },
									            { name: 'paragraph', items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'NumberedList', 'BulletedList' ] },
									            { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
									            { name: 'colors', items: [ 'TextColor' ] },
									            { name: 'basicstyles', items: [ 'Bold', 'Italic', 'Underline', 'Strike', ] },
									            { name: 'insert', items: [ 'Image', 'Table', 'Smiley'] }
									        ],
									        extraPlugins: 'autogrow',
                                            height: ComVar.commentMinHeight,
                                            autoGrow_minHeight:ComVar.commentMinHeight,
					                        autoGrow_maxHeight: 300,
					                        removePlugins: 'resize',
					                        uploadUrl: '/api/blog/fileupload/uploadimage',
					                    }}
					                    extentsConfig
					                    bottomBarConfig
					                    onChange={v=>{}}
					                    onStatusChange={s => {}}
					                    onToolsChange={Util.transfStr}
					            	/>
				            	</div>
			            	}
                            <CommentBar
                                setPrivate={this.setPrivate}
                                submitComment={this.submitComment.bind(this, ckeId)}
                                cancelComment={this.cancelComment.bind(this, ckeId)}
                                style={{display: (commenting ? "" : "none"), margin: "12px 12px 10px 12px"}}
                            />
                        </div>
                    ) : <div/>
                }
            </li>
        )

    }

    deleteComment = () => {
        let {actions, blogId, blogType, data, user} = this.props,
            {id, workdate} = data

        let params = {
            replyId: id,
            workdate: workdate
        }
        if (blogId) params.discussId = blogId
        actions.deleteComment(
            params,
            {
                type: blogType,
                workdate: workdate,
                discussId: blogId,
                userId: user.id
            }
        )
    }

    addComment = (ckeId) => {
        if (!this.state.commenting) {
            let {blogId, data} = this.props,
                {userid, username} = data,
                succeedExec = () => {
                    this.setState({commenting: true})
                },
                failExec = () => {
                    this.setState({commenting: false})
                }
            succeedExec()
            // if (CountEditor() === (this.props.hasEditor ? 1 : 0)) {
            //     succeedExec()
            // } else {
            //     Confirm({
            //         title: '你确定继续评论！',
            //         content: '你有未保存的操作信息，请先关闭再继续评论！',
            //         onOk: () => {
            //             succeedExec()
            //         },
            //         onCancel: () => {
            //             failExec()
            //         }
            //     })
            // }
        }
    }

    setPrivate = (event) => {
        this.setState({isPrivate: event.target.checked})
    }

    submitComment = (ckeId) => {
        let contents = this.refs[ckeId].getData(),
            {data, blogId, blogType, user} = this.props,
            {workdate, username, userid, id, bediscussantid} = data;
        let ckref = this.refs[ckeId];
        if (!ckref.checkMode()) {
            message.error("不能以源码模式或markdown模式保存，请将编辑器切换到可视化模式");
            return;
        }
        this.setState({commenting: false})

        let params = {
            content: contents,
            bediscussantId: bediscussantid,
            workdate: workdate,
            commentType: this.state.isPrivate ? 1 : 0,
            replyId: id,
            relatedId: userid,
            comefrom: ComeFrom.pc
        }
        if (blogId) params.discussId = blogId
        this.props.actions.editComment(
            params,
            {
                type: blogType,
                workdate: workdate,
                discussId: blogId,
                userId: user.id
            }
        )
    }

    cancelComment = (ckeId) => {
        this.setState({commenting: false})
    }
}

export default function CommentList(props) {
    const {data, blogId, blogType, user, actions, weiboCKEditorConfig} = props
    if (data && data.length > 0) {
        return (
            <ul className="wea-comment-list">
                {
                    data.map(
                        (item, key) => {
                            return (
                                <CommentItem
                                    _key={key}
                                    data={item}
                                    user={user}
                                    blogId={blogId}
                                    blogType={blogType}
                                    actions={actions}
                                    weiboCKEditorConfig={weiboCKEditorConfig}
                                />
                            )
                        }
                    )
                }
            </ul>
        )
    } else {
        return <div/>
    }
}