import React from 'react'

import {Tabs, Modal, Button, Table, Pagination} from 'antd'
const Confirm = Modal.confirm;

class WorkRecordList extends React.Component {
	constructor(props) {
		super(props);
        let {data = []} = props;
        this.state = {
            total: data.length,
            curList: data.slice(0, 10),
            pageNum: 1,
            pageSize: 10,
        };
	}
	componentWillReceiveProps(nextProps) {
        let {data = []} = nextProps;
	    this.setState({
	        total: data.length,
            curList: data.slice(0, 10),
            pageNum: 1,
            pageSize: 10
        });
    }
    queryNext(pageNum) {
        let {data=[]} = this.props, {pageSize} = this.state;
        let curList = data.slice((pageNum - 1) * pageSize, pageNum * pageSize)
        this.setState({
            curList: curList,
            pageNum: pageNum
        });
    }
	render() {
        let {total, curList, pageNum} = this.state;
        return (
            <div>
                <ul className="wea-work-record-list">
                    {
                        total > 0 ? (
                            curList.map(
                                (item, key) => {
                                    const {taskname, taskcontent, url} = item
                                    let index = (pageNum - 1) * 10 + key + 1;
                                    return (
                                        <li>
                                            <div className="wea-wrl-item wea-wrl-title">
                                                <span className="wea-wrlt-item wea-wrlt-key">{index}</span>
                                                <span
                                                    className="wea-wrlt-item wea-wrlt-content"
                                                    onClick={() => window.open(url)}
                                                >{taskname}</span>
                                            </div>
                                            <div
                                                className="wea-wrl-item wea-wrl-content"
                                                dangerouslySetInnerHTML={{__html: `<div class="wea-ckeditor-content">${taskcontent}</div>`}}
                                            />
                                        </li>
                                    )
                                }
                            )
                        ) : (
                            <li className="wea-wrl-none">无记录</li>
                        )
                    }
                </ul>
                {total > 10 && <div style={{marginTop: "15px"}}><Pagination total={total} onChange={::this.queryNext} /></div>}
            </div>
        )
	}
}

export default WorkRecordList;