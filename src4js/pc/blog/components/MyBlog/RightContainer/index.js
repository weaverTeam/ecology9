import React, {Component} from 'react'
import PropTypes from 'react-router/lib/PropTypes'

import {Tabs, Tooltip, Spin} from 'antd'
const TabPane = Tabs.TabPane

import WeaUtils from "../../Component/wea-utils"
const {DateFormat, DateAddition} = WeaUtils

import TopSearchPanel from './Weibo/TopSearchPanel'
import Weibo from './Weibo/'

import Fans from "./Fan"
import Attentions from "./Attention"
import {WeaNewScrollPagination} from 'ecCom'
import {WeaNewScroll} from 'ecCom'

class RightContainer extends Component {
    static contextTypes = {
        router: PropTypes.routerShape
    }

    constructor(props) {
        super(props);
        let {cardType} = props;
        this.state = {...this.initData(), cardType: cardType}
        this.getWeiboListByAction = this.getWeiboListByAction.bind(this);
    }

    initData = () => {
        return {
            startSearch: false,
            searching: false,
            searchForm: {},
            weiboType: 1,
            weiboGroup: "all"
        }
    }

    componentWillReceiveProps(props) {
        if (props.cardType != this.state.cardType) {
            this.setState({...this.initData(), cardType: props.cardType})
        }
    }

    get currentType() {
        let {cardType} = this.props, {weiboType} = this.state;
        if (cardType * 1 == 1 && weiboType > 2) {
            weiboType = 1;
        }
        return cardType * 10 + weiboType * 1
    }

    set currentType(value) {
        this.setState({weiboType: value})
    }

    get currentGroup() {
        return this.state.weiboGroup
    }

    set clear(value) {
        if(value) this.setState({...this.initData()})
    }

    render() {
        let {cardType, userInfo, weiboCKEditorConfig, weiboGroup, weiboList, actions, topHeight, systemWorkLog, fanList,
                basicInfo: {countData = {}},
                attentionList, checkAttention, changeGroup, defaultTemplate, loading, keyWords} = this.props,
            {weiboType, startSearch} = this.state,
            containerHeight = topHeight - 120,
            commonProps = {
                actions: actions,
                userInfo: userInfo,
                height: containerHeight,
                systemWorkLog: systemWorkLog,
                //isReset: startSearch,
                defaultTemplate: defaultTemplate.defaultTemplate || {},
                weiboCKEditorConfig: weiboCKEditorConfig.appList
            },
            mainpage = [
                {id: 1, type: 11, name: "全部微博", data: weiboList, others: commonProps, hasImage: true},
                {id: 2, type: 12, name: "未读微博", data: weiboList, others: commonProps, hasImage: true},
            ],
            weibos = [
                {id: 1, type: 21, name: "微博", data: weiboList, others: commonProps, hasScroll: true, hasEditor: true},
                {id: 2, type: 22, name: "评论", data: weiboList, others: commonProps,commentCount : countData.commentCount || 0},
                {id: 3, type: 23, name: "赞", data: weiboList, others: commonProps},
            ],
                // {id: 4, type: 24, name: "@我", data: weiboList, others: commonProps}
            groups = weiboGroup.groupList || [],
            fans = fanList.attnList || [],
            attentions = attentionList.attentionList || [];
        let seq = 0
        groups.forEach(
            item => {
                const {groupId, groupName} = item
                if (groupId != "all" && groupId != "nogroup") {
                    mainpage.push(
                        {
                            id: 3 + seq,
                            type: 11,
                            name: groupName,
                            data: weiboList,
                            others: commonProps,
                            group: groupId,
                            hasImage: true,
                            weiboCKEditorConfig: weiboCKEditorConfig
                        }
                    )
                    seq++
                }
            }
        )
    
        return (
            <div className="wea-right-container">
            
                {
                    cardType == 1 ? (
                            <Tabs
                                animation={false}
                                activeKey={weiboType+""}
                                className="wea-tabs"
                                size="small"
                                onChange={this.changeWeiboType.bind(this, mainpage)}
                                tabBarExtraContent={
                                    weiboType == 1 ? (
                                        <TopSearchPanel
                                            ref="TopSearchPanel"
                                            requestData={this.getWeiboList.bind(this, null)}
                                            onChange={v=>actions.setKeywords(v)}
                                        />
                                    ) : ""
                                }
                            >
                                {this.getTabsDom(1, mainpage, loading, keyWords)}
                            </Tabs>
                    ) : ""
                }
                {
                    cardType == 2 ? (
                            <Tabs
                                animation={false}
                                activeKey={weiboType+""}
                                size="small"
                                onChange={this.changeWeiboType.bind(this, weibos)}
                                tabBarExtraContent={
                                    weiboType == 1 ? (
                                        <TopSearchPanel
                                            ref="TopSearchPanel"
                                            requestData={this.getWeiboList.bind(this, null)}
                                            onChange={v=>actions.setKeywords(v)}
                                        />
                                    ) : ""
                                }
                            >
                                {this.getTabsDom(2, weibos, loading, keyWords)}
                            </Tabs>
                    ) : ""
                }
                {
                    cardType == 3 ? (
                            <Fans
                                height={topHeight}
                                actions={actions}
                                data={fans}
                                changeGroup={changeGroup}
                            />
                    ) : ""
                }
                {
                    cardType == 4 ? (
                            <Attentions
                                ref="attentions"
                                weiboGroup={weiboGroup}
                                height={topHeight}
                                actions={actions}
                                data={attentions}
                                onCheck={checkAttention}
                                changeGroup={changeGroup}
                            />
                    ) : ""
                }
            </div>
        )
    }
    getTabsDom = (cardType, tabs, loading, keyWords) => {
        return tabs.map(
            (item, key) => {
                let {id, group, type, name, data, others, hasScroll, hasEditor, hasImage,commentCount} = item,
                    hasData = data.discussList && data.discussList.length > 0 , end = !data.__hasData,
                    topHeight = this.props.topHeight - 61;
                return (
                    <TabPane
                        tab={
                            <Tooltip
                                placement="bottomLeft"
                                title={name}
                                overlayStyle={{maxWidth: "200px"}}
                            >
                                <div>
                                    <span>{name}</span>
                                    {commentCount ? <span className="tip">{`(${commentCount})`}</span> : undefined}
                                </div>
                            </Tooltip>
                        }
                        key={id}
                    >
                    
                        <WeaNewScrollPagination onScrollEnd={this.fetchData} height={topHeight} loading={loading}>
                            <div className="wea-blog-spin-con">
                                <Weibo
                                    {...others}
                                    records={data}
                                    getList={this.getWeiboList.bind(this, type, group)}
                                    blogType={cardType * 100 + id * 1}
                                    hasScroll={hasScroll || false}
                                    hasEditor={hasEditor || false}
                                    hasImage={hasImage || false}
                                    keyWords={keyWords}
                                />
                                {hasData && loading && <div className="align-center pt10"><Spin tip="加载中，请稍候..."/></div>}
                                {hasData && !loading && end && <div className="align-center pt10">没有微博了</div>}
                            </div>
                        </WeaNewScrollPagination>
                    </TabPane>
                )
            }
        )
    }

    getWeiboList = (blogType, params) => {
        const {cardType, actions, weiboList} = this.props,
            {currentPage, minUpdateid} = weiboList,
            {weiboType, searching, searchForm} = this.state
        let others = {},
            result = {};
        switch (cardType * 1) {
            case 1 :  //微博主页tab相关页面
                if (blogType) {  //非自定义分组对应的微博，有blogtype值。
                    if (searching) {
                        result = {
                            ...searchForm,
                            currentPage: currentPage + 1,
                            groupId: "all"
                        }
                        this.searchWeiboMainPageListByAction(result)
                    } else {
						if(blogType == 12) {  //未读微博列表
							result = {
								...searchForm,
								minUpdateid: minUpdateid, //处理未读微博的特殊参数
								groupId: params || "all"
							}
						} else { //微博主页列表 及 自定义分组微博列表
							result = {
								...searchForm,
								currentPage: currentPage + 1,
								groupId: params || "all"
							}
						}
                        others = {type: blogType}
                        this.getWeiboListByAction(others, result)
                    }
                } else {
                    const {startDate, endDate, content} = params
                    result = {
                        startDate: DateFormat(startDate, "yyyy-MM-dd"),
                        endDate: DateFormat(endDate, "yyyy-MM-dd"),
                        content: content,
                        currentPage: 1,
                        groupId: "all"
                    }
                    this.searchWeiboMainPageListByAction(result)
                    this.props.changeReset(true)
                    this.setState({searching: true, searchForm: result, startSearch: true, weiboType: weiboType})
                }
                break;
            case 2:
                if (blogType) {
                    if (blogType == 21) {
                        if (searching) {
                            result = {
                                ...searchForm,
                                currentPage: currentPage + 1
                            }
                            this.searchWeiboListByAction(result)
                        } else {
                            const {discussList = []} = weiboList,
                                date = new Date(discussList[discussList.length - 1].workdate)
                            this.getWeiboListByAction({type: blogType}, {endDate: DateFormat(DateAddition(date, -1, "day"), "yyyy-MM-dd")})
                        }
                    } else {
                        others = {type: blogType},
                            result = {
                                currentPage: currentPage + 1
                            }
                        this.getWeiboListByAction(others, result)
                    }
                } else {
                    const {startDate, endDate, content} = params
                    result = {
                        startDate: DateFormat(startDate, "yyyy-MM-dd"),
                        endDate: DateFormat(endDate, "yyyy-MM-dd"),
                        content: content,
                        currentPage: 1,
                    }
                    this.searchWeiboListByAction(result)
                    this.props.changeReset(true)
                    this.setState({searching: true, searchForm: result, startSearch: true, weiboType: weiboType})
                }
                break;
            default:
                break;
        }

        this.setState({startSearch: false})
        this.props.changeReset(false)
    }
    
    getWeiboListByAction(others, params) {
        let {actions} = this.props;
        actions.getWeiboList(others, params)
    }
    searchWeiboListByAction(params) {
        let {actions} = this.props;
        actions.searchWeiboList(params);
    }
    searchWeiboMainPageListByAction(params) {
        let {actions} = this.props;
        actions.searchWeiboMainPageList(params);
    }

    changeWeiboType = (data, weiboType) => {
        let {actions, cardType} = this.props,
            params = {}
        switch (cardType * 1) {
            case 1 :
                switch (weiboType * 1) {
                    case 1 :
                        params = {currentPage: 1, groupId: "all"};
                        break;
                    case 2 :
                        params = {currentPage: 1};
                        break;
                    default:
                        params = {currentPage: 1, groupId: data[weiboType - 1].group};
                        break;
                }
                this.getWeiboListByAction({type: cardType * 10 + (weiboType > 2 ? 1 : weiboType * 1), status: true}, params)
                break;
            case 2 :
                switch (weiboType * 1) {
                    case 1 :
                        params = {endDate: DateFormat(new Date(), "yyyy-MM-dd")};
                        break;
                    case 2 :
                        params = {currentPage: 1, type: "reply"};
                        break;
                    case 3 :
                        params = {currentPage: 1, type: "zan"};
                        break;
                    case 4 :
                        params = {currentPage: 1, type: 'at'};
                        break;
                    default:
                        break;
                }
                this.getWeiboListByAction({type: cardType * 10 + weiboType * 1, status: true}, params)
                break;
            default:
                break;
        }
        this.setState({
            searching: false,
            searchForm: {},
            startSearch: false,
            weiboType: weiboType,
            weiboGroup: params.groupId
        })
    }
    fetchData = () => {
        const weiboList = this.props.weiboList;
        if (weiboList.__hasData)
            this.getWeiboList(this.currentType, this.currentGroup)
    }
}

export default RightContainer