import React, {Component} from 'react'
import PropTypes from 'react-router/lib/PropTypes'

import {WeaTop, WeaLeftRightLayout, WeaNewScroll} from 'ecCom'

import {Button} from 'antd'

import LeftContainer from './LeftContainer/index'
import RightContainer from './RightContainer/index'

import GroupSelect from "./RightContainer/Attention/GroupSelect"
import '../../css/myBlog/myBlog.less'
const P = 'wea-myBlog'

import WeaUtils from "../Component/wea-utils"
const {DateFormat} = WeaUtils

class MyBlog extends Component {
    static contextTypes = {
        router: PropTypes.routerShape
    }

    constructor(props) {
        super(props)
        this.state = {
            topHeight: 0,

            cardType: 2,
            attentionCheck: false,
            currentGroup: "all",

            isReset: false
        }
    }

    componentDidMount() {
        const {actions} = this.props
        actions.init();
    }

    render() {
        const {loading, title, actions, userInfo, weiboCKEditorConfig, indexInfo, basicInfo, receiverList,
                visitorList, weiboList, systemWorkLog,weiboGroup, fanList, attentionList,
                messageList, note, defaultTemplate, keyWords } = this.props,
            {cardType, topHeight, attentionCheck, currentGroup, isReset} = this.state,
            groups = weiboGroup.groupList || []
        let buttonList = [
            {
                name: "移动到组",
                disabled: !attentionCheck,
                title: "新建分组并移动",
                disabledGroup: currentGroup,
                onChange: this.selectGroup.bind(null, "move"),
                createGroup: this.selectGroup.bind(null, "createAndMove")
            },
            {
                name: "复制到组",
                disabled: !attentionCheck,
                title: "新建分组并复制",
                disabledGroup: currentGroup,
                onChange: this.selectGroup.bind(null, "copy"),
                createGroup: this.selectGroup.bind(null, "createAndCopy")
            },
            {
                name: "从该组删除",
                disabled: currentGroup == "all" || currentGroup == "nogroup" || (currentGroup != "all" && currentGroup != "nogroup" && !attentionCheck),
                cancelSelect: true,
                onClick: this.selectGroup.bind(null, "remove")
            },
            {
                name: "取消关注",
                disabled: !attentionCheck,
                cancelSelect: true,
                onClick: this.cancelAttention
            }
        ]
        let groupList = []
        groups.forEach(
            item => {
                if (item.groupId != "all" && item.groupId != "nogroup") {
                    groupList.push(item)
                }
            }
        )

        let renderContainer = () => (
            <WeaLeftRightLayout
                defaultShowLeft={true}
                leftCom={
                    <LeftContainer
                        cardType={cardType}
                        onClick={this.selectCard}
                        actions={actions}
                        userInfo={userInfo}
                        indexInfo={indexInfo}
                        basicInfo={basicInfo}
                        messageList={messageList}
                        receiverList={receiverList}
                        visitorList={visitorList}
                        weiboCKEditorConfig={weiboCKEditorConfig}
                        note={note}
                    />
                }
                leftWidth={25}
            >
                <RightContainer
                    ref="rightContainer"
                    checkAttention={this.checkAttention}
                    changeGroup={this.changeGroup}
                    cardType={cardType}
                    topHeight={topHeight}
                    actions={actions}
                    userInfo={userInfo}
                    weiboGroup={weiboGroup}
                    weiboList={weiboList}
                    basicInfo={basicInfo}
                    fanList={fanList}
                    defaultTemplate={defaultTemplate}
                    attentionList={attentionList}
                    systemWorkLog={systemWorkLog}
                    weiboCKEditorConfig={weiboCKEditorConfig}
                    changeReset={this.changeReset}
                    keyWords={keyWords}
                    loading={loading}
                />
            </WeaLeftRightLayout>
        )
    
        return (
            <div className={P}>
                <WeaTop
                    title={title}
                    loading={loading}
                    icon={<span className='icon-coms-blog'/>}
                    iconBgcolor="#6d3cf7"
                    buttonSpace={10}
                    buttons={
                        (cardType == 4 ? (
                            buttonList.map(
                                item => {
                                    let {name, title, disabled, disabledGroup, cancelSelect, ...rest} = item
                                    if (cancelSelect) {
                                        return (
                                            <Button
                                                type="primary"
                                                disabled={disabled} {...rest}
                                            >{name}</Button>
                                        )
                                    } else {
                                        return (
                                            <GroupSelect
                                                disabledGroup={disabledGroup}
                                                groups={groupList}
                                                title={title}
                                                {...rest}
                                            >
                                                <Button type="primary" disabled={disabled}>{name}</Button>
                                            </GroupSelect>
                                        )
                                    }
                                }
                            )
                        ) : [])
                    }
                    showDropIcon={false}
                    getHeight={this.getTopHeight}
                >
                    {renderContainer()}
                </WeaTop>
            </div>
        )
    }

    changeReset = (value) => {
        this.setState({isReset: value})
    }

    getTopHeight = (height) => {
        this.setState({topHeight: height})
    }

    selectCard = (currentType) => {
        //if (currentType != this.state.cardType) {
            const {actions} = this.props
            switch (currentType) {
                case 1 :
                    params = {currentPage: 1, groupId: "all"};
                    this.refs.rightContainer.getWeiboListByAction({type: currentType * 10 + 1, status: true}, params)
                    break;
                case 2 :
                    params = {endDate: DateFormat(new Date(), "yyyy-MM-dd")};
                    this.refs.rightContainer.getWeiboListByAction({type: currentType * 10 + 1, status: true}, params)
                    break;
                case 3 :
                    actions.getFanList()
                    break;
                case 4 :
                    actions.getAttentionList({groupId: "all"})
                default:
                    break;
            }
            this.setState({cardType: currentType})
            this.refs.rightContainer.clear = true
        //}
    }

    checkAttention = (value) => {
        this.setState({attentionCheck: value})
    }

    changeGroup = (value) => {
        this.setState({currentGroup: value.groupId})
    }

    cancelAttention = () => {
        let {actions} = this.props,
            {currentGroup} = this.state,
            ref = this.refs.rightContainer.refs.attentions
        actions.editAttitions(
            {idSet: ref.userIdsString, islowers: ref.isLowersString,},
            result => {
                if (result.status == "1") {
                    actions.getAttentionList({groupId: currentGroup})
                    actions.getBasicInfo()
                }
            }
        )
    }

    selectGroup = (type, obj) => {
        let {actions} = this.props,
            {currentGroup} = this.state,
            ref = this.refs.rightContainer.refs.attentions,
            idSet = ref.userIdsString
        switch (type) {
            case "copy":
                actions.copyGroup(
                    {idSet: idSet, destGroup: obj.value},
                    result => {
                        if (result.status == "1") {
                            actions.getWeiboGroup()
                            actions.getAttentionList({groupId: currentGroup})
                            ref.clearData = true
                        }
                    }
                )
                break;
            case "move":
                actions.moveGroup(
                    {idSet: idSet, sourceGroup: currentGroup, destGroup: obj.value},
                    result => {
                        if (result.status == "1") {
                            actions.getWeiboGroup()
                            actions.getAttentionList({groupId: currentGroup})
                            ref.clearData = true
                        }
                    }
                )
                break;
            case "remove":
                actions.removeGroup(
                    {idSet: idSet, sourceGroup: currentGroup},
                    result => {
                        if (result.status == "1") {
                            actions.getWeiboGroup()
                            actions.getAttentionList({groupId: currentGroup})
                            ref.clearData = true
                        }
                    }
                )
                break;
            case "createAndMove":
                actions.createAndMoveGroup(
                    {idSet: idSet, sourceGroup: currentGroup, groupName: obj},
                    result => {
                        if (result.status == "1") {
                            actions.getWeiboGroup()
                            actions.getAttentionList({groupId: currentGroup})
                            ref.clearData = true
                        }
                    }
                )
                break;
            case "createAndCopy":
                actions.createAndCopyGroup(
                    {idSet: idSet, groupName: obj},
                    result => {
                        if (result.status == "1") {
                            actions.getWeiboGroup()
                            actions.getAttentionList({groupId: currentGroup})
                            ref.clearData = true
                        }
                    }
                )
                break;
            default:
                break;
        }
    }
}

export default MyBlog