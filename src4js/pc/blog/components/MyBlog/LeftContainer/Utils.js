import React from 'react'
import {Rate, Tooltip, Button} from 'antd'

import '../../../css/myBlog/leftContainer/utils.less'

import WeaCircleImage from '../../Component/wea-circle-image'
import WeaBlogDict from '../../Component/wea-blog-dict';
import moment from 'moment';

const innerHref = WeaBlogDict.href;
const Utils = {}

Utils.example = (props) => {
    const {...rest} = props
    return (
        <div {...rest}>

        </div>
    )
}

Utils.RangeStar = (props) => {
    const getLength = value => {
        let length = value
        switch (true) {
            case value <= 0:
                length = 0
                break
            case value <= 5:
                let arr = value.toString().split('.')
                if (arr[1] >= 5) {
                    length = Number(arr[0]) + 0.5
                } else {
                    length = arr[0]
                }
                break
            case value > 5:
                length = 5
                break
            default:
                length = 0
                break
        }
        return length
    }
    const {name, value, tip} = props,
        length = getLength(value)
    return (
        <div className="wea-range-star">
            <span className="wea-rs-item wea-rs-title">{name + ' :'}</span>
            <Tooltip title={tip || "无信息"} placement="right">
                <div style={{display: "inline-block"}}>
                    <Rate allowHalf disabled value={length}/>
                    <span className="wea-rs-item wea-rs-value">{Number(value).toFixed(1)}</span>
                </div>
            </Tooltip>
        </div>
    )
}

Utils.BasicInfoCard = (props) => {
    const {isActive, name, value, color, className, border, onClick} = props,
        borderStyle = {}
    border.forEach(
        item => {
            borderStyle[`border-${item}`] = '1px solid #eaeaea'
        }
    )
    return (
        <div className={'wea-basic-info-card ' + className + (isActive ? " wea-basic-info-card-active" : "")}
             style={borderStyle} onClick={onClick}>
            <span className="wea-bic-item wea-bic-name">{name}</span>
            <span className="wea-bic-item wea-bic-value" style={{color: color}}>{value}</span>
        </div>
    )
}

Utils.ToolTipBar = (props) => {
    const {name, icon, color, tips, onClick} = props
    return (
        <div className="wea-tool-tip-bar" onClick={onClick}>
            <span className={'wea-ttb-item wea-ttb-icon ' + icon} style={{color: color}}/>
            <span className="wea-ttb-item wea-ttb-name">{name}</span>
            {
                tips ? (
                    <div className="wea-ttb-item wea-ttb-tip">
                        <span>{tips}</span>
                    </div> ) : null
            }
        </div>
    )
}

const CustomerItem = (props) => {
    const {userid, lastname, visitdatetime, imageUrl, type} = props
    let visitMoment = moment(visitdatetime);
    let smallDate = visitMoment.format("MM-DD");
    let title = type=='visit' ? 
        <span>
            <div>{lastname}于{visitdatetime}来访</div>
            <div>点击查看对方微博</div>
        </span> : 
        <span>
            <span>点击查看对方微博</span>
        </span>;
    return (
        <Tooltip title={title} placement="bottom">
            <li className="wea-customer-item">
                <a href={innerHref.blog(userid)} target="_blank">
                    <WeaCircleImage diameter={40} src={imageUrl || ""} name={lastname || ""}/>
                </a>
                <div className="wea-ci-desc">
                    <span
                    className="wea-cid-item wea-cid-name"
                    onClick={() => window.open(innerHref.blog(userid))}
                    >{lastname || ""}</span>
                    <div className="wea-cid-item wea-cid-date">{visitdatetime}</div>
                    <div className="wea-cid-smallDate">{smallDate}</div>
                </div>
            </li>
        </Tooltip>
    )
}

Utils.CustomerList = (props) => {
    const {type, records, getList} = props,
        {
            list = [],
            currentPage,
            totalSize,
            pageSize
        } = records,
        leftStatus = !(currentPage > 1),
        rightStatus = !(currentPage < (totalSize / pageSize)),
        onLeftClick = () => {
            getList({
                recordType: type,
                currentPage: currentPage - 1,
                perPageSize: pageSize
            })
        },
        onRightClick = () => {
            getList({
                recordType: type,
                currentPage: currentPage + 1,
                perPageSize: 5
            })
        }
    return (
        !!totalSize ? (
            <div className="wea-customer-list">
                <div className="wea-customer-switch">
                    <span
                        onClick={leftStatus ? () => {
                        } : onLeftClick}
                        className={"ant-tabs-tab-prev-icon wea-cs-item " + (leftStatus ? "wea-cs-disabled" : "")}
                    />
                    <span
                        onClick={rightStatus ? () => {
                        } : onRightClick}
                        className={"ant-tabs-tab-next-icon wea-cs-item " + (rightStatus ? "wea-cs-disabled" : "")}
                    />
                </div>
                <ul className="wea-customer-content">
                    {
                        list.map(item => {
                            return <CustomerItem type={type} {...item}/>
                        })
                    }
                </ul>
            </div>
        ) : (
            <div className="wea-customer-none">暂无记录</div>
        )
    )
}

Utils.MessageItem = (props) => {
    const {id, name, type, relatedId, tips, url, onIgnoreClick, onAttentionClick} = props,
        loadIcon = (type) => {
            switch (type * 1) {
                case 7:
                    return ['icon-blog-remind', '#f5b400']
                case 8:
                    return ['icon-blog-remind', '#32a8ff']
                case 11:
                    return ['icon-blog-Good', '#f5b400']
                case 12:
                    return ['icon-blog-Comment', '#6a8ab9']
                case 13:
                    return ['icon-blog-at', '#75bf26']
                default:
                    return ['icon-blog-MessageReminder', '#32a8ff']
            }
        },
        iconData = loadIcon(type),
        iconName = iconData[0],
        iconColor = iconData[1],
        onItemEnter = (e) => {
            let dom = e.currentTarget.querySelectorAll('.wea-mic-operation')[0]
            dom.style.display = 'inline-block'
        },
        onItemLeave = (e) => {
            let dom = e.currentTarget.querySelectorAll('.wea-mic-operation')[0]
            dom.style.display = 'none'
        },
        onContentEnter = (e) => {
            let dom = e.currentTarget.children
            Array.prototype.slice.call(dom, 0).forEach(item => {
                item.style.color = '#32a8ff'
            })
        },
        onContentLeave = (e) => {
            let dom = e.currentTarget.children
            Array.prototype.slice.call(dom, 0).forEach(item => {
                item.style.color = '#000'
            })
        }
    return (
        <div className="wea-message-item" onMouseEnter={onItemEnter} onMouseLeave={onItemLeave}>
            <div className="wea-mi-container">
                <span className={`wea-mic-item wea-mic-icon ${iconName}`} style={{color: iconColor}}>&nbsp;</span>
                <div
                    className="wea-mic-item wea-mic-content"
                    onMouseEnter={onContentEnter}
                    onMouseLeave={onContentLeave}
                >
                    {name ? <div className="wea-micc-item wea-micc-name">{name}</div> : ""}
                    <div
                        className="wea-micc-item wea-micc-tip"
                        dangerouslySetInnerHTML={{__html: `<div class="wea-ckeditor-content">${tips}</div>`}}
                    />
                </div>
                <div className="wea-mic-item wea-mic-operation" style={{display: 'none'}}>
                    {type == 1 ?
                        <span
                            className="wea-mico-item"
                            onClick={() => {
                                onAttentionClick(id, relatedId, -1)
                            }}
                        >拒绝</span> : null}
                    {type == 1 ?
                        <span className="wea-mico-item"
                              onClick={() => {
                                  onAttentionClick(id, relatedId, 1)
                              }}
                        >同意</span> : null}
                    <span
                        className="wea-mico-item"
                        onClick={() => {
                            onIgnoreClick(id, type)
                        }}
                    >忽略</span>
                </div>
            </div>
            <hr className="wea-mi-border">&nbsp;</hr>
        </div>
    )
}

export default Utils