import {WeaEchart} from 'ecCom';
class WeaChartComp extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            categories : ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"],
            series : [0.5, 2, 0.43, 3.4, 4, 0],
            option:{
                title: {
                    text: 'ECharts 入门示例'
                },
                tooltip: {},
                legend: {
                    data:['销量']
                },
                xAxis: {
                    data: ["衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"]
                },
                yAxis: {},
                series: [{
                    name: '销量',
                    type: 'bar',
                    data: [5, 20, 36, 10, 10, 20]
                }]
            }
        }
	}
	render() {
	    let {categories, series, option} = this.state;
		return (
			<div style={{height: 800}}>
                <div style={{height: 300}}>
                    <WeaEchart ref="chart" option={option} useDefault={false} />
                </div>
                <div style={{height: 300}}>
                    <WeaEchart ref="chart2" categories={categories} series={series}/>
                </div>
                <button onClick={this.changeState}>调整数据</button>
            </div>
		);
	}
	
	changeState = () => {
		this.setState({
            series : [0.6, 3, 3, 3.4, 2, 1],
        });
	}
}

export default WeaChartComp;