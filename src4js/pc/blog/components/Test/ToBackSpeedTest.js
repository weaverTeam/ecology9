import {Button} from 'antd';
import {WeaTools} from 'ecCom';
import Apis from '../../apis/myBlog';

class ToBackSpeedTest extends React.Component {
	constructor(props) {
		super(props);
        this.state = {
            weaTime : undefined,
            weaLoading : false
        };
	}
	
	render() {
	    let {weaTime, weaLoading} = this.state;
		return (
			<div>
                <div style={{padding: 10}}>
                    <input type="text" ref="api" style={{width: 200}} defaultValue="/api/blog/base/getMyBlogList?endDate=2017-08-03"/>
                </div>
                <div style={{padding: 10}}>
                    <Button onClick={this.testWeaToolSpeed}>WeaTool读取</Button>
                    <span style={{marginLeft : 20}}>消耗时间：{weaLoading ? '读取中' : (weaTime || '未开始')}</span>
                </div>
                
            </div>
		);
	}
	
	testWeaToolSpeed = () => {
	    this.setState({weaLoading: true});
        Apis.getAttentionList().then((result) => {});
        Apis.getBasicInfo().then((result) => {});
        Apis.getDefaultTemplate().then((result) => {});
        Apis.getFanList().then((result) => {});
        Apis.getIndexInfo().then((result) => {});
        Apis.getMessageList().then((result) => {});
	    let start = new Date().getTime(), api = this.refs.api.value;
        WeaTools.callApi(api, "GET", {}).then(
            (result) => {
            	let end = new Date().getTime();
                this.setState({
                    weaLoading: false,
                    weaTime: end - start
                });
            }
        ).catch(e => {
            this.setState({
                weaLoading: false,
                weaTime: '出错了'
            });
        });
    }
}
export default ToBackSpeedTest;