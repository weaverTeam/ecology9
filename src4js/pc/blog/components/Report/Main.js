import React, {Component} from 'react'
import PropTypes from 'react-router/lib/PropTypes'

import {WeaTop,WeaNewScroll} from 'ecCom'

import {Tabs, Tooltip, Modal, Button} from 'antd'
const TabPane = Tabs.TabPane

import '../../css/report/index.less'
import AddReportModal from "./AddReportModal"
import ReportList from "../Component/ReportList/index"
import {REPORT_TYPE} from '../Component/ReportList/Const';

class Report extends Component {
    static contextTypes = {
        router: PropTypes.routerShape
    }

    constructor(props) {
        super(props)
        this.state = {
            topHeight: 0,
            currentType: "blog",
            showModalAddReport: false
        }
    }

    componentDidMount() {
        const {actions} = this.props;
        actions.getBlogReportConfig();
        actions.getReportList();
    }

    render() {
        const {loading, owner, actions, list, table, hasMood} = this.props,
            {topHeight, currentType} = this.state;

        let reportList = list.reportList || [],
            tableInfo = table.tableInfo || {},
            commProps = {
                data: tableInfo,
                style: {margin: "8px"},
                loading: loading,
                type: currentType,
                owner: owner,
                key: owner.key,
                actions: actions,
                editReport: this.editReport
            },
            dropMenuDatas = [
                {
                    key: 'add',
                    content: '添加自定义报表'
                }
            ];
        if (owner.key != REPORT_TYPE.attention && owner.key != REPORT_TYPE.my) {
            dropMenuDatas.push({
                key: 'edit',
                content: '编辑报表'
            });
            dropMenuDatas.push({
                key: 'del',
                content: '删除报表'
            });
        }
    
        return (
            <div className="wea-report">
                <WeaTop title="微博报表" loading={loading} showDropIcon={true} getHeight={this.setTopHeight}
                    icon={<span className='icon-coms-blog'/>}
                    iconBgcolor="#6d3cf7"
                    buttons={[
                        <Button type="primary" onClick={this.editReport.bind(this, {title: '自定义报表'})}>
                            <span className="icon-blog-AddTo">&nbsp;&nbsp;&nbsp;添加自定义报表</span>
                        </Button>
                    ]}
                    dropMenuDatas={dropMenuDatas}
                    onDropMenuClick={this.onDropMenuClick}
                >
                    <WeaNewScroll height={topHeight}>
                        <Tabs activeKey={owner.key} className="wea-tabs" size="small" onChange={this.changeOwner}>
                            {reportList.map(item => {
                                const {key, title} = item;
                                return (
                                    <TabPane key={key} tab={this.getTabPane(item)} />
                                )
                            })}
                        </Tabs>
        
                        {owner.key != REPORT_TYPE.my && hasMood &&
                            <Tabs activeKey={currentType}  className="wea-grey-tabs" type="card" onChange={this.changeType} >
                                <TabPane tab={"微博报表"} key={"blog"} />
                                <TabPane tab={"心情报表"} key={"mood"} />
                            </Tabs>
                        }
                        <ReportList ref="reportList" {...commProps} />
                    </WeaNewScroll>
                </WeaTop>
                <AddReportModal ref="addReportModal" requestData={this.saveReport}/>
            </div>
        )
    }
    
    setTopHeight = (height) => {
        this.setState({topHeight: height})
    }

    changeOwner = (currentOwner) => {
        this.props.actions.chooseOwner(currentOwner);
    }

    deleteReport = (item) => {
        const {actions} = this.props
        Modal.confirm({
            title: '提醒',
            content: `确定删除微博报表"${item.title}"？`,
            onOk: () => actions.deleteReport({id: item.key}),
            onCancel: () => 0,
        })
    }

    saveReport = (params) => {
        const {actions} = this.props;
            
        if (params.id) {
            actions.editCustomizeReport(params, result => {
                if (result.status == "1") {
                    this.refs.addReportModal.visible = false
                }
            });
        } else {
            actions.addReport(
                {tempName: params.name},result => {
                    if (result.status == "1") {
                        this.refs.addReportModal.visible = false
                    }
                }
            )
        }
    }
    
    editReport = (owner) => {
        this.refs.addReportModal.setForm(owner);
        this.refs.addReportModal.visible = true;
    }
    

    changeType = (currentType) => {
        this.setState({currentType: currentType})
    }
    
    getTabPane = (item) => {
        let {key, title} = item;
        let closable = !(key == REPORT_TYPE.my || key == REPORT_TYPE.attention);
        return (
            <div>
                <span className="lh30">{title}</span>
                {closable && (
                    <span
                        className="icon-blog-Absenteeism"
                        style={{marginLeft: "8px"}}
                        onClick={() => this.deleteReport(item)}
                    />
                )}
            </div>
        );
    }
    
    onDropMenuClick = (key) => {
        if (key === 'add') {
            this.editReport({title: '自定义报表'});
        } else if (key === 'edit') {
            this.editReport(this.props.owner);
        } else {
            this.deleteReport(this.props.owner);
        }
    }
}

export default Report