import React, {Component} from 'react'

import {Modal, Button,message} from 'antd'
import {WeaDialog} from 'ecCom';
import AddReportForm from "./AddReportForm"

class AddReportModal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            visible: false,
            name: '自定义报表',
            id: ''
        }
    }

    set visible(value) {
        this.setState({visible: value})
    }
    

    render() {
        let {id,name,visible} = this.state
 
        return (
            <WeaDialog
                style={{height: 80}}
                visible={visible}
                title={id ? `编辑${name}`: '添加自定义报表'}
                icon="icon-coms-blog"
                iconBgcolor="#6d3cf7"
                buttons={[
                    <Button type="primary" onClick={this.onSave}>保存</Button>,
                    <Button onClick={() => this.setVisible(false)}>取消</Button>
                ]}
                onCancel={() => this.setVisible(false)}
            >
                <div style={{paddingTop: 20}}>
                    <AddReportForm
                        ref={
                            form => {
                                this.form = form
                            }
                        }
                        data={{name: name, id: id}}
                    />
                </div>
            </WeaDialog>
        )
    }

    setVisible = (visible) => {
        this.setState({visible: visible})
    }

    onSave = (e) => {
        e.preventDefault()
        this.form.validateFieldsAndScroll((errors, values) => {
            if (!!errors) {
                message.error('填写有误！')
                return;
            }
            this.setState(values);
            this.props.requestData(values)
        })
    }
    
    setForm = (owner) => {
        this.setState({
            name: owner.title,
            id: owner.key
        });
    }
}

export default AddReportModal