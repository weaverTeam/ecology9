import React, {Component} from 'react'

import {Input, Button, Form} from 'antd'
const FormItem = Form.Item;

import Utils from "../Component/wea-utils"
import {WeaInput} from 'ecCom';
const {MapPropsToFields} = Utils

class AddReportForm extends Component {
    constructor(props) {
        super(props)
    }

    get form(){
        return this.props.form
    }

    render() {
        const {getFieldProps} = this.props.form,
            formItemLayout = {
                labelCol: { span: 6, offset: 2 },
                wrapperCol: { span: 14 },
            },
            nameProps = getFieldProps('name', {
                rules: [
                    {required: true, message: '请填写名称'},
                ],
            }),
            idProps = getFieldProps('id');
        return (
            <Form>
                <FormItem
                    label="报表名称"
                    {...formItemLayout}
                >
                    <WeaInput style={{width:"100%"}} placeholder="请输入报表名称" {...nameProps}/>
                    <Input style={{display:"none"}} {...idProps}/>
                </FormItem>
            </Form>
        )
    }
}

AddReportForm = Form.create({
    mapPropsToFields: props => MapPropsToFields(props.data)
})(AddReportForm)

export default AddReportForm