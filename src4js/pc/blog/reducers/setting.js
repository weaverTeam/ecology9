import * as types from '../constants/ActionTypes'

let initialState = {
	title: "微博设置",
	loading: false,
	spinning:false,
	tabkey: 0,
	tabDatas:[
				{title:'基本设置',key:"0"},
           		{title:'分享设置',key:"1"},
           		{title:'模板设置',key:"2"},
           	],
    checked:'',
    showSearchAd:false,
    sharinginfos:'',//上级和指定共享人
    shareSessionkey:'',
    moudleSessionkey:'',
    info:'',
    shareinfo:'',//分享设置--根据id获取共享信息
    orderFields:'',
    moudledefaultid:'',
    showMoudleModal:false,
    moudleinfo:'',//模版设置--根据blog_template主键id，获得模版对象信息
    preview:false,
    previewinfo:'',
    moudleShareSessionkey:'',
    ckvalue:''//富文本内容
};

export default function setting(state = initialState, action) {
	switch(action.type) {
		case types.SETTING_LOADING:
			return {...state, loading: action.loading};
		case types.SETTING_SPINNING:
			return {...state, spinning: action.spinning};
		case types.SETTING_SET_TABKEY:
			return {...state, tabkey: action.data};
		case types.SETTING_CHANGE_CHECKED:
			return {...state, checked: action.data};
		case types.SETTING_GET_SETTING:
			return {...state, checked: action.data};
		case types.SETTING_SHOW_SEARCHAD:
			return {...state, showSearchAd: action.data};
		case types.SETTING_GET_FIXED_SHARINGINFOS:
			return {...state, sharinginfos: action.data};
		case types.SETTING_SHARE_SAVE_SESSIONKEY:
			return {...state, shareSessionkey: action.data};
		case types.SETTING_GET_USER_CONDITION:
			return {...state, info: action.data};
		case types.SETTING_GET_USER_SHAREBYID:
			return {...state, shareinfo: action.data};
		case types.SETTING_SAVE_ORDER_FIELDS:
			return {...state, orderFields: action.data};
		case types.SETTING_MOUDLE_SAVE_SESSIONKEY:
			return {...state, moudleSessionkey: action.data};
		case types.SETTING_SAVE_USER_BLOG_MOUDLE_DEFAULTID:
			return {...state, moudledefaultid: action.data};
		case types.SETTING_SET_MOUDLE_MODAL:
			return {...state, showMoudleModal: action.data};
		case types.SETTING_GET_BLOG_TEMPLETEINFO_BYID:
			return {...state, moudleinfo: action.data};
		case types.SETTING_SAVE_CKVALUE:
			return {...state, ckvalue: action.data};
		case types.SETTING_SET_PREVIEW:
			return {...state, previewinfo: action.data};
		case types.SETTING_MOUDLE_SHARE_SESSIONKEY:
			return {...state, moudleShareSessionkey: action.data}
		case types.SETTING_SET_PREVIEWSHOW:
			return {...state, preview:action.data}
			
		default:
			return state
	}
}