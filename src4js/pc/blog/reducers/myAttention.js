import * as types from '../constants/ActionTypes';
import * as attCon from '../constants/AttentionConstant';
import objectAssign from 'object-assign'

let initialState = {
	title: "我的关注",
	loading: false,
    countLoaded: false,//右侧各日期的关注人员情况是否加载完毕
    myAttLoaded: false,//左侧我的关注列表是否加载完毕
    myAttLasted: false,//左侧我的关注列表是否已到最末页
	myAttentions: [],// 我的关注列表
	rightType: attCon.RIGHT_TYPE_WEIBO,//右侧显示的类型，包括微博、用户。默认是微博
	myCountList: [],//按时间统计的微博数列表
    searchWeiboList: {},//搜索后的微博列表
	userId: '',//他的微博对应的人员id
    userInfo: {}//当前登录人员
};

export default function myAttention(state = initialState, action) {
	switch(action.type) {
		case types.MYATTENTION_LOADING:
			return {...state, loading: action.loading};
		case types.MYATTENTION_LIST:
		    let serverTotal = action.data.total || 0;
            let webTotal = (action.params.currentPage || 1) * (action.params.pageSize || 0);
            let myAttLasted = serverTotal <= webTotal;
            let attentionList = [].concat(state.myAttentions).concat(action.data.attentionList);
			return {...state, myAttentions: attentionList, myAttLoaded: true, myAttLasted: myAttLasted};
        case types.MYATTENTION_LIST_LOADING:
            return {...state, myAttLoaded: false};
		case types.MYATTENTION_COUNTLIST:
			return {...state, myCountList: action.data.countInfos, weiboListArray: new Array(action.data.countInfos.length), countLoaded: true};
		case types.MYATTENTIONWORK_BY_DATE:
			if (!state.myCountList || !state.myCountList[action.index])
				return state;
			let cdata = {...state.myCountList[action.index], data:action.data, loaded: true};
			let newstate = {...state};
			newstate.myCountList = objectAssign([], state.myCountList);
			newstate.myCountList[action.index] = cdata;
			return newstate;
		case types.MYATTENTION_CHOOSEUSER: {
		    let attlist = state.myAttentions.map(att=>{
		        if (att.userId == action.userId)
		            return {...att, isnew:'0'};
                return att;
            });
            
			return {...state, myAttentions:attlist, userId: action.userId, rightType: attCon.RIGHT_TYPE_USER};
        }
        case types.CURRENT_USER:
            return {...state, userInfo:action.data.currentUser};
        case types.MYATTENTION_SEARCHWEIBO:
            let data = action.data;
            data.__hasData = data.discussList ? !!data.discussList.length : null
            if (data.currentPage == 1) {
                data.discussList = data.discussList
            } else {
                data.discussList = state.searchWeiboList.discussList.concat(data.discussList || [])
            }
            return {...state, searchWeiboList: data, countLoaded: true}
        case types.MYATTENTION_COUNTLIST_LOADING:
            return {...state, countLoaded: false};
        case types.MYATTENTION_UPDATE_USER_ATTENTIONSTATUS:
            let {userId,attentionStatus} = action.attnInfo;
            let attlist = state.myAttentions.map(att=>{
                if (att.userId == userId)
                    return {...att, attentionStatus: attentionStatus};
                return att;
            });
            return {...state, myAttentions:attlist};
        case types.MYATTENTION_RESETRIGHT:
            return {...state, rightType: attCon.RIGHT_TYPE_WEIBO, userId: ''}
        case types.MYATTENTION_CLEARLEFT:
            return {...state, myAttentions:[]};
        case types.MYATTENTION_CLEARRIGHTLIST:
            return {...state, searchWeiboList:{}, countLoaded: false};
		default:
			return state
	}
}