import * as types from '../constants/ActionTypes'
import {REPORT_TYPE} from '../components/Component/ReportList/Const';
import {message} from 'antd'
import isEqual from 'lodash/isEqual';

let initialState = {
    loading: false,
    userInfo: {},

    list: {},
    table: {},
    owner: {
        key: REPORT_TYPE.my,
        title: "我的报表"
    },//当前所查看报表
    hasMood: false,//是否拥有心情模块
    currentParams: {},//当前搜索条件
};

export default function report(state = initialState, action) {
    let {type, data, others,parent, currentParams} = action, result = null, table = null;
    switch (type) {
        case types.REPORT_LOADING:
            return {...state, loading: action.loading}
        case types.REPORT_LOADING_LIST:
            return {...state, loading: action.loading, currentParams: action.currentParams};
        case types.USER_INFO:
            return {...state, userInfo: data}
        case types.REPORT_LIST:
            return {...state, list: data}
        case types.REPORT_ADD:
            message.success("操作成功！")
            result = {...state.list}
            result.reportList.push(data.newReport || {})
            return {...state, list: result}
        case types.REPORT_EDIT:
            if (!data.newReport) return state;
            message.success("操作成功！")
            result = {...state.list};
            result.reportList = result.reportList.map((r) => {
                if (r.key == data.newReport.key)
                    return data.newReport;
                else
                    return r;
            });
            return {...state, list: result, owner: data.newReport}
        case types.REPORT_DELETE:
            message.success("操作成功！")
            result = {reportList: []}
            state.list.reportList.forEach(
                item => {
                    if (item.key != others.id) {
                        result.reportList.push(item)
                    }
                }
            )
            return {...state, list: result}
        case types.REPORT_TABLE:
            if (!isEqual(state.currentParams, currentParams))
                return state;
            return {...state, table: data,  loading: false}
        case types.REPORT_CHOOSE_OWNER:
            let reportList = state.list.reportList || [];
            let ownerKey = action.owner;
            let owners = reportList.filter((report) => report.key == ownerKey);
            return {...state, owner: owners.length > 0 ? owners[0] : {}};
        case types.BLOG_REPORT_CONFIG:
            return {...state, hasMood: !!action.data.isMood};
        case types.BLOG_REPORT_SUBLIST:
            table = {...state.table};
            table.tableInfo = changeTableRow(state, (row) => {
                if (parent.conditionId == row[0].conditionId) {
                    let newRow = [...row];
                    newRow[0].subList = data;
                    newRow[0].open = true;
                    newRow[0].loading = false;
                    return newRow;
                }
                return row;
            })
            return {...state, table: table};
        case types.BLOG_REPORT_SUBLIST_LOADING:
            table = {...state.table};
            table.tableInfo = changeTableRow(state, (row) => {
                if (parent.conditionId == row[0].conditionId) {
                    let newRow = [...row];
                    newRow[0].loading = true;
                    return newRow;
                }
                return row;
            })
            return {...state, table: table};
        case types.BLOG_REPORT_TOGGLE_CONDITION_EXPAND:
            let {conditionId} = action;
            table = {...state.table};
            table.tableInfo = changeTableRow(state, (row) => {
                if (conditionId == row[0].conditionId) {
                    let newRow = [...row];
                    newRow[0].open = !newRow[0].open;
                    return newRow;
                }
                return row;
            })
            return {...state, table: table};
        default:
            return state
    }
}

const changeTableRow = (state, func) => {
    let tableInfo = {...state.table.tableInfo} || {}, tableBody = tableInfo.tableBody || [];
    tableBody = tableBody.map(func);
    tableInfo.tableBody = tableBody;
    return tableInfo;
};