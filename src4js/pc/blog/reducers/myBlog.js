import * as types from '../constants/ActionTypes'
import isEqual from 'lodash/isEqual';
import { message } from 'antd';

import {enableBatching} from 'redux-batched-actions';

let initialState = {
    title: '我的微博',
    loading: false,
    keyWords: '',

    userInfo: {},
    weiboGroup: {},
    weiboCKEditorConfig: {},
    currentParams: {},//当前搜索条件,用于判断返回的数据是否为搜索数据

    indexInfo: {},
    basicInfo: {},
    receiverList: {},
    visitorList: {},

    weiboList: {},
    fanList: {},
    attentionList: {},

    messageList: {},

    note: {},

    defaultTemplate: {},
}

let replaceWeiboList = (others, _old, _new, state, data) => {
    const weiboList = {...state.weiboList}
    if (weiboList.discussList && weiboList.discussList.length > 0) {
        weiboList.discussList.forEach(
            (item, key) => {
                if(others.discussId){
                    if(item.id == others.discussId){
                        if (_old) weiboList.discussList[key][_old] = data[_new]
                        else weiboList.discussList[key] = data[_new]
                    }
                }else{
                    if(others.userId){
                        if ((item.workdate == others.workdate) && (item.userid == others.userId)) {
                            if (_old) weiboList.discussList[key][_old] = data[_new]
                            else weiboList.discussList[key] = data[_new]
                        }
                    }else{
                        if (item.workdate == others.workdate) {
                            if (_old) weiboList.discussList[key][_old] = data[_new]
                            else weiboList.discussList[key] = data[_new]
                        }
                    }

                }
                //anruo return
                return
            }
        )
    } else {
        weiboList.discussList.push(data[_new])
    }
    return {...state, weiboList: weiboList}
}

function myBlog(state = initialState, action) {
    const {type, data, others, currentParams} = action
    if (data && (data.status == 1)) {
        switch (type) {
        	case types.MYBLOG_SET_KEYWORDS:
                return {...state, keyWords: data.value}
            case types.MYBLOG_LOADING:
                return {...state, loading: action.loading, currentParams: action.currentParams}
            case types.USER_INFO:
                return {...state, userInfo: data}
            case types.WEIBO_CKEDITOR_CONFIG:
                return {...state, weiboCKEditorConfig: data}
            case types.MYBLOG_DEFAULT_TEMPLATE:
                return {...state, defaultTemplate: data}
            case types.MYBLOG_INDEX_INFO:
                return {...state, indexInfo: data}
            case types.MYBLOG_BASIC_INFO:
                return {...state, basicInfo: data}
            case types.MYBLOG_RECEIVER_LIST:
                return {...state, receiverList: data}
            case types.MYBLOG_VISITOR_LIST:
                return {...state, visitorList: data}
            case types.MYBLOG_SEARCH_WEIBO_LIST:
                if (!isEqual(state.currentParams, currentParams))
                    return state;
                data.__hasData = data.discussList ? !!data.discussList.length : false
                if (data.currentPage == 1) {
                    data.discussList = data.discussList
                } else {
                    data.discussList = state.weiboList.discussList.concat(data.discussList || [])
                }
                return {...state, weiboList: data, loading: false}
            case types.MYBLOG_SEARCH_WEIBO_MAIN_PAGE_LIST:
                if (!isEqual(state.currentParams, currentParams))
                    return state;
                data.__hasData = data.discussList ? !!data.discussList.length : false
                if (data.currentPage == 1) {
                    data.discussList = data.discussList
                } else {
                    data.discussList = state.weiboList.discussList.concat(data.discussList || [])
                }
                return {...state, weiboList: data, loading: false}
            case types.MYBLOG_WEIBO_LIST:
                if (!isEqual(state.currentParams, currentParams))
                    return state;
                data.__hasData = data.total && (data.currentPage * data.pageSize <= data.total)
                if (others.type == 12 || others.type == 21) {
                    data.__hasData = data.discussList ? !!data.discussList.length : false
                }
                if (!others.status) {
                    data.discussList = data.currentPage == 1 ? data.discussList : state.weiboList.discussList.concat(data.discussList || [])
                }
                return {...state, weiboList: data,loading : false, keyWords: ""}
            case types.MYBLOG_SYSTEM_WORK_LOG:
                return replaceWeiboList(others, "sysWorkLog", "sysWorkLog", state, data)
            case types.MYBLOG_EDIT_WEIBO:
                message.success("操作成功！")
                return replaceWeiboList(others, "", "blogDiscussVo", state, data)
            case types.MYBLOG_EDIT_COMMENT:
                message.success("操作成功！")
                return replaceWeiboList(others, "blogReplyList", "blogReplyList", state, data)
            case types.MYBLOG_DELETE_COMMENT:
                message.success("操作成功！")
                return replaceWeiboList(others, "blogReplyList", "replyList", state, data)
            case types.MYBLOG_EDIT_AGREE:
                message.success("操作成功！")
                return replaceWeiboList(others, "blogZanBean", "blogZanBean", state, data)
            case types.MYBLOG_SET_SCORE:
                message.success("操作成功！")
                return replaceWeiboList(others, "", "blogDiscussVo", state, data)
            case types.MYBLOG_SET_NOTICE:
                message.success("操作成功！")
                return state
            case types.MYBLOG_SET_READED:
                return replaceWeiboList(others, "", "blogDiscussVo", state, data)

            case types.MYBLOG_FAN_LIST:
                return {...state, fanList: data}
            case types.MYBLOG_EDIT_FAN:
                message.success("操作成功！")
                let fanList = {...state.fanList}
                fanList.attnList.forEach(
                    (item, key) => {
                        if (item.userId == others.userId) {
                            fanList.attnList[key] = data.attnInfo
                            return
                        }
                    }
                )
                return {...state, fanList: fanList}
            case types.MYBLOG_ATTENTION_LIST:
                return {...state, attentionList: data}
            case types.MYBLOG_EDIT_ATTENTION:
                message.success("操作成功！")
                let attentionList = {...state.attentionList}
                attentionList.attentionList.forEach(
                    (item, key) => {
                        if (item.userId == others.userId) {
                            attentionList.attentionList[key] = data.attnInfo
                            return
                        }
                    }
                )
                return {...state, attentionList: attentionList}
            case types.MYBLOG_EDIT_ATTENTIONS:
                message.success("操作成功！")
                return state
            case types.WEIBO_GROUP:
                return {...state, weiboGroup: data}
            case types.MYBLOG_CREATE_GROUP:
                message.success("操作成功！")
                if (data.groupInfo.groupId) {
                    let weiboGroup = {...state.weiboGroup}
                    weiboGroup.groupList.push(data.groupInfo)
                    return {...state, weiboGroup: weiboGroup}
                }
            case types.MYBLOG_EDIT_GROUP:
                message.success("操作成功！")
                if (data.groupInfo.groupId) {
                    let weiboGroup = {...state.weiboGroup}
                    weiboGroup.groupList.forEach(
                        (item, key) => {
                            if (item.groupId == others.groupId) {
                                weiboGroup.groupList[key] = data.groupInfo
                                return
                            }
                        }
                    )
                    return {...state, weiboGroup: weiboGroup}
                }
            case types.MYBLOG_DELETE_GROUP:
                message.success("操作成功！")
                return {...state, weiboGroup: data}
            case types.MYBLOG_COPY_GROUP:
                message.success("操作成功！")
                return state
            case types.MYBLOG_MOVE_GROUP:
                message.success("操作成功！")
                return state
            case types.MYBLOG_RESET_GROUP:
                message.success("操作成功！")
                return state
            case types.MYBLOG_REMOVE_GROUP:
                message.success("操作成功！")
                return state
            case types.MYBLOG_CREATE_AND_MOVE_GROUP:
                message.success("操作成功！")
                return state
            case types.MYBLOG_CREATE_AND_COPY_GROUP:
                message.success("操作成功！")
                return state

            case types.MYBLOG_MESSAGE_LIST:
                return {...state, messageList: data}
            case types.MYBLOG_IGNORE_MESSAGE:
                message.success("操作成功！")
                data.remindList = []
                state.messageList.remindList.forEach(
                    item => {
                        if (item.id != others.id) {
                            data.remindList.push(item)
                        }
                    }
                )
                return {...state, messageList: data}
            case types.MYBLOG_IGNORE_MESSAGES:
                message.success("操作成功！")
                data.remindList = []
                return {...state, messageList: data}
            case types.MYBLOG_APPROVE_MESSAGE:
                message.success("操作成功！")
                data.remindList = []
                state.messageList.remindList.forEach(
                    item => {
                        if (item.id != others.id) {
                            data.remindList.push(item)
                        }
                    }
                )
                return {...state, messageList: data}
            case types.MYBLOG_NOTE:
                return {...state, note: data}
            case types.MYBLOG_SAVE_NOTE:
                message.success("操作成功！")
                return state
            case types.MYBLOG_CLEAR_COMMENT_COUNT:
                if (!state.basicInfo || !state.basicInfo.countData)
                    return state;
                let basicInfo = {...state.basicInfo};
                basicInfo.countData = {...basicInfo.countData, commentCount:0};
                return {...state, basicInfo: basicInfo};
            default:
                return state
        }
    } else {
        return state
    }
}
export default enableBatching(myBlog);