import { WeaTools } from 'ecCom'

export const getDatas = params => {
	return WeaTools.callApi('/', 'GET', params);
}


export const getUserBlogBaseSettingP = params => {
	return WeaTools.callApi('/api/blog/setting/getUserBlogBaseSetting', 'GET', params);
}


export const saveBlogUserSettingP = params => {
	return WeaTools.callApi('/api/blog/setting/saveBlogUserSetting', 'POST', params);
}

export const getFixedSharingInfosP = params => {
	return WeaTools.callApi('/api/blog/setting/getFixedSharingInfos', 'GET', params);
}


export const getBlogShareTableP = params => {
	return WeaTools.callApi('/api/blog/setting/getBlogShareTable', 'GET', params);
}

export const saveBlogShareListP = params => {
	return WeaTools.callApi('/api/blog/setting/saveBlogShareList', 'POST', params);
}

export const getUserBlogTemplateShareConditionP = params => {
	return WeaTools.callApi('/api/blog/setting/getUserBlogTemplateShareCondition', 'GET', params);
}


export const deleteBlogUserShareP = params => {
	return WeaTools.callApi('/api/blog/setting/deleteBlogUserShare', 'GET', params);
}

export const getBlogUserShareByIdP = params => {
	return WeaTools.callApi('/api/blog/setting/getBlogUserShareById', 'GET', params);
}

export const getUserBlogTemplateTableP = params => {
	return WeaTools.callApi('/api/blog/setting/getUserBlogTemplateTable', 'GET', params);
}

export const deleteBlogTemplateByIdsP = params => {
	return WeaTools.callApi('/api/blog/setting/deleteBlogTemplateByIds', 'GET', params);
}


export const setUserBlogTemplateToDefaultP = params => {
	return WeaTools.callApi('/api/blog/setting/setUserBlogTemplateToDefault', 'GET', params);
}

export const saveOrUpdateBlogTemplateP = params => {
	return WeaTools.callApi('/api/blog/setting/saveOrUpdateBlogTemplate', 'POST', params);
}

export const getBlogTempleteInfoByIdP = params => {
	return WeaTools.callApi('/api/blog/setting/getBlogTempleteInfoById', 'GET', params);
}

export const setPreviewP = params => {
	return WeaTools.callApi('/api/blog/setting/getBlogTempleteInfoById', 'GET', params);
}

export const getUserBlogTemplateShareTableP = params => {
	return WeaTools.callApi('/api/blog/setting/getUserBlogTemplateShareTable', 'GET', params);
}

export const addUserTempShareP = params => {
	return WeaTools.callApi('/api/blog/setting/addUserTempShare', 'POST', params);
}

export const deleteUserTempShareP = params => {
	return WeaTools.callApi('/api/blog/setting/deleteUserTempShare', 'GET', params);
}
