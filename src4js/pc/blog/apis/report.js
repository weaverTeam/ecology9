import {WeaTools} from 'ecCom'

export const getUserInfo = params => WeaTools.callApi('/api/blog/base/getCurrentUserInfo', 'GET', null);
export const getReportList = params => WeaTools.callApi('/api/blog/report/getMyBlogReportList', 'GET', null);
export const addReport = params => WeaTools.callApi('/api/blog/report/addCustomizeReport', 'GET', params);
export const deleteReport = params => WeaTools.callApi('/api/blog/report/deleteCustomizeReport', 'GET', params);
export const getReportTable = params => WeaTools.callApi('/api/blog/report/getMyBlogReport', 'GET', params);
export const getAttentionReport = (params) => WeaTools.callApi('/api/blog/report/getAttentionReport', 'GET', params);
export const getTempReportTableInfo = (params) => WeaTools.callApi('/api/blog/report/getTempReportTableInfo', 'GET', params);
export const addTempCondition = (params) => WeaTools.callApi('/api/blog/report/addTempCondition', 'POST', params);
export const deleteTempCondition = (params) => WeaTools.callApi('/api/blog/report/deleteTempCondition', 'GET', params);
export const getBlogReportConfig = (params) => WeaTools.callApi('/api/blog/report/getBlogReportConfig', 'GET', params);
export const getTempReportExpandRows =  (params) => WeaTools.callApi('/api/blog/report/getTempReportExpandRows', 'GET', params);
export const getBlogChartInfos = (params) => WeaTools.callApi('/api/blog/report/getBlogChartInfos', 'GET', params);
export const getBlogComparedChartInfos = (params) => WeaTools.callApi('/api/blog/report/getBlogComparedChartInfos','GET', params);
export const editCustomizeReport = (params) => WeaTools.callApi('/api/blog/report/editCustomizeReport','POST', params);