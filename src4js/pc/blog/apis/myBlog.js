import {WeaTools} from 'ecCom'

const Apis = {}

Apis.getNull = () => {
    return Promise.resolve()
}

Apis.getWeiboCKEditorConfig = params => {
    return WeaTools.callApi('/api/blog/base/getBlogAppList', 'GET', null)
}

Apis.getWeiboGroup = params => {
    return WeaTools.callApi('/api/blog/setting/getMyAttentionGroup', 'GET', null)
}

Apis.getUserInfo = params => {
    return WeaTools.callApi('/api/blog/base/getCurrentUserInfo', 'GET', null)
}

Apis.getDefaultTemplate = params => {
    return WeaTools.callApi('/api/blog/base/getDefaultTemplate', 'GET', null)
}

Apis.getIndexInfo = params => {
    return WeaTools.callApi('/api/blog/base/getBlogWorkIndex', 'GET', null)
}

Apis.getBasicInfo = params => {
    return WeaTools.callApi('/api/blog/base/getBlogMenuCountdata', 'GET', null)
}

Apis.getReceiverList = params => {

    return WeaTools.callApi('/api/blog/base/getVisitorList', 'GET', params)
}

Apis.getVisitorList = params => {

    return WeaTools.callApi('/api/blog/base/getVisitorList', 'GET', params)
}

Apis.getSystemWorkLog = params => {

    return WeaTools.callApi('/api/blog/base/getSysWorkLog', 'GET', params)
}

Apis.searchWeiboList = params => {

    return WeaTools.callApi('/api/blog/base/getMyBlogListBySearch', 'GET', params)
}

Apis.searchWeiboMainPageList = params => {

    return WeaTools.callApi('/api/blog/base/getBlogHomepageListBySearch', 'GET', params)
}

Apis.getWeiboList = params => {

    return WeaTools.callApi('/api/blog/base/getMyBlogList', 'GET', params)
}

Apis.getWeiboEveryTypeList = params => {

    return WeaTools.callApi('/api/blog/base/getMyblogListByType', 'GET', params)
}

Apis.getWeiboMainPageList = params => {

    return WeaTools.callApi('/api/blog/base/getBlogHomepageList', 'GET', params)
}

Apis.getWeiboMainPageNewList = params => {

    return WeaTools.callApi('/api/blog/base/getMyBlogHomepageNew', 'GET', params)
}

Apis.editWeibo = params => {

    return WeaTools.callApi('/api/blog/base/saveOrUpdateBlog', 'POST', params)
}

Apis.setWeiboReaded = params => {

    return WeaTools.callApi('/api/blog/base/setBlogDiscussReaded', 'POST', params)
}

Apis.editComment = params =>{

    return WeaTools.callApi('/api/blog/base/addBlogReply', 'POST', params)
}

Apis.deleteComment = params =>{

    return WeaTools.callApi('/api/blog/base/deleteBlogReplay', 'POST', params)
}

Apis.editAgree = params =>{

    return WeaTools.callApi('/api/blog/base/addOrDeleteZan', 'POST', params)
}

Apis.getFanList = params =>{

    return WeaTools.callApi('/api/blog/base/getAttentionMesUsers', 'GET', params)
}

Apis.getAttentionList = params =>{

    return WeaTools.callApi('/api/blog/base/getMyBlogAttention', 'GET', params)
}


Apis.createGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/createBlogGroup', 'POST', params)
}

Apis.editGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/editBlogGroup', 'POST', params)
}

Apis.deleteGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/deleteBlogGroup', 'POST', params)
}

Apis.copyGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/copyContactToBlogGroup', 'POST', params)
}

Apis.moveGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/moveContactToBlogGroup', 'POST', params)
}

Apis.resetGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/resetContactToBlogGroup', 'POST', params)
}

Apis.removeGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/removeFromBlogGroup', 'POST', params)
}

Apis.createAndMoveGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/createAndMoveContactToBlogGroup', 'POST', params)
}

Apis.createAndCopyGroup = params =>{

    return WeaTools.callApi('/api/blog/setting/createAndCopyContactToBlogGroup', 'POST', params)
}

Apis.editAttition = params =>{

    return WeaTools.callApi('/api/blog/base/attentionOpt', 'POST', params)
}

Apis.editAttitions = params =>{

    return WeaTools.callApi('/api/blog/base/batchCancelAttn', 'POST', params)
}

Apis.editFan = params =>{

    return WeaTools.callApi('/api/blog/base/attentionMeOpt', 'POST', params)
}


Apis.getMessageList = params => {

    return WeaTools.callApi('/api/blog/base/getMsgRemindList', 'GET', params)
}

Apis.ignoreMessage = params =>{

    return WeaTools.callApi('/api/blog/base/neglectMsgRemind', 'POST', params)
}

Apis.ignoreMessages = params =>{

    return WeaTools.callApi('/api/blog/base/neglectMsgRemindAll', 'POST', params)
}

Apis.approveMessage = params =>{

    return WeaTools.callApi('/api/blog/base/dealMsgRemind', 'POST', params)
}


Apis.getNote = params => {

    return WeaTools.callApi('/api/blog/base/getBlogNote', 'GET', params)
}

Apis.saveNote = params => {

    return WeaTools.callApi('/api/blog/base/saveBlogNote', 'POST', params)
}


Apis.setScore = params => {
    return WeaTools.callApi('/api/blog/base/managerScore', 'POST', params)
}

Apis.setNotice = params => {
    return WeaTools.callApi('/api/blog/base/sendRemindUnsubmit', 'GET', params)
}

Apis.setReaded = params => {
    return WeaTools.callApi('/api/blog/base/setBlogDiscussReaded', 'POST', params)
}

Apis.getBlogLocations = (params) => {
	return WeaTools.callApi('/api/blog/base/getBlogLocations', 'GET', params)
}

export default Apis

