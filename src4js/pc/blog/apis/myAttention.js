import { WeaTools } from 'ecCom'

const myAttentions = {
	getMyBlogAttentions: (params) => WeaTools.callApi('/api/blog/base/getMyAttentionUsers', 'GET', params),
    getMyAttentionShare: (params) => WeaTools.callApi('/api/blog/base/getMyAttentionShare', 'GET', params),
	editAttention: params => WeaTools.callApi('/api/blog/base/attentionOpt', 'POST', params),
	getAttentionCountInfos: (params) => WeaTools.callApi('/api/blog/base/getAttentionCountInfos', 'GET', params),
	getAttentionWorkDateList: (params) => WeaTools.callApi('/api/blog/base/getAttentionWorkDateList', 'GET', params),
	getBlogPersonalInfo: (params) => WeaTools.callApi('/api/blog/base/getBlogPersonalInfo', 'GET', params),
	getBlogWorkIndex: (params) => WeaTools.callApi('/api/blog/base/getBlogWorkIndex', 'GET', params),
	// 查询他的关注
	getMyBlogAttention: (params) => WeaTools.callApi('/api/blog/base/getMyBlogAttention', 'GET', params),
	//查询他的粉丝，参数userId
	getAttentionMesUsers: (params) => WeaTools.callApi('/api/blog/base/getAttentionMesUsers', 'GET', params),
	// 他的微博搜索。参数包括：blogId 他的用户id;startDate 开始日期;endDate 结束日期，content 搜索内容;pageSize 每页数量，currentPage 当前页
	getBlogViewBySearch: (params) => WeaTools.callApi('/api/blog/base/getBlogViewBySearch', 'GET', params),
	// 查询他的微博
	getViewBlogList: (params) => WeaTools.callApi('/api/blog/base/getViewBlogList', 'GET', params),
    // 全部提醒
    sendRemindUnsubmitAll: (params) => WeaTools.callApi('/api/blog/base/sendRemindUnsubmitAll', 'GET', params)
};
export default myAttentions;