import { Modal } from 'antd';

export const transfStr = (name = '', ids = '', list = [], type = '') => {
	let str = '';
	const mirror = {
		37: "doc",
		task: "task",
		18: "crm",
		152: "workflow",
		135: "project",
		workplan: "workplan",
		blogTemplate: "blogTemplate"
	}
	list.map(item => {
		if(name === 'Upload' && type === 'image'){
			str += '<img class="formImgPlay" src="' + item.filelink + '" data-imgsrc="' + (item.loadlink || item.filelink) + '" />'
		}
		if(name === 'Upload' && type === 'file'){
			str += (item.filelink + '<br>')
		}
		if(name === 'Browser'){
			str += (type === 'blogTemplate' ? item.name : `<a href='javascript:void(0)'  linkid='${item.id}' linkType='${mirror[type]}' onclick='try{return openAppLink(this,${item.id});}catch(e){}' ondblclick='return false;'  unselectable='off' contenteditable='false' style='cursor:pointer;text-decoration:underline !important;margin-right:8px'>${item.name || item.showname}</a>`)
		}
	})
	return str
}

export const templetChange = (valueNow = '' ,callback,  name = '', ids = '', list = [], type = '') => {
	if(valueNow){
		Modal.confirm({
			title: '替换当前内容？',
			onOk: () => {
				callback(transfStr(name, ids, list, type))
			},
			onCancel: () => {
				
			}
		});
	}else{
		callback(transfStr(name, ids, list, type))
	}
}
