class App21 extends React.Component { //App2是一个可复用组件
    constructor(props) {
        super(props);
        //可复用组件有自己状态
        this.state = {
            num:0
        }
    }
    render() {
        const {fatherNum} = this.props;
        const {num} = this.state;
        return (
            <div>
                <button onClick={()=>{
                    this.setState({
                        num:num+1
                    })
                }}>测试</button>
                <button onClick={()=>{
                    if(typeof this.props.onChange==="function") {
                        this.props.onChange(num);
                    }
                }}>回调修改父级状态</button>
                <span>{num}</span>
                <span>{fatherNum}</span>
            </div>
        )
    }
}

class App21Father extends React.Component { //App2的父级组件
    constructor(props) {
        super(props);
        //父级组件的状态
        this.state = {
            fatherNum:0
        }
    }
    render() {
        const {fatherNum} = this.state;
        return (
            <div>
                <button onClick={()=>{
                    this.setState({
                        fatherNum:fatherNum+1
                    })
                }}>测试</button>
                <App21 fatherNum={fatherNum}
                      onChange={(v)=>{
                           this.setState({
                               fatherNum:v
                           })
                      }} />
            </div>
        )
    }
}

class App22 extends React.Component { //App2是一个可复用组件
    //Mounting
    constructor(props) {
        super(props);
    }
    componentWillMount() {

    }
    render() {
        return (
            <div></div>
        )
    }
    componentDidMount() {
        //1、用来发起请求
        //2、这时候真实dom已有，这里用来集成传统组件或者做dom操作

    }

    //Updating
    componentWillReceiveProps() { //当父组件变化时，子组件需要受控，要在这里加判断然后setState

    }
    shouldComponentUpdate() { //是否允许更新，用来做组件优化
        return true;
    }
    componentWillUpdate() {
        //next will render

    }
    componentDidUpdate() { //每次组件setState都会触发，通常用来动态调整一些样式

    }

    //Unmounting
    componentWillUnmount() {

    }
}
