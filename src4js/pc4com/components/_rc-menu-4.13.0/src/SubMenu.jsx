import SubPopupMenu from './SubPopupMenu';
import React, { PropTypes } from 'react';
import KeyCode from 'rc-util/lib/KeyCode';
import guid from 'rc-util/lib/guid';
import classnames from 'classnames';
import { noop, loopMenuItemRecusively } from './util';

/* eslint react/no-is-mounted:0 */


//获取元素的纵坐标
function getTop(e){
var offset=e.offsetTop;
//alert("getTop: "+e + " offset: " + offset);
//console.log("getTop: ",e,offset);
if(!!e.offsetParent) offset+=getTop(e.offsetParent);
return offset;
}
//获取元素的横坐标
function getLeft(e){
var offset=e.offsetLeft;
if(!!e.offsetParent) offset+=getLeft(e.offsetParent);
return offset;
}

function getScrollTop() {
    var scrollTop = 0;
    if(document.documentElement&&document.documentElement.scrollTop)
        scrollTop=document.documentElement.scrollTop;
    else if(document.body)
        scrollTop=document.body.scrollTop;
    return scrollTop;
}

const SubMenu = React.createClass({
  propTypes: {
    parentMenu: PropTypes.object,
    title: PropTypes.node,
    children: PropTypes.any,
    selectedKeys: PropTypes.array,
    openKeys: PropTypes.array,
    onClick: PropTypes.func,
    onOpenChange: PropTypes.func,
    rootPrefixCls: PropTypes.string,
    eventKey: PropTypes.string,
    multiple: PropTypes.bool,
    active: PropTypes.bool,
    onSelect: PropTypes.func,
    closeSubMenuOnMouseLeave: PropTypes.bool,
    openSubMenuOnMouseEnter: PropTypes.bool,
    onDeselect: PropTypes.func,
    onDestroy: PropTypes.func,
    onItemHover: PropTypes.func,
    onMouseEnter: PropTypes.func,
    onMouseLeave: PropTypes.func,
    onTitleMouseEnter: PropTypes.func,
    onTitleMouseLeave: PropTypes.func,
    onTitleClick: PropTypes.func,
  },

  mixins: [require('./SubMenuStateMixin')],

  getDefaultProps() {
    return {
      onMouseEnter: noop,
      onMouseLeave: noop,
      onTitleMouseEnter: noop,
      onTitleMouseLeave: noop,
      onTitleClick: noop,
      title: '',
    };
  },

  getInitialState() {
    this.isSubMenu = 1;
    return {
      defaultActiveFirst: false,
    };
  },

  componentWillUnmount() {
    const props = this.props;
    if (props.onDestroy) {
      props.onDestroy(props.eventKey);
    }
    if (props.parentMenu.subMenuInstance === this) {
      this.clearSubMenuTimers();
    }
    //this._menuId
  },

  componentDidUpdate(prevProps) {
    if(this.props.ifFixed&&this.props.mode=="vertical") {
        const {level} = this.props;
        const obj = document.getElementById("sub"+this._menuId);
	    const obj1 = document.getElementById(this._menuId);
        if(obj1&&obj) {
        // const top = (!!window.weaScrollTop['wea-scroll']?window.weaScrollTop['wea-scroll']:0)+getScrollTop();
//      const top = getScrollTop();
        const top = jQuery('#menuScrollWrapper').find('.scroller').scrollTop();
        const fatherTop = getTop(obj);
        const fatherLeft = getLeft(obj);
        const fatherWidth = obj.offsetWidth;
        const fatherHeight = obj.offsetHeight;
        const nowLeft = fatherLeft+fatherWidth-4;
        const ownHeight = obj1.offsetHeight;
        const winHeight = document.documentElement.clientHeight;
        let nowTop = level==1?fatherTop-top:fatherTop;

//      alert("fatherTop" + fatherTop + " fatherLeft" + fatherLeft);
//      console.log("fatherTop" + fatherTop + " fatherLeft" + fatherLeft);

//      alert('nowTop:' + nowTop+';  nowTop+ownHeight:' + (nowTop + ownHeight) +'; winHeight:'+winHeight);
//      console.log('nowTop',nowTop,'nowTop + ownHeight',nowTop + ownHeight,'winHeight',winHeight);
        if(nowTop + ownHeight > winHeight) nowTop = winHeight - ownHeight;
        if(nowTop < 0) nowTop = 0;

        //console.log("id:",this._menuId," level:",level," fatherTop:",fatherTop," nowTop:",nowTop);
        //console.log("id:",this._menuId," level:",level," fatherLeft:",fatherLeft," nowLeft:",nowLeft," fatherWidth:",fatherWidth);
        obj1.style.top = nowTop+"px";
        obj1.style.left = nowLeft+"px";
      }
    }
  },

  onDestroy(key) {
    this.props.onDestroy(key);
  },

  onKeyDown(e) {
    const keyCode = e.keyCode;
    const menu = this.menuInstance;
    const isOpen = this.isOpen();

    if (keyCode === KeyCode.ENTER) {
      this.onTitleClick(e);
      this.setState({
        defaultActiveFirst: true,
      });
      return true;
    }

    if (keyCode === KeyCode.RIGHT) {
      if (isOpen) {
        menu.onKeyDown(e);
      } else {
        this.triggerOpenChange(true);
        this.setState({
          defaultActiveFirst: true,
        });
      }
      return true;
    }
    if (keyCode === KeyCode.LEFT) {
      let handled;
      if (isOpen) {
        handled = menu.onKeyDown(e);
      } else {
        return undefined;
      }
      if (!handled) {
        this.triggerOpenChange(false);
        handled = true;
      }
      return handled;
    }

    if (isOpen && (keyCode === KeyCode.UP || keyCode === KeyCode.DOWN)) {
      return menu.onKeyDown(e);
    }
  },

  onOpenChange(e) {
    this.props.onOpenChange(this.addKeyPath(e));
  },

  onMouseEnter(e) {
    const props = this.props;
    this.clearSubMenuLeaveTimer(props.parentMenu.subMenuInstance !== this);
    props.onMouseEnter({
      key: props.eventKey,
      domEvent: e,
    });
  },

  onTitleMouseEnter(e) {
    const props = this.props;
    const parentMenu = props.parentMenu;
    this.clearSubMenuTitleLeaveTimer(parentMenu.subMenuInstance !== this);
    if (parentMenu.menuItemInstance) {
      parentMenu.menuItemInstance.clearMenuItemMouseLeaveTimer(true);
    }
    props.onItemHover({
      key: props.eventKey,
      item: this,
      hover: true,
      trigger: 'mouseenter',
    });
    if (props.openSubMenuOnMouseEnter) {
      this.triggerOpenChange(true);
    }
    this.setState({
      defaultActiveFirst: false,
    });
    props.onTitleMouseEnter({
      key: props.eventKey,
      domEvent: e,
    });
  },

  onTitleMouseLeave(e) {
    const { props } = this;
    const parentMenu = props.parentMenu;
    parentMenu.subMenuInstance = this;
    parentMenu.subMenuTitleLeaveFn = () => {
      const eventKey = props.eventKey;
      if (this.isMounted()) {
        // leave whole sub tree
        // still active
        if (props.mode === 'inline' && props.active) {
          props.onItemHover({
            key: eventKey,
            item: this,
            hover: false,
            trigger: 'mouseleave',
          });
        }
        props.onTitleMouseLeave({
          key: props.eventKey,
          domEvent: e,
        });
      }
    };
    parentMenu.subMenuTitleLeaveTimer = setTimeout(parentMenu.subMenuTitleLeaveFn, 100);
  },

  onMouseLeave(e) {
    const { props } = this;
    const parentMenu = props.parentMenu;
    parentMenu.subMenuInstance = this;
    parentMenu.subMenuLeaveFn = () => {
      const eventKey = props.eventKey;
      if (this.isMounted()) {
        // leave whole sub tree
        // still active
        if (props.mode !== 'inline') {
          if (props.active) {
            props.onItemHover({
              key: eventKey,
              item: this,
              hover: false,
              trigger: 'mouseleave',
            });
          }
          if (this.isOpen()) {
            if (props.closeSubMenuOnMouseLeave) {
              this.triggerOpenChange(false);
            }
          }
        }
        // trigger mouseleave
        props.onMouseLeave({
          key: eventKey,
          domEvent: e,
        });
      }
    };
    // prevent popup menu and submenu gap
    parentMenu.subMenuLeaveTimer = setTimeout(parentMenu.subMenuLeaveFn, 100);
  },

  onTitleClick(e) {
    const { props } = this;
    props.onTitleClick({
      key: props.eventKey,
      domEvent: e,
    });
    if (props.openSubMenuOnMouseEnter) {
      return;
    }
    this.triggerOpenChange(!this.isOpen(), 'click');
    this.setState({
      defaultActiveFirst: false,
    });
  },

  onSubMenuClick(info) {
    this.props.onClick(this.addKeyPath(info));
  },

  onSelect(info) {
    this.props.onSelect(info);
  },

  onDeselect(info) {
    this.props.onDeselect(info);
  },

  getPrefixCls() {
    return `${this.props.rootPrefixCls}-submenu`;
  },

  getActiveClassName() {
    return `${this.getPrefixCls()}-active`;
  },

  getDisabledClassName() {
    return `${this.getPrefixCls()}-disabled`;
  },

  getSelectedClassName() {
    return `${this.getPrefixCls()}-selected`;
  },

  getOpenClassName() {
    return `${this.props.rootPrefixCls}-submenu-open`;
  },

  saveMenuInstance(c) {
    this.menuInstance = c;
  },

  addKeyPath(info) {
    return {
      ...info,
      keyPath: (info.keyPath || []).concat(this.props.eventKey),
    };
  },

  triggerOpenChange(open, type) {
    const key = this.props.eventKey;
    this.onOpenChange({
      key,
      item: this,
      trigger: type,
      open,
    });
  },

  clearSubMenuTimers(callFn) {
    this.clearSubMenuLeaveTimer(callFn);
    this.clearSubMenuTitleLeaveTimer(callFn);
  },

  clearSubMenuTitleLeaveTimer(callFn) {
    const parentMenu = this.props.parentMenu;
    if (parentMenu.subMenuTitleLeaveTimer) {
      clearTimeout(parentMenu.subMenuTitleLeaveTimer);
      parentMenu.subMenuTitleLeaveTimer = null;
      if (callFn && parentMenu.subMenuTitleLeaveFn) {
        parentMenu.subMenuTitleLeaveFn();
      }
      parentMenu.subMenuTitleLeaveFn = null;
    }
  },

  clearSubMenuLeaveTimer(callFn) {
    const parentMenu = this.props.parentMenu;
    if (parentMenu.subMenuLeaveTimer) {
      clearTimeout(parentMenu.subMenuLeaveTimer);
      parentMenu.subMenuLeaveTimer = null;
      if (callFn && parentMenu.subMenuLeaveFn) {
        parentMenu.subMenuLeaveFn();
      }
      parentMenu.subMenuLeaveFn = null;
    }
  },

  isChildrenSelected() {
    const ret = { find: false };
    loopMenuItemRecusively(this.props.children, this.props.selectedKeys, ret);
    return ret.find;
  },
  isOpen() {
    return this.props.openKeys.indexOf(this.props.eventKey) !== -1;
  },

  renderChildren(children) {
    const props = this.props;
    //console.log("props:",props);
    //console.log("props.openAnimation:",props.openAnimation," props.openTransitionName:",props.openTransitionName);
    const baseProps = {
      mode: props.mode === 'horizontal' ? 'vertical' : props.mode,
      visible: this.isOpen(),
      level: props.level + 1,
      inlineIndent: props.inlineIndent,
      focusable: false,
      onClick: this.onSubMenuClick,
      onSelect: this.onSelect,
      onDeselect: this.onDeselect,
      onDestroy: this.onDestroy,
      selectedKeys: props.selectedKeys,
      eventKey: `${props.eventKey}-menu-`,
      openKeys: props.openKeys,
      openTransitionName: props.ifFixed?"":props.openTransitionName,
      openAnimation: props.ifFixed?"":props.openAnimation,
      onOpenChange: this.onOpenChange,
      closeSubMenuOnMouseLeave: props.closeSubMenuOnMouseLeave,
      defaultActiveFirst: this.state.defaultActiveFirst,
      multiple: props.multiple,
      prefixCls: props.rootPrefixCls,
      id: this._menuId,
      ref: this.saveMenuInstance
    };
    //style:props.ifFixed?{"possion":"fixed"}:{} //add by dyf
    //style={this.props.ifFixed?{"possion":"fixed"}:{}}
    return <SubPopupMenu {...baseProps}>{children}</SubPopupMenu>;
  },

  render() {
    const isOpen = this.isOpen();
    this.haveOpen = this.haveOpen || isOpen;
    const props = this.props;
    const prefixCls = this.getPrefixCls();
    const classes = {
      [props.className]: !!props.className,
      [`${prefixCls}-${props.mode}`]: 1,
    };

    classes[this.getOpenClassName()] = isOpen;
    classes[this.getActiveClassName()] = props.active;
    classes[this.getDisabledClassName()] = props.disabled;
    classes[this.getSelectedClassName()] = this.isChildrenSelected();

    this._menuId = this._menuId || guid();
    classes[prefixCls] = true;
    classes[`${prefixCls}-${props.mode}`] = 1;
    let titleClickEvents = {};
    let mouseEvents = {};
    let titleMouseEvents = {};
    if (!props.disabled) {
      titleClickEvents = {
        onClick: this.onTitleClick,
      };
      mouseEvents = {
        onMouseLeave: this.onMouseLeave,
        onMouseEnter: this.onMouseEnter,
      };
      // only works in title, not outer li
      titleMouseEvents = {
        onMouseEnter: this.onTitleMouseEnter,
        onMouseLeave: this.onTitleMouseLeave,
      };
    }
    const style = {};
    if (props.mode === 'inline') {
        //console.log("props.inlineIndent:",props.inlineIndent," props.level:",props.level," props.inlineIndent * props.level:",props.inlineIndent * props.level);
        if(this.props.ifFixed) {
          if(props.level==1)
              style.paddingLeft = props.inlineIndent+(props.level-1)*10;//props.inlineIndent * props.level;
          else
              style.paddingLeft = props.inlineIndent+25+(props.level-1)*12;//props.inlineIndent * props.level;
        }
        else {
           style.paddingLeft = props.inlineIndent * props.level;
        }
   }
    const selectedClassName = this.props.selectedClassName;
    return (
      <li id={"sub"+this._menuId} className={classnames(classes)} {...mouseEvents}>
        <div
          style={style}
          className={`${prefixCls}-title ${selectedClassName}`}
          {...titleMouseEvents}
          {...titleClickEvents}
          aria-open={isOpen}
          aria-owns={this._menuId}
          aria-haspopup="true"
        >
          {props.title}
        </div>
        {this.renderChildren(props.children)}
      </li>
    );
  }
});

SubMenu.isSubMenu = 1;

export default SubMenu;
