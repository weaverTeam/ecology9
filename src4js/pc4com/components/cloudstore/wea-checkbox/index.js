import {Checkbox} from 'antd'
// import Checkbox from '../../_antd1.11.2/checkbox'

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value:""
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.defaultChecked !== this.props.defaultChecked;
	}
	render() {
		return (
			<Checkbox defaultChecked={false} {...this.props} />
		)
	}
}

export default Main;