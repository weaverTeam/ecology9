import {TimePicker,Badge} from 'antd';
// import TimePicker from '../../_antd1.11.2/time-picker'
// import Badge from '../../_antd1.11.2/badge'
//import DateTimeTool from '../tool/DateTime';

class WeaTimePicker extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value:props.value?props.value:""
		};
	}
	componentWillReceiveProps(nextProps) {
		if(this.props.value!==nextProps.value) {
			this.setState({
				value:nextProps.value
			});
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value;
	}
	render() {
		const {value} = this.state;
		let {et} = this.props;
		et = et?et:0;
		//defaultValue = defaultValue?defaultValue:"";
		if(et==2 && value=="") {
			return (
				<Badge dot><TimePicker {...this.props} format="HH:mm" onChange={this.changeDate.bind(this)} /></Badge>
			)
		}
		return (
			<TimePicker {...this.props} format="HH:mm" onChange={this.changeDate.bind(this)} />
		)
	}
	changeDate(v,s) {
		//console.log("v:",v," s:",s);
		//console.log("s:",s);
		this.props.onChange(s,s);
	}
}

/*


	changeDate(v,s) {
		//onChange={this.changeDate.bind(this)}
		//format="HH:mm:ss"
		console.log("s :",s);
		//this.props.onChange(DateTimeTool.format(v,"HH:mm:ss"));
		//this.props.onChange(s);
	}

*/

export default WeaTimePicker;

/*
import './css/WeaInput.css';
const FormItem = Form.Item;

const WeaTimePicker = React.createClass({
	render() {
		if(!this.props.NoFormItem) {
			return <FormItem label={this.props.label}>
						<TimePicker name={this.props.name} className="WeaInputWidth" onChange={this.onChange} />
		        	</FormItem>;
		}
		else {
			return <TimePicker name={this.props.name} className="WeaInputWidth" onChange={this.onChange} />;
		}
	},
	change() {

	}
});

export default WeaTimePicker;
*/