import {Tree, Row, Col, Modal, Tooltip, Tabs, Table, Select, Menu, Button, Input, Spin} from 'antd'
// import Tree from '../../_antd1.11.2/Tree';
// import Row from '../../_antd1.11.2/Row';
// import Col from '../../_antd1.11.2/Col';
// import Modal from '../../_antd1.11.2/Modal';
// import Tooltip from '../../_antd1.11.2/Tooltip';
// import Tabs from '../../_antd1.11.2/Tabs';
// import Table from '../../_antd1.11.2/Table';
const TabPane = Tabs.TabPane;
const TreeNode = Tree.TreeNode;
// import Select from '../../_antd1.11.2/Select';
const Option = Select.Option;
// import isArray  from 'lodash/isArray'
// import Menu from '../../_antd1.11.2/menu';
// import Button from '../../_antd1.11.2/Button';
// import Input from '../../_antd1.11.2/Input';
// import Spin from '../../_antd1.11.2/Spin';

import cloneDeep from 'lodash/cloneDeep';
import WeaScroll from '../wea-scroll';
import WeaInputSearch from '../wea-input-search';
import * as util from './util';

export default class main extends React.Component {
    constructor(props) {
        super(props);
        const {url,isaccount,min,max,componyParam,seacrhPara} = props;
        // console.log("url",url);
        let propUrl = `${url}?isaccount=${isaccount}&min=${min}&max=${max}${!!seacrhPara ? `${seacrhPara}` : ''}`;
        this.state = {
            url:propUrl,
            showDrop: false,
            loading: true,
            datas:[],
            record:{},
            selectedRowKeys:'',
            rowIndex:'',
            id:"",
            name:'',
            rowKey:'',
            count:0,
            current:1,
            pageSize:10,
            min:min,
            max:max,
        }
    }
    componentDidMount() {
        let {url}= this.state;
        // console.log("url",url);
        let _this = this;
        url = `${url}&${new Date().getTime()}=`;
        util.getDatas(url,"GET").then(data => {//,{isaccount:isaccount,min:min,max:max}
            // console.log("data",data);
            _this.setState({
                loading:false,
                datas:data.datas,
                count:data.count,
            })
        });
    }
    componentWillReceiveProps(nextProps) {
        // console.log("nextProps",nextProps);
        const {url,isaccount,min,max,componyParam,seacrhPara}=this.props;
        let _this = this;
        if(this.props.componyParam != nextProps.componyParam || seacrhPara !== nextProps.seacrhPara){
            // let newUrl = url + nextProps.componyParam;
            let newUrl = `${url}?isaccount=${isaccount}&min=${min}&max=${max}${nextProps.componyParam?nextProps.componyParam:''}${nextProps.seacrhPara ? `${nextProps.seacrhPara}`:''}`;
            newUrl = `${newUrl}&${new Date().getTime()}=`;
            util.getDatas(newUrl,"GET").then(data => {
            // console.log("data",data);
                _this.setState({datas:data.datas})
            });
        }
    }
    onChange(activeKey) {
        this.setState({
            activeKey
        });
    }
    onRowClick(record, index){
        // console.log("record",record,"index",index);
        const{id,name,rowIndex} = this.state;
        const{isMult}=this.props;
        if(isMult)return false;//多选取消行点击
        if (typeof this.props.setData === "function") {
            this.props.setData(record.key,record.lastname,record);
        }
        this.setState({
            id:record.key,
            name:record.lastname,
            rowKey:record.key
        })
    }
    rowCls = (record, index) => {
        // console.log('rowCls===',record,index);
        if(this.state.rowKey === record.key){
            return 'wea-browser-old-row-height-light';
        }
    }
    onRowDoubleClick(record, index) {
        const{isMult}=this.props;
        if(isMult)return false;//多选取消行点击
        if(typeof this.props.onRowDoubleClick  === 'function')
         this.props.onRowDoubleClick(record);
    }

    render() {
        let that = this;
        const {icon,iconBgcolor,title,allWf,bookMarkWf, browerType,listUrl,countUrl,otherPara,} = this.props;
        const {showDrop,activeKey,selectedRowKeys,id,name,datas,current,pageSize,loading,count} = this.state;
        // console.log('datas',datas.length);
        const pagination = {
            total: count ,
            showSizeChanger: true,
            current:current,
            pageSize:pageSize,
            onShowSizeChange(current, pageSize) {
                //console.log('Current: ', current, '; PageSize: ', pageSize);
                that.goPage(current,pageSize);
            },
            onChange(current) {
                //console.log('Current: ', current);
                that.goPage(current);
            }
        };
        const columns = [{
          title: '头像',
          dataIndex: 'messagerurl',
          key:"messagerurl",
          width: 100,
          render: (text) => <div className="wea-table-hr-portrait">{text}</div>
        }, {
          title: '姓名',
          dataIndex: 'lastname',
          key:"lastname",
          width: 100,
        },{
          title: '职位',
          dataIndex: 'jobtitlename',
          key:"jobtitlename",
        }];

        const dataSource = [];
        this.state.datas.forEach(item=>{
            dataSource.push({
                key:item.id,
                lastname:item.lastname,
                jobtitlename:item.jobtitlename,
                pinyinlastname:item.pinyinlastname,
                messagerurl:<img className='wea-tree-hr-portrait'  src={item.messagerurl} alt="img"/>,
            })
        });
        // 通过 rowSelection 对象表明需要行选择
        let _this = this;
        const rowSelection = {
          onChange(selectedRowKeys, selectedRows) {
            if(typeof _this.props.onChange === 'function'){
                // console.log('selectedRows',selectedRows);
                _this.props.onChange(selectedRows);
            }
          },
          onSelect(record, selected, selectedRows) {
            // console.log(record, selected, selectedRows);
          },
          onSelectAll(selected, selectedRows, changeRows) {
            // console.log(selected, selectedRows, changeRows);
          },
        };
        let hasRowSelection = {rowSelection:null};
        this.props.isMult && (hasRowSelection = {rowSelection:rowSelection});
        return  (<div>
                    <WeaScroll className="wea-scroll" typeClass="scrollbar-macosx" style={{"height":"300px",width:"100%"}}>
                        <Table className="wea-table-hr"
                            rowClassName={this.rowCls}
                            showHeader={false}
                            onRowClick={this.onRowClick.bind(this)}
                            onRowDoubleClick={this.onRowDoubleClick.bind(this)}
                            columns={columns}
                            dataSource={dataSource}
                            pagination={pagination}
                            {...hasRowSelection}
                        />
                    </WeaScroll>
                </div>)

    }

    goPage(current,pageSizeChange) {
        let that = this;
        let {pageSize} = this.state;
        if(pageSizeChange) pageSize = pageSizeChange;
        const min = pageSize*(current-1)+1;
        const max = pageSize*current;

        let {url,isaccount,seacrhPara}=this.props;
        let newSearchPara = !!seacrhPara ? `${seacrhPara}&timestamp=${new Date().getTime()}` : '';
        let newUrl = `${url}?isaccount=${isaccount}&min=${min}&max=${max}${newSearchPara}`;
        util.getDatas(newUrl,"GET").then(data => {//,{isaccount:isaccount,min:min,max:max}
            // console.log("data",data);
            that.setState({
                loading:false,
                datas:data.datas,
                current:current,
                pageSize:pageSize,
                min:min,
                max:max,
            })
        });
    }
}

