import WeaInput4Base from '../wea-input4-base'

class WeaInpput4Request extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base 
				title="请求"
				browerType={['list']}
				listUrl="/cloudStore/system/ControlServlet.jsp?action=Action_GetBrowerDataList"
				countUrl="/cloudStore/system/ControlServlet.jsp?action=Action_GetBrowerDataCount"
				modalWidth="800px"
				datasType="request"
				{...this.props} 
			/>
		)
	}
}

export default WeaInpput4Request;



/*

<WeaList 
				actionUrl="/cloudStore/system/ControlServlet.jsp?action=Action_GetBrowerDataList"
				{...this.props}
			/>

*/