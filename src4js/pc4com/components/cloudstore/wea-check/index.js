const Check = {
	isNull:function(s) {
		return typeof(s)=="undefined";
	},
	isEmpty:function(s) {
		return typeof(s)=="undefined"||s=="";
	},
	isArray:function(s) {
		if(this.isEmpty(s))
			return false;
		if(!s instanceof Array)
			return false;
		return true;
	},
	setNotNull(s) {
		if(!this.isEmpty(s)) {
			return s;
		}
		else {
			return "";
		}
	},
	setArray(s) {
		if(!this.isArray(s)) {
			return [];
		}
		return s;
	},
	setEval(txtValue) {
		txtValue = txtValue.replace(/\/\*((\n|\r|.)* )\*\//mg, "");	//去掉多行注释/*..*/
		txtValue = txtValue.replace(/(\s+)\/\/(.*)\n/g,"");	//去掉单行注释//(前面有空格的注释)
		txtValue = txtValue.replace(/;\/\/(.*)\n/g,";");	//去掉单行注释//(前面是分号的注释)	
		txtValue = txtValue.replace(/\/\/[^"][^']\n/g,"");	//去掉单行注释//(//后面只有一个'或一个"的不替换)	
		txtValue = txtValue.replace(/[\r]/g,"\\\r");	//替换换行
		txtValue = txtValue.replace(/[\n]/g,"\\\n");	//替换回车
		return txtValue;
	}
}

export default Check;