import { Upload, message, Button, Icon ,Input,Modal } from 'antd';
// import Upload from '../../_antd1.11.2/upload'
// import message from '../../_antd1.11.2/message'
// import Button from '../../_antd1.11.2/button'
// import Icon from '../../_antd1.11.2/icon'
// import Input from '../../_antd1.11.2/input'
// import Modal from '../../_antd1.11.2/modal'

class Test1 extends React.Component {
	render() {
		return (
		  <Upload {...props}>
		    <Button type="ghost">
		      <Icon type="upload" /> 点击上传
		    </Button>
		  </Upload>
		)
	}
}

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible:false,
            ids:props.value,
            names:props.valueSpan,
            status: '',
        }
    }
    componentWillReceiveProps(nextProps) {
        const {value,valueSpan} = this.props;
        if(value!==nextProps.value&&valueSpan!==nextProps.valueSpan) {
          //console.log("this.props.value:",this.props.value);
          //console.log("nextProps.value:",nextProps.value);
          //console.log("nextProps.value:",nextProps.value," nextProps.valueSpan:",nextProps.valueSpan);
            this.setState({
                ids:nextProps.value,
                names:nextProps.valueSpan
            });
        }
    }
    render() {
        let that = this;
        const {visible,ids,names,status} = this.state;
        const {style} = this.props;
        const props = {
          name: 'Filedata',
          action: '/cloudstore/doc/UploadFile.jsp',
          onRemove(file) {
              //console.log("reomve file:",file);
              const idArr = ids && ids!==""?ids.split(","):[];
              const nameArr = names && names!==""?names.split(","):[];
              let ids = new Array();
              let names = new Array();
              for(let i=0;i<idArr.length;i++) {
                  if(idArr[i]!==file.response) {
                      ids.push(idArr[i]);
                      names.push(nameArr[i]);
                  }
              }
              that.setState({
                  ids:ids?ids.join(","):"",
                  names:names?names.join(","):""
              });
//return true;
          },
          onChange(info) {
            if (info.file.status !== 'uploading') {
            	this.setState({status: 'uploading' })
             	console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
              message.success(`${info.file.name} 上传成功。`);
              if(info.fileList) {
                  let ids = new Array();
                  let names = new Array();
                  info.fileList.map((fileList)=>{
                      ids.push(fileList.response);
                      names.push(fileList.name);
                  });
                  that.setState({
                  	status: 'done',
                    ids:ids?ids.join(","):"",
                    names:names?names.join(","):""
                  })
              }
            } else if (info.file.status === 'error') {
            	this.setState({status: 'error'})
             	message.error(`${info.file.name} 上传失败。`);
            }
            //console.log("info:",info);

          }
        };
        //console.log("ids:",ids," names:",names);
        return (
            <div>
            <div className="ant-search-custom">
            <Input key="theInput" style={style} addonAfter={<div style={{"cursor":"pointer"}}>
            <i onClick={()=>this.setState({visible:true})} className="anticon anticon-search"></i>
            </div>} value={names} />
            </div>
            <Modal title="请�?�择附件" visible={visible}
              onOk={()=>{
              	if(status === 'done' || ''){
              		this.setState({visible:false});
              		if(typeof(this.props.onChange)=="function") {
              		    this.props.onChange(ids,names);
              		}
              	}else{
              		message.error('文件上传�?...请稍等！');
              	}
              }} onCancel={()=>this.setState({visible:false})}
            >
              <Upload {...props}>
                <Button type="ghost">
                  <Icon type="upload" /> 点击上传
                </Button>
              </Upload>
            </Modal>
            </div>
        )
    }
}

export default Main;