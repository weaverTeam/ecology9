import WeaInput4Base from '../wea-input4-base'

class WeaInput4Com extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base 
				title="分部"
				topPrefix="gs"
				prefix="gs"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Com"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props}
			/>
		)
	}
}

export default WeaInput4Com;


/*

const WeaInput4Com = React.createClass({
	render() {
		return  <WeaInput4Base 
				title="公司"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="gs"
				prefix="gs"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Com"]}
				isMult={this.props.isMult}
				ifAsync={true}
				ifCheck={true}
				value={this.props.value}
				valueName={this.props.valueName}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				style={this.props.style}
				onChange={this.props.onChange}
				NoFormItem={this.props.NoFormItem}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

export default WeaInput4Com;

*/