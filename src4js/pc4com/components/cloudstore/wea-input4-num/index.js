import {Input,Badge} from 'antd';
// import Input from '../../_antd1.11.2/input'
// import Badge from '../../_antd1.11.2/badge'

class Main extends React.Component {
	constructor(props) {
		super(props);
		//console.log("step:",props.step);
		const value = (typeof props.value !== 'undefined' && props.value !== '') ? this.changeStep(props.value,props.step) : props.value;
		this.state = {
			value:value,
			showBadge:false
		};
	}
	componentWillReceiveProps(nextProps) {
		if(this.props.value!==nextProps.value) {
			const value = (typeof nextProps.value !== 'undefined' && nextProps.value !== '') ? this.changeStep(nextProps.value,nextProps.step) : nextProps.value;
			this.setState({
				value:value
			});
			//console.log("this.props.value:",this.props.defaultValue);
			//console.log("nextProps.value:",nextProps.defaultValue);
			/*
			this.setState({
				ids:nextProps.value,
				names:nextProps.valueSpan
			});
			*/
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		//if(nextProps.defaultValue !== this.props.defaultValue) {
			//console.log("nextProps.defaultValue:",nextProps.defaultValue);
			//console.log("this.props.defaultValue:",this.props.defaultValue);
		//}
		return nextProps.value !== this.props.value || nextState.showBadge!==this.state.showBadge || nextState.value !== this.state.value;
	}
	render() {
		const {value,showBadge} = this.state;
		let {et} = this.props;
		et = et?et:0;
		let theProps = this.props;
		//if(value!="")
		theProps.value = value;
		//console.log("onChange?");
		//console.log("value1:",value);
		if(et==2 && (value=="" || showBadge)) {
			return (
				<Badge dot><Input {...theProps} onChange={this.setText.bind(this)} onBlur={this.hideBadge.bind(this)} onFocus={this.showBadge.bind(this)} /></Badge>
			)
		}
		else {
			return (
				<Input {...theProps} onChange={this.setText.bind(this)} onBlur={this.hideBadge.bind(this)} onFocus={this.showBadge.bind(this)} />
			)
		}
		/*
		let {et,defaultValue} = this.props;
		et = et?et:0;
		defaultValue = defaultValue?defaultValue:"";
		let inputArr = (<div><Input type="text" {...this.props} /></div>)
		if(et==2 && defaultValue=="") {
			inputArr = (<Badge dot>{inputArr}</Badge>)
		}
		return inputArr
		*/
	}
	setText(e) {
		//console.log("is ok");
		//console.log("value:",value);
		//console.log("ok");
		const value = e.target.value;
		this.setState({
			value:value
		});
		if(typeof(this.props.onChange)=="function") {
			this.props.onChange(e);
		}
	}
	hideBadge(e) {
		//console.log("e:",e);
		this.setState({
			showBadge:false
		});
		//const {step} = this.props;
		//console.log("step:",step);
		//const {value} = this.state;
		const {step} = this.props;
		//console.log("hide step:",step);
		let newValue = e.target.value;
		newValue = (typeof newValue !== 'undefined' && newValue !== '') ? this.changeStep(newValue,step) : newValue;
		this.setState({
			value:newValue
		});
		//console.log("newValue:",newValue);
		if(typeof(this.props.onBlur)=="function") {
			this.props.onBlur(newValue);
		}
	}
	showBadge(e) {
		//console.log("e:",e);
		this.setState({
			showBadge:true
		});
		if(typeof(this.props.onFocus)=="function") {
			this.props.onFocus(e);
		}
	}
	changeStep(newValue,step) {
		//const {step} = this.props;
		//alert("ok");
		//console.log("now step:",step);
		try {
			newValue = parseFloat(newValue);
		}
		catch(e) {newValue=0;}
		let r = /^[0-9]*[1-9][0-9]*$/;
		if(!step || (step && step==0)) {
			eval(`r = /^[+-]?\\d*$/;`);
			newValue = newValue.toFixed(0);
		}
		else if(step && step>0) {
			//eval("r = /^\\+?(\\d*\\.\\d{"+step+"})$/;");
			eval(`r = /^[+-]?(\\d*\\.\\d{${step}})$/;`);
			//r = /^\+?(\d*\.\d{2})$/
			newValue = newValue.toFixed(parseInt(step));
		}
		//console.log("newValue:",newValue);
		//console.log("e.target.value:",e.target.value);
		//console.log("r:",r);
		//console.log("r.test(newValue):",r.test(newValue));
		if(!r.test(newValue)&&newValue!="") {
			newValue = 0;
		}
		return newValue;
	}
}

export default Main;

/*

const FormItem = Form.Item;

const WeaInput = React.createClass({
	render() {
		return <FormItem label={this.props.label} >
			   <InputNumber name={this.props.name} style={{width: 184}} defaultValue="" />
			   </FormItem>;
	}
});

export default WeaInput;

*/