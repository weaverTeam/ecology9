import WeaInput4Base from '../wea-input4-base'

class WeaInput4Dep extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base
				title="部门"
				topPrefix="gs"
				prefix="bm"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Dep"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props}
			/>
		)
	}
}

export default WeaInput4Dep;

/*

const WeaInput4Dep = React.createClass({
	render() {
		return  <WeaInput4Base
				title="部门"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="gs"
				prefix="bm"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Dep"]}
				isMult={this.props.isMult}
				ifAsync={true}
				ifCheck={true}
				style={this.props.style}
				value={this.props.value}
				valueName={this.props.valueName}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				NoFormItem={this.props.NoFormItem}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

export default WeaInput4Dep;
*/