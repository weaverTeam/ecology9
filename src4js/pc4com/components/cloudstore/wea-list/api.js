const server = "";

const getFd = (values) => {
    let fd = "";
    //var data = new FormData();
    for(let p in values) {
        fd += p+"="+encodeURIComponent(values[p])+"&";
        //data.append(p,values[p]);
    }
    if(fd!="") {
        fd = fd.substring(0,fd.length-1);
    }
    return fd;
}

const getFetchParams = (method,params)=>{
    let obj = {
        method:method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
            'X-Requested-With':'XMLHttpRequest'
        },
    };
    if(server=="") {
        obj.credentials = "include";
    }
    // else {
    //     obj.mode = "no-cors";
    // }
    if(params) {
        obj.body = getFd(params);
    }
    //console.log("obj:",obj);
    return obj;
}

export const doDatasGet = (url,params) => {
    return new Promise((resolve,reject) => {
        fetch(url,{
            method: 'post',
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'},
            credentials: 'include',
            body: params
        }).then(function(response) {
            return response.json();
        }).then((datas)=>{
            if(datas && datas.status && datas.status==="false") {
                reject(datas.error?datas.error:"后端数据处理异常");
            }
            else {
                resolve(datas);
            }
        }).catch(function(ex) {
            resolve([]);
            //alert('后端AJAX异常:'+ex);
        });
    });
}