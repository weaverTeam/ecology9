import WeaInput4Base from '../wea-input4-base'

class WeaInpput4Request extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base
				title="费用科目"
				topPrefix="type"
				prefix="subject"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Subject"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props}
			/>
		)
	}
}

export default WeaInpput4Request;