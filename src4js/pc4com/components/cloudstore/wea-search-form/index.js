import {Row,Col,Form} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Form from '../../_antd1.11.2/Form'

const SearchForm = React.createClass({
	render() {
		var style = {};
		if(!this.props.showForm) {
			style = {"display":"none"}
		}
		return <Row type="flex" justify="center" style={style}>
					<Col span="24" style={{"margin-top":"10px"}}>
						<Row className="advanced-search-form">
							<Col span="24">
		                        <Form inline id={this.props.id}>
		                            {this.props.children}
		                        </Form>
		                    </Col>
		                    <Col span="24">
		                    	{this.props.buttons}
		                    </Col>
						</Row>
					</Col>
               </Row>
	}
});

export default SearchForm;