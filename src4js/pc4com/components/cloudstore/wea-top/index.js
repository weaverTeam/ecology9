import {Row,Col} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'

const Top = React.createClass({
	render() {
		return <Row>
			   <Col span="24">
			   <Row style={{"padding-bottom":"5px","padding-top":"5px","border-bottom":"1px solid #eeeeee"}}>
			   		<Col span="12" style={{"paddingLeft":"20px"}}><h2>{this.props.title}</h2></Col>
			   		<Col span="12" style={{"text-align":"right","paddingRight":"10px"}}>{this.props.children}</Col>
			   </Row>
			   </Col>
			   </Row>
	}
});

export default Top;