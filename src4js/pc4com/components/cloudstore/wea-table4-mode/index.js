import * as API from './api'

import {Table} from 'antd'
// import Table from '../../_antd1.11.2/table'

//import EcTable from '../ec-table'

//console.log("EcTable:",EcTable);

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			columns:[],
			datas:[],
			count:0,
			id:props.customid||0,
			thisorg:"",
			thidate:"",
			current:1,
			pageSize:10,
			min:1,
			max:10,
			loading:false,
			fixedLeft:props.fixedLeft||[],
			fixedRight:props.fixedRight||[],
			width:1500
		}
	}
	componentDidMount() {
		//console.log("yes");
		let that = this;
		const {id,thisorg,thidate,min,max,fixedLeft,fixedRight} = this.state;
		this.setState({
			loading:true
		})
		API.doModeHeadGet({id:id,thisorg:thisorg,thidate:thidate}).then((data)=>{
			console.log("heads:",data);
			let heads = data.heads;
			let columns = [];
			let width = 0;
			let noWidth = false;
			heads.map((head)=>{
				const realWidth = head.width?head.width*15:150;
				width += realWidth;
				let column = {
					title:head.label,
					dataIndex:head.dbname,
					width:realWidth
				};
				if(fixedLeft.indexOf(head.dbname)>=0) {
					column.fixed = "left"
					column.width = realWidth;
				}
				else if(fixedRight.indexOf(head.dbname)>=0) {
					column.fixed = "right"
					column.width = realWidth;
				}
				else if(!noWidth) {
					noWidth = true;
				}
				else {
					column.width = realWidth;
				}
				if(head.dbtype.indexOf("decimal")===0||head.dbtype.indexOf("number")===0) {
					column.title = (<div style={{"textAlign":"right"}}>{head.label}</div>)
					column.render = (text,record,index)=>{
						return that.renderColumn(head,record,index,{"textAlign":"right"});
					}
				}
				else {
					column.render = (text,record,index)=>{
						return that.renderColumn(head,record,index,{});
					}
				}
				columns.push(column);
			});
			that.setState({
				columns:columns,
				width:width
			});
			return API.doModeDataCountGet({id:id,thisorg:thisorg,thidate:thidate});
		}).then((data)=>{
			console.log("count:",data);
			that.setState({
				count:data.count
			});
			return API.doModeDataGet({id:id,thisorg:thisorg,thidate:thidate,min:min,max:max})
		}).then((data)=>{
			console.log("datas:",data);
			const datas = data.datas;
			// for(let i=0;i<10000;i++) {
			// 	datas = datas.concat(data.datas);
			// }
			that.setState({
				datas:datas,
				loading:false
			});
		});
	}
	renderColumn(head,record,index,style) {
		const value = record[head.dbname];
		let valueSpan = record[head.dbname+"Span"];
		valueSpan = (valueSpan && valueSpan!="")?valueSpan:value;
		if(head.hreflink && head.hreflink!="") {
			return (
				<div style={style}><a className="ant-form-text" href={head.hreflink} target="_blank">{valueSpan}</a></div>
			)
		}
		else {
			return (
				<div style={style}>{valueSpan}</div>
			)
		}
	}
	render() {
		let that = this;
		const {columns,datas,count,current,pageSize,loading,width} = this.state;
		const rowSelection = null;
		const pagination = {
			total: count,
			showSizeChanger: true,
			current:current,
			pageSize:pageSize,
			onShowSizeChange(current, pageSize) {
			 	//console.log('Current: ', current, '; PageSize: ', pageSize);
			 	that.goPage(current,pageSize);
			},
			onChange(current) {
				//console.log('Current: ', current);
				that.goPage(current);
			}
		};
		return (
			<div style={{margin:20}}>
				<Table rowSelection={rowSelection} pagination={pagination} columns={columns} dataSource={datas} loading={loading} scroll={{x:width+50}} bordered size="middle" />
			</div>
		)
	}
	goPage(current,pageSizeChange) {
		let that = this;
		let {pageSize} = this.state;
		const {id,thisorg,thidate} = this.state;
		if(pageSizeChange) pageSize = pageSizeChange;
		const min = pageSize*(current-1)+1;
		const max = pageSize*current;
		this.setState({
			loading:true
		})
		API.doModeDataGet({id:id,thisorg:thisorg,thidate:thidate,min:min,max:max}).then((data)=>{
			console.log("datas:",data," min:",min," max:",max);
			const datas = data.datas;
			// for(let i=0;i<10000;i++) {
			// 	datas = datas.concat(data.datas);
			// }
			that.setState({
				datas:datas,
				current:current,
				pageSize:pageSize,
				min:min,
				max:max,
				loading:false
			});
		});
	}
}

//<EcTable className="bordered" rowSelection={rowSelection} pagination={pagination} columns={columns} data={datas} loading={loading} scroll={{x:width}} size="middle" />


export default Main