const getFd = (values) => {
    let fd = "";
    //var data = new FormData();
    for(let p in values) {
        fd += p+"="+encodeURIComponent(values[p])+"&";
        //data.append(p,values[p]);
    }
    if(fd!="") {
        fd = fd.substring(0,fd.length-1);
    }
    return fd;
}
const server = "";

const getFetchParams = (method,params)=>{
    let obj = {
        method:method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
            'X-Requested-With':'XMLHttpRequest'
        },
    };
    if(server=="") {
        obj.credentials = "include";
    }
    // else {
    //     obj.mode = "no-cors";
    // }
    if(params) {
        obj.body = getFd(params);
    }
    //console.log("obj:",obj);
    return obj;
}

export const doModeHeadGet = (params) => {
    return new Promise((resolve,reject) => {
        fetch(server+"/api/ec/mode/search/heads",getFetchParams("POST",params)).then(function(response) {
            const datas = response.json();
            if(datas && datas.status && datas.status==="false") {
                reject(datas.error?datas.error:"后端数据处理异常");
            }
            else {
                resolve(datas);
            }
        }).catch(function(ex) {
            reject("后端AJAX异常:",ex);
            alert("ex:",ex);
            //console.log('parsing failed', ex)
        });
    });
}

export const doModeDataCountGet = (params) => {
    return new Promise((resolve,reject) => {
        fetch(server+"/api/ec/mode/search/count",getFetchParams("POST",params)).then(function(response) {
            const datas = response.json();
            if(datas && datas.status && datas.status==="false") {
                reject(datas.error?datas.error:"后端数据处理异常");
            }
            else {
                resolve(datas);
            }
        }).catch(function(ex) {
            reject("后端AJAX异常:",ex);
            alert("ex:",ex);
            //console.log('parsing failed', ex)
        });
    });
}

export const doModeDataGet = (params) => {
    return new Promise((resolve,reject) => {
        fetch(server+"/api/ec/mode/search/datas",getFetchParams("POST",params)).then(function(response) {
            const datas = response.json();
            if(datas && datas.status && datas.status==="false") {
                reject(datas.error?datas.error:"后端数据处理异常");
            }
            else {
                resolve(datas);
            }
        }).catch(function(ex) {
            reject("后端AJAX异常:",ex);
            alert("ex:",ex);
            //console.log('parsing failed', ex)
        });
    });
}