import WeaInput4Base from '../wea-input4-base'

class WeaInput4Node extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base
				title="节点"
				topPrefix="wt"
				prefix="node"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Node"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props}
			/>
		)
	}
}

export default WeaInput4Node;


/*

const WeaInput4Com = React.createClass({
	render() {
		return  <WeaInput4Base
				title="节点"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="wt"
				prefix="node"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Node"]}
				isMult={this.props.isMult}
				style={this.props.style}
				value={this.props.value}
				valueName={this.props.valueName}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				ifAsync={true}
				ifCheck={true}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

*/