import {DatePicker,Form} from 'antd'
// import DatePicker from '../../_antd1.11.2/date-picker'
// import Form from '../../_antd1.11.2/form'
const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
function DateFormat(date,fmt)
{ //author: meizz
  var o = {
    "M+" : date.getMonth()+1,                 //月份
    "d+" : date.getDate(),                  //日
    "h+" : date.getHours(),                   //小时
    "m+" : date.getMinutes(),                 //分
    "s+" : date.getSeconds(),                 //秒
    "q+" : Math.floor((date.getMonth()+3)/3), //季度
    "S"  : date.getMilliseconds()             //毫秒
  };
  if(/(y+)/.test(fmt))
    fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)
    if(new RegExp("("+ k +")").test(fmt))
  fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
  return fmt;
}

const WeaDateRange = React.createClass({
	getInitialState() {
		return {
			startDt:"",
			endDt:""
		}
	},
	render() {
		var startDt = this.state.startDt;
		var endDt = this.state.endDt;
		//console.log("render d1:"+startDt);
		//console.log("render d2:"+endDt);
		return <FormItem label={this.props.label}>
				<RangePicker style={{width: 184}} format="yyyy-MM-dd" onChange={this.change} />
				<input key="0" type="hidden" name={this.props.name+'Start'} value={startDt} />
				<input key="1" type="hidden" name={this.props.name+'End'} value={endDt} />
		       </FormItem>;
	},
	change(value) {
		const d1 = DateFormat(value[0],"yyyy-MM-dd");
		const d2 = DateFormat(value[1],"yyyy-MM-dd");
		//console.log("d1:"+d1);
		//console.log("d2:"+d2);
		this.setState({
			startDt:d1,
			endDt:d2,
		});
	}
});

export default WeaDateRange;