import {DatePicker} from 'antd'
// import DatePicker from '../../_antd1.11.2/date-picker'
const MonthPicker = DatePicker.MonthPicker;
import DateTimeTool from '../../_util/datetime'

const Main = React.createClass({
	getInitialState() {
    	return {
    		value:this.props.defaultValue
    	};
	},
	render() {
		return (
			<div>
			<MonthPicker style={{"width": this.props.width}} onChange={this.onChange} defaultValue={this.props.defaultValue} />
			<input type="hidden" name={this.props.name} value={this.state.value} />
			</div>
		)
	},
	onChange(v) {
		//console.log(DateTimeTool.format(v,"yyyy-MM-dd"));
		let formatStr = "yyyy-MM";
		this.setState({
			value:DateTimeTool.format(v,formatStr)
		});
		const { onChange } = this.props;
		typeof onChange === 'function' && onChange(DateTimeTool.format(v,formatStr))
	}
});

export default Main;