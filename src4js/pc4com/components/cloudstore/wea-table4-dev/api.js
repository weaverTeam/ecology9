const getFd = (values) => {
    let fd = "";
    //var data = new FormData();
    for(let p in values) {
        fd += p+"="+encodeURIComponent(values[p])+"&";
        //fd += p+"="+values[p]+"&";
        //data.append(p,values[p]);
    }
    if(fd!="") {
        fd = fd.substring(0,fd.length-1);
    }
    return fd;
}
const server = "";

const getFetchParams = (method,params)=>{
    let obj = {
        method:method,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
            'X-Requested-With':'XMLHttpRequest'
        },
    };
    if(server=="") {
        obj.credentials = "include";
    }
    // else {
    //     obj.mode = "no-cors";
    // }
    if(params) {
        obj.body = getFd(params);
    }
    //console.log("obj:",obj);
    return obj;
}

export const doExport = (params) => {
    return new Promise((resolve,reject) => {
        fetch(server+"/api/ec/dev/search/export",getFetchParams("POST",params))
        .then((response)=>{
            return response.json();
        })
        .then((data)=>{
            if(data && data.status && data.status==="false") {
                reject(data.error?data.error:"后端数据处理异常");
            }
            else {
                resolve(data);
            }
        })
        .catch(function(ex) {
            reject("后端AJAX异常:",ex);
            alert("ex:",ex);
        });
    });
}

export const doCountGet = (params) => {
    return new Promise((resolve,reject) => {
        fetch(server+"/api/ec/dev/search/count",getFetchParams("POST",params))
        .then((response)=>{
            return response.json();
        })
        .then((data)=>{
            if(data && data.status && data.status==="false") {
                reject(data.error?data.error:"后端数据处理异常");
            }
            else {
                resolve(data);
            }
        })
        .catch(function(ex) {
            reject("后端AJAX异常:",ex);
            alert("ex:",ex);
        });
    });
}

export const doDatasGet = (params) => {
    return new Promise((resolve,reject) => {
        //console.log("doDatasGet in:",params);
        fetch(server+"/api/ec/dev/search/datas",getFetchParams("POST",params))
        .then((response)=>{
            return response.json();
        })
        .then((data)=>{
            if(data && data.status && data.status==="false") {
                reject(data.error?data.error:"后端数据处理异常");
            }
            else {
                resolve(data);
            }
        })
        .catch(function(ex) {
            reject("后端AJAX异常:",ex);
            alert("ex:",ex);
        });
    });
}

export const getLs = (name)=>{
    const ls = window.localStorage;
    const tableObj = ls.tableObj?JSON.parse(ls.tableObj):null;
    //console.log("tableObj:",tableObj);
    return tableObj?tableObj[name]:"";
}

export const setLs = (name,value)=>{
    let ls = window.localStorage;
    let tableObj = {};
    if(ls.tableObj) {
        tableObj = JSON.parse(ls.tableObj);
    }
    tableObj[name] = value;
    ls.tableObj = JSON.stringify(tableObj);
}

export const formatNumber = (num,cent,isThousand) => { 
    num = num.toString().replace(/\$|\,/g,''); 
    if(isNaN(num))//检查传入数值为数值类型. 
        num = "0"; 
    if(isNaN(cent))//确保传入小数位为数值型数值. 
        cent = 0; 
    cent = parseInt(cent); 
    cent = Math.abs(cent);//求出小数位数,确保为正整数. 
    if(isNaN(isThousand))//确保传入是否需要千分位为数值类型. 
        isThousand = 0; 
    isThousand = parseInt(isThousand); 
    if(isThousand < 0) 
        isThousand = 0; 
    if(isThousand >=1) //确保传入的数值只为0或1 
        isThousand = 1; 
    sign = (num == (num = Math.abs(num)));//获取符号(正/负数) 
    //Math.floor:返回小于等于其数值参数的最大整数 
    num = Math.floor(num*Math.pow(10,cent)+0.50000000001);//把指定的小数位先转换成整数.多余的小数位四舍五入. 
    cents = num%Math.pow(10,cent); //求出小数位数值. 
    num = Math.floor(num/Math.pow(10,cent)).toString();//求出整数位数值. 
    cents = cents.toString();//把小数位转换成字符串,以便求小数位长度. 
    while(cents.length<cent){//补足小数位到指定的位数. 
        cents = "0" + cents; 
    } 
    if(isThousand == 0) //不需要千分位符. 
        return (((sign)?'':'-') + num + '.' + cents); 
    //对整数部分进行千分位格式化. 
    for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) 
        num = num.substring(0,num.length-(4*i+3))+','+ 
    num.substring(num.length-(4*i+3)); 
    return (((sign)?'':'-') + num + '.' + cents);
} 