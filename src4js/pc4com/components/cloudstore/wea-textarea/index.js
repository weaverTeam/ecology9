import {Input,Badge} from 'antd';
// import Input from '../../_antd1.11.2/input'
// import Badge from '../../_antd1.11.2/badge'

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value:props.value?props.value:"",
			showBadge:false
		};
	}
	componentWillReceiveProps(nextProps) {
		if(this.props.value!==nextProps.value) {
			//onsole.log("this.props.value:",this.props.defaultValue," nextProps.value:",nextProps.value);
			this.setState({
				value:nextProps.value
			});
			//console.log("this.props.value:",this.props.defaultValue);
			//console.log("nextProps.value:",nextProps.defaultValue);
			/*
			this.setState({
				ids:nextProps.value,
				names:nextProps.valueSpan
			});
			*/
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		//if(nextProps.defaultValue !== this.props.defaultValue) {
			// console.log("nextProps.value:",nextProps.value);
			// console.log("this.props.value:",this.props.value);
			// console.log("nextState.value:",nextState.value);
			// console.log("this.state.value:",this.state.value);
		//}
		return nextProps.value !== this.props.value || nextState.showBadge!==this.state.showBadge || nextState.value !== this.state.value;
	}
	render() {
		const {value,showBadge} = this.state;
		let {et} = this.props;
		et = et?et:0;
		//console.log("showBadge:",showBadge);
		//value={value}

		let theProps = this.props;
		//if(value!="")
		theProps.value = value;

		//console.log("props:",theProps);
		//console.log("render value:",value);
		const inputArr = (<Input type="textarea" {...theProps} onChange={this.setText.bind(this)} onBlur={this.hideBadge.bind(this)} onFocus={this.showBadge.bind(this)} />);
		if(et==2 && (value=="" || showBadge)) {
			return (
				<Badge dot>{inputArr}</Badge>
			)
		}
		else {
			return inputArr
		}
		/*
		let {et,defaultValue} = this.props;
		et = et?et:0;
		defaultValue = defaultValue?defaultValue:"";
		let inputArr = (<div><Input type="text" {...this.props} /></div>)
		if(et==2 && defaultValue=="") {
			inputArr = (<Badge dot>{inputArr}</Badge>)
		}
		return inputArr
		*/
	}
	setText(e) {
		//console.log("setText:[",e.target.value,"]");
		//console.log("typeof(this.props.onChange):",typeof(this.props.onChange));
		//onChange={this.setText.bind(this)}
		this.setState({
			value:e.target.value
		});
		//.replace(/^\s+|\s+$/g,'')
		//.replace(/(^\s+)|(\s+$)/g,"")
		if(typeof(this.props.onChange)=="function") {
			//this.props.onChange(e);
			this.props.onChange(e);
		}
	}
	hideBadge(e) {
		//console.log("e:",e);
		this.setState({
			showBadge:false
		});
		//console.log("blur value:",e.target.value);
		if(typeof(this.props.onBlur)=="function") {
			this.props.onBlur(e);
		}
	}
	showBadge(e) {
		this.setState({
			showBadge:true
		});
		if(typeof(this.props.onFocus)=="function") {
			this.props.onFocus(e);
		}
	}
}

export default Main;

/*

const WeaTextarea = React.createClass({
	render() {
		if(!this.props.NoFormItem) {
			return <FormItem label={this.props.label} >
			   <Input type="textarea" rows="5" name={this.props.name} style={{"width": "182px"}} defaultValue="" onChange={this.props.onChange} />
			   </FormItem>;
		}
		else {
			return <Input type="textarea" rows="5" name={this.props.name} style={{"width": "100%"}} defaultValue="" onChange={this.props.onChange} />;
		}
	}
});

*/