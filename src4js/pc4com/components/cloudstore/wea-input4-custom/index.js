import WeaInput4Base from '../wea-input4-base'

class WeaInput4Custom extends React.Component {
	constructor(props) {
		super(props);
	}
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return (nextProps.value !== this.props.value);
	}
	// componentDidMount() {
	// 	let that = this;
 //    	const {ht,t,dt} = this.props;
 //    	this.props.checkBrowerData(ht,t,dt).then(function(datas) {
 //    		//console.log(datas);
 //    		//Storage.setBrowerDatas(ht+"_"+t+"_"+fieldId,datas);
 //  			//console.log("inputType:",datas);
 //    		that.setState({
 //    			inputType:datas
 //    		});
 //        });

	// 		const {ht,t,dt,listUrl,countUrl,datasType} = this.props;
	// 		tabPaneArr[tabPaneArr.length] = (
	// 			<TabPane tab="按列表" key="list">
	// 				<WeaList
	// 				listUrl={listUrl}
	// 				countUrl={countUrl}
	// 				fieldhtmltype={ht}
	// 				fielddbtype={dt}
	// 				type={t}
	// 				isMult={isMult}
	// 				setData={this.setData.bind(this)}
	// 				ids={ids}
	// 				names={names}
	// 				datasType={datasType}
	// 				 />
	// 			</TabPane>
	// 		)


	// }
	render() {
		//console.log("WeaInput4Custom:",this.props);
		const {inputType} = this.props;
		if("tree"==inputType) {
			const {ht,t,dt} = this.props;
			return (
				<WeaInput4Base
					{...this.props}
					title="自定义"
					topPrefix="custom"
					prefix="custom"
					topIconUrl="/cloudstore/images/icon/treeTop.gif"
					dataUrl="/cloudstore/system/ControlServlet.jsp"
					dataKey="id"
					otherPara={["action","Action_GetBrowerDataList","fielddbtype",dt,"fieldhtmltype",ht,"type",t]}
					ifAsync={true}
					ifCheck={true}
				/>
			)
			//fielddbtype:dt,fieldhtmltype:i,type:j
		}
		else if("list"==inputType) {
			return (
				<WeaInput4Base
					{...this.props}
					title="自定义"
					browerType={['list']}
					paramsUrl="/cloudstore/system/ControlServlet.jsp?action=Action_ReturnName"
					listUrl="/cloudstore/system/ControlServlet.jsp?action=Action_GetBrowerDataList"
					countUrl="/cloudstore/system/ControlServlet.jsp?action=Action_GetBrowerDataCount"
					modalWidth="800px"
					datasType="custom"
				/>
			)
		}
		else {
			return (<div></div>)
		}
	}
}

export default WeaInput4Custom;