import {Badge,DatePicker} from 'antd'
// import Badge from '../../_antd1.11.2/badge'
// import DatePicker from '../../_antd1.11.2/date-picker'
import DateTimeTool from '../../_util/datetime'

class WeaDatePicker extends React.Component {
	constructor(props) {
		super(props);
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value;
	}
	render() {
		let props = this.props;
		let {et,value} = this.props;
		et = et?et:0;
		if(et==2 && value=="") {
			return (
				<Badge dot>
					<DatePicker {...props} onChange={this.changeDate.bind(this)} />
				</Badge>
			)
		}
		return (
			<DatePicker {...props} onChange={this.changeDate.bind(this)} />
		)
	}
	changeDate(v,v2) {
		// console.log('v',v,v2);
		let dateValue = '';
		if(!!v){
			dateValue = DateTimeTool.format(v,"yyyy-MM-dd");
		}
		// console.log('dateValue',dateValue);
		this.props.onChange(dateValue,dateValue);
	}
}

export default WeaDatePicker;