import {Row,Col,Icon} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Icon from '../../_antd1.11.2/icon'

class SearchGroup extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            showGroup: props.showGroup ? props.showGroup : false
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.showGroup !== nextProps.showGroup) {
            this.setState({showGroup: nextProps.showGroup});
        }
    }
	render() {
        const {title,items,needTigger} = this.props;
        const {showGroup} = this.state;
        //console.log("items:",items);
        const cellNum = 2;
        return (
            <div className="wea-search-group">
                <Row className="wea-title">
                    <Col span="20">
                        <div>{title}</div>
                    </Col>
                    {needTigger &&
	                    <Col span="4" style={{textAlign:"right",paddingRight:10,fontSize:12}}>
	                        <Icon type={showGroup ? 'up' : 'down'} onClick={()=>this.setState({showGroup:!showGroup})}/>
	                    </Col>
                    }
                </Row>
                <Row className="wea-content" style={showGroup ? {} : {height:0}}>
                    {
                        items.map((obj)=>{
                            return (
                                <Col className="wea-form-cell" span={24/cellNum}>
                                    {obj.com}
                                </Col>
                            )
                        })
                    }
                </Row>
            </div>
        )
    }

}

export default SearchGroup