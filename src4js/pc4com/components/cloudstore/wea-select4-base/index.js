import WeaSelect from '../wea-select';

class WeaSelect4Base extends React.Component {
	constructor(props) { 
        super(props);
    }
    shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		/*
		if(nextProps.defaultValue !== this.props.defaultValue || this.state.datas.length===0) {
			console.log("nextProps.defaultValue:",nextProps.defaultValue);
			console.log("this.props.defaultValue:",this.props.defaultValue);
			console.log("nextProps.datas:",nextProps.datas);
			console.log("this.props.datas:",this.props.datas);
		}
		*/
		return (nextProps.defaultValue !== this.props.defaultValue);
	}
	// let {defaultValue} = this.state;
 //    	let {datas} = this.props;
 //    	let defaultValueSpan = "";
	// 	defaultValue = !defaultValue?"":defaultValue;
 //    	datas = JSON.parse(datas);
    	
 //    	//console.log("datas:",datas);
 //    	//console.log(datas);
 //    	//Storage.setBrowerDatas(ht+"_"+t+"_"+fieldId,datas);
	// 	for(let i=0;i<datas.length;i++) {
	// 		datas[i].name = datas[i].selectname;
	// 		datas[i].value = datas[i].selectvalue;
	// 		//console.log("datas[i].selectvalue:",datas[i].selectvalue);
	// 		//console.log("datas[i].selectname:",datas[i].selectname);
	// 		//console.log("datas["+i+"].isdefault:",datas[i].isdefault);
	// 		if(datas[i].isdefault=="y") {
	// 			defaultValue = datas[i].selectvalue;
	// 			defaultValueSpan = datas[i].selectname;
	// 		}
	// 	}
	// 	//console.log("sel datas:",datas);
	// 	//console.log("defaultValue:",defaultValue);
	// 	this.setState({
	// 		datas:datas,
	// 		defaultValue:defaultValue
	// 	});
	// 	if(defaultValue!==""&&defaultValueSpan!=="")
	// 		this.props.onChange(defaultValue,defaultValueSpan);
	render() {
		//let {defaultValue} = this.state;
		let {defaultValue,datas} = this.props;
		let props = this.props;
		datas = JSON.parse(datas);
		let find = false;
		for(let i=0;i<datas.length;i++) {
			if(datas[i].selectname === '' || datas[i].selectvalue === '') find = true;
			datas[i].name = datas[i].selectname;
			datas[i].value = datas[i].selectvalue;
			if(datas[i].isdefault=="y"&&defaultValue=="") {
				defaultValue = datas[i].selectvalue;
				//defaultValueSpan = datas[i].selectname;
			}
		}
		!find && datas.unshift({name:"",value:""});
		defaultValue = defaultValue||defaultValue==0?defaultValue.toString():"";
		//console.log("defaultValue:",defaultValue);
		//defaultValue = defaultValue || "";
		//console.log("defaultValue:",defaultValue);
		//console.log("defaultValue:",defaultValue," datas:",datas);
		return (
			<WeaSelect 
				actionUrl="/cloudStore/system/ControlServlet.jsp?action=Action_GetBrowerDataList"
				{...props}
				value = {defaultValue}
				datas = {datas}
				onChange={this.changeValue.bind(this)}
			/>
		)
	}
	changeValue(value,label) {
		//console.log("changeValue",value,label);
		this.setState({
			defaultValue:value
		});
		this.props.onChange(value,label);
	}
}

export default WeaSelect4Base;

//1、web调用，过渡期
//2、nodejs调用