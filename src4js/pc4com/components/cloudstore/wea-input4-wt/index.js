import WeaInput4Base from '../wea-input4-base'

class WeaInput4Wt extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		//console.log("WeaInput4Wf props:",this.props);
		return (
			<WeaInput4Base 
				title="流程类型"
				topPrefix="wt"
				prefix="wt"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Wt"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props} 
			/>
		)
	}
}

export default WeaInput4Wt;

/*

const WeaInput4Com = React.createClass({
	render() {
		return  <WeaInput4Base 
				title="流程类型"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="wt"
				prefix="wt"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Wt"]}
				isMult={this.props.isMult}
				style={this.props.style}
				value={this.props.value}
				valueName={this.props.valueName}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				ifAsync={true}
				ifCheck={true}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

export default WeaInput4Com;

*/