import {Input,Badge} from 'antd'
// import Input from '../../_antd1.11.2/input'
// import Badge from '../../_antd1.11.2/badge'
import cloneDeep from 'lodash/cloneDeep'

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value:props.value?props.value:"",
			showBadge:false
		};
	}
	componentWillReceiveProps(nextProps) {
		if(this.props.value!==nextProps.value) {
			this.setState({
				value:nextProps.value
			});
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value || nextState.showBadge!==this.state.showBadge || nextState.value !== this.state.value;
	}
	render() {
		const {value,showBadge} = this.state;
		let {et} = this.props;
		et = et?et:0;
		let theProps = cloneDeep(this.props);
		theProps.value = value;
		const inputArr = (<Input type="text" {...theProps} onChange={this.setText.bind(this)} onBlur={this.hideBadge.bind(this)} onFocus={this.showBadge.bind(this)} />);
		if(et==2 && (value=="" || showBadge)) {
			return (
				<Badge dot>{inputArr}</Badge>
			)
		}
		else {
			return inputArr
		}
	}
	setText(e) {
		this.setState({
			value:e.target.value
		});
		if(typeof(this.props.onChange)=="function") {
			this.props.onChange(e);
		}
	}
	hideBadge(e) {
		//console.log("e:",e);
		this.setState({
			showBadge:false
		});

		//console.log("blur value:",e.target.value);
		if(typeof(this.props.onBlur)=="function") {
			this.props.onBlur(e);
		}
	}
	showBadge(e) {
		this.setState({
			showBadge:true
		});
		if(typeof(this.props.onFocus)=="function") {
			this.props.onFocus(e);
		}
	}
}

export default Main;