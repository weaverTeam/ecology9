import { Select,Badge } from 'antd';
// import Select from '../../_antd1.11.2/select'
// import Badge from '../../_antd1.11.2/badge'
const Option = Select.Option;

class WeaSelect extends React.Component {
	render() {
		const {datas,name,id} = this.props;
		let {et,value,defaultValue} = this.props;

		et = et?et:0;
		value = value?value:"";
		//console.log("value?:",value);
		defaultValue = defaultValue?defaultValue:"";
//		console.log("value:",value);
//		console.log("defaultValue:",defaultValue);
		//console.log("sel datas:",datas);
		//<Option value="">请选择</Option>
		//console.log("style1:",this.props.style);
		let inputArr = (
			<div style={this.props.style}>
			<Select {...this.props} onChange={this.changeSelect.bind(this)}>
			{
				datas.map(function(data,index) {
					return (
						<Option key={index} value={data.value}>
							{data.name}
						</Option>
					)
				})
			}
			</Select>
			<input ref="inpVal" type="hidden" id={id} name={name} value={value} />
			</div>
		)
		//console.log("sel value:",value," =",value==="");
		if(et==2 && value=="") {
			inputArr = (<Badge dot>{inputArr}</Badge>)
		}
		return inputArr
	}
	changeSelect(value) {
		//console.log("changeSelect:",a,b);
		let valueSpan = "";
		//console.log("value:",value);
		let valueArr = value;
		if(typeof value === "string") {
			valueArr = value.split(",");
		}
		else {
			value = value.join(",");
		}
		const {datas} = this.props;
		for(let i=0;i<datas.length;i++) {
			const data = datas[i];
			for(let j=0;j<valueArr.length;j++) {
				if(data.value===valueArr[j]) {
					valueSpan += data.name+",";
				}
			}
		}
		this.refs.inpVal.value = value;
		valueSpan = valueSpan!=""?valueSpan.substring(0,valueSpan.length-1):valueSpan;
		this.props.onChange(value,valueSpan);
	}
}

export default WeaSelect;

/*



componentDidMount() {
		let that = this;
		const {actionUrl,fieldId} = this.props;
		const params = "fieldId="+fieldId;
		doDatasGet(actionUrl,params).then(function(datas) {
			that.setState({
				datas:datas
			});
		});
	}

{


<Select size="large" defaultValue="0">
      <Option value="jack">Jack</Option>
      <Option value="0">Lucy</Option>
      <Option value="disabled" disabled>Disabled</Option>
      <Option value="yiminghe">yiminghe</Option>
    </Select>


const WeaSelect = React.createClass({
	render() {
		let defaultValue = "";
		let value = "";
		const datas = this.props.datas;
		const opArr = new Array();
		let i=0;
		let j=0;
		while(i<datas.length) {
			if(datas[i].value==this.props.defaultValue)
				defaultValue = datas[i].name;
			if(datas[i].value==this.props.value)
				value = datas[i].name;
			opArr[j++] = <Option value={datas[i].value}>{datas[i].name}</Option>
			i++;
		}
		if(this.props.NoFormItem||!this.props.label||this.props.label=="") {
			return <div>
					<Select disabled={this.props.disabled} value={value} defaultValue={defaultValue} style={this.props.style} onChange={this.props.onChange} size={this.props.size}>
				    {opArr}
				    </Select>
				    <input type="hidden" name={this.props.name} value={this.props.value} />
					</div>
		}
		return <FormItem label={this.props.label} labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol} required={this.props.required} >
				   <Select disabled={this.props.disabled} value={value} defaultValue={defaultValue} style={this.props.style} onChange={this.props.onChange} size={this.props.size}>
				   {opArr}
				   </Select>
				   <input type="hidden" name={this.props.name} value={this.props.value} />
			   </FormItem>
	}
});


export default WeaSelect;


*/