import {Upload, message, Button, Icon, Input, Modal} from 'antd'
// import Upload from '../../_antd1.11.2/upload'
// import message from '../../_antd1.11.2/message'
// import Button from '../../_antd1.11.2/button'
// import Icon from '../../_antd1.11.2/icon'
// import Input from '../../_antd1.11.2/input'
// import Modal from '../../_antd1.11.2/modal'
let uploadFileName = [];
let uploadFileSize = [];
let uploadFileUid = [];
let alreadyUploadSize=0;
class Main extends React.Component {
	  constructor(props) {
        super(props);
        const {acceptFileType,isEditorBox,UploadedFileSizeVisib,visibleType,url,alreadyUploadFile,removeCallBack,successCallBack,file_count,file_size,file_sizeSum,isShowUploadList} = props;
     		alreadyUploadSize = this.alreadyUploadSize();
        this.state = {
					fileList: alreadyUploadFile ? alreadyUploadFile:[],
					defaultFileCount : file_count ?  file_count : 5,
					defaultFileSize :file_size ? file_size : 1024*1024*5*1000,
					priviewVisible: false,
    			priviewImage: '',
    			visible: false,
        }
    }
	  componentWillReceiveProps(nextProps) {
			const props = {};
		    if ('defaultFileCount' in nextProps) {
		      props.defaultFileCount = nextProps.defaultFileCount;
		    }
		    if ('defaultFileSize' in nextProps) {
		      props.defaultFileSize = nextProps.defaultFileSize;
		    }
		    this.setState(props);
		}
		alreadyUploadSize(){
			let size = 0;
			let uploadFile = !!this.props.alreadyUploadFile ? this.props.alreadyUploadFile : false;
			if(uploadFile){
				for(let i = 0;i< uploadFile.length;i++){
					size += uploadFile[i].size;
				}
				return size;
			}
			else{return []}
		}
		componentDidMount(){
	  		//点击转发传过来的已上传的文件数据 eg:[{uid: -1,"response":"001",size:1024*1024*2,name:"001"}]
		  	const alreadyUploadFileSize = this.props.alreadyUploadFile;
		  	//console.log("this.props.alreadyUploadFile",this.props.alreadyUploadFile,"alreadyUploadFileSize",alreadyUploadFileSize)
	  		if(!!alreadyUploadFileSize){
	  			for(let i = 0; i< alreadyUploadFileSize.length; i++){
					if(typeof alreadyUploadFileSize[i].uid === "number"){
						//$(".ant-upload-list-item-name")[i].innerText = alreadyUploadFileSize[i].name;
						//$(".ant-upload-list-item-name")[i].innerText = alreadyUploadFileSize[i].name+"  ("+Math.floor(alreadyUploadFileSize[i].size/1024).toFixed(2)+"KB)";
					}
				}
	  		}
		}
	  componentDidUpdate(){
	  	let {fileList,defaultFileCount,defaultFileSize} = this.state;
	  	if($(".ant-upload-list-item-name")){
				for(let i=0;i<fileList.length;i++){
					//fileList[i].name = fileList[i].name+"  ("+Math.pow(fileList[i].size/1024).toFixed(2)+"KB)";
					//console.log("fileList[i].name",fileList[i].name)
				}
			}
	  }

	  uploadedFileSize(){
	  	let allSize = 0;
	  	this.state.fileList.map((item)=>{
	  		 allSize += item.size;
	  	});
	  	return (allSize/Math.pow(1024,2)).toFixed(2) + "MB";
	  }
	 	showModal() {
    	this.setState({
      	visible: true,
   		});
  	}
	  handleOk() {
	    let fileList = this.state.fileList;
			fileList.splice(0,fileList.length);
	    this.setState({
	      visible: false,
	    });
			this.props.removeAllCallBack();
	  }
	  handleCancel(e) {
	    this.setState({
	      visible: false,
	    });
	  }
	  render() {
     	let that = this;
			let {fileList,defaultFileCount,defaultFileSize} = that.state;
			const {acceptFileType,isEditorBox,UploadedFileSizeVisib,visibleType,isShowUploadList,url} = that.props;
	   	//console.log("filsList.length",fileList.length);
      const props = {
          name: 'Filedata',
          action: url,
          accept:acceptFileType,
          showUploadList:isShowUploadList,
          multiple: true,
          listType: 'text',//or picture
          defaultFileList:!!that.props.alreadyUploadFile?that.props.alreadyUploadFile : [],
					onPreview: (file) => {
			   			//console.log("预览",file);
			     		that.setState({
				            priviewImage: file.thumbUrl,
				            priviewVisible: true,
				      });
					},
	     		onRemove(file) {
		      	//console.log("file",file);
		      	let fileList = that.state.fileList;
		      	for (let i =0;i< fileList.length;i++) {
	      			if(file.uid === fileList[i].uid){
	      				fileList.splice(i,1);
	      			}
	      		}
						that.setState({ fileList });
						that.props.removeCallBack(file.response);

	      	},
		      onChange(info) {
		      	//console.log("info",info.file);
		      	let file = info.file;
		      	let fileList = info.fileList;
		      	 //最多上传文件个数
		        if(fileList.length > defaultFileCount ) {
							 return false;
						}
		        if (info.file.status === 'uploading') {
		          //console.log(info.file, info.fileList);
		        }
		        if (info.file.status === 'done') {
		          //console.log("info.filelist:",info.fileList);
		          //console.log(file);
						  that.props.successCallBack(file.response);//上传成功的回调
		          message.success(`${info.file.name} 上传成功。`);
		        } else if (info.file.status === 'error') {
		          message.error(`${info.file.name} 上传失败。`);
		        }
						that.setState({ fileList });
						//console.log(that.state.fileList.length);
			    },
				  beforeUpload(file) {
				  	//console.log("fiel",file);
						let {defaultFileSize,defaultFileCount,alreadyUploadFileLength,fileList} = that.state;
						//console.log("fileList.leng",that.state.fileList.length)
						if(fileList.length >= defaultFileCount ) {
							 message.error('最多只能上传'+ defaultFileCount +'个文件哦');
							 return false;
						}
					//单个文件限制
				  	let fileSize = file.size <= defaultFileSize;
				    if (!fileSize) {
				      message.error('只能上传小于'+(defaultFileSize/Math.pow(1024,2)).toFixed(2)+'M的文件哦');
				      return false;
				    }

				  //总的文件大小限制
				    file.size += file.size;
				    //console.log(file.size);
						if(file.size > defaultFileCount * defaultFileSize){
							message.error('文件上传总量最多'+(defaultFileCount * defaultFileSize/Math.pow(1024,2)).toFixed(2)+'M哦');
							return false;
						}
					//也可在此限制上传文件类型
			  }
  		}
			return (
				<div className="wea-email-upLoad" >
				{fileList.length !=0 &&  (<span className="uploaded-filefize"
				  	style={that.props.UploadedFileSizeVisib ? {"display":"inline"} :  {"display":"none"}}>
				  		已上传文件的大小:{that.uploadedFileSize()}
				  </span>)
				}
				  <Upload {...props} fileList={that.state.fileList} >
				  	 {that.props.visibleType ? (<Button type="ghost" ><span className="anticon anticon-upload"></span>	选取多个文件</Button>)
				  	 : (<span><Icon type="paper-clip" /> <span style={isEditorBox?{"display":"none"}:{"display":"inline"}}>附件</span>{!isEditorBox && that.props.file_size/Math.pow(1024,2).toFixed(2)+"M"}</span>)}
				  </Upload>
					{that.props.visibleType && !that.props.UploadedFileSizeVisib && (<Button type="ghost"  disabled={fileList.length === 0}  onClick={that.showModal.bind(that)} > X 清除所有选项</Button>)}

				 	<Modal
				 		title="信息确认对话框"
				 		wrapClassName="vertical-center-modal"
					 	visible={this.state.visible}
					 	style={{ top: 200 }}
	          onOk={that.handleOk.bind(that)}
	          onCancel={that.handleCancel.bind(that)}
        	>
						<Icon type="question-circle-o" /> <span>确定清除所有选择 ?</span>
        	</Modal>


			  </div>
			)
	}
}

export default Main;

/*
				  <span style={{"display":"none"}}> 最大{that.props.file_size/Math.pow(1024,2).toFixed(2)} M/个</span>
  	<Modal className="wea-email-upload-modal"
				  visible={that.state.priviewVisible} footer={null} onCancel={that.handleCancel2.bind(that)}>
		  			<img alt="img" src={that.state.priviewImage} />
		 		  </Modal>
 */