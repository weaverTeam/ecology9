import {Menu, Icon} from 'antd'
// import Menu from '../../_antd1.11.2/menu';
// import Icon from '../../_antd1.11.2/icon';
const SubMenu = Menu.SubMenu;
const ItemMenu = Menu.Item;
import WeaScroll from '../wea-scroll';
import cloneDeep from 'lodash/cloneDeep';

class WeaMenuVertical extends React.Component {
    constructor(props) {
		super(props);
        this.state = {
            mode:props.mode || "vertical",
            datas:props.datas || [],
            inlineWidth: props.inlineWidth || 197,
            verticalWidth: props.verticalWidth || 61,
            selectedKey:props.defaultSelectedKey || "", //包含SubMenu，只做单选
            defaultSelectedKey:props.defaultSelectedKey || "", //包含SubMenu，只做单选
            canClick: true
        }
    }
    shouldComponentUpdate(nextProps) {
        //  if(JSON.stringify(this.props.datas) !== JSON.stringify(nextProps.datas)  || (this.props.defaultSelectedKey !== nextProps.defaultSelectedKey&&this.state.defaultSelectedKey !== nextProps.defaultSelectedKey)) {
        //      return true;
        //  }
         return true;
    }
    componentWillReceiveProps(nextProps) {
        //console.log("nextProps:",nextProps);
         if(JSON.stringify(this.props.datas) !== JSON.stringify(nextProps.datas)  || (this.props.defaultSelectedKey !== nextProps.defaultSelectedKey&&this.state.defaultSelectedKey !== nextProps.defaultSelectedKey)) {
            let key = (nextProps.defaultSelectedKey && nextProps.defaultSelectedKey.indexOf("verTop_")===0) ? nextProps.defaultSelectedKey.substring(7) : nextProps.defaultSelectedKey;
            this.setState({
                datas:nextProps.datas || [],
                defaultSelectedKey:nextProps.defaultSelectedKey || "",
                selectedKey: key || ""
            });
        }
    }
    render() {
        const {mode,datas,verticalWidth} = this.state;
        const {selectedKey,defaultSelectedKey} = this.state;
        let style = {width:verticalWidth};
        let that = this;
        return (
                <Menu
                    openAnimation={()=>{}}
                    inlineIndent={14}
                    mode={mode}
                    selectedKeys={[]}
                    // onOpen={(e)=>{that.changeOpen(e.openKeys)}}
                    // onClose={(e)=>{that.changeOpen(e.openKeys)}}
                    onClick={(e)=>{
                                that.changeSelectedKey(e.key,that.getDataByKey(e.key,datas),0)
                            }
                    }
                    >
                    {this.renderMenuItem(datas,1)}
                </Menu>
        )
    }
//          <div className="wea-menu wea-menu-vertical" style={style}>
//              <div className="wea-menu-switch" onClick={this.onModeChange.bind(this)}><Icon type={mode == "vertical" ? "menu-unfold" : "menu-fold"} /></div>
//              <WeaScroll className="wea-scroll" typeClass="scrollbar-macosx" conClass="wea-menu-vertical" conHeightNum={40}>
//              </WeaScroll>
//          </div>
    onModeChange() {
        const {mode} = this.state;
        if(typeof this.props.onModeChange=="function") {
            this.props.onModeChange(mode);
        }
    }
    changeSelectedKey(key,data,type) {
    	const {canClick} = this.state;
        const {clickBlock} = this.props;
    	const that = this;
//  	let d = new Date();
//  	console.log(canClick,d.getTime());
    	if(canClick){
	        if(key && key.indexOf("verTop_")===0) {
	            key = key.substring(7);
	        }
	        this.setState({
	            canClick:false,
	            selectedKey:key
	        });
	        if(typeof this.props.onSelect=="function") {
	            this.props.onSelect(key,data,type);
	        }
	        setTimeout(()=>{that.setState({canClick:true})},!!clickBlock ? clickBlock : 300);
    	}
    }
    getDataByKey(key,datas) {
        let that = this;
        let obj = "";
        for(let i=0;i<datas.length&&obj=="";i++) {
            if(datas[i].id==key) {
                obj = cloneDeep(datas[i]);
            }
            if(obj==""&&datas[i].child&&datas[i].child.length>0) {
                obj = that.getDataByKey(key,datas[i].child);
            }
        }
        return obj;
    }
    renderMenuItem(datas,level) {
        const selectedClassName = "wea-menu-selected";
        let that = this;
        const {selectedKey} = this.state;
        let datasArr = new Array();
        for(let i=0;i<datas.length;i++) {
            const data = datas[i];
            const isSubMenu = data.child && data.child.length>0;
            const isSelected = selectedKey==data.id||"verTop_"+selectedKey==data.id;
            if(data.isVerticalHide !== 'true'){
	            if(isSubMenu) {
	                datasArr.push(
	                    <SubMenu
	                        selectedClassName={(isSelected?selectedClassName:"")+(level==1?" wea-menu-submenu-title":"")}
	                        key={data.id}
	                        ifFixed={true}
	                        onTitleClick={(e)=>{that.changeSelectedKey(e.key,data,0)}}
	                        title={that.renderTitle(data,level)}>
	                        {that.renderMenuItem(data.child,level+1)}
	                    </SubMenu>
	                )
	                if(level==2&&i==0&&!data.isVerticalHide) {
	                    datasArr.push(<Menu.Divider />);
	                }
	            }
	            else {
	                datasArr.push(
	                    <ItemMenu
	                        className={isSelected?selectedClassName:""}
	                        key={data.id}>
	                        {that.renderTitle(data,level)}
	                    </ItemMenu>
	                )
	            }
            }
        }
        return datasArr;
    }
    renderTitle(data,level) {
        const needImg = level==1;
        const showImg = needImg && data.icon && data.icon!="";
        const img = showImg?(<img style={{verticalAlign:"middle"}} align="middle" src={data.icon} alt={data.name} width="18px" height="18px" />):(
                    <i style={{fontSize:"18px",verticalAlign:"middle",margin:"auto"}} className={"wevicon wevicon-menu-default" + " " + "wevicon-menu-" + data.levelid} />
                )
        if(level>1) {
            return <span style={{verticalAlign:"middle"}}>{data.name}</span>
        }
        else {
            return needImg?img:""
        }
    }
}

//<Icon type="team" />

//<i style={{display:"inline",width:16,height:16}} className={"anticon anticon-menu-default" + " " + "anticon-menu-" + data.levelid} />
//<img style={{verticalAlign:"middle"}} align="middle" src={data.icon} alt={data.name} width="16px" height="16px" />

export default WeaMenuVertical