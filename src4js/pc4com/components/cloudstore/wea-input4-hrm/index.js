import WeaInput4Base from '../wea-input4-base'

class WeaInput4Hrm extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		//if(nextProps.value !== this.props.value) {
			//console.log("nextProps.value:",nextProps.value);
			//console.log("this.props.value:",this.props.value);
		//}
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base
				title="人力资源"
				topPrefix="gs"
				prefix="ry"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Hrm"]}
				ifAsync={true}
				ifCheck={true}
				singleGroup={true}
				selectUrl = "/data.jsp?type=1&f_weaver_belongto_userid=&f_weaver_belongto_usertype=null&bdf_wfid=76&bdf_fieldid=6420&bdf_viewtype=0"
				{...this.props}
			/>
		)
	}
}

export default WeaInput4Hrm;

/*

const WeaInput4Hrm = React.createClass({
	render() {
		return  <WeaInput4Base
				title="人力资源"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="gs"
				prefix="ry"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Hrm"]}
				isMult={this.props.isMult}
				ifAsync={true}
				ifCheck={true}
				style={this.props.style}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				value={this.props.value}
				valueName={this.props.valueName}
				NoFormItem={this.props.NoFormItem}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

export default WeaInput4Hrm;

*/