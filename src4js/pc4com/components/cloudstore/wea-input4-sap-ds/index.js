import WeaInput4Base from '../wea-input4-base'

class WeaInput4SapDs extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaInput4Base 
				title="SAP数据源"
				topPrefix="sapd"
				prefix="sapd"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/api/ec/tool/wf/releasediff/getSapTree"
				dataKey="id"
				otherPara={[]}
				ifAsync={true}
				ifCheck={true}
				{...this.props} 
			/>
		)
	}
}

export default WeaInput4SapDs;