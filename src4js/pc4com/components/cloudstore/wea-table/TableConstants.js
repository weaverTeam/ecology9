import keyMirror from 'keymirror';

export default keyMirror({
  	TABLE_SEARCH: null,
  	TABLE_GOPAGE: null,
  	TABLE_SEARCHBYKEY: null,
  	TABLE_SETOPTIONS: null,
  	TABLE_EXCELOUT: null,
});