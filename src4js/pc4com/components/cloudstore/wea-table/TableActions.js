import AppDispatcher from './TableDispatcher';
import TableConstants from './TableConstants';

const TableActions = {
	search(options) {
		AppDispatcher.dispatch({
	        actionType:TableConstants.TABLE_SEARCH,
	        options:options
	    });
	},
	excelOut() {
		AppDispatcher.dispatch({
	        actionType:TableConstants.TABLE_EXCELOUT
	    });
	},
	goPage(newPage) {
		AppDispatcher.dispatch({
	        actionType:TableConstants.TABLE_GOPAGE,
	    	newPage:newPage
	    });
	},
	searchByKey(key) {
		AppDispatcher.dispatch({
	        actionType:TableConstants.TABLE_SEARCHBYKEY,
	    	key:key
	    });
	},
	setOptions(options) {
		AppDispatcher.dispatch({
	        actionType:TableConstants.TABLE_SETOPTIONS,
	    	options:options
	    });
	}
}

export default TableActions;