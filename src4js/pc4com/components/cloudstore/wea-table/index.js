//import React from 'react';
import { Table,Row,Col,Input,Form } from 'antd';
// import Table from '../../_antd1.11.2/table'
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Input from '../../_antd1.11.2/input'
// import Form from '../../_antd1.11.2/form'
import TableStore from './TableStore';
import TableActions from './TableActions';
const InputGroup = Input.Group;
const FormItem = Form.Item;

const pagination = {
	total: 0,
	current: 1,
	showSizeChanger: false
};

class DataTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
			datas:[],
			pagination:pagination,
			haveAllData:false,
			loading:true,
			quickSearch:props.options.quickSearch
		}
		//console.log("options:",props.options);
		TableActions.setOptions(props.options);
		this.doRender = this.doRender.bind(this);
	}
	componentDidMount() {
	  	TableStore.addChangeListener(this.doRender); //监听表格变化
	}
	componentWillUnmount() {
	  	TableStore.removeChangeListener(this.doRender); //监听表格变化
	}
	doRender() {
		const page = TableStore.getPage();
		pagination.total = page.maxNum;
		pagination.current = page.nowPage;
		this.setState({
			datas:TableStore.getDatas(),
			pagination:pagination,
			haveAllData:TableStore.getHaveAllData(),
			loading:TableStore.getLoading(),
			quickSearch:TableStore.getQuickSearch(), //开启快速搜索就是本地模式
		})
	}
	render() {
		let quickSearch = this.state.quickSearch;
		let quickSearchCol = new Array();
		let datas = this.state.datas;
		for(let i=0;i<datas.length;i++) {
			datas[i].key = (i+1);
		}
		if(quickSearch&&!this.props.showForm&&!this.state.loading) {
			quickSearchCol[0] = <Input addonBefore="快捷查询" defaultValue="" id="validating" style={{"width":"300px"}} onChange={this.doSearchByKey.bind(this)} />;
		}
		return (<Row type="flex" justify="center">
			   	   <Col span="24" style={{"margin-top":"10px"}}>
			   	   {quickSearchCol}
			   	   </Col>
				   <Col span="24" style={{"margin-top":"10px"}}>
				   		<Table className="table"
				   			   size="middle"
				   			   loading={this.state.loading}
				   		       columns={this.props.options.columns}
				   		       expandedRowRender={this.props.expandedRowRender}
				   		       dataSource={datas}
				   		       pagination={this.state.pagination}
				   		       bordered={this.props.bordered}
				   		       onChange={this.doDataChange.bind(this)} />
				   </Col>
			   </Row>)

	}
	doSearchByKey(e) {
		//console.log(e.target.value);
		TableActions.searchByKey(e.target.value);
	}
	doDataChange(p,f,s) {
		//console.log("p:"+p.current+" f:"+f);
		TableActions.goPage(p.current);
	}
};

DataTable.TableActions = TableActions;

export default DataTable;