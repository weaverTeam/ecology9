import {message, Row, Col} from 'antd'
// import message from '../../_antd1.11.2/message';
import {defaultIcon,splitUserInfo,createQRCode} from './util/index'
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'

class WeaPopoverHrm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			userid: '',
			userinfo: '',
			visible: false,
			showSQR: false,
			x:-1000,
			y:-1000,
			loading: false,
			imgLoading: false,
			targetHeight: 0,
			targetWidth: 0,

			pWidth: 0,
			pHeight: 0,
			pTop: 0,
			pLeft: 0
		}
	}
	componentDidMount(){
    	window.openhrm = id =>{
    		this.getParentStyle()
			let change = id && this.state.userid !== id;
			let initStates = change ? {loading: true,imgLoading: true, userid:id, userinfo: ''} : {};
			this.setState(initStates);
			if(change){
				jQuery.get("/hrm/resource/simpleHrmResourceTemp.jsp?userid=" + id,
			        result =>{
			            let userinfo = splitUserInfo(result);
			            this.setState({userinfo:userinfo,loading:false})
			            jQuery.get(userinfo.userimg, (data)=>{
			                if (data.trim() == '') {
			                    userinfo.userimg = defaultIcon[userinfo.sex]
			                    this.setState({userinfo:userinfo,imgLoading:false});
			                }else{
			                	this.setState({imgLoading:false});
			                }
			            })
			            createQRCode(userinfo)
			    })
			}
		}
    	window.pointerXY = event =>{
//		    let popupContainer = document.createElement('div');
//		    let mountNode = this.props.getPopupContainer ? this.props.getPopupContainer() : document.body;
//		    mountNode.appendChild(popupContainer);
			let e = event || window.event;
    		this.setState({
    			x: jQuery(e.target).offset().left,
    			y: jQuery(e.target).offset().top,
    			targetWidth: e.target.offsetWidth,
    			targetHeight: e.target.offsetHeight,
    			visible: true
    		})
    	}
    	this.getParentStyle();
    	jQuery(window).resize(() => {
		    this && this.getParentStyle && this.getParentStyle();
		});
    }
	getParentStyle(){
		const { children } = this.props;
    	let com = children ? jQuery('.wea-popover-hrm-relative-parent') : '';
    	if(com){
			this.setState({
				pWidth: com.width(),
				pHeight: com.height(),
				pTop: com.offset().top,
				pLeft: com.offset().left,
			})
    	}
	}
	render(){
		const { children } = this.props;
		const { userid, userinfo, showSQR, visible, x, y, targetWidth, targetHeight, loading, imgLoading,
			pWidth, pHeight, pTop, pLeft} = this.state;
		const _pHeight = pHeight || 0;
		const _pLeft = pLeft || 0;
		const _pTop = pTop || 0;
		const sex = {'Mr.':'（ 男 ）','Ms.':'（ 女 ）'};
		const winW = document.body.clientWidth;
    	const winH = document.body.clientHeight;
		const nowStyle = children ?
		{
			display: visible ? 'block' : 'none',
    		position : 'absolute',
    		left: winW - x <= 500 ? (winW - 500) : (x - _pLeft),
    		top: winH - y <= 293 ? (y - 293 - _pTop) : (y + targetHeight  - _pTop)
		} : {
    		display: visible ? 'block' : 'none',
    		position : 'fixed',
    		left: winW - x <= 500 ? (winW - 500) : x,
    		top: winH - y <= 293 ? (y - 293) : (y + targetHeight)
    	}
		return <div className='wea-popover-hrm-relative-parent'>
			{children ? children : ''}
			<div className='wea-popover-hrm-wrapper' style={nowStyle}>
				<img className="wea-popover-hrm-close" src="/images/messageimages/temp/closeicno_wev8.png" onClick={()=>this.setState({visible:false})}/>
				{ userinfo ?
					<Row>
						<Col span={10} style={{height:288}}>
							<img className='wea-popover-hrm-userimg' src={imgLoading ? '/images/messageimages/temp/loading_wev8.gif' : userinfo.userimg} />
							<div className='wea-popover-hrm-btns'>
								<img onClick={()=>{sendMsgToPCorWeb(userid, 0, '', '')}} src="/images/messageimages/temp/emessage_wev8.png" title="发消息"/>
								<img onClick={()=>{window.open(`/email/MailAdd.jsp?isInternal=1&internalto=${userid}`)}} src="/images/messageimages/temp/email_wev8.png" title="发送邮件"/>
								<img onClick={()=>{showDialog(`/workplan/data/WorkPlan.jsp?resourceid=${userid}&add=1`, "新建日程", 800, 500)}} src="/images/messageimages/temp/workplan_wev8.png" title="新建日程"/>
								<img onClick={()=>{showDialog(`/cowork/AddCoWork.jsp?hrmid=${userid}`, "新建日程", 800, 500)}} src="/images/messageimages/temp/cowork_wev8.png" title="新建协作"/>
							</div>
						</Col>
						<Col span={14} style={{paddingLeft:16}}>
							<div className='wea-popover-hrm-title'>
								<a href={`/hrm/HrmTab.jsp?_fromURL=HrmResource&id=${userid}`} target='_blank' style={{fontSize:14,color:'#018efb'}}>{userinfo.name}</a>
								<span>{`${sex[userinfo.sex]} ${userinfo.code}`}</span>
								<img style={{marginLeft:20,position:'relative',top:5}} onClick={()=>this.setState({showSQR:!showSQR})} src="/images/messageimages/temp/qcode_wev8.png"/>
								<div className='wea-popover-hrm-sqr' style={{display: showSQR ? 'block' : 'none'}} onClick={()=>this.setState({showSQR:false})}></div>
							</div>
							<div className='wea-popover-hrm-info'>
								<p><span>部门:</span>{userinfo.dept}</p>
								<p><span>分部:</span>{userinfo.sub}</p>
								<p><span>岗位:</span>{userinfo.job}</p>
								<p><span>上级:</span>{userinfo.manager}</p>
								<p><span>状态:</span>{userinfo.status}</p>
								<p><span>手机:</span>{userinfo.mobile}</p>
								<p><span>电话:</span>{userinfo.tel}</p>
								<p><span>邮件:</span>{userinfo.email}</p>
							</div>
						</Col>
					</Row>
					: ''
				}
			</div>
		</div>
	}
}

export default WeaPopoverHrm;