import './icons.css'
import './index.css'
import './icon-coms.css'
import './common.css'
import util from './_util/util'
class MyErrorHandler extends React.Component {
    render(){
        return (
            <div className="terrible-error">{this.props.error}</div>
        );
    }
}

let req = require.context('./ecology9', true, /^\.\/[^_][\w-]+\/(style\/)?index\.js?$/);

req.keys().forEach((mod) => {
  let v = req(mod);
  const match = mod.match(/^\.\/([^_][\w-]+)\/index\.js?$/);
  if (match && match[1]) {
      try {
          //console.log(v.default);
          const name = util.camelCase(match[1]);
          // v.default = util.tryCatch(React, MyErrorHandler, {error: "前端组件库异常，请联系管理员!"})(v.default);
          exports[name] = v.default;

      }
      catch(e) {
      }
  }
});

require.context('./ecology9/base', true, /^\.\/[^_][\w-]+\/(style\/)?index\.js?$/);