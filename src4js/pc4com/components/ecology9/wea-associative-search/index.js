import WeaAssociativeSearch from '../base/wea-associative-search';
import WeaAssociativeSearchMult from '../base/wea-associative-search-mult';

class main extends React.Component {
	constructor(props) {
		super(props);
	}
    render() {
        const {props} = this;
        if (props.mult) {
            delete props.mult;
            return (<WeaAssociativeSearchMult {...props} />);
        }
        return (<WeaAssociativeSearch {...props} />);
    }
}

export default main;