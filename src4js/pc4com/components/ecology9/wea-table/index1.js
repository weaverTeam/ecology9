import {Table, Menu, Dropdown, Icon} from 'antd';
// import Table from '../_antd1.11.2/table'
// import Menu from '../_antd1.11.2/menu'
// import Dropdown from '../_antd1.11.2/dropdown'
// import Icon from '../_antd1.11.2/icon'
import cloneDeep from 'lodash/cloneDeep'

const MenuItem = Menu.Item;

class WeaNewTable extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            height:0,
        }
    }
	componentDidMount(){
        let top = jQuery(".wea-new-table").offset() ? (jQuery(".wea-new-table").offset().top ? jQuery(".wea-new-table").offset().top : 0) : 0;
        let scrollheigth = document.documentElement.clientHeight - top;
        this.setState({
	        height: scrollheigth-140
	    })
    }
    render() {
        const {columns,datas,rowSel,loading,operates,tableCheck} = this.props;
        const {height} = this.state;
        let newColumns = cloneDeep(columns);
        let oldWidthObj = {}
        let num = 0;
        let nowWidth = 0;
        newColumns = newColumns.filter((newColumn)=>{
            if(newColumn.display==="true") {
                //console.log("oldWidth：",newColumn.oldWidth," ",parseInt(newColumn.oldWidth.substring(0,newColumn.oldWidth.length-1)));
                const width = newColumn.oldWidth?parseFloat(newColumn.oldWidth.substring(0,newColumn.oldWidth.length-1)):10;
                oldWidthObj[newColumn.dataIndex] = width;
                num++;
                nowWidth+=width;
            }
           // console.log("newColumn.oldWidth:",newColumn.oldWidth," newColumn:",newColumn);
            return newColumn.display==="true";
        });
        newColumns.push({
            title:"",
            dataIndex:"randomFieldOperate",
            key:"randomFieldOperate",
            width:"10%",
            render(text,record,index) {
                const rfs = record.randomFieldOp?JSON.parse(record.randomFieldOp):{};
                let argumentString = [];
                !!record.randomFieldOpPara && record.randomFieldOpPara.map(r=>{argumentString.push(r.obj)})

                let showOperate = null;
                let shouFn = null;
                let hiddenOperate = new Array();
                let opNum = 0;
                operates.map((operate,index)=>{
                    let flag = operate.index || "-1";
                    if(rfs[flag]&&rfs[flag]!="false") opNum++;
                    let fn = !!operate.href ? `${operate.href.split(':')[1].split(')')[0]}${record.randomFieldId},${argumentString});` : ""
                    // if(opNum==1&&rfs[flag]&&rfs[flag]!="false") {
                    //     showOperate = operate;
                    //     shouFn = fn;
                    // }
                    //{showOperate && <a href="javascript:void(0);" onClick={()=>{eval(shouFn)}} style={{marginRight:5}}>{showOperate.text}</a>}
                    if(rfs[flag]&&rfs[flag]!="false") {
                        hiddenOperate.push(
                            <MenuItem>
                                <a href='javascript:void(0);' onClick={()=>{eval(fn)}}>{operate.text}</a>
                            </MenuItem>
                        );
                    }
                });
                const menu = (
                    <Menu>
                        {hiddenOperate}
                    </Menu>
                )
                return (
                    <div>
                        {hiddenOperate.length>0 &&
                        (<Dropdown overlay={menu}>
                            <a className="ant-dropdown-link" href="javascript:void(0);">
                                <Icon type="down" />
                            </a>
                        </Dropdown>)}
                    </div>
                )
            }
        });
        nowWidth += 8; //操作按钮
        if(tableCheck) nowWidth += 2; //check框位置
        newColumns = newColumns.map((newColumn)=>{
            if(newColumn.display==="true") {
                newColumn.width = (
                    parseFloat(oldWidthObj[newColumn.dataIndex])+
                    (100-nowWidth)*parseFloat(oldWidthObj[newColumn.dataIndex]/nowWidth)
                )+"%";
                return newColumn;
            }
            return newColumn;
        });
        const newRowSel = tableCheck?{
            ...rowSel,
            getCheckboxProps: (record) => {
                return {disabled: record["randomFieldCk"] !== "true"};
            }
        }:null;
        return (
            <div className="wea-new-table">
                <Table rowSelection={newRowSel} columns={newColumns} dataSource={datas} pagination={this.getPagination()} scroll={{ y:height }} loading={loading} rowKey={record => record.randomFieldId} />
            </div>
        )
    }
    getPagination() {
        const {count} = this.props;
        let that = this;
        return {
            total:count,
            showTotal(total) {
                return `共 ${total} 条`;
            },
            showSizeChanger: true,
            showQuickJumper: true,
            pageSizeOptions:[10,20,50,100],
            onShowSizeChange(current, pageSize) {
                if(typeof (that.props.onShowSizeChange) == "function") {
                    that.props.onShowSizeChange(current,pageSize);
                }
                //console.log('Current: ', current, '; PageSize: ', pageSize);
            },
            onChange(current) {
                if(typeof (that.props.onChange) == "function") {
                    that.props.onChange(current);
                }
                //console.log('Current: ', current);
            },
        };
    }
}

export default WeaNewTable;