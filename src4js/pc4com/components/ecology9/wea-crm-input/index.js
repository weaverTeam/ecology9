import WeaInput4Base from '../base/wea-input4-base'
import WeaWorkflowRadio from '../base/wea-workflow-radio'

export default class main extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="相关客户"
				type='7'//联想搜索
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","cuslist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
				/>

		)
	}
}

