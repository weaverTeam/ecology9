import Table from './Table'
import TableModal from './TableModal'

class WeaTableEdit extends React.Component {
	render() {
		const { isModalEdit = false } = this.props;
		return (
			isModalEdit ? <TableModal {...this.props} /> : <Table {...this.props}/>
		)
	}
}

export default WeaTableEdit;