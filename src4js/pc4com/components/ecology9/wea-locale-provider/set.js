import {Table, Row, Col, Button, Select, Icon, message} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Table from '../../_antd1.11.2/table'
// import Button from '../../_antd1.11.2/button'
// import Select from '../../_antd1.11.2/select'
// import Icon from '../../_antd1.11.2/icon'
// import message from '../../_antd1.11.2/message'

import WeaInputSearch from '../wea-input-search'
import WeaTools from '../wea-tools'

const Option = Select.Option;

export default class Set extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			loadingL: false,
			loadingR: false,
			datas: [],
			language: [],
			columns: [],
			router: [],
			routerid: '',
			datasSelected: [],
			selectedRowKeysL: [],
			selectedRowKeysR: [],
			selectedRowsL: [],
			selectedRowsR: [],
		}
		this.onChange = this.onChange.bind(this);
		this.addWords = this.addWords.bind(this);
		this.deleteWords = this.deleteWords.bind(this);
		this.doSave = this.doSave.bind(this);
		this.doSearch = this.doSearch.bind(this);
	}
	getTypes(){
		WeaTools.callApi('/api/ec/api/locale/getlist','GET').then( res => {
			const { language, router } = res;
			let columns = [{
				title: 'ID',
				dataIndex: 'indexid',
				width: 80
			}];
			language.map(l => {
				columns.push({
					title: l.language,
					dataIndex: l.languageid
				})
			})
			this.setState({columns, language, router, routerid: '-999999'});
			this.getDatas({routerid: '-999999'},false);
		});
	}
	getDatas(params = {},left = true){
		WeaTools.callApi('/api/ec/api/locale/taglist','GET',params).then( res => {
			let resDatas = [];
			res.datas.map(r => {
				let data = {
					indexid: r.indexid,
					indexdesc: r.indexdesc
				}
				r.tagObjList.map(t => {
					data[t.languageid] = t.labelname
				});
				resDatas.push(data);
			})
			if(left){
				const { datasSelected } = this.state;
				resDatas = this.datasFilter(resDatas,datasSelected);
				this.setState({datas:resDatas,loadingL:false});
			}else{
				let { datas } = this.state;
				datas = this.datasFilter(datas,resDatas);
				this.setState({datas,datasSelected:resDatas,loadingR:false});
			}
		});
	}
	componentDidMount() {
		this.setState({loadingL:true})
		this.getDatas();
		this.getTypes();
	}
	componentDidUpdate() {

	}
	componentWillUnMount() {

	}
	doSearch(search){
		this.setState({loadingL:true})
		this.getDatas({search});
	}
	onChange(routerid){
		this.setState({routerid,loadingR:true})
		this.getDatas({routerid},false);
	}
	doSave(){
		this.setState({loadingL:true,loadingR:true})
		const { routerid, datasSelected } = this.state;
		let labelids = [];
		datasSelected.map(d => {
			labelids.push(d.indexid)
		})
		labelids = `${labelids}`;
		WeaTools.callApi('/api/ec/api/locale/save','POST', {routerid,labelids}).then( res => {
			res.status && this.setState({loadingL:false,loadingR:false})
			message.success('保存成功！');
		});
	}
	datasFilter(datas, filters){
		filters.map(f => {
			datas = datas.filter(d => {
				return d.indexid !== f.indexid;
			})
		})
		return datas;
	}
	addWords(data,index){
		let { datas, datasSelected, selectedRowsL } = this.state;
		datasSelected = 'indexid' in data ? [data].concat(this.datasFilter(datasSelected,[data])) : selectedRowsL.concat(this.datasFilter(datasSelected,selectedRowsL));
		datas = this.datasFilter(datas,datasSelected);
		this.setState({datas,datasSelected,selectedRowsL:[],selectedRowKeysL:[]});
	}
	deleteWords(data,index){
		let { datas, datasSelected, selectedRowsR } = this.state;
		datas = 'indexid' in data ? [data].concat(this.datasFilter(datas,[data])) : selectedRowsR.concat(this.datasFilter(datas,selectedRowsR));
		datasSelected = this.datasFilter(datasSelected,datas);
		this.setState({datas,datasSelected,selectedRowsR:[],selectedRowKeysR:[]});
	}
	render() {
		const _this = this;
		const { datas, datasSelected, selectedRowsL, selectedRowsR, selectedRowKeysL, selectedRowKeysR, loadingL, loadingR, language, columns, router, routerid } = this.state;
		return(
			<div className='wea-locale-provider-set'>
	    		<Row>
	    			<Col span={11}>
	    				<WeaInputSearch style={{position:'absolute',top:0}} onSearch={this.doSearch}/>
	    				<div className='wea-locale-provider-set-table-wrap'>
		    				<Table
		    					size='small'
		    					pagination={false}
		    					loading={loadingL}
		    					columns={columns}
		    					dataSource={datas}
		    					bordered={false}
		    					rowSelection={{
		    						selectedRowKeys:selectedRowKeysL,
		    						onChange(selectedRowKeys,selectedRows){
										_this.setState({selectedRowKeysL:selectedRowKeys,selectedRowsL:selectedRows});
									}
		    					}}
		    					onRowDoubleClick={this.addWords}
		    				/>
	    				</div>
	    			</Col>
	    			<Col span={2}>
	    				<div className='wea-locale-provider-set-trans-btn'>
		    				<Icon type="circle-right" onClick={this.addWords}/>
		    				<Icon type="circle-left" onClick={this.deleteWords} style={{marginTop:44}}/>
	    				</div>
	    			</Col>
	    			<Col span={11}>
	    				<Select showSearch
						    placeholder="请选择路由"
						    value={routerid}
						    optionFilterProp="children"
						    onChange={this.onChange}
						>
	    					{
	    						router.map(t => {
	    							return <Option value={t.id}>{t.name}</Option>
	    						})
	    					}
						</Select>
						<Button className='wea-locale-provider-set-save' type="primary" onClick={this.doSave}>保存</Button>
						<div className='wea-locale-provider-set-table-wrap'>
							<Table
								size='small'
								pagination={false}
								loading={loadingR}
		    					columns={columns}
		    					dataSource={datasSelected}
		    					bordered={false}
		    					rowSelection={{
		    						selectedRowKeys:selectedRowKeysR,
		    						onChange(selectedRowKeys,selectedRows){
										_this.setState({selectedRowKeysR:selectedRowKeys,selectedRowsR:selectedRows});
									}
		    					}}
		    					onRowDoubleClick={this.deleteWords}
		    				/>
	    				</div>
	    			</Col>
	    		</Row>
	    	</div>
		)
	}
}