import {Input} from 'antd';
// import Input from '../../_antd1.11.2/Input'
import WeaInput4Base from '../base/wea-input4-base'
import WeaBrowserSingle from '../base/wea-browser-single'
import WeaBrowserMuti from '../base/wea-browser-muti'
import classNames from 'classnames';
import WeaHrmInput from '../wea-hrm-input';
import WeaCityPicker from '../wea-city-picker';
import WeaBrowserCheckbox from '../base/wea-browser-checkbox';
import WeaTools from '../wea-tools';
import WeaSelect from '../wea-select';
import WeaHrmCondition from '../base/wea-hrm-condition';
import WeaBrowserTableEdit from '../base/wea-browser-table-edit';
import isEmpty from 'lodash/isEmpty'
import WeaBrowserThumbnail from '../base/wea-browser-thumbnail'
import WeaWorkflowHrRadio from '../base/wea-workflow-hr-radio';
import WeaHrMutiInput from '../base/wea-hr-muti-input'

class main extends React.Component {
    static defaultProps={
        topPrefix:'wea',
        isLoadAll:false,
        type:'7',  //字段的类型，对应workflow_billfield表中的type字段
        fieldName:'',  //浏览按钮的名称，在表单中是字段的ID
        value:'' ,//默认数据
        viewAttr:2,   //显示属性 1(只读)/2(可编辑)/3(必填)
        isSingle:true,//是否单选; true:单选 false:多选
        title:'标题',//浏览框标题
        desc:'', //浏览按钮的中文描述
        icon:'icon-toolbar-Organization-list',     //浏览框标题图标类名
        iconBgcolor:'#b32e37',//浏览框标题图标颜色
        onChange:'',//回调函数
        linkUrl:'',//浏览按钮显示值对应的链接地址
        inputStyle:{},//输入框的样式 宽度等
        modalStyle:{width:784},//浏览框样式
        isDetail:'',//字段类型，主表字段最小宽度为230px，明细字段最小宽度为120px
        dataParams:{} ,//dataURL默认参数
        conditionDataParams :{},//条件接口默认参数
        completeParams:{},//completeURL默认参数
        isAutoComplete:0 ,//是否自动提示 默认值:否;
        hasAdd:false,//是否显示快捷添加的按钮，true：显示 false：不显示
        ddUrl:'',//添加按钮对应的链接地址
        addOnClick:'',//单击添加按钮时触发的onclick事件
        hasScroll: true, // 输入框默认显示美化滚动条
        memorise: false, // customized 为true，默认不记忆右侧的选中数据
    }
    static propTypes = {
        type: React.PropTypes.string,
        title: React.PropTypes.string,
        icon:React.PropTypes.string,
        iconBgcolor:React.PropTypes.string,
        dataParams:React.PropTypes.object,
        conditionDataParams:React.PropTypes.object,
        completeParams:React.PropTypes.object,

      }
    constructor(props) {
        super(props);
        // console.log('wea-browser->props',props.tabs);
        this.state = {
            selectOptions: [],
        }
    }
    setMinWidth() {
        if (this.props.resize && this.refs.weaBrowser) {
            let dom = this.refs.weaBrowser.getDOMNode();
            let browserWidth = 210;
            let interval = setInterval(()=>{
                let width =  $(dom).parent().outerWidth();
                if (width > 0) {
                    if (width < 112) width = 112;
                    $(dom).css('max-width', width);
                    // 明细表单特殊处理
                    let minWidth = browserWidth;
                    if (width < browserWidth) {
                        minWidth = width;
                    }
                    if (this.props.isDetail == 1) minWidth = width;
                    $(dom).find('.wea-associative-search').css('min-width', minWidth - 12).css('margin-right', 12);
                    $(dom).css('visibility', 'visible');
                    dom = null;
                    clearInterval(interval);
                }
            }, 10)
        }
    }
    componentDidMount() {
        if (this.props.type == '34') {
            return this.getLevelCmpsDatas();
        }
        this.setMinWidth();
    }
    componentDidUpdate() {
        this.setMinWidth();
    }
    renderSingle(){
        const {isSingle,type,value,dataParams,completeParams,conditionDataParams,...restProps} = this.props;
        let {dataURL,conditionURL,completeURL} = this.props;
        dataURL = dataURL || `/api/workflow/browser01/data/${type}`;
        conditionURL = conditionURL || `/api/workflow/browser01/condition/${type}`;
        completeURL = completeURL || `/api/workflow/browser01/complete/${type}`;
        if(type=='1'){
            return  (
                <WeaWorkflowHrRadio
                    ref="browser"
                    dropMenuIcon={<i className='icon-toolbar-Organization-list' />}
                    resourcetree="/api/ec/api/hrm/v2/resourcetree"//按照组织结构
                    virtualUrl="/api/ec/api/hrm/companyvirtual"//维度
                    grouptree="/api/ec/api/hrm/v2/grouptree"
                    conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`}
                    completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`}
                    {...this.props}
                />
            );
        }
       if(type=='258' || type=='2222' || type=='58' || type=='263' ){//国家 省份 城市 区县
            return <WeaCityPicker
                     dataUrl = {`${dataURL}?${WeaTools.getFd(dataParams)}`} //浏览框数据URL
                     completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`} //联想搜索URL
                     conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`} //浏览框高级查询条件URL
                     {...this.props}
                    />
        }
        return (
            <WeaBrowserSingle
                ref="browser"
                dataUrl = {`${dataURL}?${WeaTools.getFd(dataParams)}`} //浏览框数据URL
                completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`} //联想搜索URL
                conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`} //浏览框高级查询条件URL
                contentHeight={460}//浏览框内容高度
                {...this.props}
            />
        );
    }
    renderMult(){
        const {isSingle, type,value,dataParams,completeParams,conditionDataParams,destDataParams,isMultCheckbox,...restProps} = this.props;
        let {dataURL,conditionURL,completeURL} = this.props;
        dataURL = dataURL || `/api/workflow/browser01/data/${type}`;
        conditionURL = conditionURL || `/api/workflow/browser01/condition/${type}`;
        completeURL = completeURL || `/api/workflow/browser01/complete/${type}`;
//      if(type=='_141'){//弹框编辑table,类人力资源条件
//          return (
//          	<WeaBrowserTableEdit
//                  contentHeight={460}
//                  {...this.props}
//                  conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`}
//              />
//          )
//      }
        if(type=='17'){
            return  (
                <WeaHrMutiInput
                    ref="browser"
                    dropMenuIcon={<i className='icon-toolbar-Organization-list' />}
                    resourcetree="/api/ec/api/hrm/v2/resourcetree"//按照组织结构
                    virtualUrl="/api/ec/api/hrm/companyvirtual"//维度
                    grouptree="/api/ec/api/hrm/v2/grouptree"
                    completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`}
                    conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`}
                    {...this.props}
                />
            );
        }
        if(type=='141'){//人力资源条件 多选
            return (
            	<WeaHrmCondition
                    contentHeight={460}
                    {...this.props}
                    conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`} //浏览框高级查询条件URL
                />
            )
        }
        if(isMultCheckbox) {
            return  (
                <WeaBrowserCheckbox
                    ref="browser"
                    dataUrl = {`${dataURL}?${WeaTools.getFd(dataParams)}`} //浏览框数据URL
                    completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`} //联想搜索URL
                    conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`} //浏览框高级查询条件URL
                    contentHeight={460}//浏览框内容高度
                    {...this.props}
                />
            );
        }
        if(type=='184'){
			return (
				<WeaBrowserThumbnail
					dataUrl = {`${dataURL}?${WeaTools.getFd(dataParams)}`} //浏览框数据URL
	                completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`} //联想搜索URL
	                conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`} //浏览框高级查询条件URL
	                contentHeight={450}//浏览框内容高度
	                {...this.props}
				/>
			)
		}
        return (
            <WeaBrowserMuti
                ref="browser"
                dataUrl = {`${dataURL}?${WeaTools.getFd(dataParams)}`} //浏览框数据URL
                completeURL={`${completeURL}?${WeaTools.getFd(completeParams)}`} //联想搜索URL
                conditionURL={`${conditionURL}?${WeaTools.getFd(conditionDataParams)}`} //浏览框高级查询条件URL
                destUrl={`/api/workflow/browser01/destData/${type}?${WeaTools.getFd(destDataParams)}`} // 多选右侧查找数据接口
                contentHeight={410}//浏览框内容高度
                {...this.props}
            />
        );
    }
    renderInput(){
        //readOnly ? this.renderInput() :
        const {fieldName,value} = this.props;
        return(
            <Input id={fieldName+'span'} value={value} disabled/>
        );
    }
    renderLevelCmps() {
        const {selectOptions} = this.state;
        return (
            <div
                style={{display: 'inline-block'}}
                ref="selectWrapper"
            >
                <WeaSelect
                    {...this.props}
                    layout={this.refs.selectWrapper}
                    detailtype={1}
                    options={selectOptions}
                    style={isEmpty(this.props.inputStyle)? {width: 210} : this.props.inputStyle}
                />
            </div>
        )
    }
    getLevelCmpsDatas() {
        let {dataURL, dataParams, type} = this.props;
        dataURL = dataURL || `/api/workflow/browser01/data/${type}`;
        dataURL = `${dataURL}?${WeaTools.getFd(dataParams)}`;
        WeaTools.callApi(dataURL).then(data=> {
            this.setState({selectOptions: data.datas});
        });
    }
    set(datas) {
        this.refs.browser && this.refs.browser.set(data);
    }
    render() {
        const {customized, fieldName} = this.props;
        const className = classNames({
          'wea-browser': true,
          'customized': customized,
          'wea-field': /^field/.test(fieldName),
        });
        const {isSingle,type,title,tempTitle,...restProps} = this.props;
        // 请假类型
        if (type == '34') {
            return this.renderLevelCmps();
        }
        const browser =  isSingle ? this.renderSingle() : this.renderMult();
        const style = {};
        if (this.props.resize) {
            style.visibility = 'hidden';
        }
        return (
            <div className={`${className} ${this.props.className || ''}`} ref="weaBrowser" style={style}>{browser}</div>
        );
    }
}

export default main;


