import _get from 'lodash/get';
class WeaEchart extends React.Component {
    constructor(props) {
        super(props);
        this.chart = undefined;
        this.state = {
            repaint: false
        };
    }
    
    componentDidMount() {
        this.paint();
    }
    componentWillReceiveProps(nextProps) {
        let repaint = false;
        if (nextProps.useDefault) {
            let newSeries = nextProps.series, {series} = this.props;
            if (newSeries.length != series.length) {
                repaint = true;
            } else {
                if (Array.isArray(newSeries)) {
                    repaint = newSeries.filter((c,i) => c != series[i]).length > 0;
                }
            }
        }
        this.setState({
            repaint: repaint
        });
    }
    componentDidUpdate() {
        if (this.state.repaint) {
            this.paint();
            this.setState({repaint: false});
        }
    }
    componentWillUnmount() {
        this.clear();
    }
    render() {
        let minWidth = _get(this, 'props.categories.length', 0) * 50 + 50;
        let style = {height: '100%', minWidth: minWidth};
        return (
            <div ref="chartDiv" style={style} {...this.props}></div>
        );
    }
    clear() {
        this.chart && this.chart.clear();
    }
    paint() {
        let {chartName, categories, series, useDefault, option, legendData} = this.props;
        this.chart || (this.chart = window.echarts.init(ReactDOM.findDOMNode(this.refs.chartDiv)));
            
        // 指定图表的配置项和数据
        let defaultOption = useDefault ? {
            noDataLoadingOption: {
                text: '暂无数据',
                effect: 'bubble',
                effectOption: {
                    effect: {
                        n:0
                    }
                }
            },
            title: {text: chartName},
            legend: {
                data: legendData
            },
            xAxis: {data: categories,
                axisLine:{lineStyle:{color:'#c5c5c5'}},
                splitLine: {
                    show:true,
                    lineStyle: {color:['#c5c5c5']}
                },
                axisTick:{lineStyle:{color: '#c5c5c5'}},
                axisLabel: {
                    interval: 0,
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            yAxis: {
                max: 5,
                axisLine:{lineStyle:{color:'#c5c5c5', width: 1, shadowBlur: 0, opacity: 0}},
                axisLabel: {
                    textStyle: {
                        color: '#000'
                    }
                }
            },
            grid: {
                left:30,
                right:0,
                borderColor: '#c5c5c5'
            },
            color: ['#4bb1fd'],
            series: [{type: 'bar', data: series}],
            tooltip: {
                formatter: '<strong>' + chartName + '</strong><br/>{b0}: {c0}'
            }
        } : {};
        
        let mergeOption =  option ? Object.assign({}, defaultOption, option) : defaultOption;
        this.chart.setOption(mergeOption);
    }
}

WeaEchart.propTypes = {
    chartName: React.PropTypes.string,//表格标题
    categories: React.PropTypes.array,//x轴
    series: React.PropTypes.array,//柱状图数据
    legendData: React.PropTypes.array,//图例
    option: React.PropTypes.object,// 选项。
    useDefault: React.PropTypes.bool//是否使用默认值，默认为true。true时，上面的option会与默认值合并，如改为false，则只会使用传入的option。
};

WeaEchart.defaultProps = {
    chartName: '泛微表格',
    useDefault: true
};

// if (window.ecCom) {
//     window.ecCom.WeaEchart = WeaEchart;
// } else {
//     console.error("无法取到ecCom");
// }

export default WeaEchart;