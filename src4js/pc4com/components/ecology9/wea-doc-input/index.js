import WeaInput4Base from '../base/wea-input4-base'
import WeaWorkflowRadio from '../base/wea-workflow-radio'

export default class WeaInput4DocsNew extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="相关文档"
				type='9'//联想搜索
				icon={<i className='icon-portal-doc' />}
				iconBgcolor='#e98931'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","doclist"]}//otherPara={["action","BrowserAction","actiontype","doclist"]}
				{...this.props}
				/>

		)
	}
}

