import WeaInputNormal from '../base/wea-input-normal';

class main extends React.Component {
	static defaultProps={
		inputType: 'NORMAl',
	}
	static propTypes = {
		inputType: React.PropTypes.string,
	}
	constructor(props) {
		super(props);
	}
	render() {
		const {props} = this;
		if (props.inputType.toUpperCase() == 'FORM') {
			return (<input />)
		}
		return (<WeaInputNormal {...props}/>);
	}
}

export default main;


