require("./scroll");

class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			height: 0
		}
    }
	componentDidMount() {
        const {className,typeClass,conClass,conHeightNum} = this.props;
		this.createScroll(className,typeClass,conClass,conHeightNum);
	}
	componentDidUpdate(prevProps)　{
		const {className,typeClass,conClass,conHeightNum} = this.props;
		if(prevProps.conHeightNum != conHeightNum || prevProps.conClass != conClass)
			this.createScroll(className,typeClass,conClass,conHeightNum);
	}
    render() {
        const {typeClass,className,children} = this.props;
        return (
            <div className={className + " " + typeClass}>{children}</div>
        )
    }
	createScroll(className,typeClass,conClass,conHeightNum) {
		const that = this;
		let isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
		if(isIE8){
			if(!!conClass && !!conHeightNum){
				let height = jQuery("."+conClass).height()-conHeightNum;
				jQuery('.' + className).css({maxHeight: (height + 2) + 'px',overflowY:'scroll',overflowX:'hidden'});
			}
		}else{
			jQuery('.'+className).scrollbar({
				onInit:function() {
					if(!!conClass && !!conHeightNum){
						let height = jQuery("."+conClass).height()-conHeightNum;
						jQuery('.' + className).css({'max-height': height + 'px'});
					}
				},
				onScroll:function(a,b,c,d) {
					window.weaScrollTop = {};
					weaScrollTop[className] = a.scroll;
				}
			});
		}
	}
}

export default Main