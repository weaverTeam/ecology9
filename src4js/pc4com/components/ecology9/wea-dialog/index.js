import {Modal, Row, Col, Button} from 'antd';
// import Modal from '../../_antd1.11.2/Modal';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import WeaTools from '../wea-tools';
// import Row from '../../_antd1.11.2/row';
// import Col from '../../_antd1.11.2/col';
// import Button from '../../_antd1.11.2/Button';
import isString from 'lodash/isString';

class Main extends React.Component {
    static defaultProps = {
        style: {width: 520, height: 400},
        icon: 'icon-coms-ModelingEngine',
        maskClosable: false,
        closable: true,
        title: 'Dialog',
        iconBgcolor: '#2db7f5',
    }
    static contextTypes = {
        router: React.PropTypes.routerShape
    }
    listenRouter(){
        this.context.router.listen(nextState => {
            const { path } = this.state;
            const { pathname } = nextState;
            if(path !== pathname){
                this._reactInternalInstance !== undefined && this.setState({path:pathname, visible:false});
            }
        })
    }
    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            modalBody: '',
            title: props.title || '',
            url: props.url || '',
            style: props.style,
            icon: props.icon,
            closable: props.closable,
            buttons: props.buttons,
            maskClosable: props.maskClosable,
            iconBgcolor: props.iconBgcolor,
            path: window.location.hash && window.location.hash.split('#/')[1].split('?')[0],
        }
        this.onCancel = this.onCancel.bind(this);
    }
    componentWillReceiveProps(nextProps){
        if (this.props.visible !== nextProps.visible || this.state.visible !== nextProps.visible) {
            this.setState({visible: nextProps.visible});
        }
        if (this.props.url !== nextProps.url) {
            this.setState({url: nextProps.url});
        }
        if (!WeaTools.isEqual(this.props.style, nextProps.style)) {
            this.setState({style: nextProps.style});
        }
        if ('buttons' in this.props) {
            this.setState({buttons: nextProps.buttons});
        }
        if (this.props.title !== nextProps.title) {
            this.setState({title: nextProps.title});
        }
        // if (this.props.icon !== nextProps.icon) {
        //     this.setState({icon: nextProps.icon});
        // }
        // if (this.props.iconBgcolor !== nextProps.iconBgcolor) {
        //     this.setState({iconBgcolor: nextProps.iconBgcolor});
        // }
    }
    shouldComponentUpdate(...args) {
        return PureRenderMixin.shouldComponentUpdate.apply(this, args);
    }
    componentDidMount(){
        this.listenRouter();
        window.initWeaDialog = params => {
            let state = {};
            if (params.style) {
                state.style = params.style;
            }
            if (params.buttons) {
                state.buttons = params.buttons;
            }
            if (params.url) {
                state.url = params.url;
            }
            if (params.modalBody) {
                state.modalBody = params.modalBody;
            }
            if (params.title) {
                state.title = params.title;
            }
            if (params.icon) {
                state.icon = params.icon;
            }
            if (params.closable !== undefined) {
                state.closable = params.closable;
            }
            if (params.maskClosable !== undefined) {
                state.maskClosable = params.maskClosable;
            }
            if (params.iconBgcolor) {
                state.iconBgcolor = params.iconBgcolor;
            }
            this.setState(state);
        }
        window.showWeaDialog = () => {
            this.setState({visible: true});
        }
        window.closeWeaDialog = () => {
            this.onCancel();
        }
    }
    onCancel() {
        this.setState({visible: false});
        this.props.onCancel && this.props.onCancel();
    }
    render(){
        const {children, hideIcon = false} = this.props;
        const {modalBody, visible, url, closable, style, maskClosable, buttons, title, icon, iconBgcolor} = this.state;
        let titleEle = (
            <Row>
                <Col span="22" style={{paddingLeft:20,lineHeight:'48px'}}>
                    <div>
                        {
                            !hideIcon &&
                            <div className="wea-browser-single-icon-circle" style={{background: iconBgcolor}}>
                                <i className={icon}/>
                            </div>
                        }
                        <span style={{verticalAlign:'middle'}}>{title}</span>
                    </div>
                </Col>
            </Row>
        );
        let body;
        if (modalBody) {
            body = modalBody;
            if (isString(modalBody)) body = <div  dangerouslySetInnerHTML={{__html: modalBody}}></div>
        }
        return (
            <Modal
                wrapClassName={`wea-dialog ${this.props.className}`}
                zIndex={this.props.zIndex}
                title={titleEle}
                width={style.width}
                closable={closable}
                maskClosable={maskClosable}
                visible={visible}
                onCancel={this.onCancel}
                footer={buttons?buttons:''}
                >
                    <div className="wea-dialog-body" style={{height: style.height}}>
                        {children ? children : ''}
                        {body}
                        {url?
                            <iframe src={url} height={style.height - 20} width={style.width}>
                            </iframe>
                            : ''
                        }
                    </div>
            </Modal>
        )
    }
}
export default Main;