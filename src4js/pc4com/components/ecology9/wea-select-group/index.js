import {Row, Col} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
import WeaTools from '../wea-tools'
import WeaSelect from '../wea-select'
import WeaInput from '../wea-input'
import WeaBrowser from '../wea-browser'
import WeaCheckbox from '../wea-checkbox'
import isEmpty from 'lodash/isEmpty'

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectValue: props.value || '',
            componentValue: '',
        };
    }

    componentWillReceiveProps(nextProps) {
        const {value,form,domkey} = nextProps;
        if (this.props.value !== nextProps.value && isEmpty(nextProps.value)){
            let selectValue = WeaTools.getSelectDefaultValue(this.props.options);
            if (domkey) {
                let obj = {};
                let rightKey = this.getRightComponentKey();
                obj[domkey[0]] = selectValue;
                obj[rightKey] = '';
                form && form.setFieldsValue(obj);
            }
            this.setState({
                selectValue,
                componentValue: '',
            });
        }
        if (this.state.selectValue !== nextProps.value && !isEmpty(nextProps.value)) {
            this.setState({selectValue: nextProps.value})
        }
    }
    selectHandler(v) {
        const {domkey, form}=this.props;
        let {selectValue, componentValue} = this.state;
        if (v !== selectValue) {
            let rightKey = this.getRightComponentKey(selectValue);
            let rightKeyAfter = this.getRightComponentKey(v);
            if (rightKey !== rightKeyAfter) {
                componentValue = '';
            }
            if (domkey) {
                let obj = {};
                obj[domkey[0]] = v;
                obj[rightKey] = componentValue;
                form && form.setFieldsValue(obj);
            }
            this.setState({selectValue: v, componentValue});
            !form && this.props.onChange && this.props.onChange(v, componentValue);
        }
    }
    componentHandler(...args) {
        const {selectValue} = this.state;
        this.setState({componentValue: args[0]});
        this.props.onChange && this.props.onChange(selectValue, args[0]);
    }
    getRightComponentObj(selectValue) {
        selectValue = selectValue || this.state.selectValue;
        const {selectLinkageDatas, options} = this.props;
        let obj;
        if (!isEmpty(selectLinkageDatas)) {
            obj = selectLinkageDatas[selectValue] || selectLinkageDatas[options[0].key]
        }
        return obj;
    }
    getRightComponentKey(selectValue) {
        let key;
        let obj = this.getRightComponentObj(selectValue);
        if (obj && obj.domkey) {
            key = obj.domkey[0];
        }
        return key;
    }
    renderComponent(obj) {
        const {componentValue} = this.state;
        if (obj) {
            let ct =  obj.conditionType.toUpperCase();
            if(ct == 'INPUT'){
                return (<WeaInput
                         defaultValue={obj.value}
                         value={componentValue}
                         onChange={this.componentHandler.bind(this)}
                         />);
            }
            if(ct == 'SELECT'){
                return (
                    <WeaSelect
                        options={obj.options}
                        value={componentValue}
                        onChange={this.componentHandler.bind(this)}
                    />
                );
            }
            if(ct == 'BROWSER'){
                const  showDropMenu = obj.browserConditionParam.type=='4' || obj.browserConditionParam.type=='164'; //部门分部显示维度菜单
                return (
                    <WeaBrowser
                        {...obj.browserConditionParam}
                        value={componentValue}
                        onChange={this.componentHandler.bind(this)}
                        showDropMenu={showDropMenu} />
                );
            }
            if(ct == 'CHECKBOX'){
                return (
                    <WeaCheckbox
                    onChange={this.componentHandler.bind(this)}
                    />
                );
            }
        }
    }
    componentFormHandler(...args) {
        const {domkey, form}=this.props;
        const {selectValue} = this.state;
        if (domkey) {
            let obj = {};
            obj[domkey[0]] = selectValue;
            setTimeout(()=> {
                form.setFieldsValue(obj);
            }, 0)
        }
        this.setState({componentValue: args[0]});
    }
    renderComponentWithForm(obj) {
        const {form} = this.props;
        const {getFieldProps} = form;
        if (obj) {
            let ct =  obj.conditionType.toUpperCase();
            if(ct == 'INPUT'){
                return (<WeaInput
                        {...getFieldProps(obj.domkey[0], {initialValue: obj.value})}
                        />);
            }
            if(ct == 'SELECT'){
                return (
                    <WeaSelect
                        {...getFieldProps(obj.domkey[0], {initialValue: obj.value})}
                        options={obj.options}
                    />
                );
            }
            if(ct == 'BROWSER'){
                const  showDropMenu = obj.browserConditionParam.type=='4' || obj.browserConditionParam.type=='164'; //部门分部显示维度菜单
                return (
                    <WeaBrowser
                        {...getFieldProps(obj.domkey[0],{onChange: this.componentFormHandler.bind(this)})}
                        layout={document.body}
                        {...obj.browserConditionParam}
                        showDropMenu={showDropMenu} />
                );
            }
        }
    }
    render() {
        const {options, selectWidth, form} = this.props;
        const {selectValue} = this.state;
        const selectStyle = {};
        if (selectWidth) {
            selectStyle.width = selectWidth;
        }
        let obj = this.getRightComponentObj();
        return (
            <div className="wea-select-group">
                <Col span={10} className="select-wrapper" style={selectStyle}>
                    <WeaSelect {...this.props} options={options} value={selectValue} onChange={this.selectHandler.bind(this)}/>
                </Col>
                <Col span={14} style={{paddingLeft: 10}}>
                    {form? this.renderComponentWithForm(obj) : this.renderComponent(obj)}
                </Col>
            </div>
        )
    }
}

export default Main;