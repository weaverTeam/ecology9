import {Row, Col, Icon} from 'antd';
// import Row from '../../../_antd1.11.2/row'
// import Col from '../../../_antd1.11.2/col'
// import Icon from '../../../_antd1.11.2/icon'

export default class WeaSearchBrowserBox extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
        }
    }

	render() {
        const {items} = this.props;
        return (
            <div className="wea-search-group">
                    <Row className="wea-content" >
                        {
                            items.map(item => {
                                let style = {};
                                if (item.hide) {
                                    style.display = 'none';
                                }
                                return (
                                    <Col className="wea-form-cell" span={24/item.colSpan} style={style}>
                                        {item.com}
                                    </Col>
                                )
                            })
                        }
                    </Row>
            </div>
        )
    }

}
