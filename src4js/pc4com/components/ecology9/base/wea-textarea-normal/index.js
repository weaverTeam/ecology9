import './style/index.css'
import {Input} from 'antd';
// import Input from '../../../_antd1.11.2/Input'
import trim from 'lodash/trim'
import classNames from 'classnames'

class Main extends React.Component {
	static defaultProps={
		style:{},
		viewAttr: 2,
		disabled: false,
		minRows: 2,
	}
	constructor(props) {
		super(props);
		this.state = {
			value: props.value ? props.value : "",
		};
	}

	componentWillReceiveProps(nextProps) {
		if (this.state.value !== nextProps.value) {
			this.setState({
				value: nextProps.value
			});
		}
	}
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value ||
			nextProps.viewAttr !== this.props.viewAttr ||
			this.state.value !== nextState.value;
	}

	componentDidMount() {
	}

	render() {
		const {value} = this.state;
		const {viewAttr, minRows, maxRows, fieldName, style, disabled} = this.props;
		let styleInfo = { 'word-break': 'break-all', 'word-wrap': 'break-word' ,'width':'100%'};
		const req = classNames({
			'required': viewAttr == '3' && trim(value).length === 0,
		});

		if (viewAttr === 1) {
			return <span className="wea-textarea-normal text" style={style} id={fieldName} dangerouslySetInnerHTML={{__html: value}}></span>
		}

		return (
			<div className={`wea-textarea-normal ${req}`} style={style}>
				<Input
					type="textarea"
					disabled={disabled}
					autosize={{minRows: minRows, maxRows:maxRows}}
					value={value}
					id={fieldName}
					name={fieldName}
					style={styleInfo}
					onBlur={this.onBlur.bind(this)}
					onFocus={this.onFocus.bind(this)}
					onChange={this.onChange.bind(this)}
				/>
			</div>
		)
	}
	onFocus(e) {
        this.props.onFocus && this.props.onFocus(e.target.value);
    }
	onBlur(e) {
		this.props.onBlur && this.props.onBlur(e.target.value);
	}

	onChange(e){
		if (this.props.length && trim(e.target.value).length > this.props.length) {
			alert(`最长不能超过${this.props.length}个字符`);
			return;
		}
		this.setState({
			value: e.target.value
		})
		this.props.onChange && this.props.onChange(e.target.value);
	}
}

export default Main