import Promise from 'promise'
import * as util from './util'

const server = window.server||"";

export const getTableDatas = (params) => {
    return new Promise((resolve,reject)=>{
        fetch(server+"/api/ec/dev/table/datas",
            util.getFetchParams("POST",params)).then((response)=>{
                return response.json();
            }).then(function(json) {
            const data = json;
            if(util.checkReject(data)) {
                reject(data.error || "getTableDatas后端数据处理异常");
            }
            else {
                resolve(data);
            }
        }).catch(function(ex) {
            reject("getTableDatas后端AJAX异常:",ex);
        });
    });
}
export const getTableSet = (params) => {
    return new Promise((resolve,reject)=>{
        fetch(server+"/api/ec/dev/table/set",
            util.getFetchParams("POST",params)).then((response)=>{
                return response.json();
            }).then(function(json) {
            const data = json;
            if(util.checkReject(data)) {
                reject(data.error || "getTableSet后端数据处理异常");
            }
            else {
                resolve(data);
            }
        }).catch(function(ex) {
            reject("getTableDatasAJAX异常:",ex);
        });
    });
}


export const getTableChecks = (params) => {
    return new Promise((resolve,reject)=>{
        fetch(server+"/api/ec/dev/table/checks",
            util.getFetchParams("POST",params)).then((response)=>{
                return response.json();
            }).then(function(json) {
            const data = json;
            if(util.checkReject(data)) {
                reject(data.error || "getTableChecks后端数据处理异常");
            }
            else {
                resolve(data);
            }
        }).catch(function(ex) {
            reject("getTableChecks后端AJAX异常:",ex);
        });
    });
}
export const getTableCounts = (params) => {
    return new Promise((resolve,reject)=>{
        fetch(server+"/api/ec/dev/table/counts",
            util.getFetchParams("POST",params)).then((response)=>{
                return response.json();
            }).then(function(json) {
            const data = json;
            if(util.checkReject(data)) {
                reject(data.error || "getTableCounts后端数据处理异常");
            }
            else {
                resolve(data);
            }
        }).catch(function(ex) {
            reject("getTableCounts后端AJAX异常:",ex);
        });
    });
}