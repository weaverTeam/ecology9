import {Menu, Dropdown, Icon, Button, Spin, Row, Col, Checkbox, Pagination} from 'antd';
// import Menu from '../../../_antd1.11.2/menu'
const Menub = Menu.b;
// import Dropdown from '../../../_antd1.11.2/dropdown'
// import Icon from '../../../_antd1.11.2/icon'
// import Button from '../../../_antd1.11.2/button'
// import Spin from '../../../_antd1.11.2/spin'
// import Row from '../../../_antd1.11.2/row'
// import Col from '../../../_antd1.11.2/col'
// import Checkbox from '../../../_antd1.11.2/checkbox'
// import Pagination from '../../../_antd1.11.2/pagination';
import cloneDeep from 'lodash/cloneDeep'
import isEqual from 'lodash/isEqual'
import isEmpty from 'lodash/isEmpty'

// import Immutable from 'immutable'
import WeaScroll from '../../wea-scroll';
import uniq from 'lodash/uniq'
class Main extends React.Component {
    static defaultProps = {
        justShowChecked:'仅显示已选'
    }
	constructor(props) {
        super(props);
        this.state = {
            browser:props.browser||[],
            checked: false,
            disabled: false,
            fiterBrowserData:[],
            showSelected:false,
        }
        const func = ['filterChecked','onMouseEnter','onMouseLeave'];
        func.forEach(f=>this[f]=this[f].bind(this))
    }
    componentWillReceiveProps(nextProps){
        if(!isEqual(this.props.browser,nextProps.browser)){
            this.setState({browser:nextProps.browser})
        }

    }
    shouldComponentUpdate(nextProps, nextState) {
        return !isEqual(this.props.browser,nextProps.browser)||
            !isEqual(this.state.browser,nextState.browser)||
            this.state.checked !== nextState.checked||
            this.state.disabled !== nextState.disabled||
            !isEqual(this.state.fiterBrowserData,nextState.fiterBrowserData)||
            this.state.showSelected !== nextState.showSelected||
            !isEqual(this.props.tableDatas,nextProps.tableDatas)||
            this.props.num !== nextProps.num ||
            !isEqual(this.props.ctrIds,nextProps.ctrIds)||
            !isEqual(this.props.ctrDatas,nextProps.ctrDatas)||
            !isEqual(this.props.children,nextProps.children)

    }
	render(){
		let {browser,fiterBrowserData,showSelected} = this.state;
        let {tableDatas,num=4,ctrIds=[],ctrDatas=[]} = this.props;
        // console.log('browser',browser,'fiterBrowserData',fiterBrowserData);
        // console.log('render---ctrIds',ctrIds,'ctrDatas',ctrDatas);
        let thumbnailsArr=[];
        const dom =(b,index)=>{
            let imgUrl = '/cloudstore/resource/pc/com/images/meeting_default.png';
            if (b.linkvaluecolumn) {
                imgUrl = `${b.imgurl}?${b.linkkey}=${b.linkvaluecolumn.split(',')[0]}`;
            }
            return (
                <Col class="wea-thumbnails-row"  span={24/num}>
                   <div className='wea-thumbnails-box'>
                        <div className='wea-thumbnails-check'>
                            <Checkbox id={b.id}
                            imgId={b.linkvaluecolumn.split(',')[0]}
                            checked={ctrIds.indexOf(b.id)>=0}
                            onChange={this.checkboxOnChange.bind(this)}/>
                        </div>
                        <div className='wea-thumbnails-img-wrap'
                            imgIds={b.linkvaluecolumn}
                            onClick={(...e)=>{console.log('img-wrap--onClick',e,b.linkvaluecolumn)}}
                            onMouseEnter={this.onMouseEnter.bind(null,b.id)}
                            onMouseLeave={this.onMouseLeave.bind(null,b.id)}
                        >
                            <img src={imgUrl} alt={''} />
                        </div>
                        <div className='wea-thumbnails-supernatant' style={{display:b.hover?'block':'none'}}>
                            {b.desccolumn}
                        </div>
                        <div className='wea-thumbnails-title' title={b.linktitlecolumn} style={{fontSize:14}} >
                            {b.linktitlecolumn}
                        </div>
                         <div className='wea-thumbnails-subcolumn' title={b.subcolumn} style={{color:'#ccc'}}>
                            {b.subcolumn}
                        </div>
                   </div>
                </Col>
            )
        };
        !showSelected && browser && browser.forEach((b,index)=>{
            b.id = tableDatas[index].id;
            thumbnailsArr.push(dom(b,index));
        });
        showSelected && fiterBrowserData.forEach((b,index)=>{
            thumbnailsArr.push(dom(b,index));
        })
		return (
			<div className='wea-thumbnails-table'>
                <span className='wea-thumbnail-filter'>
                    <Checkbox  checked={this.state.showSelected} onChange={this.filterChecked} >{this.props.justShowChecked}</Checkbox>
                </span>
                <span>
                    {this.props.children}
                </span>
                <div className='wea-thumbnail-table-body' style={{height: this.props.contentHeight || 450}}>
                    <WeaScroll className="wea-scroll" typeClass="scrollbar-macosx">
                        <Row>
                            {thumbnailsArr}
                        </Row>
                    </WeaScroll>
                </div>
            </div>
        )
	}
    onMouseEnter(id,event){
        // console.log('---',event,id)
        const {browser}=this.state;
        let newBroser = browser
        const {tableDatas} = this.props;
        newBroser.forEach((b,index)=>{
            if(tableDatas[index].id == id){
                b.hover=true;
            }
        })
        this.setState({
            browser:newBroser,
        });
    }
    onMouseLeave(id,event){
        // console.log('onMouseLeave',event);
        const {browser}=this.state;
        let newBroser = browser;
        const {tableDatas} = this.props;
        newBroser.forEach((b,index)=>{
            if(tableDatas[index].id == id){
                b.hover=false;
            }
        })
        this.setState({
            browser:newBroser,
        });
    }
    filterChecked(e){
        // console.log('filterChecked=',e)
        const {browser} = this.state;
        const {ctrIds} = this.props;
        const newBroser = cloneDeep(browser);
        const isChecked = e.target.checked;
        const nb = newBroser.filter(b=> ctrIds.indexOf(b.id) >= 0);
        this.setState({
            showSelected:isChecked,
            fiterBrowserData:nb,
        });
    }
	checkboxOnChange(e){
        // console.log('checkboxOnChange = ', e.target);
        e && e.stopPropagation && e.stopPropagation();
        e && e.preventDefault && e.preventDefault();
        const {browser} = this.state;
        const {tableDatas,ctrIds,ctrDatas,ctrNames} = this.props;
        let newBroser = cloneDeep(browser);
        let ids = uniq([...ctrIds]),  datas = [...ctrDatas];
        newBroser.forEach((b,index)=>{
            b.id = tableDatas[index].id;
            b.name = b.linktitlecolumn;
            if(b.id == e.target.id){
                if(e.target.checked ){
                    ids.push(b.id);
                    datas.push(b);
                }
                if(!e.target.checked){
                    const ix = ids.indexOf(e.target.id);
                    if(ix >= 0){
                        ids.splice(ix,1);
                        datas.splice(ix,1);
                    }
                }
            }
        })
        // console.log('thumbnailsOnCheck------ids',ids,'datas',datas);
        if(typeof this.props.thumbnailsOnCheck == 'function'){
            this.props.thumbnailsOnCheck(ids, datas)
        }
    }
    thumbnailsPageChange(page) {
        // console.log('page',page);
       if(typeof this.props.onChange == "function"){
            this.props.onThumbnailsChange(page,this.props.pageSize, '');
        }
    }
    thumbnailOnShowSizeChange(current, pageSize){
         this.props.onThumbnailsChange(current,pageSize, '');
    }

}
export default Main;
