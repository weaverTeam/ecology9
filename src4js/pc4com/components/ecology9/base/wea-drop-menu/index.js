import {Row, Col, Button, Menu} from 'antd';
// import Row from '../../../_antd1.11.2/row'
// import Col from '../../../_antd1.11.2/col'
// import Button from '../../../_antd1.11.2/button'
// import Menu from '../../../_antd1.11.2/menu'
const Item = Menu.Item

import cloneDeep from 'lodash/cloneDeep'

class WeaDropMenu extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        showDrop: false
      }
  }
  render() {
    const {showDrop} = this.state;
    const {dropMenuDatas} = this.props;//isFixed:wea-new-top是否固定
    const menu = dropMenuDatas ?
      <Menu mode='vertical' onClick={o => {if(typeof this.props.onDropMenuClick == 'function') this.props.onDropMenuClick(o.key)}}>
      {
        dropMenuDatas.map((d, i)=> {
            return <Item key={d.key || i} disabled={d.disabled}>
              <span className='wea-right-menu-icon'>{d.icon}</span>
              {d.content}
             </Item>
      })}
      </Menu> : '';
    return (
      <div className="wea-drop-menu">
        <span className='wea-new-top-drop-btn wea-btn-top' onClick={()=>this.setState({showDrop:true})}>
          <i className="icon-button icon-New-Flow-menu" />
        </span>
        <div className='wea-new-top-drop-menu wea-right-menu' onMouseLeave={()=>this.setState({showDrop:false})} style={{display:showDrop ? 'block' : 'none'}}>
          <span className='wea-new-top-drop-btn' onClick={()=>this.setState({showDrop:false})}>
            <i className="icon-button icon-New-Flow-menu" />
          </span>
          <div className='wea-right-menu-icon-background'></div>
          {menu}
        </div>
      </div>
    )
  }
}

export default WeaDropMenu;
