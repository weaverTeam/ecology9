import {Row, Col, Input, Modal, Tabs, Pagination, Select, Menu, Button, Dropdown, Icon, Form, message, Spin} from 'antd';
// import Row from '../../../_antd1.11.2/Row';
// import Col from '../../../_antd1.11.2/Col';
// import Input from '../../../_antd1.11.2/Input';
// import Modal from '../../../_antd1.11.2/Modal';
// import Tabs from '../../../_antd1.11.2/Tabs';
const TabPane = Tabs.TabPane;
// import Select from '../../../_antd1.11.2/Select';
const Option = Select.Option;
// import Menu from '../../../_antd1.11.2/menu';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
// import Button from '../../../_antd1.11.2/Button';
// import Dropdown from '../../../_antd1.11.2/Dropdown';
// import Icon from '../../../_antd1.11.2/Icon';
// import Form from '../../../_antd1.11.2/Form'
const FormItem = Form.Item;
import cloneDeep from 'lodash/cloneDeep';
const  _  =  require('lodash');
import forEach from 'lodash/forEach';
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual';

import WeaInputSearch from '../../wea-input-search';
import WeaSearchBrowserBox from '../wea-search-browser-box';
import WeaAssociativeSearch from '../wea-associative-search';

import WeaDepInput from '../../wea-dep-input';
import WeaComInput from '../../wea-com-input';
import WeaNewScroll from '../../wea-new-scroll';
import WeaInput from '../../wea-input';
import WeaSelect from '../../wea-select';

import WeaInputRoleNew from '../wea-input4-role-new';
import WeaWorkflowTalbeHr from '../wea-workflow-table-hr';
import WeaTreeHr from '../wea-tree-hr';
// import Pagination from '../../../_antd1.11.2/pagination';
import WeaHrmList from '../wea-hrm-list';
import WeaTools from '../../wea-tools';
import WeaBrowserSearch from '../../wea-browser-search';
// import Spin from '../../../_antd1.11.2/Spin';
import debounce from 'lodash/debounce'

const initialState = {
    activeKey:"1",
    ids: "",
    names: "",
    url: '/api/ec/api/hrm/newly', // 最近
    hasPagination: true,
    count: 0,
    currentPage: 1,
    searchPara: {},
    searched:false,
    showSearchAd: false,
    inputValue: '',
    visible: false,
    browserConditions: [],
    isAccount: false,
    showAllbranch: false,
    showList: true,
    listDatas: [],
    loading: true,
}

class WeaWorkflowHrRadio extends React.Component {
    static defaultProps = {
        topPrefix: 'wea'
    }
    constructor(props) {
        super(props);
        this.state = {
            visible:false,
            showDrop:false,
            activeKey:"1",
            ids: "",
            names: "",
            virtualDatas:[],
            companysDefault:'',
            componyParam:'',
            showSearchAd:false,
            grouptreeData:null,
            rsctreeData:null,
            seacrhPara:'',
            isChange:false,
            searched:false,
            hrmStatus:[],
            isClearData: false,
            listDatas: [],
            currentPage: 1,
            pageSize: 20,
            hasPagination: true,
            count: 0,
            url: '/api/ec/api/hrm/newly',
            inputValue: '',
            searchPara: {},
            browserConditions: [],
            quickSearch: '',
            isAccount: false,  // 有无账号
            showAllbranch: false,
            loading: true,
        }
    }
    componentWillReceiveProps(nextProps){
        if (this.props.value !== nextProps.value && isEmpty(nextProps.value)) {
            this.setState({ids: '', names: '', isClearData: !this.state.isClearData});
        }
        if(this.props.value !== nextProps.value && !isEmpty(nextProps.value) && nextProps.valueSpan){//form value改变&&有值
            this.setState({ids:nextProps.value, names:nextProps.valueSpan});
            // this.throwCurrentState(nextProps.value, nextProps.valueSpan);
        }
        if (!isEqual(this.props.replaceDatas, nextProps.replaceDatas)) {
            let id = '', name = '';
            if (!isEmpty(nextProps.replaceDatas) && !isEmpty(nextProps.replaceDatas[0])) {
                name = nextProps.replaceDatas[0].name || this.props.replaceDatas[0].lastname;
                id = nextProps.replaceDatas[0].id;
            } else {
                this.setState({isClearData: !this.state.isClearData})
            }
            this.setState({ids: id, names: name});
            // this.throwCurrentState(id, name);
        }
    }
    componentDidMount(){
        const {value, valueSpan} = this.props;
    	if(!isEmpty(value) && valueSpan){
    		this.setState({ids: value, names: valueSpan});
    	}
        // 默认显示数据
        if (!isEmpty(this.props.replaceDatas)) {
            let name = this.props.replaceDatas[0].name || this.props.replaceDatas[0].lastname;
            let id = this.props.replaceDatas[0].id;
            this.setState({ids: id, names: name});
            // this.throwCurrentState(id, name);
        }
    }
    set(datas) {
        let id = '', name = '';
        if (!isEmpty(datas)) {
            name = datas[0].name || datas[0].lastname;
            id = datas[0].id;
        } else {
            this.setState({isClearData: !this.state.isClearData})
        }
        this.setState({ids: id, names: name});
    }
    setLoading(bool) {
        this.setState({loading: bool});
    }
    callApi(){
        const {virtualUrl,grouptree,resourcetree} = this.props;
        const {virtualDatas} = this.state;
        WeaTools.callApi(virtualUrl,"GET").then((data)=>{//,{isAccount:isAccount,min:min,max:max}
            let companysDefault = data.datas.map(item => {if (item.id==0) return item.virtualtype})
            this.setState({
                companysDefault:companysDefault,//获取行政组织
                virtualDatas:data.datas,
            });
            this.companysDefault = companysDefault;
            this.companysDefaultId = 0;
        });
        this.getResourceTree(0);
        this.getGrouptree();
    }
    getResourceTree(companyId = 0, isAccount) {
        const {resourcetree} = this.props;
        let params = {};
        this.setLoading(true);
        if (companyId < 0) params.virtualtype = companyId;
        if (isAccount) params['isNoAccount'] = 1;
        WeaTools.callApi(resourcetree, 'GET', params).then((datas)=>{//按照组织结构
            this.setState({rsctreeData:datas, loading: false});
        });
    }
    getGrouptree(isAccount) {
        const {grouptree} = this.props;
        let params = {};
        this.setLoading(true);
        if (isAccount) params['isNoAccount'] = 1;
        WeaTools.callApi(grouptree, 'GET', params).then((datas)=>{//常用组
            this.setState({grouptreeData: datas, loading: false});
        });
    }
    onBranchBtnClick() {
        const {showAllbranch, isAccount} = this.state;
        let url = '/api/ec/api/hrm/branch';
        this.getLists(url, 1, true, this.defaultConditionParams, null, isAccount, !showAllbranch);
        this.setState({showAllbranch: !showAllbranch});
    }
    onAccountBtnClick() {
        const {searched, isAccount, activeKey, showAllbranch} = this.state;
        let url;
        let params = this.defaultConditionParams;
        if (searched) {
            url = '/api/ec/api/hrm/search';
            params = this.state.searchPara;
        }
        else if (activeKey === '1') {
        } else if (activeKey === '2') {
            url = '/api/ec/api/hrm/underling'  //同部门
        } else if (activeKey === '3') {
            url = '/api/ec/api/hrm/branch'  //我的下属
        } else if (activeKey === '4') { // 组织结构
            const {companyId} = this.state;
            this.getResourceTree(companyId, !isAccount);
        } else if (activeKey === '5'){
            this.getGrouptree(!isAccount);
        }
        if (searched || activeKey === '2' || activeKey === '3') {
            this.getLists(url, 1, true, params, null, !isAccount, showAllbranch);
        }
        this.setState({isAccount: !isAccount, currentPage: 1});
    }
    getLists(url, currentPage, hasPagination, searchPara = {}, companyId, isAccount, showAllbranch = false, activeKey) {
        this.setLoading(true);
        let params = {...searchPara};
        const {pageSize} = this.state;
        url = url || this.state.url;
        currentPage = currentPage || this.state.currentPage;
        hasPagination = hasPagination || this.state.hasPagination;
        if (companyId === undefined || companyId === null) {
            companyId = this.state.companyId;
        }
        if (hasPagination) {
            params['min'] = (currentPage - 1) * pageSize + 1;
            params['max'] = currentPage * pageSize;
        }
        if (isAccount) params['isNoAccount'] = 1;
        if (companyId) params['virtualtype'] = companyId;
        activeKey = activeKey || this.state.activeKey;
        if (showAllbranch && activeKey === '3') params['alllevel'] = 1;
        WeaTools.callApi(url,'GET', params).then((data)=> {
            data.datas.forEach((item)=> {
                item.icon = item.messagerurl;
                item.nodeid = `resource_${item.id}x`;
                item.type = 'resource'
            })
            this.setState({listDatas: data.datas, count: data.count, loading: false})
        })
    }
    handleCancel() {
        this.setState(initialState);
    }
    handleClear() {
        this.setState({
            isClearData: !this.state.isClearData
        });
        this.setState(initialState);
        this.props.onChange && this.props.onChange("", "");
    }
    onPageChange(page) {
        this.getLists('', page, true, this.state.searchPara, '', this.state.isAccount, this.state.showAllbranch);
        this.setState({currentPage: page});
        this.refs.weaSrcoll && this.refs.weaSrcoll.scrollerToTop();
    }
    onTabsChange(activeKey) {
        let url, companyId, hasPagination = true;
        const {resetFields, setFieldsValue} = this.props.form;
        this.setState({browserConditions: []});
        const {isAccount, showAllbranch} = this.state;
        resetFields(); // 重置高级搜索
        if (activeKey === '1') {
            url = '/api/ec/api/hrm/newly' // 最近
            companyId = null
        } else if (activeKey === '2') {
            url = '/api/ec/api/hrm/underling'  //同部门
            companyId = this.companysDefaultId
        } else if (activeKey === '3') {
            url = '/api/ec/api/hrm/branch'  //我的下属
            companyId = this.companysDefaultId
        } else if (activeKey === '4') { // 组织结构
            companyId = this.companysDefaultId
            this.getResourceTree(companyId, isAccount);
            hasPagination = false
        } else {
            companyId = null
            hasPagination = false
        }
        const quickSearch = this.state.quickSearch || 'name';
        this.setState({
            hasPagination,
            url,
            activeKey,
            companyId,
            count: 0,
            inputValue: this.defaultConditionParams[quickSearch] || '',
            currentPage: 1,
            searched: false,
            showSearchAd: false,
            companysDefault: this.companysDefault,
            browserConditions: this.browserConditionsBk,
            searchPara: this.defaultConditionParams,
        });
        setFieldsValue(this.defaultConditionParams);
        if (activeKey === '1' || activeKey === '2' || activeKey === '3') {
            this.getLists(url, 1, true, this.defaultConditionParams, 0, isAccount, showAllbranch, activeKey);
        }
        this.refs.weaSrcoll && this.refs.weaSrcoll.scrollerToTop();
    }
    showModal() {
        this.setState({
            visible: true
        });
    }
    onSelect(e){
        const {resourcetree} = this.props;
        const {activeKey, virtualDatas} = this.state;
        let _this = this;
        virtualDatas.forEach((item)=>{
            if(e.key == item.id){
                _this.setState({
                    companysDefault: item.virtualtype,
                    companyId: e.key
                });
                if (activeKey === '4') {
                    this.getResourceTree(e.key);
                } else {
                    this.getLists(null, 1, true, {}, e.key);
                }
            }
        })
    }
    onLeftListCheck(id, data){
        this.setData(data[0].id, data[0].lastname);
    }
    setShowSearchAd(bool){
        this.setState({showSearchAd:bool});
    }
    onBrowerClick =(v)=>{
        const {resetFields} = this.props.form;
        resetFields(); // 重置高级搜索
        this.setState(initialState);//初始化浏览框
        const {conditionURL} = this.props;

        WeaTools.callApi(conditionURL,'GET').then(data=>{
            let quickSearch;
            if (!isEmpty(data.conditions)) {
                data.conditions.forEach((item)=> {
                    if (item.isQuickSearch) {
                        quickSearch = item.domkey[0];
                    }
                })
            }
            const params = WeaTools.getParamsByConditions(data.conditions);
            let url = null, activeKey = '1', searched = false;
            if (!isEmpty(params)) {
                // 初始条件更新在form中
                const {setFieldsValue} = this.props.form;
                setFieldsValue(params);
                url = '/api/ec/api/hrm/search';
                activeKey = '';
                searched = true;
            }
            this.setState({
                browserConditions: data.conditions, quickSearch,
                inputValue: params[quickSearch] || '',
                searchPara: params, url, activeKey, searched});
            this.browserConditionsBk = data.conditions;
            this.defaultConditionParams = {...params};
            this.callApi();
            this.getLists(url,null,null,params,0,this.state.isAccount);
        });
        this.showModal();

    }
    onBrowerChange =(id,name)=>{
        this.throwCurrentState(id,name);
    }
    renderTab() {
        const {activeKey} = this.state;
        return (
            <Tabs className={this.props.is_012app?'searchPosition':''}
                onChange={this.onTabsChange.bind(this)}
                activeKey={activeKey}
            >
                <TabPane tab="最近" key="1"></TabPane>
                <TabPane tab="同部门" key="2"></TabPane>
                <TabPane tab="我的下属" key="3"></TabPane>
                <TabPane tab="按组织结构" key="4"></TabPane>
                <TabPane tab="常用组" key="5"></TabPane>
            </Tabs>
        );
    }
    topComponents() {
        const {inputValue,showSearchAd,activeKey} = this.state;
        const tab = this.renderTab();
        return (
            <WeaBrowserSearch
                buttonsAd={this.getTabButtonsAd()}
                addSelect={''}
                tabs={tab}
                searchType={['base','advanced']}
                searchsBaseValue={inputValue}
                searchsAd={<Form horizontal>{this.getSearchs()}</Form>}
                showSearchAd={showSearchAd}
                setShowSearchAd={(bool)=>{this.setShowSearchAd(bool)}}
                onSearchChange={debounce(this.onSearchChange, 200).bind(this)}
                onSearch={this.onSearchChange.bind(this)}
            />
        )
    }
    strToArray(str) {
        let r = [];
        if (str) r.push(str);
        return r;
    }
    render() {
        const {fieldName, icon,dropMenuIcon,iconBgcolor,title,browerType,dataUrl,recentUrl,sameDepUrl,mySubUrl,min,max,virtualUrl,searchUrl}=this.props;
        const {id,name,style,topPrefix,topIconUrl,otherPara,dataKey,isMult,ifCheck,disabled,prefix,ifAsync,grouptree,resourcetree,value,type} = this.props;
        const {loading,showDrop,activeKey,componyParam,showSearchAd,virtualDatas,grouptreeData,rsctreeData,isAccount,showAllbranch} = this.state;
        const {companyId, ids, names,seacrhPara,isChange,searched,listDatas,hasPagination,count,pageSize,currentPage,companysDefault, inputValue} = this.state;
        let titleEle = (
            <Row>
                <Col span="8" style={{paddingLeft:20,lineHeight:'48px'}}>
                    <div className="wea-workflow-hr-radio-title">
                        {iconBgcolor ?
                            <div className="wea-browser-single-icon-circle" style={{background: iconBgcolor}}>
                                <i className={icon}/>
                            </div>
                            : <span style={{verticalAlign:'middle',marginRight:10}}><i className={icon}/></span>
                        }
                        <span style={{verticalAlign:'middle'}}>{title}</span>
                    </div>
                </Col>
            </Row>
        );
        let menuItem = [];
        virtualDatas.map((item)=>{
            menuItem.push(<Menu.Item key={item.id}>{item.virtualtype}</Menu.Item>);
            menuItem.push(<Menu.Divider />)
        });

        let typeMenu = (
            <Menu className="wea-workflow-hr-radio-drop-menu"  onSelect={this.onSelect.bind(this)} selectedKeys={this.strToArray(companyId)}>
            {menuItem}
            </Menu>
        );
        let inputArr = (<WeaAssociativeSearch
                        {...this.props}
                        style={this.props.inputStyle}
                        isClearData={this.state.isClearData}
                        type={type}
                        ids={ids}
                        names={names}
                        clickCallback={this.onBrowerClick}
                        onChange={this.onBrowerChange}
                    />);
        const showList = activeKey === '1' || activeKey === '2' || activeKey === '3' || searched;
        let sheight = 435;
        if (hasPagination) sheight -= 35; //显示分页
        if (activeKey != "1" && activeKey != "5" && !searched) sheight -= 37; //显示维度

        return (
            <div>
               {inputArr}
               <input type="hidden" id={fieldName} name={fieldName} value={ids} />
               <Modal
                  wrapClassName="wea-workflow-hr-radio wea-browser-modal"
                  title={titleEle}
                  zIndex={this.props.zIndex}
                  // style={this.props.modalStyle}
                  width={784}
                  maskClosable={false}
                  visible={this.state.visible}
                  onCancel={this.handleCancel.bind(this)}
                  footer={[
                        <Button key="clear" type="ghost" size="large" onClick={this.handleClear.bind(this)}>清 除</Button>,
                        <Button key="back" type="ghost" size="large" onClick={this.handleCancel.bind(this)}>取 消</Button>,
                        ]}
                >
                    {this.topComponents()}
                    {activeKey != "1" && activeKey != "5" && !searched &&
                    <Row>
                        <Col span={24}>
                             <Dropdown overlay={typeMenu} trigger={['click']}>
                                <a className="ant-dropdown-link" href="javascript:;">
                                  {dropMenuIcon}{companysDefault} <Icon type="down" style={{float:"right"}}/>
                                </a>
                              </Dropdown>
                        </Col>
                    </Row>}
                    {
                        activeKey != "1" &&
                        <div className="operation-wrap" style={{top: (searched || activeKey == "5")? 110: 142}}>
                            {
                                activeKey == "3" &&
                                <Button size="small" onClick={this.onBranchBtnClick.bind(this)} title={'显示下级下属'}>
                                    <span>
                                        {showAllbranch?
                                            <i className='icon-coms-DisplaySubordinates' style={{color: '#2db7f5'}}/>
                                            :<i className='icon-coms-NoDisplayOfSubordinates'/>}</span>
                                </Button>
                            }
                            <Button size="small" onClick={this.onAccountBtnClick.bind(this)} title={isAccount?'隐藏无账号人员':'显示无账号人员'}>
                                <span>{isAccount?
                                    <i className='icon-coms-ShowNoAccount' style={{color: '#2db7f5'}}/>
                                    :<i className='icon-coms-NoAccountNoDisplay'/>}</span>
                            </Button>
                        </div>
                    }
                    <Spin spinning={loading}>
                        <WeaNewScroll height={sheight} ref="weaSrcoll">
                            {showList &&
                                <WeaHrmList
                                    onClick={this.onLeftListCheck.bind(this)}
                                    datas={listDatas}
                                />
                            }
                            {!showList && activeKey == "4" &&
                                <WeaTreeHr dataUrl={resourcetree} recentUrl={recentUrl}
                                    isAccount={isAccount}
                                    virtualtype={this.state.companyId}
                                    treeKey="hr"
                                    topPrefix={topPrefix}
                                    checkable={false}
                                    id={id}
                                    isMult={isMult}
                                    ifAsync={ifAsync}
                                    ifCheck={ifCheck}
                                    setData={this.setData.bind(this)}
                                    ids={ids}
                                    names={names}
                                    treeData={rsctreeData}
                                    defaultExpandedKeys={this.defaultExpandedKeys(rsctreeData && rsctreeData.datas)}
                                />}
                            {!showList && activeKey == "5" &&
                                <WeaTreeHr dataUrl={grouptree} recentUrl={recentUrl}
                                    isAccount={isAccount}
                                    treeKey="hr"
                                    topPrefix={topPrefix}
                                    checkable={false}
                                    id={id}
                                    isMult={isMult}
                                    ifAsync={ifAsync}
                                    ifCheck={ifCheck}
                                    setData={this.setData.bind(this)}
                                    ids={ids}
                                    names={names}
                                    treeData={grouptreeData}
                                    defaultExpandedKeys={this.defaultExpandedKeys(grouptreeData && grouptreeData.datas)}
                                />}
                        </WeaNewScroll>
                        {
                            hasPagination &&
                            <Pagination
                            weaSimple
                            showTotal={total => `共${total}条`}
                            total={count}
                            pageSize={pageSize}
                            current={currentPage}
                            onChange={this.onPageChange.bind(this)}/>
                        }
                    </Spin>
               </Modal>
            </div>
        )
    }
    throwCurrentState(id,name){
        const data = [];
        if (id || name) {
            data.push({
                id: id,
                name: name,
            })
        }
        this.props.onChange && this.props.onChange(id, name, data);
    }
    setData(ids, names) {
        this.throwCurrentState(ids,names);
        if (ids) WeaTools.callApi('/api/ec/api/hrm/newly', 'post', {ids: ids});
        this.setState({
            ids:ids,
            names:names,
            visible:false,
        });
    }
    onSearchChange(v){//按照人员姓名搜索
        const quickSearch = this.state.quickSearch || 'name';
        const {setFieldsValue,validateFields} = this.props.form;
        let params = {};
        validateFields((errors, object) => {
            if (!!errors) {
                message.error('Errors in form!!!');
                return;
            }
            forEach(object, (value, key) => {
                if (value) params[key] = value;
            })
        });
        params[quickSearch] = v;
        const url = '/api/ec/api/hrm/search';
        this.getLists(url, 1, true, params, 0, this.state.isAccount); // 搜索获取数据
        setFieldsValue(params); // 高级搜索数据联动
        this.setState({
            searched: true,
            hasPagination: true,
            inputValue: v,
            url,
            showSearchAd: false,
            searchPara: params,
            activeKey: '',
        });
        this.refs.weaSrcoll && this.refs.weaSrcoll.scrollerToTop();
    }
    goSearch=()=>{
        const quickSearch = this.state.quickSearch || 'name';
        const {validateFields} = this.props.form;
        let params = {};
        validateFields((errors, object) => {
            if (!!errors) {
                message.error('Errors in form!!!');
                return;
            }
            forEach(object, (value, key) => {
                if (value) params[key] = value;
            })
        });
        const url = '/api/ec/api/hrm/search';
        this.getLists(url, 1, true, params, 0, this.state.isAccount); // 搜索获取数据
        this.setState({
            hasPagination: true,
            searchPara: params,
            showSearchAd: false,
            searched: true,
            inputValue: params[quickSearch] || '',
            activeKey: '',
            url
        });
        this.refs.weaSrcoll && this.refs.weaSrcoll.scrollerToTop();
    }
    getSearchs(){
        const {browserConditions} = this.state;
        let items = [];
        !isEmpty(browserConditions)&& browserConditions.forEach( field => {
            items.push({
                com:(<FormItem
                    label={`${field.label}`}
                    labelCol={{span: `${field.labelcol}`}}
                    wrapperCol={{span: `${field.fieldcol}`}}>
                        {WeaTools.getComponent(field.conditionType, field.browserConditionParam, field.domkey,this.props, field)}
                    </FormItem>),
                colSpan: 2,
                hide: WeaTools.isComponentHide(field),
            });
        });
        return <WeaSearchBrowserBox  items={items} />
    }
    getTabButtonsAd() {
        const {resetFields,validateFields} = this.props.form;
        return [
            (<Button type="primary" onClick={()=>{this.goSearch()}}>搜索</Button>),
            (<Button type="primary" onClick={(e)=>{e.preventDefault(); this.reset();}}>重置</Button>),
            (<Button type="ghost" onClick={()=>{this.setState({showSearchAd:false})}}>取消</Button>)
        ]
    }
    reset() {
        const {resetFields,setFieldsValue} = this.props.form;
        const {browserConditions} = this.state;
        const fields = {};
        browserConditions.forEach((c) => {
            let ct = c.conditionType.toUpperCase();
            let domkey = c.domkey[0];
            const filterKeys = [1, -1, '1', '-1'];
            if (ct === 'BROWSER') {
                if (!isEmpty(c.browserConditionParam) && filterKeys.indexOf(c.browserConditionParam.viewAttr) == -1) {
                    fields[domkey] = undefined;
                }
            } else {
                if (filterKeys.indexOf(c.viewAttr) == -1) {
                    fields[domkey] = undefined;
                }
            }
        })
        setFieldsValue(fields);
        this.setState({inputValue: ''});
    }
    defaultExpandedKeys(data){
        let arr = [];
        const {topPrefix} = this.props;
        const loop = data => data.forEach((item) => {
            if (item.children && !!item.open) {
               arr.push(`${topPrefix}0-${item.nodeid}`);
               loop(item.children);
            }else{
                !!item.open && arr.push(item.nodeid);
            }

        });
        !!data.open && arr.push(`${topPrefix}0-${data.nodeid}`);
        if(data.children){loop(data.children)}
        return arr;
    }
}
const formatSelectOptions = (options) => {
    let results = [];
    forEach(options, (option) => {
        results.push({
            value: option.id,
            name: option.name ? option.name : option.virtualtype
        })
    })
    return results;
}

export default Form.create()(WeaWorkflowHrRadio);
