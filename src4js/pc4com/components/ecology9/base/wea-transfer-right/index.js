import {Tree} from 'antd';
// import Tree from '../../../_antd1.11.2/tree'
import WeaInputSearch from '../../wea-input-search';
const TreeNode = Tree.TreeNode;
import WeaScroll from '../../wea-scroll';
import WeaNewScroll from '../../wea-new-scroll';
import cloneDeep from 'lodash/cloneDeep';
import isArray from 'lodash/isArray';
import trim from 'lodash/trim';
import PureRenderMixin from 'react-addons-pure-render-mixin';
let timeout = null;
export default class main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            key: ''
        }
    }
    shouldComponentUpdate(...args) {
        return PureRenderMixin.shouldComponentUpdate.apply(this, args);
    }

    count(item) {
        if (item.type === 'all') return item.count;
        return (item.users || []).length;
    }
    reset() {
        this.setState({key: ''});
    }
    generateTreeNodes() {
        const {data} = this.props;
        const {key} = this.state;
        const treeNodes = [];
        let showData = [...data];
        if (trim(key)) {
            showData = data.filter((item)=> {
                let bool = false;
                bool = item.lastname.indexOf(trim(key)) > -1;
                if (!bool && item.pingyinname) {
                    bool = item.pingyinname.indexOf(trim(key)) > -1;
                }
                return bool;
            })
        }
        this.nodeIds = [];
        this.nodeObj = {};
        showData.map((item) => {
            let title=null;
            let crmNames = [];
            if (item.departmentname) crmNames.push(item.departmentname);
            if (item.supsubcompanyname) crmNames.push(item.supsubcompanyname);
            if (item.subcompanyname) crmNames.push(item.subcompanyname);
            item.crmNames = crmNames.join('／');

            if(item.type == "subcom" || item.type == "com"){
                title = <div className="tree-title">
                            <i className="icon-process-scene"/><span title={item.lastname}>{item.lastname}</span>({this.count(item)})
                        </div>;
            }else if(item.type == "dept"){
                title = <div className="tree-title">
                            <i className="icon-mail-folder"/> <span title={item.lastname}>{item.lastname}</span>({this.count(item)})
                        </div>;
            }else if(item.type == "resource"){
                title = <div className="tree-title resource">
                            <img className="wea-tree-hr-portrait" src={item.icon} alt="img"/>
                            <div className="resource-content">
                                <div>
                                    <span className="lastname" title={item.lastname}>{item.lastname}</span>
                                    <span className="jobtitlename gray" title={item.jobtitlename}>{item.jobtitlename}</span>
                                </div>
                                <div className="overflow gray overflow" title={item.crmNames}>
                                    {item.crmNames}
                                </div>
                            </div>
                        </div>
            }else{
                title = <div className="tree-title">
                            <i className="icon-process-scene"/><span title={item.lastname}>{item.lastname}</span>({this.count(item)})
                        </div>
            }
            treeNodes.push(
                <TreeNode title={title}
                    name={item.lastname}
                    jobtitlename={item.jobtitlename}
                    key={item.nodeid}
                    isLeaf={true}
                    id={item.id}
                    type={item.type}
                />
            )
            this.nodeIds.push(item.nodeid);
            this.nodeObj[item.nodeid] = item;
        })
        return treeNodes;
    }
    onSearchChange(v) {
        this.setState({key: v});
    }
    checkHandler(v, p) {
        let _this = this;
        // 取消上次延时未执行的方法
        clearTimeout(timeout);
        //执行延时
        timeout = setTimeout(function(){
            _this.props.checkedCb && _this.props.checkedCb(v);
        },200);
    }
    onDoubleClick(key) {
        clearTimeout(timeout);
        this.props.onDoubleClick && this.props.onDoubleClick(key);
    }
    onDragStart(obj) {
        clearTimeout(timeout);
        this.props.checkedCb && this.props.checkedCb([]);
    }
    onDrop(obj) {
        const dragNodes = obj.dragNodesKeys;
        const targetNode = obj.node.props.eventKey;
        const result = [];
        this.nodeIds.filter((item) => {
            return dragNodes.indexOf(item) === -1;
        }).forEach((nodeid) => {
            if (nodeid === targetNode) {
                dragNodes.forEach((drag)=> {
                    result.push(this.nodeObj[drag]);
                })
            }
            result.push(this.nodeObj[nodeid]);
        })
        this.props.onDrag && this.props.onDrag(result);
    }
    render() {
        const {checkedKeys, height} = this.props;
        const treeNodes = this.generateTreeNodes();
        return (
            <div className="wea-transfer-right">
                <WeaInputSearch onSearchChange={_.debounce(this.onSearchChange,20).bind(this)} placeholder="请输入关键字搜索" value={this.state.key}/>
                <WeaNewScroll height={height || 400}>
                    <Tree className="transfer-tree"
                        draggable
                        onDragStart={this.onDragStart.bind(this)}
                        onDrop={this.onDrop.bind(this)}
                        onSelect={this.checkHandler.bind(this)}
                        onDoubleClick={this.onDoubleClick.bind(this)}
                        selectedKeys={checkedKeys}
                        multiple={true}
                        async={true}
                        selectable={true}
                    >
                        {treeNodes}
                    </Tree>
                </WeaNewScroll>
            </div>
        )
    }
}