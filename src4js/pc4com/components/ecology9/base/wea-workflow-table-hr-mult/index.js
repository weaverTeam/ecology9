import {Row, Col, Input, Modal, Tooltip, Tree, Table, Tabs, Pagination, Select, Menu, Button, Spin} from 'antd';
// import Tree from '../../../_antd1.11.2/Tree';
// import Row from '../../../_antd1.11.2/Row';
// import Col from '../../../_antd1.11.2/Col';
// import Modal from '../../../_antd1.11.2/Modal';
// import Tooltip from '../../../_antd1.11.2/Tooltip';
// import Tabs from '../../../_antd1.11.2/Tabs';
// import Table from '../../../_antd1.11.2/Table';
const TabPane = Tabs.TabPane;
const TreeNode = Tree.TreeNode;
// import Select from '../../../_antd1.11.2/Select';
const Option = Select.Option;
// import Menu from '../../../_antd1.11.2/menu';
// import Button from '../../../_antd1.11.2/Button';
// import Input from '../../../_antd1.11.2/Input';
// import Spin from '../../../_antd1.11.2/Spin';
// const  _  =  require('lodash');
import isArray from 'lodash/isArray'
import WeaScroll from '../../wea-scroll';
import WeaInputSearch from '../../wea-input-search';
import * as util from './util';

export default class WeaWorkflowTalbeHr extends React.Component {
    constructor(props) {
        super(props);
        const {url,isaccount,min,max,componyParam,seacrhPara} = props;
        // console.log("url",url);
        this.state = {
            url:`${url}?isaccount=${isaccount}&min=${min}&max=${max}${!!seacrhPara ? `${seacrhPara}` : ''}`,
            showDrop: false,
            loading: true,
            datas:[],
            dataSource:[],
            newdataSource:[],
            listDatas:[],
            listColumns:[],
            record:{},
            selectedRowKeys:[],
            selectedRows:[],
            id:"",
            name:'',
        }
    }
    getApiData = (data)=>{
        let dataArray = new Array();
        data && data.datas.forEach(item=>{
            dataArray.push({
                key:item.id,
                lastname:item.lastname,
                jobtitlename:item.jobtitlename,
                pinyinlastname:item.pinyinlastname,
                messagerurl:<img className='wea-tree-hr-portrait'  src={item.messagerurl} alt="头像"/>,
                icon: item.messagerurl,
                id: item.id,
                nodeid: `resource_${item.id}`,
                type: 'resource',
            })
        });
        return dataArray || [];
    }
    filterRightDatas = (dataSource,filterData)=>{
        //过滤左侧数据
        let newdataSource=[];
        if(this.props.mult){
            if(isArray(filterData) && filterData.length>0){
                newdataSource = dataSource.filter(item=>!filterData.some(m=>m.id === item.key));
            }else{
                newdataSource = dataSource;
            }
        }
        return newdataSource;
    }
    callBack = d =>{
        if(typeof this.props.leftDataSource === 'function'){
            console.log('d',d);
            this.props.leftDataSource(d);
        }
    }
    componentDidMount() {
        let {url}= this.state;
        // console.log("url",url);
        url = `${url}&${new Date().getTime()}=`;
        util.getDatas(url,"GET").then(data => {//,{isaccount:isaccount,min:min,max:max}
            // console.log("data",data);
            const dataArray = this.getApiData(data);
            const newData = this.filterRightDatas(dataArray,this.props.filterData);
            // console.log('dataArray',dataArray,'newData',newData);
            this.callBack(newData);
            this.setState({
                loading:false,
                dataSource:dataArray,
                newdataSource:newData,
            })
        })
    }
    componentWillReceiveProps(nextProps) {
        // console.log("nextProps",nextProps);
        const {url,isaccount,min,max,componyParam,seacrhPara,hasCheckedDatas}=this.props;
        if(this.props.componyParam != nextProps.componyParam || seacrhPara !== nextProps.seacrhPara){
            // let newUrl = url + nextProps.componyParam;
            let newUrl = `${url}?isaccount=${isaccount}&min=${min}&max=${max}${nextProps.componyParam?nextProps.componyParam:''}${nextProps.seacrhPara ? `${nextProps.seacrhPara}`:''}`;
            newUrl = `${newUrl}&${new Date().getTime()}=`;
            util.getDatas(newUrl,"GET").then(data => {
                // console.log("data2",data);
                const dataArray = this.getApiData(data);
                const newData = this.filterRightDatas(dataArray,this.props.filterData);
                this.callBack(newData);
                this.setState({
                    dataSource:dataArray,
                    newdataSource:newData,
                })
            });
        }
        if(this.props.filterData !== nextProps.filterData){
            const newData = this.filterRightDatas(this.state.dataSource,nextProps.filterData);
            this.callBack(newData);
            this.setState({
                newdataSource:newData,
            })
        }
    }
    onChange(activeKey) {
        this.setState({activeKey});
    }
    onRowClick(record, index){
        // console.log("record",record,"index",index);
        const{id,name,selectedRowKeys,selectedRows} = this.state;
        const {checkedKeys}=this.props;
        // console.log('==========',checkedKeys);
        if (typeof this.props.setData === "function") {
            this.props.setData(record.key,record.lastname);
        }
        this.setState({
            id:record.key,
            name:record.lastname,
        })
        let newselectedRowKeys = [...checkedKeys];
        let newSelectedRows = [...selectedRows];
        if(!newselectedRowKeys.includes(record.key)){
            newselectedRowKeys.push(record.key);
            newSelectedRows.push(record);
        }
        if(typeof this.props.onChange === 'function'){
            this.props.onChange(newselectedRowKeys,newSelectedRows);
        }
    }
    onRowDoubleClick(record, index) {
        this.props.onRowDoubleClick && this.props.onRowDoubleClick([record]);
    }
    render() {
        const {icon,iconBgcolor,title,allWf,bookMarkWf, browerType,listUrl,countUrl,otherPara,} = this.props;
        const {showDrop,activeKey,id,name,newdataSource,dataSource} = this.state;
        // console.log('render==selectedRowKeys',this.state.selectedRowKeys);
        // console.log('===========',dataSource,newdataSource);
        const pagination = {
          total: dataSource.length || 0,
          showSizeChanger: true,
          onShowSizeChange(current, pageSize) {
            // console.log('Current: ', current, '; PageSize: ', pageSize);
          },
          onChange(current) {
            // console.log('Current: ', current);
          },
        };
        const columns = [{
          title: '头像',
          dataIndex: 'messagerurl',
          key:"messagerurl",
          width: this.props.mult?'15%':100,
          render: (text) => <div className="wea-table-hr-portrait">{text}</div>
        }, {
          title: '姓名',
          dataIndex: 'lastname',
          key:"lastname",
          width: this.props.mult?'25%':100,

        },{
          title: '职位',
          dataIndex: 'jobtitlename',
          key:"jobtitlename",
          width: this.props.mult?'50%':100,
        }];

        let _this = this;
        const checkedKeys = isArray(this.props.checkedKeys) ? this.props.checkedKeys : [];
        // console.log('checkedKeys',checkedKeys);
        const rowSelection = {
          type:'checkbox ',
          selectedRowKeys:checkedKeys,
          onChange(selectedRowKeys, selectedRows) {
            // console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            if(typeof _this.props.onChange === 'function'){
                _this.props.onChange(selectedRowKeys,selectedRows);
                // _this.setState({selectedRowKeys:checkedKeys});
            }
          },
          onSelect(record, selected, selectedRows) {
            // console.log(record, selected, selectedRows);
          },
          onSelectAll(selected, selectedRows, changeRows) {
            // console.log(selected, selectedRows, changeRows);
          },
        };
        let hasRowSelection = {rowSelection:null};
        this.props.mult && (hasRowSelection = {rowSelection:rowSelection});
        const size = this.props.size === 'small' ?'small':'default';
        // console.log('newdataSource',newdataSource);
        return  (<div>
                    <Table className="wea-table-hr"
                        showHeader={false}
                        // onRowClick={this.onRowClick}
                        onRowDoubleClick={this.onRowDoubleClick.bind(this)}
                        columns={columns}
                        dataSource={this.props.mult? newdataSource:dataSource}
                        pagination={!this.props.mult?pagination:false}
                        {...hasRowSelection}
                        size={size}
                    />
                </div>)

    }
}

