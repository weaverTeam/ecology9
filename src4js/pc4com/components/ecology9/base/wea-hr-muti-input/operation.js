import React, { PropTypes } from 'react';
import {Button, Icon} from 'antd';
// import Button from '../../../_antd1.11.2/Button';
// import Icon from '../../../_antd1.11.2/Icon';

function noop() {
}

export default class TransferOperation extends React.Component {
  static defaultProps = {
    leftArrowText: '',
    rightArrowText: '',
    moveToLeft: noop,
    moveToRight: noop,
  }

  static propTypes = {
    className: PropTypes.string,
    leftArrowText: PropTypes.string,
    rightArrowText: PropTypes.string,
    moveToLeft: PropTypes.func,
    moveToRight: PropTypes.func,
  }

  render() {
    const {
      moveToLeft,
      moveToRight,
      leftArrowText,
      rightArrowText,
      leftActive,
      rightActive,
      className,
      leftAllActive,
      moveAllToLeft,
      rightAllActive,
      moveAllToRight,
      is_012app
    } = this.props;

    const moveToLeftButton = (
      <Button type="primary" size="small" disabled={!rightActive} onClick={moveToRight}>

        <span>{is_012app ? <Icon type="right" style={{ marginLeft:"50%"}}/> : <i className='icon-coms-Browse-box-Add-to'/>}</span>
      </Button>
    );
    const moveToRightButton = (
      <Button type="primary" size="small" disabled={!leftActive} onClick={moveToLeft}>
        <span>{is_012app ? <Icon type="left" style={{ marginLeft:"50%"}}/> :<i className='icon-coms-Browse-box-delete'/>}</span>
      </Button>
    );

    const moveAllToLeftButton = (
      <Button type="primary" size="small" disabled={!leftAllActive} onClick={moveAllToRight}>
        <span>{is_012app ? <Icon type="double-right" style={{ marginLeft:"50%"}}/> :<i className='icon-coms-Browse-box-add-all'/>}</span>
      </Button>
    );
    const moveAllToRightButton = (
      <Button type="primary" size="small" disabled={!rightAllActive} onClick={moveAllToLeft}>
        <span>{is_012app ? <Icon type="double-left" style={{ marginLeft:"50%"}}/> :<i className='icon-coms-Browse-box-Delete-all'/>}</span>
      </Button>
    );
    return (
      <div className={className}>
        {moveToLeftButton}
        {moveToRightButton}
        {moveAllToLeftButton}
        {moveAllToRightButton}
      </div>
    );
  }
}
