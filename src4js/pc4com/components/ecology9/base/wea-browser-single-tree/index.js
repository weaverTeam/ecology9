import {Row, Col, Modal, Tooltip, Menu, Button, Dropdown, Icon} from 'antd';
// import Row from '../../../_antd1.11.2/Row';
// import Col from '../../../_antd1.11.2/Col';
// import Modal from '../../../_antd1.11.2/Modal';
// import Tooltip from '../../../_antd1.11.2/Tooltip';
// import Menu from '../../../_antd1.11.2/menu';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
// import Button from '../../../_antd1.11.2/Button';
// import Dropdown from '../../../_antd1.11.2/Dropdown';
// import Icon from '../../../_antd1.11.2/Icon';

import WeaTools from '../../wea-tools';
import WeaInput from '../../wea-input';
import WeaNewScroll from '../../wea-new-scroll';
import WeaBrowserTree from './WeaBrowserTree';
import isEqual from 'lodash/isEqual';

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: true,
            loading: false,
            listDatas:[],
            companysDefault:'',
            componyParam:'',
            showSearchAd:false,
            pagination: {},
            quickSearchPara:'',
            treeData: props.treeDatas,
            defaultExpandedKeys: props.defaultExpandedKeys,
        }
        const d = {};
        if (props.id && props.name) {
            d.id = props.id;
            d.name = props.name;
        }
        this.state.checkedData = d;
    }
    componentDidMount(){
        const {showDropMenu} = this.props;
        showDropMenu && this.fetchTreeDatas();
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.id != nextProps.id && this.props.name != nextProps.name) {
            let d = {};
            if (nextProps.id && nextProps.name) {
                d.id = nextProps.id;
                d.name = nextProps.name;
            }
            this.setState({checkedData: d});
        }
        if (!isEqual(this.props.treeDatas, nextProps.treeDatas)) {
            this.setState({treeData: nextProps.treeDatas || []})
        }
    }
    companysDefault(data){
        for (let i=0,leng=data.companys.length;i<leng;i++) {
            if(Number(data.companys[i].companyid) >= 0){
                return data.companys[i].name;
            }
       }
    }
    fetchTreeDatas=()=>{
        const {url} = this.props;
        const {showDropMenu,topPrefix} = this.props;
        let keysArr = [];
        if (url) {
            WeaTools.callApi(url,'GET').then(datas => {
                // console.log('部门分部树datas:',datas,_this.companysDefault(datas));
                keysArr.push(`${topPrefix}0-${datas.datas.id}`);//默认展开一级树节点
                if(showDropMenu){
                    this.setState({
                        companysDefault:this.companysDefault(datas),
                        listDatas:this.getWfList(datas.companys),
                        treeData:datas.datas,
                        defaultExpandedKeys:keysArr,
                    });
                }else{
                     this.setState({
                        treeData:datas.datas,
                        defaultExpandedKeys:keysArr,
                    });
                }
            });
        }
    }
    onSelect(e){
        // console.log("onSelect",e);
        const {url} = this.props;
        let  _this = this;
        this.state.listDatas.forEach((item)=>{
            if(e.key == item.companyid){
                this.setState({
                    companysDefault:item.name,
                    componyParam:e.key=="1" ? "" : "&virtualCompanyid="+e.key,//是虚拟维度才传参
                })
            }
        })
        let param = e.key=="1" ? "" : "&virtualCompanyid="+e.key;//是虚拟维度才传参
        WeaTools.callApi(url+param,'GET').then(datas => {
            // console.log("onSelect==datas:",datas);
            let keysArr = [];
            // datas.result.rootCompany.subOrgs.map(data=>{
            //     data.id && keysArr.push(data.id);
            // });
            _this.setState({
                treeData:datas.datas,
                // defaultExpandedKeys:keysArr,
            });
         });
    }
    render(){
        const {isLoadAll,contentHeight, url, hasAdvanceSerach, tabs, type,icon,style,topPrefix,disabled,iconBgcolor,title,browerType,dataUrl,showSearchArea,showDropMenu,singleTabName} = this.props;
        const {activeKey,datas,componyParam,showSearchAd,defaultExpandedKeys,treeData,companysDefault, checkedData} = this.state;
        let menuItem = [];
        this.state.listDatas.map((item)=>{
            menuItem.push(<Menu.Item key={item.companyid}>{item.name}</Menu.Item>);
            menuItem.push(<Menu.Divider />)
        });
        const typeMenu = (
            <Menu className="wea-browser-drop-menu" onSelect={this.onSelect.bind(this)}>
            {menuItem}
            </Menu>
        );
        let cHeight = contentHeight;
        if (hasAdvanceSerach || tabs) cHeight -= 48;
        if (showDropMenu) cHeight -= 37;
        return(
            <div>
                {showDropMenu  &&
                 (<Row>
                    <Col span={24}>
                        <Dropdown overlay={typeMenu} trigger={['click']}>
                            <a className="ant-dropdown-link" href="javascript:;">
                              <i className={icon}/>{companysDefault} <Icon type="down" style={{float:"right"}}/>
                            </a>
                        </Dropdown>
                    </Col>
                 </Row>)}
                <WeaNewScroll height={cHeight}>
                    <WeaBrowserTree
                        clickNodeExpandChildren
                        isLoadAll={isLoadAll}
                        hasRadio={true}
                        type={type}
                        title={title}
                        browserTree={false}
                        checkable={true}
                        topPrefix={topPrefix}
                        dataUrl={url}
                        componyParam={componyParam}
                        setRadioData={this.setData.bind(this)}
                        icon={icon}
                        treeData={treeData}
                        defaultExpandedKeys={defaultExpandedKeys}
                        radioCheckedData={checkedData}
                        />
                </WeaNewScroll>
            </div>
        );
    }
    setData(ids,names,data){
        if(typeof this.props.setData === "function"){
            this.props.setData(ids,names,data);
            let d = {}
            if (ids && names) {
                d.id = ids;
                d.name = names;
            }
            this.setState({checkedData: d});
        }
    }
    getWfList(datas){
        let arr = new Array();
        for (let i=0;i<datas.length;i++) {
            const data = datas[i];
                arr.push({
                    companyid:data.companyid,
                    name: data.name,
                    isVirtual:data.isVirtual,
                });
            }
        return arr;
    }
}
export default Main;
