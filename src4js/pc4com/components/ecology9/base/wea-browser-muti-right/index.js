 import {Tree, Icon} from 'antd';
// import Tree from '../../../_antd1.11.2/tree';
import WeaInputSearch from '../../wea-input-search';
const TreeNode = Tree.TreeNode;
// import Icon from '../../../_antd1.11.2/icon';
import WeaScroll from '../../wea-scroll';
import WeaNewScroll from '../../wea-new-scroll';
import cloneDeep from 'lodash/cloneDeep';
import isArray from 'lodash/isArray';
import trim from 'lodash/trim';
import classNames from 'classnames';
let timeout = null;
export default class main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            key: ''
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.data !== nextProps.data
            || this.state.key !== nextState.key
            || this.props.checkedKeys !== nextProps.checkedKeys;
    }
    count(item) {
        if (item.type === 'all') return item.count;
        return (item.users || []).length;
    }
    generateTreeNodes() {
        const {data, inputId, inputName, inputTop, inputBottom} = this.props;
        const {key} = this.state;
        const treeNodes = [];
        let showData = [...data];
        if (inputId) {
            if (trim(key)) {
                showData = showData.filter((item)=> {
                    return encodeURI(item[inputTop[0]]).indexOf(encodeURI(trim(key))) > -1;
                })
            }
            this.nodeIds = [];
            this.nodeObj = {};
            showData.map((item) => {
                let top = [];
                let bottom = [];
                inputTop && inputTop.forEach((field)=> {
                    top.push(<span style={{marginRight: '5px'}} dangerouslySetInnerHTML={{__html: item[field + 'span'] || item[field]}}></span>)
                })
                inputBottom && inputBottom.forEach((field)=> {
                    bottom.push(<span style={{marginRight: '5px'}} dangerouslySetInnerHTML={{__html: item[field + 'span'] || item[field]}}></span>)
                })
                let title = (
                    <div>
                        <div className="item-wrap" style={{whiteSpace: 'normal'}}>
                            <div className="item-top">
                                {top}
                            </div>
                            <div className="item-bottom">
                                {bottom}
                            </div>
                        </div>
                    </div>
                );
                treeNodes.push(
                    <TreeNode title={title}
                        key={item[inputId]}
                        isLeaf={true}
                    />
                )
                this.nodeIds.push(item[inputId]);
                this.nodeObj[item[inputId]] = item;
            })
        }
        return treeNodes;
    }
    onSearchChange(v) {
        this.setState({key: v});
    }
    reset() {
        this.setState({key: ''});
    }
    checkHandler(v, p) {
        let _this = this;
        // 取消上次延时未执行的方法
        clearTimeout(timeout);
        //执行延时
        timeout = setTimeout(function(){
            _this.props.checkedCb && _this.props.checkedCb(v);
        },200);
    }
    onDoubleClick(key) {
        clearTimeout(timeout);
        this.props.onDoubleClick && this.props.onDoubleClick(key);
    }
    onDragStart(obj) {
        clearTimeout(timeout);
        this.props.checkedCb && this.props.checkedCb([]);
    }
    onDrop(obj) {
        const dragNodes = obj.dragNodesKeys;
        const targetNode = obj.node.props.eventKey;
        const result = [];
        this.nodeIds.filter((item) => {
            return dragNodes.indexOf(item) === -1;
        }).forEach((id) => {
            if (id === targetNode) {
                dragNodes.forEach((drag)=> {
                    result.push(this.nodeObj[drag]);
                })
            }
            result.push(this.nodeObj[id]);
        })
        this.props.onDrag && this.props.onDrag(result);
    }
    render() {
        const {checkedKeys, height} = this.props;
        const treeNodes = this.generateTreeNodes();
        return (
            <div className="wea-transfer-right">
                <WeaInputSearch onSearchChange={_.debounce(this.onSearchChange,200).bind(this)} placeholder="请输入关键字搜索" value={this.state.key} />
                    <div>
                        <WeaNewScroll height={ height || 400}>
                        <Tree className="transfer-tree"
                            draggable
                            onDragStart={this.onDragStart.bind(this)}
                            onDrop={this.onDrop.bind(this)}
                            onSelect={this.checkHandler.bind(this)}
                            onDoubleClick={this.onDoubleClick.bind(this)}
                            selectedKeys={checkedKeys}
                            multiple={true}
                            async={true}
                            selectable={true}
                        >
                            {treeNodes}
                        </Tree>
                    </WeaNewScroll>
                    </div>
            </div>
        )
    }
}