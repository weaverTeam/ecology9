import {Input, Icon, Tree} from 'antd';
// import Icon from '../../../_antd1.11.2/icon'
// import Input from '../../../_antd1.11.2/input'
// import Tree from '../../../_antd1.11.2/tree'
const TreeNode = Tree.TreeNode;
import PureRenderMixin from 'react-addons-pure-render-mixin';
import cloneDeep from 'lodash/cloneDeep'
import isArray from 'lodash/isArray'
import WeaTools from '../../wea-tools'

let that = {};
export default class main extends React.Component {
    constructor(props) {
        super(props);
        const {dataUrl,otherPara,prefix,topPrefix,dataKey,isMult,ids,names,defaultExpandedKeys} = props;
        let idArr = ids?ids.split(","):[];
        let nameArr = names?names.split(","):[];
         for(let i=0;i<idArr.length;i++) {
            idArr[i] = prefix+idArr[i];
        }
        // console.log("idArr:",idArr);
        this.state = {
            treeData: [],
            prefix:prefix,
            dataKey:dataKey,
            topPrefix:topPrefix,
            isMult:isMult,
            ids:idArr,
            names:nameArr,
            expandedKeys:defaultExpandedKeys,
            defaultExpandedKeys:defaultExpandedKeys
        }
        that = this;
    }
    componentDidMount() {
        const{treeData,} = this.props;
        generateData(treeData.datas);
        this.setState({
            treeData: objToArray(treeData.datas),
        });

    }
    shouldComponentUpdate(...args) {
        return PureRenderMixin.shouldComponentUpdate.apply(this, args);
    }
    componentWillReceiveProps(nextProps) {
        const{treeData,componyParam,defaultExpandedKeys} = this.props;
        if(this.props.treeData !== nextProps.treeData){
            // console.log("componentWillReceiveProps=====treeData",nextProps.treeData);
            generateData(nextProps.treeData.datas);
            this.setState({
                expandedKeys:nextProps.defaultExpandedKeys,
                treeData: objToArray(nextProps.treeData.datas),
            });
        }
        if(defaultExpandedKeys !== nextProps.defaultExpandedKeys){
            // console.log("nextProps.defaultExpandedKeys",nextProps.defaultExpandedKeys);
            this.setState({defaultExpandedKeys:nextProps.defaultExpandedKeys})
        }
    }
    onExpandChange(expandedKeys, nodeObj){
        this.setState({expandedKeys:expandedKeys})
    }
    onLoadData(treeNode) {
        let _this = this;
        const {dataUrl, isAccount, virtualtype, checkStrictly = false} = this.props;
        const {eventKey,treeKey,id,type} = treeNode.props;
        const params = {
            type: type,
            id: id
        }
        if (isAccount) params['isNoAccount'] = 1;
        if (virtualtype) params['virtualtype'] = virtualtype;
        if (!checkStrictly) params['alllevel'] = 1;
        return new Promise((resolve)=> {
            WeaTools.callApi(dataUrl, 'GET', params).then((datas)=> {
                const treeData = [..._this.state.treeData];
                getNewTreeData(treeData, treeKey, generateTreeNodes(datas,treeNode), 3);
                _this.setState({treeData});
                resolve();
            });
        });
    }
    isCheckedDisable(node) {
        const {filterData} = this.props;
        if (node.canClick === 'false') {
            return true;
        }
        if (isArray(filterData)) {
            return filterData.some((d)=>node.treeKey.indexOf(d.nodeid) > -1 )
        }
        return false;
    }
    isRoot(node) {
        return node.type === 'com';
    }
    onCheck(keys, info) {
        const {checkGroup = true} = this.props;
        const selectNodes = [];
        let targetKeys = cloneDeep(keys);
        targetKeys = targetKeys.filter((key) => this.treeDataList[key].canClick !== 'false');
        // 获取到所有不是人员节点的key
        const groupKeys = targetKeys.filter((item)=>{
            return item.indexOf('resource') === -1;
        })
        // 获取到所有人员节点的key
        const resourceKeys = targetKeys.filter((item)=>{
            return item.indexOf('resource') > -1;
        })
        if (checkGroup) {
            // 可以选择部门，分部
            targetKeys.forEach((i)=> {
                let targetNode = this.treeDataList[i];
                if (!groupKeys.some((key) => targetNode.treeKey.indexOf(key +'-') > -1)) {
                    selectNodes.push(targetNode);
                }
            })
        } else {
            // 只能选择人
            resourceKeys.forEach((i)=> {
                selectNodes.push(this.treeDataList[i]);
            })
        }
        this.props.checkedCb && this.props.checkedCb(targetKeys, selectNodes);
    }
    onDoubleClick(key) {
        const {filterData} = this.props;
        if (this.treeDataList[key].type === 'com' || this.treeDataList[key].canClick === 'false') return;
        if (isArray(filterData) && filterData.some((d)=>key.indexOf(d.nodeid) > -1)) return;
        this.props.onDoubleClick && this.props.onDoubleClick([this.treeDataList[key]]);
    }
    render() {
        const {isMult,treeData,defaultExpandedKeys} = this.state;
        // console.log("defaultExpandedKeys",defaultExpandedKeys)
        const {checkedKeys,icon, className, checkStrictly = false} = this.props;
        // console.log("this.state.treeData",this.state.treeData);
        const loop = data => data.map((item) => {
            let title=null;
            if(item.type == "com"){
                title = <div>{icon}{item.lastname}<span style={{fontSize:"12px",color:"#979797"}}>{/*人数占位*/}</span></div>;
            }else if(item.type == "subcom"){
                title = <div><i className="icon-process-scene"/>{item.lastname}</div>;
            }else if(item.type == "dept"){
                title = <div><i className="icon-mail-folder"/> {item.lastname}</div>;
            }else if(item.type == "resource"){
                title = <div style={{fontSize:"12px"}}><img className="wea-tree-hr-portrait" src={item.icon} alt="img"/><span style={{paddingLeft:8}}>{item.lastname}</span>  <span style={{color:"#929390",paddingLeft:8}}>{item.jobtitlename}</span></div>;
            }else{
                title = <div><i className="icon-process-scene"/> {item.lastname}</div>
            }

            if (item.children && item.children.length>0) {
                // console.log("item.treeKey",item.treeKey);
                for(let i =0;i<item.children.length;i++){
                    item.children[i].treeKey = `${item.treeKey}-${item.children[i].nodeid}`;
                }
                this.treeDataList[item.treeKey] = item;
                // console.log("item",item);
                return <TreeNode title={title}
                            checkboxCls={this.isRoot(item)?'root-checkbox': ''}
                            lastname={item.lastname}
                            jobtitlename={item.jobtitlename}
                            key={item.treeKey}
                            id={item.id}
                            treeKey={item.treeKey}
                            isLeaf={item.isLeaf}
                            disableCheckbox={this.isCheckedDisable(item)}
                            type={item.type}>
                        {loop(item.children)}
                        </TreeNode>
            }
            this.treeDataList[item.treeKey] = item;
            return <TreeNode title={title}
                        checkboxCls={this.isRoot(item)?'root-checkbox': ''}
                        lastname={item.lastname}
                        jobtitlename={item.jobtitlename}
                        key={item.treeKey}
                        id={item.id}
                        treeKey={item.treeKey}
                        isLeaf={item.isLeaf}
                        disableCheckbox={this.isCheckedDisable(item)}
                        type={item.type}
                        />
        });
        this.treeDataList = {};
        const treeNodes = loop(this.state.treeData);
        return (
            <div className={`${className}`} style={{display: 'inline-block'}}>
                <Tree className="my-Tree"
                    clickNodeExpandChildren
                    checkStrictly={checkStrictly}
                    defaultExpandedKeys={defaultExpandedKeys}
                    expandedKeys={this.state.expandedKeys}
                    onDoubleClick={this.onDoubleClick.bind(this)}
                    onCheck={this.onCheck.bind(this)}
                    loadData={this.onLoadData.bind(this)}
                    checkable={isMult}
                    multiple={isMult}
                    selectedKeys={[]}
                    checkedKeys={checkedKeys}
                    needExpandChildren
                    onExpand={this.onExpandChange.bind(this)}>
                {treeNodes}
                </Tree>
            </div>
        )
    }
};

function objToArray(obj) {
    if (!isArray(obj)) {
        let arr = [];
        arr.push(obj);
        obj = arr;
    }
    return obj;
}

function generateData(data, level = 1) {
    const {topPrefix} = that.state;
    if (!isArray(data)) {
        data.isLeaf = data.isParent === "false";
        if (level == 1) data.treeKey = `${topPrefix}0-${data.nodeid}`;
        if (data.children && data.children.length > 0) generateData(data.children, level+1);
    } else {
        data.forEach((item) => {
            if (level == 1) item.isLeaf = item.isParent === "false";
            if (item.children && item.children.length > 0) generateData(item.children, level+1);
        })
    }
}

function generateTreeNodes(data,treeNode) {
    // console.log("data",data,"data.children",data.children);
    let arr = new Array();
    const {topPrefix} = that.state;
    if(!treeNode){
        arr.push({
            id:data.id,
            lastname:data.lastname,
            departmentname: data.departmentname,
            supsubcompanyname: data.supsubcompanyname,
            subcompanyname: data.subcompanyname,
            nodeid:data.nodeid,
            jobtitlename:data.jobtitlename,
            treeKey:`${topPrefix}0-${data.nodeid}`,
            icon:data.icon,
            isLeaf:!data.isParent || data.isParent === "false",//isLeaf:data.children.length === 0,
            children:data.children,
            open:data.open,
            canClick:data.canClick,
            type:data.type,
        });
    }else{
        const loop = datas =>{
            for (let i=0;i<datas.length;i++) {
                const data = datas[i];
                const {treeKey,id} = treeNode.props;
                // console.log("treeNode.props.treeKey",treeKey);
                arr.push({
                    id:data.id,
                    departmentname: data.departmentname,
                    supsubcompanyname: data.supsubcompanyname,
                    subcompanyname: data.subcompanyname,
                    lastname:data.lastname,
                    nodeid:data.nodeid,
                    jobtitlename:data.jobtitlename,
                    treeKey:`${treeKey}-${data.nodeid}`,
                    icon:data.icon,
                    isLeaf:!data.isParent || data.isParent === "false",//isLeaf:data.children.length === 0,
                    children:data.children,
                    open:data.open,
                    canClick:data.canClick,
                    type:data.type,
                });
            }
        }
        loop(data.datas);
    }
    return arr;
}

function getNewTreeData(treeData, curKey, child, level) {
    // console.log(treeData, curKey, child, level);
    let hrData = new Array();
    const findData = data => data.forEach(item => {
        // console.log("item.open",!!item.open)
        if (item.children && !!item.open) {
            findData(item.children);
        }else if(!item.open){
            // console.log("item",item);
            hrData.push(item);
        }
    });
    findData(treeData);

    // console.log("treeData:",treeData," hrData",hrData);

    const loop = (data) => {
        //if (level < 1 || curKey.length - 3 > level * 2) return;
        data.forEach((item) => {
            // console.log("curKey:",curKey," item.nodeid:",item.nodeid);
            // if (curKey.indexOf(item.nodeid) === 0) {
            if ((curKey+"-").indexOf(item.treeKey+"-") === 0) {
                // console.log("进来");
                if (item.children && item.children.length>0) {

                    loop(item.children);
                } else {
                    // console.log("给children添加数据-child",child);
                    item.children = child;
                }
            }else{
                // console.log("curKey",curKey,"item.nodeid",item.nodeid);
            }
        });
        // console.log("data2",data);
    };

    loop(hrData);

    // setLeaf(treeData, curKey, level);
}











