import WeaInput4Base from '../wea-input4-base'
import WeaWorkflowRadio from '../wea-workflow-radio'

export class WeaInput4DocsNewCategory extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="文档目录" //type=''//联想搜索
				icon={<i className='icon-portal-doc' />}
				iconBgcolor='#e98931'
				topPrefix='category'
				contentType="tree"
				dataUrl = '/api/workflow/browser/browserdata'
				otherPara={["actiontype","categoryBrowserlist"]}
				asyncUrl='/api/workflow/browser/browserdata?actiontype=categoryBrowserlist'
				showSearchArea={true} //显示搜索框
				notShowDropMenu={true}//不显示dropmenu
				singleTabName='按照文档目录'
				{...this.props} 
				/>
		)
	}
}

