import WeaInput4Base from '../wea-input4-base'
import WeaWorkflowRadio from '../wea-workflow-radio'
// import WeaInput from '../../wea-input';

export  class WeaInput4CustomNewType extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="类型"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","customertypelist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
				/>

		)
	}
}

export class WeaInput4CustomNewStatus extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="状态"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","customerstatuslist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
				/>

		)
	}
}
export class WeaInput4CustomNewCountry extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="国家"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","hrmCounList"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
				/>

		)
	}
}
export class WeaInput4CustomNewCity extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return ( <WeaWorkflowRadio
				title="城市"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				topPrefix='city'
				contentType="tree"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","countryXmllist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				asyncUrl='/api/workflow/browser/browserdata?actiontype=countryXmllist'
				notShowDropMenu={true}//不显示dropmenu
				singleTabName='按城市'
				{...this.props}
				/>
		)
	}
}
export class WeaInput4CustomNewDescription extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="描述"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","customerdesclist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
				/>

		)
	}
}
export class WeaInput4CustomNewScale extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="客户规模"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","customersizelist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
				/>

		)
	}
}
export class WeaInput4CustomNewIndustry extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {

		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			 <WeaWorkflowRadio
				title="行业"
				icon={<i className='icon-customer-me' />}
				iconBgcolor='#b32e37'
				topPrefix='indestry'
				contentType="tree"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","sectorInfolist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				asyncUrl='/api/workflow/browser/browserdata?actiontype=sectorInfolist'
				notShowDropMenu={true}//不显示dropmenu
				singleTabName='按行业'
				{...this.props}
				/>

		)
	}
}