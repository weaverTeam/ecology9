import {Icon, Button, Spin, Row, Col, Card, Checkbox, Pagination, Menu, Dropdown} from 'antd';
// import Icon from '../../../_antd1.11.2/icon'
// import Button from '../../../_antd1.11.2/button'
// import Spin from '../../../_antd1.11.2/spin'
// import Row from '../../../_antd1.11.2/row'
// import Col from '../../../_antd1.11.2/col'
// import Card from '../../../_antd1.11.2/card'
// import Checkbox from '../../../_antd1.11.2/checkbox'
// import Pagination from '../../../_antd1.11.2/pagination';
// import Menu from '../../../_antd1.11.2/menu'
const MenuItem = Menu.Item;
// import Dropdown from '../../../_antd1.11.2/dropdown'
import cloneDeep from 'lodash/cloneDeep';
import isEqual from 'lodash/isEqual';
import debounce from 'lodash/debounce';

const config ={
    gap:105,
    borderColor:'#add8e6'
}
function compare(key){
    return function(obj1,obj2){
        let val1 = obj1[key],
            val2 = obj2[key];
        return 1*val1 <= 1*val2 ? -1 : 1;
    }
}
class Main extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            checked: false,
            disabled: false,
            ids:[],
            style:null,
            recordId:'',
        };
        this.checkboxOnChange = this.checkboxOnChange.bind(this);
        this.thumbnailsPageChange = this.thumbnailsPageChange.bind(this);
        this.thumbnailOnShowSizeChange = this.thumbnailOnShowSizeChange.bind(this);
    }
    componentDidMount(){

    }
    componentWillReceiveProps(nextProps) {

    }
    shouldComponentUpdate(nextProps,nextState) {
        return this.props.height !== nextProps.height||
        this.state.checked !== nextState.checked||
        this.state.disabled !== nextState.disabled||
        this.state.style !== nextState.style||
        this.state.recordId !== nextState.recordId||
        this.state.ids !== nextState.ids||
        !isEqual(this.props.browser,nextProps.browser)
    }
    getOpMenu(record){
        const { operates, layout } = this.props;
        const rfs = record.randomFieldOp?JSON.parse(record.randomFieldOp):{};
        let items = new Array();
        operates.forEach((operate,index)=>{
            let flag = operate.index || "-1";
            if(rfs[flag] && rfs[flag] != "false") {
                items.push(
                    <MenuItem key={index}>
                        <a href='javascript:void(0);' onClick={()=>{
                                typeof this.props.onOperatesClick === 'function' && this.props.onOperatesClick(record,null,operate,null);
                            }
                        }>{operate.text}</a>
                    </MenuItem>
                );
            }
        });
        const menu = (
            <Menu kj>
                {items}
            </Menu>
        )
        if(items.length>0){
            return  (
                <Dropdown overlay={menu}>
                    <span className='wea-thumbnails-doc-t_text'>操作<Icon className='wea-thumbnails-doc-t_column' type="caret-down" style={{fontSize: 10, color: '#a3a3a3', marginLeft: 3}}/></span>
                </Dropdown>
            )
        }
    }
    getCards(){
        const{ ids }=this.state;
        const {layout,browser,datas} = this.props;
        let _this = this;
        let thumbnailsArr=[];
        browser && browser.forEach((item,browserIndex) => {
            const linkvaluecolumn = item.linkvaluecolumn;
            const linkkey = item.linkkey;
            let list = cloneDeep(item.list);
            let title=null;
            list.forEach( (l,index) => {
                if(l.t_type=='title' ){
                    title = l.t_column;
                    list.splice(index,1);
                }
            });
            const newList = list.sort(compare('t_showorder'));
            const cardExtra = (
                <div className='wea-thumbnails-doc-extra'>
                    <Checkbox
                        checked={ids.indexOf(linkvaluecolumn)>=0}
                        id={linkvaluecolumn}
                        onChange={this.checkboxOnChange}
                    />
                </div>
            )
            thumbnailsArr.push(
                <Col span="12" >
                    <Card  extra={cardExtra}>
                        {item.isNew == '1' && <div className='wea-thumbnails-doc-triangle'/>}
                        <div className='wea-thumbnails-doc-image'>
                        	{
                                newList.map(l => {
                                	if(l.t_type === 'img'){
                                		return <i className={l.t_column} />
                                	}
                                })
                            }
                        </div>
                        <div className='wea-thumbnails-doc-content'>
	                        <p style={{fontSize: 14, color: '#484a4d'}}><span dangerouslySetInnerHTML={{__html:title}} /></p>
                            {
                                newList.map(l => {
                                	if(l.t_type === 'subtitle'){
                                		return <p><span className='wea-thumbnails-doc-content-subtitle' >{l.t_text} : </span><span dangerouslySetInnerHTML={{__html:l.t_column}} /></p>
                                	}
                                })
                            }
                        </div>
                        <div className='wea-thumbnails-doc-content-foot'>
                        	<p>
	                            {
	                                newList.map(l => {
	                                    if(l.t_type=='foot') {
	                                        return <span><i className={l.t_icon} /> {l.t_column}</span>
	                                    }
	                                })
	                            }
                            	<span style={{cursor: 'pointer'}}>{this.getOpMenu(datas[browserIndex])}</span>
                            </p>
                        </div>
                    </Card>
                </Col>
            )
        })
        return thumbnailsArr;
    }
	render(){
        const{ ids,thumbnailsArr}=this.state;
        const { browser, height } = this.props;
		return (
			<div className='wea-thumbnails-doc'>
                 <Row style={{maxHeight: height, overflowY:'auto', padding: '0 20px 20px 0'}}>
                     {this.getCards()}
                 </Row>
            </div>
        )
	}
//               <Row className='wea-thumbnails-doc-pg'>
//                  <Pagination
//                  	className='ant-table-pagination'
//                      {...this.props}
//                      {...this.props.paginationConfig}
//                      onChange={this.thumbnailsPageChange}
//                      onShowSizeChange={this.thumbnailOnShowSizeChange}
//                  />
//               </Row>
	checkboxOnChange(e){
        const isChecked = e.target.checked;
        const eId = e.target.id;
        this.setState(function(preState){
            const curIds = preState.ids;
            isChecked ? this.throwIds = [...preState.ids,eId] || [] : this.throwIds = curIds.filter(id => eId != id) || [];
            this.throwCheckedIds();
            //this.changeCardCss(eId,isChecked);
            return{
                ids:this.throwIds
            }
        });
    }
    throwCheckedIds(){
         if(typeof this.props.thumbnailsOnChange == 'function'){
            this.props.thumbnailsOnChange(this.throwIds)
        }
    }
    thumbnailsPageChange(page) {
       if(typeof this.props.onChange == "function"){
            this.props.onThumbnailsChange(page,this.props.pageSize, '');
        }
    }
    thumbnailOnShowSizeChange(current, pageSize){
         this.props.onThumbnailsChange(current,pageSize, '');
    }
}

export default Main;
