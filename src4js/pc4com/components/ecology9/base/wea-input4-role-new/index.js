import WeaInput4Base from '../wea-input4-base'
import WeaWorkflowRadio from '../wea-workflow-radio'

export default class WeaInput4RoleNew extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="角色"
				type='160'
				icon={<i className='icon-portal-hrm' />}
				iconBgcolor='#f00'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","rolelist"]}//otherPara={["action","BrowserAction","actiontype","cuslist"]}
				{...this.props}
			/>
		)
	}
}

