import {Row, Col, Input, Modal, Table, Tooltip, Tabs, Select, Menu, Button, Spin} from 'antd';
// import Row from '../../../_antd1.11.2/Row';
// import Col from '../../../_antd1.11.2/Col';
// import Tooltip from '../../../_antd1.11.2/Tooltip';
// import Tabs from '../../../_antd1.11.2/Tabs';
// import Table from '../../../_antd1.11.2/Table';
const TabPane = Tabs.TabPane;
// import Spin from '../../../_antd1.11.2/Spin';
// import Select from '../../../_antd1.11.2/Select';
const Option = Select.Option;
// import Menu from '../../../_antd1.11.2/menu';
// import Button from '../../../_antd1.11.2/Button';
// import Input from '../../../_antd1.11.2/Input';
import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import objectAssign from 'object-assign'

import WeaTools from '../../wea-tools';
import WeaBrowserTable from '../wea-browser-table';
import WeaDropMenu from '../wea-drop-menu';
import WeaBrowserSearch from '../../wea-browser-search';
import PureRenderMixin from 'react-addons-pure-render-mixin';

const isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible:false,
            loading: true,
            showDrop: false,
            activeKey: "allData",
            dataKey:props.dataKey,
            hasPgTableData: props.hasPgTableData || {},
            noPgTableData: props.noPgTableData || {},
            contentType:props.contentType,
            hasPagination:props.hasPagination,
            url_pgTable:props.url,
            initialShowSearchAd:false,
            id:"",
            name:'',
            seacrhPara:'',
            hasSearchValue:false,
            pagination: {},
        }
    }
    componentDidMount(){

    }
    shouldComponentUpdate(...args) {
        return PureRenderMixin.shouldComponentUpdate.apply(this, args);
    }
    componentWillReceiveProps(nextProps, nextState) {
        const {dataKey,noPgTableData,hasPgTableData,contentType,hasPagination,url}=this.props;
        if(dataKey !== nextProps.dataKey &&!isEmpty(nextProps.dataKey)){
            this.setState({dataKey:nextProps.dataKey})
        }
        if(!isEqual(noPgTableData, nextProps.noPgTableData)){
            this.setState({noPgTableData: nextProps.noPgTableData})
        }
        if(!isEqual(hasPgTableData, nextProps.hasPgTableData)){
            this.setState({hasPgTableData: nextProps.hasPgTableData})
        }
        if(contentType !== nextState.contentType&&!isEmpty(nextProps.contentType)){
            this.setState({contentType:nextProps.contentType})
        }
        if(hasPagination !== nextState.hasPagination){
            this.setState({hasPagination:nextProps.hasPagination})
        }
        if(url !== nextProps.url){
            this.setState({url_pgTable:nextProps.url})
        }
    }

    onRowClick(record, index){
        const{id,name,hasPagination} = this.state;
        let inputName = '',throwName='';
        let inputId='',throwId='';
        if (typeof this.props.setData === "function") {
            const columns = this.refs.tableList.state.columns;
            columns.forEach(item=>{//分页
                if(item.isInputCol=='true'){//name标识
                    inputName =  item.dataIndex;
                }
                if(item.isPrimarykey=='true'){//id标识
                    inputId =  item.dataIndex;
                }
            })
            throwName = record[inputName + 'span'] || record[inputName] || record['name'] || '';
            throwId = record[inputId + 'span'] || record[inputId] || record['id'] || '';
            this.props.setData(throwId,throwName);
        }
        this.setState({
            id:throwId,
            name:throwName,
        })
    }
    setShowSearchAd(bool){
        const {showSearchAd}=this.state;
        this.setState({showSearchAd:bool,initialShowSearchAd:true})
    }
    render(){
        let _this = this;
        const {contentHeight, pageSize} = this.props;
        const {noPgTableData,hasPgTableData,dataKey,contentType,hasPagination,url_pgTable}=this.state;
        let keyOrNoKey = {};
        if (contentType =='dk_hasPgTable') {
            keyOrNoKey = {
                dataKey: dataKey
            }
        }
        if (contentType =='hasPgTable') {
            keyOrNoKey = {
                noSkHasPgTable: {contentType: contentType, url:url_pgTable, hasPgTableData:hasPgTableData}
            }
        }
        if (contentType =='noPgTable') {
            keyOrNoKey = {
                noPgTableData: noPgTableData
            }
        }
        return(
            <WeaBrowserTable
                ref="tableList"
                {...keyOrNoKey}
                pageSize={pageSize}
                setLoading={this.props.setLoading}
                useFixedHeader
                scroll
                weaSimple
                usePagination={hasPagination}
                onChange={(p)=>{ /*console.log('p',p);*/ this.setState({pagination: p})}}
                showTotal
                noOperate
                onRowClick={(record,index)=> _this.onRowClick(record, index)}
                getTableLoading={loading => _this.setState({loading:loading})}
                contentHeight={contentHeight}
            />
        );
    };
    isPreviousDisabled() {
        return this.state.pagination.current === 1;
    }
    isNextDisabled() {
        const {pagination} = this.state;
        return pagination.current === Math.ceil(pagination.count/pagination.pageSize)
    }
    previous() {
        const {pagination} = this.state;
        const current = pagination.current - 1;
        if (current === 0) return;
        this.refs.tableList.getTableDatas('', false, current, pagination.pageSize);
    }
    next() {
        const {pagination} = this.state;
        if (pagination.current === Math.ceil(pagination.count/pagination.pageSize)) return;
        this.refs.tableList.getTableDatas('', false, pagination.current + 1, pagination.pageSize);
    }


}

export default Main;
