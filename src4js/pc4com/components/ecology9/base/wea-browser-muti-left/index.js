import classNames from 'classnames';
// import Icon from '../../../_antd1.11.2/icon';
import {Icon} from 'antd';
import keys from 'lodash/keys';
let timeout = null;
class main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const {datas, inputId, inputName, inputTop, inputBottom, contentType} = this.props;
        this.dataObj = {}
        let list = []
        if (inputId) {
            list = datas.map(item => {
                item.id = item[inputId];//我的请求

                this.dataObj[item.id] = item
                let top = [];
                let bottom = [];
                inputTop && inputTop.forEach((field)=> {
                    top.push(<span style={{marginRight: '5px'}} dangerouslySetInnerHTML={{__html: item[field + 'span'] || item[field]}}></span>)
                })
                inputBottom && inputBottom.forEach((field)=> {
                    bottom.push(<span style={{marginRight: '5px'}} dangerouslySetInnerHTML={{__html: item[field + 'span'] || item[field]}}></span>)
                })
                return <li
                        className={this.cls(item)}
                        onDoubleClick={this.onDoubleClick.bind(this, item)}
                        onClick={this.onClick.bind(this, item)}
                        >
                        <div className="item-wrap" style={{fontSize: 12}}>
                            <div className="item-top">
                                {top}
                            </div>
                            <div className="item-bottom">
                                {bottom}
                            </div>
                        </div>
                        <div className="icon-wrap" >
                        </div>
                        <Icon type="check" className="icon-selected"/>
                    </li>
            });
        }
        return (
            <div className="wea-crm-list">
                <ul className="wea-crm-list-wrapper">
                    {list}
                </ul>
                {
                    list.length == 0 && <div className="empty-tip" style={{color: '#2f2929', paddingTop: '10', textAlign: 'center'}}>
                        没有可显示的数据
                    </div>
                }
            </div>
        )
    }

    cls(item) {
        const {selectedKeys, filterData, inputId} = this.props;
        let cls = [];
        if (selectedKeys && selectedKeys.indexOf(item.id) > -1) {
            cls.push('selected');
        }
        if (filterData && filterData.filter((d) => d[inputId] === item.id).length > 0) {
            cls.push('hide');
        }
        return cls.join(' ');
    }
    onClick(data) {
        let _this = this;
        // 取消上次延时未执行的方法
        clearTimeout(timeout);
        //执行延时
        timeout = setTimeout(function(){
            let {selectedKeys} = _this.props;
            let keys = selectedKeys ? [...selectedKeys]: [];
            let datas = [];
            if (keys.indexOf(data.id) > -1 ) {
                keys = keys.filter((k) => k !== data.id );
            } else {
                keys.push(data.id);
            }
            keys.forEach((k)=> {
                _this.dataObj[k] && datas.push(_this.dataObj[k])
            })
            _this.props.onClick && _this.props.onClick(keys, datas);
        },200);
    }
    onDoubleClick(data) {
        clearTimeout(timeout);
        this.props.onDoubleClick && this.props.onDoubleClick([data]);
    }
}

export default main;
