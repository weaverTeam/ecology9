import {Form,Input,Modal,Button,Icon,Col,Tabs,Badge,Tag,Tooltip,Row} from 'antd'
// import Form from '../../../_antd1.11.2/form'
// import Input from '../../../_antd1.11.2/input'
// import Modal from '../../../_antd1.11.2/modal'
// import Button from '../../../_antd1.11.2/button'
// import Icon from '../../../_antd1.11.2/icon'
// import Col from '../../../_antd1.11.2/col'
// import Tabs from '../../../_antd1.11.2/tabs'
// import Badge from '../../../_antd1.11.2/badge'
// import Tag from '../../../_antd1.11.2/tag'
// import Tooltip from '../../../_antd1.11.2/tooltip'
// import Row from '../../../_antd1.11.2/row'

import WeaTree from '../wea-tree'
import WeaList from '../wea-list'
const FormItem = Form.Item;
const InputGroup = Input.Group;
const TabPane = Tabs.TabPane;
//modal组件皮肤
//import '../../../Theme/theme_e8.css';
//import '../../../Theme/theme_ant.css';


class WeaInput4Base extends React.Component {
	constructor(props) {
		super(props);
		const {value, valueSpan} = props;
		this.state = {
			visible: false,
			ids: value ? value : "",
			names: valueSpan ? valueSpan : ""
		}
	}
	componentWillReceiveProps(nextProps) {
		if (this.props.value !== nextProps.value && this.props.valueSpan !== nextProps.valueSpan) {
			//console.log("this.props.value:",this.props.value);
			//console.log("nextProps.value:",nextProps.value);
			this.setState({
				ids: nextProps.value,
				names: nextProps.valueSpan
			});
		}
	}
	showModal() {
		//		this.ShowDivCenter;
		this.setState({
			visible: true
		});
	}
	hiddenModal=()=>{
		this.handleOk();
	}
	handleOk() {
		this.setState({
			visible: false
		});
		const {ids, names} = this.state;
		if (typeof (this.props.onChange) == "function") {
			this.props.onChange(ids, names);
		}
	}
	handleCancel() {
		this.setState({
			visible: false
		});
	}
	handleClear() {
		this.setState({
			visible: false,
			ids: "",
			names: ""
		});
		if (typeof (this.props.onChange) == "function") {
			this.props.onChange("", "");
		}
	}

	//弹框动态居中
	//	componentDidMount:function(){
	showCenter() {
		//		 console.log("弹框");
		//    		let top = ($(window).height() - $('.ant-modal').height()) / 2;
		//    		 let left = ($(window).width() - $('.ant-modal').width()) / 2;
		//   	 	 let scrollTop = $(document).scrollTop();
		//  	 	 let scrollLeft = $(document).scrollLeft();
		//  	  	 $('.ant-modal').css({ position: 'fixed', 'top': top + scrollTop, left: left + scrollLeft });
		//  	  	console.log("弹框2");
	}

	//	}



	//	getInitialState: function () {
	//  return showCenter: () =>{
	//    		 return  function() {
	//    		 let top = ($(window).height() - $('.ant-modal-content').height()) / 2;
	//    		 let left = ($(window).width() - $('.ant-modal-content').width()) / 2;
	//   	 	 let scrollTop = $(document).scrollTop();
	//  	 	 let scrollLeft = $(document).scrollLeft();
	//  	  	 $('.ant-modal-content').css({ position: 'absolute', 'top': top + scrollTop, left: left + scrollLeft });
	//  	  }
	//    	}
	//
	//},

	render() {
		//console.log("this.props:",this.props);
		const {visible} = this.state;
		let {ids, names} = this.state;
		const {singleGroup, id, name, width, style, title, topPrefix, topIconUrl, dataUrl, paramsUrl, dataKey, otherPara, isMult, ifAsync, ifCheck, disabled, prefix, value, valueSpan, modalWidth, et} = this.props;
		let {browerType} = this.props;
		//console.log("browerType:",browerType);
		if (!browerType) browerType = ['tree'];
		//console.log("browerType.indexOf(list)>=0:",browerType.indexOf("list")>=0);
		//console.log("value1:",value," valueSpan:",valueSpan);
		//&&(ids==""||!ids)
		if (value && value != "" && (ids == "" || !ids)) {
			ids = value;
			names = valueSpan;
		}
		//console.log("ids1:",ids," names:",names);
		//console.log("ids:",ids);
		//console.log("names:",names);
		//console.log("value:",value);
		//console.log("valueSpan:",valueSpan);
		ids = ids ? ids.toString() : "";
		names = names ? names.toString() : "";
		//console.log("ids  :",ids);
		const idArr = ids ? ids.split(",") : [];
		const nameArr = ids ? names.split(",") : [];
		let tabHtml = "";
		if (ids != "") {
			tabHtml = <Badge count={idArr.length}>已选</Badge>;
		}
		else {
			tabHtml = "已选";
		}
		let nameHtmlArr = new Array();
		for (let i = 0; i < idArr.length && ids != ""; i++) {
			const tid = idArr[i];
			const tname = nameArr[i];
			nameHtmlArr[i] = <Tag key={i} color="blue">{tname}</Tag>;
		}
		//let buttonClass = "ant-search-btn ant-input-group-formitem";
		let tabPaneArr = []
		//		console.log("是true吗？",browerType.indexOf("tree")>=0);
		if (browerType.indexOf("tree") >= 0) {
			tabPaneArr[tabPaneArr.length] = (
				<TabPane tab="按树" key="tree">
					<div style={{ "height": "490px", "overflow": "auto" }}>
						<WeaTree
							id={id}
							topPrefix={topPrefix}
							prefix={prefix}
							topIconUrl={topIconUrl}
							dataUrl={dataUrl}
							dataKey={dataKey}
							otherPara={otherPara}
							isMult={isMult}
							ifAsync={ifAsync}
							ifCheck={ifCheck}
							setData={this.setData.bind(this) }
							ids={ids}
							names={names}
							/>
					</div>
				</TabPane>
			)
			//			console.log("tabPaneArr",tabPaneArr);
		}
		if (singleGroup) {
			tabPaneArr[tabPaneArr.length] = (
				<TabPane tab="按组" key="group">
					<div style={{ 'height': '490px', 'overflow': 'auto' }}>
						<WeaTree
							id={id}
							topPrefix={"group"}
							prefix={"groupry"}
							topIconUrl={topIconUrl}
							dataUrl={"/cloudstore/system/ControlServlet.jsp"}
							dataKey={"id"}
							otherPara={["action", "Action_GetGroupDataList"]}
							isMult={isMult}
							ifAsync={ifAsync}
							ifCheck={ifCheck}
							setData={this.setData.bind(this) }
							ids={ids}
							names={names}
							/>
					</div>
				</TabPane>
			)
		}
		if (browerType.indexOf("list") >= 0) {
			const {ht, t, dt, listUrl, countUrl, datasType, dn, keyNum} = this.props;
			//console.log("base:",dn," keyNum:",keyNum);
			tabPaneArr[tabPaneArr.length] = (
				<TabPane tab="按列表" key="list">
					<WeaList
						id={id}
						paramsUrl={paramsUrl}
						listUrl={listUrl}
						countUrl={countUrl}
						fieldhtmltype={ht}
						fielddbtype={dt}
						type={t}
						isMult={isMult}
						setData={this.setData.bind(this) }
						ids={ids}
						names={names}
						datasType={datasType}
						dn={dn}
						keyNum={keyNum}
						visible={visible}
						ondblclickCallBack={this.hiddenModal}
						/>
				</TabPane>
			)
		}

		tabPaneArr[tabPaneArr.length] = (
			<TabPane tab={tabHtml} key="isUse">
				<div style={{ "height": "490px", "overflow": "auto" }}>
					{nameHtmlArr}
				</div>
			</TabPane>
		)
		//console.log("style:",style);
		//console.log("names:",names);
		let inputArr = (
			<div className="ant-search-custom">
				<Input key="theInput" style={style} addonAfter={<div style={{ "cursor": "pointer" }}>
					<i onClick={this.showModal.bind(this) } className="anticon anticon-search" ></i>
				</div>} value={names} />
			</div>
		);
		if (ids.length > 0) {
			inputArr = (<Tooltip title={names}>{inputArr}</Tooltip>)
		}
		if (et && et == 2 && ids.length == 0) {
			inputArr = (<Badge dot>{inputArr}</Badge>)
		}
		//console.log("yes yes");
		return (
			<div>
				{inputArr}
				<input type="hidden" id={id} name={name} value={ids} />
				<Modal className="wea-browser-old"
					wrapClassName="vertical-center-modal"
					title={title}
					visible={visible}
					onCancel={this.handleCancel.bind(this) }
					width={modalWidth}
					footer={[
						<Button  key="submit" type="primary" size="small" onClick={this.handleOk.bind(this) }>确 定</Button>,
						<Button  key="back" type="ghost" size="small" onClick={this.handleClear.bind(this) }>清 除</Button>,
						<Button key="cancle" type="ghost" size="small" onClick={this.handleCancel.bind(this) }>取消</Button>
					]}
					>
					<p>请选择</p><img src="/cloudstore/images/store/mnav2_wev8.png"  alt="bumen" /><span>{title}</span>

					<Tabs defaultActiveKey={browerType[0]} style={{ "margin-left": "10px", "margin-right": "10px" }}>
						{tabPaneArr}
					</Tabs>
				</Modal>
				{this.showCenter() }
			</div>
		)
		//{browerType.indexOf("tree")>=0 &&
	}
	setData(ids, names) {
		//console.log("ids:",ids);
		//console.log("names:",names);
		if (typeof (this.props.onChange) == "function") {
			this.props.onChange(ids, names);
		}
		this.setState({
			ids: ids,
			names: names
		});
	}
};

export default WeaInput4Base


///


/*

<InputGroup className="ant-search-input" style={{"width":width}}>
			<Tooltip title={names}>
			<Input id={id+"span"} name={name+"span"} style={style} disabled value={names} />
			 </Tooltip>
			   <input type="hidden" id={id} name={name} value={ids} />
			   <div className="ant-input-group-wrap">

		       </div>
		    </InputGroup>

*/