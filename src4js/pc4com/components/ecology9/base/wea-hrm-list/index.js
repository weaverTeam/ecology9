import classNames from 'classnames'
import {Icon} from 'antd';
// import Icon from '../../../_antd1.11.2/icon'
let timeout = null;
class main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        const {datas} = this.props;
        this.dataObj = {}
        const list = datas.map(item => {
            let crmNames = [];
            if (item.departmentname) crmNames.push(item.departmentname);
            if (item.supsubcompanyname) crmNames.push(item.supsubcompanyname);
            if (item.subcompanyname) crmNames.push(item.subcompanyname);
            item.crmNames = crmNames.join('／');
            this.dataObj[item.id] = item
            // console.log('itme.workcode',item.workcode);
            return <li
                    className={this.cls(item)}
                    onDoubleClick={this.onDoubleClick.bind(this, item)}
                    onClick={this.onClick.bind(this, item)}
                    >
                    <div className="item-wrap">
                        <img className="wea-tree-hr-portrait" src={item.icon} alt="img"/>
                        <div className="resource-content">
                            <div>
                                {this.props.is_012app&&item.workcode&& <span title={item.workcode} style={{marginRight: 10}}>{item.workcode}</span>}
                                <span className="lastname" title={item.lastname}>{item.lastname}</span>
                                <span className="jobtitlename gray" title={item.jobtitlename}>{item.jobtitlename}</span>
                            </div>
                            <div className="overflow gray overflow" title={item.crmNames}>
                                {item.crmNames}
                            </div>
                        </div>
                    </div>
                    <div className="icon-wrap" >
                    </div>
                    <Icon type="check" className="icon-selected"/>
                </li>
        })
        return (
            <div className="wea-crm-list">
                <ul className="wea-crm-list-wrapper">
                    {list}
                </ul>
                {
                    list.length == 0 &&
                    <div className="empty-tip" style={{color: '#2f2929', paddingTop: '10', textAlign: 'center'}}>
                        没有可显示的数据
                    </div>
                }
            </div>
        )
    }
    cls(item) {
        const {selectedKeys, filterData} = this.props;
        let cls = [];
        if (selectedKeys && selectedKeys.indexOf(item.id) > -1) {
            cls.push('selected');
        }
        if (filterData) {
            let ids = [];
            filterData.forEach((d) => {
                if (d.type == 'resource') {
                    ids.push(d.id)
                } else {
                    d.users && d.users.forEach((u)=> {
                        ids.push(u.id)
                    })
                }
            })
            if (ids.indexOf(item.id) > -1) {
                cls.push('hide');
            }
        }
        return cls.join(' ');
    }
    onClick(data) {
        let _this = this;
        // 取消上次延时未执行的方法
        clearTimeout(timeout);
        //执行延时
        timeout = setTimeout(function(){
            let {selectedKeys} = _this.props;
            let keys = selectedKeys ? [...selectedKeys]: [];
            let datas = [];
            if (keys.indexOf(data.id) > -1 ) {
                keys = keys.filter((k) => k !== data.id );
            } else {
                keys.push(data.id);
            }
            keys.forEach((k)=> {
                _this.dataObj[k] && datas.push(_this.dataObj[k])
            })
            _this.props.onClick && _this.props.onClick(keys, datas);
        }, 200);

    }
    onDoubleClick(data) {
        clearTimeout(timeout);
        this.props.onDoubleClick && this.props.onDoubleClick([data]);
    }
}

export default main;
