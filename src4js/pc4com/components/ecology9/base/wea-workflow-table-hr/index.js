import {Row, Col, Input, Modal, Tooltip, Tree, Table, Tabs, Pagination, Select, Menu, Button, Spin} from 'antd';
// import Tree from '../../../_antd1.11.2/Tree';
// import Row from '../../../_antd1.11.2/Row';
// import Col from '../../../_antd1.11.2/Col';
// import Modal from '../../../_antd1.11.2/Modal';
// import Tooltip from '../../../_antd1.11.2/Tooltip';
// import Tabs from '../../../_antd1.11.2/Tabs';
// import Table from '../../../_antd1.11.2/Table';
const TabPane = Tabs.TabPane;
const TreeNode = Tree.TreeNode;
// import Select from '../../../_antd1.11.2/Select';
const Option = Select.Option;
// import Menu from '../../../_antd1.11.2/menu';
// import Button from '../../../_antd1.11.2/Button';
// import Input from '../../../_antd1.11.2/Input';
// import Spin from '../../../_antd1.11.2/Spin';

import cloneDeep from 'lodash/cloneDeep';
import WeaScroll from '../../wea-scroll';
import WeaInputSearch from '../../wea-input-search';
import * as util from './util';

let workflowDatas;

export default class WeaWorkflowTalbeHr extends React.Component {
    constructor(props) {
        super(props);
        const {url,isaccount,min,max,componyParam,seacrhPara} = props;
        // console.log("url",url);
        this.state = {
            url:`${url}?isaccount=${isaccount}&min=${min}&max=${max}${!!seacrhPara ? `${seacrhPara}` : ''}`,
            showDrop: false,
            loading: true,
            datas:[],
            listDatas:[],
            listColumns:[],
            record:{},
            selectedRowKeys:'',
            rowIndex:'',
            id:"",
            name:'',
        }
    }
    componentDidMount() {
        let {url}= this.state;
        // console.log("url",url);
        let _this = this;
        url = `${url}&${new Date().getTime()}=`;
        util.getDatas(url,"GET").then(data => {//,{isaccount:isaccount,min:min,max:max}
            // console.log("data",data);
            _this.setState({
                loading:false,
                datas:data.datas
            })
        });


    }
    componentWillReceiveProps(nextProps) {
        // console.log("nextProps",nextProps);
        const {url,isaccount,min,max,componyParam,seacrhPara}=this.props;
        let _this = this;
        if(this.props.componyParam != nextProps.componyParam || seacrhPara !== nextProps.seacrhPara){
            // let newUrl = url + nextProps.componyParam;
            let newUrl = `${url}?isaccount=${isaccount}&min=${min}&max=${max}${nextProps.componyParam?nextProps.componyParam:''}${nextProps.seacrhPara ? `${nextProps.seacrhPara}`:''}`;
            newUrl = `${newUrl}&${new Date().getTime()}=`;
            util.getDatas(newUrl,"GET").then(data => {
            // console.log("data",data);
                _this.setState({datas:data.datas})
            });
        }
    }
    componentDidUpdate(){
        /*const {rowIndex}=this.state;
        if(rowIndex || rowIndex === 0){
            // console.log("rowIndex",rowIndex);
            jQuery(".wea-table-hr .ant-table-tbody").each(function(){
                jQuery(this).find("tr").css({"background-color":"#fff"}).find("td").css("color","#666");
            });
            jQuery(".wea-table-hr .ant-table-tbody").each(function(){
                jQuery(this).find("tr").eq(rowIndex).css({"background-color":"#0D93F6"}).find("td").css("color","#fff");
            });
        }*/
    }
    handleChange(selectedRowKeys, selectedRows) {

    }
    onChange(activeKey) {
        this.setState({
            activeKey
        });
    }
    onRowClick(record, index){
        // console.log("record",record,"index",index);
        const{id,name,rowIndex} = this.state;
        if (typeof this.props.setData === "function") {
            this.props.setData(record.key,record.lastname,record);
        }
        this.setState({
            id:record.key,
            name:record.lastname,
            rowIndex:index,
        })
    }
    render() {
        // console.log("this.state.visible2",this.state.visible);
        // console.log("this.state.listDatas",this.state.listDatas);
        // console.log("this.state.record",this.state.record);
        const {icon,iconBgcolor,title,allWf,bookMarkWf, browerType,listUrl,countUrl,otherPara,} = this.props;
        const {showDrop,activeKey,selectedRowKeys,id,name,datas} = this.state;

        const menu = (
            <Menu>
             <Menu.Item>选项一workflow</Menu.Item>
             <Menu.Item>选项二workflow</Menu.Item>
            </Menu>
        );
        const operations = (
            <div>
                <Select defaultValue="sixMoth" style={{ width: 120 }} onChange={this.handleChange.bind(this)}>
                  <Option value="sixMoth">最近六个月</Option>
                </Select>
                <span className='wea-table-hr-drop-btn' onClick={()=>this.setState({showDrop:!showDrop})}>
                    <i className="icon-button icon-New-Flow-menu" />
                </span>
                <div className='wea-table-hr-drop-menu' style={{display:showDrop ? 'block' : 'none'}}>
                    <span className='wea-table-hr-drop-btn' onClick={()=>this.setState({showDrop:!showDrop})}>
                        <i className="icon-button icon-New-Flow-menu" />
                    </span>
                    {menu}
                </div>
            </div>
        );
        const pagination = {
          total: datas.length,
          showSizeChanger: true,
          onShowSizeChange(current, pageSize) {
            // console.log('Current: ', current, '; PageSize: ', pageSize);
          },
          onChange(current) {
            // console.log('Current: ', current);
          },
        };
        const columns = [{
          title: '头像',
          dataIndex: 'messagerurl',
          key:"messagerurl",
          width: 100,
          render: (text) => <div className="wea-table-hr-portrait">{text}</div>
        }, {
          title: '姓名',
          dataIndex: 'lastname',
          key:"lastname",
          width: 100,
        },{
          title: '职位',
          dataIndex: 'jobtitlename',
          key:"jobtitlename",
        }];

        const dataSource = [];
        this.state.datas.forEach(item=>{
            dataSource.push({
                key:item.id,
                lastname:item.lastname,
                jobtitlename:item.jobtitlename,
                pinyinlastname:item.pinyinlastname,
                messagerurl:<img className='wea-tree-hr-portrait'  src={item.messagerurl} alt="img"/>,
            })
        });
        // 通过 rowSelection 对象表明需要行选择
        let _this = this;
        const rowSelection = {
          onChange(selectedRowKeys, selectedRows) {
            if(typeof _this.props.onChange === 'function'){
                _this.props.onChange(selectedRows);
            }
          },
          onSelect(record, selected, selectedRows) {
            // console.log(record, selected, selectedRows);
          },
          onSelectAll(selected, selectedRows, changeRows) {
            console.log(selected, selectedRows, changeRows);
          },
        };
        let hasRowSelection = {rowSelection:null};
        this.props.mult && (hasRowSelection = {rowSelection:rowSelection});
        return  (<div>
                    <WeaScroll className="wea-scroll" typeClass="scrollbar-macosx" style={{"height":"300px",width:"100%"}}>
                        <Table className="wea-table-hr"
                            showHeader={false}
                            onRowClick={this.onRowClick.bind(this)}
                            columns={columns}
                            dataSource={dataSource}
                            pagination={pagination}
                            {...hasRowSelection}
                        />
                    </WeaScroll>
                </div>)

    }
}

