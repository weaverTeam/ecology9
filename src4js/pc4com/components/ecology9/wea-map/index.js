import "./index.less";
class WeaMap extends React.Component {
	constructor(props) {
		super(props);
        this.map;
        this.marker;
	}
	componentDidMount() {
	    this.paint(this.props);
	}
	componentDidUpdate(prevProps, prevState) {
	    this.paint(this.props);
	}
	paint(obj) {
	    let {position, title, zoom} = obj;
        if (!position) {
            return;
        }
        if (!this.map) {
            this.map = new AMap.Map(ReactDOM.findDOMNode(this.refs.map), {
                resizeEnable: true,
                zoom: zoom,
                center: position
            });
        } else {
            this.map.setCenter(position);
        }
        let markerConfig = {
            map:this.map,
            icon:"http://webapi.amap.com/theme/v1.3/markers/n/mark_b.png",
            title: title,
            position:position
        };
        if (title) {
            markerConfig.content = "<div class='wea-map-marker'><img src='http://webapi.amap.com/theme/v1.3/markers/n/mark_b.png' align='absmiddle'><span class='wea-map-marker-span'>"+ title +"</span></div>";
        }
        this.marker = new AMap.Marker(markerConfig);
        this.map.setFitView();
    }
	render() {
		return (
            <div ref="map" className="wea-map">
                
            </div>
		);
	}
}
WeaMap.propTypes = {
    position: React.PropTypes.any,//位置，可以是经纬度数组或LngLat实例
    title: React.PropTypes.string,//提示文字
    zoom: React.PropTypes.number//缩放比例
};
WeaMap.defaultProps = {
    zoom: 14
};

export default WeaMap;