import Draggable from 'react-draggable';

class Main extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
        return (<Draggable {...this.props}/>);
	}
}
export default Main;