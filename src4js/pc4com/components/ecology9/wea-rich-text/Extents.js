import React from 'react'
import ReactDOM from 'react-dom'

export default class Main extends React.Component {
  append() {
    const { children } = this.props;
    ReactDOM.unstable_renderSubtreeIntoContainer(this, children, this.container)
  }
  componentDidMount() {
    const { container, style } = this.props;
    this.container = document.createElement('div');
    this.container.className = 'wea-rich-text-extents';
    for(key in style){
      if(style.hasOwnProperty(key)){
        this.container.style[key] = style[key];
      }
    }
    container.appendChild(this.container);
    this.append()
  }
  componentDidUpdate() {
    this.append()
  }
  componentWillUnmount() {
    
  }
  render() {
    return null
  }
}