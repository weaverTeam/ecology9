import {Progress} from 'antd';
import React, { Component } from 'react';
import WeaUpload from '../../wea-upload';
// import Progress from '../../../_antd1.11.2/progress'

export default class Upload extends Component {
  constructor(props) {
    super(props)
    this.state = {
      status: "",
      percent: 0,
      speed: 500,
      datas: [],
    }
    this.onChange = this.onChange.bind(this);
    this.onUploading = this.onUploading.bind(this);
  }
  componentDidMount() {

  }
  render() {
    const { show, needLoading = true } = this.props;
    const { percent, datas } = this.state;
    return (
      <WeaUpload
        {...this.props}
        onUploading={this.onUploading}
        onChange={this.onChange}
        datas={ datas }
      >
        <div style={{display: "inline-block"}} onClick={()=> this.setState({percent: 0, speed: 500})}>
          { show }
        </div>
        {
          needLoading ? (
            percent == 0 || percent == 100 ? "" : (
              <div style={{display: "inline-block"}}>
                <span style={{verticalAlign: "middle"}}>
                  &nbsp;(&nbsp;
                </span>
                <Progress
                  type="circle"
                  status="active"
                  percent={percent}
                  width="16"
                  format={percent => ''}
                />
                <span style={{verticalAlign: "middle"}}>
                  &nbsp;{`${percent}%`}&nbsp;)
                </span>
              </div>
            )
          ) : null
        }
      </WeaUpload>
    )
  }
  onChange(ids, list) {
    const { onToolsChange, name, type } = this.props;
    typeof onToolsChange === 'function' && onToolsChange(name, ids, list, type === 'image' ? type : 'file');
    this.setState({datas: list});
  }
  onUploading(status) {
    this.setState({status: status})
    if (status == "uploading") {
      let timeCount = () => {
        let {percent} = this.state
      if (percent == 80) {
          this.setState({percent: percent + 1, speed: 10000})
        } else if (percent > 80) {
          if (percent >= 98) {
            clearInterval(this.interval)
            this.setState({percent: 98, speed: 500})
          } else {
            this.setState({percent: percent + 1})
          }
        } else {
          this.setState({percent: this.state.percent + 10})
        }
      }
      this.interval = setInterval(timeCount, this.state.speed)
      timeCount()
    }
    if (status == "uploaded") {
      clearInterval(this.interval)
      this.setState({percent: 100, speed: 500, datas: []})
    }
  }
}