import React, { Component } from 'react';
import WeaBrowser from '../../wea-browser';

export default class Browser extends Component {
    constructor(props) {
        super(props)
        this.onChange = this.onChange.bind(this);
    }
    render(){
    	const { show, type } = this.props;
        return (
            <WeaBrowser
                customized={true}
                hasAdvanceSerach={true}
                isSingle={false}
                {...this.props}
                onChange={this.onChange}
            >
                {show || 'no icon'}
            </WeaBrowser>
        )
    }
    onChange(a, b, list) {
        const { onToolsChange, name, type } = this.props;
    	typeof onToolsChange === 'function' && onToolsChange(name, a, list, type);
    }
}
