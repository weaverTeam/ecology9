import "./style/index.less";

class WeaAlertPage extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
	    let {icon, iconSize, paddingTop, top, style} = this.props;
        let iconProps = iconSize ? {style: {fontSize: iconSize + "px", lineHeight: iconSize + "px"}} : {};
        let conProps;
        if (style)
            conProps = {className:"wea-alert-page-con", style: style};
        else if (top)
            conProps = {className:"wea-alert-page-con top40", style: {top: top}};
        else if (paddingTop)
            conProps = {className:"wea-alert-page-con", paddingTop: paddingTop};
        else
            conProps = {className:"wea-alert-page-con top40"};
		return (
			<div className="wea-alert-page">
                <div {...conProps}>
                    <span className="wea-alert-page-icon" {...iconProps}>
                        {icon ?
                            (typeof(icon) === 'string' ?  <span className={icon} /> : {icon}) :
                            <span className="icon-coms-permission">
                                <span className="path1"></span>
                                <span className="path2"></span>
                                <span className="path3"></span>
                                <span className="path4"></span>
                                <span className="path5"></span>
                                <span className="path6"></span>
                                <span className="path7"></span>
                                <span className="path8"></span>
                                <span className="path9"></span>
                                <span className="path10"></span>
                                <span className="path11"></span>
                            </span>
                        }
                    </span>
                    {this.props.children}
                </div>
            </div>
		);
	}
}

// if (window.ecCom) {
//     window.ecCom.WeaAlertPage = WeaAlertPage;
// } else {
//     console.error("无法取到ecCom");
// }
WeaAlertPage.propTypes = {
    icon: React.PropTypes.any,//图标，可以接受传字符串或组件
    iconSize: React.PropTypes.number,//图标大小
    paddingTop: React.PropTypes.string,//与上方的距离。
    top: React.PropTypes.string,//与上方的距离。可以使用百分数，使用此属性时，position会被设为absolute,paddingTop不会生效。默认使用40%。
    style: React.PropTypes.object,//内部整体样式，包括图标.使用此属性时，paddingTop,top不会生效。
};
WeaAlertPage.defaultProps = {
};
export default WeaAlertPage;