import {Menu, message} from 'antd';
// import Menu from '../../_antd1.11.2/menu';
// import message from '../../_antd1.11.2/message';

const Item = Menu.Item

class WeaRightMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      random: `${new Date().getTime()}_${Math.ceil(Math.random() * 100000)}`,
      visible: false,
      targetL: 0,
      targetT: 0,
      eventL: 0,
      eventT: 0,
      wrapL: 0,
      wrapT: 0,
      wrapW: 0,
      wrapH: 0,
      frameOffset: {},
      frameT: 0,
      frameL: 0,
    }
    this.onContextMenu = this.onContextMenu.bind(this);
  }
  componentDidMount(){
  	let { key = ''} = this.props,
      { random, frameOffset } = this.state,
      iframes = jQuery(`#wea_right_menu_wrap_${key}_${random}`).find('iframe');
    iframes.length > 0 && iframes.map((index, frame) => {
    	$(frame.contentWindow).load(() => {
    		// doc ready 事件不稳定。。
    		frame.contentWindow[`wea_right_menu_${key}_${random}_frame_index`.toLocaleUpperCase()] = index;
    		this.setState({ frameOffset: { ...frameOffset, [index]: $(frame).offset() } });
    		frame.contentWindow.document.oncontextmenu = this.onContextMenu;
    		this.onDocClick(frame.contentWindow.document);
    	})
    })
    this.onDocClick(document);
  }
  onDocClick(doc){
  	if(typeof doc.onclick == 'function'){
	    let docOnclick = doc.onclick;
	    doc.onclick = () => {
        let { visible } = this.state;
        visible && this.setState({ visible: false });
        docOnclick();
      }
    }else{
      doc.onclick = () => {
        let { visible } = this.state;
        visible && this.setState({visible: false});
    	}
    }
  }
  onContextMenu(e){
    let isIframe = !e.nativeEvent,
    	{ key = ''} = this.props,
      { random, frameOffset } = this.state,
      targetL = $(e.target).offset().left,
      targetT = $(e.target).offset().top,
      eventL = isIframe ? e.offsetX : e.nativeEvent.offsetX,
      eventT = isIframe ? e.offsetY : e.nativeEvent.offsetY,
      wrap = $(`#wea_right_menu_wrap_${key}_${random}`) || '',
      wrapL = 0,
      wrapT = 0,
      wrapW = 0,
      wrapH = 0,
      _frameOffset = isIframe ? frameOffset[e.view[`wea_right_menu_${key}_${random}_frame_index`.toLocaleUpperCase()]] : { top: 0, left: 0 },
      frameT = _frameOffset.top,
      frameL = _frameOffset.left;
    if(wrap && wrap.offset){
      wrapL = wrap.offset().left,
      wrapT = wrap.offset().top,
      wrapW = wrap[0].offsetWidth,
      wrapH = wrap[0].offsetHeight;
    }
    this.setState({ targetL, targetT, eventL, eventT, wrapL, wrapT, wrapW, wrapH, frameT, frameL, visible: true })
    e.stopPropagation && e.stopPropagation();
    e.preventDefault && e.preventDefault();
    e.nativeEvent && e.nativeEvent.preventDefault();
  }
  render() {
    //datas 菜单数据  属性 icon: 图标 ，disabled: 禁用，content: 显示内容
    let { datas = [], width = 0, children, key = ''} = this.props,
      { visible, random, targetL, targetT, eventL, eventT, wrapL, wrapT, wrapW, wrapH, frameT, frameL } = this.state,
      winW = document.body.clientWidth,
      winH = document.body.clientHeight,
      newWidth = width || (jQuery(`#weaRrightMenu_${key}_${random}`)[0] ? jQuery(`#weaRrightMenu_${key}_${random}`).width() : 400),
      newHeight = jQuery(`#weaRrightMenu_${key}_${random}`)[0] ? jQuery(`#weaRrightMenu_${key}_${random}`).height() : 400,
      left = targetL - wrapL + eventL,
      top = targetT - wrapT + eventT;
      
    left = left >= winW - wrapL - frameL - newWidth ? (winW - wrapL - newWidth -5) : ( left >= wrapW - frameL - newWidth ? (wrapW - newWidth - 2) : (left + frameL));
    top = top >= winH - wrapT - frameT - newHeight ? (winH - wrapT - newHeight -5) : ( top >= wrapH - frameT - newHeight ? (wrapH - newHeight - 2) : (top + frameT));
    let nowStyle = {
      display: visible ? 'block' : 'none',
      position : 'absolute',
      maxWidth: newWidth,
      left,
      top,
    };
    return (
      <div className='wea-right-menu-wrap' id={`wea_right_menu_wrap_${key}_${random}`} onContextMenu={this.onContextMenu}>
	      {children}
	      <div className='wea-right-menu' id={`weaRrightMenu_${key}_${random}`} style={nowStyle} onClick={()=>this.setState({visible: false})}>
	        <iframe className='wea-right-menu-iframe'/>
		      <div className='wea-right-menu-icon-background' />
	        {
	          datas && <Menu mode='vertical' onClick={o => {
	            if(typeof this.props.onClick == 'function') this.props.onClick(o.key)
	            datas.map((d, i)=> {
	              if(d.key === o.key && typeof d.onClick === 'function') d.onClick(o.key)
	            })
	          }}>
	            {
	              datas.map((d, i)=> {
	                return <Item key={d.key || i} disabled={d.disabled}>
	                  <span className='wea-right-menu-icon'>{d.icon}</span>
	                  {d.content}
	                 </Item>
	            })}
	          </Menu>
	        }
        </div>
      </div>
    )
  }
}

export default WeaRightMenu;