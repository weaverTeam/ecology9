import trim from 'lodash/trim'

export const formatToDate = (realVal, formatPattern) => {
    const pattern  =  new RegExp("^\\d{2,4}-\\d{1,2}-\\d{1,2}$");
    if(!pattern.test(realVal)){
        return realVal;
    }
    /**
     * formatPattern
     * 1：yyyy/MM/dd
     * 2：yyyy-MM-dd
     * 3：yyyy年MM月dd日
     * 4：yyyy年MM月
     * 5：MM月dd日
     * 6：EEEE
     * 7：日期大写
     * 8：yyyy/MM/dd hh:mm a
     * 9：yyyy/MM/dd HH:mm
     */
    let arr = realVal.split("-");
    let year =  arr[0];
    let month = arr[1];
    let day = arr[2];
    if(parseInt(month) > 12 || parseInt(day) > 31 || parseInt(year) <= 0 || parseInt(month) <= 0 || parseInt(day) <= 0){
        return realVal;
    }
    if(year.length == 2) year = "00"+year;
    if(year.length == 3) year = "0"+year;
    if(month.length == 1) month = "0"+month;
    if(day.length == 1) day = "0"+day;
    realVal = year + "-" + month + "-" + day;
    if(new Date(realVal).toString().indexOf("undefined") > -1){     //解决IE下类似不存在的日期2014-9-31通过new Date无法生成日期问题，chrome下默认取的下一天
        return realVal;
    }

    switch(parseInt(formatPattern)){
        case 1:
            return formatDate(new Date(realVal),"yyyy/MM/dd");
        case 2:
            return formatDate(new Date(realVal),"yyyy-MM-dd");
        case 3:
            return formatDate(new Date(realVal),"yyyy年MM月dd日");
        case 4:
            return formatDate(new Date(realVal),"yyyy年MM月");
        case 5:
            return formatDate(new Date(realVal),"MM月dd日");
        case 6:
            return formatDate(new Date(realVal),"wwww");
        case 7:
            return dataToChinese(new Date(realVal));
        case 8:
            return formatDate(new Date(realVal),"yyyy/MM/dd 12:00 a");
        case 9:
            return formatDate(new Date(realVal),"yyyy/MM/dd 00:00");
        default:
            return realVal;
    }
}

//日期格式
const formatDate = (date, fmt) =>{
    fmt = fmt || 'yyyy-MM-dd HH:mm:ss';
    const obj = {
        'y': date.getFullYear(), // 年份，注意必须用getFullYear
        'M': date.getMonth() + 1, // 月份，注意是从0-11
        'd': date.getDate(), // 日期
        'q': Math.floor((date.getMonth() + 3) / 3), // 季度
        'w': date.getDay(), // 星期，注意是0-6
        'H': 12, // 24小时制
        'h': 12, // 12小时制
        'm': 0, // 分钟
        's': 0, // 秒
        'S': 0, // 毫秒
        'a': 'AM'
    };
    var week = ['天', '一', '二', '三', '四', '五', '六'];
    for(var i in obj)
    {
        fmt = fmt.replace(new RegExp(i+'+', 'g'), function(m)
        {

            var val = obj[i] + '';
            if(i == 'a') return val;
            if(i == 'w') return (m.length > 2 ? '星期' : '周') + week[val];
            for(var j = 0, len = val.length; j < m.length - len; j++) val = '0' + val;
            return m.length == 1 ? val : val.substring(val.length - m.length);
        });
    }
    return fmt;
}

//日期中文
const dataToChinese = (date) =>{
        const arr =  ["〇", "一", "二", "三", "四", "五", "六", "七", "八", "九"];
        let year  = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        let tempYear = year.toString().split('').map((o)=>{
            return arr[parseInt(o)];
        }).toString();
        return (tempYear + "年" + convertNumToChinese(month) + "月" + convertNumToChinese(day) + "日").replace(/\,/g,'');
}

const convertNumToChinese = (num) =>{
    const arr1 = ["", "", "二", "三"];
    const arr2 = ["", "一", "二", "三", "四", "五", "六", "七", "八", "九"];
    num = num<10 ? "0"+num.toString() : num.toString();
    const first = parseInt(num.toString().split("")[0]);
    const second = parseInt(num.toString().split("")[1]);
    return arr1[first] + (first > 0 ? "十" : "") + arr2[second];
}

export const formatToTime = (realVal, formatPattern) =>{
    const pattern  =  new RegExp("^(\\d{1,2}:\\d{1,2})(:\\d{1,2})?$");
    if(!pattern.test(realVal)){
        return realVal;
    }
    /**
     * formatPattern
     * 1：HH:MI:SS
     * 2：HH:MI:SS AM/PM
     * 3：HH:MI
     * 4：HH:MI AM/PM
     * 5：HH时MI分SS秒
     * 6：HH时MI分
     * 7：HH时MI分SS秒 AM/PM
     * 8：HH时MI分 AM/PM
     **/
    const realValArr = realVal.split(":");
    const hour  = realValArr[0];
    const minute = realValArr[1];
    const separatorChar = ":";
    const suffix  = parseInt(hour) < 12 ? " AM":" PM";
    switch(parseInt(formatPattern)){
        case 1:
            return hour + separatorChar +  minute + separatorChar + "00";
        case 2:
            return hour + separatorChar +  minute + separatorChar + "00" +　suffix;
        case 3:
            return hour + separatorChar +  minute;
        case 4:
            return hour + separatorChar +  minute + suffix;
        case 5:
            return hour + "时" + minute + "分00秒";
        case 6:
            return hour + "时" + minute + "分";
        case 7:
            return hour + "时" + minute + "分00秒" + suffix;
        case 8:
            return hour + "时" + minute + "分" + suffix;
        default:
            return realVal;
    }
}