import {Row, Col, Button, Dropdown, Menu, Progress} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Button from '../../_antd1.11.2/button'
// import Dropdown from '../../_antd1.11.2/dropdown'
// import Menu from '../../_antd1.11.2/menu'
// import Progress from '../../_antd1.11.2/progress';
const Item = Menu.Item

import cloneDeep from 'lodash/cloneDeep'

class WeaTop extends React.Component {
	constructor(props) {
        super(props);
        this.state = {
            showDrop:false,
            percent:85,
            end:true,
            height:0
        }
    }
	scrollheigth(){
		const { heightSpace , getHeight} =  this.props;
		let heightOther = heightSpace || 10;
    	let top = jQuery(".wea-new-top-content").offset() ? (jQuery(".wea-new-top-content").offset().top ? jQuery(".wea-new-top-content").offset().top : 0) : 0;
        let scrollheigth = document.documentElement.clientHeight - top - heightOther;
        this.setState({height:scrollheigth});
        typeof getHeight == 'function' && getHeight(scrollheigth);
    }
	componentDidMount(){
		this.scrollheigth();
		jQuery(window).resize(() => {
		    this._reactInternalInstance !== undefined && this.scrollheigth()
		});
	}
	componentWillReceiveProps(nextProps){
		const {loading} = this.props;
		if(!loading && nextProps.loading){
			this.timer = setInterval(()=>{
				let percent = this.state.percent;
//				console.log('---------',percent);
				let newP = percent < 96 ? (percent + 1) : 96;
//				let newP = percent < 80 ? (percent + 5) : (percent < 95 ? (percent + 1) : 95);
				this._reactInternalInstance !== undefined && this.setState({end:false, percent:newP})
			},10);
		}else if(loading && !nextProps.loading){
			this.setState({percent: 100})
			this.timer && clearInterval(this.timer);
			setTimeout(()=>{this._reactInternalInstance !== undefined && this.setState({percent:85,end:true})},300);
		}
//		console.log(loading,nextProps.loading,end)
//      !loading && nextProps.loading && end && this.setState({percent: 30,end:false});
//      loading && nextProps.loading && !end && this.setState({percent: percent + 10});
//		loading && !nextProps.loading && !end && this.setState({percent: 100});
//		!loading && !nextProps.loading && !end && this.setState({percent: 110,end:true});
    }
	componentDidUpdate(prevProps,prevState){

	}
    render() {
    	const {showDrop,percent,end,height} = this.state;
        const {children, icon, iconBgcolor,title,buttons,isFixed,buttonSpace,loading,showDropIcon,dropMenuDatas} = this.props;//isFixed:wea-new-top是否固定
        const menu = dropMenuDatas ?
        <Menu mode='vertical' onClick={o => {if(typeof this.props.onDropMenuClick == 'function') this.props.onDropMenuClick(o.key)}}>
    		{
    			dropMenuDatas.map((d, i)=> {
        			return <Item key={d.key || i} disabled={d.disabled}>
        				<span className='wea-right-menu-icon'>{d.icon}</span>
        				{d.content}
        			 </Item>
    		})}
    	</Menu> : '';
        return (
        	<div className="wea-new-top-wapper">
	            <Row className={!!isFixed ? "wea-new-top-fixed" : "wea-new-top"}>
	                {!end ? <Progress percent={percent} showInfo={false} strokeWidth={2} status="active"/> : ''}
	                <Col span="14" style={{paddingLeft:20,lineHeight:'50px'}}>
	                    <div className="wea-new-top-title">
	                    	{iconBgcolor ?
		                		<div className="icon-circle-base" style={{background:iconBgcolor ? iconBgcolor : ""}}>
					   				{icon}
								</div>
								:
								<span style={{verticalAlign:'middle',marginRight:10}}>
						   			{icon}
								</span>
	                    	}
							<span style={{verticalAlign:'middle'}}>{title}</span>
	                    </div>
	                </Col>
	                <Col span="10" style={{textAlign:"right",lineHeight:'50px',paddingRight:14}}>
	                {
	                    buttons.map((data)=>{
	                        return (
	                            <span style={{display:'inline-block',lineHeight:'28px',verticalAlign:'middle',marginLeft:!!buttonSpace ? buttonSpace : 10}}>{data}</span>
	                        )
	                    })
	                }
	                {
	                	showDropIcon &&
		                <span className='wea-new-top-drop-btn' onClick={()=>this.setState({showDrop:true})}>
		                	<i className="icon-button icon-New-Flow-menu" />
		                </span>
	                }
	                <div className='wea-new-top-drop-menu wea-right-menu' onMouseLeave={()=>this.setState({showDrop:false})} style={{display:showDrop ? 'block' : 'none'}}>
	                	<span className='wea-new-top-drop-btn' onClick={()=>this.setState({showDrop:false})}>
		                	<i className="icon-button icon-New-Flow-menu" />
		                </span>
		                <div className='wea-right-menu-icon-background'></div>
	                	{menu}
	                </div>
	                </Col>
	            </Row>
	            {
	            	children && <div className='wea-new-top-content' style={{height:height}}>
		            	{children}
		            </div>
	            }
            </div>
        )
    }
}


export default WeaTop;

