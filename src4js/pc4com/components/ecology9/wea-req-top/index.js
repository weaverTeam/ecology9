import {Row, Col, Button, Dropdown, Menu, Progress, Tabs} from 'antd';
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Button from '../../_antd1.11.2/button'
// import Dropdown from '../../_antd1.11.2/dropdown'
// import Menu from '../../_antd1.11.2/menu'
// import Progress from '../../_antd1.11.2/progress';
// import Tabs from '../../_antd1.11.2/tabs'

import WeaTab from '../wea-tab';

import cloneDeep from 'lodash/cloneDeep'
const Item = Menu.Item

class WeaNewTopReq extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showDrop:false,
        percent:85,
        end:true,
        height:0
    }
  }
  scrollheigth(){
    const {heightSpace} =  this.props;
    let heightOther = heightSpace || 0;
    let top = jQuery(".wea-new-top-req-content").offset() ? (jQuery(".wea-new-top-req-content").offset().top ? jQuery(".wea-new-top-req-content").offset().top : 0) : 0;
    let scrollheigth = document.documentElement.clientHeight - top - heightOther;
    this.setState({height:scrollheigth});
  }
  componentDidMount(){
    this.scrollheigth();
    jQuery(window).resize(() => {
      this._reactInternalInstance !== undefined && this.scrollheigth()
    });
  }
  componentWillReceiveProps(nextProps){
    const {loading} = this.props;
    if(!loading && nextProps.loading){
      this.timer = setInterval(()=>{
        let percent = this.state.percent;
//        console.log('---------',percent);
        let newP = percent < 96 ? (percent + 1) : 96;
//        let newP = percent < 80 ? (percent + 5) : (percent < 95 ? (percent + 1) : 95);
        this.setState({end:false, percent:newP})
      },10);
    }else if(loading && !nextProps.loading){
      this.setState({percent: 100})
      this.timer && clearInterval(this.timer);
      setTimeout(()=>{this.setState({percent:85,end:true})},300);
    }
//    console.log(loading,nextProps.loading,end)
//      !loading && nextProps.loading && end && this.setState({percent: 30,end:false});
//      loading && nextProps.loading && !end && this.setState({percent: percent + 10});
//    loading && !nextProps.loading && !end && this.setState({percent: 100});
//    !loading && !nextProps.loading && !end && this.setState({percent: 110,end:true});
  }
  render() {
    const {showDrop,percent,height,end} = this.state;
    const {children, icon = '', iconBgcolor = '',title = '',buttons=[], buttonSpace = 0,loading,selectedKey = [],tabDatas = [],showDropIcon,dropMenuDatas = [], replaceTab = ''} = this.props;//isFixed:wea-new-top-req是否固定
    const menu = dropMenuDatas ?
      <Menu mode='vertical' onClick={o => {if(typeof this.props.onDropMenuClick == 'function') this.props.onDropMenuClick(o.key)}}>
        {
          dropMenuDatas.map((d, i)=> {
              return <Item key={d.key || i} disabled={d.disabled}>
                <span className='wea-right-menu-icon'>{d.icon}</span>
                {d.content}
               </Item>
        })}
      </Menu> : '';
    let isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
    let isIE9 = window.navigator.appVersion.indexOf("MSIE 9.0") >= 0;
//    console.log(percent);
    return (
      <div className="wea-new-top-req-wapper">
        <Row className="wea-new-top-req">
          {!end ? <Progress percent={percent} showInfo={false} strokeWidth={2} status="active"/> : ''}
            <Col className="wea-new-top-req-main" span={24} >
              <div className='wea-new-top-req-icon'>
                { iconBgcolor ?
                  <div className="icon-circle-base" style={{background:iconBgcolor ? iconBgcolor : ""}}>
                     {icon}
                  </div>
                  :
                  <span style={{verticalAlign:'middle',marginRight:10}}>
                     {icon}
                  </span>
                }
              </div>
              <div className="wea-new-top-req-title-text">{title}</div>
              <Row className="wea-new-top-req-title">
                <Col xs={isIE8 ? 10 : 6} sm={6} md={8} lg={10}>
                  {
                    replaceTab ?
                    <div style={{paddingLeft: 8}}>{replaceTab}</div>
                    :
                    <Tabs defaultActiveKey={selectedKey} activeKey={selectedKey}
                      onChange={this.onChange.bind(this)} >
                      {
                        tabDatas && tabDatas.map(data=>{
                          return <Tabs.TabPane tab={data.title} key={data.key} />
                        })
                      }
                    </Tabs>
                  }
                </Col>
                <Col xs={isIE8 ? 14 : 18} sm={18} md={16} lg={14} style={{textAlign:"right",marginTop:-5}}>
                {
                  buttons.map((data)=>{
                    return (
                      <span style={{display:'inline-block',lineHeight:'28px',verticalAlign:'middle',marginLeft:!!buttonSpace ? buttonSpace : 10}}>{data}</span>
                    )
                  })
                }
                {
                  showDropIcon &&
                  <span className='wea-new-top-req-drop-btn' onClick={()=>this.setState({showDrop:true})}>
                    <i className="icon-button icon-New-Flow-menu" />
                  </span>
                }
                <div className='wea-new-top-req-drop-menu wea-right-menu' onMouseLeave={()=>this.setState({showDrop:false})} style={{display:showDrop ? 'block' : 'none'}}>
                  <span className='wea-new-top-req-drop-btn' onClick={()=>this.setState({showDrop:false})}>
                    <i className="icon-button icon-New-Flow-menu" />
                  </span>
                  <div className='wea-right-menu-icon-background'></div>
                  {menu}
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
        {
          children && <div className='wea-new-top-req-content' style={{height:height}}>
            {children}
          </div>
        }
      </div>
    )
  }
  onChange(key){
    if (typeof (this.props.onChange) == "function") {
      this.props.onChange(key);
    }
  }
}

export default WeaNewTopReq;