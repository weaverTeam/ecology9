import {InputNumber, Modal, Icon} from 'antd';
// import InputNumber from '../../_antd1.11.2/input-number';
// import Input from '../../_antd1.11.2/input';
// import Modal from '../../_antd1.11.2/modal';
const confirm = Modal.confirm;
// import Icon from '../../_antd1.11.2/Icon';
import isEmpty from 'lodash/isEmpty';

class Main extends React.Component {
  static propTypes={
    startValue:React.PropTypes.number,
    endValue:React.PropTypes.number,
  }
  constructor(props){
    super(props)
    const sv = (props.startValue==0 || props.startValue) ? props.startValue : '';
    const ev = (props.endValue==0 || props.endValue) ? props.endValue : '';
    this.state={
      startValue:sv,
      endValue: ev,
      minFrom:!!props.min&&props.min[0]&&props.min[0],
      minTo:!!props.min&& props.min[1]&&props.min[1],
      maxFrom:!!props.max&&props.max[0]&&props.max[0],
      maxTo:!!props.max&&props.max[1]&&props.max[1],
    }
    if (!isEmpty(props.values) && !isEmpty(props.domkey)) {
            if (props.values[props.domkey[0]]) {
                this.state.startValue = props.values[props.domkey[0]]
            }
            if (props.values[props.domkey[1]]) {
                this.state.endValue = props.values[props.domkey[1]]
            }
        }
  }
  // componentWillReceiveProps(nextProps){
  //     if (this.props.value !== nextProps.value && isEmpty(nextProps.value)) {
  //         const {domkey, form} = this.props;
  //         if (domkey && form) {
  //             let obj = {};
  //             obj[domkey[1]] = '';
  //             form.setFieldsValue(obj);
  //         }
  //         this.setState({startValue: '', endValue: ''});
  //     }
  // }
  onChangeL=(value)=> {
    const {domkey,form}=this.props;
    const { endValue } = this.state;
      let obj = {};
    obj[domkey[0]] = value;
    form && form.setFieldsValue(obj);
    this.setState({startValue:value});
    !form && this.props.onChange && this.props.onChange(value,endValue)
  }
  onChangeR=(value)=> {
    const {domkey,form}=this.props;
    const { startValue } = this.state;
      let obj = {};
    obj[domkey[1]] = value;
    form && form.setFieldsValue(obj)
    this.setState({endValue:value});
    !form && this.props.onChange && this.props.onChange(startValue,value)

  }
  render(){
    const {minFrom,maxFrom,minTo,maxTo,startValue,endValue} = this.state;
    const {domkey,step,form,viewAttr}=this.props;
    const initStartValue = form && form.getFieldValue(domkey[0]);
    const initEndValue = form && form.getFieldValue(domkey[1]);

    let startDefaultProps = {},endDefaultProps={};
    minFrom && (startDefaultProps['min'] = minFrom);
    minTo && (startDefaultProps['max'] = minTo);
    startDefaultProps['value'] = (startValue==0 || startValue) ? startValue : initStartValue;
    step && (startDefaultProps['step'] = step);

    maxFrom && (endDefaultProps['min'] = maxFrom);
    maxTo && (endDefaultProps['max'] = maxTo);
    endDefaultProps['value'] = (endValue==0 || endValue) ? endValue : initEndValue;
    step && (endDefaultProps['step'] = step);

     // const clsname = classNames({
   //          'required': viewAttr=='3' && (isEmpty(startValue) || isEmpty(endValue)
   //      })
    return(
      <div className='wea-scope'>
        <InputNumber  style={{width:100}} {...startDefaultProps}  onChange={this.onChangeL} />
        <span style={{paddingRight:4}}>-</span>
        <InputNumber style={{width:100}} {...endDefaultProps}  onChange={this.onChangeR} />
      </div>
     );
  }
}

export  default Main;