import {DatePicker} from 'antd';
// import DatePicker from '../../_antd1.11.2/date-picker'
import cloneDeep from 'lodash/cloneDeep'
import isEmpty from 'lodash/isEmpty';
import classNames from 'classnames';
import './index.less';
import WeaTools from '../wea-tools';

class Main extends React.Component {
    /**
     * formatPattern
     * 1：yyyy/MM/dd
     * 2：yyyy-MM-dd
     * 3：yyyy年MM月dd日
     * 4：yyyy年MM月
     * 5：MM月dd日
     * 6：EEEE
     * 7：日期大写
     * 8：yyyy/MM/dd hh:mm a
     * 9：yyyy/MM/dd HH:mm
    */
    static defaultProps={
        format:"yyyy-MM-dd",
        viewAttr:2,//显示属性 1(只读)/2(可编辑)/3(必填)
        formatPattern: 2,
        noInput: false,
	}
	static propTypes={
		promptRequiredMark: React.PropTypes.number,
	}
    constructor(props) {
		super(props);
		this.state = {
		  value: 'value' in props ? props.value : null,
		}
	}
	componentWillReceiveProps(nextProps) {
		if('value' in nextProps && this.state.value !== nextProps.value) {
			this.setState({ value: nextProps.value });
		}
	}
    onChange=(value,valuestring)=> {
        this.setState({value:valuestring});
        typeof this.props.onChange == 'function' && this.props.onChange(valuestring);
    }
    disabledDate(v) {
        const {startValue, endValue} = this.props;
        let bool = false;
        if (startValue) {
            bool = v.getTime() < new Date(startValue).getTime();
            if (bool) return bool;
        }
        if (endValue) {
            bool = v.getTime() > new Date(endValue).getTime();
        }
        return bool;
    }
    isReadOnly() {
        const {viewAttr} = this.props;
        return viewAttr === 1 || viewAttr === '1';
    }
    render() {
    	const {viewAttr, isDetail, fieldName, noInput, hasBorder, underline}=this.props;
    	const req = classNames({
            'required': viewAttr==3 && !this.state.value,
            'noInput': noInput,
        });
        const style = this.props.style || {};
        style.textDecoration = 'none';
        delete this.props.style;
        if (this.isReadOnly()) {
            const readonly = classNames({
                'border': hasBorder,
                'underline': underline,
            });
            return (
                <span className={`wea-field-readonly ${readonly}`} style={style}>
                    <span className="child-item wdb">{WeaTools.formatDate(this.state.value, this.props.formatPattern)}</span>
                    <input type="hidden" id={fieldName} name={fieldName} value={WeaTools.formatDate(this.state.value, this.props.formatPattern)} />
                </span>
            )
        }
        return (
        	<span className={`wea-date-picker ${req}`} style={style}>
        	    <DatePicker
                    {...this.props}
                	getCalendarContainer={() => (this.props.layout || document.body)}
                	onChange={this.onChange}
                	value={this.state.value}
                    formatPattern={this.props.formatPattern || 2}
                    disabledDate={this.disabledDate.bind(this)}
                />
                <input type="hidden" id={fieldName} name={fieldName} value={this.state.value} />
        	</span>

        )
    }
}

export default Main;