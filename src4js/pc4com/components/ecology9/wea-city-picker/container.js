import { findDOMNode } from 'react-dom';
import addEventListener from 'rc-util/lib/Dom/addEventListener';
// import throttle from 'lodash/throttle'

 export default class Main extends React.Component{
    constructor(props) {
        super(props);
        let funcs = ['test','scrollheigth','renderLayer','unrenderLayer']
        funcs.forEach(f=>this[f]= this[f].bind(this))
    }
    componentDidMount() {
        this.componentDidUpdate()
        jQuery(window).resize(() => {
        	this._reactInternalInstance !== undefined && this.scrollheigth()
		});
    }
    scrollheigth(){
        let {parentClass='wea-city-picker-58',refId}= this.props;
        let offset = jQuery('.'+parentClass).offset();
        let top =  offset ? (offset.top ? offset.top : 0) : 0;
        let left =  offset ? (offset.left ? offset.left : 0) : 0;
        jQuery("#wea-city-card-container-"+refId).css({top:top+25,left:left});
    }
    componentDidUpdate() {
        const {props} = this;
        this.renderLayer();
        this.scrollheigth();

        let currentDocument;
        if (!this.clickOutsideHandler && props.visible ) {
            currentDocument = props.layout || window.document;
            this.clickOutsideHandler = addEventListener(currentDocument,'scroll', this.test);
        }
    }
    test(){
        this.scrollheigth()
    }
    componentWillUnmount() {
        this.unrenderLayer();
        if (this.clickOutsideHandler) {
          this.clickOutsideHandler.remove();
          this.clickOutsideHandler = null;
        }
    }
    unrenderLayer() {
        if(!this.layer){
            return;
        }
        ReactDOM.unmountComponentAtNode(this.layer); //销毁指定容器内的所有React节点
        document.body.removeChild(this.layer);
        this.layer=null;
    }
    renderLayer() {//自定义渲染
        const {props} = this;
        if(!props.visible){
            this.unrenderLayer()//卸载
            return;
        }
        if(!this.layer){
            this.layer=document.createElement('div');
            this.layer.className='wea-city-card-container';
            this.layer.id = 'wea-city-card-container-'+props.refId;
            document.body.appendChild(this.layer);
            // console.log('findDOMNode',findDOMNode(props.layout).getAttribute&&findDOMNode(props.layout).getAttribute('class'))
        }
        ReactDOM.unstable_renderSubtreeIntoContainer(this,this.props.children,this.layer)
    }
    render() { 
        return null;
    }
 }
