import {Tabs, Row, Col} from 'antd';
import WeaAssociativeSearch from '../base/wea-associative-search';
import addEventListener from 'rc-util/lib/Dom/addEventListener';
import T from 'prop-types';
// import  Tabs from '../../_antd1.11.2/Tabs';
import WeaTools from '../wea-tools';
const TabPane = Tabs.TabPane;
// import Row from '../../_antd1.11.2/Row';
// import Col from '../../_antd1.11.2/Col';
import message from '../../_antd1.11.2/message';
import classNames from 'classnames';
import isEmpty from 'lodash/isEmpty'
import isArray from 'lodash/isArray'
import Container from './container'
import './style/index.less'

class Main extends React.Component {
  static propTypes={
    value:T.string,
    replaceDatas:T.array,
  }
  constructor(props) {
    super(props);
    let defaultId = '',defaultName = '',activeKey='';
    const isCountryBrowser = props.type=='258';
    const isProvBrowser = props.type=='2222';
    const isCityBrowser = props.type=='58';
    const isDistrictBrowser = props.type=='263';

    if(isArray(props.replaceDatas) && props.replaceDatas.length>0){
      const rD = props.replaceDatas;
      defaultId = rD[rD.length-1].id;
      defaultName = rD[rD.length-1].name;
      isCountryBrowser && (activeKey='coun');
      isProvBrowser && (activeKey='prov');
      isCityBrowser && (activeKey='city');
      isDistrictBrowser && (activeKey='district');
    }
    this.state={
        id:defaultId,
        name:defaultName,
        url:props.dataUrl,
        isClearData: false,
        visible:false,
        countrys:[],
        provs:[],
        citys:[],
        districts:[],
        counSelected:'',
        provsSelected:'',
        citySelected:'',
        districtSelected:'',
        activeKey:activeKey,
    }
    this.hideSearchAd = this.hideSearchAd.bind(this);
    this.alertModel = this.alertModel.bind(this);
    this.tabsChange = this.tabsChange.bind(this);
  }
  componentWillReceiveProps(nextProps, nextState) {
    if (this.props.value !== nextProps.value && isEmpty(nextProps.value)) {
        this.setState({id: '', name: '', isClearData: !this.state.isClearData});
    }
  }
  hideSearchAd(){
    this.setState({visible:false})
  }
  fetchDefaultShowDatas(){
    const {url,id,counSelected,provsSelected,citySelected,districtSelected,activeKey} = this.state;
    WeaTools.callApi(`${url}${WeaTools.getFd({from:'1',id:id})}&`,'GET').then(datas => {
        const result = datas.datas;
        const countrys = result.glob;
        const provs = result.coun || [];
        const citys = result.prov || [];
        const districts = result.city || [];
        this.setState({countrys,provs,citys,districts});
    }).catch(err=>{
        message.error(err);
    })
  }
  fetchCountryDatas(){
    const {url,counSelected}=this.state;
    const isCountryBrowser = this.props.type=='258';
    WeaTools.callApi(url,'GET').then(c => {
        this.setState({countrys:c.datas});
        if(!isCountryBrowser && c.id == counSelected){
          WeaTools.callApi(`${url}${WeaTools.getFd(c)}&`,'GET').then(prov=>{
            this.setState({provs:prov.datas});
          });
        }
    });
  }
  alertModel(){
    const {replaceDatas,type}=this.props;
    const {id} = this.state;
      this.setState({visible:!this.state.visible});
      this.fetchCountryDatas();
      id && this.fetchDefaultShowDatas();
      if(isArray(replaceDatas)&&replaceDatas.length==0||!replaceDatas){
        !id && this.countryClick(null,{type:"coun",id:1,isParent:true});//默认中国
        this.setState({
          visible:!this.state.visible,
          activeKey:this.state.activeKey || 'prov',
        })
      }
  }
  tabsChange(activeKey){
    this.setState({activeKey})
  }
  selectedSearch = (id,name)=>{
    if (this.props.onChange) {
      this.props.onChange(id,name);
    }
  }
  renderAsssosiate=()=>{
    const {type,value}=this.props;
    const {id,name,visible} = this.state;
    return (
       <WeaAssociativeSearch
            {...this.props}
            isCity
            style={this.props.inputStyle}
            isClearData={this.state.isClearData}
            type={type}
            ids={id}
            names={name}
            modalVisable={visible}
            clickCallback={this.alertModel}
            onChange={this.selectedSearch}
            ref={r=>this.Ass=r}
        />
      );
  }
  countryClick=(e,obj)=>{
    const {type} = this.props;
    const isCountryBrowser = type=='258';
    !!e && e.stopPropagation();
    let url = this.state.url;
    url = `${url}${WeaTools.getFd({...obj})}&`;
    if(isCountryBrowser){
       if(typeof this.props.onChange==='function') {
        this.props.onChange(obj.id,obj.name);
      }
      this.setState({
          counSelected:obj.id,
          id:obj.id,
          name:obj.name,
          visible:false,
      });
      return;
    }
    if(obj.isParent){
      WeaTools.callApi(url,'GET').then(datas => {
        // console.log('countryClick---datas',datas)
        this.setState({
          counSelected:obj.id,
          provs:datas.datas,
          activeKey:'prov',
        })
      });
    }else{
        this.setState({
          counSelected:obj.id,
          provs:[],
          activeKey:'prov',
        })
    }
  }
  provClick=(e,obj)=>{
    const {type} = this.props;
    const isProvBrowser = type=='2222';
    !!e && e.stopPropagation();
    // console.log('provClick==>',obj);
    let url = this.state.url;
    url = `${url}${WeaTools.getFd({...obj})}&`;
    if(isProvBrowser){
      this.setState({
          provsSelected:obj.id,
          id:obj.id,
          name:obj.name,
          visible:false,
      });
      return;
    }
    if(obj.isParent){
      WeaTools.callApi(url,'GET').then(datas => {
        this.setState({
          provsSelected:obj.id,
          citys:datas.datas,
          activeKey:'city',
        })
      });
    }
  }
  cityClick=(e,obj)=>{
    !!e && e.stopPropagation();
    // console.log('cityClick==>obj',obj);
    let url = this.state.url;
    url = `${url}${WeaTools.getFd({...obj})}&`;
    if(this.props.type=='58'){
      if(typeof this.props.onChange==='function'){
        this.props.onChange(obj.id,obj.name);
      }
      this.setState({
          citySelected:obj.id,
          id:obj.id,
          name:obj.name,
          visible:false,
      })
    }
    if(this.props.type == '263'){
      if(obj.isParent){
        WeaTools.callApi(url,'GET').then(datas => {
          this.setState({
            citySelected:obj.id,
            districts:datas.datas,
            activeKey:'district',
          })
        });
      }
    }
  }
  districtClick = (e,obj)=>{
    !!e && e.stopPropagation();
    // console.log('districtClick==>obj',obj);
    if(typeof this.props.onChange==='function'){
      this.props.onChange(obj.id,obj.name);
    }
    this.setState({
        districtSelected:obj.id,
        id:obj.id,
        name:obj.name,
        visible:false,
    })
  }
  renderTabs=()=>{
    const {countrys,provs,citys,districts,activeKey,counSelected,provsSelected,citySelected,districtSelected}=this.state;
    const {type} = this.props;
    const style={
      cursor:'pointer'
    }
    const coountry = (
        <TabPane tab="国家" key="coun">
          <Row>
            {
             !isEmpty(countrys)&& countrys.map(item =>{
                const className = classNames({
                  spantag:true,
                  selected:counSelected==item.id || item.selected
                });
                return (
                    <Col span={4}>
                      <span className={className} {...item} style={style}  onClick={(e)=>{this.countryClick(e,item)}} >
                      {item.name}
                      </span>
                    </Col>
                );
              })
            }
          </Row>
        </TabPane>
      );
    const prov = (
      <TabPane tab="省份" key="prov">
          <Row>
            {
             !isEmpty(provs)&& provs.map(item =>{
                const className = classNames({
                  spantag:true,
                  selected:provsSelected==item.id || item.selected
                });
                return (
                  <Col span={4}>
                    <span className={className} {...item} style={style}  onClick={(e)=>{this.provClick(e,item)}} >
                      {item.name}
                    </span>
                  </Col>
                );
              })
            }
         </Row>
        </TabPane>
    );
    const city = (
      <TabPane tab="城市" key="city">
          <Row>
            {
             !isEmpty(citys)&& citys.map(item =>{
                const className = classNames({
                  spantag:true,
                  selected:citySelected==item.id || item.selected
                });
                return (
                  <Col span={4}>
                    <span className={className} {...item} style={style}  onClick={(e)=>{this.cityClick(e,item)}} >
                      {item.name}
                    </span>
                  </Col>
                );
              })
            }
          </Row>
        </TabPane>
    );
    const district = (
      <TabPane tab="区县" key="district">
         <Row>
              {
               !isEmpty(districts)&& districts.map(item =>{
                  const className = classNames({
                    spantag:true,
                    selected:districtSelected==item.id || item.selected
                  });
                  return (
                    <Col span={4}>
                      <span className={className} {...item} style={style}  onClick={(e)=>{this.districtClick(e,item)}} >
                        {item.name}
                      </span>
                    </Col>
                  );
                })
              }
            </Row>
        </TabPane>
    );
    let TabPaneArr = [];
    const coy = type == "258";
    const pv = type == "2222";
    const ciy = type == "58";
    const dist = type == "263";
    coy && TabPaneArr.push(coountry);
    pv && TabPaneArr.push([coountry,prov]);
    ciy && TabPaneArr.push([coountry,prov,city]);
    dist && TabPaneArr.push([coountry,prov,city,district]);
    return (
      <Tabs type="card" activeKey={activeKey} onChange={this.tabsChange}>
       {TabPaneArr}
      </Tabs>
    );
  }
  render(){
    const {countrys,provs,citys,visible,}=this.state;
    const {type,layout,isAdSearch} = this.props;
    const assProps = this.Ass ? this.Ass.props : '';
    const refId = this.Ass ? this.Ass._reactInternalInstance._rootNodeID.replace(/[^a-zA-Z0-9]+/g,'') : '';
    const cn = `wea-city-picker-${refId}`;//唯一标识 for 定位
    return (
        <div className={`${cn}`}>
          {this.renderAsssosiate()}
          <Container layout={layout} getPopupContainer={()=> layout || document.body} visible={visible} parentClass={cn} refId={refId}>
            { this.renderTabs()}
          </Container>
          {isAdSearch && <div className="wea-city-picker-modal-mask" style={{display:visible ? 'block' : 'none'}} onClick={this.hideSearchAd}/>}
        </div>
    );
  }
}

export default Main;

