import WeaWorkflowHrRadio from '../base/wea-workflow-hr-radio';
import WeaHrMutiInput from '../base/wea-hr-muti-input'
import PureRenderMixin from 'react-addons-pure-render-mixin';

export default class main extends React.Component {
	shouldComponentUpdate(...args) {
		return PureRenderMixin.shouldComponentUpdate.apply(this, args);
	}
	render() {
		if (this.props.mult) {
			return (
				<WeaHrMutiInput
					checkGroup
					title="人员"
					type={17}//联想搜索
					icon={<i className='icon-portal-hrm' />}
					dropMenuIcon={<i className='icon-toolbar-Organization-list' />}
					iconBgcolor='#68a54c'
					topPrefix="hr"
					prefix="ry"
					searchUrl="/api/ec/api/hrm/search"//搜索
					recentUrl="/api/ec/api/hrm/newly"//最近
					sameDepUrl="/api/ec/api/hrm/underling"//同部门
					mySubUrl="/api/ec/api/hrm/branch" //我的下属
					resourcetree="/api/ec/api/hrm/v2/resourcetree"//按照组织结构
					virtualUrl="/api/ec/api/hrm/companyvirtual"//维度
					grouptree="/api/ec/api/hrm/v2/grouptree"  // 常用组"/cloudstore/system/groupTree.json"
					dataKey="id"
					isaccount={1}//是否显示无账号人员
					alllevel={1}//是否显示子成员
					{...this.props}
					/>
			)
		} else {
			return (
				<WeaWorkflowHrRadio
					title="人员"
					type=''//联想搜索
					icon={<i className='icon-portal-hrm' />}
					dropMenuIcon={<i className='icon-toolbar-Organization-list' />}
					iconBgcolor='#68a54c'
					topPrefix="hr"
					prefix="ry"
					searchUrl="/api/ec/api/hrm/search"//搜索
					dataUrl= "/cloudstore/system/ControlServlet.jsp"//旧的人力资源接口
					otherPara={["action","Action_GetTree4Hrm"]}
					recentUrl="/api/ec/api/hrm/newly"//最近
					sameDepUrl="/api/ec/api/hrm/underling"//同部门
					mySubUrl="/api/ec/api/hrm/branch" //我的下属
					resourcetree="/api/ec/api/hrm/v2/resourcetree"//按照组织结构
					virtualUrl="/api/ec/api/hrm/companyvirtual"//维度
					grouptree="/api/ec/api/hrm/v2/grouptree"  // 常用组"/cloudstore/system/groupTree.json"
					dataKey="id"
					isaccount={1}//是否显示无账号人员
					alllevel={1}//是否显示子成员
					min={1}//分页参数最小
					max={1000}//分页参数最大
					ifAsync={true}
					ifCheck={true}
					{...this.props}
					/>
			)
		}
	}
}

/*

const WeaInput4Hrm = React.createClass({
	render() {
		return  <WeaInput4Base
				title="人力资源"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="gs"
				prefix="ry"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Hrm"]}
				isMult={this.props.isMult}
				ifAsync={true}
				ifCheck={true}
				style={this.props.style}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				value={this.props.value}
				valueName={this.props.valueName}
				NoFormItem={this.props.NoFormItem}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

export default WeaInput4Hrm;

*/