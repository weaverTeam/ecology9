import WeaInput4Base from '../base/wea-input4-base'
import WeaWorkflowRadio from '../base/wea-workflow-radio'
export default class WeaInput4WtNew extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		//console.log("WeaInput4Wf props:",this.props);
		return (
			<WeaWorkflowRadio
				title="选择路径类型"
				type='worktypeBrowser'//联想搜索
				icon={<i className='icon-portal-workflow' />}
				iconBgcolor='#55D2D4'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata"//dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","wftypelist"]} //otherPara={["action","BrowserAction","actiontype","wftypelist"]}
				{...this.props}
				/>

			/*<WeaInput4Base
				title="流程类型"
				topPrefix="wt"
				prefix="wt"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Wt"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props}
			/>*/
		)
	}
}


/*

const WeaInput4Com = React.createClass({
	render() {
		return  <WeaInput4Base
				title="流程类型"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="wt"
				prefix="wt"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Wt"]}
				isMult={this.props.isMult}
				style={this.props.style}
				value={this.props.value}
				valueName={this.props.valueName}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				ifAsync={true}
				ifCheck={true}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

export default WeaInput4Com;

*/