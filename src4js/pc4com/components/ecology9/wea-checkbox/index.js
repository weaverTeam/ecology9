import './style/index.css'
import {Checkbox} from 'antd';
// import Checkbox from '../../_antd1.11.2/Checkbox'
import PureRenderMixin from 'react-addons-pure-render-mixin';

class main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			value: props.value ? props.value : '0'
		};
	}
	componentWillReceiveProps(nextProps, nextState){
        if (this.props.value !== nextProps.value) {
            this.setState({value: nextProps.value});
        }
    }
	doChange(e){
		const value  = e.target.checked ? '1':'0';
		this.setState({value});
		this.props.onChange && this.props.onChange(value);
	}
	shouldComponentUpdate(...args) {
        return PureRenderMixin.shouldComponentUpdate.apply(this, args);
    }
	render() {
		const {viewAttr,fieldName} = this.props;
		const {value} = this.state;
		return (
			<div className="wea-checkbox">
				<Checkbox disabled = {viewAttr === 1} checked={value == '1'} onChange={this.doChange.bind(this)}/>
				<input type='hidden' id={fieldName} name={fieldName} value={value}/>
			</div>
		)
	}
}

export default main