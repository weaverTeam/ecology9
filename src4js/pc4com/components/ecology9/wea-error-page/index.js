
class ErrorPage extends React.Component {
	render() {
        const {msg} = this.props;
		return (
            <div style={{height:"100%"}}>
                <div style={{position:"absolute",top:"35%",left:"40%",textAlign:"center"}}>
                        <img src="/cloudstore/images/e9/error.png" />
                        <div style={{textAlign:"center"}}>{msg}</div>
                </div>
            </div>
		)
	}
}

export default ErrorPage