import './index.less';
import {Icon} from 'antd';
// import Icon from '../../_antd1.11.2/Icon'
import WeaNewScroll from '../wea-new-scroll';

class main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showAddbtn: true,
        }
    }
    close() {
        this.props.onVisibleChange(false);
    }
    showAll(e) {
        this.setState({showAddbtn: false});
        e.preventDefault();
    }
    componentWillReceiveProps(nextProps, nextState){
        if ('visible' in this.props) {
            if (nextProps.visible) this.setState({showAddbtn: true});
        }
    }
    openHrm(e, item) {
        window.openhrm(item.id);
        window.pointerXY(e.nativeEvent);
    }
    render() {
        const {list=[], style = {}, visible} = this.props;
        const {showAddbtn} = this.state;
        const scrollProps = {};
        if (list && list.length > 6 && !showAddbtn) {
            scrollProps.height = 197;
        }
        const cls = visible? 'wea-show' : 'wea-hide';
        return (<div className={`wea-hrmlist ${cls}`} style={style}>
            <WeaNewScroll {...scrollProps}>
                <ul>
                    {
                        list && list.map((item, index) => {
                            if (showAddbtn && list && list.length > 6 && index > 4) {
                                return;
                            }
                            return <li>
                                <span
                                    className="name text-overflow cursor-pointer"
                                    onClick={(e)=> {
                                        e.preventDefault();
                                        e.stopPropagation();
                                        this.openHrm(e, item);
                                    }}
                                    title={item.name || item.lastname}>
                                    {item.name || item.lastname}
                                </span>
                                <span className="jobname text-overflow" title={item.jobtitlename}>{item.jobtitlename}</span>
                            </li>
                        })
                    }
                    {showAddbtn && list && list.length > 6 && <li className="align-center cursor-pointer" onClick={this.showAll.bind(this)}>显示全部</li>}
                </ul>
            </WeaNewScroll>
        </div>);
    }
}

export default main;