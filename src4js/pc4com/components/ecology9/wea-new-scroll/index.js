const Baron = require('./react-baron');
import { findDOMNode } from 'react-dom';

class WeaNewScroll extends React.Component {
    constructor(props) {
        super(props);
    }
    scrollerToTop() {
        if (this.refs.weaBaron.getScroller()) this.refs.weaBaron.getScroller().scrollTop = 0;
        if (this.refs.weaBaron.getBar()) this.refs.weaBaron.getBar().top = 0;
    }
    render() {
        return (
            <div className="wea-new-scroll" ref="weaNewScroll" style={{height: this.props.height}} id={this.props.scrollId}>
                <Baron $={jQuery}
                    {...this.props}
                    ref="weaBaron"
                    clipperCls="clipper"
                    scrollerCls="scroller"
                    trackCls="track"
                    barCls="bar"
                    barOnCls="baron">
                        {this.props.children}
                </Baron>
            </div>
        );
    }
    update = () => {
    	this.refs.weaBaron && this.refs.weaBaron.baron.update();
    }
}

export default WeaNewScroll