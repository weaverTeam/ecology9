import {Row, Col, Icon} from 'antd';
// import Row from '../../_antd1.11.2/Row'
// import Col from '../../_antd1.11.2/Col'
// import Icon from '../../_antd1.11.2/icon'

import cloneDeep from 'lodash/cloneDeep'

class WeaLeftRightLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLeft:props.defaultShowLeft || false,
            isHover:false
        }
    }
    leftShowCo(e){
    	const {showLeft} = this.state;
    	this.setState({showLeft:!showLeft});
    	e.stopPropagation();
    	e.preventDefault();
    	e.nativeEvent.preventDefault();
    }
    scrollheigth(){
//  	let top = jQuery(".wea-tree-search-layout .wea-right").offset() ? (jQuery(".wea-tree-search-layout .wea-right").offset().top ? jQuery(".wea-tree-search-layout .wea-right").offset().top : 0) : 0;
//      let scrollheigth = document.documentElement.clientHeight - top - 20;
//      jQuery(".wea-tree-search-layout .wea-right").height(scrollheigth);
    }
	componentDidMount(){
		this.scrollheigth();
	}
    render() {
        const {showLeft,height,isHover} = this.state;
        const {children,leftCom,leftWidth} = this.props;
        //判断IE8,禁用部分功能
	    let isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
	    let isIE9 = window.navigator.appVersion.indexOf("MSIE 9.0") >= 0;
	    const leftCol = showLeft ? {
		    	xs: isIE8 ? 6 : 10,
		    	sm: 8,
		    	md: 6,
		    	lg: 5
	    	} : {
		    	xs: 0,
		    	sm: 0,
		    	md: 0,
		    	lg: 0
	    	};
	    const rightCol = showLeft ? {
		    	xs: isIE8 ? 18 : 14,
		    	sm: 16,
		    	md: 18,
		    	lg: 19
	    	} : {
		    	xs: 24,
		    	sm: 24,
		    	md: 24,
		    	lg: 24
	    	};
        return (
            <Row className="wea-tree-search-layout">
                <Col {...leftCol} className="wea-left">
                    {leftCom}
                </Col>
                <Col {...rightCol} className="wea-right">
                    <div className='wea-tree-search-layout-show-left'
	                    onClick={this.leftShowCo.bind(this)}
	                    onMouseEnter={()=>this.setState({isHover:true})}
	                    onMouseLeave={()=>this.setState({isHover:false})}
	                    style={{background: `url('/cloudstore/images/e9/leftTree-${showLeft ? "show" : "hide"}${isHover ? "-hover" : ""}.png') no-repeat -2px 0`}}
                    ></div>
                    {children}
                </Col>
            </Row>
        )
    }
}

export default WeaLeftRightLayout;

/*

<Row className="wea-tree-search-layout">
                <Col span="6" className="wea-left">
                    {leftCom}
                </Col>
                <Col span="18" className="wea-right">
                    {children}
                </Col>
            </Row>

*/