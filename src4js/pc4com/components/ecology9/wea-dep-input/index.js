import WeaWorkflowRadio from '../base/wea-workflow-radio'

export default class main extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="部门"
				singleTabName='按组织结构'
				type='4'//联想搜索
				icon={<i className='icon-toolbar-Organization-list' />}
				iconBgcolor='#68a54c'
				contentType="tree"
				topPrefix="wt"
				prefix="wf"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"	 dataKey="psubcompanyid"
				otherPara={["actiontype","orgtree","isLoadSubDepartment","1","isLoadAllSub","0"]}//otherPara={["action","BrowserAction","actiontype","orgtree","isLoadSubDepartment","1","isLoadAllSub","0"]}
				ifAsync={true}
				asyncUrl="/api/workflow/browser/browserdata?actiontype=suborglist&isLoadSubDepartment=1"
				ifCheck={true}
				isMult={false}
				ids=''
				names={''}
				{...this.props}
				/>
		)
	}
}

