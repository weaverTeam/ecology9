import T from 'prop-types'
import QRCode from "qrcode.react";

export default class Main extends React.Component {
    static propTypes ={
        text:T.string.isRequired,
        bgColor:T.string,
        level:T.string,
        size:T.number
    }
    render() {
        const {size,bgColor,fgColor,level} = this.props;
        return (
            <QRCode 
              size={size || 128}
              bgColor={bgColor || "#ffffff"}
              fgColor={fgColor || "#000000"}
              level={level || "L"}
              value={this.props.text} 
            />
        );
    }
}

