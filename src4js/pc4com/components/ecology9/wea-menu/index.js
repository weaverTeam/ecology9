import {Icon} from 'antd';
import MenuInline from './MenuInline'
import MenuVertical from './MenuVertical'
import cloneDeep from 'lodash/cloneDeep'
// import Icon from '../../_antd1.11.2/icon';
import WeaScroll from '../wea-scroll';
import WeaNewScroll from '../wea-new-scroll';
class WeaMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mode:props.mode || "inline",
            defaultSelectedKey:props.defaultSelectedKey || "",
            datas:props.datas || [],
            verticalWidth:props.verticalWidth || 50,
            inlineWidth:props.inlineWidth || 190,
            menuHeight: jQuery(document.body).height() - 95
        }
    }
    resetHeigth(){
        this.setState({menuHeight:jQuery(document.body).height() - 95});
    }
    componentDidMount(){
    	jQuery(window).resize(() => {
		    this._reactInternalInstance !== undefined && this.resetHeigth();
		});
    }
    shouldComponentUpdate(nextProps) {
        // console.log("JSON.stringify(this.props.datas):",JSON.stringify(this.props.datas));
        // console.log("JSON.stringify(nextProps.datas):",JSON.stringify(nextProps.datas));

        //  if(JSON.stringify(this.props.datas) !== JSON.stringify(nextProps.datas)  || (this.props.defaultSelectedKey !== nextProps.defaultSelectedKey&&this.state.defaultSelectedKey !== nextProps.defaultSelectedKey)) {
        //      return true;
        //  }
         return true;
    }
    componentWillReceiveProps(nextProps) {
         if(JSON.stringify(this.props.datas) !== JSON.stringify(nextProps.datas)  || (this.props.defaultSelectedKey !== nextProps.defaultSelectedKey&&this.state.defaultSelectedKey !== nextProps.defaultSelectedKey)) {
            //计算出openKeys
            this.setState({
                datas:nextProps.datas || [],
                defaultSelectedKey:nextProps.defaultSelectedKey || ""
            });
        }
    }
    render() {
        const {mode,datas,verticalWidth,inlineWidth,defaultSelectedKey,menuHeight} = this.state;
        const newDatas = this.setTreeKey("",datas);
        const {addonBefore,addonAfter,addonBeforeHeight,addonAfterHeight,clickBlock,needSwitch} = this.props;
        let style = {width:mode == "vertical" ? verticalWidth : inlineWidth};
        let conHeightNum = (!!needSwitch ? 40 : 0) + (!!addonBeforeHeight && mode == 'inline' ? addonBeforeHeight : 0) + (!!addonAfterHeight && mode == 'inline' ? addonAfterHeight : 0);
        return (
            <div style={{height:"100%"}}>
            <div className={"wea-menu wea-menu-" + mode} style={style}>
            {!!needSwitch ? <div className="wea-menu-switch" onClick={this.onModeChange.bind(this,mode)}><Icon type={mode == "vertical" ? "menu-unfold" : "menu-fold"} /></div> : ""}
            {!!addonBefore && mode == 'inline' ? addonBefore : ''}
            <WeaNewScroll height={menuHeight} scrollId={'menuScrollWrapper'}>
            {mode==="inline"?(
                <MenuInline
                mode={mode}
                datas={newDatas}
                inlineWidth={inlineWidth}
                verticalWidth={verticalWidth}
                clickBlock={clickBlock}
                defaultSelectedKey={defaultSelectedKey}
                onSelect={this.onSelect.bind(this)}
                onModeChange={this.onModeChange.bind(this)}
                />
            ):(
                <MenuVertical
                mode={mode}
                datas={this.getVerticalDatas(newDatas)}
                inlineWidth={inlineWidth}
                verticalWidth={verticalWidth}
                clickBlock={clickBlock}
                defaultSelectedKey={defaultSelectedKey}
                onSelect={this.onSelect.bind(this)}
                onModeChange={this.onModeChange.bind(this)}
                />
            )}
            </WeaNewScroll>
            {!!addonAfter && mode == 'inline' ? addonAfter : ''}
            </div>
            </div>
        )
    }
    onModeChange(mode) {
        //console.log("mode:",mode);
        const {inlineWidth,verticalWidth} = this.state;
        const newMode = mode=="inline"?"vertical":"inline";
        const newWidth = newMode=="inline"?inlineWidth:verticalWidth;
        this.setState({
            mode:newMode
        });
        if(typeof this.props.onModeChange=="function") {
            this.props.onModeChange(newMode,newWidth);
        }
    }
    onSelect(key,data,type) {
        //console.log("key:",key," data:",data,"type",type);
        this.setState({
            defaultSelectedKey:key
        });
        if(typeof this.props.onSelect=="function") {
            this.props.onSelect(key,data,type);
        }
    }
    setTreeKey(pKey,datas) {
        //console.log("datas:",datas);
        let that = this;
        let newDatas = new Array();
        for(let i=0;i<datas.length;i++) {
            let data = datas[i];
            data.treeKey = pKey!=""?(pKey+"{$}"+data.id):data.id;
            if(data.child && data.child.length>0) {
                data.child = that.setTreeKey(data.treeKey,data.child);
            }
            newDatas.push(data);
        }
        return newDatas;
    }
    getVerticalDatas(datas) {
        //console.log("datas:",datas);
        let newDatas = new Array();
        for(let i=0;i<datas.length;i++) {
            const data = datas[i];
            let newData = cloneDeep(data);
            newData.child = [];
            let newDataNoChild = cloneDeep(newData);
            newDataNoChild.id = "verTop_"+newDataNoChild.id;
            if(data.child) {
                newData.child = [newDataNoChild].concat(data.child);
            }
            else {
                newData.child = [newDataNoChild];
            }
            newDatas.push(newData);
            // newDataNoChild.child = [cloneDeep(datas[i])];
            // newDatas.push(newDataNoChild);
        }
        //console.log("newDatas:",newDatas);
        return newDatas;
    }
}


export default WeaMenu