import WeaWorkflowRadio from '../base/wea-workflow-radio'

export default class WeaProjectInput extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="相关项目"
				type='8'//联想搜索
				icon={<i className='icon-knowledge-copy' />}
				iconBgcolor='#6c2a6a'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata" //dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","prolist"]}//otherPara={["action","BrowserAction","actiontype","prolist"]}
				{...this.props}
				/>
		)
	}
}



