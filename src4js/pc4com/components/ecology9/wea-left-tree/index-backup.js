import {Tree, Row, Col, Tooltip} from 'antd';
// import Tree from '../_antd1.11.2/Tree'
// import Row from '../_antd1.11.2/Row'
// import Col from '../_antd1.11.2/Col'
// import Tooltip  from '../_antd1.11.2/Tooltip'

const TreeNode = Tree.TreeNode;
import WeaInputFocus from '../wea-input-focus'
import WeaScroll from '../wea-scroll';
import cloneDeep from 'lodash/cloneDeep'

const isArray = (datas) => {
    return datas && datas.length>0;
}
let sb = "";
class WeaNewTree extends React.Component {
     constructor(props) {
        super(props);
        this.state = {
            height:0
        }
        sb = this;
    }
    scrollheigth(){
        let scrollheigth = document.documentElement.clientHeight - 96;
        jQuery(".wea-new-tree .wea-scroll").height(scrollheigth);
    }
    componentDidMount(){
      this.setState({
        heigth: this.scrollheigth()
      })
    }
    componentDidUpdate(){
        this.scrollheigth && this.scrollheigth();
    }
    render() {
        const {datas,counts,countsType} = this.props;
        //console.log("datas autoExpandParent:",datas);
        //expandedKeys={this.getRootKeys(datas)}
        //console.log("!isArray(datas)：",!isArray(datas));
        if(!isArray(datas)) {
            return <div></div>
        }
        //console.log("is in :",datas);
        //console.log("counts:",counts);
        return (
            <div className="wea-new-tree">
                <div className="wea-input-wraper">
                    <span onClick={this.onSearch.bind(this,'')} style={{"cursor":"pointer"}}><b>全部类型</b></span>
                    <WeaInputFocus onSearch={this.onSearch.bind(this)}/>
                </div>
                <WeaScroll className="wea-scroll" typeClass="scrollbar-macosx" style={{"height":this.state.height}}>
                    <Tree showLine defaultExpandedKeys={this.getRootKeys(datas)} onSelect={this.onSelect.bind(this)}>
                        {this.renderTreeNodes(datas,counts,countsType)}
                    </Tree>
                </WeaScroll>
            </div>
        )
    }
    onSelect(keys) {
        const {counts} = this.props;
        const key = keys && keys.length>0?keys[0]:"";
        if (typeof (this.props.onSelect) == "function") {
            //console.log("counts:",counts[key]);
            this.props.onSelect(key,counts[key]);
        }
    }
    onSearch(v){
    	if(typeof this.props.onSearch == 'function'){
    		this.props.onSearch(v);
    	}
    }
    renderTreeNodes(datas,counts,countsType) {
        //console.log("datas",datas,"counts",counts,"countsType",countsType);
        let that = this;
        return datas && datas.map((data)=>{
            return (
                <TreeNode title={that.getTitle(data,counts,countsType)} key={data.domid}>
                   {isArray(data.childs) && that.renderTreeNodes(data.childs,counts,countsType)}
                </TreeNode>
            )
        });
    }
    getTitle(data,counts,countsType) {
        let _this = this;
        let flowSpan = [];
        let countsTypeShow = [];
        let notShowEm = 0;
    	countsType.map((c,i)=>{
            let num = (counts && counts[data.domid]) ? (counts[data.domid][c.name] ? counts[data.domid][c.name] : ''):'';
    		if(num && num !== '0'){
    			notShowEm ++;
    			countsTypeShow.push(c);
    			notShowEm !== 1 && flowSpan.push(<em className={c.isshow ? '' : 'wf-isshow'} style={{"display":c.isshow ? "inline" : "none",fontFamily: 'Helvetica Neue,Helvetica,PingFang SC,Hiragino Sans GB,Microsoft YaHei,\\5FAE\8F6F\96C5\9ED1,Arial,sans-serif'}}> / </em>)
	    		flowSpan.push(
	    			<Tooltip title={c.title}>
	                    <span className={c.isshow ? '' : 'wf-isshow'} style={{"color":c.color,"display":c.isshow ? "inline-block" : "none"}} onClick={_this.titleNumClick.bind(_this,c)}>{num}</span>
	                </Tooltip>
	    		);
    		}
    	});
        return (
            <Row onMouseEnter={this.titleHoverIn.bind(this,countsTypeShow)} onMouseLeave={this.titleHoverOut.bind(this,countsTypeShow)}>
                <Col span="16">
                {data.name}
                </Col>
                <Col span="8" style={{textAlign:"right"}} className="titleNum"  >
                {flowSpan}
                </Col>
            </Row>
        )
    }
    getRootKeys(datas) {
        let keyArr = [];
        datas.map((data)=>{
            keyArr.push(data.domid);
        });
        return keyArr;
    }
    titleNumClick(v){
        if(typeof this.props.titleNumClick == 'function'){
        	this.props.titleNumClick(v);
        }
    }
    titleHoverIn(countsTypeShow,e){
        jQuery(e.target).closest(".wea-tree-wrap").find(".ant-tree-title .titleNum").each((index,item)=>{
        	jQuery(item).find(".wf-isshow").css({'display':'inline-block'});
            jQuery(item).find("span").each((i,m)=>{
                jQuery(m).css({"background-color":countsTypeShow[i].color,"color":"#fff"});
            })
        })
    }
	titleHoverOut(countsTypeShow,e){
    	jQuery(e.target).closest(".wea-tree-wrap").find(".ant-tree-title .titleNum").each((index,item)=>{
    		jQuery(item).find(".wf-isshow").css({'display':'none'});
            jQuery(item).find("span").each((i,m)=>{
                jQuery(m).css({"color":countsTypeShow[i].color,"background-color":"rgba(0,0,0,0)"});
            })
        })
    }
}
jQuery(window).resize(function() {
    sb.setState({
        height:sb.scrollheigth()
    });
});
export default WeaNewTree;