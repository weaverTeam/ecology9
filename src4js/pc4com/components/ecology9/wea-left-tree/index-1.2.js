import Tree from '../_antd1.11.2/Tree'
import Row from '../_antd1.11.2/Row'
import Col from '../_antd1.11.2/Col'
import Tooltip  from '../_antd1.11.2/Tooltip'

const TreeNode = Tree.TreeNode;
import WeaInputFocus from '../wea-input-focus'
import WeaScroll from '../wea-scroll';
import cloneDeep from 'lodash/cloneDeep'

const isArray = (datas) => {
    return datas && datas.length>0;
}
let sb = "";
let titleHoverstyle = {}, flowSpanNew = [],countsTypeCopy;
class WeaNewTree extends React.Component {
     constructor(props) {
        super(props);
        this.state = {
            height:0
        }
        sb = this;
    }
    scrollheigth(){
        let scrollheigth = document.documentElement.clientHeight - 96;
        jQuery(".wea-new-tree .wea-scroll").height(scrollheigth);
    }
    componentDidMount(){
      this.setState({
        heigth: this.scrollheigth()
      })      
    }
    componentDidUpdate(){
        this.scrollheigth && this.scrollheigth();
    }
    handleClick(){
        console.log("点了全部类型");
    }
    render() {
        const {datas,counts,countsType} = this.props;
        countsTypeCopy = this.props.countsType;
        //console.log("datas autoExpandParent:",datas);
        //expandedKeys={this.getRootKeys(datas)}
        //console.log("!isArray(datas)：",!isArray(datas));
        if(!isArray(datas)) {
            return <div></div>
        }
        //console.log("is in :",datas);
        //console.log("counts:",counts);
        return (
            <div className="wea-new-tree"> 
                <div className="wea-input-wraper">
                    <span onClick={this.handleClick.bind(this)} style={{"cursor":"pointer"}}><b>全部类型</b></span>
                    <WeaInputFocus/>
                </div>
                <WeaScroll className="wea-scroll" typeClass="scrollbar-macosx" style={{"height":this.state.height}}>
                    <Tree showLine defaultExpandedKeys={this.getRootKeys(datas)} onSelect={this.onSelect.bind(this)}>
                        {this.renderTreeNodes(datas,counts,countsType)}
                    </Tree>
                </WeaScroll>
            </div>
        )
    }
    onSelect(keys) {
        const {counts} = this.props;
        const key = keys && keys.length>0?keys[0]:"";
        if (typeof (this.props.onSelect) == "function") {
            //console.log("counts:",counts[key]);
            this.props.onSelect(key,counts[key]);
        }
    }
    renderTreeNodes(datas,counts,countsType) {
        //console.log("datas",datas,"counts",counts,"countsType",countsType);
        let that = this;
        return datas && datas.map((data)=>{
            return (    
                <TreeNode title={that.getTitle(data,counts,countsType)} key={data.domid}>
                   {isArray(data.childs) && that.renderTreeNodes(data.childs,counts,countsType)}
                </TreeNode> 
            )
        });
    }
    getTitle(data,counts,countsType) {
        let _this = this, flowSpan = "";
        flowSpanNew = [];
        if(counts&&countsType) {
            countsType.map((ct)=>{
                flowSpan += this.getFlowStr(counts[data.domid],ct);
            })
            // const flowAll = this.getFlowStr(counts[data.domid],"flowAll");
            // const flowNew = this.getFlowStr(counts[data.domid],"flowNew");
            // const flowOver = this.getFlowStr(counts[data.domid],"flowOver");
            // const flowRes = this.getFlowStr(counts[data.domid],"flowRes");
            // const flowSup = this.getFlowStr(counts[data.domid],"flowSup");
            if(flowSpan!=="") {
                flowSpan = flowSpan.substring(0,flowSpan.length-1).split("/");
            }
        }
        //console.log(flowSpan);
        flowSpan.forEach && flowSpan.map((m,i) => {
            if(flowSpan.length === 4){//默认颜色
                renderTitleNum(0);
            }else if(flowSpan.length === 3){
                renderTitleNum(1);
            }else if(flowSpan.length === 2){
                renderTitleNum(2);
            }else if(flowSpan.length === 1){
                renderTitleNum(3);
            }
            
            function renderTitleNum(num){
                titleHoverstyle = {"color":countsType[i+num].color,"display":countsType[i+num].isshow ? "inline-block" : "none"};
                flowSpanNew.push(
                     <Tooltip title={countsType[i+num].title}>
                        <span style={titleHoverstyle} onClick={_this.titleNumClick}>{m}</span>
                     </Tooltip>
                 );
                flowSpanNew.push(<em style={{"display":countsType[i+num].isshow ? "inline" : "none"}}> / </em>);
            }
         });
        flowSpanNew.splice(flowSpanNew.length-1,1);
        return (
            <Row onMouseEnter={this.titleHoverIn} onMouseLeave={this.titleHoverOut}>
                <Col span="18">
                {data.name}
                </Col>
                <Col span="6" style={{textAlign:"right"}} className="titleNum"  >
                {flowSpanNew}
                </Col>
            </Row>
        )
    }
    getFlowStr(obj,ct) {
        if(!obj) return "";
        const num = obj[ct.name];
        if(num=="" || num=="0") return "";
        return num?(num+"/"):"";
    }
    getRootKeys(datas) {
        let keyArr = [];
        datas.map((data)=>{
            keyArr.push(data.domid);
        });
        return keyArr;
    }
    titleNumClick(e){
        e.stopPropagation();
        console.log("点了titleNum",e.target);
    }

    titleHoverIn(e){
        //console.log("鼠标进来");
        //console.log(e.target);
        jQuery(e.target).closest(".wea-tree-wrap").find(".ant-tree-title .titleNum").each((index,item)=>{
            if(jQuery(item).find("span").length === 4){
              sb.hoverInColor(0,item);
            }else if(jQuery(item).find("span").length === 3){
              sb.hoverInColor(1,item)
            }else if(jQuery(item).find("span").length === 2){
               sb.hoverInColor(2,item);
            }else if(jQuery(item).find("span").length === 1){
              jQuery(item).find("span").each((i,m)=>{
                 jQuery(m).css({"background-color":countsTypeCopy[countsTypeCopy.length-1].hovercolor,"color":"#fff"});
              })
            }
            
        }) 
    };

    titleHoverOut(e){
        //console.log("鼠标离开");
        jQuery(e.target).closest(".wea-tree-wrap").find(".ant-tree-title .titleNum").each((index,item)=>{
           if(jQuery(item).find("span").length === 4){
                sb.hoverOutColor(0,item);
           }else if(jQuery(item).find("span").length === 3){
                sb.hoverOutColor(1,item);
           }else if(jQuery(item).find("span").length === 2){
                sb.hoverOutColor(2,item);
           }else if(jQuery(item).find("span").length === 1){
             jQuery(item).find("span").each((i,m)=>{
                jQuery(m).css({"color":countsTypeCopy[countsTypeCopy.length-1].color,"background-color":"#fff"});
                if(jQuery(m).parent().parent().parent(".ant-tree-node-selected")){
                    jQuery(m).css({"background-color":"inherit"});
                }
             })
           }
        }) 
    };
    hoverInColor(num,item){
         jQuery(item).find("span").each((i,m)=>{
            jQuery(m).css({"background-color":countsTypeCopy[i+num].hovercolor,"color":"#fff"});
         })
    };
    hoverOutColor(num,item){
        jQuery(item).find("span").each((i,m)=>{
            jQuery(m).css({"color":countsTypeCopy[i+num].color,"background-color":"#fff"});
            if(jQuery(m).parent().parent().parent(".ant-tree-node-selected")){
                jQuery(m).css({"background-color":"inherit"});
            }
        })
    };

}
jQuery(window).resize(function() {
    sb.setState({
        height:sb.scrollheigth()
    });
});
export default WeaNewTree;