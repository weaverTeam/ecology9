import WeaTextareaNormal from '../base/wea-textarea-normal';

class main extends React.Component {
    static defaultProps={
        inputType: 'NORMAl',
    }
    static propTypes = {
        inputType: React.PropTypes.string,
    }
    constructor(props) {
        super(props);
    }
    render() {
        const {props} = this;
        if (props.inputType.toUpperCase() == 'FORM') {
            return (<input />)
        }
        return (<WeaTextareaNormal {...props}/>);
    }
}

export default main;
