import {Tabs, Row, Col, Input, Button, Menu} from 'antd';
// import Tabs from '../../_antd1.11.2/tabs'
// import Row from '../../_antd1.11.2/row'
// import Col from '../../_antd1.11.2/col'
// import Input from '../../_antd1.11.2/input'
// import Button from '../../_antd1.11.2/button'
// import Menu from '../../_antd1.11.2/menu'
import WeaInputSearch from '../wea-input-search'
import objectAssign from 'object-assign'
const TabPane = Tabs.TabPane;
const isIE8 = window.navigator.appVersion.indexOf("MSIE 8.0") >= 0;
class main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchAd: props.showSearchAd || false
        };
    }
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.showSearchAd !== this.props.showSearchAd ||
                nextState.showSearchAd!==this.state.showSearchAd ||
                nextProps.buttonsAd !== this.props.buttonsAd ||
                nextProps.searchsAd !== this.props.searchsAd ||
                nextProps.searchType !== this.props.searchType ||
                nextProps.searchsBaseValue !== this.props.searchsBaseValue;
    }
    componentWillReceiveProps(nextProps) {
        if(this.props.showSearchAd!==nextProps.showSearchAd) {
            this.setState({
                showSearchAd: nextProps.showSearchAd
            });
        }
    }
    render() {
        let that = this;
        const {showSearchAd} = this.state;
        const {buttonsAd,searchsAd,searchsBaseValue,tabs,buttons,addSelect,searchType} = this.props;
        const searchBase =  `${searchType}`.indexOf('base') >= 0;
        const searchAdvanced =  `${searchType}`.indexOf('advanced') >= 0;
        return (
            <div className="wea-tab">
                <Row>
                    <Col xs={isIE8 ? 15 : 13} >
                        {tabs}
                    </Col>
                    <Col xs={isIE8 ? 9 : 11}  style={{textAlign:"right"}}>
                        <div className="wea-search-tab">
                            {
                                buttons && buttons.map(data=>{
                                    return (
                                        <span style={{marginLeft:15}}>{data}</span>
                                    )
                                })
                            }
                            {addSelect ? addSelect:''}
                            <div className="advance-search-wrapper">
                                {searchBase &&  <WeaInputSearch value={searchsBaseValue} onSearch={this.onSearch.bind(this)} onSearchChange={this.onSearchChange.bind(this)}/>}
                                {searchAdvanced && <Button type='ghost' className="wea-advanced-search" onClick={this.setShowSearchAd.bind(this,true)}>高级搜索</Button>}
                            </div>
                        </div>
                    </Col>
                </Row>
                   <div className="wea-search-container" style={{display:showSearchAd ? 'block' : 'none'}}>
                    <Button type='ghost' className="wea-advanced-search" onClick={this.setShowSearchAd.bind(this,false)}>高级搜索</Button>
                    <div className='wea-advanced-searchsAd' >
                        {searchsAd}
                    </div>
                    <div className="wea-search-buttons">
                        <div style={{"textAlign":"center"}}>
                        {
                            buttonsAd && buttonsAd.map((data)=>{
                                return (
                                    <span style={{marginLeft:15}}>{data}</span>
                                )
                            })
                        }
                        </div>
                    </div>
                </div>
                <div className="mask-dark" style={{display:showSearchAd ? 'block' : 'none'}}></div>
                <div className="mask-wrapper" style={{display:showSearchAd ? 'block' : 'none'}} onClick={()=> this.props.hideSearchAd && this.props.hideSearchAd()}> </div>
            </div>
        )
    }
    onSearch(v){
        if (typeof this.props.onSearch == 'function') {
            this.props.onSearch(v);
        }
    }
    onSearchChange(v){
        if (typeof this.props.onSearchChange == 'function') {
            this.props.onSearchChange(v);
        }
    }
    setShowSearchAd(bool){
        this.setState({showSearchAd: bool});
        this.props.setShowSearchAd && this.props.setShowSearchAd(bool);
    }
}

export default main;