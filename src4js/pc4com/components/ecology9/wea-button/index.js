import {Button} from 'antd';
// import Button from '../../_antd1.11.2/Button'

class main extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const {text} = this.props;
		return(
			<div className="wea-button">
                <Button {...this.props}>
                    {text}
                </Button>
            </div>
		)
	}
}

export default main