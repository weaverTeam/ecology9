import WeaWorkflowRadio from '../base/wea-workflow-radio'

export default class WeaInput4ComNew extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="分部"
				singleTabName='按组织结构'
				type='164'//联想搜索
				icon={<i className='icon-New-Flow-Personnel-matters' />}
				iconBgcolor='#68a54c'
				contentType="tree"
				topPrefix="com"
				prefix="com"
				dataUrl = "/api/workflow/browser/browserdata"//dataUrl="/workflow/core/ControlServlet.jsp"
				dataKey="psubcompanyid"
				otherPara={["actiontype","orgtree","isLoadSubDepartment","0","isLoadAllSub","0"]}//otherPara={["action","BrowserAction","actiontype","orgtree","isLoadSubDepartment","0","isLoadAllSub","0"]}
				asyncUrl="/api/workflow/browser/browserdata?actiontype=suborglist&isLoadSubDepartment=0"
				ifCheck={false}
				isMult={false}
				{...this.props}
				/>
		)
	}
}


