import {TimePicker} from 'antd';
// import TimePicker from '../../_antd1.11.2/time-picker';
// import isEmpty from 'lodash/isEmpty';
import classNames from 'classnames';
import WeaTools from '../wea-tools';

class Main extends React.Component {
	/**
     * formatPattern
     * 1：HH:MI:SS
     * 2：HH:MI:SS AM/PM
     * 3：HH:MI
     * 4：HH:MI AM/PM
     * 5：HH时MI分SS秒
     * 6：HH时MI分
     * 7：HH时MI分SS秒 AM/PM
     * 8：HH时MI分 AM/PM
     **/
    static defaultProps={
		format:"HH:mm",
		viewAttr:2,//显示属性 1(只读)/2(可编辑)/3(必填)
        formatPattern: 3,
        minuteStep: 1,
        noInput: false,
	}
	static propTypes = {
	  placeholder: React.PropTypes.string,
	  size:React.PropTypes.isRequired,
	  onChange: React.PropTypes.func,
	  promptRequiredMark:React.PropTypes.number,
	}
    constructor(props) {
		super(props);
		// console.log('timevalue',props.value);
		this.state = {
		  value: 'value' in props ? props.value : null,
		}
	}
	componentWillReceiveProps(nextProps) {
		if('value' in nextProps && this.state.value !== nextProps.value) {
			this.setState({ value: nextProps.value });
		}
	}

	onChange=(value,valuestring)=> {
	    this.setState({ value: valuestring });
	    typeof this.props.onChange == 'function' && this.props.onChange(valuestring);
	}
    isReadOnly() {
        const {viewAttr} = this.props;
        return viewAttr === 1 || viewAttr === '1';
    }
    render() {
    	const {viewAttr,isDetail,fieldName, noInput, hasBorder, underline}=this.props;
    	const req = classNames({
            'required': viewAttr==3 && !this.state.value,
            'noInput': noInput,
        });
        const style = this.props.style || {};
        style.textDecoration = 'none';
        delete this.props.style;
        if (this.isReadOnly()) {
            const readonly = classNames({
                'border': hasBorder,
                'underline': underline,
            });
            return (
                <span className={`wea-field-readonly ${readonly}`} style={style}>
                    <span className="child-item wdb">{WeaTools.formatTime(this.state.value, this.props.formatPattern)}</span>
                    <input type="hidden" id={fieldName} name={fieldName} value={WeaTools.formatTime(this.state.value, this.props.formatPattern)} />
                </span>
            )
        }
        return (
            <span className={`wea-date-picker ${req}`} style={style}>
        	    <TimePicker
                    {...this.props}
    	            getPopupContainer={() => (this.props.layout || document.body)}
    	            value={this.state.value}
    	            onChange={this.onChange}
                    formatPattern={this.props.formatPattern || 3}
	            />
                <input type="hidden" id={fieldName} name={fieldName} value={this.state.value} />
            </span>
        );
    }
}
export default Main;

