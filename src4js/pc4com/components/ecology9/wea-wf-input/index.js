import WeaInput4Base from '../base/wea-input4-base'
import WeaWorkflowRadio from '../base/wea-workflow-radio'

export default class WeaInput4WfNew extends React.Component {
	shouldComponentUpdate(nextProps, nextState) {
		//console.log("nextProps.value:",nextProps.value);
		//console.log("this.props.value:",this.props.value);
		//console.log("nextProps.value !== this.props.value:",nextProps.value !== this.props.value);
		return nextProps.value !== this.props.value;
	}
	render() {
		return (
			<WeaWorkflowRadio
				title="选择路径"
				type='workflowBrowser'//联想搜索
				icon={<i className='icon-portal-workflow' />}
				iconBgcolor='#55D2D4'
				contentType="list"
				dataUrl = "/api/workflow/browser/browserdata"//dataUrl="/workflow/core/ControlServlet.jsp"
				otherPara={["actiontype","wflist"]}//otherPara={["action","BrowserAction","actiontype","wflist"]}
				{...this.props}
				/>

			/*<WeaInput4Base
				title="流程"
				topPrefix="wt"
				prefix="wf"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Wf"]}
				ifAsync={true}
				ifCheck={true}
				{...this.props}
			/>*/
		)
	}
}




/*

const WeaInput4Wf = React.createClass({
	render() {
		return  <WeaInput4Base
				title="流程"
				label={this.props.label}
				name={this.props.name}
				id={this.props.id}
				topPrefix="wt"
				prefix="wf"
				topIconUrl="/cloudstore/images/icon/treeTop.gif"
				dataUrl="/cloudstore/system/ControlServlet.jsp"
				dataKey="id"
				otherPara={["action","Action_GetTree4Wf"]}
				isMult={this.props.isMult}
				ifAsync={true}
				ifCheck={true}
				value={this.props.value}
				valueName={this.props.valueName}
				style={this.props.style}
				labelCol={this.props.labelCol} wrapperCol={this.props.wrapperCol}
				onChange={this.props.onChange}
				NoFormItem={this.props.NoFormItem}
				width={this.props.width}
				disabled={this.props.disabled} />;
	}
});

*/