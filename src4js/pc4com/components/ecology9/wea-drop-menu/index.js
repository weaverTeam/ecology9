import {Row, Col, Menu, Dropdown, Icon} from 'antd';
// import Row from '../../_antd1.11.2/Row'
// import Col from '../../_antd1.11.2/Col'
// import Menu from '../../_antd1.11.2/menu';
// import Dropdown from '../../_antd1.11.2/Dropdown';
// import Icon from '../../_antd1.11.2/Icon';
import cloneDeep from 'lodash/cloneDeep'
import isEqual from 'lodash/isEqual'

export default class Main extends React.Component {
     constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const {dropMenu,defaultMenu} = this.props;
        let menuItem = [];
        dropMenu && dropMenu.forEach((item)=>{
            menuItem.push(<Menu.Item key={item.companyid}>{item.name}</Menu.Item>);
            menuItem.push(<Menu.Divider />)
        });
        const typeMenu = (
            <Menu className="wea-browser-drop-menu" onSelect={this.menuSelect}>
            {menuItem}
            </Menu>
        );
        return (
            <div className="wea-drop-menu" style={{width:'100%'}}>
                <Row>
                    <Col span={24}>
                        <Dropdown overlay={typeMenu} trigger={['click']}>
                            <a className="ant-dropdown-link" href="javascript:;">
                              <i className={'icon-toolbar-Organization-list'}/>
                                {defaultMenu}
                              <Icon type="down" style={{float:"right"}}/>
                            </a>
                        </Dropdown>
                    </Col>
                </Row>
            </div>
        );
    }
    menuSelect=(e)=>{
        if(typeof this.props.onSelect == 'function'){
            this.props.onSelect(e)
        }
    }
}
