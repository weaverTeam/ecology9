import React, { PropTypes } from 'react';

const LazyRenderBox = React.createClass({
  propTypes: {
    className: PropTypes.string,
    visible: PropTypes.bool,
    hiddenClassName: PropTypes.string,
  },
  shouldComponentUpdate(nextProps) {
    return nextProps.hiddenClassName || nextProps.visible;
  },
  setPosition(){
    if (this.props.role == 'document') {
      const renderBox = $(this.refs.renderBox.getDOMNode());
      const modal = renderBox.find('.ant-modal-content');
      let modalHeight = modal.outerHeight();
      let modalWidth = modal.outerWidth();
      let bodyHeight = $(document.body).height();
      let bodyWidth = $(document.body).width();
      let top = (bodyHeight - modalHeight) / 2;
      let left = (bodyWidth - modalWidth) / 2;
      renderBox.css('top', `${top}px`)
      .css('left', `${left}px`).css('margin', '0').css('visibility', 'visible');
    }
  },
  componentDidMount() {
    this.setPosition();
  },
  componentDidUpdate() {
    this.setPosition();
  },
  render() {
    let className = this.props.className;
    if (this.props.hiddenClassName && !this.props.visible) {
      className += ` ${this.props.hiddenClassName}`;
    }
    const props = { ...this.props };
    delete props.hiddenClassName;
    delete props.visible;
    props.className = className;
    let modalStyle = {...props.style} || {};
    // if (props.role == 'document') modalStyle.visibility = 'hidden';
    return <div ref="renderBox" {...props} style={modalStyle}/>;
  },
});

export default LazyRenderBox;
