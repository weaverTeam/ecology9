const DateTime = {
	getDateStr(n) {
		var date1 = new Date();
		//console.log(date1.getFullYear()+"-"+(date1.getMonth()+1)+"-"+date1.getDate())
		var date2 = new Date(date1);
		date2.setDate(date1.getDate()+7);
		var times = date2.getFullYear()+"-"+(date2.getMonth()+1)+"-"+date2.getDate();
		return times;
	},
	format(d,fmt) { //author: meizz 
	    let o = {
	        "M+": d.getMonth() + 1, //月份 
	        "d+": d.getDate(), //日 
	        "h+": d.getHours(), //小时 
	        "m+": d.getMinutes(), //分 
	        "s+": d.getSeconds(), //秒 
	        "q+": Math.floor((d.getMonth() + 3) / 3), //季度 
	        "S": d.getMilliseconds() //毫秒 
	    };
	    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (d.getFullYear() + "").substr(4 - RegExp.$1.length));
	    for (let k in o)
	    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	    return fmt;
	}
}

export default DateTime;