import React, { PropTypes } from 'react';
import Trigger from 'rc-trigger';
import Panel from './module/Panel';
import placements from './util/placements';
import CommonMixin from './mixin/CommonMixin';
import { getFormatter } from './util/index';
import util from '../../_util/util';
import Icon from '../../_antd1.11.2/icon';

function noop() {
}

function refFn(field, component) {
  this[field] = component;
}

const Picker = React.createClass({
  propTypes: {
    prefixCls: PropTypes.string,
    locale: PropTypes.object,
    value: PropTypes.object,
    disabled: PropTypes.bool,
    allowEmpty: PropTypes.bool,
    defaultValue: PropTypes.object,
    open: PropTypes.bool,
    defaultOpen: PropTypes.bool,
    align: PropTypes.object,
    placement: PropTypes.any,
    transitionName: PropTypes.string,
    getPopupContainer: PropTypes.func,
    placeholder: PropTypes.string,
    formatter: PropTypes.any,
    showHour: PropTypes.bool,
    style: PropTypes.object,
    className: PropTypes.string,
    showSecond: PropTypes.bool,
    disabledHours: PropTypes.func,
    disabledMinutes: PropTypes.func,
    disabledSeconds: PropTypes.func,
    hideDisabledOptions: PropTypes.bool,
    onChange: PropTypes.func,
    onOpen: PropTypes.func,
    onClose: PropTypes.func,
  },

  mixins: [CommonMixin],

  getDefaultProps() {
    return {
      prefixCls: 'rc-time-picker',
      defaultOpen: false,
      style: {},
      className: '',
      align: {},
      allowEmpty: true,
      showHour: true,
      showSecond: true,
      disabledHours: noop,
      disabledMinutes: noop,
      disabledSeconds: noop,
      hideDisabledOptions: false,
      placement: 'bottomLeft',
      onChange: noop,
      onOpen: noop,
      onClose: noop,
    };
  },

  getInitialState() {
    this.savePanelRef = refFn.bind(this, 'panelInstance');
    const { defaultOpen, defaultValue, open = defaultOpen, value = defaultValue } = this.props;
    return {
      open,
      value,
      minWidth: 0,
    };
  },
  componentDidMount() {
    let w = 0;
    const {formatPattern} = this.props;
    if (formatPattern) {
      let v = util.formatTime('12:11', formatPattern);
      w = util.getTextWidth(v) + 40;
    }
    this.setState({minWidth: w});
  },
  componentWillReceiveProps(nextProps) {
    const { value, open } = nextProps;
    if ('value' in nextProps) {
      this.setState({
        value,
      });
    }
    if (open !== undefined) {
      this.setState({ open });
    }
  },

  onPanelChange(value) {
    this.setValue(value);
  },

  onPanelClear() {
    this.setValue(null);
    this.setOpen(false);
  },

  onVisibleChange(open) {
    this.setOpen(open);
  },

  onEsc() {
    this.setOpen(false);
    this.refs.picker.focus();
  },

  onKeyDown(e) {
    if (e.keyCode === 40) {
      this.setOpen(true);
    }
  },

  setValue(value) {
    if (!('value' in this.props)) {
      this.setState({
        value,
      });
    }
    this.props.onChange(value);
  },

  getFormatter() {
    const formatter = this.props.formatter;
    const locale = this.props.locale;
    if (formatter) {
      if (formatter === this.lastFormatter) {
        return this.normalFormatter;
      }
      this.normalFormatter = getFormatter(formatter, locale);
      this.lastFormatter = formatter;
      return this.normalFormatter;
    }
    if (!this.props.showSecond) {
      if (!this.notShowSecondFormatter) {
        this.notShowSecondFormatter = getFormatter('HH:mm', locale);
      }
      return this.notShowSecondFormatter;
    }
    if (!this.props.showHour) {
      if (!this.notShowHourFormatter) {
        this.notShowHourFormatter = getFormatter('mm:ss', locale);
      }
      return this.notShowHourFormatter;
    }
    if (!this.normalFormatter) {
      this.normalFormatter = getFormatter('HH:mm:ss', locale);
    }
    return this.normalFormatter;
  },

  getPanelElement() {
    const {
      prefixCls, defaultValue, locale, placeholder, disabledHours,
      disabledMinutes, disabledSeconds, hideDisabledOptions,
      allowEmpty, showHour, showSecond, minuteStep
    } = this.props;
    return (
      <Panel
        prefixCls={`${prefixCls}-panel`}
        ref={this.savePanelRef}
        value={this.state.value}
        onChange={this.onPanelChange}
        gregorianCalendarLocale={locale.calendar}
        onClear={this.onPanelClear}
        defaultValue={defaultValue}
        showHour={showHour}
        onEsc={this.onEsc}
        showSecond={showSecond}
        locale={locale}
        allowEmpty={allowEmpty}
        formatter={this.getFormatter()}
        placeholder={placeholder}
        disabledHours={disabledHours}
        disabledMinutes={disabledMinutes}
        disabledSeconds={disabledSeconds}
        hideDisabledOptions={hideDisabledOptions}
        minuteStep={minuteStep}
        onVisibleChange={this.onVisibleChange}
      />
    );
  },

  setOpen(open, callback) {
    const { onOpen, onClose } = this.props;
    if (this.state.open !== open) {
      this.setState({
        open,
      }, callback);
      const event = {
        open,
      };
      if (open) {
        onOpen(event);
      } else {
        onClose(event);
      }
    }
  },

  render() {
    const { prefixCls, placeholder, placement, align, disabled, transitionName, style, className, showHour, showSecond, getPopupContainer } = this.props;
    const { open, value, minWidth } = this.state;
    const {props} = this;
    let popupClassName;
    if (!showHour || !showSecond) {
      popupClassName = `${prefixCls}-panel-narrow`;
    }
    const clearIcon = (!disabled && value) ?
        <Icon type="cross-circle"
          className="ant-calendar-picker-clear"
          onClick={this.clearSelection}
        /> : null;
    return (
      <Trigger
        prefixCls={`${prefixCls}-panel`}
        popupClassName={popupClassName}
        popup={this.getPanelElement()}
        popupAlign={align}
        builtinPlacements={placements}
        popupPlacement={placement}
        action={disabled ? [] : ['click']}
        destroyPopupOnHide
        getPopupContainer={getPopupContainer}
        popupTransitionName={transitionName}
        popupVisible={open}
        onPopupVisibleChange={this.onVisibleChange}
      >
        <span className={`${prefixCls} ${className}`} style={style}>
          {
            !!props.noInput ? ''
            :
            <input
              className={`${prefixCls}-input`}
              ref="picker" type="text" placeholder={placeholder}
              readOnly
              onKeyDown={this.onKeyDown}
              style={{minWidth: minWidth}}
              disabled={disabled} value={this.formatValue(value)}
            />
          }
          <span className="picker-icon icon-coms-overtime cursor-pointer" onClick={()=> {this.setOpen(true)}}/>
          {props.noInput && <span className="text">{this.formatValue(value)}</span>}
        </span>
      </Trigger>
    );
  },
  formatValue(value) {
    const {formatPattern} = this.props;
    let v = '';
    if (value) {
      if (!formatPattern) {
        v = this.getFormatter().format(value);
      } else {
        v = this.getFormatter('HH:mm').format(value);
        v = util.formatTime(v, formatPattern);
      }
    }
    return v;
  },
});

export default Picker;
