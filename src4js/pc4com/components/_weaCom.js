import util from './_util/util'

class MyErrorHandler extends React.Component {
    render(){
        return (
            <div className="terrible-error">{this.props.error}</div>
        );
    }
}

if(!window.console) { //避免日志导致IE出问题
  window.console = {
      log:function(param){
          //alert(param);
      }
  };
}

let req = require.context('./cloudstore', true, /^\.\/[^_][\w-]+\/(style\/)?index\.js?$/);

req.keys().forEach((mod) => {
  let v = req(mod);
  const match = mod.match(/^\.\/([^_][\w-]+)\/index\.js?$/);
  if (match && match[1]) {
      try {
          //console.log(v.default);
          const name = util.camelCase(match[1]);
          // v.default = util.tryCatch(React, MyErrorHandler, {error: "前端组件库异常，请联系管理员!"})(v.default);
          exports[name] = v.default;

      }
      catch(e) {
      }
  }
});

exports["initCom"] = function(domEle,name,params,callback){
    name = eval("this."+name);
    if(typeof domEle === 'string') {
        domEle = document.getElementById(domEle)
    }
    let thatCom = null;
    class TheCom extends React.Component {
        constructor(props) {
                super(props);
            this.state = params;
            thatCom = this;
        }
        render() {
            return React.createElement(name,this.state,"");
        }
    }
    ReactDOM.render(<TheCom />, domEle ,function() {
        callback();
    });
    return thatCom;
}
