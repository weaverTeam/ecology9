import React, { PropTypes } from 'react';
import assign from 'object-assign';
import classNames from 'classnames';
import Animate from 'rc-animate';
import { browser } from './util';
import Icon from '../../_antd1.11.2/icon';
import Radio from '../../_antd1.11.2/radio';
import PureRenderMixin from 'react-addons-pure-render-mixin';

const browserUa = typeof window !== 'undefined' ? browser(window.navigator) : '';
const ieOrEdge = /.*(IE|Edge).+/.test(browserUa);
// const uaArray = browserUa.split(' ');
// const gtIE8 = uaArray.length !== 2 || uaArray[0].indexOf('IE') === -1 || Number(uaArray[1]) > 8;

const defaultTitle = '---';
let timeout = null;
class TreeNode extends React.Component {

  constructor(props) {
    super(props);
    [
      'onExpand',
      'onCheck',
      'onContextMenu',
      'onMouseEnter',
      'onMouseLeave',
      'onDragStart',
      'onDragEnter',
      'onDragOver',
      'onDragLeave',
      'onDrop',
      'onRadioCheck',
    ].forEach((m) => {
      this[m] = this[m].bind(this);
    });
    this.state = {
      dataLoading: false,
      dragNodeHighlight: false,
    };
  }

  shouldComponentUpdate(...args) {
      return PureRenderMixin.shouldComponentUpdate.apply(this, args);
  }

  componentDidMount() {
    if (!this.props.root._treeNodeInstances) {
      this.props.root._treeNodeInstances = [];
    }
    this.props.root._treeNodeInstances.push(this);
  }
  // shouldComponentUpdate(nextProps) {
  //   if (!nextProps.expanded) {
  //     return false;
  //   }
  //   return true;
  // }

  onCheck() {
    this.props.root.onCheck(this);
  }

  onSelect() {
    this.props.root.onSelect(this);
  }

  onDoubleClick(e) {
    clearTimeout(timeout);
    e.preventDefault();
    e.stopPropagation();
    this.props.root.onDoubleClick(this);
  }

  onMouseEnter(e) {
    e.preventDefault();
    this.props.root.onMouseEnter(e, this);
  }

  onMouseLeave(e) {
    e.preventDefault();
    this.props.root.onMouseLeave(e, this);
  }

  onContextMenu(e) {
    e.preventDefault();
    this.props.root.onContextMenu(e, this);
  }

  onDragStart(e) {
    // console.log('dragstart', this.props.eventKey, e);
    // e.preventDefault();
    e.stopPropagation();
    this.setState({
      dragNodeHighlight: false,
    });
    this.props.root.onDragStart(e, this);
    try {
      // ie throw error
      // firefox-need-it
      e.dataTransfer.setData('text/plain', '');
    } finally {
      // empty
    }
  }

  onDragEnter(e) {
    // console.log('dragenter', this.props.eventKey, e);
    e.preventDefault();
    e.stopPropagation();
    this.props.root.onDragEnter(e, this);
  }

  onDragOver(e) {
    // console.log(this.props.eventKey, e);
    // todo disabled
    e.preventDefault();
    e.stopPropagation();
    this.props.root.onDragOver(e, this);
    return false;
  }

  onDragLeave(e) {
    // console.log(this.props.eventKey, e);
    e.stopPropagation();
    this.props.root.onDragLeave(e, this);
  }

  onDrop(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      dragNodeHighlight: false,
    });
    this.props.root.onDrop(e, this);
  }

  onExpand() {
    let _this = this;
    // 取消上次延时未执行的方法
    clearTimeout(timeout);
    //执行延时
    timeout = setTimeout(function(){
      const callbackPromise = _this.props.root.onExpand(_this);
      if (callbackPromise && typeof callbackPromise === 'object') {
        const setLoading = (dataLoading) => {
          _this.setState({ dataLoading });
        };
        setLoading(true);
        callbackPromise.then(() => {
          setLoading(false);
        }, () => {
          setLoading(false);
        });
      }
    },10);
  }

  // keyboard event support
  onKeyDown(e) {
    e.preventDefault();
  }

  onRadioCheck(e) {
    e.preventDefault();
    this.props.root.onRadioCheck(this);
  }

  renderSwitcher(props, expandedState, expanded) {
    const prefixCls = props.prefixCls;
    const switcherCls = {
      [`${prefixCls}-switcher`]: true,
    };
    if (!props.showLine) {
      if (expanded) {
        switcherCls['icon-coms-organization-down'] = true;
      } else {
        switcherCls['icon-coms-organization-right'] = true;
      }
    } else if (props.pos === '0-0') {
      switcherCls[`${prefixCls}-roots_${expandedState}`] = true;
    } else {
      switcherCls[`${prefixCls}-center_${expandedState}`] = !props.last;
      switcherCls[`${prefixCls}-bottom_${expandedState}`] = props.last;
    }
    if (props.disabled) {
      switcherCls[`${prefixCls}-switcher-disabled`] = true;
      return <span className={classNames(switcherCls)} style={{fontSize: 8}}></span>;
    }
    return <span className={classNames(switcherCls)} onClick={this.onExpand} style={{fontSize: 8}}></span>;//span
  }

  renderCheckbox(props) {
    const prefixCls = props.prefixCls;
    const checkboxCls = {
      [`${prefixCls}-checkbox`]: true,
    };
    if (props.checked) {
      checkboxCls[`${prefixCls}-checkbox-checked`] = true;
    } else if (props.halfChecked) {
      checkboxCls[`${prefixCls}-checkbox-indeterminate`] = true;
    }
    let customEle = null;
    if (typeof props.checkable !== 'boolean') {
      customEle = props.checkable;
    }
    if(props.checkboxCls) {
      checkboxCls[props.checkboxCls] = true;
    }
    if (props.disabled || props.disableCheckbox) {
      checkboxCls[`${prefixCls}-checkbox-disabled`] = true;
      return <span ref="checkbox" className={classNames(checkboxCls)}>{customEle}</span>;
    }
    return (
      <span ref="checkbox"
        className={classNames(checkboxCls) }
        onClick={this.onCheck}
      >{customEle}</span>);
  }

  renderRadio(props) {
    const prefixCls = props.prefixCls;
    const radioCls = {
      [`${prefixCls}-radio`]: true,
    };
    if (!props.disableRadio) {
      return (
        <Radio
          ref="radio"
          className={classNames(radioCls) }
          value={props.treeKey}
          checked={props.radioChecked}
          onChange={this.onRadioCheck}
        >
        </Radio>
      );
    }
  }

  renderChildren(props) {
    const renderFirst = this.renderFirst;
    this.renderFirst = 1;
    let transitionAppear = true;
    if (!renderFirst && props.expanded) {
      transitionAppear = false;
    }
    const children = props.children;
    let newChildren = children;
    if (children &&
      (children.type === TreeNode ||
      Array.isArray(children) &&
      children.every((item) => {
        return item.type === TreeNode;
      }))) {
      const cls = {
        [`${props.prefixCls}-child-tree`]: true,
        [`${props.prefixCls}-child-tree-open`]: props.expanded,
      };
      if (props.showLine) {
        cls[`${props.prefixCls}-line`] = !props.last;
      }
      const animProps = {};
      if (props.openTransitionName) {
        animProps.transitionName = props.openTransitionName;
      } else if (typeof props.openAnimation === 'object') {
        animProps.animation = assign({}, props.openAnimation);
        if (!transitionAppear) {
          delete animProps.animation.appear;
        }
      }
      newChildren = (
        <Animate {...animProps}
          showProp="data-expanded"
          transitionAppear={transitionAppear}
          component=""
        >
          {!props.expanded ? null : <ul className={classNames(cls)} data-expanded={props.expanded}>
            {React.Children.map(children, (item, index) => {
              return props.root.renderTreeNode(item, index, props.pos);
            }, props.root)}
          </ul>}
        </Animate>
      );
    }
    return newChildren;
  }

  render() {
    const props = this.props;
    // console.log("props",props)
    const prefixCls = props.prefixCls;
    const expandedState = props.expanded ? 'open' : 'close';

    const iconEleCls = {
      [`${prefixCls}-iconEle`]: true,
      [`${prefixCls}-icon_loading`]: this.state.dataLoading,
      [`${prefixCls}-icon__${expandedState}`]: true,
    };

    let canRenderSwitcher = true;
    const content = props.title;
    let newChildren = this.renderChildren(props);
    if (props.isLeaf) { // fix 展开节点的受控
      canRenderSwitcher = false;
    }
    if (!newChildren || newChildren === props.children) {
      // content = newChildren;
      newChildren = null;
      if (!props.loadData || props.isLeaf) {
        canRenderSwitcher = false;
      }
    }
    // For performance, does't render children into dom when `!props.expanded` (move to Animate)
    // if (!props.expanded) {
    //   newChildren = null;
    // }

    const selectHandle = () => {
      const icon = (props.showIcon || props.loadData && this.state.dataLoading) ?
        <span className={classNames(iconEleCls)}></span> : null;
      const title = <span className={`${prefixCls}-title`}>{content}</span>;
      const domProps = {
        className: `${prefixCls}-node-content-wrapper`,
      };
      if (!props.disabled) {
        if (props.selected || !props._dropTrigger && this.state.dragNodeHighlight) {
          domProps.className += ` ${prefixCls}-node-selected`;
        }
        domProps.onClick = (e) => {
          e.preventDefault();
          props.clickNodeExpandChildren && !props.isLeaf && this.onExpand();
          if (props.selectable) {
            this.onSelect();
          }

          props.hasRadio && !props.disableRadio && props.root.onRadioCheck(this);
          // not fire check event
          // if (props.checkable) {
          //   this.onCheck();
          // }
        };
        if (props.onRightClick) {
          domProps.onContextMenu = this.onContextMenu;
        }
        if (props.onMouseEnter) {
          domProps.onMouseEnter = this.onMouseEnter;
        }
        if (props.onMouseLeave) {
          domProps.onMouseLeave = this.onMouseLeave;
        }
        if (props.draggable) {
          domProps.className += ' draggable';
          if (ieOrEdge) {
            // ie bug!
            domProps.href = '#';
          }
          domProps.draggable = true;
          domProps['aria-grabbed'] = true;
          domProps.onDragStart = this.onDragStart;
        }
      }
      return (
        <a ref="selectHandle" title={typeof content === 'string' ? content : ''} {...domProps}>
          {icon}{title}
          <div className="wea-icon-selected-bg">
          </div>
          <Icon type="check" className="wea-icon-selected"/>
        </a>
      );
    };

    const liProps = {};
    if (props.draggable) {
      liProps.onDragEnter = this.onDragEnter;
      liProps.onDragOver = this.onDragOver;
      liProps.onDragLeave = this.onDragLeave;
      liProps.onDrop = this.onDrop;
    }

    let disabledCls = '';
    let dragOverCls = '';
    if (props.disabled) {
      disabledCls = `${prefixCls}-treenode-disabled`;
    } else if (props.dragOver) {
      dragOverCls = 'drag-over';
    } else if (props.dragOverGapTop) {
      dragOverCls = 'drag-over-gap-top';
    } else if (props.dragOverGapBottom) {
      dragOverCls = 'drag-over-gap-bottom';
    }

    const filterCls = props.filterTreeNode(this) ? 'filter-node' : '';

    const noopSwitcher = () => {
      const cls = {
        [`${prefixCls}-switcher`]: true,
        [`${prefixCls}-switcher-noop`]: true,
      };
      if (props.showLine) {
        cls[`${prefixCls}-center_docu`] = !props.last;
        cls[`${prefixCls}-bottom_docu`] = props.last;
      } else {
        cls[`${prefixCls}-noline_docu`] = true;
      }
      return <span className={classNames(cls)}></span>;
    };
    return (
      !!props.browserTree ?
      (<li {...liProps} ref="li"
        onDoubleClick={this.onDoubleClick.bind(this)}
        className={classNames(props.className, disabledCls, dragOverCls, filterCls) }
      >
        <div className={`wea-tree-wrap wea-tree-wrap-${props.selected}`} style={{margin:0,width:'100%',lineHeight:'24px',padding:`0 10px 0 ${props.level * 24 + 10}px`}}>
        {canRenderSwitcher ? this.renderSwitcher(props, expandedState, props.expanded) : noopSwitcher()}
        {selectHandle()}
        </div>
        {props.checkable ? this.renderCheckbox(props) : null}
        {newChildren}
      </li>)
      :(<li {...liProps} ref="li"
        onDoubleClick={this.onDoubleClick.bind(this)}
        className={classNames(props.className, disabledCls, dragOverCls, filterCls) }
      >
        {canRenderSwitcher ? this.renderSwitcher(props, expandedState, props.expanded) : noopSwitcher()}
        {props.checkable ? this.renderCheckbox(props) : null}
        {props.hasRadio ? this.renderRadio(props) : null}
        {selectHandle()}
        {newChildren}
      </li>)
    );
  }
}

TreeNode.isTreeNode = 1;

TreeNode.propTypes = {
  prefixCls: PropTypes.string,
  disabled: PropTypes.bool,
  disableCheckbox: PropTypes.bool,
  expanded: PropTypes.bool,
  isLeaf: PropTypes.bool,
  root: PropTypes.object,
  onSelect: PropTypes.func,
};

TreeNode.defaultProps = {
  title: defaultTitle,
};

export default TreeNode;
