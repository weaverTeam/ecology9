import './icons.css'
import './index.css'
import './icon-coms.css'
import './common.css'

//import wrapWithTryCatch from 'react-try-catch-render'

class MyErrorHandler extends React.Component {
    render(){
        return (
            <div className="terrible-error">{this.props.error}</div>
        );
    }
}

if(!window.console) { //避免日志导致IE出问题
  window.console = {
      log:function(param){
          //alert(param);
      }
  };
}

function camelCase(name) {
  return name.charAt(0).toUpperCase() +
    name.slice(1).replace(/-(\w)/g, (m, n) => {
      return n.toUpperCase();
    });
}

let antdCom = {};

let req = require.context('./_antd1.11.2', true, /^\.\/[^_][\w-]+\/(style\/)?index\.jsx?$/);

req.keys().forEach((mod) => {
  const v = req(mod);
  const match = mod.match(/^\.\/([^_][\w-]+)\/index\.jsx?$/);
  if (match && match[1]) {
    if (match[1] === 'message' || match[1] === 'notification') {
      // message & notification should not be capitalized
      //console.log(match[1]);
      antdCom[match[1]] = v.default;
      //exports[match[1]] = v;
    } else {
      //console.log(match[1]);
      antdCom[camelCase(match[1])] = v.default;
      //exports[camelCase(match[1])] = v;
    }
  }
});

//console.log(antd);

window.antd = antdCom;

//console.log(antd);

let weaCom = {};

req = require.context('./cloudstore', true, /^\.\/[^_][\w-]+\/(style\/)?index\.js?$/);

import WeaTools from './ecology9/wea-tools';

req.keys().forEach((mod) => {
  let v = req(mod);
  const match = mod.match(/^\.\/([^_][\w-]+)\/index\.js?$/);
  if (match && match[1]) {
      //console.log(camelCase(match[1]));
      //antdCom[camelCase(match[1])] = v;
      try {
          //console.log(v.default);
          const name = camelCase(match[1]);
          if(name!=="WeaTools"&&name!=="WeaCheck") {
              //console.log("is1:"+name);
              v.default = WeaTools.tryCatch(React, MyErrorHandler, {error: "前端组件库异常，请联系管理员!"})(v.default);
              //v = wrapWithTryCatch(React, MyErrorHandler, {error: "Some custom error message!"})(v)
          }
          exports[name] = v.default;

      }
      catch(e) {
          //console.log(e);
      }
  }
});

exports["initCom"] = function(domEle,name,params,callback){
    name = eval("this."+name);
    if(typeof domEle === 'string') {
        domEle = document.getElementById(domEle)
    }
    let thatCom = null;
    class TheCom extends React.Component {
        constructor(props) {
		        super(props);
            this.state = params;
            thatCom = this;
        }
        render() {
            return React.createElement(name,this.state,"");
        }
    }
    ReactDOM.render(<TheCom />, domEle ,function() {
        callback();
    });
    return thatCom;
}

let ecCom = {};

req = require.context('./ecology9', true, /^\.\/[^_][\w-]+\/(style\/)?index\.js?$/);

req.keys().forEach((mod) => {
  let v = req(mod);
  const match = mod.match(/^\.\/([^_][\w-]+)\/index\.js?$/);
  if (match && match[1]) {
      try {
          //console.log(v.default);
          const name = camelCase(match[1]);
          if(name!=="WeaTools"&&name!=="WeaCheck") {
              //console.log("is1:"+name);
              v.default = WeaTools.tryCatch(React, MyErrorHandler, {error: "前端组件库异常，请联系管理员!"})(v.default);
              //v = wrapWithTryCatch(React, MyErrorHandler, {error: "Some custom error message!"})(v)
          }
          // exports[name] = v.default;
          ecCom[name] = v.default;

      }
      catch(e) {
          //console.log(e);
      }
  }
});
window.ecCom = ecCom;

require.context('./ecology9/base', true, /^\.\/[^_][\w-]+\/(style\/)?index\.js?$/);

// var weaSel = weaCom.initCom(
//     "dyf", //要渲染的DOM节点ID
//     "WeaSelect", //要引用的组件
//     {
//       id:"field8783",
//       name:"field8783",
//       tags:true, //标签模式，输入过的会临时存储
//       value:jQuery("#field8783").val()?jQuery("#field8783").val().split(","):[], //初始数据ID，多个用逗号隔开
//       multiple:false, //是否多选
//       datas:[{name:"a",value:"a"},{name:"b",value:"b"},{name:"c",value:"c"}],
//       style:{width:"100%"},
//       onChange:change.bind(this) //组件变化事件，多浏览器支持完善，包括chrome、IE8+
//     },
//     function() {
//       //组件加载完成后回调
//     }
// );
// function change(value,valueSpan) {
//     if(value && value.split(",").length>0) {
//         //console.log("value:",value," weaSel:",weaSel);
//         weaSel.setState({value:value.split(",")});
//     }
// }