import React from 'react';
import RcDropdown from '../../_rc-dropdown-1.4.8/src';

export default class Dropdown extends React.Component {
  static defaultProps = {
    transitionName: 'slide-up',
    prefixCls: 'ant-dropdown',
    mouseEnterDelay: 0.15,
    mouseLeaveDelay: 0.1,
  }

  render() {
    return <RcDropdown {...this.props} />;
  }
}
