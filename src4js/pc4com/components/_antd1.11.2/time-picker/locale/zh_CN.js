import TimepickerLocale from '../../../_rc-time-picker-1.1.6/src/locale/zh_CN';

const locale = {
  placeholder: '请选择时间',
  ... TimepickerLocale,
};

export default locale;
