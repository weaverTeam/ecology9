import React from 'react';
import CalendarLocale from '../../_rc-calendar-5.6.2/src/locale/zh_CN';
import RcCalendar from '../../_rc-calendar-5.6.2';

export default class Calendar extends React.Component {
  static defaultProps = {
    locale: CalendarLocale,
    prefixCls: 'ant-calendar',
  }

  render() {
    return <RcCalendar {...this.props} />;
  }
}
