import RcCalendar from '../../_rc-calendar-5.6.2';
import MonthCalendar from '../../_rc-calendar-5.6.2/src/MonthCalendar';
import createPicker from './createPicker';
import wrapPicker from './wrapPicker';
import RangePicker from './RangePicker';
import Calendar from './Calendar';

const DatePicker = wrapPicker(createPicker(RcCalendar));
const MonthPicker = wrapPicker(createPicker(MonthCalendar), 'yyyy-MM');

DatePicker.Calendar = Calendar;
DatePicker.RangePicker = wrapPicker(RangePicker, 'yyyy-MM-dd');
DatePicker.MonthPicker = MonthPicker;

export default DatePicker;
