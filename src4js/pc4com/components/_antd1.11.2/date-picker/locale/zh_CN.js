import GregorianCalendarLocale from '../../../_gregorian-calendar/lib/locale/zh_CN';
import CalendarLocale from '../../../_rc-calendar-5.6.2/src/locale/zh_CN';
import TimePickerLocale from '../../time-picker/locale/zh_CN';

// 统一合并为完整的 Locale
// const locale = { ...GregorianCalendarLocale };
let locale = {
  // in minutes
  timezoneOffset: 8 * 60,
  firstDayOfWeek: 0,
  minimalDaysInFirstWeek: 1
}
locale.lang = {
  placeholder: '请选择日期',
  rangePlaceholder: ['开始日期', '结束日期'],
  today: '今天',
  now: '此刻',
  backToToday: '返回今天',
  ok: '确定',
  clear: '清除',
  month: '月',
  year: '年',
  previousMonth: '上个月 (翻页上键)',
  nextMonth: '下个月 (翻页下键)',
  monthSelect: '选择月份',
  yearSelect: '选择年份',
  decadeSelect: '选择年代',
  yearFormat: 'yyyy\'年\'',
  monthFormat: 'M\'月\'',
  dateFormat: 'yyyy\'年\'M\'月\'d\'日\'',
  previousYear: '上一年 (Control键加左方向键)',
  nextYear: '下一年 (Control键加右方向键)',
  previousDecade: '上一年代',
  nextDecade: '下一年代',
  previousCentury: '上一世纪',
  nextCentury: '下一世纪',
  format: {
      eras: ['公元前', '公元'],
      months: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
      shortMonths: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
      weekdays: ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
      shortWeekdays: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
      veryShortWeekdays: ['日', '一', '二', '三', '四', '五', '六'],
      ampms: ['上午', '下午'],
      datePatterns: ['yyyy\'年\'M\'月\'d\'日\' EEEE', 'yyyy\'年\'M\'月\'d\'日\'', 'yyyy-M-d', 'yy-M-d'],
      timePatterns: ['ahh\'时\'mm\'分\'ss\'秒\' \'GMT\'Z', 'ahh\'时\'mm\'分\'ss\'秒\'', 'H:mm:ss', 'ah:mm'],
      dateTimePattern: '{date} {time}'
  },
};

locale.timePickerLocale = { ...TimePickerLocale };

// should add whitespace between char in Button
locale.lang.ok = '确 定';

// All settings at:
// https://github.com/ant-design/ant-design/issues/424

export default locale;
