import React from 'react';
import { changeConfirmLocale } from '../modal/locale';
import merge from 'lodash/merge'

export default class LocaleProvider extends React.Component {
  static propTypes = {
    locale: React.PropTypes.object,
  }

  static childContextTypes = {
    antLocale: React.PropTypes.object,
  }
  constructor(props) {
		super(props);
		this.state = {
			comsLocale: {}
		}
  }

  getChildContext() {
    return {
      antLocale: merge(this.state.comsLocale, this.props.locale || {}),
    };
  }

  componentDidMount() {
//	WeaTools.callApi('/api/ec/api/locale/taglist','GET',{isused:true,routername:'/coms'}).then( res => {
//	  	this.componentDidUpdate();
//		});
  }

  componentDidUpdate() {
    const { locale } = this.props;
    const { comsLocale } = this.state;
    const newLocale =  merge(comsLocale, locale || {});
    changeConfirmLocale(newLocale.Modal);
  }

  componentWillUnMount() {
    changeConfirmLocale();
  }

  render() {
    return React.Children.only(this.props.children);
  }
}
