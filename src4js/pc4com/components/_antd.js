import util from './_util/util'

class MyErrorHandler extends React.Component {
    render(){
        return (
            <div className="terrible-error">{this.props.error}</div>
        );
    }
}

let req = require.context('./_antd1.11.2', true, /^\.\/[^_][\w-]+\/(style\/)?index\.jsx?$/);
req.keys().forEach((mod) => {
  let v = req(mod);
  const match = mod.match(/^\.\/([^_][\w-]+)\/index\.jsx?$/);
  if (match && match[1]) {
    let name = match[1];
    if (match[1] === 'message' || match[1] === 'notification') {
    } else {
      name = util.camelCase(name);
    }
    // v.default = util.tryCatch(React, MyErrorHandler, {error: "前端组件库异常，请联系管理员!"})(v.default);
    exports[name] = v.default;
  }
});